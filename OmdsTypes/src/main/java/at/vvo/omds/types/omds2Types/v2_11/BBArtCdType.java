
package at.vvo.omds.types.omds2Types.v2_11;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für BBArtCd_Type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="BBArtCd_Type"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="ABL"/&gt;
 *     &lt;enumeration value="ERL"/&gt;
 *     &lt;enumeration value="SLF"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "BBArtCd_Type")
@XmlEnum
public enum BBArtCdType {


    /**
     * Ablebensfall
     * 
     */
    ABL,

    /**
     * Erlebensfall
     * 
     */
    ERL,

    /**
     * sonstiger Leistungsfall
     * 
     */
    SLF;

    public String value() {
        return name();
    }

    public static BBArtCdType fromValue(String v) {
        return valueOf(v);
    }

}
