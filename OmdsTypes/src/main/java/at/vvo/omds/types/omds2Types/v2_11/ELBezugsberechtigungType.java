
package at.vvo.omds.types.omds2Types.v2_11;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für EL-Bezugsberechtigung_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="EL-Bezugsberechtigung_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="BBArtCd" use="required" type="{urn:omds20}BBArtCd_Type" /&gt;
 *       &lt;attribute name="BBTxt" use="required"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="255"/&gt;
 *             &lt;minLength value="1"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EL-Bezugsberechtigung_Type")
public class ELBezugsberechtigungType {

    @XmlAttribute(name = "BBArtCd", required = true)
    protected BBArtCdType bbArtCd;
    @XmlAttribute(name = "BBTxt", required = true)
    protected String bbTxt;

    /**
     * Ruft den Wert der bbArtCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BBArtCdType }
     *     
     */
    public BBArtCdType getBBArtCd() {
        return bbArtCd;
    }

    /**
     * Legt den Wert der bbArtCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BBArtCdType }
     *     
     */
    public void setBBArtCd(BBArtCdType value) {
        this.bbArtCd = value;
    }

    /**
     * Ruft den Wert der bbTxt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBBTxt() {
        return bbTxt;
    }

    /**
     * Legt den Wert der bbTxt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBBTxt(String value) {
        this.bbTxt = value;
    }

}
