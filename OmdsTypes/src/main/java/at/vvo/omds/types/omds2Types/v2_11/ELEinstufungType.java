
package at.vvo.omds.types.omds2Types.v2_11;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für EL-Einstufung_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="EL-Einstufung_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="EstArtCd" use="required" type="{urn:omds20}EstArtCd_Type" /&gt;
 *       &lt;attribute name="EstWert" use="required"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="5"/&gt;
 *             &lt;minLength value="1"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EL-Einstufung_Type")
public class ELEinstufungType {

    @XmlAttribute(name = "EstArtCd", required = true)
    protected EstArtCdType estArtCd;
    @XmlAttribute(name = "EstWert", required = true)
    protected String estWert;

    /**
     * Ruft den Wert der estArtCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EstArtCdType }
     *     
     */
    public EstArtCdType getEstArtCd() {
        return estArtCd;
    }

    /**
     * Legt den Wert der estArtCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EstArtCdType }
     *     
     */
    public void setEstArtCd(EstArtCdType value) {
        this.estArtCd = value;
    }

    /**
     * Ruft den Wert der estWert-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstWert() {
        return estWert;
    }

    /**
     * Legt den Wert der estWert-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstWert(String value) {
        this.estWert = value;
    }

}
