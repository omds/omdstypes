
package at.vvo.omds.types.omds2Types.v2_11;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für EL-Entscheidungsfrage_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="EL-Entscheidungsfrage_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="EFrageCd" use="required" type="{urn:omds20}EFrageCd_Type" /&gt;
 *       &lt;attribute name="EFrageAntw" use="required" type="{urn:omds20}Entsch3_Type" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EL-Entscheidungsfrage_Type")
public class ELEntscheidungsfrageType {

    @XmlAttribute(name = "EFrageCd", required = true)
    protected String eFrageCd;
    @XmlAttribute(name = "EFrageAntw", required = true)
    protected String eFrageAntw;

    /**
     * Ruft den Wert der eFrageCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEFrageCd() {
        return eFrageCd;
    }

    /**
     * Legt den Wert der eFrageCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEFrageCd(String value) {
        this.eFrageCd = value;
    }

    /**
     * Ruft den Wert der eFrageAntw-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEFrageAntw() {
        return eFrageAntw;
    }

    /**
     * Legt den Wert der eFrageAntw-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEFrageAntw(String value) {
        this.eFrageAntw = value;
    }

}
