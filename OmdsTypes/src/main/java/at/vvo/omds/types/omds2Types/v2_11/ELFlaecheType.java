
package at.vvo.omds.types.omds2Types.v2_11;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für EL-Flaeche_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="EL-Flaeche_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="FlaechenAttributCd" use="required" type="{urn:omds20}FlaechenAttributCd_Type" /&gt;
 *       &lt;attribute name="Nummer"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="2"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="InnenFlaeche" type="{urn:omds20}decimal" /&gt;
 *       &lt;attribute name="VerbauteFlaeche" type="{urn:omds20}decimal" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EL-Flaeche_Type")
public class ELFlaecheType {

    @XmlAttribute(name = "FlaechenAttributCd", required = true)
    protected FlaechenAttributCdType flaechenAttributCd;
    @XmlAttribute(name = "Nummer")
    protected String nummer;
    @XmlAttribute(name = "InnenFlaeche")
    protected BigDecimal innenFlaeche;
    @XmlAttribute(name = "VerbauteFlaeche")
    protected BigDecimal verbauteFlaeche;

    /**
     * Ruft den Wert der flaechenAttributCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link FlaechenAttributCdType }
     *     
     */
    public FlaechenAttributCdType getFlaechenAttributCd() {
        return flaechenAttributCd;
    }

    /**
     * Legt den Wert der flaechenAttributCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link FlaechenAttributCdType }
     *     
     */
    public void setFlaechenAttributCd(FlaechenAttributCdType value) {
        this.flaechenAttributCd = value;
    }

    /**
     * Ruft den Wert der nummer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNummer() {
        return nummer;
    }

    /**
     * Legt den Wert der nummer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNummer(String value) {
        this.nummer = value;
    }

    /**
     * Ruft den Wert der innenFlaeche-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getInnenFlaeche() {
        return innenFlaeche;
    }

    /**
     * Legt den Wert der innenFlaeche-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setInnenFlaeche(BigDecimal value) {
        this.innenFlaeche = value;
    }

    /**
     * Ruft den Wert der verbauteFlaeche-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getVerbauteFlaeche() {
        return verbauteFlaeche;
    }

    /**
     * Legt den Wert der verbauteFlaeche-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setVerbauteFlaeche(BigDecimal value) {
        this.verbauteFlaeche = value;
    }

}
