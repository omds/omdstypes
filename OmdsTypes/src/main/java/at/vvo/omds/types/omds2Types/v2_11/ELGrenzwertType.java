
package at.vvo.omds.types.omds2Types.v2_11;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für EL-Grenzwert_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="EL-Grenzwert_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="GrwArtCd" use="required" type="{urn:omds20}GrwArtCd_Type" /&gt;
 *       &lt;attribute name="GrWert" use="required" type="{urn:omds20}decimal" /&gt;
 *       &lt;attribute name="GrwTyp"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="40"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EL-Grenzwert_Type")
public class ELGrenzwertType {

    @XmlAttribute(name = "GrwArtCd", required = true)
    protected GrwArtCdType grwArtCd;
    @XmlAttribute(name = "GrWert", required = true)
    protected BigDecimal grWert;
    @XmlAttribute(name = "GrwTyp")
    protected String grwTyp;

    /**
     * Ruft den Wert der grwArtCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GrwArtCdType }
     *     
     */
    public GrwArtCdType getGrwArtCd() {
        return grwArtCd;
    }

    /**
     * Legt den Wert der grwArtCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GrwArtCdType }
     *     
     */
    public void setGrwArtCd(GrwArtCdType value) {
        this.grwArtCd = value;
    }

    /**
     * Ruft den Wert der grWert-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getGrWert() {
        return grWert;
    }

    /**
     * Legt den Wert der grWert-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setGrWert(BigDecimal value) {
        this.grWert = value;
    }

    /**
     * Ruft den Wert der grwTyp-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGrwTyp() {
        return grwTyp;
    }

    /**
     * Legt den Wert der grwTyp-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGrwTyp(String value) {
        this.grwTyp = value;
    }

}
