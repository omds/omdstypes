
package at.vvo.omds.types.omds2Types.v2_11;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java-Klasse für EL-Identifizierung_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="EL-Identifizierung_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="IdfArtCd" use="required" type="{urn:omds20}IdfArtCd_Type" /&gt;
 *       &lt;attribute name="IdfSchluessel" use="required"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="40"/&gt;
 *             &lt;minLength value="1"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="IdfDatum" type="{urn:omds20}Datum" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EL-Identifizierung_Type")
public class ELIdentifizierungType {

    @XmlAttribute(name = "IdfArtCd", required = true)
    protected IdfArtCdType idfArtCd;
    @XmlAttribute(name = "IdfSchluessel", required = true)
    protected String idfSchluessel;
    @XmlAttribute(name = "IdfDatum")
    protected XMLGregorianCalendar idfDatum;

    /**
     * Ruft den Wert der idfArtCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link IdfArtCdType }
     *     
     */
    public IdfArtCdType getIdfArtCd() {
        return idfArtCd;
    }

    /**
     * Legt den Wert der idfArtCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link IdfArtCdType }
     *     
     */
    public void setIdfArtCd(IdfArtCdType value) {
        this.idfArtCd = value;
    }

    /**
     * Ruft den Wert der idfSchluessel-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdfSchluessel() {
        return idfSchluessel;
    }

    /**
     * Legt den Wert der idfSchluessel-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdfSchluessel(String value) {
        this.idfSchluessel = value;
    }

    /**
     * Ruft den Wert der idfDatum-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getIdfDatum() {
        return idfDatum;
    }

    /**
     * Legt den Wert der idfDatum-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setIdfDatum(XMLGregorianCalendar value) {
        this.idfDatum = value;
    }

}
