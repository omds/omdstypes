
package at.vvo.omds.types.omds2Types.v2_11;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für EL-Objekt_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="EL-Objekt_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="ObjLfnr" use="required" type="{http://www.w3.org/2001/XMLSchema}unsignedShort" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EL-Objekt_Type")
public class ELObjektType {

    @XmlAttribute(name = "ObjLfnr", required = true)
    @XmlSchemaType(name = "unsignedShort")
    protected int objLfnr;

    /**
     * Ruft den Wert der objLfnr-Eigenschaft ab.
     * 
     */
    public int getObjLfnr() {
        return objLfnr;
    }

    /**
     * Legt den Wert der objLfnr-Eigenschaft fest.
     * 
     */
    public void setObjLfnr(int value) {
        this.objLfnr = value;
    }

}
