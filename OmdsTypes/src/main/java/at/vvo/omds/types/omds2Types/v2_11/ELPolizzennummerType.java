
package at.vvo.omds.types.omds2Types.v2_11;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für EL-Polizzennummer_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="EL-Polizzennummer_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="PolArtCd" use="required" type="{urn:omds20}PolArtCd_Type" /&gt;
 *       &lt;attribute name="PolNr" use="required" type="{urn:omds20}Polizzennr" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EL-Polizzennummer_Type")
public class ELPolizzennummerType {

    @XmlAttribute(name = "PolArtCd", required = true)
    protected PolArtCdType polArtCd;
    @XmlAttribute(name = "PolNr", required = true)
    protected String polNr;

    /**
     * Ruft den Wert der polArtCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PolArtCdType }
     *     
     */
    public PolArtCdType getPolArtCd() {
        return polArtCd;
    }

    /**
     * Legt den Wert der polArtCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PolArtCdType }
     *     
     */
    public void setPolArtCd(PolArtCdType value) {
        this.polArtCd = value;
    }

    /**
     * Ruft den Wert der polNr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolNr() {
        return polNr;
    }

    /**
     * Legt den Wert der polNr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolNr(String value) {
        this.polNr = value;
    }

}
