
package at.vvo.omds.types.omds2Types.v2_11;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java-Klasse für EL-Praemienfreistellung_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="EL-Praemienfreistellung_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="PfrArtCd" use="required" type="{urn:omds20}PfrArtCd_Type" /&gt;
 *       &lt;attribute name="PfrBeg" use="required" type="{urn:omds20}Datum" /&gt;
 *       &lt;attribute name="PfrEnde" type="{urn:omds20}Datum" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EL-Praemienfreistellung_Type")
public class ELPraemienfreistellungType {

    @XmlAttribute(name = "PfrArtCd", required = true)
    protected String pfrArtCd;
    @XmlAttribute(name = "PfrBeg", required = true)
    protected XMLGregorianCalendar pfrBeg;
    @XmlAttribute(name = "PfrEnde")
    protected XMLGregorianCalendar pfrEnde;

    /**
     * Ruft den Wert der pfrArtCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPfrArtCd() {
        return pfrArtCd;
    }

    /**
     * Legt den Wert der pfrArtCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPfrArtCd(String value) {
        this.pfrArtCd = value;
    }

    /**
     * Ruft den Wert der pfrBeg-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPfrBeg() {
        return pfrBeg;
    }

    /**
     * Legt den Wert der pfrBeg-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPfrBeg(XMLGregorianCalendar value) {
        this.pfrBeg = value;
    }

    /**
     * Ruft den Wert der pfrEnde-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPfrEnde() {
        return pfrEnde;
    }

    /**
     * Legt den Wert der pfrEnde-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPfrEnde(XMLGregorianCalendar value) {
        this.pfrEnde = value;
    }

}
