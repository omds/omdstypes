
package at.vvo.omds.types.omds2Types.v2_11;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für EL-Text_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="EL-Text_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="TxtArtCd" use="required" type="{urn:omds20}TxtArtCd_Type" /&gt;
 *       &lt;attribute name="TxtInhalt" use="required"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;minLength value="1"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EL-Text_Type")
public class ELTextType {

    @XmlAttribute(name = "TxtArtCd", required = true)
    protected TxtArtCdType txtArtCd;
    @XmlAttribute(name = "TxtInhalt", required = true)
    protected String txtInhalt;

    /**
     * Ruft den Wert der txtArtCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TxtArtCdType }
     *     
     */
    public TxtArtCdType getTxtArtCd() {
        return txtArtCd;
    }

    /**
     * Legt den Wert der txtArtCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TxtArtCdType }
     *     
     */
    public void setTxtArtCd(TxtArtCdType value) {
        this.txtArtCd = value;
    }

    /**
     * Ruft den Wert der txtInhalt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTxtInhalt() {
        return txtInhalt;
    }

    /**
     * Legt den Wert der txtInhalt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTxtInhalt(String value) {
        this.txtInhalt = value;
    }

}
