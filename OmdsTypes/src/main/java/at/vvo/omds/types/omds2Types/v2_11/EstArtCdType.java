
package at.vvo.omds.types.omds2Types.v2_11;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für EstArtCd_Type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="EstArtCd_Type"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="G"/&gt;
 *     &lt;enumeration value="T"/&gt;
 *     &lt;enumeration value="TVU"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "EstArtCd_Type")
@XmlEnum
public enum EstArtCdType {


    /**
     * Gefahrenklasse
     * 
     */
    G,

    /**
     * Tarif-, Bonus/Malus-Stufe offiziell
     * 
     */
    T,

    /**
     * Tarif-, Bonus/Malus-Stufe VU-intern
     * 
     */
    TVU;

    public String value() {
        return name();
    }

    public static EstArtCdType fromValue(String v) {
        return valueOf(v);
    }

}
