
package at.vvo.omds.types.omds2Types.v2_11;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java-Klasse für FONDS_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="FONDS_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="ISIN" use="required" type="{urn:omds20}ISIN_Type" /&gt;
 *       &lt;attribute name="WKN" type="{urn:omds20}WKN_Type" /&gt;
 *       &lt;attribute name="Bezeichnung" use="required" type="{urn:omds20}FondsBez_Type" /&gt;
 *       &lt;attribute name="Kurs" type="{urn:omds20}FondsBetrag_Type" /&gt;
 *       &lt;attribute name="AnteilWertpapier" type="{urn:omds20}FondsAnteil_Type" /&gt;
 *       &lt;attribute name="Prozentsatz" use="required" type="{urn:omds20}Prozentsatz_Type" /&gt;
 *       &lt;attribute name="Wert" type="{urn:omds20}FondsBetrag_Type" /&gt;
 *       &lt;attribute name="WaehrungsCd" type="{urn:omds20}WaehrungsCd_Type" /&gt;
 *       &lt;attribute name="Stichtag" type="{urn:omds20}Datum" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FONDS_Type")
public class FONDSType {

    @XmlAttribute(name = "ISIN", required = true)
    protected String isin;
    @XmlAttribute(name = "WKN")
    protected String wkn;
    @XmlAttribute(name = "Bezeichnung", required = true)
    protected String bezeichnung;
    @XmlAttribute(name = "Kurs")
    protected BigDecimal kurs;
    @XmlAttribute(name = "AnteilWertpapier")
    protected BigDecimal anteilWertpapier;
    @XmlAttribute(name = "Prozentsatz", required = true)
    protected BigDecimal prozentsatz;
    @XmlAttribute(name = "Wert")
    protected BigDecimal wert;
    @XmlAttribute(name = "WaehrungsCd")
    protected WaehrungsCdType waehrungsCd;
    @XmlAttribute(name = "Stichtag")
    protected XMLGregorianCalendar stichtag;

    /**
     * Ruft den Wert der isin-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getISIN() {
        return isin;
    }

    /**
     * Legt den Wert der isin-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setISIN(String value) {
        this.isin = value;
    }

    /**
     * Ruft den Wert der wkn-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWKN() {
        return wkn;
    }

    /**
     * Legt den Wert der wkn-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWKN(String value) {
        this.wkn = value;
    }

    /**
     * Ruft den Wert der bezeichnung-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBezeichnung() {
        return bezeichnung;
    }

    /**
     * Legt den Wert der bezeichnung-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBezeichnung(String value) {
        this.bezeichnung = value;
    }

    /**
     * Ruft den Wert der kurs-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getKurs() {
        return kurs;
    }

    /**
     * Legt den Wert der kurs-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setKurs(BigDecimal value) {
        this.kurs = value;
    }

    /**
     * Ruft den Wert der anteilWertpapier-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAnteilWertpapier() {
        return anteilWertpapier;
    }

    /**
     * Legt den Wert der anteilWertpapier-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAnteilWertpapier(BigDecimal value) {
        this.anteilWertpapier = value;
    }

    /**
     * Ruft den Wert der prozentsatz-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getProzentsatz() {
        return prozentsatz;
    }

    /**
     * Legt den Wert der prozentsatz-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setProzentsatz(BigDecimal value) {
        this.prozentsatz = value;
    }

    /**
     * Ruft den Wert der wert-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getWert() {
        return wert;
    }

    /**
     * Legt den Wert der wert-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setWert(BigDecimal value) {
        this.wert = value;
    }

    /**
     * Ruft den Wert der waehrungsCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link WaehrungsCdType }
     *     
     */
    public WaehrungsCdType getWaehrungsCd() {
        return waehrungsCd;
    }

    /**
     * Legt den Wert der waehrungsCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link WaehrungsCdType }
     *     
     */
    public void setWaehrungsCd(WaehrungsCdType value) {
        this.waehrungsCd = value;
    }

    /**
     * Ruft den Wert der stichtag-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStichtag() {
        return stichtag;
    }

    /**
     * Legt den Wert der stichtag-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStichtag(XMLGregorianCalendar value) {
        this.stichtag = value;
    }

}
