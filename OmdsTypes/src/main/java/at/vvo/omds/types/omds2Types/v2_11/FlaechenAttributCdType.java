
package at.vvo.omds.types.omds2Types.v2_11;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für FlaechenAttributCd_Type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="FlaechenAttributCd_Type"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="EG"/&gt;
 *     &lt;enumeration value="SW"/&gt;
 *     &lt;enumeration value="MA"/&gt;
 *     &lt;enumeration value="KM"/&gt;
 *     &lt;enumeration value="KO"/&gt;
 *     &lt;enumeration value="WF"/&gt;
 *     &lt;enumeration value="GF"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "FlaechenAttributCd_Type")
@XmlEnum
public enum FlaechenAttributCdType {


    /**
     * Erdgeschoß
     * 
     */
    EG,

    /**
     * Stockwerk
     * 
     */
    SW,

    /**
     * Mansarde
     * 
     */
    MA,

    /**
     * Keller mit Wohnnutzung
     * 
     */
    KM,

    /**
     * Keller ohne Wohnnutzung
     * 
     */
    KO,

    /**
     * Wohnfläche
     * 
     */
    WF,

    /**
     * Grundfläche
     * 
     */
    GF;

    public String value() {
        return name();
    }

    public static FlaechenAttributCdType fromValue(String v) {
        return valueOf(v);
    }

}
