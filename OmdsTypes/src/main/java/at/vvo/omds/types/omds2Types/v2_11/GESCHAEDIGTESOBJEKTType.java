
package at.vvo.omds.types.omds2Types.v2_11;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für GESCHAEDIGTES_OBJEKT_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="GESCHAEDIGTES_OBJEKT_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="GeschObjektLfnr" use="required" type="{http://www.w3.org/2001/XMLSchema}unsignedShort" /&gt;
 *       &lt;attribute name="SchadenBeschreibung"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="100"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="VUNrGesch" type="{urn:omds20}VUNr" /&gt;
 *       &lt;attribute name="VUNameGesch"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="40"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="PolNrGesch" type="{urn:omds20}Polizzennr" /&gt;
 *       &lt;attribute name="SchadennrGesch"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{urn:omds20}Schadennr"&gt;
 *             &lt;maxLength value="35"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="LandesCd_GeschKfz" type="{urn:omds20}LandesCd_Type" /&gt;
 *       &lt;attribute name="Kennz_GeschKfz"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="12"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GESCHAEDIGTES_OBJEKT_Type")
public class GESCHAEDIGTESOBJEKTType {

    @XmlAttribute(name = "GeschObjektLfnr", required = true)
    @XmlSchemaType(name = "unsignedShort")
    protected int geschObjektLfnr;
    @XmlAttribute(name = "SchadenBeschreibung")
    protected String schadenBeschreibung;
    @XmlAttribute(name = "VUNrGesch")
    protected String vuNrGesch;
    @XmlAttribute(name = "VUNameGesch")
    protected String vuNameGesch;
    @XmlAttribute(name = "PolNrGesch")
    protected String polNrGesch;
    @XmlAttribute(name = "SchadennrGesch")
    protected String schadennrGesch;
    @XmlAttribute(name = "LandesCd_GeschKfz")
    protected String landesCdGeschKfz;
    @XmlAttribute(name = "Kennz_GeschKfz")
    protected String kennzGeschKfz;

    /**
     * Ruft den Wert der geschObjektLfnr-Eigenschaft ab.
     * 
     */
    public int getGeschObjektLfnr() {
        return geschObjektLfnr;
    }

    /**
     * Legt den Wert der geschObjektLfnr-Eigenschaft fest.
     * 
     */
    public void setGeschObjektLfnr(int value) {
        this.geschObjektLfnr = value;
    }

    /**
     * Ruft den Wert der schadenBeschreibung-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSchadenBeschreibung() {
        return schadenBeschreibung;
    }

    /**
     * Legt den Wert der schadenBeschreibung-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSchadenBeschreibung(String value) {
        this.schadenBeschreibung = value;
    }

    /**
     * Ruft den Wert der vuNrGesch-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVUNrGesch() {
        return vuNrGesch;
    }

    /**
     * Legt den Wert der vuNrGesch-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVUNrGesch(String value) {
        this.vuNrGesch = value;
    }

    /**
     * Ruft den Wert der vuNameGesch-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVUNameGesch() {
        return vuNameGesch;
    }

    /**
     * Legt den Wert der vuNameGesch-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVUNameGesch(String value) {
        this.vuNameGesch = value;
    }

    /**
     * Ruft den Wert der polNrGesch-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolNrGesch() {
        return polNrGesch;
    }

    /**
     * Legt den Wert der polNrGesch-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolNrGesch(String value) {
        this.polNrGesch = value;
    }

    /**
     * Ruft den Wert der schadennrGesch-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSchadennrGesch() {
        return schadennrGesch;
    }

    /**
     * Legt den Wert der schadennrGesch-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSchadennrGesch(String value) {
        this.schadennrGesch = value;
    }

    /**
     * Ruft den Wert der landesCdGeschKfz-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLandesCdGeschKfz() {
        return landesCdGeschKfz;
    }

    /**
     * Legt den Wert der landesCdGeschKfz-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLandesCdGeschKfz(String value) {
        this.landesCdGeschKfz = value;
    }

    /**
     * Ruft den Wert der kennzGeschKfz-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKennzGeschKfz() {
        return kennzGeschKfz;
    }

    /**
     * Legt den Wert der kennzGeschKfz-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKennzGeschKfz(String value) {
        this.kennzGeschKfz = value;
    }

}
