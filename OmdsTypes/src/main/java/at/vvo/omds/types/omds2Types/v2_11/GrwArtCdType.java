
package at.vvo.omds.types.omds2Types.v2_11;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für GrwArtCd_Type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="GrwArtCd_Type"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="DFP"/&gt;
 *     &lt;enumeration value="KAM"/&gt;
 *     &lt;enumeration value="KAT"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "GrwArtCd_Type")
@XmlEnum
public enum GrwArtCdType {


    /**
     * DauerfolgenProz(UV)
     * 
     */
    DFP,

    /**
     * Karenzmonate
     * 
     */
    KAM,

    /**
     * Karenztage
     * 
     */
    KAT;

    public String value() {
        return name();
    }

    public static GrwArtCdType fromValue(String v) {
        return valueOf(v);
    }

}
