
package at.vvo.omds.types.omds2Types.v2_11;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java-Klasse für LOESCHANSTOSS_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="LOESCHANSTOSS_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="LoeschID" use="required" type="{urn:omds20}Datum-Zeit2" /&gt;
 *       &lt;attribute name="SystemQuelle" default="  "&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;minLength value="1"/&gt;
 *             &lt;maxLength value="2"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="GueltigAb" type="{urn:omds20}Datum" /&gt;
 *       &lt;attribute name="Polizzennr" type="{urn:omds20}Polizzennr" /&gt;
 *       &lt;attribute name="VertragsID" type="{urn:omds20}VertragsID" /&gt;
 *       &lt;attribute name="Personennr" type="{urn:omds20}Personennr" /&gt;
 *       &lt;attribute name="Schadennr" type="{urn:omds20}Schadennr" /&gt;
 *       &lt;attribute name="ProvisionsID"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="26"/&gt;
 *             &lt;minLength value="1"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="MahnverfahrenNr"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;minLength value="1"/&gt;
 *             &lt;maxLength value="32"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="LoeschCd" use="required" type="{urn:omds20}LoeschCd_Type" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LOESCHANSTOSS_Type")
public class LOESCHANSTOSSType {

    @XmlAttribute(name = "LoeschID", required = true)
    protected XMLGregorianCalendar loeschID;
    @XmlAttribute(name = "SystemQuelle")
    protected String systemQuelle;
    @XmlAttribute(name = "GueltigAb")
    protected XMLGregorianCalendar gueltigAb;
    @XmlAttribute(name = "Polizzennr")
    protected String polizzennr;
    @XmlAttribute(name = "VertragsID")
    protected String vertragsID;
    @XmlAttribute(name = "Personennr")
    protected String personennr;
    @XmlAttribute(name = "Schadennr")
    protected String schadennr;
    @XmlAttribute(name = "ProvisionsID")
    protected String provisionsID;
    @XmlAttribute(name = "MahnverfahrenNr")
    protected String mahnverfahrenNr;
    @XmlAttribute(name = "LoeschCd", required = true)
    protected LoeschCdType loeschCd;

    /**
     * Ruft den Wert der loeschID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getLoeschID() {
        return loeschID;
    }

    /**
     * Legt den Wert der loeschID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setLoeschID(XMLGregorianCalendar value) {
        this.loeschID = value;
    }

    /**
     * Ruft den Wert der systemQuelle-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSystemQuelle() {
        if (systemQuelle == null) {
            return "  ";
        } else {
            return systemQuelle;
        }
    }

    /**
     * Legt den Wert der systemQuelle-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSystemQuelle(String value) {
        this.systemQuelle = value;
    }

    /**
     * Ruft den Wert der gueltigAb-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getGueltigAb() {
        return gueltigAb;
    }

    /**
     * Legt den Wert der gueltigAb-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setGueltigAb(XMLGregorianCalendar value) {
        this.gueltigAb = value;
    }

    /**
     * Ruft den Wert der polizzennr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolizzennr() {
        return polizzennr;
    }

    /**
     * Legt den Wert der polizzennr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolizzennr(String value) {
        this.polizzennr = value;
    }

    /**
     * Ruft den Wert der vertragsID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVertragsID() {
        return vertragsID;
    }

    /**
     * Legt den Wert der vertragsID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVertragsID(String value) {
        this.vertragsID = value;
    }

    /**
     * Ruft den Wert der personennr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPersonennr() {
        return personennr;
    }

    /**
     * Legt den Wert der personennr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPersonennr(String value) {
        this.personennr = value;
    }

    /**
     * Ruft den Wert der schadennr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSchadennr() {
        return schadennr;
    }

    /**
     * Legt den Wert der schadennr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSchadennr(String value) {
        this.schadennr = value;
    }

    /**
     * Ruft den Wert der provisionsID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProvisionsID() {
        return provisionsID;
    }

    /**
     * Legt den Wert der provisionsID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProvisionsID(String value) {
        this.provisionsID = value;
    }

    /**
     * Ruft den Wert der mahnverfahrenNr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMahnverfahrenNr() {
        return mahnverfahrenNr;
    }

    /**
     * Legt den Wert der mahnverfahrenNr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMahnverfahrenNr(String value) {
        this.mahnverfahrenNr = value;
    }

    /**
     * Ruft den Wert der loeschCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link LoeschCdType }
     *     
     */
    public LoeschCdType getLoeschCd() {
        return loeschCd;
    }

    /**
     * Legt den Wert der loeschCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link LoeschCdType }
     *     
     */
    public void setLoeschCd(LoeschCdType value) {
        this.loeschCd = value;
    }

}
