
package at.vvo.omds.types.omds2Types.v2_11;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java-Klasse für NATUERLICHE_PERSON_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="NATUERLICHE_PERSON_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="Familienname" use="required"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="80"/&gt;
 *             &lt;minLength value="1"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="Vorname"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="40"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="GeschlechtCd" use="required" type="{urn:omds20}GeschlechtCd_Type" /&gt;
 *       &lt;attribute name="Gebdat" type="{urn:omds20}Datum" /&gt;
 *       &lt;attribute name="LandesCd" use="required" type="{urn:omds20}LandesCd_Type" /&gt;
 *       &lt;attribute name="FamilienstandCd" use="required" type="{urn:omds20}FamilienstandCd_Type" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NATUERLICHE_PERSON_Type")
public class NATUERLICHEPERSONType {

    @XmlAttribute(name = "Familienname", required = true)
    protected String familienname;
    @XmlAttribute(name = "Vorname")
    protected String vorname;
    @XmlAttribute(name = "GeschlechtCd", required = true)
    protected String geschlechtCd;
    @XmlAttribute(name = "Gebdat")
    protected XMLGregorianCalendar gebdat;
    @XmlAttribute(name = "LandesCd", required = true)
    protected String landesCd;
    @XmlAttribute(name = "FamilienstandCd", required = true)
    protected String familienstandCd;

    /**
     * Ruft den Wert der familienname-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFamilienname() {
        return familienname;
    }

    /**
     * Legt den Wert der familienname-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFamilienname(String value) {
        this.familienname = value;
    }

    /**
     * Ruft den Wert der vorname-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVorname() {
        return vorname;
    }

    /**
     * Legt den Wert der vorname-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVorname(String value) {
        this.vorname = value;
    }

    /**
     * Ruft den Wert der geschlechtCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGeschlechtCd() {
        return geschlechtCd;
    }

    /**
     * Legt den Wert der geschlechtCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGeschlechtCd(String value) {
        this.geschlechtCd = value;
    }

    /**
     * Ruft den Wert der gebdat-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getGebdat() {
        return gebdat;
    }

    /**
     * Legt den Wert der gebdat-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setGebdat(XMLGregorianCalendar value) {
        this.gebdat = value;
    }

    /**
     * Ruft den Wert der landesCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLandesCd() {
        return landesCd;
    }

    /**
     * Legt den Wert der landesCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLandesCd(String value) {
        this.landesCd = value;
    }

    /**
     * Ruft den Wert der familienstandCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFamilienstandCd() {
        return familienstandCd;
    }

    /**
     * Legt den Wert der familienstandCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFamilienstandCd(String value) {
        this.familienstandCd = value;
    }

}
