
package at.vvo.omds.types.omds2Types.v2_11;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the at.vvo.omds.types.omds2Types.v2_11 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ELKommunikation_QNAME = new QName("urn:omds20", "EL-Kommunikation");
    private final static QName _KLAUSEL_QNAME = new QName("urn:omds20", "KLAUSEL");
    private final static QName _LOESCHANSTOSS_QNAME = new QName("urn:omds20", "LOESCHANSTOSS");
    private final static QName _PERSON_QNAME = new QName("urn:omds20", "PERSON");
    private final static QName _ELAntrag_QNAME = new QName("urn:omds20", "EL-Antrag");
    private final static QName _ELAnzahl_QNAME = new QName("urn:omds20", "EL-Anzahl");
    private final static QName _ELBetrag_QNAME = new QName("urn:omds20", "EL-Betrag");
    private final static QName _ELBezugsberechtigung_QNAME = new QName("urn:omds20", "EL-Bezugsberechtigung");
    private final static QName _ELEinstufung_QNAME = new QName("urn:omds20", "EL-Einstufung");
    private final static QName _ELEntscheidungsfrage_QNAME = new QName("urn:omds20", "EL-Entscheidungsfrage");
    private final static QName _ELIdentifizierung_QNAME = new QName("urn:omds20", "EL-Identifizierung");
    private final static QName _ELKlausel_QNAME = new QName("urn:omds20", "EL-Klausel");
    private final static QName _ELPolizzennummer_QNAME = new QName("urn:omds20", "EL-Polizzennummer");
    private final static QName _ELPraemienfreistellung_QNAME = new QName("urn:omds20", "EL-Praemienfreistellung");
    private final static QName _ELPraemienkorrektur_QNAME = new QName("urn:omds20", "EL-Praemienkorrektur");
    private final static QName _ELRahmenvereinbarung_QNAME = new QName("urn:omds20", "EL-Rahmenvereinbarung");
    private final static QName _ELText_QNAME = new QName("urn:omds20", "EL-Text");
    private final static QName _VERTRAGSPERSON_QNAME = new QName("urn:omds20", "VERTRAGSPERSON");
    private final static QName _VERSOBJEKT_QNAME = new QName("urn:omds20", "VERS_OBJEKT");
    private final static QName _SPARTE_QNAME = new QName("urn:omds20", "SPARTE");
    private final static QName _SCHADEN_QNAME = new QName("urn:omds20", "SCHADEN");
    private final static QName _PROVISION_QNAME = new QName("urn:omds20", "PROVISION");
    private final static QName _MAHNUNG_QNAME = new QName("urn:omds20", "MAHNUNG");
    private final static QName _VERTRAGSFONDS_QNAME = new QName("urn:omds20", "VERTRAGSFONDS");
    private final static QName _NATUERLICHEPERSON_QNAME = new QName("urn:omds20", "NATUERLICHE_PERSON");
    private final static QName _SONSTIGEPERSON_QNAME = new QName("urn:omds20", "SONSTIGE_PERSON");
    private final static QName _VERSPERSON_QNAME = new QName("urn:omds20", "VERS_PERSON");
    private final static QName _VERSSACHE_QNAME = new QName("urn:omds20", "VERS_SACHE");
    private final static QName _RISIKO_QNAME = new QName("urn:omds20", "RISIKO");
    private final static QName _FONDS_QNAME = new QName("urn:omds20", "FONDS");
    private final static QName _SCHADENBETEILIGTER_QNAME = new QName("urn:omds20", "SCHADEN_BETEILIGTER");
    private final static QName _GESCHAEDIGTESOBJEKT_QNAME = new QName("urn:omds20", "GESCHAEDIGTES_OBJEKT");
    private final static QName _ZAHLUNG_QNAME = new QName("urn:omds20", "ZAHLUNG");
    private final static QName _ELGewinnbeteiligung_QNAME = new QName("urn:omds20", "EL-Gewinnbeteiligung");
    private final static QName _ELGrenzwert_QNAME = new QName("urn:omds20", "EL-Grenzwert");
    private final static QName _ELIndex_QNAME = new QName("urn:omds20", "EL-Index");
    private final static QName _ELLegitimation_QNAME = new QName("urn:omds20", "EL-Legitimation");
    private final static QName _ELObjekt_QNAME = new QName("urn:omds20", "EL-Objekt");
    private final static QName _ELObjektdaten_QNAME = new QName("urn:omds20", "EL-Objektdaten");
    private final static QName _ELFlaeche_QNAME = new QName("urn:omds20", "EL-Flaeche");
    private final static QName _ELRente_QNAME = new QName("urn:omds20", "EL-Rente");
    private final static QName _ELSteuer_QNAME = new QName("urn:omds20", "EL-Steuer");
    private final static QName _ELVersicherungssumme_QNAME = new QName("urn:omds20", "EL-Versicherungssumme");
    private final static QName _ELZeitraum_QNAME = new QName("urn:omds20", "EL-Zeitraum");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: at.vvo.omds.types.omds2Types.v2_11
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OMDS }
     * 
     */
    public OMDS createOMDS() {
        return new OMDS();
    }

    /**
     * Create an instance of {@link PAKET }
     * 
     */
    public PAKET createPAKET() {
        return new PAKET();
    }

    /**
     * Create an instance of {@link VERSUNTERNEHMEN }
     * 
     */
    public VERSUNTERNEHMEN createVERSUNTERNEHMEN() {
        return new VERSUNTERNEHMEN();
    }

    /**
     * Create an instance of {@link ELKommunikationType }
     * 
     */
    public ELKommunikationType createELKommunikationType() {
        return new ELKommunikationType();
    }

    /**
     * Create an instance of {@link SCHLUESSELART }
     * 
     */
    public SCHLUESSELART createSCHLUESSELART() {
        return new SCHLUESSELART();
    }

    /**
     * Create an instance of {@link SCHLUESSEL }
     * 
     */
    public SCHLUESSEL createSCHLUESSEL() {
        return new SCHLUESSEL();
    }

    /**
     * Create an instance of {@link KLAUSELType }
     * 
     */
    public KLAUSELType createKLAUSELType() {
        return new KLAUSELType();
    }

    /**
     * Create an instance of {@link LOESCHANSTOSSType }
     * 
     */
    public LOESCHANSTOSSType createLOESCHANSTOSSType() {
        return new LOESCHANSTOSSType();
    }

    /**
     * Create an instance of {@link PERSONType }
     * 
     */
    public PERSONType createPERSONType() {
        return new PERSONType();
    }

    /**
     * Create an instance of {@link VERTRAG }
     * 
     */
    public VERTRAG createVERTRAG() {
        return new VERTRAG();
    }

    /**
     * Create an instance of {@link VERTRAGType }
     * 
     */
    public VERTRAGType createVERTRAGType() {
        return new VERTRAGType();
    }

    /**
     * Create an instance of {@link ELAntragType }
     * 
     */
    public ELAntragType createELAntragType() {
        return new ELAntragType();
    }

    /**
     * Create an instance of {@link ELAnzahlType }
     * 
     */
    public ELAnzahlType createELAnzahlType() {
        return new ELAnzahlType();
    }

    /**
     * Create an instance of {@link ELBetragType }
     * 
     */
    public ELBetragType createELBetragType() {
        return new ELBetragType();
    }

    /**
     * Create an instance of {@link ELBezugsberechtigungType }
     * 
     */
    public ELBezugsberechtigungType createELBezugsberechtigungType() {
        return new ELBezugsberechtigungType();
    }

    /**
     * Create an instance of {@link ELEinstufungType }
     * 
     */
    public ELEinstufungType createELEinstufungType() {
        return new ELEinstufungType();
    }

    /**
     * Create an instance of {@link ELEntscheidungsfrageType }
     * 
     */
    public ELEntscheidungsfrageType createELEntscheidungsfrageType() {
        return new ELEntscheidungsfrageType();
    }

    /**
     * Create an instance of {@link ELIdentifizierungType }
     * 
     */
    public ELIdentifizierungType createELIdentifizierungType() {
        return new ELIdentifizierungType();
    }

    /**
     * Create an instance of {@link ELKlauselType }
     * 
     */
    public ELKlauselType createELKlauselType() {
        return new ELKlauselType();
    }

    /**
     * Create an instance of {@link ELPolizzennummerType }
     * 
     */
    public ELPolizzennummerType createELPolizzennummerType() {
        return new ELPolizzennummerType();
    }

    /**
     * Create an instance of {@link ELPraemienfreistellungType }
     * 
     */
    public ELPraemienfreistellungType createELPraemienfreistellungType() {
        return new ELPraemienfreistellungType();
    }

    /**
     * Create an instance of {@link ELPraemienkorrekturType }
     * 
     */
    public ELPraemienkorrekturType createELPraemienkorrekturType() {
        return new ELPraemienkorrekturType();
    }

    /**
     * Create an instance of {@link ELRahmenvereinbarungType }
     * 
     */
    public ELRahmenvereinbarungType createELRahmenvereinbarungType() {
        return new ELRahmenvereinbarungType();
    }

    /**
     * Create an instance of {@link ELSelbstbehalt }
     * 
     */
    public ELSelbstbehalt createELSelbstbehalt() {
        return new ELSelbstbehalt();
    }

    /**
     * Create an instance of {@link ELSelbstbehaltType }
     * 
     */
    public ELSelbstbehaltType createELSelbstbehaltType() {
        return new ELSelbstbehaltType();
    }

    /**
     * Create an instance of {@link ELTextType }
     * 
     */
    public ELTextType createELTextType() {
        return new ELTextType();
    }

    /**
     * Create an instance of {@link VERTRAGSPERSONType }
     * 
     */
    public VERTRAGSPERSONType createVERTRAGSPERSONType() {
        return new VERTRAGSPERSONType();
    }

    /**
     * Create an instance of {@link VERSOBJEKTType }
     * 
     */
    public VERSOBJEKTType createVERSOBJEKTType() {
        return new VERSOBJEKTType();
    }

    /**
     * Create an instance of {@link SPARTEType }
     * 
     */
    public SPARTEType createSPARTEType() {
        return new SPARTEType();
    }

    /**
     * Create an instance of {@link SCHADENType }
     * 
     */
    public SCHADENType createSCHADENType() {
        return new SCHADENType();
    }

    /**
     * Create an instance of {@link PROVISIONType }
     * 
     */
    public PROVISIONType createPROVISIONType() {
        return new PROVISIONType();
    }

    /**
     * Create an instance of {@link MAHNUNGType }
     * 
     */
    public MAHNUNGType createMAHNUNGType() {
        return new MAHNUNGType();
    }

    /**
     * Create an instance of {@link VERTRAGSFONDSType }
     * 
     */
    public VERTRAGSFONDSType createVERTRAGSFONDSType() {
        return new VERTRAGSFONDSType();
    }

    /**
     * Create an instance of {@link NATUERLICHEPERSONType }
     * 
     */
    public NATUERLICHEPERSONType createNATUERLICHEPERSONType() {
        return new NATUERLICHEPERSONType();
    }

    /**
     * Create an instance of {@link SONSTIGEPERSONType }
     * 
     */
    public SONSTIGEPERSONType createSONSTIGEPERSONType() {
        return new SONSTIGEPERSONType();
    }

    /**
     * Create an instance of {@link VERSPERSONType }
     * 
     */
    public VERSPERSONType createVERSPERSONType() {
        return new VERSPERSONType();
    }

    /**
     * Create an instance of {@link VERSKFZ }
     * 
     */
    public VERSKFZ createVERSKFZ() {
        return new VERSKFZ();
    }

    /**
     * Create an instance of {@link VERSKFZType }
     * 
     */
    public VERSKFZType createVERSKFZType() {
        return new VERSKFZType();
    }

    /**
     * Create an instance of {@link VERSSACHEType }
     * 
     */
    public VERSSACHEType createVERSSACHEType() {
        return new VERSSACHEType();
    }

    /**
     * Create an instance of {@link RISIKOType }
     * 
     */
    public RISIKOType createRISIKOType() {
        return new RISIKOType();
    }

    /**
     * Create an instance of {@link FONDSType }
     * 
     */
    public FONDSType createFONDSType() {
        return new FONDSType();
    }

    /**
     * Create an instance of {@link PORTFOLIO }
     * 
     */
    public PORTFOLIO createPORTFOLIO() {
        return new PORTFOLIO();
    }

    /**
     * Create an instance of {@link PORTFOLIOTYPE }
     * 
     */
    public PORTFOLIOTYPE createPORTFOLIOTYPE() {
        return new PORTFOLIOTYPE();
    }

    /**
     * Create an instance of {@link SCHADENBETEILIGTERType }
     * 
     */
    public SCHADENBETEILIGTERType createSCHADENBETEILIGTERType() {
        return new SCHADENBETEILIGTERType();
    }

    /**
     * Create an instance of {@link GESCHAEDIGTESOBJEKTType }
     * 
     */
    public GESCHAEDIGTESOBJEKTType createGESCHAEDIGTESOBJEKTType() {
        return new GESCHAEDIGTESOBJEKTType();
    }

    /**
     * Create an instance of {@link ZAHLUNGType }
     * 
     */
    public ZAHLUNGType createZAHLUNGType() {
        return new ZAHLUNGType();
    }

    /**
     * Create an instance of {@link ELGewinnbeteiligungType }
     * 
     */
    public ELGewinnbeteiligungType createELGewinnbeteiligungType() {
        return new ELGewinnbeteiligungType();
    }

    /**
     * Create an instance of {@link ELGrenzwertType }
     * 
     */
    public ELGrenzwertType createELGrenzwertType() {
        return new ELGrenzwertType();
    }

    /**
     * Create an instance of {@link ELIndexType }
     * 
     */
    public ELIndexType createELIndexType() {
        return new ELIndexType();
    }

    /**
     * Create an instance of {@link ELLegitimationType }
     * 
     */
    public ELLegitimationType createELLegitimationType() {
        return new ELLegitimationType();
    }

    /**
     * Create an instance of {@link ELObjektType }
     * 
     */
    public ELObjektType createELObjektType() {
        return new ELObjektType();
    }

    /**
     * Create an instance of {@link ELObjektdatenType }
     * 
     */
    public ELObjektdatenType createELObjektdatenType() {
        return new ELObjektdatenType();
    }

    /**
     * Create an instance of {@link ELFlaecheType }
     * 
     */
    public ELFlaecheType createELFlaecheType() {
        return new ELFlaecheType();
    }

    /**
     * Create an instance of {@link ELRenteType }
     * 
     */
    public ELRenteType createELRenteType() {
        return new ELRenteType();
    }

    /**
     * Create an instance of {@link ELSteuerType }
     * 
     */
    public ELSteuerType createELSteuerType() {
        return new ELSteuerType();
    }

    /**
     * Create an instance of {@link ELVersicherungssummeType }
     * 
     */
    public ELVersicherungssummeType createELVersicherungssummeType() {
        return new ELVersicherungssummeType();
    }

    /**
     * Create an instance of {@link ELZeitraumType }
     * 
     */
    public ELZeitraumType createELZeitraumType() {
        return new ELZeitraumType();
    }

    /**
     * Create an instance of {@link ADRESSEType }
     * 
     */
    public ADRESSEType createADRESSEType() {
        return new ADRESSEType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ELKommunikationType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds20", name = "EL-Kommunikation")
    public JAXBElement<ELKommunikationType> createELKommunikation(ELKommunikationType value) {
        return new JAXBElement<ELKommunikationType>(_ELKommunikation_QNAME, ELKommunikationType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link KLAUSELType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds20", name = "KLAUSEL")
    public JAXBElement<KLAUSELType> createKLAUSEL(KLAUSELType value) {
        return new JAXBElement<KLAUSELType>(_KLAUSEL_QNAME, KLAUSELType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LOESCHANSTOSSType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds20", name = "LOESCHANSTOSS")
    public JAXBElement<LOESCHANSTOSSType> createLOESCHANSTOSS(LOESCHANSTOSSType value) {
        return new JAXBElement<LOESCHANSTOSSType>(_LOESCHANSTOSS_QNAME, LOESCHANSTOSSType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PERSONType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds20", name = "PERSON")
    public JAXBElement<PERSONType> createPERSON(PERSONType value) {
        return new JAXBElement<PERSONType>(_PERSON_QNAME, PERSONType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ELAntragType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds20", name = "EL-Antrag")
    public JAXBElement<ELAntragType> createELAntrag(ELAntragType value) {
        return new JAXBElement<ELAntragType>(_ELAntrag_QNAME, ELAntragType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ELAnzahlType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds20", name = "EL-Anzahl")
    public JAXBElement<ELAnzahlType> createELAnzahl(ELAnzahlType value) {
        return new JAXBElement<ELAnzahlType>(_ELAnzahl_QNAME, ELAnzahlType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ELBetragType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds20", name = "EL-Betrag")
    public JAXBElement<ELBetragType> createELBetrag(ELBetragType value) {
        return new JAXBElement<ELBetragType>(_ELBetrag_QNAME, ELBetragType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ELBezugsberechtigungType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds20", name = "EL-Bezugsberechtigung")
    public JAXBElement<ELBezugsberechtigungType> createELBezugsberechtigung(ELBezugsberechtigungType value) {
        return new JAXBElement<ELBezugsberechtigungType>(_ELBezugsberechtigung_QNAME, ELBezugsberechtigungType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ELEinstufungType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds20", name = "EL-Einstufung")
    public JAXBElement<ELEinstufungType> createELEinstufung(ELEinstufungType value) {
        return new JAXBElement<ELEinstufungType>(_ELEinstufung_QNAME, ELEinstufungType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ELEntscheidungsfrageType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds20", name = "EL-Entscheidungsfrage")
    public JAXBElement<ELEntscheidungsfrageType> createELEntscheidungsfrage(ELEntscheidungsfrageType value) {
        return new JAXBElement<ELEntscheidungsfrageType>(_ELEntscheidungsfrage_QNAME, ELEntscheidungsfrageType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ELIdentifizierungType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds20", name = "EL-Identifizierung")
    public JAXBElement<ELIdentifizierungType> createELIdentifizierung(ELIdentifizierungType value) {
        return new JAXBElement<ELIdentifizierungType>(_ELIdentifizierung_QNAME, ELIdentifizierungType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ELKlauselType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds20", name = "EL-Klausel")
    public JAXBElement<ELKlauselType> createELKlausel(ELKlauselType value) {
        return new JAXBElement<ELKlauselType>(_ELKlausel_QNAME, ELKlauselType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ELPolizzennummerType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds20", name = "EL-Polizzennummer")
    public JAXBElement<ELPolizzennummerType> createELPolizzennummer(ELPolizzennummerType value) {
        return new JAXBElement<ELPolizzennummerType>(_ELPolizzennummer_QNAME, ELPolizzennummerType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ELPraemienfreistellungType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds20", name = "EL-Praemienfreistellung")
    public JAXBElement<ELPraemienfreistellungType> createELPraemienfreistellung(ELPraemienfreistellungType value) {
        return new JAXBElement<ELPraemienfreistellungType>(_ELPraemienfreistellung_QNAME, ELPraemienfreistellungType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ELPraemienkorrekturType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds20", name = "EL-Praemienkorrektur")
    public JAXBElement<ELPraemienkorrekturType> createELPraemienkorrektur(ELPraemienkorrekturType value) {
        return new JAXBElement<ELPraemienkorrekturType>(_ELPraemienkorrektur_QNAME, ELPraemienkorrekturType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ELRahmenvereinbarungType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds20", name = "EL-Rahmenvereinbarung")
    public JAXBElement<ELRahmenvereinbarungType> createELRahmenvereinbarung(ELRahmenvereinbarungType value) {
        return new JAXBElement<ELRahmenvereinbarungType>(_ELRahmenvereinbarung_QNAME, ELRahmenvereinbarungType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ELTextType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds20", name = "EL-Text")
    public JAXBElement<ELTextType> createELText(ELTextType value) {
        return new JAXBElement<ELTextType>(_ELText_QNAME, ELTextType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VERTRAGSPERSONType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds20", name = "VERTRAGSPERSON")
    public JAXBElement<VERTRAGSPERSONType> createVERTRAGSPERSON(VERTRAGSPERSONType value) {
        return new JAXBElement<VERTRAGSPERSONType>(_VERTRAGSPERSON_QNAME, VERTRAGSPERSONType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VERSOBJEKTType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds20", name = "VERS_OBJEKT")
    public JAXBElement<VERSOBJEKTType> createVERSOBJEKT(VERSOBJEKTType value) {
        return new JAXBElement<VERSOBJEKTType>(_VERSOBJEKT_QNAME, VERSOBJEKTType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SPARTEType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds20", name = "SPARTE")
    public JAXBElement<SPARTEType> createSPARTE(SPARTEType value) {
        return new JAXBElement<SPARTEType>(_SPARTE_QNAME, SPARTEType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SCHADENType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds20", name = "SCHADEN")
    public JAXBElement<SCHADENType> createSCHADEN(SCHADENType value) {
        return new JAXBElement<SCHADENType>(_SCHADEN_QNAME, SCHADENType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PROVISIONType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds20", name = "PROVISION")
    public JAXBElement<PROVISIONType> createPROVISION(PROVISIONType value) {
        return new JAXBElement<PROVISIONType>(_PROVISION_QNAME, PROVISIONType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MAHNUNGType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds20", name = "MAHNUNG")
    public JAXBElement<MAHNUNGType> createMAHNUNG(MAHNUNGType value) {
        return new JAXBElement<MAHNUNGType>(_MAHNUNG_QNAME, MAHNUNGType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VERTRAGSFONDSType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds20", name = "VERTRAGSFONDS")
    public JAXBElement<VERTRAGSFONDSType> createVERTRAGSFONDS(VERTRAGSFONDSType value) {
        return new JAXBElement<VERTRAGSFONDSType>(_VERTRAGSFONDS_QNAME, VERTRAGSFONDSType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NATUERLICHEPERSONType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds20", name = "NATUERLICHE_PERSON")
    public JAXBElement<NATUERLICHEPERSONType> createNATUERLICHEPERSON(NATUERLICHEPERSONType value) {
        return new JAXBElement<NATUERLICHEPERSONType>(_NATUERLICHEPERSON_QNAME, NATUERLICHEPERSONType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SONSTIGEPERSONType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds20", name = "SONSTIGE_PERSON")
    public JAXBElement<SONSTIGEPERSONType> createSONSTIGEPERSON(SONSTIGEPERSONType value) {
        return new JAXBElement<SONSTIGEPERSONType>(_SONSTIGEPERSON_QNAME, SONSTIGEPERSONType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VERSPERSONType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds20", name = "VERS_PERSON")
    public JAXBElement<VERSPERSONType> createVERSPERSON(VERSPERSONType value) {
        return new JAXBElement<VERSPERSONType>(_VERSPERSON_QNAME, VERSPERSONType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VERSSACHEType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds20", name = "VERS_SACHE")
    public JAXBElement<VERSSACHEType> createVERSSACHE(VERSSACHEType value) {
        return new JAXBElement<VERSSACHEType>(_VERSSACHE_QNAME, VERSSACHEType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RISIKOType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds20", name = "RISIKO")
    public JAXBElement<RISIKOType> createRISIKO(RISIKOType value) {
        return new JAXBElement<RISIKOType>(_RISIKO_QNAME, RISIKOType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FONDSType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds20", name = "FONDS")
    public JAXBElement<FONDSType> createFONDS(FONDSType value) {
        return new JAXBElement<FONDSType>(_FONDS_QNAME, FONDSType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SCHADENBETEILIGTERType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds20", name = "SCHADEN_BETEILIGTER")
    public JAXBElement<SCHADENBETEILIGTERType> createSCHADENBETEILIGTER(SCHADENBETEILIGTERType value) {
        return new JAXBElement<SCHADENBETEILIGTERType>(_SCHADENBETEILIGTER_QNAME, SCHADENBETEILIGTERType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GESCHAEDIGTESOBJEKTType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds20", name = "GESCHAEDIGTES_OBJEKT")
    public JAXBElement<GESCHAEDIGTESOBJEKTType> createGESCHAEDIGTESOBJEKT(GESCHAEDIGTESOBJEKTType value) {
        return new JAXBElement<GESCHAEDIGTESOBJEKTType>(_GESCHAEDIGTESOBJEKT_QNAME, GESCHAEDIGTESOBJEKTType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ZAHLUNGType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds20", name = "ZAHLUNG")
    public JAXBElement<ZAHLUNGType> createZAHLUNG(ZAHLUNGType value) {
        return new JAXBElement<ZAHLUNGType>(_ZAHLUNG_QNAME, ZAHLUNGType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ELGewinnbeteiligungType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds20", name = "EL-Gewinnbeteiligung")
    public JAXBElement<ELGewinnbeteiligungType> createELGewinnbeteiligung(ELGewinnbeteiligungType value) {
        return new JAXBElement<ELGewinnbeteiligungType>(_ELGewinnbeteiligung_QNAME, ELGewinnbeteiligungType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ELGrenzwertType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds20", name = "EL-Grenzwert")
    public JAXBElement<ELGrenzwertType> createELGrenzwert(ELGrenzwertType value) {
        return new JAXBElement<ELGrenzwertType>(_ELGrenzwert_QNAME, ELGrenzwertType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ELIndexType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds20", name = "EL-Index")
    public JAXBElement<ELIndexType> createELIndex(ELIndexType value) {
        return new JAXBElement<ELIndexType>(_ELIndex_QNAME, ELIndexType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ELLegitimationType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds20", name = "EL-Legitimation")
    public JAXBElement<ELLegitimationType> createELLegitimation(ELLegitimationType value) {
        return new JAXBElement<ELLegitimationType>(_ELLegitimation_QNAME, ELLegitimationType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ELObjektType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds20", name = "EL-Objekt")
    public JAXBElement<ELObjektType> createELObjekt(ELObjektType value) {
        return new JAXBElement<ELObjektType>(_ELObjekt_QNAME, ELObjektType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ELObjektdatenType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds20", name = "EL-Objektdaten")
    public JAXBElement<ELObjektdatenType> createELObjektdaten(ELObjektdatenType value) {
        return new JAXBElement<ELObjektdatenType>(_ELObjektdaten_QNAME, ELObjektdatenType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ELFlaecheType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds20", name = "EL-Flaeche")
    public JAXBElement<ELFlaecheType> createELFlaeche(ELFlaecheType value) {
        return new JAXBElement<ELFlaecheType>(_ELFlaeche_QNAME, ELFlaecheType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ELRenteType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds20", name = "EL-Rente")
    public JAXBElement<ELRenteType> createELRente(ELRenteType value) {
        return new JAXBElement<ELRenteType>(_ELRente_QNAME, ELRenteType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ELSteuerType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds20", name = "EL-Steuer")
    public JAXBElement<ELSteuerType> createELSteuer(ELSteuerType value) {
        return new JAXBElement<ELSteuerType>(_ELSteuer_QNAME, ELSteuerType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ELVersicherungssummeType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds20", name = "EL-Versicherungssumme")
    public JAXBElement<ELVersicherungssummeType> createELVersicherungssumme(ELVersicherungssummeType value) {
        return new JAXBElement<ELVersicherungssummeType>(_ELVersicherungssumme_QNAME, ELVersicherungssummeType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ELZeitraumType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds20", name = "EL-Zeitraum")
    public JAXBElement<ELZeitraumType> createELZeitraum(ELZeitraumType value) {
        return new JAXBElement<ELZeitraumType>(_ELZeitraum_QNAME, ELZeitraumType.class, null, value);
    }

}
