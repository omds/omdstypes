
package at.vvo.omds.types.omds2Types.v2_11;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für PaketInhCd_Type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="PaketInhCd_Type"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="AI"/&gt;
 *     &lt;enumeration value="VF"/&gt;
 *     &lt;enumeration value="VI"/&gt;
 *     &lt;enumeration value="VK"/&gt;
 *     &lt;enumeration value="VM"/&gt;
 *     &lt;enumeration value="VP"/&gt;
 *     &lt;enumeration value="VS"/&gt;
 *     &lt;enumeration value="VV"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "PaketInhCd_Type")
@XmlEnum
public enum PaketInhCdType {


    /**
     * Allgem. Initialbestand (generelle Schlüssel)
     * 
     */
    AI,

    /**
     * VU Fondsbestand
     * 
     */
    VF,

    /**
     * VU Initialbestand (VU Schlüssel)
     * 
     */
    VI,

    /**
     * VU Mahn/Klagebestand
     * 
     */
    VK,

    /**
     * VU Mischbestand
     * 
     */
    VM,

    /**
     * VU Provisionen
     * 
     */
    VP,

    /**
     * VU Schadenbestand
     * 
     */
    VS,

    /**
     * VU Vertragsbestand
     * 
     */
    VV;

    public String value() {
        return name();
    }

    public static PaketInhCdType fromValue(String v) {
        return valueOf(v);
    }

}
