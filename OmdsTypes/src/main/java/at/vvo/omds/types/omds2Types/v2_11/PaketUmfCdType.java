
package at.vvo.omds.types.omds2Types.v2_11;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für PaketUmfCd_Type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="PaketUmfCd_Type"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="D"/&gt;
 *     &lt;enumeration value="G"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "PaketUmfCd_Type")
@XmlEnum
public enum PaketUmfCdType {


    /**
     * Differenz
     * 
     */
    D,

    /**
     * gesamt
     * 
     */
    G;

    public String value() {
        return name();
    }

    public static PaketUmfCdType fromValue(String v) {
        return valueOf(v);
    }

}
