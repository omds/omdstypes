
package at.vvo.omds.types.omds2Types.v2_11;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für PersArtCd_Type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="PersArtCd_Type"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="N"/&gt;
 *     &lt;enumeration value="S"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "PersArtCd_Type")
@XmlEnum
public enum PersArtCdType {


    /**
     * natürliche Person
     * 
     */
    N,

    /**
     * sonstige Person
     * 
     */
    S;

    public String value() {
        return name();
    }

    public static PersArtCdType fromValue(String v) {
        return valueOf(v);
    }

}
