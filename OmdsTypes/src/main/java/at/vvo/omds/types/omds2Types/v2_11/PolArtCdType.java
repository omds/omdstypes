
package at.vvo.omds.types.omds2Types.v2_11;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für PolArtCd_Type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="PolArtCd_Type"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="E"/&gt;
 *     &lt;enumeration value="N"/&gt;
 *     &lt;enumeration value="V"/&gt;
 *     &lt;enumeration value="X"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "PolArtCd_Type")
@XmlEnum
public enum PolArtCdType {


    /**
     * Ersatzpolizze bei Ersatz/Konv.
     * 
     */
    E,

    /**
     * Nachversicherung
     * 
     */
    N,

    /**
     * Vorpolizze bei Ersatz/Konv.
     * 
     */
    V,

    /**
     * Vorpolizze bei Migration
     * 
     */
    X;

    public String value() {
        return name();
    }

    public static PolArtCdType fromValue(String v) {
        return valueOf(v);
    }

}
