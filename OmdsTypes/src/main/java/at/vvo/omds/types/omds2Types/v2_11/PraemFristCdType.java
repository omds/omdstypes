
package at.vvo.omds.types.omds2Types.v2_11;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für PraemFristCd_Type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="PraemFristCd_Type"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="E"/&gt;
 *     &lt;enumeration value="J"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "PraemFristCd_Type")
@XmlEnum
public enum PraemFristCdType {


    /**
     * Einmal
     * 
     */
    E,

    /**
     * Jahr
     * 
     */
    J;

    public String value() {
        return name();
    }

    public static PraemFristCdType fromValue(String v) {
        return valueOf(v);
    }

}
