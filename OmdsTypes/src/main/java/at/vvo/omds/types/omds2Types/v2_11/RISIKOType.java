
package at.vvo.omds.types.omds2Types.v2_11;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für RISIKO_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="RISIKO_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;element ref="{urn:omds20}EL-Anzahl"/&gt;
 *           &lt;element ref="{urn:omds20}EL-Betrag"/&gt;
 *           &lt;element ref="{urn:omds20}EL-Bezugsberechtigung"/&gt;
 *           &lt;element ref="{urn:omds20}EL-Einstufung"/&gt;
 *           &lt;element ref="{urn:omds20}EL-Entscheidungsfrage"/&gt;
 *           &lt;element ref="{urn:omds20}EL-Gewinnbeteiligung"/&gt;
 *           &lt;element ref="{urn:omds20}EL-Grenzwert"/&gt;
 *           &lt;element ref="{urn:omds20}EL-Index"/&gt;
 *           &lt;element ref="{urn:omds20}EL-Identifizierung"/&gt;
 *           &lt;element ref="{urn:omds20}EL-Klausel"/&gt;
 *           &lt;element ref="{urn:omds20}EL-Objekt"/&gt;
 *           &lt;element ref="{urn:omds20}EL-Praemienkorrektur"/&gt;
 *           &lt;element ref="{urn:omds20}EL-Rente"/&gt;
 *           &lt;element ref="{urn:omds20}EL-Selbstbehalt"/&gt;
 *           &lt;element ref="{urn:omds20}EL-Steuer"/&gt;
 *           &lt;element ref="{urn:omds20}EL-Text"/&gt;
 *           &lt;element ref="{urn:omds20}EL-Versicherungssumme"/&gt;
 *           &lt;element ref="{urn:omds20}EL-Zeitraum"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="RisikoLfnr" use="required" type="{http://www.w3.org/2001/XMLSchema}unsignedShort" /&gt;
 *       &lt;attribute name="RisikoArtCd" type="{urn:omds20}RisikoArtCd_Type" /&gt;
 *       &lt;attribute name="RisikoBez" use="required"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="80"/&gt;
 *             &lt;minLength value="1"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="PraemFristCd" type="{urn:omds20}PraemFristCd_Type" /&gt;
 *       &lt;attribute name="PraemieNto" type="{urn:omds20}decimal" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RISIKO_Type", propOrder = {
    "elAnzahlOrELBetragOrELBezugsberechtigung"
})
public class RISIKOType {

    @XmlElements({
        @XmlElement(name = "EL-Anzahl", type = ELAnzahlType.class),
        @XmlElement(name = "EL-Betrag", type = ELBetragType.class),
        @XmlElement(name = "EL-Bezugsberechtigung", type = ELBezugsberechtigungType.class),
        @XmlElement(name = "EL-Einstufung", type = ELEinstufungType.class),
        @XmlElement(name = "EL-Entscheidungsfrage", type = ELEntscheidungsfrageType.class),
        @XmlElement(name = "EL-Gewinnbeteiligung", type = ELGewinnbeteiligungType.class),
        @XmlElement(name = "EL-Grenzwert", type = ELGrenzwertType.class),
        @XmlElement(name = "EL-Index", type = ELIndexType.class),
        @XmlElement(name = "EL-Identifizierung", type = ELIdentifizierungType.class),
        @XmlElement(name = "EL-Klausel", type = ELKlauselType.class),
        @XmlElement(name = "EL-Objekt", type = ELObjektType.class),
        @XmlElement(name = "EL-Praemienkorrektur", type = ELPraemienkorrekturType.class),
        @XmlElement(name = "EL-Rente", type = ELRenteType.class),
        @XmlElement(name = "EL-Selbstbehalt", type = ELSelbstbehalt.class),
        @XmlElement(name = "EL-Steuer", type = ELSteuerType.class),
        @XmlElement(name = "EL-Text", type = ELTextType.class),
        @XmlElement(name = "EL-Versicherungssumme", type = ELVersicherungssummeType.class),
        @XmlElement(name = "EL-Zeitraum", type = ELZeitraumType.class)
    })
    protected List<Object> elAnzahlOrELBetragOrELBezugsberechtigung;
    @XmlAttribute(name = "RisikoLfnr", required = true)
    @XmlSchemaType(name = "unsignedShort")
    protected int risikoLfnr;
    @XmlAttribute(name = "RisikoArtCd")
    protected RisikoArtCdType risikoArtCd;
    @XmlAttribute(name = "RisikoBez", required = true)
    protected String risikoBez;
    @XmlAttribute(name = "PraemFristCd")
    protected PraemFristCdType praemFristCd;
    @XmlAttribute(name = "PraemieNto")
    protected BigDecimal praemieNto;

    /**
     * Gets the value of the elAnzahlOrELBetragOrELBezugsberechtigung property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the elAnzahlOrELBetragOrELBezugsberechtigung property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getELAnzahlOrELBetragOrELBezugsberechtigung().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ELAnzahlType }
     * {@link ELBetragType }
     * {@link ELBezugsberechtigungType }
     * {@link ELEinstufungType }
     * {@link ELEntscheidungsfrageType }
     * {@link ELGewinnbeteiligungType }
     * {@link ELGrenzwertType }
     * {@link ELIndexType }
     * {@link ELIdentifizierungType }
     * {@link ELKlauselType }
     * {@link ELObjektType }
     * {@link ELPraemienkorrekturType }
     * {@link ELRenteType }
     * {@link ELSelbstbehalt }
     * {@link ELSteuerType }
     * {@link ELTextType }
     * {@link ELVersicherungssummeType }
     * {@link ELZeitraumType }
     * 
     * 
     */
    public List<Object> getELAnzahlOrELBetragOrELBezugsberechtigung() {
        if (elAnzahlOrELBetragOrELBezugsberechtigung == null) {
            elAnzahlOrELBetragOrELBezugsberechtigung = new ArrayList<Object>();
        }
        return this.elAnzahlOrELBetragOrELBezugsberechtigung;
    }

    /**
     * Ruft den Wert der risikoLfnr-Eigenschaft ab.
     * 
     */
    public int getRisikoLfnr() {
        return risikoLfnr;
    }

    /**
     * Legt den Wert der risikoLfnr-Eigenschaft fest.
     * 
     */
    public void setRisikoLfnr(int value) {
        this.risikoLfnr = value;
    }

    /**
     * Ruft den Wert der risikoArtCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link RisikoArtCdType }
     *     
     */
    public RisikoArtCdType getRisikoArtCd() {
        return risikoArtCd;
    }

    /**
     * Legt den Wert der risikoArtCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link RisikoArtCdType }
     *     
     */
    public void setRisikoArtCd(RisikoArtCdType value) {
        this.risikoArtCd = value;
    }

    /**
     * Ruft den Wert der risikoBez-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRisikoBez() {
        return risikoBez;
    }

    /**
     * Legt den Wert der risikoBez-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRisikoBez(String value) {
        this.risikoBez = value;
    }

    /**
     * Ruft den Wert der praemFristCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PraemFristCdType }
     *     
     */
    public PraemFristCdType getPraemFristCd() {
        return praemFristCd;
    }

    /**
     * Legt den Wert der praemFristCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PraemFristCdType }
     *     
     */
    public void setPraemFristCd(PraemFristCdType value) {
        this.praemFristCd = value;
    }

    /**
     * Ruft den Wert der praemieNto-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPraemieNto() {
        return praemieNto;
    }

    /**
     * Legt den Wert der praemieNto-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPraemieNto(BigDecimal value) {
        this.praemieNto = value;
    }

}
