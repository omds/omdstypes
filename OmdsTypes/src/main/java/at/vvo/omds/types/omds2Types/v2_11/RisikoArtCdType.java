
package at.vvo.omds.types.omds2Types.v2_11;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für RisikoArtCd_Type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="RisikoArtCd_Type"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="AH1"/&gt;
 *     &lt;enumeration value="AK1"/&gt;
 *     &lt;enumeration value="AK2"/&gt;
 *     &lt;enumeration value="AS1"/&gt;
 *     &lt;enumeration value="AS2"/&gt;
 *     &lt;enumeration value="AS3"/&gt;
 *     &lt;enumeration value="AS4"/&gt;
 *     &lt;enumeration value="AU1"/&gt;
 *     &lt;enumeration value="AU2"/&gt;
 *     &lt;enumeration value="AU3"/&gt;
 *     &lt;enumeration value="AU4"/&gt;
 *     &lt;enumeration value="AU5"/&gt;
 *     &lt;enumeration value="AV1"/&gt;
 *     &lt;enumeration value="BA1"/&gt;
 *     &lt;enumeration value="BD1"/&gt;
 *     &lt;enumeration value="B01"/&gt;
 *     &lt;enumeration value="B02"/&gt;
 *     &lt;enumeration value="B03"/&gt;
 *     &lt;enumeration value="B04"/&gt;
 *     &lt;enumeration value="B05"/&gt;
 *     &lt;enumeration value="B06"/&gt;
 *     &lt;enumeration value="B11"/&gt;
 *     &lt;enumeration value="E01"/&gt;
 *     &lt;enumeration value="E02"/&gt;
 *     &lt;enumeration value="EC1"/&gt;
 *     &lt;enumeration value="F01"/&gt;
 *     &lt;enumeration value="F02"/&gt;
 *     &lt;enumeration value="F03"/&gt;
 *     &lt;enumeration value="F04"/&gt;
 *     &lt;enumeration value="F05"/&gt;
 *     &lt;enumeration value="G01"/&gt;
 *     &lt;enumeration value="G02"/&gt;
 *     &lt;enumeration value="H01"/&gt;
 *     &lt;enumeration value="H02"/&gt;
 *     &lt;enumeration value="H03"/&gt;
 *     &lt;enumeration value="H04"/&gt;
 *     &lt;enumeration value="H05"/&gt;
 *     &lt;enumeration value="H06"/&gt;
 *     &lt;enumeration value="H07"/&gt;
 *     &lt;enumeration value="H08"/&gt;
 *     &lt;enumeration value="H99"/&gt;
 *     &lt;enumeration value="HH1"/&gt;
 *     &lt;enumeration value="K01"/&gt;
 *     &lt;enumeration value="K02"/&gt;
 *     &lt;enumeration value="K03"/&gt;
 *     &lt;enumeration value="K04"/&gt;
 *     &lt;enumeration value="K09"/&gt;
 *     &lt;enumeration value="K10"/&gt;
 *     &lt;enumeration value="K99"/&gt;
 *     &lt;enumeration value="L01"/&gt;
 *     &lt;enumeration value="L02"/&gt;
 *     &lt;enumeration value="L03"/&gt;
 *     &lt;enumeration value="L04"/&gt;
 *     &lt;enumeration value="L05"/&gt;
 *     &lt;enumeration value="L06"/&gt;
 *     &lt;enumeration value="L07"/&gt;
 *     &lt;enumeration value="L08"/&gt;
 *     &lt;enumeration value="L09"/&gt;
 *     &lt;enumeration value="L10"/&gt;
 *     &lt;enumeration value="L11"/&gt;
 *     &lt;enumeration value="L99"/&gt;
 *     &lt;enumeration value="LS1"/&gt;
 *     &lt;enumeration value="LW1"/&gt;
 *     &lt;enumeration value="M01"/&gt;
 *     &lt;enumeration value="M02"/&gt;
 *     &lt;enumeration value="M03"/&gt;
 *     &lt;enumeration value="M04"/&gt;
 *     &lt;enumeration value="M05"/&gt;
 *     &lt;enumeration value="M06"/&gt;
 *     &lt;enumeration value="M07"/&gt;
 *     &lt;enumeration value="R01"/&gt;
 *     &lt;enumeration value="R02"/&gt;
 *     &lt;enumeration value="R03"/&gt;
 *     &lt;enumeration value="R04"/&gt;
 *     &lt;enumeration value="R05"/&gt;
 *     &lt;enumeration value="R06"/&gt;
 *     &lt;enumeration value="R07"/&gt;
 *     &lt;enumeration value="R08"/&gt;
 *     &lt;enumeration value="R09"/&gt;
 *     &lt;enumeration value="R10"/&gt;
 *     &lt;enumeration value="R11"/&gt;
 *     &lt;enumeration value="R12"/&gt;
 *     &lt;enumeration value="R13"/&gt;
 *     &lt;enumeration value="R14"/&gt;
 *     &lt;enumeration value="R15"/&gt;
 *     &lt;enumeration value="R16"/&gt;
 *     &lt;enumeration value="R99"/&gt;
 *     &lt;enumeration value="RE1"/&gt;
 *     &lt;enumeration value="RE2"/&gt;
 *     &lt;enumeration value="RE3"/&gt;
 *     &lt;enumeration value="RE4"/&gt;
 *     &lt;enumeration value="S01"/&gt;
 *     &lt;enumeration value="S12"/&gt;
 *     &lt;enumeration value="S13"/&gt;
 *     &lt;enumeration value="S14"/&gt;
 *     &lt;enumeration value="S15"/&gt;
 *     &lt;enumeration value="S16"/&gt;
 *     &lt;enumeration value="S17"/&gt;
 *     &lt;enumeration value="S99"/&gt;
 *     &lt;enumeration value="ST1"/&gt;
 *     &lt;enumeration value="T01"/&gt;
 *     &lt;enumeration value="T02"/&gt;
 *     &lt;enumeration value="T03"/&gt;
 *     &lt;enumeration value="T04"/&gt;
 *     &lt;enumeration value="T05"/&gt;
 *     &lt;enumeration value="T06"/&gt;
 *     &lt;enumeration value="T07"/&gt;
 *     &lt;enumeration value="T08"/&gt;
 *     &lt;enumeration value="T09"/&gt;
 *     &lt;enumeration value="T11"/&gt;
 *     &lt;enumeration value="T12"/&gt;
 *     &lt;enumeration value="T13"/&gt;
 *     &lt;enumeration value="T20"/&gt;
 *     &lt;enumeration value="T21"/&gt;
 *     &lt;enumeration value="T22"/&gt;
 *     &lt;enumeration value="T23"/&gt;
 *     &lt;enumeration value="T99"/&gt;
 *     &lt;enumeration value="TI1"/&gt;
 *     &lt;enumeration value="U01"/&gt;
 *     &lt;enumeration value="U02"/&gt;
 *     &lt;enumeration value="U03"/&gt;
 *     &lt;enumeration value="U04"/&gt;
 *     &lt;enumeration value="U05"/&gt;
 *     &lt;enumeration value="U06"/&gt;
 *     &lt;enumeration value="U09"/&gt;
 *     &lt;enumeration value="U10"/&gt;
 *     &lt;enumeration value="U11"/&gt;
 *     &lt;enumeration value="U12"/&gt;
 *     &lt;enumeration value="U13"/&gt;
 *     &lt;enumeration value="U14"/&gt;
 *     &lt;enumeration value="U15"/&gt;
 *     &lt;enumeration value="U16"/&gt;
 *     &lt;enumeration value="U17"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "RisikoArtCd_Type")
@XmlEnum
public enum RisikoArtCdType {


    /**
     * KFZ-Haftpflicht
     * 
     */
    @XmlEnumValue("AH1")
    AH_1("AH1"),

    /**
     * Voll- und Gross-Schadenkasko
     * 
     */
    @XmlEnumValue("AK1")
    AK_1("AK1"),

    /**
     * Teilkasko
     * 
     */
    @XmlEnumValue("AK2")
    AK_2("AK2"),

    /**
     * KFZ-Assistance
     * 
     */
    @XmlEnumValue("AS1")
    AS_1("AS1"),

    /**
     * Reise-Assistance
     * 
     */
    @XmlEnumValue("AS2")
    AS_2("AS2"),

    /**
     * Wohn-Assistance
     * 
     */
    @XmlEnumValue("AS3")
    AS_3("AS3"),

    /**
     * Unfall-Assistance
     * 
     */
    @XmlEnumValue("AS4")
    AS_4("AS4"),

    /**
     * Insassen-Unfall
     * 
     */
    @XmlEnumValue("AU1")
    AU_1("AU1"),

    /**
     * Insassen-Unfall Todesfall
     * 
     */
    @XmlEnumValue("AU2")
    AU_2("AU2"),

    /**
     * Insassen-Unfall Invalidität
     * 
     */
    @XmlEnumValue("AU3")
    AU_3("AU3"),

    /**
     * Insassen-Unfall Taggeld
     * 
     */
    @XmlEnumValue("AU4")
    AU_4("AU4"),

    /**
     * Insassen-Unfall Heilkosten
     * 
     */
    @XmlEnumValue("AU5")
    AU_5("AU5"),

    /**
     * Abfertigungs-Versicherung
     * 
     */
    @XmlEnumValue("AV1")
    AV_1("AV1"),

    /**
     * Bausparen Ansparen
     * 
     */
    @XmlEnumValue("BA1")
    BA_1("BA1"),

    /**
     * Bausparen Darlehen
     * 
     */
    @XmlEnumValue("BD1")
    BD_1("BD1"),

    /**
     * Betr.Unterbrechung-Feuer-Zivil
     * 
     */
    @XmlEnumValue("B01")
    B_01("B01"),

    /**
     * Betr.Unterbrechung Feuer-Industrie
     * 
     */
    @XmlEnumValue("B02")
    B_02("B02"),

    /**
     * Betr.Unterbrechung-EC
     * 
     */
    @XmlEnumValue("B03")
    B_03("B03"),

    /**
     * Betr.Unterbrechung-Maschinenbruch
     * 
     */
    @XmlEnumValue("B04")
    B_04("B04"),

    /**
     * Betr.Unterbrechung-Elementarschaden
     * 
     */
    @XmlEnumValue("B05")
    B_05("B05"),

    /**
     * Betr.Unterbrechung f.freiber. Tätige
     * 
     */
    @XmlEnumValue("B06")
    B_06("B06"),

    /**
     * Veranstaltungen
     * 
     */
    @XmlEnumValue("B11")
    B_11("B11"),

    /**
     * Einbruch-Diebstahl
     * 
     */
    @XmlEnumValue("E01")
    E_01("E01"),

    /**
     * Kassenboten
     * 
     */
    @XmlEnumValue("E02")
    E_02("E02"),

    /**
     * Extended Coverage
     * 
     */
    @XmlEnumValue("EC1")
    EC_1("EC1"),

    /**
     * Feuer-Zivil
     * 
     */
    @XmlEnumValue("F01")
    F_01("F01"),

    /**
     * Feuer-Landwirtschaft
     * 
     */
    @XmlEnumValue("F02")
    F_02("F02"),

    /**
     * Feuer-Industrie
     * 
     */
    @XmlEnumValue("F03")
    F_03("F03"),

    /**
     * Feuer-EC
     * 
     */
    @XmlEnumValue("F04")
    F_04("F04"),

    /**
     * Waldbrand
     * 
     */
    @XmlEnumValue("F05")
    F_05("F05"),

    /**
     * Glasbruch privat
     * 
     */
    @XmlEnumValue("G01")
    G_01("G01"),

    /**
     * Glasbruch Geschäft
     * 
     */
    @XmlEnumValue("G02")
    G_02("G02"),

    /**
     * Allg.Haftpflicht
     * 
     */
    @XmlEnumValue("H01")
    H_01("H01"),

    /**
     * Sonder-Haftpflicht
     * 
     */
    @XmlEnumValue("H02")
    H_02("H02"),

    /**
     * Flug-Haftpflicht
     * 
     */
    @XmlEnumValue("H03")
    H_03("H03"),

    /**
     * Vermögensschaden
     * 
     */
    @XmlEnumValue("H04")
    H_04("H04"),

    /**
     * Atom-Haftpflicht
     * 
     */
    @XmlEnumValue("H05")
    H_05("H05"),

    /**
     * Verkehrs-Haftpflicht
     * 
     */
    @XmlEnumValue("H06")
    H_06("H06"),

    /**
     * Bauherrn-Haftpflicht
     * 
     */
    @XmlEnumValue("H07")
    H_07("H07"),

    /**
     * Boots-Haftpflicht
     * 
     */
    @XmlEnumValue("H08")
    H_08("H08"),

    /**
     * Sonstige Haftpflicht
     * 
     */
    @XmlEnumValue("H99")
    H_99("H99"),

    /**
     * Haushalt
     * 
     */
    @XmlEnumValue("HH1")
    HH_1("HH1"),

    /**
     * Spital
     * 
     */
    @XmlEnumValue("K01")
    K_01("K01"),

    /**
     * Ambulanz
     * 
     */
    @XmlEnumValue("K02")
    K_02("K02"),

    /**
     * Heilmittel
     * 
     */
    @XmlEnumValue("K03")
    K_03("K03"),

    /**
     * Taggeld
     * 
     */
    @XmlEnumValue("K04")
    K_04("K04"),

    /**
     * Reise-KV
     * 
     */
    @XmlEnumValue("K09")
    K_09("K09"),

    /**
     * Pflegegeld
     * 
     */
    @XmlEnumValue("K10")
    K_10("K10"),

    /**
     * KV sonstige
     * 
     */
    @XmlEnumValue("K99")
    K_99("K99"),

    /**
     * Leben Kapital
     * 
     */
    @XmlEnumValue("L01")
    L_01("L01"),

    /**
     * Leben Risiko
     * 
     */
    @XmlEnumValue("L02")
    L_02("L02"),

    /**
     * Leben Rente
     * 
     */
    @XmlEnumValue("L03")
    L_03("L03"),

    /**
     * Leben Dread Disease
     * 
     */
    @XmlEnumValue("L04")
    L_04("L04"),

    /**
     * Leben veranlagungsorientiert
     * 
     */
    @XmlEnumValue("L05")
    L_05("L05"),

    /**
     * Leben Kreditrestschuld
     * 
     */
    @XmlEnumValue("L06")
    L_06("L06"),

    /**
     * Leben prämiengefördert
     * 
     */
    @XmlEnumValue("L07")
    L_07("L07"),

    /**
     * Leben fondgebunden
     * 
     */
    @XmlEnumValue("L08")
    L_08("L08"),

    /**
     * Berufsunfähigkeit
     * 
     */
    @XmlEnumValue("L09")
    L_09("L09"),

    /**
     * Erwerbsunfähigkeit
     * 
     */
    @XmlEnumValue("L10")
    L_10("L10"),

    /**
     * Pflegegeld
     * 
     */
    @XmlEnumValue("L11")
    L_11("L11"),

    /**
     * Leben sonstige
     * 
     */
    @XmlEnumValue("L99")
    L_99("L99"),

    /**
     * Leasing
     * 
     */
    @XmlEnumValue("LS1")
    LS_1("LS1"),

    /**
     * Leitungswasser
     * 
     */
    @XmlEnumValue("LW1")
    LW_1("LW1"),

    /**
     * Maschinenbruch
     * 
     */
    @XmlEnumValue("M01")
    M_01("M01"),

    /**
     * Maschinen-Montage
     * 
     */
    @XmlEnumValue("M02")
    M_02("M02"),

    /**
     * Maschinen-Garantie
     * 
     */
    @XmlEnumValue("M03")
    M_03("M03"),

    /**
     * Elektrogeräte
     * 
     */
    @XmlEnumValue("M04")
    M_04("M04"),

    /**
     * Computer und Unterhaltungselektronik
     * 
     */
    @XmlEnumValue("M05")
    M_05("M05"),

    /**
     * Tiefkühltruhen
     * 
     */
    @XmlEnumValue("M06")
    M_06("M06"),

    /**
     * Haustechnik
     * 
     */
    @XmlEnumValue("M07")
    M_07("M07"),

    /**
     * Privat Rechtschutz
     * 
     */
    @XmlEnumValue("R01")
    R_01("R01"),

    /**
     * Kfz-Rechtschutz
     * 
     */
    @XmlEnumValue("R02")
    R_02("R02"),

    /**
     * Firmen-Rechtschutz
     * 
     */
    @XmlEnumValue("R03")
    R_03("R03"),

    /**
     * Schadenersatz- und Strafrechtsschutz
     * 
     */
    @XmlEnumValue("R04")
    R_04("R04"),

    /**
     * Arbeitsgerichtsrechtsschutz
     * 
     */
    @XmlEnumValue("R05")
    R_05("R05"),

    /**
     * Sozialversicherungsrechtsschutz
     * 
     */
    @XmlEnumValue("R06")
    R_06("R06"),

    /**
     * Beratungsrechtsschutz
     * 
     */
    @XmlEnumValue("R07")
    R_07("R07"),

    /**
     * Allgemeiner Vertragsrechtsschutz
     * 
     */
    @XmlEnumValue("R08")
    R_08("R08"),

    /**
     * Grundstückseigentum- und Mietenrechtsschutz
     * 
     */
    @XmlEnumValue("R09")
    R_09("R09"),

    /**
     * Erb- und Familienrechtsschutz
     * 
     */
    @XmlEnumValue("R10")
    R_10("R10"),

    /**
     * Disziplinarverfahren
     * 
     */
    @XmlEnumValue("R11")
    R_11("R11"),

    /**
     * Disziplinarverfahren für angestellte Ärzte
     * 
     */
    @XmlEnumValue("R12")
    R_12("R12"),

    /**
     * Vorsatzdelikte
     * 
     */
    @XmlEnumValue("R13")
    R_13("R13"),

    /**
     * Fahrzeugrechtsschutz
     * 
     */
    @XmlEnumValue("R14")
    R_14("R14"),

    /**
     * Lenkerrechtsschutz
     * 
     */
    @XmlEnumValue("R15")
    R_15("R15"),

    /**
     * Fahrzeugvertragsrechtsschutz
     * 
     */
    @XmlEnumValue("R16")
    R_16("R16"),

    /**
     * Sonstiger Rechtsschutz
     * 
     */
    @XmlEnumValue("R99")
    R_99("R99"),

    /**
     * Reise-Storno
     * 
     */
    @XmlEnumValue("RE1")
    RE_1("RE1"),

    /**
     * Reise-Assistance
     * 
     */
    @XmlEnumValue("RE2")
    RE_2("RE2"),

    /**
     * Reise-Kranken
     * 
     */
    @XmlEnumValue("RE3")
    RE_3("RE3"),

    /**
     * Reise-Unfall
     * 
     */
    @XmlEnumValue("RE4")
    RE_4("RE4"),

    /**
     * Kühlgut
     * 
     */
    @XmlEnumValue("S01")
    S_01("S01"),

    /**
     * Lizenzverlust
     * 
     */
    @XmlEnumValue("S12")
    S_12("S12"),

    /**
     * Atom-Sach
     * 
     */
    @XmlEnumValue("S13")
    S_13("S13"),

    /**
     * Bauwesen
     * 
     */
    @XmlEnumValue("S14")
    S_14("S14"),

    /**
     * Flugkasko
     * 
     */
    @XmlEnumValue("S15")
    S_15("S15"),

    /**
     * Bootskasko
     * 
     */
    @XmlEnumValue("S16")
    S_16("S16"),

    /**
     * Grabstätten
     * 
     */
    @XmlEnumValue("S17")
    S_17("S17"),

    /**
     * sonstige SV
     * 
     */
    @XmlEnumValue("S99")
    S_99("S99"),

    /**
     * Sturm
     * 
     */
    @XmlEnumValue("ST1")
    ST_1("ST1"),

    /**
     * Land-Binnenwaren
     * 
     */
    @XmlEnumValue("T01")
    T_01("T01"),

    /**
     * See
     * 
     */
    @XmlEnumValue("T02")
    T_02("T02"),

    /**
     * Krieg
     * 
     */
    @XmlEnumValue("T03")
    T_03("T03"),

    /**
     * Lager
     * 
     */
    @XmlEnumValue("T04")
    T_04("T04"),

    /**
     * Valoren-Gewerblich
     * 
     */
    @XmlEnumValue("T05")
    T_05("T05"),

    /**
     * Valoren-Privat
     * 
     */
    @XmlEnumValue("T06")
    T_06("T06"),

    /**
     * Sportboot-Kasko
     * 
     */
    @XmlEnumValue("T07")
    T_07("T07"),

    /**
     * Musik-Instrumente
     * 
     */
    @XmlEnumValue("T08")
    T_08("T08"),

    /**
     * Kunst
     * 
     */
    @XmlEnumValue("T09")
    T_09("T09"),

    /**
     * Seekasko
     * 
     */
    @XmlEnumValue("T11")
    T_11("T11"),

    /**
     * Flusskasko
     * 
     */
    @XmlEnumValue("T12")
    T_12("T12"),

    /**
     * Landkasko
     * 
     */
    @XmlEnumValue("T13")
    T_13("T13"),

    /**
     * Reisegepäck
     * 
     */
    @XmlEnumValue("T20")
    T_20("T20"),

    /**
     * Fotoapparate
     * 
     */
    @XmlEnumValue("T21")
    T_21("T21"),

    /**
     * Film/Sach
     * 
     */
    @XmlEnumValue("T22")
    T_22("T22"),

    /**
     * Film/Ausfall
     * 
     */
    @XmlEnumValue("T23")
    T_23("T23"),

    /**
     * Sonstige Transport
     * 
     */
    @XmlEnumValue("T99")
    T_99("T99"),

    /**
     * Tier
     * 
     */
    @XmlEnumValue("TI1")
    TI_1("TI1"),

    /**
     * Allg.Unfall
     * 
     */
    @XmlEnumValue("U01")
    U_01("U01"),

    /**
     * Kollektiv Unfall
     * 
     */
    @XmlEnumValue("U02")
    U_02("U02"),

    /**
     * Schülerunfall
     * 
     */
    @XmlEnumValue("U03")
    U_03("U03"),

    /**
     * Volksunfall
     * 
     */
    @XmlEnumValue("U04")
    U_04("U04"),

    /**
     * Flug-Unfall
     * 
     */
    @XmlEnumValue("U05")
    U_05("U05"),

    /**
     * Boots-Unfall
     * 
     */
    @XmlEnumValue("U06")
    U_06("U06"),

    /**
     * Besucher-Unfall
     * 
     */
    @XmlEnumValue("U09")
    U_09("U09"),

    /**
     * Unfall mit Kapitalrückgewähr
     * 
     */
    @XmlEnumValue("U10")
    U_10("U10"),

    /**
     * Taggeld
     * 
     */
    @XmlEnumValue("U11")
    U_11("U11"),

    /**
     * Invalidität
     * 
     */
    @XmlEnumValue("U12")
    U_12("U12"),

    /**
     * Unfallrente
     * 
     */
    @XmlEnumValue("U13")
    U_13("U13"),

    /**
     * Unfalltod
     * 
     */
    @XmlEnumValue("U14")
    U_14("U14"),

    /**
     * Spitalgeld
     * 
     */
    @XmlEnumValue("U15")
    U_15("U15"),

    /**
     * Unfallkosten
     * 
     */
    @XmlEnumValue("U16")
    U_16("U16"),

    /**
     * Kostenersatz
     * 
     */
    @XmlEnumValue("U17")
    U_17("U17");
    private final String value;

    RisikoArtCdType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static RisikoArtCdType fromValue(String v) {
        for (RisikoArtCdType c: RisikoArtCdType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
