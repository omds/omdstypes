
package at.vvo.omds.types.omds2Types.v2_11;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für TxtArtCd_Type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="TxtArtCd_Type"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="ANR"/&gt;
 *     &lt;enumeration value="BER"/&gt;
 *     &lt;enumeration value="FRT"/&gt;
 *     &lt;enumeration value="TIT"/&gt;
 *     &lt;enumeration value="VKL"/&gt;
 *     &lt;enumeration value="SRT"/&gt;
 *     &lt;enumeration value="EXP"/&gt;
 *     &lt;enumeration value="ONR"/&gt;
 *     &lt;enumeration value="FZU"/&gt;
 *     &lt;enumeration value="RIM"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "TxtArtCd_Type")
@XmlEnum
public enum TxtArtCdType {


    /**
     * Anrede
     * 
     */
    ANR,

    /**
     * Beruf
     * 
     */
    BER,

    /**
     * Freitext
     * 
     */
    FRT,

    /**
     * Titel
     * 
     */
    TIT,

    /**
     * Vertragsspez. Klausel
     * 
     */
    VKL,

    /**
     * Beschreibung Schadenort
     * 
     */
    SRT,

    /**
     * Schaden-Expertise (Besichtigungsdaten)
     * 
     */
    EXP,

    /**
     * Oeamtc-Clubkarten-Nr
     * 
     */
    ONR,

    /**
     * Ordnungsbegriff für Zuordnung Fremdsystem
     * 
     */
    FZU,

    /**
     * Risikomerkmal
     * 
     */
    RIM;

    public String value() {
        return name();
    }

    public static TxtArtCdType fromValue(String v) {
        return valueOf(v);
    }

}
