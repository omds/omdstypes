
package at.vvo.omds.types.omds2Types.v2_11;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für VERS_OBJEKT_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="VERS_OBJEKT_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice minOccurs="0"&gt;
 *           &lt;element ref="{urn:omds20}VERS_PERSON"/&gt;
 *           &lt;element ref="{urn:omds20}VERS_KFZ"/&gt;
 *           &lt;element ref="{urn:omds20}VERS_SACHE"/&gt;
 *         &lt;/choice&gt;
 *         &lt;choice maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;element ref="{urn:omds20}EL-Anzahl"/&gt;
 *           &lt;element ref="{urn:omds20}EL-Betrag"/&gt;
 *           &lt;element ref="{urn:omds20}EL-Einstufung"/&gt;
 *           &lt;element ref="{urn:omds20}EL-Entscheidungsfrage"/&gt;
 *           &lt;element ref="{urn:omds20}EL-Identifizierung"/&gt;
 *           &lt;element ref="{urn:omds20}EL-Grenzwert"/&gt;
 *           &lt;element ref="{urn:omds20}EL-Text"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="ObjLfnr" use="required" type="{http://www.w3.org/2001/XMLSchema}unsignedShort" /&gt;
 *       &lt;attribute name="VersObjTxt"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="100"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VERS_OBJEKT_Type", propOrder = {
    "versperson",
    "verskfz",
    "verssache",
    "elAnzahlOrELBetragOrELEinstufung"
})
public class VERSOBJEKTType {

    @XmlElement(name = "VERS_PERSON")
    protected VERSPERSONType versperson;
    @XmlElement(name = "VERS_KFZ")
    protected VERSKFZ verskfz;
    @XmlElement(name = "VERS_SACHE")
    protected VERSSACHEType verssache;
    @XmlElements({
        @XmlElement(name = "EL-Anzahl", type = ELAnzahlType.class),
        @XmlElement(name = "EL-Betrag", type = ELBetragType.class),
        @XmlElement(name = "EL-Einstufung", type = ELEinstufungType.class),
        @XmlElement(name = "EL-Entscheidungsfrage", type = ELEntscheidungsfrageType.class),
        @XmlElement(name = "EL-Identifizierung", type = ELIdentifizierungType.class),
        @XmlElement(name = "EL-Grenzwert", type = ELGrenzwertType.class),
        @XmlElement(name = "EL-Text", type = ELTextType.class)
    })
    protected List<Object> elAnzahlOrELBetragOrELEinstufung;
    @XmlAttribute(name = "ObjLfnr", required = true)
    @XmlSchemaType(name = "unsignedShort")
    protected int objLfnr;
    @XmlAttribute(name = "VersObjTxt")
    protected String versObjTxt;

    /**
     * Ruft den Wert der versperson-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VERSPERSONType }
     *     
     */
    public VERSPERSONType getVERSPERSON() {
        return versperson;
    }

    /**
     * Legt den Wert der versperson-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VERSPERSONType }
     *     
     */
    public void setVERSPERSON(VERSPERSONType value) {
        this.versperson = value;
    }

    /**
     * Ruft den Wert der verskfz-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VERSKFZ }
     *     
     */
    public VERSKFZ getVERSKFZ() {
        return verskfz;
    }

    /**
     * Legt den Wert der verskfz-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VERSKFZ }
     *     
     */
    public void setVERSKFZ(VERSKFZ value) {
        this.verskfz = value;
    }

    /**
     * Ruft den Wert der verssache-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VERSSACHEType }
     *     
     */
    public VERSSACHEType getVERSSACHE() {
        return verssache;
    }

    /**
     * Legt den Wert der verssache-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VERSSACHEType }
     *     
     */
    public void setVERSSACHE(VERSSACHEType value) {
        this.verssache = value;
    }

    /**
     * Gets the value of the elAnzahlOrELBetragOrELEinstufung property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the elAnzahlOrELBetragOrELEinstufung property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getELAnzahlOrELBetragOrELEinstufung().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ELAnzahlType }
     * {@link ELBetragType }
     * {@link ELEinstufungType }
     * {@link ELEntscheidungsfrageType }
     * {@link ELIdentifizierungType }
     * {@link ELGrenzwertType }
     * {@link ELTextType }
     * 
     * 
     */
    public List<Object> getELAnzahlOrELBetragOrELEinstufung() {
        if (elAnzahlOrELBetragOrELEinstufung == null) {
            elAnzahlOrELBetragOrELEinstufung = new ArrayList<Object>();
        }
        return this.elAnzahlOrELBetragOrELEinstufung;
    }

    /**
     * Ruft den Wert der objLfnr-Eigenschaft ab.
     * 
     */
    public int getObjLfnr() {
        return objLfnr;
    }

    /**
     * Legt den Wert der objLfnr-Eigenschaft fest.
     * 
     */
    public void setObjLfnr(int value) {
        this.objLfnr = value;
    }

    /**
     * Ruft den Wert der versObjTxt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersObjTxt() {
        return versObjTxt;
    }

    /**
     * Legt den Wert der versObjTxt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersObjTxt(String value) {
        this.versObjTxt = value;
    }

}
