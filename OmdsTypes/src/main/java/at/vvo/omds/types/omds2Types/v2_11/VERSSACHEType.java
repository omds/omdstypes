
package at.vvo.omds.types.omds2Types.v2_11;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für VERS_SACHE_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="VERS_SACHE_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:omds20}EL-Objektdaten" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:omds20}EL-Flaeche" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attGroup ref="{urn:omds20}Adresse_Attribute"/&gt;
 *       &lt;attribute name="VersSacheCd" use="required" type="{urn:omds20}VersSacheCd_Type" /&gt;
 *       &lt;attribute name="VersSacheBez"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="60"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="GebaeudeArtCd" type="{urn:omds20}GebaeudeArtCd_Type" /&gt;
 *       &lt;attribute name="GebaeudeBez"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="60"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="BauartCd" type="{urn:omds20}BauartCd_Type" /&gt;
 *       &lt;attribute name="GebaeudeHoeheCd" type="{urn:omds20}GebaeudeHoeheCd_Type" /&gt;
 *       &lt;attribute name="AusstattungCd" type="{urn:omds20}AusstattungCd_Type" /&gt;
 *       &lt;attribute name="DachungCd" type="{urn:omds20}DachungCd_Type" /&gt;
 *       &lt;attribute name="NutzungCd" type="{urn:omds20}NutzungCd_Type" /&gt;
 *       &lt;attribute name="SichergKz" type="{urn:omds20}Entsch2_Type" /&gt;
 *       &lt;attribute name="VersLwCd" type="{urn:omds20}VersLwCd_Type" /&gt;
 *       &lt;attribute name="StatusCd" type="{urn:omds20}StatusCd_Type" /&gt;
 *       &lt;attribute name="Hersteller"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="40"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="Modell"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="40"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VERS_SACHE_Type", propOrder = {
    "elObjektdaten",
    "elFlaeche"
})
public class VERSSACHEType {

    @XmlElement(name = "EL-Objektdaten")
    protected List<ELObjektdatenType> elObjektdaten;
    @XmlElement(name = "EL-Flaeche")
    protected List<ELFlaecheType> elFlaeche;
    @XmlAttribute(name = "VersSacheCd", required = true)
    protected String versSacheCd;
    @XmlAttribute(name = "VersSacheBez")
    protected String versSacheBez;
    @XmlAttribute(name = "GebaeudeArtCd")
    protected String gebaeudeArtCd;
    @XmlAttribute(name = "GebaeudeBez")
    protected String gebaeudeBez;
    @XmlAttribute(name = "BauartCd")
    protected String bauartCd;
    @XmlAttribute(name = "GebaeudeHoeheCd")
    protected String gebaeudeHoeheCd;
    @XmlAttribute(name = "AusstattungCd")
    protected String ausstattungCd;
    @XmlAttribute(name = "DachungCd")
    protected String dachungCd;
    @XmlAttribute(name = "NutzungCd")
    protected String nutzungCd;
    @XmlAttribute(name = "SichergKz")
    protected Entsch2Type sichergKz;
    @XmlAttribute(name = "VersLwCd")
    protected String versLwCd;
    @XmlAttribute(name = "StatusCd")
    protected String statusCd;
    @XmlAttribute(name = "Hersteller")
    protected String hersteller;
    @XmlAttribute(name = "Modell")
    protected String modell;
    @XmlAttribute(name = "Pac")
    @XmlSchemaType(name = "unsignedInt")
    protected Long pac;
    @XmlAttribute(name = "LandesCd")
    protected String landesCd;
    @XmlAttribute(name = "PLZ")
    protected String plz;
    @XmlAttribute(name = "Ort")
    protected String ort;
    @XmlAttribute(name = "Strasse")
    protected String strasse;
    @XmlAttribute(name = "Hausnr")
    protected String hausnr;
    @XmlAttribute(name = "Zusatz")
    protected String zusatz;

    /**
     * Gets the value of the elObjektdaten property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the elObjektdaten property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getELObjektdaten().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ELObjektdatenType }
     * 
     * 
     */
    public List<ELObjektdatenType> getELObjektdaten() {
        if (elObjektdaten == null) {
            elObjektdaten = new ArrayList<ELObjektdatenType>();
        }
        return this.elObjektdaten;
    }

    /**
     * Gets the value of the elFlaeche property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the elFlaeche property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getELFlaeche().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ELFlaecheType }
     * 
     * 
     */
    public List<ELFlaecheType> getELFlaeche() {
        if (elFlaeche == null) {
            elFlaeche = new ArrayList<ELFlaecheType>();
        }
        return this.elFlaeche;
    }

    /**
     * Ruft den Wert der versSacheCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersSacheCd() {
        return versSacheCd;
    }

    /**
     * Legt den Wert der versSacheCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersSacheCd(String value) {
        this.versSacheCd = value;
    }

    /**
     * Ruft den Wert der versSacheBez-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersSacheBez() {
        return versSacheBez;
    }

    /**
     * Legt den Wert der versSacheBez-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersSacheBez(String value) {
        this.versSacheBez = value;
    }

    /**
     * Ruft den Wert der gebaeudeArtCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGebaeudeArtCd() {
        return gebaeudeArtCd;
    }

    /**
     * Legt den Wert der gebaeudeArtCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGebaeudeArtCd(String value) {
        this.gebaeudeArtCd = value;
    }

    /**
     * Ruft den Wert der gebaeudeBez-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGebaeudeBez() {
        return gebaeudeBez;
    }

    /**
     * Legt den Wert der gebaeudeBez-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGebaeudeBez(String value) {
        this.gebaeudeBez = value;
    }

    /**
     * Ruft den Wert der bauartCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBauartCd() {
        return bauartCd;
    }

    /**
     * Legt den Wert der bauartCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBauartCd(String value) {
        this.bauartCd = value;
    }

    /**
     * Ruft den Wert der gebaeudeHoeheCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGebaeudeHoeheCd() {
        return gebaeudeHoeheCd;
    }

    /**
     * Legt den Wert der gebaeudeHoeheCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGebaeudeHoeheCd(String value) {
        this.gebaeudeHoeheCd = value;
    }

    /**
     * Ruft den Wert der ausstattungCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAusstattungCd() {
        return ausstattungCd;
    }

    /**
     * Legt den Wert der ausstattungCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAusstattungCd(String value) {
        this.ausstattungCd = value;
    }

    /**
     * Ruft den Wert der dachungCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDachungCd() {
        return dachungCd;
    }

    /**
     * Legt den Wert der dachungCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDachungCd(String value) {
        this.dachungCd = value;
    }

    /**
     * Ruft den Wert der nutzungCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNutzungCd() {
        return nutzungCd;
    }

    /**
     * Legt den Wert der nutzungCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNutzungCd(String value) {
        this.nutzungCd = value;
    }

    /**
     * Ruft den Wert der sichergKz-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Entsch2Type }
     *     
     */
    public Entsch2Type getSichergKz() {
        return sichergKz;
    }

    /**
     * Legt den Wert der sichergKz-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Entsch2Type }
     *     
     */
    public void setSichergKz(Entsch2Type value) {
        this.sichergKz = value;
    }

    /**
     * Ruft den Wert der versLwCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersLwCd() {
        return versLwCd;
    }

    /**
     * Legt den Wert der versLwCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersLwCd(String value) {
        this.versLwCd = value;
    }

    /**
     * Ruft den Wert der statusCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatusCd() {
        return statusCd;
    }

    /**
     * Legt den Wert der statusCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatusCd(String value) {
        this.statusCd = value;
    }

    /**
     * Ruft den Wert der hersteller-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHersteller() {
        return hersteller;
    }

    /**
     * Legt den Wert der hersteller-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHersteller(String value) {
        this.hersteller = value;
    }

    /**
     * Ruft den Wert der modell-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModell() {
        return modell;
    }

    /**
     * Legt den Wert der modell-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModell(String value) {
        this.modell = value;
    }

    /**
     * Ruft den Wert der pac-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getPac() {
        return pac;
    }

    /**
     * Legt den Wert der pac-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setPac(Long value) {
        this.pac = value;
    }

    /**
     * Ruft den Wert der landesCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLandesCd() {
        return landesCd;
    }

    /**
     * Legt den Wert der landesCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLandesCd(String value) {
        this.landesCd = value;
    }

    /**
     * Ruft den Wert der plz-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPLZ() {
        return plz;
    }

    /**
     * Legt den Wert der plz-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPLZ(String value) {
        this.plz = value;
    }

    /**
     * Ruft den Wert der ort-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrt() {
        return ort;
    }

    /**
     * Legt den Wert der ort-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrt(String value) {
        this.ort = value;
    }

    /**
     * Ruft den Wert der strasse-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStrasse() {
        return strasse;
    }

    /**
     * Legt den Wert der strasse-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStrasse(String value) {
        this.strasse = value;
    }

    /**
     * Ruft den Wert der hausnr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHausnr() {
        return hausnr;
    }

    /**
     * Legt den Wert der hausnr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHausnr(String value) {
        this.hausnr = value;
    }

    /**
     * Ruft den Wert der zusatz-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZusatz() {
        return zusatz;
    }

    /**
     * Legt den Wert der zusatz-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZusatz(String value) {
        this.zusatz = value;
    }

}
