
package at.vvo.omds.types.omds2Types.v2_11;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für VERTRAGSFONDS_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="VERTRAGSFONDS_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:omds20}PORTFOLIO" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:omds20}FONDS" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:omds20}EL-Betrag" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:omds20}EL-Text" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="Polizzennr" use="required" type="{urn:omds20}Polizzennr" /&gt;
 *       &lt;attribute name="VertragsID" type="{urn:omds20}VertragsID" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VERTRAGSFONDS_Type", propOrder = {
    "portfolio",
    "fonds",
    "elBetrag",
    "elText"
})
public class VERTRAGSFONDSType {

    @XmlElement(name = "PORTFOLIO")
    protected List<PORTFOLIO> portfolio;
    @XmlElement(name = "FONDS")
    protected List<FONDSType> fonds;
    @XmlElement(name = "EL-Betrag")
    protected List<ELBetragType> elBetrag;
    @XmlElement(name = "EL-Text")
    protected List<ELTextType> elText;
    @XmlAttribute(name = "Polizzennr", required = true)
    protected String polizzennr;
    @XmlAttribute(name = "VertragsID")
    protected String vertragsID;

    /**
     * Gets the value of the portfolio property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the portfolio property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPORTFOLIO().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PORTFOLIO }
     * 
     * 
     */
    public List<PORTFOLIO> getPORTFOLIO() {
        if (portfolio == null) {
            portfolio = new ArrayList<PORTFOLIO>();
        }
        return this.portfolio;
    }

    /**
     * Gets the value of the fonds property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fonds property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFONDS().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FONDSType }
     * 
     * 
     */
    public List<FONDSType> getFONDS() {
        if (fonds == null) {
            fonds = new ArrayList<FONDSType>();
        }
        return this.fonds;
    }

    /**
     * Gets the value of the elBetrag property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the elBetrag property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getELBetrag().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ELBetragType }
     * 
     * 
     */
    public List<ELBetragType> getELBetrag() {
        if (elBetrag == null) {
            elBetrag = new ArrayList<ELBetragType>();
        }
        return this.elBetrag;
    }

    /**
     * Gets the value of the elText property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the elText property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getELText().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ELTextType }
     * 
     * 
     */
    public List<ELTextType> getELText() {
        if (elText == null) {
            elText = new ArrayList<ELTextType>();
        }
        return this.elText;
    }

    /**
     * Ruft den Wert der polizzennr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolizzennr() {
        return polizzennr;
    }

    /**
     * Legt den Wert der polizzennr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolizzennr(String value) {
        this.polizzennr = value;
    }

    /**
     * Ruft den Wert der vertragsID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVertragsID() {
        return vertragsID;
    }

    /**
     * Legt den Wert der vertragsID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVertragsID(String value) {
        this.vertragsID = value;
    }

}
