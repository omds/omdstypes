
package at.vvo.omds.types.omds2Types.v2_11;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für VSArtCd_Type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="VSArtCd_Type"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="ALS"/&gt;
 *     &lt;enumeration value="BDS"/&gt;
 *     &lt;enumeration value="BVS"/&gt;
 *     &lt;enumeration value="ELS"/&gt;
 *     &lt;enumeration value="EVS"/&gt;
 *     &lt;enumeration value="HHS"/&gt;
 *     &lt;enumeration value="HLS"/&gt;
 *     &lt;enumeration value="OVS"/&gt;
 *     &lt;enumeration value="PFR"/&gt;
 *     &lt;enumeration value="PPF"/&gt;
 *     &lt;enumeration value="PVS"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "VSArtCd_Type")
@XmlEnum
public enum VSArtCdType {


    /**
     * Ablebensumme
     * 
     */
    ALS,

    /**
     * Bausparen Darlehenssumme
     * 
     */
    BDS,

    /**
     * Bausparen Vertragssumme
     * 
     */
    BVS,

    /**
     * Erlebensumme
     * 
     */
    ELS,

    /**
     * EinzelVS
     * 
     */
    EVS,

    /**
     * Höchsthaftungssumme
     * 
     */
    HHS,

    /**
     * Höchstleistungssumme
     * 
     */
    HLS,

    /**
     * ohne Versicherungssumme
     * 
     */
    OVS,

    /**
     * prämienfrei
     * 
     */
    PFR,

    /**
     * prämienpflichtig
     * 
     */
    PPF,

    /**
     * PauschalVS
     * 
     */
    PVS;

    public String value() {
        return name();
    }

    public static VSArtCdType fromValue(String v) {
        return valueOf(v);
    }

}
