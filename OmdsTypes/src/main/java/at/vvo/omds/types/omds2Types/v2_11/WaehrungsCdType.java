
package at.vvo.omds.types.omds2Types.v2_11;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für WaehrungsCd_Type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="WaehrungsCd_Type"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="AUD"/&gt;
 *     &lt;enumeration value="BGL"/&gt;
 *     &lt;enumeration value="CAD"/&gt;
 *     &lt;enumeration value="CHF"/&gt;
 *     &lt;enumeration value="CYP"/&gt;
 *     &lt;enumeration value="CZR"/&gt;
 *     &lt;enumeration value="DKK"/&gt;
 *     &lt;enumeration value="EUR"/&gt;
 *     &lt;enumeration value="GBP"/&gt;
 *     &lt;enumeration value="GIP"/&gt;
 *     &lt;enumeration value="HKD"/&gt;
 *     &lt;enumeration value="HRK"/&gt;
 *     &lt;enumeration value="HUF"/&gt;
 *     &lt;enumeration value="ILS"/&gt;
 *     &lt;enumeration value="INR"/&gt;
 *     &lt;enumeration value="ISK"/&gt;
 *     &lt;enumeration value="JOD"/&gt;
 *     &lt;enumeration value="JPY"/&gt;
 *     &lt;enumeration value="MLT"/&gt;
 *     &lt;enumeration value="MXP"/&gt;
 *     &lt;enumeration value="NOK"/&gt;
 *     &lt;enumeration value="NZD"/&gt;
 *     &lt;enumeration value="PLZ"/&gt;
 *     &lt;enumeration value="ROL"/&gt;
 *     &lt;enumeration value="SEK"/&gt;
 *     &lt;enumeration value="SGD"/&gt;
 *     &lt;enumeration value="SIT"/&gt;
 *     &lt;enumeration value="SKK"/&gt;
 *     &lt;enumeration value="SUR"/&gt;
 *     &lt;enumeration value="TND"/&gt;
 *     &lt;enumeration value="TRL"/&gt;
 *     &lt;enumeration value="TWD"/&gt;
 *     &lt;enumeration value="USD"/&gt;
 *     &lt;enumeration value="YUN"/&gt;
 *     &lt;enumeration value="ZAR"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "WaehrungsCd_Type")
@XmlEnum
public enum WaehrungsCdType {


    /**
     * Dollar Australien
     * 
     */
    AUD,

    /**
     * Lewa Bulgarien
     * 
     */
    BGL,

    /**
     * Dollar Kanadien
     * 
     */
    CAD,

    /**
     * Franken Schweiz
     * 
     */
    CHF,

    /**
     * Pfund Zypern
     * 
     */
    CYP,

    /**
     * Krone Tschechien
     * 
     */
    CZR,

    /**
     * Krone Dänemark
     * 
     */
    DKK,

    /**
     * EURO
     * 
     */
    EUR,

    /**
     * Pfund Großbritannien
     * 
     */
    GBP,

    /**
     * Pfund Gibraltar
     * 
     */
    GIP,

    /**
     * Dollar Hongkong
     * 
     */
    HKD,

    /**
     * Kuna Kroatia
     * 
     */
    HRK,

    /**
     * Forint Ungarn
     * 
     */
    HUF,

    /**
     * Shekel Israel
     * 
     */
    ILS,

    /**
     * Rupie Indien
     * 
     */
    INR,

    /**
     * Krone Island
     * 
     */
    ISK,

    /**
     * Dinar Jordanien
     * 
     */
    JOD,

    /**
     * Yen Japan
     * 
     */
    JPY,

    /**
     * Pfund Malta
     * 
     */
    MLT,

    /**
     * Peso Mexiko
     * 
     */
    MXP,

    /**
     * Krone Norwegen
     * 
     */
    NOK,

    /**
     * Dollar Neuseeland
     * 
     */
    NZD,

    /**
     * Zloty Polen
     * 
     */
    PLZ,

    /**
     * Lau Rumänien
     * 
     */
    ROL,

    /**
     * Krone Schweden
     * 
     */
    SEK,

    /**
     * Dollar Singapur
     * 
     */
    SGD,

    /**
     * Tolar Slowenien
     * 
     */
    SIT,

    /**
     * Krone Slowakei
     * 
     */
    SKK,

    /**
     * Rubel Rußland
     * 
     */
    SUR,

    /**
     * Dinar Tunesien
     * 
     */
    TND,

    /**
     * Lira Türkei
     * 
     */
    TRL,

    /**
     * Dollar Taiwan
     * 
     */
    TWD,

    /**
     * US Dollar $
     * 
     */
    USD,

    /**
     * Dinar Jugoslawien-Rest
     * 
     */
    YUN,

    /**
     * Rand Südafrika
     * 
     */
    ZAR;

    public String value() {
        return name();
    }

    public static WaehrungsCdType fromValue(String v) {
        return valueOf(v);
    }

}
