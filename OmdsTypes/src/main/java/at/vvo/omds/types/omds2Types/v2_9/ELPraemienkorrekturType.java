
package at.vvo.omds.types.omds2Types.v2_9;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für EL-Praemienkorrektur_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="EL-Praemienkorrektur_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="PraemKorrArtCd" use="required" type="{urn:omds20}PraemKorrArtCd_Type" /&gt;
 *       &lt;attribute name="PraemKorrWert" type="{urn:omds20}decimal" /&gt;
 *       &lt;attribute name="PraemKorrProz" type="{urn:omds20}decimal" /&gt;
 *       &lt;attribute name="PraemKorrText"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="80"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EL-Praemienkorrektur_Type")
public class ELPraemienkorrekturType {

    @XmlAttribute(name = "PraemKorrArtCd", required = true)
    protected String praemKorrArtCd;
    @XmlAttribute(name = "PraemKorrWert")
    protected BigDecimal praemKorrWert;
    @XmlAttribute(name = "PraemKorrProz")
    protected BigDecimal praemKorrProz;
    @XmlAttribute(name = "PraemKorrText")
    protected String praemKorrText;

    /**
     * Ruft den Wert der praemKorrArtCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPraemKorrArtCd() {
        return praemKorrArtCd;
    }

    /**
     * Legt den Wert der praemKorrArtCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPraemKorrArtCd(String value) {
        this.praemKorrArtCd = value;
    }

    /**
     * Ruft den Wert der praemKorrWert-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPraemKorrWert() {
        return praemKorrWert;
    }

    /**
     * Legt den Wert der praemKorrWert-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPraemKorrWert(BigDecimal value) {
        this.praemKorrWert = value;
    }

    /**
     * Ruft den Wert der praemKorrProz-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPraemKorrProz() {
        return praemKorrProz;
    }

    /**
     * Legt den Wert der praemKorrProz-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPraemKorrProz(BigDecimal value) {
        this.praemKorrProz = value;
    }

    /**
     * Ruft den Wert der praemKorrText-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPraemKorrText() {
        return praemKorrText;
    }

    /**
     * Legt den Wert der praemKorrText-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPraemKorrText(String value) {
        this.praemKorrText = value;
    }

}
