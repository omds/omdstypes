
package at.vvo.omds.types.omds2Types.v2_9;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java-Klasse für EL-Rente_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="EL-Rente_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="RntRhythmCd" use="required" type="{urn:omds20}RntRhythmCd_Type" /&gt;
 *       &lt;attribute name="RntBeg" type="{urn:omds20}Datum" /&gt;
 *       &lt;attribute name="RntBetrag" use="required" type="{urn:omds20}decimal" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EL-Rente_Type")
public class ELRenteType {

    @XmlAttribute(name = "RntRhythmCd", required = true)
    protected String rntRhythmCd;
    @XmlAttribute(name = "RntBeg")
    protected XMLGregorianCalendar rntBeg;
    @XmlAttribute(name = "RntBetrag", required = true)
    protected BigDecimal rntBetrag;

    /**
     * Ruft den Wert der rntRhythmCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRntRhythmCd() {
        return rntRhythmCd;
    }

    /**
     * Legt den Wert der rntRhythmCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRntRhythmCd(String value) {
        this.rntRhythmCd = value;
    }

    /**
     * Ruft den Wert der rntBeg-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRntBeg() {
        return rntBeg;
    }

    /**
     * Legt den Wert der rntBeg-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRntBeg(XMLGregorianCalendar value) {
        this.rntBeg = value;
    }

    /**
     * Ruft den Wert der rntBetrag-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRntBetrag() {
        return rntBetrag;
    }

    /**
     * Legt den Wert der rntBetrag-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRntBetrag(BigDecimal value) {
        this.rntBetrag = value;
    }

}
