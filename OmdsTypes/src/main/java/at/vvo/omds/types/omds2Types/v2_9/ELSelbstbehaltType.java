
package at.vvo.omds.types.omds2Types.v2_9;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für EL-Selbstbehalt_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="EL-Selbstbehalt_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="SbhArtCd" use="required" type="{urn:omds20}SbhArtCd_Type" /&gt;
 *       &lt;attribute name="SbhBetrag" type="{urn:omds20}decimal" /&gt;
 *       &lt;attribute name="SbhProzent" type="{urn:omds20}decimal" /&gt;
 *       &lt;attribute name="SbhText"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="80"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EL-Selbstbehalt_Type")
@XmlSeeAlso({
    ELSelbstbehalt.class
})
public class ELSelbstbehaltType {

    @XmlAttribute(name = "SbhArtCd", required = true)
    protected SbhArtCdType sbhArtCd;
    @XmlAttribute(name = "SbhBetrag")
    protected BigDecimal sbhBetrag;
    @XmlAttribute(name = "SbhProzent")
    protected BigDecimal sbhProzent;
    @XmlAttribute(name = "SbhText")
    protected String sbhText;

    /**
     * Ruft den Wert der sbhArtCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SbhArtCdType }
     *     
     */
    public SbhArtCdType getSbhArtCd() {
        return sbhArtCd;
    }

    /**
     * Legt den Wert der sbhArtCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SbhArtCdType }
     *     
     */
    public void setSbhArtCd(SbhArtCdType value) {
        this.sbhArtCd = value;
    }

    /**
     * Ruft den Wert der sbhBetrag-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSbhBetrag() {
        return sbhBetrag;
    }

    /**
     * Legt den Wert der sbhBetrag-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSbhBetrag(BigDecimal value) {
        this.sbhBetrag = value;
    }

    /**
     * Ruft den Wert der sbhProzent-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSbhProzent() {
        return sbhProzent;
    }

    /**
     * Legt den Wert der sbhProzent-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSbhProzent(BigDecimal value) {
        this.sbhProzent = value;
    }

    /**
     * Ruft den Wert der sbhText-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSbhText() {
        return sbhText;
    }

    /**
     * Legt den Wert der sbhText-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSbhText(String value) {
        this.sbhText = value;
    }

}
