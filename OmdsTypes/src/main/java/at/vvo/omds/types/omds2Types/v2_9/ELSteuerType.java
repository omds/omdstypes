
package at.vvo.omds.types.omds2Types.v2_9;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für EL-Steuer_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="EL-Steuer_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="StArtCd" use="required" type="{urn:omds20}StArtCd_Type" /&gt;
 *       &lt;attribute name="StBetrag" use="required" type="{urn:omds20}decimal" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EL-Steuer_Type")
public class ELSteuerType {

    @XmlAttribute(name = "StArtCd", required = true)
    protected String stArtCd;
    @XmlAttribute(name = "StBetrag", required = true)
    protected BigDecimal stBetrag;

    /**
     * Ruft den Wert der stArtCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStArtCd() {
        return stArtCd;
    }

    /**
     * Legt den Wert der stArtCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStArtCd(String value) {
        this.stArtCd = value;
    }

    /**
     * Ruft den Wert der stBetrag-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getStBetrag() {
        return stBetrag;
    }

    /**
     * Legt den Wert der stBetrag-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setStBetrag(BigDecimal value) {
        this.stBetrag = value;
    }

}
