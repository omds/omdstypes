
package at.vvo.omds.types.omds2Types.v2_9;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für EL-Versicherungssumme_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="EL-Versicherungssumme_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="VSArtCd" use="required" type="{urn:omds20}VSArtCd_Type" /&gt;
 *       &lt;attribute name="VSBetrag" use="required" type="{urn:omds20}decimal14_2" /&gt;
 *       &lt;attribute name="VSBez"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="255"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EL-Versicherungssumme_Type")
public class ELVersicherungssummeType {

    @XmlAttribute(name = "VSArtCd", required = true)
    protected VSArtCdType vsArtCd;
    @XmlAttribute(name = "VSBetrag", required = true)
    protected BigDecimal vsBetrag;
    @XmlAttribute(name = "VSBez")
    protected String vsBez;

    /**
     * Ruft den Wert der vsArtCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VSArtCdType }
     *     
     */
    public VSArtCdType getVSArtCd() {
        return vsArtCd;
    }

    /**
     * Legt den Wert der vsArtCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VSArtCdType }
     *     
     */
    public void setVSArtCd(VSArtCdType value) {
        this.vsArtCd = value;
    }

    /**
     * Ruft den Wert der vsBetrag-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getVSBetrag() {
        return vsBetrag;
    }

    /**
     * Legt den Wert der vsBetrag-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setVSBetrag(BigDecimal value) {
        this.vsBetrag = value;
    }

    /**
     * Ruft den Wert der vsBez-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVSBez() {
        return vsBez;
    }

    /**
     * Legt den Wert der vsBez-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVSBez(String value) {
        this.vsBez = value;
    }

}
