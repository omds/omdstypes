
package at.vvo.omds.types.omds2Types.v2_9;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für IdfArtCd_Type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="IdfArtCd_Type"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="FB"/&gt;
 *     &lt;enumeration value="SV"/&gt;
 *     &lt;enumeration value="VB"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "IdfArtCd_Type")
@XmlEnum
public enum IdfArtCdType {


    /**
     * Firmenbuchnummer
     * 
     */
    FB,

    /**
     * Sozialversicherungsnr
     * 
     */
    SV,

    /**
     * Versicherungsbestätigung
     * 
     */
    VB;

    public String value() {
        return name();
    }

    public static IdfArtCdType fromValue(String v) {
        return valueOf(v);
    }

}
