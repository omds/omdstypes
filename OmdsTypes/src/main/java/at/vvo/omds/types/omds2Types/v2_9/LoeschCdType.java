
package at.vvo.omds.types.omds2Types.v2_9;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für LoeschCd_Type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="LoeschCd_Type"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="M"/&gt;
 *     &lt;enumeration value="L"/&gt;
 *     &lt;enumeration value="G"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "LoeschCd_Type")
@XmlEnum
public enum LoeschCdType {


    /**
     * Markieren als nichtmehrversorgt
     * 
     */
    M,

    /**
     * Löschen wg.z.B.Falschlieferung
     * 
     */
    L,

    /**
     * Gesetzlich: Hinweis an Makler - Person/Vertrag/Schaden wurden bei der VU wegen DSG gelöscht
     * 
     */
    G;

    public String value() {
        return name();
    }

    public static LoeschCdType fromValue(String v) {
        return valueOf(v);
    }

}
