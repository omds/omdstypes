
package at.vvo.omds.types.omds2Types.v2_9;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java-Klasse für MAHNUNG_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="MAHNUNG_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;element ref="{urn:omds20}EL-Text"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="MahnverfahrenNr" use="required"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;minLength value="1"/&gt;
 *             &lt;maxLength value="32"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="MahnungNr" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&gt;
 *       &lt;attribute name="Polizzennr" use="required" type="{urn:omds20}Polizzennr" /&gt;
 *       &lt;attribute name="VertragsID" type="{urn:omds20}VertragsID" /&gt;
 *       &lt;attribute name="Vermnr" use="required" type="{urn:omds20}Vermnr" /&gt;
 *       &lt;attribute name="MahnBetrag" use="required" type="{urn:omds20}decimal" /&gt;
 *       &lt;attribute name="MahnSpesen" type="{urn:omds20}decimal" /&gt;
 *       &lt;attribute name="MahnStufeCd" use="required" type="{urn:omds20}MahnStufeCd_Type" /&gt;
 *       &lt;attribute name="MahnStufeTextVU"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;minLength value="1"/&gt;
 *             &lt;maxLength value="40"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="MahnStelleVU"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;minLength value="1"/&gt;
 *             &lt;maxLength value="80"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="MahnStelleBeauftragt"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;minLength value="1"/&gt;
 *             &lt;maxLength value="80"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="MahnStufeGueltigAb" type="{urn:omds20}Datum" /&gt;
 *       &lt;attribute name="MahnStufeGueltigBis" type="{urn:omds20}Datum" /&gt;
 *       &lt;attribute name="MahnStufeCdNext" type="{urn:omds20}MahnStufeCd_Type" /&gt;
 *       &lt;attribute name="MahnStufeTextVUNext"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;minLength value="1"/&gt;
 *             &lt;maxLength value="40"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="MahnStufeGueltigAbNext" type="{urn:omds20}Datum" /&gt;
 *       &lt;attribute name="MahnLetzteZahlung" type="{urn:omds20}Datum" /&gt;
 *       &lt;attribute name="MahnAeltesteFaelligkeit" type="{urn:omds20}Datum" /&gt;
 *       &lt;attribute name="MahnAnzahlFaelligkeiten" type="{http://www.w3.org/2001/XMLSchema}unsignedShort" /&gt;
 *       &lt;attribute name="GrundRuecklaufCd" type="{urn:omds20}GrundRuecklaufCd_Type" /&gt;
 *       &lt;attribute name="MahnDeckungBis" type="{urn:omds20}Datum" /&gt;
 *       &lt;attribute name="VtgSparteCd" type="{urn:omds20}VtgSparteCd_Type" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MAHNUNG_Type", propOrder = {
    "elText"
})
public class MAHNUNGType {

    @XmlElement(name = "EL-Text")
    protected List<ELTextType> elText;
    @XmlAttribute(name = "MahnverfahrenNr", required = true)
    protected String mahnverfahrenNr;
    @XmlAttribute(name = "MahnungNr")
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger mahnungNr;
    @XmlAttribute(name = "Polizzennr", required = true)
    protected String polizzennr;
    @XmlAttribute(name = "VertragsID")
    protected String vertragsID;
    @XmlAttribute(name = "Vermnr", required = true)
    protected String vermnr;
    @XmlAttribute(name = "MahnBetrag", required = true)
    protected BigDecimal mahnBetrag;
    @XmlAttribute(name = "MahnSpesen")
    protected BigDecimal mahnSpesen;
    @XmlAttribute(name = "MahnStufeCd", required = true)
    protected String mahnStufeCd;
    @XmlAttribute(name = "MahnStufeTextVU")
    protected String mahnStufeTextVU;
    @XmlAttribute(name = "MahnStelleVU")
    protected String mahnStelleVU;
    @XmlAttribute(name = "MahnStelleBeauftragt")
    protected String mahnStelleBeauftragt;
    @XmlAttribute(name = "MahnStufeGueltigAb")
    protected XMLGregorianCalendar mahnStufeGueltigAb;
    @XmlAttribute(name = "MahnStufeGueltigBis")
    protected XMLGregorianCalendar mahnStufeGueltigBis;
    @XmlAttribute(name = "MahnStufeCdNext")
    protected String mahnStufeCdNext;
    @XmlAttribute(name = "MahnStufeTextVUNext")
    protected String mahnStufeTextVUNext;
    @XmlAttribute(name = "MahnStufeGueltigAbNext")
    protected XMLGregorianCalendar mahnStufeGueltigAbNext;
    @XmlAttribute(name = "MahnLetzteZahlung")
    protected XMLGregorianCalendar mahnLetzteZahlung;
    @XmlAttribute(name = "MahnAeltesteFaelligkeit")
    protected XMLGregorianCalendar mahnAeltesteFaelligkeit;
    @XmlAttribute(name = "MahnAnzahlFaelligkeiten")
    @XmlSchemaType(name = "unsignedShort")
    protected Integer mahnAnzahlFaelligkeiten;
    @XmlAttribute(name = "GrundRuecklaufCd")
    protected String grundRuecklaufCd;
    @XmlAttribute(name = "MahnDeckungBis")
    protected XMLGregorianCalendar mahnDeckungBis;
    @XmlAttribute(name = "VtgSparteCd")
    protected String vtgSparteCd;

    /**
     * Gets the value of the elText property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the elText property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getELText().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ELTextType }
     * 
     * 
     */
    public List<ELTextType> getELText() {
        if (elText == null) {
            elText = new ArrayList<ELTextType>();
        }
        return this.elText;
    }

    /**
     * Ruft den Wert der mahnverfahrenNr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMahnverfahrenNr() {
        return mahnverfahrenNr;
    }

    /**
     * Legt den Wert der mahnverfahrenNr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMahnverfahrenNr(String value) {
        this.mahnverfahrenNr = value;
    }

    /**
     * Ruft den Wert der mahnungNr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMahnungNr() {
        return mahnungNr;
    }

    /**
     * Legt den Wert der mahnungNr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMahnungNr(BigInteger value) {
        this.mahnungNr = value;
    }

    /**
     * Ruft den Wert der polizzennr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolizzennr() {
        return polizzennr;
    }

    /**
     * Legt den Wert der polizzennr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolizzennr(String value) {
        this.polizzennr = value;
    }

    /**
     * Ruft den Wert der vertragsID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVertragsID() {
        return vertragsID;
    }

    /**
     * Legt den Wert der vertragsID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVertragsID(String value) {
        this.vertragsID = value;
    }

    /**
     * Ruft den Wert der vermnr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVermnr() {
        return vermnr;
    }

    /**
     * Legt den Wert der vermnr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVermnr(String value) {
        this.vermnr = value;
    }

    /**
     * Ruft den Wert der mahnBetrag-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMahnBetrag() {
        return mahnBetrag;
    }

    /**
     * Legt den Wert der mahnBetrag-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMahnBetrag(BigDecimal value) {
        this.mahnBetrag = value;
    }

    /**
     * Ruft den Wert der mahnSpesen-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMahnSpesen() {
        return mahnSpesen;
    }

    /**
     * Legt den Wert der mahnSpesen-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMahnSpesen(BigDecimal value) {
        this.mahnSpesen = value;
    }

    /**
     * Ruft den Wert der mahnStufeCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMahnStufeCd() {
        return mahnStufeCd;
    }

    /**
     * Legt den Wert der mahnStufeCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMahnStufeCd(String value) {
        this.mahnStufeCd = value;
    }

    /**
     * Ruft den Wert der mahnStufeTextVU-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMahnStufeTextVU() {
        return mahnStufeTextVU;
    }

    /**
     * Legt den Wert der mahnStufeTextVU-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMahnStufeTextVU(String value) {
        this.mahnStufeTextVU = value;
    }

    /**
     * Ruft den Wert der mahnStelleVU-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMahnStelleVU() {
        return mahnStelleVU;
    }

    /**
     * Legt den Wert der mahnStelleVU-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMahnStelleVU(String value) {
        this.mahnStelleVU = value;
    }

    /**
     * Ruft den Wert der mahnStelleBeauftragt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMahnStelleBeauftragt() {
        return mahnStelleBeauftragt;
    }

    /**
     * Legt den Wert der mahnStelleBeauftragt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMahnStelleBeauftragt(String value) {
        this.mahnStelleBeauftragt = value;
    }

    /**
     * Ruft den Wert der mahnStufeGueltigAb-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getMahnStufeGueltigAb() {
        return mahnStufeGueltigAb;
    }

    /**
     * Legt den Wert der mahnStufeGueltigAb-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setMahnStufeGueltigAb(XMLGregorianCalendar value) {
        this.mahnStufeGueltigAb = value;
    }

    /**
     * Ruft den Wert der mahnStufeGueltigBis-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getMahnStufeGueltigBis() {
        return mahnStufeGueltigBis;
    }

    /**
     * Legt den Wert der mahnStufeGueltigBis-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setMahnStufeGueltigBis(XMLGregorianCalendar value) {
        this.mahnStufeGueltigBis = value;
    }

    /**
     * Ruft den Wert der mahnStufeCdNext-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMahnStufeCdNext() {
        return mahnStufeCdNext;
    }

    /**
     * Legt den Wert der mahnStufeCdNext-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMahnStufeCdNext(String value) {
        this.mahnStufeCdNext = value;
    }

    /**
     * Ruft den Wert der mahnStufeTextVUNext-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMahnStufeTextVUNext() {
        return mahnStufeTextVUNext;
    }

    /**
     * Legt den Wert der mahnStufeTextVUNext-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMahnStufeTextVUNext(String value) {
        this.mahnStufeTextVUNext = value;
    }

    /**
     * Ruft den Wert der mahnStufeGueltigAbNext-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getMahnStufeGueltigAbNext() {
        return mahnStufeGueltigAbNext;
    }

    /**
     * Legt den Wert der mahnStufeGueltigAbNext-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setMahnStufeGueltigAbNext(XMLGregorianCalendar value) {
        this.mahnStufeGueltigAbNext = value;
    }

    /**
     * Ruft den Wert der mahnLetzteZahlung-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getMahnLetzteZahlung() {
        return mahnLetzteZahlung;
    }

    /**
     * Legt den Wert der mahnLetzteZahlung-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setMahnLetzteZahlung(XMLGregorianCalendar value) {
        this.mahnLetzteZahlung = value;
    }

    /**
     * Ruft den Wert der mahnAeltesteFaelligkeit-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getMahnAeltesteFaelligkeit() {
        return mahnAeltesteFaelligkeit;
    }

    /**
     * Legt den Wert der mahnAeltesteFaelligkeit-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setMahnAeltesteFaelligkeit(XMLGregorianCalendar value) {
        this.mahnAeltesteFaelligkeit = value;
    }

    /**
     * Ruft den Wert der mahnAnzahlFaelligkeiten-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMahnAnzahlFaelligkeiten() {
        return mahnAnzahlFaelligkeiten;
    }

    /**
     * Legt den Wert der mahnAnzahlFaelligkeiten-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMahnAnzahlFaelligkeiten(Integer value) {
        this.mahnAnzahlFaelligkeiten = value;
    }

    /**
     * Ruft den Wert der grundRuecklaufCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGrundRuecklaufCd() {
        return grundRuecklaufCd;
    }

    /**
     * Legt den Wert der grundRuecklaufCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGrundRuecklaufCd(String value) {
        this.grundRuecklaufCd = value;
    }

    /**
     * Ruft den Wert der mahnDeckungBis-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getMahnDeckungBis() {
        return mahnDeckungBis;
    }

    /**
     * Legt den Wert der mahnDeckungBis-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setMahnDeckungBis(XMLGregorianCalendar value) {
        this.mahnDeckungBis = value;
    }

    /**
     * Ruft den Wert der vtgSparteCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVtgSparteCd() {
        return vtgSparteCd;
    }

    /**
     * Legt den Wert der vtgSparteCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVtgSparteCd(String value) {
        this.vtgSparteCd = value;
    }

}
