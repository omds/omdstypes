
package at.vvo.omds.types.omds2Types.v2_9;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java-Klasse für anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice&gt;
 *         &lt;sequence&gt;
 *           &lt;element ref="{urn:omds20}VERS_UNTERNEHMEN" maxOccurs="unbounded" minOccurs="0"/&gt;
 *           &lt;element ref="{urn:omds20}SCHLUESSELART" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;/sequence&gt;
 *         &lt;sequence&gt;
 *           &lt;element ref="{urn:omds20}KLAUSEL" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;/sequence&gt;
 *         &lt;group ref="{urn:omds20}bestand"/&gt;
 *       &lt;/choice&gt;
 *       &lt;attribute name="VUNr" use="required" type="{urn:omds20}VUNr" /&gt;
 *       &lt;attribute name="MaklerID" use="required"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;minLength value="1"/&gt;
 *             &lt;maxLength value="26"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="PaketZpktErstell" use="required" type="{urn:omds20}Datum-Zeit" /&gt;
 *       &lt;attribute name="PaketZpktLetztErstell" type="{urn:omds20}Datum-Zeit" /&gt;
 *       &lt;attribute name="PaketInhCd" use="required" type="{urn:omds20}PaketInhCd_Type" /&gt;
 *       &lt;attribute name="PaketUmfCd" use="required" type="{urn:omds20}PaketUmfCd_Type" /&gt;
 *       &lt;attribute name="OMDSVersion" use="required"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="12"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="VUVersion" use="required"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="12"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="DVRNrAbs" use="required"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;minLength value="1"/&gt;
 *             &lt;maxLength value="8"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="PaketKommentar"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="80"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "versunternehmen",
    "schluesselart",
    "klausel",
    "loeschanstoss",
    "person",
    "vertrag",
    "schaden",
    "provision",
    "mahnung",
    "vertragsfonds"
})
@XmlRootElement(name = "PAKET")
public class PAKET {

    @XmlElement(name = "VERS_UNTERNEHMEN")
    protected List<VERSUNTERNEHMEN> versunternehmen;
    @XmlElement(name = "SCHLUESSELART")
    protected List<SCHLUESSELART> schluesselart;
    @XmlElement(name = "KLAUSEL")
    protected List<KLAUSELType> klausel;
    @XmlElement(name = "LOESCHANSTOSS")
    protected List<LOESCHANSTOSSType> loeschanstoss;
    @XmlElement(name = "PERSON")
    protected List<PERSONType> person;
    @XmlElement(name = "VERTRAG")
    protected List<VERTRAG> vertrag;
    @XmlElement(name = "SCHADEN")
    protected List<SCHADENType> schaden;
    @XmlElement(name = "PROVISION")
    protected List<PROVISIONType> provision;
    @XmlElement(name = "MAHNUNG")
    protected List<MAHNUNGType> mahnung;
    @XmlElement(name = "VERTRAGSFONDS")
    protected List<VERTRAGSFONDSType> vertragsfonds;
    @XmlAttribute(name = "VUNr", required = true)
    protected String vuNr;
    @XmlAttribute(name = "MaklerID", required = true)
    protected String maklerID;
    @XmlAttribute(name = "PaketZpktErstell", required = true)
    protected XMLGregorianCalendar paketZpktErstell;
    @XmlAttribute(name = "PaketZpktLetztErstell")
    protected XMLGregorianCalendar paketZpktLetztErstell;
    @XmlAttribute(name = "PaketInhCd", required = true)
    protected PaketInhCdType paketInhCd;
    @XmlAttribute(name = "PaketUmfCd", required = true)
    protected PaketUmfCdType paketUmfCd;
    @XmlAttribute(name = "OMDSVersion", required = true)
    protected String omdsVersion;
    @XmlAttribute(name = "VUVersion", required = true)
    protected String vuVersion;
    @XmlAttribute(name = "DVRNrAbs", required = true)
    protected String dvrNrAbs;
    @XmlAttribute(name = "PaketKommentar")
    protected String paketKommentar;

    /**
     * Gets the value of the versunternehmen property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the versunternehmen property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVERSUNTERNEHMEN().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link VERSUNTERNEHMEN }
     * 
     * 
     */
    public List<VERSUNTERNEHMEN> getVERSUNTERNEHMEN() {
        if (versunternehmen == null) {
            versunternehmen = new ArrayList<VERSUNTERNEHMEN>();
        }
        return this.versunternehmen;
    }

    /**
     * Gets the value of the schluesselart property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the schluesselart property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSCHLUESSELART().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SCHLUESSELART }
     * 
     * 
     */
    public List<SCHLUESSELART> getSCHLUESSELART() {
        if (schluesselart == null) {
            schluesselart = new ArrayList<SCHLUESSELART>();
        }
        return this.schluesselart;
    }

    /**
     * Gets the value of the klausel property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the klausel property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getKLAUSEL().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link KLAUSELType }
     * 
     * 
     */
    public List<KLAUSELType> getKLAUSEL() {
        if (klausel == null) {
            klausel = new ArrayList<KLAUSELType>();
        }
        return this.klausel;
    }

    /**
     * Gets the value of the loeschanstoss property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the loeschanstoss property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLOESCHANSTOSS().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LOESCHANSTOSSType }
     * 
     * 
     */
    public List<LOESCHANSTOSSType> getLOESCHANSTOSS() {
        if (loeschanstoss == null) {
            loeschanstoss = new ArrayList<LOESCHANSTOSSType>();
        }
        return this.loeschanstoss;
    }

    /**
     * Gets the value of the person property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the person property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPERSON().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PERSONType }
     * 
     * 
     */
    public List<PERSONType> getPERSON() {
        if (person == null) {
            person = new ArrayList<PERSONType>();
        }
        return this.person;
    }

    /**
     * Gets the value of the vertrag property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the vertrag property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVERTRAG().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link VERTRAG }
     * 
     * 
     */
    public List<VERTRAG> getVERTRAG() {
        if (vertrag == null) {
            vertrag = new ArrayList<VERTRAG>();
        }
        return this.vertrag;
    }

    /**
     * Gets the value of the schaden property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the schaden property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSCHADEN().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SCHADENType }
     * 
     * 
     */
    public List<SCHADENType> getSCHADEN() {
        if (schaden == null) {
            schaden = new ArrayList<SCHADENType>();
        }
        return this.schaden;
    }

    /**
     * Gets the value of the provision property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the provision property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPROVISION().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PROVISIONType }
     * 
     * 
     */
    public List<PROVISIONType> getPROVISION() {
        if (provision == null) {
            provision = new ArrayList<PROVISIONType>();
        }
        return this.provision;
    }

    /**
     * Gets the value of the mahnung property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the mahnung property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMAHNUNG().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MAHNUNGType }
     * 
     * 
     */
    public List<MAHNUNGType> getMAHNUNG() {
        if (mahnung == null) {
            mahnung = new ArrayList<MAHNUNGType>();
        }
        return this.mahnung;
    }

    /**
     * Gets the value of the vertragsfonds property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the vertragsfonds property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVERTRAGSFONDS().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link VERTRAGSFONDSType }
     * 
     * 
     */
    public List<VERTRAGSFONDSType> getVERTRAGSFONDS() {
        if (vertragsfonds == null) {
            vertragsfonds = new ArrayList<VERTRAGSFONDSType>();
        }
        return this.vertragsfonds;
    }

    /**
     * Ruft den Wert der vuNr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVUNr() {
        return vuNr;
    }

    /**
     * Legt den Wert der vuNr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVUNr(String value) {
        this.vuNr = value;
    }

    /**
     * Ruft den Wert der maklerID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMaklerID() {
        return maklerID;
    }

    /**
     * Legt den Wert der maklerID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMaklerID(String value) {
        this.maklerID = value;
    }

    /**
     * Ruft den Wert der paketZpktErstell-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPaketZpktErstell() {
        return paketZpktErstell;
    }

    /**
     * Legt den Wert der paketZpktErstell-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPaketZpktErstell(XMLGregorianCalendar value) {
        this.paketZpktErstell = value;
    }

    /**
     * Ruft den Wert der paketZpktLetztErstell-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPaketZpktLetztErstell() {
        return paketZpktLetztErstell;
    }

    /**
     * Legt den Wert der paketZpktLetztErstell-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPaketZpktLetztErstell(XMLGregorianCalendar value) {
        this.paketZpktLetztErstell = value;
    }

    /**
     * Ruft den Wert der paketInhCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PaketInhCdType }
     *     
     */
    public PaketInhCdType getPaketInhCd() {
        return paketInhCd;
    }

    /**
     * Legt den Wert der paketInhCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PaketInhCdType }
     *     
     */
    public void setPaketInhCd(PaketInhCdType value) {
        this.paketInhCd = value;
    }

    /**
     * Ruft den Wert der paketUmfCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PaketUmfCdType }
     *     
     */
    public PaketUmfCdType getPaketUmfCd() {
        return paketUmfCd;
    }

    /**
     * Legt den Wert der paketUmfCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PaketUmfCdType }
     *     
     */
    public void setPaketUmfCd(PaketUmfCdType value) {
        this.paketUmfCd = value;
    }

    /**
     * Ruft den Wert der omdsVersion-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOMDSVersion() {
        return omdsVersion;
    }

    /**
     * Legt den Wert der omdsVersion-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOMDSVersion(String value) {
        this.omdsVersion = value;
    }

    /**
     * Ruft den Wert der vuVersion-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVUVersion() {
        return vuVersion;
    }

    /**
     * Legt den Wert der vuVersion-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVUVersion(String value) {
        this.vuVersion = value;
    }

    /**
     * Ruft den Wert der dvrNrAbs-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDVRNrAbs() {
        return dvrNrAbs;
    }

    /**
     * Legt den Wert der dvrNrAbs-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDVRNrAbs(String value) {
        this.dvrNrAbs = value;
    }

    /**
     * Ruft den Wert der paketKommentar-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaketKommentar() {
        return paketKommentar;
    }

    /**
     * Legt den Wert der paketKommentar-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaketKommentar(String value) {
        this.paketKommentar = value;
    }

}
