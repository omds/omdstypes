
package at.vvo.omds.types.omds2Types.v2_9;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds20}PORTFOLIO_TYPE"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:omds20}FONDS" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "fonds"
})
@XmlRootElement(name = "PORTFOLIO")
public class PORTFOLIO
    extends PORTFOLIOTYPE
{

    @XmlElement(name = "FONDS")
    protected List<FONDSType> fonds;

    /**
     * Gets the value of the fonds property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fonds property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFONDS().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FONDSType }
     * 
     * 
     */
    public List<FONDSType> getFONDS() {
        if (fonds == null) {
            fonds = new ArrayList<FONDSType>();
        }
        return this.fonds;
    }

}
