
package at.vvo.omds.types.omds2Types.v2_9;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java-Klasse für PROVISION_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="PROVISION_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="ProvisionsID" use="required"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="26"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="Vermnr" use="required" type="{urn:omds20}Vermnr" /&gt;
 *       &lt;attribute name="Polizzennr" type="{urn:omds20}Polizzennr" /&gt;
 *       &lt;attribute name="VertragsID" type="{urn:omds20}VertragsID" /&gt;
 *       &lt;attribute name="SpartenCd" type="{urn:omds20}SpartenCd_Type" /&gt;
 *       &lt;attribute name="SpartenErweiterung"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="10"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="BuchDat" use="required" type="{urn:omds20}Datum" /&gt;
 *       &lt;attribute name="ProvVon" type="{urn:omds20}Datum" /&gt;
 *       &lt;attribute name="ProvBis" type="{urn:omds20}Datum" /&gt;
 *       &lt;attribute name="ProvArtCd" use="required" type="{urn:omds20}ProvArtCd_Type" /&gt;
 *       &lt;attribute name="ProvTxt"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="90"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="ProvGrdlg" type="{urn:omds20}decimal" /&gt;
 *       &lt;attribute name="ProvSatz" type="{urn:omds20}decimal" /&gt;
 *       &lt;attribute name="ProvBetrag" use="required" type="{urn:omds20}decimal" /&gt;
 *       &lt;attribute name="WaehrungsCd" use="required" type="{urn:omds20}WaehrungsCd_Type" /&gt;
 *       &lt;attribute name="ProvArtText"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="90"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="Vorpolizze" type="{urn:omds20}Polizzennr" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PROVISION_Type")
public class PROVISIONType {

    @XmlAttribute(name = "ProvisionsID", required = true)
    protected String provisionsID;
    @XmlAttribute(name = "Vermnr", required = true)
    protected String vermnr;
    @XmlAttribute(name = "Polizzennr")
    protected String polizzennr;
    @XmlAttribute(name = "VertragsID")
    protected String vertragsID;
    @XmlAttribute(name = "SpartenCd")
    protected String spartenCd;
    @XmlAttribute(name = "SpartenErweiterung")
    protected String spartenErweiterung;
    @XmlAttribute(name = "BuchDat", required = true)
    protected XMLGregorianCalendar buchDat;
    @XmlAttribute(name = "ProvVon")
    protected XMLGregorianCalendar provVon;
    @XmlAttribute(name = "ProvBis")
    protected XMLGregorianCalendar provBis;
    @XmlAttribute(name = "ProvArtCd", required = true)
    protected String provArtCd;
    @XmlAttribute(name = "ProvTxt")
    protected String provTxt;
    @XmlAttribute(name = "ProvGrdlg")
    protected BigDecimal provGrdlg;
    @XmlAttribute(name = "ProvSatz")
    protected BigDecimal provSatz;
    @XmlAttribute(name = "ProvBetrag", required = true)
    protected BigDecimal provBetrag;
    @XmlAttribute(name = "WaehrungsCd", required = true)
    protected WaehrungsCdType waehrungsCd;
    @XmlAttribute(name = "ProvArtText")
    protected String provArtText;
    @XmlAttribute(name = "Vorpolizze")
    protected String vorpolizze;

    /**
     * Ruft den Wert der provisionsID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProvisionsID() {
        return provisionsID;
    }

    /**
     * Legt den Wert der provisionsID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProvisionsID(String value) {
        this.provisionsID = value;
    }

    /**
     * Ruft den Wert der vermnr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVermnr() {
        return vermnr;
    }

    /**
     * Legt den Wert der vermnr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVermnr(String value) {
        this.vermnr = value;
    }

    /**
     * Ruft den Wert der polizzennr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolizzennr() {
        return polizzennr;
    }

    /**
     * Legt den Wert der polizzennr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolizzennr(String value) {
        this.polizzennr = value;
    }

    /**
     * Ruft den Wert der vertragsID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVertragsID() {
        return vertragsID;
    }

    /**
     * Legt den Wert der vertragsID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVertragsID(String value) {
        this.vertragsID = value;
    }

    /**
     * Ruft den Wert der spartenCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSpartenCd() {
        return spartenCd;
    }

    /**
     * Legt den Wert der spartenCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSpartenCd(String value) {
        this.spartenCd = value;
    }

    /**
     * Ruft den Wert der spartenErweiterung-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSpartenErweiterung() {
        return spartenErweiterung;
    }

    /**
     * Legt den Wert der spartenErweiterung-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSpartenErweiterung(String value) {
        this.spartenErweiterung = value;
    }

    /**
     * Ruft den Wert der buchDat-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getBuchDat() {
        return buchDat;
    }

    /**
     * Legt den Wert der buchDat-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setBuchDat(XMLGregorianCalendar value) {
        this.buchDat = value;
    }

    /**
     * Ruft den Wert der provVon-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getProvVon() {
        return provVon;
    }

    /**
     * Legt den Wert der provVon-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setProvVon(XMLGregorianCalendar value) {
        this.provVon = value;
    }

    /**
     * Ruft den Wert der provBis-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getProvBis() {
        return provBis;
    }

    /**
     * Legt den Wert der provBis-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setProvBis(XMLGregorianCalendar value) {
        this.provBis = value;
    }

    /**
     * Ruft den Wert der provArtCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProvArtCd() {
        return provArtCd;
    }

    /**
     * Legt den Wert der provArtCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProvArtCd(String value) {
        this.provArtCd = value;
    }

    /**
     * Ruft den Wert der provTxt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProvTxt() {
        return provTxt;
    }

    /**
     * Legt den Wert der provTxt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProvTxt(String value) {
        this.provTxt = value;
    }

    /**
     * Ruft den Wert der provGrdlg-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getProvGrdlg() {
        return provGrdlg;
    }

    /**
     * Legt den Wert der provGrdlg-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setProvGrdlg(BigDecimal value) {
        this.provGrdlg = value;
    }

    /**
     * Ruft den Wert der provSatz-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getProvSatz() {
        return provSatz;
    }

    /**
     * Legt den Wert der provSatz-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setProvSatz(BigDecimal value) {
        this.provSatz = value;
    }

    /**
     * Ruft den Wert der provBetrag-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getProvBetrag() {
        return provBetrag;
    }

    /**
     * Legt den Wert der provBetrag-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setProvBetrag(BigDecimal value) {
        this.provBetrag = value;
    }

    /**
     * Ruft den Wert der waehrungsCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link WaehrungsCdType }
     *     
     */
    public WaehrungsCdType getWaehrungsCd() {
        return waehrungsCd;
    }

    /**
     * Legt den Wert der waehrungsCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link WaehrungsCdType }
     *     
     */
    public void setWaehrungsCd(WaehrungsCdType value) {
        this.waehrungsCd = value;
    }

    /**
     * Ruft den Wert der provArtText-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProvArtText() {
        return provArtText;
    }

    /**
     * Legt den Wert der provArtText-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProvArtText(String value) {
        this.provArtText = value;
    }

    /**
     * Ruft den Wert der vorpolizze-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVorpolizze() {
        return vorpolizze;
    }

    /**
     * Legt den Wert der vorpolizze-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVorpolizze(String value) {
        this.vorpolizze = value;
    }

}
