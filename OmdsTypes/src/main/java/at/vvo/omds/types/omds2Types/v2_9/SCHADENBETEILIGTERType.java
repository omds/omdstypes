
package at.vvo.omds.types.omds2Types.v2_9;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für SCHADEN_BETEILIGTER_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SCHADEN_BETEILIGTER_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence minOccurs="0"&gt;
 *         &lt;element ref="{urn:omds20}GESCHAEDIGTES_OBJEKT" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:omds20}ZAHLUNG" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="BetLfnr" use="required" type="{http://www.w3.org/2001/XMLSchema}unsignedShort" /&gt;
 *       &lt;attribute name="Personennr" type="{urn:omds20}Personennr" /&gt;
 *       &lt;attribute name="BetRolleCd" use="required" type="{urn:omds20}BetRolleCd_Type" /&gt;
 *       &lt;attribute name="BetTxt"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="100"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SCHADEN_BETEILIGTER_Type", propOrder = {
    "geschaedigtesobjekt",
    "zahlung"
})
public class SCHADENBETEILIGTERType {

    @XmlElement(name = "GESCHAEDIGTES_OBJEKT")
    protected List<GESCHAEDIGTESOBJEKTType> geschaedigtesobjekt;
    @XmlElement(name = "ZAHLUNG")
    protected List<ZAHLUNGType> zahlung;
    @XmlAttribute(name = "BetLfnr", required = true)
    @XmlSchemaType(name = "unsignedShort")
    protected int betLfnr;
    @XmlAttribute(name = "Personennr")
    protected String personennr;
    @XmlAttribute(name = "BetRolleCd", required = true)
    protected String betRolleCd;
    @XmlAttribute(name = "BetTxt")
    protected String betTxt;

    /**
     * Gets the value of the geschaedigtesobjekt property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the geschaedigtesobjekt property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGESCHAEDIGTESOBJEKT().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GESCHAEDIGTESOBJEKTType }
     * 
     * 
     */
    public List<GESCHAEDIGTESOBJEKTType> getGESCHAEDIGTESOBJEKT() {
        if (geschaedigtesobjekt == null) {
            geschaedigtesobjekt = new ArrayList<GESCHAEDIGTESOBJEKTType>();
        }
        return this.geschaedigtesobjekt;
    }

    /**
     * Gets the value of the zahlung property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the zahlung property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getZAHLUNG().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ZAHLUNGType }
     * 
     * 
     */
    public List<ZAHLUNGType> getZAHLUNG() {
        if (zahlung == null) {
            zahlung = new ArrayList<ZAHLUNGType>();
        }
        return this.zahlung;
    }

    /**
     * Ruft den Wert der betLfnr-Eigenschaft ab.
     * 
     */
    public int getBetLfnr() {
        return betLfnr;
    }

    /**
     * Legt den Wert der betLfnr-Eigenschaft fest.
     * 
     */
    public void setBetLfnr(int value) {
        this.betLfnr = value;
    }

    /**
     * Ruft den Wert der personennr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPersonennr() {
        return personennr;
    }

    /**
     * Legt den Wert der personennr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPersonennr(String value) {
        this.personennr = value;
    }

    /**
     * Ruft den Wert der betRolleCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBetRolleCd() {
        return betRolleCd;
    }

    /**
     * Legt den Wert der betRolleCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBetRolleCd(String value) {
        this.betRolleCd = value;
    }

    /**
     * Ruft den Wert der betTxt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBetTxt() {
        return betTxt;
    }

    /**
     * Legt den Wert der betTxt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBetTxt(String value) {
        this.betTxt = value;
    }

}
