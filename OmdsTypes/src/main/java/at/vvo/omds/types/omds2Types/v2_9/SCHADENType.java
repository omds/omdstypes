
package at.vvo.omds.types.omds2Types.v2_9;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java-Klasse für SCHADEN_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SCHADEN_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence minOccurs="0"&gt;
 *         &lt;choice maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;element ref="{urn:omds20}EL-Anzahl"/&gt;
 *           &lt;element ref="{urn:omds20}EL-Betrag"/&gt;
 *           &lt;element ref="{urn:omds20}EL-Einstufung"/&gt;
 *           &lt;element ref="{urn:omds20}EL-Entscheidungsfrage"/&gt;
 *           &lt;element ref="{urn:omds20}EL-Polizzennummer"/&gt;
 *           &lt;element ref="{urn:omds20}EL-Text"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element ref="{urn:omds20}SCHADEN_BETEILIGTER" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="Schadennr" use="required" type="{urn:omds20}Schadennr" /&gt;
 *       &lt;attribute name="Polizzennr" use="required" type="{urn:omds20}Polizzennr" /&gt;
 *       &lt;attribute name="VertragsID" type="{urn:omds20}VertragsID" /&gt;
 *       &lt;attribute name="Vermnr" type="{urn:omds20}Vermnr" /&gt;
 *       &lt;attribute name="SpartenCd" use="required" type="{urn:omds20}SpartenCd_Type" /&gt;
 *       &lt;attribute name="SpartenErweiterung" use="required"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="10"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="Ereigniszpkt" use="required" type="{urn:omds20}Datum-Zeit" /&gt;
 *       &lt;attribute name="Meldedat" use="required" type="{urn:omds20}Datum" /&gt;
 *       &lt;attribute name="SchadUrsCd" type="{urn:omds20}SchadUrsCd_Type" /&gt;
 *       &lt;attribute name="SchadUrsTxt"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="40"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="VerschuldenCd" type="{urn:omds20}VerschuldenCd_Type" /&gt;
 *       &lt;attribute name="MalusWirksamKz" type="{urn:omds20}Entsch2_Type" /&gt;
 *       &lt;attribute name="BearbStandCd" use="required" type="{urn:omds20}BearbStandCd_Type" /&gt;
 *       &lt;attribute name="ErledDat" type="{urn:omds20}Datum" /&gt;
 *       &lt;attribute name="SachbearbVU"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="30"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="LeistungGeschaetzt" type="{urn:omds20}decimal" /&gt;
 *       &lt;attribute name="WaehrungsCd" use="required" type="{urn:omds20}WaehrungsCd_Type" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SCHADEN_Type", propOrder = {
    "elAnzahlOrELBetragOrELEinstufung",
    "schadenbeteiligter"
})
public class SCHADENType {

    @XmlElements({
        @XmlElement(name = "EL-Anzahl", type = ELAnzahlType.class),
        @XmlElement(name = "EL-Betrag", type = ELBetragType.class),
        @XmlElement(name = "EL-Einstufung", type = ELEinstufungType.class),
        @XmlElement(name = "EL-Entscheidungsfrage", type = ELEntscheidungsfrageType.class),
        @XmlElement(name = "EL-Polizzennummer", type = ELPolizzennummerType.class),
        @XmlElement(name = "EL-Text", type = ELTextType.class)
    })
    protected List<Object> elAnzahlOrELBetragOrELEinstufung;
    @XmlElement(name = "SCHADEN_BETEILIGTER")
    protected List<SCHADENBETEILIGTERType> schadenbeteiligter;
    @XmlAttribute(name = "Schadennr", required = true)
    protected String schadennr;
    @XmlAttribute(name = "Polizzennr", required = true)
    protected String polizzennr;
    @XmlAttribute(name = "VertragsID")
    protected String vertragsID;
    @XmlAttribute(name = "Vermnr")
    protected String vermnr;
    @XmlAttribute(name = "SpartenCd", required = true)
    protected String spartenCd;
    @XmlAttribute(name = "SpartenErweiterung", required = true)
    protected String spartenErweiterung;
    @XmlAttribute(name = "Ereigniszpkt", required = true)
    protected XMLGregorianCalendar ereigniszpkt;
    @XmlAttribute(name = "Meldedat", required = true)
    protected XMLGregorianCalendar meldedat;
    @XmlAttribute(name = "SchadUrsCd")
    protected String schadUrsCd;
    @XmlAttribute(name = "SchadUrsTxt")
    protected String schadUrsTxt;
    @XmlAttribute(name = "VerschuldenCd")
    protected String verschuldenCd;
    @XmlAttribute(name = "MalusWirksamKz")
    protected Entsch2Type malusWirksamKz;
    @XmlAttribute(name = "BearbStandCd", required = true)
    protected String bearbStandCd;
    @XmlAttribute(name = "ErledDat")
    protected XMLGregorianCalendar erledDat;
    @XmlAttribute(name = "SachbearbVU")
    protected String sachbearbVU;
    @XmlAttribute(name = "LeistungGeschaetzt")
    protected BigDecimal leistungGeschaetzt;
    @XmlAttribute(name = "WaehrungsCd", required = true)
    protected WaehrungsCdType waehrungsCd;

    /**
     * Gets the value of the elAnzahlOrELBetragOrELEinstufung property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the elAnzahlOrELBetragOrELEinstufung property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getELAnzahlOrELBetragOrELEinstufung().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ELAnzahlType }
     * {@link ELBetragType }
     * {@link ELEinstufungType }
     * {@link ELEntscheidungsfrageType }
     * {@link ELPolizzennummerType }
     * {@link ELTextType }
     * 
     * 
     */
    public List<Object> getELAnzahlOrELBetragOrELEinstufung() {
        if (elAnzahlOrELBetragOrELEinstufung == null) {
            elAnzahlOrELBetragOrELEinstufung = new ArrayList<Object>();
        }
        return this.elAnzahlOrELBetragOrELEinstufung;
    }

    /**
     * Gets the value of the schadenbeteiligter property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the schadenbeteiligter property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSCHADENBETEILIGTER().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SCHADENBETEILIGTERType }
     * 
     * 
     */
    public List<SCHADENBETEILIGTERType> getSCHADENBETEILIGTER() {
        if (schadenbeteiligter == null) {
            schadenbeteiligter = new ArrayList<SCHADENBETEILIGTERType>();
        }
        return this.schadenbeteiligter;
    }

    /**
     * Ruft den Wert der schadennr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSchadennr() {
        return schadennr;
    }

    /**
     * Legt den Wert der schadennr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSchadennr(String value) {
        this.schadennr = value;
    }

    /**
     * Ruft den Wert der polizzennr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolizzennr() {
        return polizzennr;
    }

    /**
     * Legt den Wert der polizzennr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolizzennr(String value) {
        this.polizzennr = value;
    }

    /**
     * Ruft den Wert der vertragsID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVertragsID() {
        return vertragsID;
    }

    /**
     * Legt den Wert der vertragsID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVertragsID(String value) {
        this.vertragsID = value;
    }

    /**
     * Ruft den Wert der vermnr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVermnr() {
        return vermnr;
    }

    /**
     * Legt den Wert der vermnr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVermnr(String value) {
        this.vermnr = value;
    }

    /**
     * Ruft den Wert der spartenCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSpartenCd() {
        return spartenCd;
    }

    /**
     * Legt den Wert der spartenCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSpartenCd(String value) {
        this.spartenCd = value;
    }

    /**
     * Ruft den Wert der spartenErweiterung-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSpartenErweiterung() {
        return spartenErweiterung;
    }

    /**
     * Legt den Wert der spartenErweiterung-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSpartenErweiterung(String value) {
        this.spartenErweiterung = value;
    }

    /**
     * Ruft den Wert der ereigniszpkt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEreigniszpkt() {
        return ereigniszpkt;
    }

    /**
     * Legt den Wert der ereigniszpkt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEreigniszpkt(XMLGregorianCalendar value) {
        this.ereigniszpkt = value;
    }

    /**
     * Ruft den Wert der meldedat-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getMeldedat() {
        return meldedat;
    }

    /**
     * Legt den Wert der meldedat-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setMeldedat(XMLGregorianCalendar value) {
        this.meldedat = value;
    }

    /**
     * Ruft den Wert der schadUrsCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSchadUrsCd() {
        return schadUrsCd;
    }

    /**
     * Legt den Wert der schadUrsCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSchadUrsCd(String value) {
        this.schadUrsCd = value;
    }

    /**
     * Ruft den Wert der schadUrsTxt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSchadUrsTxt() {
        return schadUrsTxt;
    }

    /**
     * Legt den Wert der schadUrsTxt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSchadUrsTxt(String value) {
        this.schadUrsTxt = value;
    }

    /**
     * Ruft den Wert der verschuldenCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVerschuldenCd() {
        return verschuldenCd;
    }

    /**
     * Legt den Wert der verschuldenCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVerschuldenCd(String value) {
        this.verschuldenCd = value;
    }

    /**
     * Ruft den Wert der malusWirksamKz-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Entsch2Type }
     *     
     */
    public Entsch2Type getMalusWirksamKz() {
        return malusWirksamKz;
    }

    /**
     * Legt den Wert der malusWirksamKz-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Entsch2Type }
     *     
     */
    public void setMalusWirksamKz(Entsch2Type value) {
        this.malusWirksamKz = value;
    }

    /**
     * Ruft den Wert der bearbStandCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBearbStandCd() {
        return bearbStandCd;
    }

    /**
     * Legt den Wert der bearbStandCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBearbStandCd(String value) {
        this.bearbStandCd = value;
    }

    /**
     * Ruft den Wert der erledDat-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getErledDat() {
        return erledDat;
    }

    /**
     * Legt den Wert der erledDat-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setErledDat(XMLGregorianCalendar value) {
        this.erledDat = value;
    }

    /**
     * Ruft den Wert der sachbearbVU-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSachbearbVU() {
        return sachbearbVU;
    }

    /**
     * Legt den Wert der sachbearbVU-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSachbearbVU(String value) {
        this.sachbearbVU = value;
    }

    /**
     * Ruft den Wert der leistungGeschaetzt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getLeistungGeschaetzt() {
        return leistungGeschaetzt;
    }

    /**
     * Legt den Wert der leistungGeschaetzt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setLeistungGeschaetzt(BigDecimal value) {
        this.leistungGeschaetzt = value;
    }

    /**
     * Ruft den Wert der waehrungsCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link WaehrungsCdType }
     *     
     */
    public WaehrungsCdType getWaehrungsCd() {
        return waehrungsCd;
    }

    /**
     * Legt den Wert der waehrungsCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link WaehrungsCdType }
     *     
     */
    public void setWaehrungsCd(WaehrungsCdType value) {
        this.waehrungsCd = value;
    }

}
