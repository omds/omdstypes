
package at.vvo.omds.types.omds2Types.v2_9;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence maxOccurs="unbounded"&gt;
 *         &lt;element ref="{urn:omds20}SCHLUESSEL"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="SchlArtCd" use="required" type="{urn:omds20}SchlArtCd_Type" /&gt;
 *       &lt;attribute name="VUWertErlaubtKz" use="required" type="{urn:omds20}Entsch2_Type" /&gt;
 *       &lt;attribute name="SchlArtBez"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="30"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "schluessel"
})
@XmlRootElement(name = "SCHLUESSELART")
public class SCHLUESSELART {

    @XmlElement(name = "SCHLUESSEL", required = true)
    protected List<SCHLUESSEL> schluessel;
    @XmlAttribute(name = "SchlArtCd", required = true)
    protected SchlArtCdType schlArtCd;
    @XmlAttribute(name = "VUWertErlaubtKz", required = true)
    protected Entsch2Type vuWertErlaubtKz;
    @XmlAttribute(name = "SchlArtBez")
    protected String schlArtBez;

    /**
     * Gets the value of the schluessel property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the schluessel property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSCHLUESSEL().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SCHLUESSEL }
     * 
     * 
     */
    public List<SCHLUESSEL> getSCHLUESSEL() {
        if (schluessel == null) {
            schluessel = new ArrayList<SCHLUESSEL>();
        }
        return this.schluessel;
    }

    /**
     * Ruft den Wert der schlArtCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SchlArtCdType }
     *     
     */
    public SchlArtCdType getSchlArtCd() {
        return schlArtCd;
    }

    /**
     * Legt den Wert der schlArtCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SchlArtCdType }
     *     
     */
    public void setSchlArtCd(SchlArtCdType value) {
        this.schlArtCd = value;
    }

    /**
     * Ruft den Wert der vuWertErlaubtKz-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Entsch2Type }
     *     
     */
    public Entsch2Type getVUWertErlaubtKz() {
        return vuWertErlaubtKz;
    }

    /**
     * Legt den Wert der vuWertErlaubtKz-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Entsch2Type }
     *     
     */
    public void setVUWertErlaubtKz(Entsch2Type value) {
        this.vuWertErlaubtKz = value;
    }

    /**
     * Ruft den Wert der schlArtBez-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSchlArtBez() {
        return schlArtBez;
    }

    /**
     * Legt den Wert der schlArtBez-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSchlArtBez(String value) {
        this.schlArtBez = value;
    }

}
