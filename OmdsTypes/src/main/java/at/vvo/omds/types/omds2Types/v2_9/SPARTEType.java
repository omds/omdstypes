
package at.vvo.omds.types.omds2Types.v2_9;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java-Klasse für SPARTE_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SPARTE_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;element ref="{urn:omds20}EL-Anzahl"/&gt;
 *           &lt;element ref="{urn:omds20}EL-Betrag"/&gt;
 *           &lt;element ref="{urn:omds20}EL-Bezugsberechtigung"/&gt;
 *           &lt;element ref="{urn:omds20}EL-Einstufung"/&gt;
 *           &lt;element ref="{urn:omds20}EL-Entscheidungsfrage"/&gt;
 *           &lt;element ref="{urn:omds20}EL-Gewinnbeteiligung"/&gt;
 *           &lt;element ref="{urn:omds20}EL-Grenzwert"/&gt;
 *           &lt;element ref="{urn:omds20}EL-Index"/&gt;
 *           &lt;element ref="{urn:omds20}EL-Klausel"/&gt;
 *           &lt;element ref="{urn:omds20}EL-Praemienkorrektur"/&gt;
 *           &lt;element ref="{urn:omds20}EL-Rente"/&gt;
 *           &lt;element ref="{urn:omds20}EL-Selbstbehalt"/&gt;
 *           &lt;element ref="{urn:omds20}EL-Steuer"/&gt;
 *           &lt;element ref="{urn:omds20}EL-Text"/&gt;
 *           &lt;element ref="{urn:omds20}EL-Zeitraum"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element ref="{urn:omds20}RISIKO" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="SpartenCd" use="required" type="{urn:omds20}SpartenCd_Type" /&gt;
 *       &lt;attribute name="SpartenErweiterung" use="required"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="10"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="SpartenID"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="40"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="VerbandSparteCd" use="required" type="{urn:omds20}VerbandSparteCd_Type" /&gt;
 *       &lt;attribute name="Spartentxt" use="required"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="80"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="SpartenBeg" type="{urn:omds20}Datum" /&gt;
 *       &lt;attribute name="SpartenEnd" type="{urn:omds20}Datum" /&gt;
 *       &lt;attribute name="HauptTarifBez"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="25"/&gt;
 *             &lt;minLength value="1"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="SichergKz" type="{urn:omds20}Entsch2_Type" /&gt;
 *       &lt;attribute name="DirBeteiligtKz" type="{urn:omds20}Entsch3_Type" /&gt;
 *       &lt;attribute name="SondervereinbarungKz" type="{urn:omds20}Entsch3_Type" /&gt;
 *       &lt;attribute name="PraemieNtoSp" use="required" type="{urn:omds20}decimal" /&gt;
 *       &lt;attribute name="PraemieBtoSp" use="required" type="{urn:omds20}decimal" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SPARTE_Type", propOrder = {
    "elAnzahlOrELBetragOrELBezugsberechtigung",
    "risiko"
})
public class SPARTEType {

    @XmlElements({
        @XmlElement(name = "EL-Anzahl", type = ELAnzahlType.class),
        @XmlElement(name = "EL-Betrag", type = ELBetragType.class),
        @XmlElement(name = "EL-Bezugsberechtigung", type = ELBezugsberechtigungType.class),
        @XmlElement(name = "EL-Einstufung", type = ELEinstufungType.class),
        @XmlElement(name = "EL-Entscheidungsfrage", type = ELEntscheidungsfrageType.class),
        @XmlElement(name = "EL-Gewinnbeteiligung", type = ELGewinnbeteiligungType.class),
        @XmlElement(name = "EL-Grenzwert", type = ELGrenzwertType.class),
        @XmlElement(name = "EL-Index", type = ELIndexType.class),
        @XmlElement(name = "EL-Klausel", type = ELKlauselType.class),
        @XmlElement(name = "EL-Praemienkorrektur", type = ELPraemienkorrekturType.class),
        @XmlElement(name = "EL-Rente", type = ELRenteType.class),
        @XmlElement(name = "EL-Selbstbehalt", type = ELSelbstbehalt.class),
        @XmlElement(name = "EL-Steuer", type = ELSteuerType.class),
        @XmlElement(name = "EL-Text", type = ELTextType.class),
        @XmlElement(name = "EL-Zeitraum", type = ELZeitraumType.class)
    })
    protected List<Object> elAnzahlOrELBetragOrELBezugsberechtigung;
    @XmlElement(name = "RISIKO", required = true)
    protected List<RISIKOType> risiko;
    @XmlAttribute(name = "SpartenCd", required = true)
    protected String spartenCd;
    @XmlAttribute(name = "SpartenErweiterung", required = true)
    protected String spartenErweiterung;
    @XmlAttribute(name = "SpartenID")
    protected String spartenID;
    @XmlAttribute(name = "VerbandSparteCd", required = true)
    protected String verbandSparteCd;
    @XmlAttribute(name = "Spartentxt", required = true)
    protected String spartentxt;
    @XmlAttribute(name = "SpartenBeg")
    protected XMLGregorianCalendar spartenBeg;
    @XmlAttribute(name = "SpartenEnd")
    protected XMLGregorianCalendar spartenEnd;
    @XmlAttribute(name = "HauptTarifBez")
    protected String hauptTarifBez;
    @XmlAttribute(name = "SichergKz")
    protected Entsch2Type sichergKz;
    @XmlAttribute(name = "DirBeteiligtKz")
    protected String dirBeteiligtKz;
    @XmlAttribute(name = "SondervereinbarungKz")
    protected String sondervereinbarungKz;
    @XmlAttribute(name = "PraemieNtoSp", required = true)
    protected BigDecimal praemieNtoSp;
    @XmlAttribute(name = "PraemieBtoSp", required = true)
    protected BigDecimal praemieBtoSp;

    /**
     * Gets the value of the elAnzahlOrELBetragOrELBezugsberechtigung property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the elAnzahlOrELBetragOrELBezugsberechtigung property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getELAnzahlOrELBetragOrELBezugsberechtigung().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ELAnzahlType }
     * {@link ELBetragType }
     * {@link ELBezugsberechtigungType }
     * {@link ELEinstufungType }
     * {@link ELEntscheidungsfrageType }
     * {@link ELGewinnbeteiligungType }
     * {@link ELGrenzwertType }
     * {@link ELIndexType }
     * {@link ELKlauselType }
     * {@link ELPraemienkorrekturType }
     * {@link ELRenteType }
     * {@link ELSelbstbehalt }
     * {@link ELSteuerType }
     * {@link ELTextType }
     * {@link ELZeitraumType }
     * 
     * 
     */
    public List<Object> getELAnzahlOrELBetragOrELBezugsberechtigung() {
        if (elAnzahlOrELBetragOrELBezugsberechtigung == null) {
            elAnzahlOrELBetragOrELBezugsberechtigung = new ArrayList<Object>();
        }
        return this.elAnzahlOrELBetragOrELBezugsberechtigung;
    }

    /**
     * Gets the value of the risiko property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the risiko property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRISIKO().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RISIKOType }
     * 
     * 
     */
    public List<RISIKOType> getRISIKO() {
        if (risiko == null) {
            risiko = new ArrayList<RISIKOType>();
        }
        return this.risiko;
    }

    /**
     * Ruft den Wert der spartenCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSpartenCd() {
        return spartenCd;
    }

    /**
     * Legt den Wert der spartenCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSpartenCd(String value) {
        this.spartenCd = value;
    }

    /**
     * Ruft den Wert der spartenErweiterung-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSpartenErweiterung() {
        return spartenErweiterung;
    }

    /**
     * Legt den Wert der spartenErweiterung-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSpartenErweiterung(String value) {
        this.spartenErweiterung = value;
    }

    /**
     * Ruft den Wert der spartenID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSpartenID() {
        return spartenID;
    }

    /**
     * Legt den Wert der spartenID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSpartenID(String value) {
        this.spartenID = value;
    }

    /**
     * Ruft den Wert der verbandSparteCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVerbandSparteCd() {
        return verbandSparteCd;
    }

    /**
     * Legt den Wert der verbandSparteCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVerbandSparteCd(String value) {
        this.verbandSparteCd = value;
    }

    /**
     * Ruft den Wert der spartentxt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSpartentxt() {
        return spartentxt;
    }

    /**
     * Legt den Wert der spartentxt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSpartentxt(String value) {
        this.spartentxt = value;
    }

    /**
     * Ruft den Wert der spartenBeg-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSpartenBeg() {
        return spartenBeg;
    }

    /**
     * Legt den Wert der spartenBeg-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSpartenBeg(XMLGregorianCalendar value) {
        this.spartenBeg = value;
    }

    /**
     * Ruft den Wert der spartenEnd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSpartenEnd() {
        return spartenEnd;
    }

    /**
     * Legt den Wert der spartenEnd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSpartenEnd(XMLGregorianCalendar value) {
        this.spartenEnd = value;
    }

    /**
     * Ruft den Wert der hauptTarifBez-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHauptTarifBez() {
        return hauptTarifBez;
    }

    /**
     * Legt den Wert der hauptTarifBez-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHauptTarifBez(String value) {
        this.hauptTarifBez = value;
    }

    /**
     * Ruft den Wert der sichergKz-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Entsch2Type }
     *     
     */
    public Entsch2Type getSichergKz() {
        return sichergKz;
    }

    /**
     * Legt den Wert der sichergKz-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Entsch2Type }
     *     
     */
    public void setSichergKz(Entsch2Type value) {
        this.sichergKz = value;
    }

    /**
     * Ruft den Wert der dirBeteiligtKz-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDirBeteiligtKz() {
        return dirBeteiligtKz;
    }

    /**
     * Legt den Wert der dirBeteiligtKz-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDirBeteiligtKz(String value) {
        this.dirBeteiligtKz = value;
    }

    /**
     * Ruft den Wert der sondervereinbarungKz-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSondervereinbarungKz() {
        return sondervereinbarungKz;
    }

    /**
     * Legt den Wert der sondervereinbarungKz-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSondervereinbarungKz(String value) {
        this.sondervereinbarungKz = value;
    }

    /**
     * Ruft den Wert der praemieNtoSp-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPraemieNtoSp() {
        return praemieNtoSp;
    }

    /**
     * Legt den Wert der praemieNtoSp-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPraemieNtoSp(BigDecimal value) {
        this.praemieNtoSp = value;
    }

    /**
     * Ruft den Wert der praemieBtoSp-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPraemieBtoSp() {
        return praemieBtoSp;
    }

    /**
     * Legt den Wert der praemieBtoSp-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPraemieBtoSp(BigDecimal value) {
        this.praemieBtoSp = value;
    }

}
