
package at.vvo.omds.types.omds2Types.v2_9;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für SbhArtCd_Type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="SbhArtCd_Type"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="MIS"/&gt;
 *     &lt;enumeration value="MXK"/&gt;
 *     &lt;enumeration value="MXS"/&gt;
 *     &lt;enumeration value="STS"/&gt;
 *     &lt;enumeration value="SSO"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "SbhArtCd_Type")
@XmlEnum
public enum SbhArtCdType {


    /**
     * Mindestselbstbehalt je Schadenfall
     * 
     */
    MIS,

    /**
     * Maximalselbstbehalt kumuliert
     * 
     */
    MXK,

    /**
     * Maximalselbstbehalt je Schadenfall
     * 
     */
    MXS,

    /**
     * Standardselbstbehalt je Schadenfall
     * 
     */
    STS,

    /**
     * Selbstbehalt sonst
     * 
     */
    SSO;

    public String value() {
        return name();
    }

    public static SbhArtCdType fromValue(String v) {
        return valueOf(v);
    }

}
