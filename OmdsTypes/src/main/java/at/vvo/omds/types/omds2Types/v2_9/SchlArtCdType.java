
package at.vvo.omds.types.omds2Types.v2_9;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für SchlArtCd_Type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="SchlArtCd_Type"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;minLength value="1"/&gt;
 *     &lt;maxLength value="25"/&gt;
 *     &lt;enumeration value="AendGrundCd"/&gt;
 *     &lt;enumeration value="AntriebsArtCd"/&gt;
 *     &lt;enumeration value="AusstattungCd"/&gt;
 *     &lt;enumeration value="BauartCd"/&gt;
 *     &lt;enumeration value="BBArtCd"/&gt;
 *     &lt;enumeration value="BearbStandCd"/&gt;
 *     &lt;enumeration value="BetragArtCd"/&gt;
 *     &lt;enumeration value="BetRolleCd"/&gt;
 *     &lt;enumeration value="DachungCd"/&gt;
 *     &lt;enumeration value="EFrageCd"/&gt;
 *     &lt;enumeration value="EstArtCd"/&gt;
 *     &lt;enumeration value="FamilienstandCd"/&gt;
 *     &lt;enumeration value="FzgArtCd"/&gt;
 *     &lt;enumeration value="GebaeudeArtCd"/&gt;
 *     &lt;enumeration value="GebaeudeHoeheCd"/&gt;
 *     &lt;enumeration value="GeschlechtCd"/&gt;
 *     &lt;enumeration value="GrundRuecklaufCd"/&gt;
 *     &lt;enumeration value="GrwArtCd"/&gt;
 *     &lt;enumeration value="IdfArtCd"/&gt;
 *     &lt;enumeration value="IndexArtCd"/&gt;
 *     &lt;enumeration value="KomArtCd"/&gt;
 *     &lt;enumeration value="LandesCd"/&gt;
 *     &lt;enumeration value="LegArtCd"/&gt;
 *     &lt;enumeration value="LoeschCd"/&gt;
 *     &lt;enumeration value="NutzungCd"/&gt;
 *     &lt;enumeration value="ObjektdatenCd"/&gt;
 *     &lt;enumeration value="PaketInhCd"/&gt;
 *     &lt;enumeration value="PaketUmfCd"/&gt;
 *     &lt;enumeration value="PersArtCd"/&gt;
 *     &lt;enumeration value="PfrArtCd"/&gt;
 *     &lt;enumeration value="PolArtCd"/&gt;
 *     &lt;enumeration value="PraemFristCd"/&gt;
 *     &lt;enumeration value="PraemKorrArtCd"/&gt;
 *     &lt;enumeration value="ProvArtCd"/&gt;
 *     &lt;enumeration value="RisikoArtCd"/&gt;
 *     &lt;enumeration value="RntRhythmCd"/&gt;
 *     &lt;enumeration value="SbhArtCd"/&gt;
 *     &lt;enumeration value="SchadUrsCd"/&gt;
 *     &lt;enumeration value="SonstPersArtCd"/&gt;
 *     &lt;enumeration value="SpartenCd"/&gt;
 *     &lt;enumeration value="StArtCd"/&gt;
 *     &lt;enumeration value="TxtArtCd"/&gt;
 *     &lt;enumeration value="VSArtCd"/&gt;
 *     &lt;enumeration value="VerbandSparteCd"/&gt;
 *     &lt;enumeration value="VersSacheCd"/&gt;
 *     &lt;enumeration value="VerschuldenCd"/&gt;
 *     &lt;enumeration value="VerwendzweckCd"/&gt;
 *     &lt;enumeration value="VtgProdCd"/&gt;
 *     &lt;enumeration value="VtgRolleCd"/&gt;
 *     &lt;enumeration value="VtgSparteCd"/&gt;
 *     &lt;enumeration value="VtgStatusCd"/&gt;
 *     &lt;enumeration value="WaehrungsCd"/&gt;
 *     &lt;enumeration value="ZRArtCd"/&gt;
 *     &lt;enumeration value="ZahlGrundCd"/&gt;
 *     &lt;enumeration value="ZahlRhythmCd"/&gt;
 *     &lt;enumeration value="ZahlWegCd"/&gt;
 *     &lt;enumeration value="MahnStufeCd"/&gt;
 *     &lt;enumeration value="RueckGrundCd"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "SchlArtCd_Type")
@XmlEnum
public enum SchlArtCdType {


    /**
     * Änderungsgrund
     * 
     */
    @XmlEnumValue("AendGrundCd")
    AEND_GRUND_CD("AendGrundCd"),

    /**
     * Antriebsart
     * 
     */
    @XmlEnumValue("AntriebsArtCd")
    ANTRIEBS_ART_CD("AntriebsArtCd"),

    /**
     * Ausstattung
     * 
     */
    @XmlEnumValue("AusstattungCd")
    AUSSTATTUNG_CD("AusstattungCd"),

    /**
     * Bauart
     * 
     */
    @XmlEnumValue("BauartCd")
    BAUART_CD("BauartCd"),

    /**
     * Bezugsberechtigungsart
     * 
     */
    @XmlEnumValue("BBArtCd")
    BB_ART_CD("BBArtCd"),

    /**
     * Bearbeitungsstand
     * 
     */
    @XmlEnumValue("BearbStandCd")
    BEARB_STAND_CD("BearbStandCd"),

    /**
     * Betragsart
     * 
     */
    @XmlEnumValue("BetragArtCd")
    BETRAG_ART_CD("BetragArtCd"),

    /**
     * Beteiligungsrolle
     * 
     */
    @XmlEnumValue("BetRolleCd")
    BET_ROLLE_CD("BetRolleCd"),

    /**
     * Dachung
     * 
     */
    @XmlEnumValue("DachungCd")
    DACHUNG_CD("DachungCd"),

    /**
     * Entscheidungsfrage
     * 
     */
    @XmlEnumValue("EFrageCd")
    E_FRAGE_CD("EFrageCd"),

    /**
     * Einstufungsart
     * 
     */
    @XmlEnumValue("EstArtCd")
    EST_ART_CD("EstArtCd"),

    /**
     * Familienstand
     * 
     */
    @XmlEnumValue("FamilienstandCd")
    FAMILIENSTAND_CD("FamilienstandCd"),

    /**
     * Fahrzeugart
     * 
     */
    @XmlEnumValue("FzgArtCd")
    FZG_ART_CD("FzgArtCd"),

    /**
     * Art des Gebäudes
     * 
     */
    @XmlEnumValue("GebaeudeArtCd")
    GEBAEUDE_ART_CD("GebaeudeArtCd"),

    /**
     * Gebäudehöhe
     * 
     */
    @XmlEnumValue("GebaeudeHoeheCd")
    GEBAEUDE_HOEHE_CD("GebaeudeHoeheCd"),

    /**
     * Geschlecht
     * 
     */
    @XmlEnumValue("GeschlechtCd")
    GESCHLECHT_CD("GeschlechtCd"),

    /**
     * GrundRuecklauf
     * 
     */
    @XmlEnumValue("GrundRuecklaufCd")
    GRUND_RUECKLAUF_CD("GrundRuecklaufCd"),

    /**
     * Grenzwertart
     * 
     */
    @XmlEnumValue("GrwArtCd")
    GRW_ART_CD("GrwArtCd"),

    /**
     * Identifizierungsart
     * 
     */
    @XmlEnumValue("IdfArtCd")
    IDF_ART_CD("IdfArtCd"),

    /**
     * Indexart
     * 
     */
    @XmlEnumValue("IndexArtCd")
    INDEX_ART_CD("IndexArtCd"),

    /**
     * KommunikationsArt
     * 
     */
    @XmlEnumValue("KomArtCd")
    KOM_ART_CD("KomArtCd"),

    /**
     * Internat. KFZ-Kennzeichen
     * 
     */
    @XmlEnumValue("LandesCd")
    LANDES_CD("LandesCd"),

    /**
     * Legitimierungsart
     * 
     */
    @XmlEnumValue("LegArtCd")
    LEG_ART_CD("LegArtCd"),

    /**
     * Löschungsarten
     * 
     */
    @XmlEnumValue("LoeschCd")
    LOESCH_CD("LoeschCd"),

    /**
     * Nutzung 
     * 
     */
    @XmlEnumValue("NutzungCd")
    NUTZUNG_CD("NutzungCd"),

    /**
     * Objektdatenart
     * 
     */
    @XmlEnumValue("ObjektdatenCd")
    OBJEKTDATEN_CD("ObjektdatenCd"),

    /**
     * Paketinhalt
     * 
     */
    @XmlEnumValue("PaketInhCd")
    PAKET_INH_CD("PaketInhCd"),

    /**
     * Paketumfang
     * 
     */
    @XmlEnumValue("PaketUmfCd")
    PAKET_UMF_CD("PaketUmfCd"),

    /**
     * Personenart
     * 
     */
    @XmlEnumValue("PersArtCd")
    PERS_ART_CD("PersArtCd"),

    /**
     * Prämienfreiart
     * 
     */
    @XmlEnumValue("PfrArtCd")
    PFR_ART_CD("PfrArtCd"),

    /**
     * Polizzenart
     * 
     */
    @XmlEnumValue("PolArtCd")
    POL_ART_CD("PolArtCd"),

    /**
     * Prämienfrist
     * 
     */
    @XmlEnumValue("PraemFristCd")
    PRAEM_FRIST_CD("PraemFristCd"),

    /**
     * Prämienkorrekturart
     * 
     */
    @XmlEnumValue("PraemKorrArtCd")
    PRAEM_KORR_ART_CD("PraemKorrArtCd"),

    /**
     * Provisionsart
     * 
     */
    @XmlEnumValue("ProvArtCd")
    PROV_ART_CD("ProvArtCd"),

    /**
     * Risikoart
     * 
     */
    @XmlEnumValue("RisikoArtCd")
    RISIKO_ART_CD("RisikoArtCd"),

    /**
     * Rentenzahlungsrhythmus
     * 
     */
    @XmlEnumValue("RntRhythmCd")
    RNT_RHYTHM_CD("RntRhythmCd"),

    /**
     * Selbstbehaltart
     * 
     */
    @XmlEnumValue("SbhArtCd")
    SBH_ART_CD("SbhArtCd"),

    /**
     * Schadenursache
     * 
     */
    @XmlEnumValue("SchadUrsCd")
    SCHAD_URS_CD("SchadUrsCd"),

    /**
     * Art der sonstigen Person
     * 
     */
    @XmlEnumValue("SonstPersArtCd")
    SONST_PERS_ART_CD("SonstPersArtCd"),

    /**
     * Sparte
     * 
     */
    @XmlEnumValue("SpartenCd")
    SPARTEN_CD("SpartenCd"),

    /**
     * Steuerart
     * 
     */
    @XmlEnumValue("StArtCd")
    ST_ART_CD("StArtCd"),

    /**
     * Textart
     * 
     */
    @XmlEnumValue("TxtArtCd")
    TXT_ART_CD("TxtArtCd"),

    /**
     * Versicherungssummenart
     * 
     */
    @XmlEnumValue("VSArtCd")
    VS_ART_CD("VSArtCd"),

    /**
     * Verbandssparte
     * 
     */
    @XmlEnumValue("VerbandSparteCd")
    VERBAND_SPARTE_CD("VerbandSparteCd"),

    /**
     * Versicherte Sache
     * 
     */
    @XmlEnumValue("VersSacheCd")
    VERS_SACHE_CD("VersSacheCd"),

    /**
     * Verschulden
     * 
     */
    @XmlEnumValue("VerschuldenCd")
    VERSCHULDEN_CD("VerschuldenCd"),

    /**
     * Verwendungszweck
     * 
     */
    @XmlEnumValue("VerwendzweckCd")
    VERWENDZWECK_CD("VerwendzweckCd"),

    /**
     * Vertragsprodukt
     * 
     */
    @XmlEnumValue("VtgProdCd")
    VTG_PROD_CD("VtgProdCd"),

    /**
     * Vertragsrolle
     * 
     */
    @XmlEnumValue("VtgRolleCd")
    VTG_ROLLE_CD("VtgRolleCd"),

    /**
     * Vertragssparte
     * 
     */
    @XmlEnumValue("VtgSparteCd")
    VTG_SPARTE_CD("VtgSparteCd"),

    /**
     * Vertragsstatus
     * 
     */
    @XmlEnumValue("VtgStatusCd")
    VTG_STATUS_CD("VtgStatusCd"),

    /**
     * Währung
     * 
     */
    @XmlEnumValue("WaehrungsCd")
    WAEHRUNGS_CD("WaehrungsCd"),

    /**
     * Art des Zeitraumes
     * 
     */
    @XmlEnumValue("ZRArtCd")
    ZR_ART_CD("ZRArtCd"),

    /**
     * Zahlungsgrund
     * 
     */
    @XmlEnumValue("ZahlGrundCd")
    ZAHL_GRUND_CD("ZahlGrundCd"),

    /**
     * Zahlungsrhythmus
     * 
     */
    @XmlEnumValue("ZahlRhythmCd")
    ZAHL_RHYTHM_CD("ZahlRhythmCd"),

    /**
     * Zahlungsweg
     * 
     */
    @XmlEnumValue("ZahlWegCd")
    ZAHL_WEG_CD("ZahlWegCd"),

    /**
     * MahnStufe
     * 
     */
    @XmlEnumValue("MahnStufeCd")
    MAHN_STUFE_CD("MahnStufeCd"),

    /**
     * GrundRuecklauf
     * 
     */
    @XmlEnumValue("RueckGrundCd")
    RUECK_GRUND_CD("RueckGrundCd");
    private final String value;

    SchlArtCdType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SchlArtCdType fromValue(String v) {
        for (SchlArtCdType c: SchlArtCdType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
