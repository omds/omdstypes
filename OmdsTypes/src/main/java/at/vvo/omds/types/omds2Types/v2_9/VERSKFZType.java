
package at.vvo.omds.types.omds2Types.v2_9;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java-Klasse für VERS_KFZ_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="VERS_KFZ_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="FzgArtCd" use="required" type="{urn:omds20}FzgArtCd_Type" /&gt;
 *       &lt;attribute name="Marke" use="required"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="40"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="Handelsbez"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="30"/&gt;
 *             &lt;minLength value="1"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="TypVarVer"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="20"/&gt;
 *             &lt;minLength value="1"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="Leasingkz" type="{urn:omds20}Entsch2_Type" /&gt;
 *       &lt;attribute name="Modelljahr" type="{http://www.w3.org/2001/XMLSchema}unsignedShort" /&gt;
 *       &lt;attribute name="Leistung" type="{http://www.w3.org/2001/XMLSchema}unsignedShort" /&gt;
 *       &lt;attribute name="Plaetze" type="{http://www.w3.org/2001/XMLSchema}unsignedShort" /&gt;
 *       &lt;attribute name="Nutzlast"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal"&gt;
 *             &lt;totalDigits value="6"/&gt;
 *             &lt;fractionDigits value="0"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="Hubraum" type="{http://www.w3.org/2001/XMLSchema}unsignedShort" /&gt;
 *       &lt;attribute name="AntriebsArtCd" use="required" type="{urn:omds20}AntriebsArtCd_Type" /&gt;
 *       &lt;attribute name="Katkz" use="required" type="{urn:omds20}Entsch3_Type" /&gt;
 *       &lt;attribute name="ABSKz" type="{urn:omds20}Entsch2_Type" /&gt;
 *       &lt;attribute name="CO2_Ausstoss" type="{http://www.w3.org/2001/XMLSchema}unsignedShort" /&gt;
 *       &lt;attribute name="Fahrgestnr" use="required"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="20"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="Motornr"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="20"/&gt;
 *             &lt;minLength value="1"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="NatCode"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;minLength value="1"/&gt;
 *             &lt;maxLength value="26"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="VerwendzweckCd" type="{urn:omds20}VerwendzweckCd_Type" /&gt;
 *       &lt;attribute name="Erstzulassdat" type="{urn:omds20}Datum" /&gt;
 *       &lt;attribute name="LandesCd" type="{urn:omds20}LandesCd_Type" /&gt;
 *       &lt;attribute name="Pol_Kennz"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="12"/&gt;
 *             &lt;minLength value="1"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VERS_KFZ_Type")
@XmlSeeAlso({
    VERSKFZ.class
})
public class VERSKFZType {

    @XmlAttribute(name = "FzgArtCd", required = true)
    protected String fzgArtCd;
    @XmlAttribute(name = "Marke", required = true)
    protected String marke;
    @XmlAttribute(name = "Handelsbez")
    protected String handelsbez;
    @XmlAttribute(name = "TypVarVer")
    protected String typVarVer;
    @XmlAttribute(name = "Leasingkz")
    protected Entsch2Type leasingkz;
    @XmlAttribute(name = "Modelljahr")
    @XmlSchemaType(name = "unsignedShort")
    protected Integer modelljahr;
    @XmlAttribute(name = "Leistung")
    @XmlSchemaType(name = "unsignedShort")
    protected Integer leistung;
    @XmlAttribute(name = "Plaetze")
    @XmlSchemaType(name = "unsignedShort")
    protected Integer plaetze;
    @XmlAttribute(name = "Nutzlast")
    protected BigDecimal nutzlast;
    @XmlAttribute(name = "Hubraum")
    @XmlSchemaType(name = "unsignedShort")
    protected Integer hubraum;
    @XmlAttribute(name = "AntriebsArtCd", required = true)
    protected String antriebsArtCd;
    @XmlAttribute(name = "Katkz", required = true)
    protected String katkz;
    @XmlAttribute(name = "ABSKz")
    protected Entsch2Type absKz;
    @XmlAttribute(name = "CO2_Ausstoss")
    @XmlSchemaType(name = "unsignedShort")
    protected Integer co2Ausstoss;
    @XmlAttribute(name = "Fahrgestnr", required = true)
    protected String fahrgestnr;
    @XmlAttribute(name = "Motornr")
    protected String motornr;
    @XmlAttribute(name = "NatCode")
    protected String natCode;
    @XmlAttribute(name = "VerwendzweckCd")
    protected String verwendzweckCd;
    @XmlAttribute(name = "Erstzulassdat")
    protected XMLGregorianCalendar erstzulassdat;
    @XmlAttribute(name = "LandesCd")
    protected String landesCd;
    @XmlAttribute(name = "Pol_Kennz")
    protected String polKennz;

    /**
     * Ruft den Wert der fzgArtCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFzgArtCd() {
        return fzgArtCd;
    }

    /**
     * Legt den Wert der fzgArtCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFzgArtCd(String value) {
        this.fzgArtCd = value;
    }

    /**
     * Ruft den Wert der marke-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMarke() {
        return marke;
    }

    /**
     * Legt den Wert der marke-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMarke(String value) {
        this.marke = value;
    }

    /**
     * Ruft den Wert der handelsbez-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHandelsbez() {
        return handelsbez;
    }

    /**
     * Legt den Wert der handelsbez-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHandelsbez(String value) {
        this.handelsbez = value;
    }

    /**
     * Ruft den Wert der typVarVer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTypVarVer() {
        return typVarVer;
    }

    /**
     * Legt den Wert der typVarVer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTypVarVer(String value) {
        this.typVarVer = value;
    }

    /**
     * Ruft den Wert der leasingkz-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Entsch2Type }
     *     
     */
    public Entsch2Type getLeasingkz() {
        return leasingkz;
    }

    /**
     * Legt den Wert der leasingkz-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Entsch2Type }
     *     
     */
    public void setLeasingkz(Entsch2Type value) {
        this.leasingkz = value;
    }

    /**
     * Ruft den Wert der modelljahr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getModelljahr() {
        return modelljahr;
    }

    /**
     * Legt den Wert der modelljahr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setModelljahr(Integer value) {
        this.modelljahr = value;
    }

    /**
     * Ruft den Wert der leistung-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getLeistung() {
        return leistung;
    }

    /**
     * Legt den Wert der leistung-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setLeistung(Integer value) {
        this.leistung = value;
    }

    /**
     * Ruft den Wert der plaetze-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPlaetze() {
        return plaetze;
    }

    /**
     * Legt den Wert der plaetze-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPlaetze(Integer value) {
        this.plaetze = value;
    }

    /**
     * Ruft den Wert der nutzlast-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getNutzlast() {
        return nutzlast;
    }

    /**
     * Legt den Wert der nutzlast-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setNutzlast(BigDecimal value) {
        this.nutzlast = value;
    }

    /**
     * Ruft den Wert der hubraum-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getHubraum() {
        return hubraum;
    }

    /**
     * Legt den Wert der hubraum-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setHubraum(Integer value) {
        this.hubraum = value;
    }

    /**
     * Ruft den Wert der antriebsArtCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAntriebsArtCd() {
        return antriebsArtCd;
    }

    /**
     * Legt den Wert der antriebsArtCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAntriebsArtCd(String value) {
        this.antriebsArtCd = value;
    }

    /**
     * Ruft den Wert der katkz-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKatkz() {
        return katkz;
    }

    /**
     * Legt den Wert der katkz-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKatkz(String value) {
        this.katkz = value;
    }

    /**
     * Ruft den Wert der absKz-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Entsch2Type }
     *     
     */
    public Entsch2Type getABSKz() {
        return absKz;
    }

    /**
     * Legt den Wert der absKz-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Entsch2Type }
     *     
     */
    public void setABSKz(Entsch2Type value) {
        this.absKz = value;
    }

    /**
     * Ruft den Wert der co2Ausstoss-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCO2Ausstoss() {
        return co2Ausstoss;
    }

    /**
     * Legt den Wert der co2Ausstoss-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCO2Ausstoss(Integer value) {
        this.co2Ausstoss = value;
    }

    /**
     * Ruft den Wert der fahrgestnr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFahrgestnr() {
        return fahrgestnr;
    }

    /**
     * Legt den Wert der fahrgestnr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFahrgestnr(String value) {
        this.fahrgestnr = value;
    }

    /**
     * Ruft den Wert der motornr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMotornr() {
        return motornr;
    }

    /**
     * Legt den Wert der motornr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMotornr(String value) {
        this.motornr = value;
    }

    /**
     * Ruft den Wert der natCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNatCode() {
        return natCode;
    }

    /**
     * Legt den Wert der natCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNatCode(String value) {
        this.natCode = value;
    }

    /**
     * Ruft den Wert der verwendzweckCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVerwendzweckCd() {
        return verwendzweckCd;
    }

    /**
     * Legt den Wert der verwendzweckCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVerwendzweckCd(String value) {
        this.verwendzweckCd = value;
    }

    /**
     * Ruft den Wert der erstzulassdat-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getErstzulassdat() {
        return erstzulassdat;
    }

    /**
     * Legt den Wert der erstzulassdat-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setErstzulassdat(XMLGregorianCalendar value) {
        this.erstzulassdat = value;
    }

    /**
     * Ruft den Wert der landesCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLandesCd() {
        return landesCd;
    }

    /**
     * Legt den Wert der landesCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLandesCd(String value) {
        this.landesCd = value;
    }

    /**
     * Ruft den Wert der polKennz-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolKennz() {
        return polKennz;
    }

    /**
     * Legt den Wert der polKennz-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolKennz(String value) {
        this.polKennz = value;
    }

}
