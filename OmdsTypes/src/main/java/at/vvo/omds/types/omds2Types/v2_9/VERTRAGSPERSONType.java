
package at.vvo.omds.types.omds2Types.v2_9;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für VERTRAGSPERSON_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="VERTRAGSPERSON_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="VtgRolleCd" use="required" type="{urn:omds20}VtgRolleCd_Type" /&gt;
 *       &lt;attribute name="Lfnr" use="required" type="{http://www.w3.org/2001/XMLSchema}unsignedShort" /&gt;
 *       &lt;attribute name="Personennr" use="required" type="{urn:omds20}Personennr" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VERTRAGSPERSON_Type")
public class VERTRAGSPERSONType {

    @XmlAttribute(name = "VtgRolleCd", required = true)
    protected VtgRolleCdType vtgRolleCd;
    @XmlAttribute(name = "Lfnr", required = true)
    @XmlSchemaType(name = "unsignedShort")
    protected int lfnr;
    @XmlAttribute(name = "Personennr", required = true)
    protected String personennr;

    /**
     * Ruft den Wert der vtgRolleCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VtgRolleCdType }
     *     
     */
    public VtgRolleCdType getVtgRolleCd() {
        return vtgRolleCd;
    }

    /**
     * Legt den Wert der vtgRolleCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VtgRolleCdType }
     *     
     */
    public void setVtgRolleCd(VtgRolleCdType value) {
        this.vtgRolleCd = value;
    }

    /**
     * Ruft den Wert der lfnr-Eigenschaft ab.
     * 
     */
    public int getLfnr() {
        return lfnr;
    }

    /**
     * Legt den Wert der lfnr-Eigenschaft fest.
     * 
     */
    public void setLfnr(int value) {
        this.lfnr = value;
    }

    /**
     * Ruft den Wert der personennr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPersonennr() {
        return personennr;
    }

    /**
     * Legt den Wert der personennr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPersonennr(String value) {
        this.personennr = value;
    }

}
