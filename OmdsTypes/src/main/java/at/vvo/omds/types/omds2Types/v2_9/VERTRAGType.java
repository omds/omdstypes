
package at.vvo.omds.types.omds2Types.v2_9;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java-Klasse für VERTRAG_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="VERTRAG_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;element ref="{urn:omds20}EL-Antrag"/&gt;
 *           &lt;element ref="{urn:omds20}EL-Anzahl"/&gt;
 *           &lt;element ref="{urn:omds20}EL-Betrag"/&gt;
 *           &lt;element ref="{urn:omds20}EL-Bezugsberechtigung"/&gt;
 *           &lt;element ref="{urn:omds20}EL-Einstufung"/&gt;
 *           &lt;element ref="{urn:omds20}EL-Entscheidungsfrage"/&gt;
 *           &lt;element ref="{urn:omds20}EL-Identifizierung"/&gt;
 *           &lt;element ref="{urn:omds20}EL-Klausel"/&gt;
 *           &lt;element ref="{urn:omds20}EL-Polizzennummer"/&gt;
 *           &lt;element ref="{urn:omds20}EL-Praemienfreistellung"/&gt;
 *           &lt;element ref="{urn:omds20}EL-Praemienkorrektur"/&gt;
 *           &lt;element ref="{urn:omds20}EL-Rahmenvereinbarung"/&gt;
 *           &lt;element ref="{urn:omds20}EL-Selbstbehalt"/&gt;
 *           &lt;element ref="{urn:omds20}EL-Text"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element ref="{urn:omds20}VERTRAGSPERSON" maxOccurs="unbounded"/&gt;
 *         &lt;element ref="{urn:omds20}VERS_OBJEKT" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:omds20}SPARTE" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="Polizzennr" use="required" type="{urn:omds20}Polizzennr" /&gt;
 *       &lt;attribute name="VertragsID" type="{urn:omds20}VertragsID" /&gt;
 *       &lt;attribute name="Vermnr" use="required" type="{urn:omds20}Vermnr" /&gt;
 *       &lt;attribute name="VtgProdCd" use="required" type="{urn:omds20}VtgProdCd_Type" /&gt;
 *       &lt;attribute name="VtgProdukt" use="required"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="40"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="VtgSparteCd" type="{urn:omds20}VtgSparteCd_Type" /&gt;
 *       &lt;attribute name="ZahlRhythmCd" use="required" type="{urn:omds20}ZahlRhythmCd_Type" /&gt;
 *       &lt;attribute name="ZahlWegCd" use="required" type="{urn:omds20}ZahlWegCd_Type" /&gt;
 *       &lt;attribute name="VtgStatusCd" use="required" type="{urn:omds20}VtgStatusCd_Type" /&gt;
 *       &lt;attribute name="VtgStatusBeg" type="{urn:omds20}Datum" /&gt;
 *       &lt;attribute name="VtgBeg" use="required" type="{urn:omds20}Datum" /&gt;
 *       &lt;attribute name="VtgEnd" type="{urn:omds20}Datum" /&gt;
 *       &lt;attribute name="Hptfaelligkeit"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}gMonthDay"&gt;
 *             &lt;minInclusive value="--01-01"/&gt;
 *             &lt;maxInclusive value="--12-31"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="DurchfDat" use="required" type="{urn:omds20}Datum" /&gt;
 *       &lt;attribute name="GueltigBeg" use="required" type="{urn:omds20}Datum" /&gt;
 *       &lt;attribute name="AendGrundCd" use="required" type="{urn:omds20}AendGrundCd_Type" /&gt;
 *       &lt;attribute name="AendGrundbez"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="60"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="PraemieNtoVtg" use="required"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{urn:omds20}decimal"&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="PraemieBtoVtg" use="required" type="{urn:omds20}decimal" /&gt;
 *       &lt;attribute name="WaehrungsCd" use="required" type="{urn:omds20}WaehrungsCd_Type" /&gt;
 *       &lt;attribute name="BLZ"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;minLength value="1"/&gt;
 *             &lt;maxLength value="9"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="Ktonr"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="15"/&gt;
 *             &lt;minLength value="1"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="BIC"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;minLength value="1"/&gt;
 *             &lt;maxLength value="11"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="IBAN"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;minLength value="1"/&gt;
 *             &lt;maxLength value="34"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VERTRAG_Type", propOrder = {
    "elAntragOrELAnzahlOrELBetrag",
    "vertragsperson",
    "versobjekt",
    "sparte"
})
@XmlSeeAlso({
    VERTRAG.class
})
public class VERTRAGType {

    @XmlElements({
        @XmlElement(name = "EL-Antrag", type = ELAntragType.class),
        @XmlElement(name = "EL-Anzahl", type = ELAnzahlType.class),
        @XmlElement(name = "EL-Betrag", type = ELBetragType.class),
        @XmlElement(name = "EL-Bezugsberechtigung", type = ELBezugsberechtigungType.class),
        @XmlElement(name = "EL-Einstufung", type = ELEinstufungType.class),
        @XmlElement(name = "EL-Entscheidungsfrage", type = ELEntscheidungsfrageType.class),
        @XmlElement(name = "EL-Identifizierung", type = ELIdentifizierungType.class),
        @XmlElement(name = "EL-Klausel", type = ELKlauselType.class),
        @XmlElement(name = "EL-Polizzennummer", type = ELPolizzennummerType.class),
        @XmlElement(name = "EL-Praemienfreistellung", type = ELPraemienfreistellungType.class),
        @XmlElement(name = "EL-Praemienkorrektur", type = ELPraemienkorrekturType.class),
        @XmlElement(name = "EL-Rahmenvereinbarung", type = ELRahmenvereinbarungType.class),
        @XmlElement(name = "EL-Selbstbehalt", type = ELSelbstbehalt.class),
        @XmlElement(name = "EL-Text", type = ELTextType.class)
    })
    protected List<Object> elAntragOrELAnzahlOrELBetrag;
    @XmlElement(name = "VERTRAGSPERSON", required = true)
    protected List<VERTRAGSPERSONType> vertragsperson;
    @XmlElement(name = "VERS_OBJEKT")
    protected List<VERSOBJEKTType> versobjekt;
    @XmlElement(name = "SPARTE", required = true)
    protected List<SPARTEType> sparte;
    @XmlAttribute(name = "Polizzennr", required = true)
    protected String polizzennr;
    @XmlAttribute(name = "VertragsID")
    protected String vertragsID;
    @XmlAttribute(name = "Vermnr", required = true)
    protected String vermnr;
    @XmlAttribute(name = "VtgProdCd", required = true)
    protected String vtgProdCd;
    @XmlAttribute(name = "VtgProdukt", required = true)
    protected String vtgProdukt;
    @XmlAttribute(name = "VtgSparteCd")
    protected String vtgSparteCd;
    @XmlAttribute(name = "ZahlRhythmCd", required = true)
    protected String zahlRhythmCd;
    @XmlAttribute(name = "ZahlWegCd", required = true)
    protected String zahlWegCd;
    @XmlAttribute(name = "VtgStatusCd", required = true)
    protected String vtgStatusCd;
    @XmlAttribute(name = "VtgStatusBeg")
    protected XMLGregorianCalendar vtgStatusBeg;
    @XmlAttribute(name = "VtgBeg", required = true)
    protected XMLGregorianCalendar vtgBeg;
    @XmlAttribute(name = "VtgEnd")
    protected XMLGregorianCalendar vtgEnd;
    @XmlAttribute(name = "Hptfaelligkeit")
    protected XMLGregorianCalendar hptfaelligkeit;
    @XmlAttribute(name = "DurchfDat", required = true)
    protected XMLGregorianCalendar durchfDat;
    @XmlAttribute(name = "GueltigBeg", required = true)
    protected XMLGregorianCalendar gueltigBeg;
    @XmlAttribute(name = "AendGrundCd", required = true)
    protected String aendGrundCd;
    @XmlAttribute(name = "AendGrundbez")
    protected String aendGrundbez;
    @XmlAttribute(name = "PraemieNtoVtg", required = true)
    protected BigDecimal praemieNtoVtg;
    @XmlAttribute(name = "PraemieBtoVtg", required = true)
    protected BigDecimal praemieBtoVtg;
    @XmlAttribute(name = "WaehrungsCd", required = true)
    protected WaehrungsCdType waehrungsCd;
    @XmlAttribute(name = "BLZ")
    protected String blz;
    @XmlAttribute(name = "Ktonr")
    protected String ktonr;
    @XmlAttribute(name = "BIC")
    protected String bic;
    @XmlAttribute(name = "IBAN")
    protected String iban;

    /**
     * Gets the value of the elAntragOrELAnzahlOrELBetrag property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the elAntragOrELAnzahlOrELBetrag property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getELAntragOrELAnzahlOrELBetrag().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ELAntragType }
     * {@link ELAnzahlType }
     * {@link ELBetragType }
     * {@link ELBezugsberechtigungType }
     * {@link ELEinstufungType }
     * {@link ELEntscheidungsfrageType }
     * {@link ELIdentifizierungType }
     * {@link ELKlauselType }
     * {@link ELPolizzennummerType }
     * {@link ELPraemienfreistellungType }
     * {@link ELPraemienkorrekturType }
     * {@link ELRahmenvereinbarungType }
     * {@link ELSelbstbehalt }
     * {@link ELTextType }
     * 
     * 
     */
    public List<Object> getELAntragOrELAnzahlOrELBetrag() {
        if (elAntragOrELAnzahlOrELBetrag == null) {
            elAntragOrELAnzahlOrELBetrag = new ArrayList<Object>();
        }
        return this.elAntragOrELAnzahlOrELBetrag;
    }

    /**
     * Gets the value of the vertragsperson property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the vertragsperson property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVERTRAGSPERSON().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link VERTRAGSPERSONType }
     * 
     * 
     */
    public List<VERTRAGSPERSONType> getVERTRAGSPERSON() {
        if (vertragsperson == null) {
            vertragsperson = new ArrayList<VERTRAGSPERSONType>();
        }
        return this.vertragsperson;
    }

    /**
     * Gets the value of the versobjekt property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the versobjekt property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVERSOBJEKT().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link VERSOBJEKTType }
     * 
     * 
     */
    public List<VERSOBJEKTType> getVERSOBJEKT() {
        if (versobjekt == null) {
            versobjekt = new ArrayList<VERSOBJEKTType>();
        }
        return this.versobjekt;
    }

    /**
     * Gets the value of the sparte property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sparte property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSPARTE().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SPARTEType }
     * 
     * 
     */
    public List<SPARTEType> getSPARTE() {
        if (sparte == null) {
            sparte = new ArrayList<SPARTEType>();
        }
        return this.sparte;
    }

    /**
     * Ruft den Wert der polizzennr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolizzennr() {
        return polizzennr;
    }

    /**
     * Legt den Wert der polizzennr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolizzennr(String value) {
        this.polizzennr = value;
    }

    /**
     * Ruft den Wert der vertragsID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVertragsID() {
        return vertragsID;
    }

    /**
     * Legt den Wert der vertragsID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVertragsID(String value) {
        this.vertragsID = value;
    }

    /**
     * Ruft den Wert der vermnr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVermnr() {
        return vermnr;
    }

    /**
     * Legt den Wert der vermnr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVermnr(String value) {
        this.vermnr = value;
    }

    /**
     * Ruft den Wert der vtgProdCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVtgProdCd() {
        return vtgProdCd;
    }

    /**
     * Legt den Wert der vtgProdCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVtgProdCd(String value) {
        this.vtgProdCd = value;
    }

    /**
     * Ruft den Wert der vtgProdukt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVtgProdukt() {
        return vtgProdukt;
    }

    /**
     * Legt den Wert der vtgProdukt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVtgProdukt(String value) {
        this.vtgProdukt = value;
    }

    /**
     * Ruft den Wert der vtgSparteCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVtgSparteCd() {
        return vtgSparteCd;
    }

    /**
     * Legt den Wert der vtgSparteCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVtgSparteCd(String value) {
        this.vtgSparteCd = value;
    }

    /**
     * Ruft den Wert der zahlRhythmCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZahlRhythmCd() {
        return zahlRhythmCd;
    }

    /**
     * Legt den Wert der zahlRhythmCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZahlRhythmCd(String value) {
        this.zahlRhythmCd = value;
    }

    /**
     * Ruft den Wert der zahlWegCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZahlWegCd() {
        return zahlWegCd;
    }

    /**
     * Legt den Wert der zahlWegCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZahlWegCd(String value) {
        this.zahlWegCd = value;
    }

    /**
     * Ruft den Wert der vtgStatusCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVtgStatusCd() {
        return vtgStatusCd;
    }

    /**
     * Legt den Wert der vtgStatusCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVtgStatusCd(String value) {
        this.vtgStatusCd = value;
    }

    /**
     * Ruft den Wert der vtgStatusBeg-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getVtgStatusBeg() {
        return vtgStatusBeg;
    }

    /**
     * Legt den Wert der vtgStatusBeg-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setVtgStatusBeg(XMLGregorianCalendar value) {
        this.vtgStatusBeg = value;
    }

    /**
     * Ruft den Wert der vtgBeg-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getVtgBeg() {
        return vtgBeg;
    }

    /**
     * Legt den Wert der vtgBeg-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setVtgBeg(XMLGregorianCalendar value) {
        this.vtgBeg = value;
    }

    /**
     * Ruft den Wert der vtgEnd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getVtgEnd() {
        return vtgEnd;
    }

    /**
     * Legt den Wert der vtgEnd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setVtgEnd(XMLGregorianCalendar value) {
        this.vtgEnd = value;
    }

    /**
     * Ruft den Wert der hptfaelligkeit-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getHptfaelligkeit() {
        return hptfaelligkeit;
    }

    /**
     * Legt den Wert der hptfaelligkeit-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setHptfaelligkeit(XMLGregorianCalendar value) {
        this.hptfaelligkeit = value;
    }

    /**
     * Ruft den Wert der durchfDat-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDurchfDat() {
        return durchfDat;
    }

    /**
     * Legt den Wert der durchfDat-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDurchfDat(XMLGregorianCalendar value) {
        this.durchfDat = value;
    }

    /**
     * Ruft den Wert der gueltigBeg-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getGueltigBeg() {
        return gueltigBeg;
    }

    /**
     * Legt den Wert der gueltigBeg-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setGueltigBeg(XMLGregorianCalendar value) {
        this.gueltigBeg = value;
    }

    /**
     * Ruft den Wert der aendGrundCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAendGrundCd() {
        return aendGrundCd;
    }

    /**
     * Legt den Wert der aendGrundCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAendGrundCd(String value) {
        this.aendGrundCd = value;
    }

    /**
     * Ruft den Wert der aendGrundbez-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAendGrundbez() {
        return aendGrundbez;
    }

    /**
     * Legt den Wert der aendGrundbez-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAendGrundbez(String value) {
        this.aendGrundbez = value;
    }

    /**
     * Ruft den Wert der praemieNtoVtg-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPraemieNtoVtg() {
        return praemieNtoVtg;
    }

    /**
     * Legt den Wert der praemieNtoVtg-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPraemieNtoVtg(BigDecimal value) {
        this.praemieNtoVtg = value;
    }

    /**
     * Ruft den Wert der praemieBtoVtg-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPraemieBtoVtg() {
        return praemieBtoVtg;
    }

    /**
     * Legt den Wert der praemieBtoVtg-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPraemieBtoVtg(BigDecimal value) {
        this.praemieBtoVtg = value;
    }

    /**
     * Ruft den Wert der waehrungsCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link WaehrungsCdType }
     *     
     */
    public WaehrungsCdType getWaehrungsCd() {
        return waehrungsCd;
    }

    /**
     * Legt den Wert der waehrungsCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link WaehrungsCdType }
     *     
     */
    public void setWaehrungsCd(WaehrungsCdType value) {
        this.waehrungsCd = value;
    }

    /**
     * Ruft den Wert der blz-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBLZ() {
        return blz;
    }

    /**
     * Legt den Wert der blz-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBLZ(String value) {
        this.blz = value;
    }

    /**
     * Ruft den Wert der ktonr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKtonr() {
        return ktonr;
    }

    /**
     * Legt den Wert der ktonr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKtonr(String value) {
        this.ktonr = value;
    }

    /**
     * Ruft den Wert der bic-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBIC() {
        return bic;
    }

    /**
     * Legt den Wert der bic-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBIC(String value) {
        this.bic = value;
    }

    /**
     * Ruft den Wert der iban-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIBAN() {
        return iban;
    }

    /**
     * Legt den Wert der iban-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIBAN(String value) {
        this.iban = value;
    }

}
