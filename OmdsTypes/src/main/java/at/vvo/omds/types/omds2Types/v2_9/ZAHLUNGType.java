
package at.vvo.omds.types.omds2Types.v2_9;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java-Klasse für ZAHLUNG_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ZAHLUNG_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="ZahlungsLfnr" use="required" type="{http://www.w3.org/2001/XMLSchema}unsignedShort" /&gt;
 *       &lt;attribute name="WaehrungsCd" use="required" type="{urn:omds20}WaehrungsCd_Type" /&gt;
 *       &lt;attribute name="ZahlBetrag" use="required" type="{urn:omds20}decimal" /&gt;
 *       &lt;attribute name="ZahlDat" use="required" type="{urn:omds20}Datum" /&gt;
 *       &lt;attribute name="ZahlGrundCd" use="required" type="{urn:omds20}ZahlGrundCd_Type" /&gt;
 *       &lt;attribute name="ZahlWegCd" use="required" type="{urn:omds20}ZahlWegCd_Type" /&gt;
 *       &lt;attribute name="BLZ"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="9"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="Kontonr"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="15"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="BIC"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;minLength value="1"/&gt;
 *             &lt;maxLength value="11"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="IBAN"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;minLength value="1"/&gt;
 *             &lt;maxLength value="34"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ZAHLUNG_Type")
public class ZAHLUNGType {

    @XmlAttribute(name = "ZahlungsLfnr", required = true)
    @XmlSchemaType(name = "unsignedShort")
    protected int zahlungsLfnr;
    @XmlAttribute(name = "WaehrungsCd", required = true)
    protected WaehrungsCdType waehrungsCd;
    @XmlAttribute(name = "ZahlBetrag", required = true)
    protected BigDecimal zahlBetrag;
    @XmlAttribute(name = "ZahlDat", required = true)
    protected XMLGregorianCalendar zahlDat;
    @XmlAttribute(name = "ZahlGrundCd", required = true)
    protected String zahlGrundCd;
    @XmlAttribute(name = "ZahlWegCd", required = true)
    protected String zahlWegCd;
    @XmlAttribute(name = "BLZ")
    protected String blz;
    @XmlAttribute(name = "Kontonr")
    protected String kontonr;
    @XmlAttribute(name = "BIC")
    protected String bic;
    @XmlAttribute(name = "IBAN")
    protected String iban;

    /**
     * Ruft den Wert der zahlungsLfnr-Eigenschaft ab.
     * 
     */
    public int getZahlungsLfnr() {
        return zahlungsLfnr;
    }

    /**
     * Legt den Wert der zahlungsLfnr-Eigenschaft fest.
     * 
     */
    public void setZahlungsLfnr(int value) {
        this.zahlungsLfnr = value;
    }

    /**
     * Ruft den Wert der waehrungsCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link WaehrungsCdType }
     *     
     */
    public WaehrungsCdType getWaehrungsCd() {
        return waehrungsCd;
    }

    /**
     * Legt den Wert der waehrungsCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link WaehrungsCdType }
     *     
     */
    public void setWaehrungsCd(WaehrungsCdType value) {
        this.waehrungsCd = value;
    }

    /**
     * Ruft den Wert der zahlBetrag-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getZahlBetrag() {
        return zahlBetrag;
    }

    /**
     * Legt den Wert der zahlBetrag-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setZahlBetrag(BigDecimal value) {
        this.zahlBetrag = value;
    }

    /**
     * Ruft den Wert der zahlDat-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getZahlDat() {
        return zahlDat;
    }

    /**
     * Legt den Wert der zahlDat-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setZahlDat(XMLGregorianCalendar value) {
        this.zahlDat = value;
    }

    /**
     * Ruft den Wert der zahlGrundCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZahlGrundCd() {
        return zahlGrundCd;
    }

    /**
     * Legt den Wert der zahlGrundCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZahlGrundCd(String value) {
        this.zahlGrundCd = value;
    }

    /**
     * Ruft den Wert der zahlWegCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZahlWegCd() {
        return zahlWegCd;
    }

    /**
     * Legt den Wert der zahlWegCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZahlWegCd(String value) {
        this.zahlWegCd = value;
    }

    /**
     * Ruft den Wert der blz-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBLZ() {
        return blz;
    }

    /**
     * Legt den Wert der blz-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBLZ(String value) {
        this.blz = value;
    }

    /**
     * Ruft den Wert der kontonr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKontonr() {
        return kontonr;
    }

    /**
     * Legt den Wert der kontonr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKontonr(String value) {
        this.kontonr = value;
    }

    /**
     * Ruft den Wert der bic-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBIC() {
        return bic;
    }

    /**
     * Legt den Wert der bic-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBIC(String value) {
        this.bic = value;
    }

    /**
     * Ruft den Wert der iban-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIBAN() {
        return iban;
    }

    /**
     * Legt den Wert der iban-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIBAN(String value) {
        this.iban = value;
    }

}
