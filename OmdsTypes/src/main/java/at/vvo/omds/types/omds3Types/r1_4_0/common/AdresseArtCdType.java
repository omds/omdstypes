
package at.vvo.omds.types.omds3Types.r1_4_0.common;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für AdresseArtCd_Type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="AdresseArtCd_Type"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="HA"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "AdresseArtCd_Type")
@XmlEnum
public enum AdresseArtCdType {


    /**
     * Hauptadresse
     * 
     */
    HA;

    public String value() {
        return name();
    }

    public static AdresseArtCdType fromValue(String v) {
        return valueOf(v);
    }

}
