
package at.vvo.omds.types.omds3Types.r1_4_0.common;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für Berechnungsvariante_Type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="Berechnungsvariante_Type"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="basic"/&gt;
 *     &lt;enumeration value="medium"/&gt;
 *     &lt;enumeration value="top"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "Berechnungsvariante_Type")
@XmlEnum
public enum BerechnungsvarianteType {

    @XmlEnumValue("basic")
    BASIC("basic"),
    @XmlEnumValue("medium")
    MEDIUM("medium"),
    @XmlEnumValue("top")
    TOP("top");
    private final String value;

    BerechnungsvarianteType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static BerechnungsvarianteType fromValue(String v) {
        for (BerechnungsvarianteType c: BerechnungsvarianteType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
