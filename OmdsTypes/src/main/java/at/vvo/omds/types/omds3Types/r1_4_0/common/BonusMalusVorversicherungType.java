
package at.vvo.omds.types.omds3Types.r1_4_0.common;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für BonusMalusVorversicherung_Type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="BonusMalusVorversicherung_Type"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Ohne Vorversicherung"/&gt;
 *     &lt;enumeration value="Mit Vorversicherung"/&gt;
 *     &lt;enumeration value="BM-Übernahme von Angehörigen"/&gt;
 *     &lt;enumeration value="BM-Übernahme vom Dienstgeber"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "BonusMalusVorversicherung_Type")
@XmlEnum
public enum BonusMalusVorversicherungType {

    @XmlEnumValue("Ohne Vorversicherung")
    OHNE_VORVERSICHERUNG("Ohne Vorversicherung"),
    @XmlEnumValue("Mit Vorversicherung")
    MIT_VORVERSICHERUNG("Mit Vorversicherung"),
    @XmlEnumValue("BM-\u00dcbernahme von Angeh\u00f6rigen")
    BM_ÜBERNAHME_VON_ANGEHÖRIGEN("BM-\u00dcbernahme von Angeh\u00f6rigen"),
    @XmlEnumValue("BM-\u00dcbernahme vom Dienstgeber")
    BM_ÜBERNAHME_VOM_DIENSTGEBER("BM-\u00dcbernahme vom Dienstgeber");
    private final String value;

    BonusMalusVorversicherungType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static BonusMalusVorversicherungType fromValue(String v) {
        for (BonusMalusVorversicherungType c: BonusMalusVorversicherungType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
