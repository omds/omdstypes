
package at.vvo.omds.types.omds3Types.r1_4_0.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.common.CalculateRequestType;
import at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.common.CreateApplicationRequestType;
import at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.common.CreateOfferRequestType;
import at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.common.SubmitApplicationRequestType;


/**
 * Abstakter Typ fuer Requests, die Teil eines Geschaeftsfalls mit eigener Id sind
 * 
 * <p>Java-Klasse für CommonProcessRequest_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CommonProcessRequest_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}CommonRequest_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:omds3CommonServiceTypes-1-1-0}Geschaeftsfallnummer" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommonProcessRequest_Type", propOrder = {
    "geschaeftsfallnummer"
})
@XmlSeeAlso({
    CalculateRequestType.class,
    CreateOfferRequestType.class,
    CreateApplicationRequestType.class,
    SubmitApplicationRequestType.class
})
public abstract class CommonProcessRequestType
    extends CommonRequestType
{

    @XmlElement(name = "Geschaeftsfallnummer")
    protected ObjektIdType geschaeftsfallnummer;

    /**
     * Optional eine Geschaeftsfallnummer, die mehrere Request-Responses einem Geschaeftsfall zuordnet
     * 
     * @return
     *     possible object is
     *     {@link ObjektIdType }
     *     
     */
    public ObjektIdType getGeschaeftsfallnummer() {
        return geschaeftsfallnummer;
    }

    /**
     * Legt den Wert der geschaeftsfallnummer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjektIdType }
     *     
     */
    public void setGeschaeftsfallnummer(ObjektIdType value) {
        this.geschaeftsfallnummer = value;
    }

}
