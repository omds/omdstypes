
package at.vvo.omds.types.omds3Types.r1_4_0.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.common.CalculateResponseType;
import at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.common.CreateApplicationResponseType;
import at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.common.CreateOfferResponseType;
import at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.common.SubmitApplicationResponseType;


/**
 * Abstrakter Typ für Ergebnisse, die Teil eines Geschaeftsfalls mit eigener Id sind
 * 
 * <p>Java-Klasse für CommonProcessResponse_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CommonProcessResponse_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}CommonResponse_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Geschaeftsfallnummer" type="{urn:omds3CommonServiceTypes-1-1-0}ObjektId_Type" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommonProcessResponse_Type", propOrder = {
    "geschaeftsfallnummer"
})
@XmlSeeAlso({
    CalculateResponseType.class,
    CreateOfferResponseType.class,
    CreateApplicationResponseType.class,
    SubmitApplicationResponseType.class
})
public abstract class CommonProcessResponseType
    extends CommonResponseType
{

    @XmlElement(name = "Geschaeftsfallnummer")
    protected ObjektIdType geschaeftsfallnummer;

    /**
     * Ruft den Wert der geschaeftsfallnummer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ObjektIdType }
     *     
     */
    public ObjektIdType getGeschaeftsfallnummer() {
        return geschaeftsfallnummer;
    }

    /**
     * Legt den Wert der geschaeftsfallnummer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjektIdType }
     *     
     */
    public void setGeschaeftsfallnummer(ObjektIdType value) {
        this.geschaeftsfallnummer = value;
    }

}
