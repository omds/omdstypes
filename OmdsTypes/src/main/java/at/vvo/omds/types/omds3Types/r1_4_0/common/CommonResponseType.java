
package at.vvo.omds.types.omds3Types.r1_4_0.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.common.GetApplicationDocumentResponseType;
import at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.sachPrivat.CalculateSachPrivatResponseType;
import at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.sachPrivat.CreateApplicationSachPrivatResponseType;
import at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.sachPrivat.CreateOfferSachPrivatResponseType;
import at.vvo.omds.types.omds3Types.r1_4_0.on3vertrag.CollectionChangeResponseType;
import at.vvo.omds.types.omds3Types.r1_4_0.on3vertrag.GetPoliciesOfPartnerResponseType;
import at.vvo.omds.types.omds3Types.r1_4_0.on3vertrag.SetMailingAddressResponseType;
import at.vvo.omds.types.omds3Types.r1_4_0.on4partner.AddCommunicationObjectResponseType;
import at.vvo.omds.types.omds3Types.r1_4_0.on4partner.ChangeCommunicationObjectResponseType;
import at.vvo.omds.types.omds3Types.r1_4_0.on4partner.ChangePartnerMainAddressResponseType;
import at.vvo.omds.types.omds3Types.r1_4_0.on4partner.ChangePersonDataResponseType;
import at.vvo.omds.types.omds3Types.r1_4_0.on4partner.CheckAddressResponseType;
import at.vvo.omds.types.omds3Types.r1_4_0.on4partner.DeleteCommunicationObjectResponseType;
import at.vvo.omds.types.omds3Types.r1_4_0.on4partner.GetPartnerResponseType;
import at.vvo.omds.types.omds3Types.r1_4_0.on7schaden.CreateClaimResponseType;
import at.vvo.omds.types.omds3Types.r1_4_0.on7schaden.GetClaimResponseLightType;
import at.vvo.omds.types.omds3Types.r1_4_0.on7schaden.GetClaimResponseType;
import at.vvo.omds.types.omds3Types.r1_4_0.on7schaden.InitiateClaimResponseType;
import at.vvo.omds.types.omds3Types.r1_4_0.on7schaden.SubmitClaimResponseType;


/**
 * Abstraktes ResponseObjekt
 * 
 * <p>Java-Klasse für CommonResponse_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CommonResponse_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Status" type="{urn:omds3CommonServiceTypes-1-1-0}ResponseStatus_Type"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommonResponse_Type", propOrder = {
    "status"
})
@XmlSeeAlso({
    CreateClaimResponseType.class,
    SubmitClaimResponseType.class,
    InitiateClaimResponseType.class,
    GetClaimResponseType.class,
    GetClaimResponseLightType.class,
    GetPoliciesOfPartnerResponseType.class,
    SetMailingAddressResponseType.class,
    CollectionChangeResponseType.class,
    GetApplicationDocumentResponseType.class,
    CalculateSachPrivatResponseType.class,
    CreateOfferSachPrivatResponseType.class,
    CreateApplicationSachPrivatResponseType.class,
    CommonProcessResponseType.class,
    CommonSearchResponseType.class,
    GetPartnerResponseType.class,
    CheckAddressResponseType.class,
    ChangePartnerMainAddressResponseType.class,
    ChangePersonDataResponseType.class,
    AddCommunicationObjectResponseType.class,
    DeleteCommunicationObjectResponseType.class,
    ChangeCommunicationObjectResponseType.class
})
public abstract class CommonResponseType {

    @XmlElement(name = "Status", required = true)
    protected ResponseStatusType status;

    /**
     * Ruft den Wert der status-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ResponseStatusType }
     *     
     */
    public ResponseStatusType getStatus() {
        return status;
    }

    /**
     * Legt den Wert der status-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseStatusType }
     *     
     */
    public void setStatus(ResponseStatusType value) {
        this.status = value;
    }

}
