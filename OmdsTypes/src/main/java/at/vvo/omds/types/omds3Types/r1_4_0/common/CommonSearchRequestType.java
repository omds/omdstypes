
package at.vvo.omds.types.omds3Types.r1_4_0.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds2Types.v2_11.ELZeitraumType;
import at.vvo.omds.types.omds3Types.r1_4_0.on1basis.GetStateChangesRequestType;


/**
 * Abstakter Typ fuer Suchanfragen
 * 
 * <p>Java-Klasse für CommonSearchRequest_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CommonSearchRequest_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}CommonRequest_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AuthFilter" type="{urn:omds3CommonServiceTypes-1-1-0}AuthorizationFilter" minOccurs="0"/&gt;
 *         &lt;element name="Suchbegriff" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Zeitraum" type="{urn:omds20}EL-Zeitraum_Type" minOccurs="0"/&gt;
 *         &lt;element name="MaxResults" type="{http://www.w3.org/2001/XMLSchema}unsignedInt"/&gt;
 *         &lt;element name="Offset" type="{http://www.w3.org/2001/XMLSchema}unsignedInt"/&gt;
 *         &lt;element name="OrderBy" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;enumeration value="Meldedatum aufsteigend"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommonSearchRequest_Type", propOrder = {
    "authFilter",
    "suchbegriff",
    "zeitraum",
    "maxResults",
    "offset",
    "orderBy"
})
@XmlSeeAlso({
    GetStateChangesRequestType.class
})
public abstract class CommonSearchRequestType
    extends CommonRequestType
{

    @XmlElement(name = "AuthFilter")
    protected AuthorizationFilter authFilter;
    @XmlElement(name = "Suchbegriff")
    protected String suchbegriff;
    @XmlElement(name = "Zeitraum")
    protected ELZeitraumType zeitraum;
    @XmlElement(name = "MaxResults")
    @XmlSchemaType(name = "unsignedInt")
    protected long maxResults;
    @XmlElement(name = "Offset")
    @XmlSchemaType(name = "unsignedInt")
    protected long offset;
    @XmlElement(name = "OrderBy")
    protected String orderBy;

    /**
     * Ruft den Wert der authFilter-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AuthorizationFilter }
     *     
     */
    public AuthorizationFilter getAuthFilter() {
        return authFilter;
    }

    /**
     * Legt den Wert der authFilter-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AuthorizationFilter }
     *     
     */
    public void setAuthFilter(AuthorizationFilter value) {
        this.authFilter = value;
    }

    /**
     * Ruft den Wert der suchbegriff-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSuchbegriff() {
        return suchbegriff;
    }

    /**
     * Legt den Wert der suchbegriff-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSuchbegriff(String value) {
        this.suchbegriff = value;
    }

    /**
     * Ruft den Wert der zeitraum-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ELZeitraumType }
     *     
     */
    public ELZeitraumType getZeitraum() {
        return zeitraum;
    }

    /**
     * Legt den Wert der zeitraum-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ELZeitraumType }
     *     
     */
    public void setZeitraum(ELZeitraumType value) {
        this.zeitraum = value;
    }

    /**
     * Ruft den Wert der maxResults-Eigenschaft ab.
     * 
     */
    public long getMaxResults() {
        return maxResults;
    }

    /**
     * Legt den Wert der maxResults-Eigenschaft fest.
     * 
     */
    public void setMaxResults(long value) {
        this.maxResults = value;
    }

    /**
     * Ruft den Wert der offset-Eigenschaft ab.
     * 
     */
    public long getOffset() {
        return offset;
    }

    /**
     * Legt den Wert der offset-Eigenschaft fest.
     * 
     */
    public void setOffset(long value) {
        this.offset = value;
    }

    /**
     * Ruft den Wert der orderBy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderBy() {
        return orderBy;
    }

    /**
     * Legt den Wert der orderBy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderBy(String value) {
        this.orderBy = value;
    }

}
