
package at.vvo.omds.types.omds3Types.r1_4_0.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Type Datenverwendung
 * 
 * <p>Java-Klasse für Datenverwendung_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Datenverwendung_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ZustimmungZurElektrUebermittlungVorvertraglDokumente" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="ElektrUebermittlungVorvertraglDokumenteEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ZustimmungZurVerwendungDerDatenZuWerbezwecken" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Datenverwendung_Type", propOrder = {
    "zustimmungZurElektrUebermittlungVorvertraglDokumente",
    "elektrUebermittlungVorvertraglDokumenteEmail",
    "zustimmungZurVerwendungDerDatenZuWerbezwecken"
})
public class DatenverwendungType {

    @XmlElement(name = "ZustimmungZurElektrUebermittlungVorvertraglDokumente")
    protected Boolean zustimmungZurElektrUebermittlungVorvertraglDokumente;
    @XmlElement(name = "ElektrUebermittlungVorvertraglDokumenteEmail")
    protected String elektrUebermittlungVorvertraglDokumenteEmail;
    @XmlElement(name = "ZustimmungZurVerwendungDerDatenZuWerbezwecken")
    protected boolean zustimmungZurVerwendungDerDatenZuWerbezwecken;

    /**
     * Ruft den Wert der zustimmungZurElektrUebermittlungVorvertraglDokumente-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isZustimmungZurElektrUebermittlungVorvertraglDokumente() {
        return zustimmungZurElektrUebermittlungVorvertraglDokumente;
    }

    /**
     * Legt den Wert der zustimmungZurElektrUebermittlungVorvertraglDokumente-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setZustimmungZurElektrUebermittlungVorvertraglDokumente(Boolean value) {
        this.zustimmungZurElektrUebermittlungVorvertraglDokumente = value;
    }

    /**
     * Ruft den Wert der elektrUebermittlungVorvertraglDokumenteEmail-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getElektrUebermittlungVorvertraglDokumenteEmail() {
        return elektrUebermittlungVorvertraglDokumenteEmail;
    }

    /**
     * Legt den Wert der elektrUebermittlungVorvertraglDokumenteEmail-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setElektrUebermittlungVorvertraglDokumenteEmail(String value) {
        this.elektrUebermittlungVorvertraglDokumenteEmail = value;
    }

    /**
     * Ruft den Wert der zustimmungZurVerwendungDerDatenZuWerbezwecken-Eigenschaft ab.
     * 
     */
    public boolean isZustimmungZurVerwendungDerDatenZuWerbezwecken() {
        return zustimmungZurVerwendungDerDatenZuWerbezwecken;
    }

    /**
     * Legt den Wert der zustimmungZurVerwendungDerDatenZuWerbezwecken-Eigenschaft fest.
     * 
     */
    public void setZustimmungZurVerwendungDerDatenZuWerbezwecken(boolean value) {
        this.zustimmungZurVerwendungDerDatenZuWerbezwecken = value;
    }

}
