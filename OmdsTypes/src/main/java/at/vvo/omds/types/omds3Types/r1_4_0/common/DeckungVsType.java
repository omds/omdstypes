
package at.vvo.omds.types.omds3Types.r1_4_0.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Type Deckung
 * 
 * <p>Java-Klasse für DeckungVs_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="DeckungVs_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Versicherungssumme" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DeckungVs_Type", propOrder = {
    "versicherungssumme"
})
public class DeckungVsType {

    @XmlElement(name = "Versicherungssumme")
    protected int versicherungssumme;

    /**
     * Ruft den Wert der versicherungssumme-Eigenschaft ab.
     * 
     */
    public int getVersicherungssumme() {
        return versicherungssumme;
    }

    /**
     * Legt den Wert der versicherungssumme-Eigenschaft fest.
     * 
     */
    public void setVersicherungssumme(int value) {
        this.versicherungssumme = value;
    }

}
