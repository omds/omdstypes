
package at.vvo.omds.types.omds3Types.r1_4_0.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Type Deckung 
 * 
 * <p>Java-Klasse für DeckungVsVI_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="DeckungVsVI_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="deckungActive" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="deckungVs" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="VersichertesInteresse" type="{http://www.w3.org/2001/XMLSchema}anyType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DeckungVsVI_Type", propOrder = {
    "deckungActive",
    "deckungVs",
    "versichertesInteresse"
})
public class DeckungVsVIType {

    protected boolean deckungActive;
    protected int deckungVs;
    @XmlElement(name = "VersichertesInteresse", required = true)
    protected Object versichertesInteresse;

    /**
     * Ruft den Wert der deckungActive-Eigenschaft ab.
     * 
     */
    public boolean isDeckungActive() {
        return deckungActive;
    }

    /**
     * Legt den Wert der deckungActive-Eigenschaft fest.
     * 
     */
    public void setDeckungActive(boolean value) {
        this.deckungActive = value;
    }

    /**
     * Ruft den Wert der deckungVs-Eigenschaft ab.
     * 
     */
    public int getDeckungVs() {
        return deckungVs;
    }

    /**
     * Legt den Wert der deckungVs-Eigenschaft fest.
     * 
     */
    public void setDeckungVs(int value) {
        this.deckungVs = value;
    }

    /**
     * Ruft den Wert der versichertesInteresse-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getVersichertesInteresse() {
        return versichertesInteresse;
    }

    /**
     * Legt den Wert der versichertesInteresse-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setVersichertesInteresse(Object value) {
        this.versichertesInteresse = value;
    }

}
