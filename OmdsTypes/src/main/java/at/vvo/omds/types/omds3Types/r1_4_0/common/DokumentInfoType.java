
package at.vvo.omds.types.omds3Types.r1_4_0.common;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Typ mit Informationen zu einem Dokument (kann auch das Dokument selbst enthalten)
 * 
 * <p>Java-Klasse für DokumentInfo_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="DokumentInfo_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Content" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/&gt;
 *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DocumentType" type="{urn:omds3CommonServiceTypes-1-1-0}DocumentType"/&gt;
 *         &lt;element name="Mimetype" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Groesse" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/&gt;
 *         &lt;element name="Datum" type="{urn:omds20}Datum-Zeit" minOccurs="0"/&gt;
 *         &lt;element name="ReferenzWeitereDokumente" type="{urn:omds3CommonServiceTypes-1-1-0}DokumentInfo_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DokumentInfo_Type", propOrder = {
    "content",
    "name",
    "documentType",
    "mimetype",
    "groesse",
    "datum",
    "referenzWeitereDokumente"
})
public class DokumentInfoType {

    @XmlElement(name = "Content")
    protected byte[] content;
    @XmlElement(name = "Name")
    protected String name;
    @XmlElement(name = "DocumentType")
    protected int documentType;
    @XmlElement(name = "Mimetype")
    protected String mimetype;
    @XmlElement(name = "Groesse")
    protected Long groesse;
    @XmlElement(name = "Datum")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar datum;
    @XmlElement(name = "ReferenzWeitereDokumente")
    protected List<DokumentInfoType> referenzWeitereDokumente;

    /**
     * Ruft den Wert der content-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getContent() {
        return content;
    }

    /**
     * Legt den Wert der content-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setContent(byte[] value) {
        this.content = value;
    }

    /**
     * Ruft den Wert der name-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Legt den Wert der name-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Ruft den Wert der documentType-Eigenschaft ab.
     * 
     */
    public int getDocumentType() {
        return documentType;
    }

    /**
     * Legt den Wert der documentType-Eigenschaft fest.
     * 
     */
    public void setDocumentType(int value) {
        this.documentType = value;
    }

    /**
     * Ruft den Wert der mimetype-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMimetype() {
        return mimetype;
    }

    /**
     * Legt den Wert der mimetype-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMimetype(String value) {
        this.mimetype = value;
    }

    /**
     * Ruft den Wert der groesse-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getGroesse() {
        return groesse;
    }

    /**
     * Legt den Wert der groesse-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setGroesse(Long value) {
        this.groesse = value;
    }

    /**
     * Ruft den Wert der datum-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDatum() {
        return datum;
    }

    /**
     * Legt den Wert der datum-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDatum(XMLGregorianCalendar value) {
        this.datum = value;
    }

    /**
     * Gets the value of the referenzWeitereDokumente property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the referenzWeitereDokumente property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReferenzWeitereDokumente().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DokumentInfoType }
     * 
     * 
     */
    public List<DokumentInfoType> getReferenzWeitereDokumente() {
        if (referenzWeitereDokumente == null) {
            referenzWeitereDokumente = new ArrayList<DokumentInfoType>();
        }
        return this.referenzWeitereDokumente;
    }

}
