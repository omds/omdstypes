
package at.vvo.omds.types.omds3Types.r1_4_0.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Ein Typ für eine Objekt-Id in Verbindung mit einem möglichen Ablaufzeitpunkt und dem Hinweis, ob die Id nur einmalig oder auch mehrmalig verwendet werden kann
 * 
 * <p>Java-Klasse für ElementIdType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ElementIdType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="idValidUntil" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="idIsSingleUse" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ElementIdType", propOrder = {
    "id",
    "idValidUntil",
    "idIsSingleUse"
})
public class ElementIdType {

    @XmlElement(required = true)
    protected String id;
    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar idValidUntil;
    protected boolean idIsSingleUse;

    /**
     * Ruft den Wert der id-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Legt den Wert der id-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Ruft den Wert der idValidUntil-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getIdValidUntil() {
        return idValidUntil;
    }

    /**
     * Legt den Wert der idValidUntil-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setIdValidUntil(XMLGregorianCalendar value) {
        this.idValidUntil = value;
    }

    /**
     * Ruft den Wert der idIsSingleUse-Eigenschaft ab.
     * 
     */
    public boolean isIdIsSingleUse() {
        return idIsSingleUse;
    }

    /**
     * Legt den Wert der idIsSingleUse-Eigenschaft fest.
     * 
     */
    public void setIdIsSingleUse(boolean value) {
        this.idIsSingleUse = value;
    }

}
