
package at.vvo.omds.types.omds3Types.r1_4_0.common;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.kfz.ElementarproduktKfzType;
import at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.kfz.VerkehrsrechtsschutzKfzType;
import at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.rs.ElementarproduktRechtsschutzType;
import at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.sachPrivat.ElementarproduktSachPrivatType;
import at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.unfall.ElementarproduktUnfallType;


/**
 * Basistyp für ein Elementarprodukt
 * 
 * <p>Java-Klasse für Elementarprodukt_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Elementarprodukt_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}ProduktbausteinAntragsprozess_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ZusaetzlicheElementarproduktdaten" type="{urn:omds3CommonServiceTypes-1-1-0}ZusaetzlicheElementarproduktdaten_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Elementarprodukt_Type", propOrder = {
    "zusaetzlicheElementarproduktdaten"
})
@XmlSeeAlso({
    ElementarproduktKfzType.class,
    VerkehrsrechtsschutzKfzType.class,
    ElementarproduktRechtsschutzType.class,
    ElementarproduktSachPrivatType.class,
    ElementarproduktUnfallType.class
})
public abstract class ElementarproduktType
    extends ProduktbausteinAntragsprozessType
{

    @XmlElement(name = "ZusaetzlicheElementarproduktdaten")
    protected List<ZusaetzlicheElementarproduktdatenType> zusaetzlicheElementarproduktdaten;

    /**
     * Gets the value of the zusaetzlicheElementarproduktdaten property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the zusaetzlicheElementarproduktdaten property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getZusaetzlicheElementarproduktdaten().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ZusaetzlicheElementarproduktdatenType }
     * 
     * 
     */
    public List<ZusaetzlicheElementarproduktdatenType> getZusaetzlicheElementarproduktdaten() {
        if (zusaetzlicheElementarproduktdaten == null) {
            zusaetzlicheElementarproduktdaten = new ArrayList<ZusaetzlicheElementarproduktdatenType>();
        }
        return this.zusaetzlicheElementarproduktdaten;
    }

}
