
package at.vvo.omds.types.omds3Types.r1_4_0.common;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Dient zur Abbildung von Kosten als absoluter oder prozentualer Wert
 * 
 * <p>Java-Klasse für KostenFixOderProzent_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="KostenFixOderProzent_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice&gt;
 *           &lt;element name="AbsoluterBetrag" type="{urn:omds20}decimal"/&gt;
 *           &lt;element name="ProzentVs" type="{http://www.w3.org/2001/XMLSchema}unsignedInt"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "KostenFixOderProzent_Type", propOrder = {
    "absoluterBetrag",
    "prozentVs"
})
public class KostenFixOderProzentType {

    @XmlElement(name = "AbsoluterBetrag")
    protected BigDecimal absoluterBetrag;
    @XmlElement(name = "ProzentVs")
    @XmlSchemaType(name = "unsignedInt")
    protected Long prozentVs;

    /**
     * Ruft den Wert der absoluterBetrag-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAbsoluterBetrag() {
        return absoluterBetrag;
    }

    /**
     * Legt den Wert der absoluterBetrag-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAbsoluterBetrag(BigDecimal value) {
        this.absoluterBetrag = value;
    }

    /**
     * Ruft den Wert der prozentVs-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getProzentVs() {
        return prozentVs;
    }

    /**
     * Legt den Wert der prozentVs-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setProzentVs(Long value) {
        this.prozentVs = value;
    }

}
