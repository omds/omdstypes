
package at.vvo.omds.types.omds3Types.r1_4_0.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * Typ Kreditkarte
 * 
 * <p>Java-Klasse für Kreditkarte_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Kreditkarte_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="Gesellschaft" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="Kartennummer" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="Inhaber" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="Pruefziffer" use="required"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt"&gt;
 *             &lt;totalDigits value="3"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="AblaufMonat" use="required"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte"&gt;
 *             &lt;totalDigits value="2"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="AblaufJahr" use="required"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte"&gt;
 *             &lt;totalDigits value="2"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Kreditkarte_Type")
public class KreditkarteType {

    @XmlAttribute(name = "Gesellschaft", namespace = "urn:omds3CommonServiceTypes-1-1-0", required = true)
    protected String gesellschaft;
    @XmlAttribute(name = "Kartennummer", namespace = "urn:omds3CommonServiceTypes-1-1-0", required = true)
    protected String kartennummer;
    @XmlAttribute(name = "Inhaber", namespace = "urn:omds3CommonServiceTypes-1-1-0", required = true)
    protected String inhaber;
    @XmlAttribute(name = "Pruefziffer", namespace = "urn:omds3CommonServiceTypes-1-1-0", required = true)
    protected long pruefziffer;
    @XmlAttribute(name = "AblaufMonat", namespace = "urn:omds3CommonServiceTypes-1-1-0", required = true)
    protected short ablaufMonat;
    @XmlAttribute(name = "AblaufJahr", namespace = "urn:omds3CommonServiceTypes-1-1-0", required = true)
    protected short ablaufJahr;

    /**
     * Ruft den Wert der gesellschaft-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGesellschaft() {
        return gesellschaft;
    }

    /**
     * Legt den Wert der gesellschaft-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGesellschaft(String value) {
        this.gesellschaft = value;
    }

    /**
     * Ruft den Wert der kartennummer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKartennummer() {
        return kartennummer;
    }

    /**
     * Legt den Wert der kartennummer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKartennummer(String value) {
        this.kartennummer = value;
    }

    /**
     * Ruft den Wert der inhaber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInhaber() {
        return inhaber;
    }

    /**
     * Legt den Wert der inhaber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInhaber(String value) {
        this.inhaber = value;
    }

    /**
     * Ruft den Wert der pruefziffer-Eigenschaft ab.
     * 
     */
    public long getPruefziffer() {
        return pruefziffer;
    }

    /**
     * Legt den Wert der pruefziffer-Eigenschaft fest.
     * 
     */
    public void setPruefziffer(long value) {
        this.pruefziffer = value;
    }

    /**
     * Ruft den Wert der ablaufMonat-Eigenschaft ab.
     * 
     */
    public short getAblaufMonat() {
        return ablaufMonat;
    }

    /**
     * Legt den Wert der ablaufMonat-Eigenschaft fest.
     * 
     */
    public void setAblaufMonat(short value) {
        this.ablaufMonat = value;
    }

    /**
     * Ruft den Wert der ablaufJahr-Eigenschaft ab.
     * 
     */
    public short getAblaufJahr() {
        return ablaufJahr;
    }

    /**
     * Legt den Wert der ablaufJahr-Eigenschaft fest.
     * 
     */
    public void setAblaufJahr(short value) {
        this.ablaufJahr = value;
    }

}
