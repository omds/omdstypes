
package at.vvo.omds.types.omds3Types.r1_4_0.common;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the at.vvo.omds.types.omds3Types.r1_4_0.common package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ServiceFault_QNAME = new QName("urn:omds3CommonServiceTypes-1-1-0", "serviceFault");
    private final static QName _OrdnungsbegriffZuordFremd_QNAME = new QName("urn:omds3CommonServiceTypes-1-1-0", "OrdnungsbegriffZuordFremd");
    private final static QName _Geschaeftsfallnummer_QNAME = new QName("urn:omds3CommonServiceTypes-1-1-0", "Geschaeftsfallnummer");
    private final static QName _ObjektId_QNAME = new QName("urn:omds3CommonServiceTypes-1-1-0", "ObjektId");
    private final static QName _Person_QNAME = new QName("urn:omds3CommonServiceTypes-1-1-0", "Person");
    private final static QName _Adresse_QNAME = new QName("urn:omds3CommonServiceTypes-1-1-0", "Adresse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: at.vvo.omds.types.omds3Types.r1_4_0.common
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ZahlwegType }
     * 
     */
    public ZahlwegType createZahlwegType() {
        return new ZahlwegType();
    }

    /**
     * Create an instance of {@link ObjektIdType }
     * 
     */
    public ObjektIdType createObjektIdType() {
        return new ObjektIdType();
    }

    /**
     * Create an instance of {@link ServiceFault }
     * 
     */
    public ServiceFault createServiceFault() {
        return new ServiceFault();
    }

    /**
     * Create an instance of {@link PersonType }
     * 
     */
    public PersonType createPersonType() {
        return new PersonType();
    }

    /**
     * Create an instance of {@link AdresseType }
     * 
     */
    public AdresseType createAdresseType() {
        return new AdresseType();
    }

    /**
     * Create an instance of {@link AgentFilterType }
     * 
     */
    public AgentFilterType createAgentFilterType() {
        return new AgentFilterType();
    }

    /**
     * Create an instance of {@link ElementIdType }
     * 
     */
    public ElementIdType createElementIdType() {
        return new ElementIdType();
    }

    /**
     * Create an instance of {@link ResponseStatusType }
     * 
     */
    public ResponseStatusType createResponseStatusType() {
        return new ResponseStatusType();
    }

    /**
     * Create an instance of {@link DateianhangType }
     * 
     */
    public DateianhangType createDateianhangType() {
        return new DateianhangType();
    }

    /**
     * Create an instance of {@link KontierungType }
     * 
     */
    public KontierungType createKontierungType() {
        return new KontierungType();
    }

    /**
     * Create an instance of {@link PraemieType }
     * 
     */
    public PraemieType createPraemieType() {
        return new PraemieType();
    }

    /**
     * Create an instance of {@link VersicherungssteuerType }
     * 
     */
    public VersicherungssteuerType createVersicherungssteuerType() {
        return new VersicherungssteuerType();
    }

    /**
     * Create an instance of {@link TechnicalKeyValueType }
     * 
     */
    public TechnicalKeyValueType createTechnicalKeyValueType() {
        return new TechnicalKeyValueType();
    }

    /**
     * Create an instance of {@link VertragspersonType }
     * 
     */
    public VertragspersonType createVertragspersonType() {
        return new VertragspersonType();
    }

    /**
     * Create an instance of {@link VinkulierungType }
     * 
     */
    public VinkulierungType createVinkulierungType() {
        return new VinkulierungType();
    }

    /**
     * Create an instance of {@link BezugsrechtType }
     * 
     */
    public BezugsrechtType createBezugsrechtType() {
        return new BezugsrechtType();
    }

    /**
     * Create an instance of {@link VinkularglaeubigerType }
     * 
     */
    public VinkularglaeubigerType createVinkularglaeubigerType() {
        return new VinkularglaeubigerType();
    }

    /**
     * Create an instance of {@link BonusMalusSystemType }
     * 
     */
    public BonusMalusSystemType createBonusMalusSystemType() {
        return new BonusMalusSystemType();
    }

    /**
     * Create an instance of {@link OffeneSchaedenType }
     * 
     */
    public OffeneSchaedenType createOffeneSchaedenType() {
        return new OffeneSchaedenType();
    }

    /**
     * Create an instance of {@link OffenerSchadenType }
     * 
     */
    public OffenerSchadenType createOffenerSchadenType() {
        return new OffenerSchadenType();
    }

    /**
     * Create an instance of {@link VorversicherungenType }
     * 
     */
    public VorversicherungenType createVorversicherungenType() {
        return new VorversicherungenType();
    }

    /**
     * Create an instance of {@link VorversicherungenDetailType }
     * 
     */
    public VorversicherungenDetailType createVorversicherungenDetailType() {
        return new VorversicherungenDetailType();
    }

    /**
     * Create an instance of {@link DatenverwendungType }
     * 
     */
    public DatenverwendungType createDatenverwendungType() {
        return new DatenverwendungType();
    }

    /**
     * Create an instance of {@link ErsatzpolizzenType }
     * 
     */
    public ErsatzpolizzenType createErsatzpolizzenType() {
        return new ErsatzpolizzenType();
    }

    /**
     * Create an instance of {@link DeckungVsType }
     * 
     */
    public DeckungVsType createDeckungVsType() {
        return new DeckungVsType();
    }

    /**
     * Create an instance of {@link DeckungVsVIType }
     * 
     */
    public DeckungVsVIType createDeckungVsVIType() {
        return new DeckungVsVIType();
    }

    /**
     * Create an instance of {@link DeckungProzentType }
     * 
     */
    public DeckungProzentType createDeckungProzentType() {
        return new DeckungProzentType();
    }

    /**
     * Create an instance of {@link DokumentInfoType }
     * 
     */
    public DokumentInfoType createDokumentInfoType() {
        return new DokumentInfoType();
    }

    /**
     * Create an instance of {@link ZahlungsdatenType }
     * 
     */
    public ZahlungsdatenType createZahlungsdatenType() {
        return new ZahlungsdatenType();
    }

    /**
     * Create an instance of {@link KreditkarteType }
     * 
     */
    public KreditkarteType createKreditkarteType() {
        return new KreditkarteType();
    }

    /**
     * Create an instance of {@link BankverbindungType }
     * 
     */
    public BankverbindungType createBankverbindungType() {
        return new BankverbindungType();
    }

    /**
     * Create an instance of {@link VersichertePersonType }
     * 
     */
    public VersichertePersonType createVersichertePersonType() {
        return new VersichertePersonType();
    }

    /**
     * Create an instance of {@link VersicherteVeranstaltungType }
     * 
     */
    public VersicherteVeranstaltungType createVersicherteVeranstaltungType() {
        return new VersicherteVeranstaltungType();
    }

    /**
     * Create an instance of {@link FahrzeugType }
     * 
     */
    public FahrzeugType createFahrzeugType() {
        return new FahrzeugType();
    }

    /**
     * Create an instance of {@link ZulassungsdatenType }
     * 
     */
    public ZulassungsdatenType createZulassungsdatenType() {
        return new ZulassungsdatenType();
    }

    /**
     * Create an instance of {@link VersicherterBetriebType }
     * 
     */
    public VersicherterBetriebType createVersicherterBetriebType() {
        return new VersicherterBetriebType();
    }

    /**
     * Create an instance of {@link VersicherteLiegenschaftType }
     * 
     */
    public VersicherteLiegenschaftType createVersicherteLiegenschaftType() {
        return new VersicherteLiegenschaftType();
    }

    /**
     * Create an instance of {@link KostenFixOderProzentType }
     * 
     */
    public KostenFixOderProzentType createKostenFixOderProzentType() {
        return new KostenFixOderProzentType();
    }

    /**
     * Create an instance of {@link SelbstbehaltType }
     * 
     */
    public SelbstbehaltType createSelbstbehaltType() {
        return new SelbstbehaltType();
    }

    /**
     * Create an instance of {@link UploadDokumentType }
     * 
     */
    public UploadDokumentType createUploadDokumentType() {
        return new UploadDokumentType();
    }

    /**
     * Create an instance of {@link DokumentenReferenzType }
     * 
     */
    public DokumentenReferenzType createDokumentenReferenzType() {
        return new DokumentenReferenzType();
    }

    /**
     * Create an instance of {@link ZeitraumType }
     * 
     */
    public ZeitraumType createZeitraumType() {
        return new ZeitraumType();
    }

    /**
     * Create an instance of {@link PolizzenObjektSpezifikationType }
     * 
     */
    public PolizzenObjektSpezifikationType createPolizzenObjektSpezifikationType() {
        return new PolizzenObjektSpezifikationType();
    }

    /**
     * Create an instance of {@link SchadenObjektSpezifikationType }
     * 
     */
    public SchadenObjektSpezifikationType createSchadenObjektSpezifikationType() {
        return new SchadenObjektSpezifikationType();
    }

    /**
     * Create an instance of {@link PolicyPartnerRole }
     * 
     */
    public PolicyPartnerRole createPolicyPartnerRole() {
        return new PolicyPartnerRole();
    }

    /**
     * Create an instance of {@link ZahlwegType.Kundenkonto }
     * 
     */
    public ZahlwegType.Kundenkonto createZahlwegTypeKundenkonto() {
        return new ZahlwegType.Kundenkonto();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ServiceFault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds3CommonServiceTypes-1-1-0", name = "serviceFault")
    public JAXBElement<ServiceFault> createServiceFault(ServiceFault value) {
        return new JAXBElement<ServiceFault>(_ServiceFault_QNAME, ServiceFault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds3CommonServiceTypes-1-1-0", name = "OrdnungsbegriffZuordFremd")
    public JAXBElement<String> createOrdnungsbegriffZuordFremd(String value) {
        return new JAXBElement<String>(_OrdnungsbegriffZuordFremd_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObjektIdType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds3CommonServiceTypes-1-1-0", name = "Geschaeftsfallnummer")
    public JAXBElement<ObjektIdType> createGeschaeftsfallnummer(ObjektIdType value) {
        return new JAXBElement<ObjektIdType>(_Geschaeftsfallnummer_QNAME, ObjektIdType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObjektIdType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds3CommonServiceTypes-1-1-0", name = "ObjektId")
    public JAXBElement<ObjektIdType> createObjektId(ObjektIdType value) {
        return new JAXBElement<ObjektIdType>(_ObjektId_QNAME, ObjektIdType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PersonType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds3CommonServiceTypes-1-1-0", name = "Person")
    public JAXBElement<PersonType> createPerson(PersonType value) {
        return new JAXBElement<PersonType>(_Person_QNAME, PersonType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdresseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds3CommonServiceTypes-1-1-0", name = "Adresse")
    public JAXBElement<AdresseType> createAdresse(AdresseType value) {
        return new JAXBElement<AdresseType>(_Adresse_QNAME, AdresseType.class, null, value);
    }

}
