
package at.vvo.omds.types.omds3Types.r1_4_0.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Type für Objektreferenzen
 * 
 * <p>Java-Klasse für ObjektId_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ObjektId_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Id" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="255"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="GueltigAb" type="{urn:omds20}Datum-Zeit" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:omds3CommonServiceTypes-1-1-0}OrdnungsbegriffZuordFremd" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ObjektId_Type", propOrder = {
    "id",
    "gueltigAb",
    "ordnungsbegriffZuordFremd"
})
public class ObjektIdType {

    @XmlElement(name = "Id")
    protected String id;
    @XmlElement(name = "GueltigAb")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar gueltigAb;
    @XmlElement(name = "OrdnungsbegriffZuordFremd")
    protected String ordnungsbegriffZuordFremd;

    /**
     * Ruft den Wert der id-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Legt den Wert der id-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Ruft den Wert der gueltigAb-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getGueltigAb() {
        return gueltigAb;
    }

    /**
     * Legt den Wert der gueltigAb-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setGueltigAb(XMLGregorianCalendar value) {
        this.gueltigAb = value;
    }

    /**
     * Ein Ordnungsbegriff aus dem System des Aufrufers, also z.B. aus dem Maklerverwaltungsprogramm
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrdnungsbegriffZuordFremd() {
        return ordnungsbegriffZuordFremd;
    }

    /**
     * Legt den Wert der ordnungsbegriffZuordFremd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrdnungsbegriffZuordFremd(String value) {
        this.ordnungsbegriffZuordFremd = value;
    }

}
