
package at.vvo.omds.types.omds3Types.r1_4_0.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.r1_4_0.on7schaden.SchadenmelderVermittlerType;


/**
 * Eine abstrakte Spezifikation eines Objekts
 * 
 * <p>Java-Klasse für ObjektSpezifikation_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ObjektSpezifikation_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ObjektSpezifikation_Type")
@XmlSeeAlso({
    SchadenmelderVermittlerType.class,
    PolizzenObjektSpezifikationType.class,
    SchadenObjektSpezifikationType.class
})
public abstract class ObjektSpezifikationType {


}
