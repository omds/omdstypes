
package at.vvo.omds.types.omds3Types.r1_4_0.common;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Type einzelner offener Schaden
 * 
 * <p>Java-Klasse für OffenerSchaden_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="OffenerSchaden_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Monat" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="Jahr" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OffenerSchaden_Type", propOrder = {
    "monat",
    "jahr"
})
public class OffenerSchadenType {

    @XmlElement(name = "Monat", required = true)
    protected BigInteger monat;
    @XmlElement(name = "Jahr", required = true)
    protected BigInteger jahr;

    /**
     * Ruft den Wert der monat-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMonat() {
        return monat;
    }

    /**
     * Legt den Wert der monat-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMonat(BigInteger value) {
        this.monat = value;
    }

    /**
     * Ruft den Wert der jahr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getJahr() {
        return jahr;
    }

    /**
     * Legt den Wert der jahr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setJahr(BigInteger value) {
        this.jahr = value;
    }

}
