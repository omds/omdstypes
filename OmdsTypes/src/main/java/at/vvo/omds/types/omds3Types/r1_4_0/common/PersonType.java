
package at.vvo.omds.types.omds3Types.r1_4_0.common;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds2Types.v2_11.ELAnzahlType;
import at.vvo.omds.types.omds2Types.v2_11.ELEinstufungType;
import at.vvo.omds.types.omds2Types.v2_11.ELEntscheidungsfrageType;
import at.vvo.omds.types.omds2Types.v2_11.ELIdentifizierungType;
import at.vvo.omds.types.omds2Types.v2_11.ELKommunikationType;
import at.vvo.omds.types.omds2Types.v2_11.ELLegitimationType;
import at.vvo.omds.types.omds2Types.v2_11.ELTextType;
import at.vvo.omds.types.omds2Types.v2_11.NATUERLICHEPERSONType;
import at.vvo.omds.types.omds2Types.v2_11.PersArtCdType;
import at.vvo.omds.types.omds2Types.v2_11.SONSTIGEPERSONType;


/**
 * Der Typ für eine Person mit ObjektId
 * 
 * <p>Java-Klasse für Person_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Person_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:omds3CommonServiceTypes-1-1-0}ObjektId" minOccurs="0"/&gt;
 *         &lt;choice&gt;
 *           &lt;element ref="{urn:omds20}NATUERLICHE_PERSON"/&gt;
 *           &lt;element ref="{urn:omds20}SONSTIGE_PERSON"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element ref="{urn:omds3CommonServiceTypes-1-1-0}Adresse" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:omds20}EL-Kommunikation" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:omds20}EL-Legitimation" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:omds20}EL-Anzahl" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:omds20}EL-Einstufung" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:omds20}EL-Entscheidungsfrage" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:omds20}EL-Identifizierung" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:omds20}EL-Text" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="PersArtCd" use="required" type="{urn:omds20}PersArtCd_Type" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Person_Type", propOrder = {
    "objektId",
    "natuerlicheperson",
    "sonstigeperson",
    "adresse",
    "elKommunikation",
    "elLegitimation",
    "elAnzahl",
    "elEinstufung",
    "elEntscheidungsfrage",
    "elIdentifizierung",
    "elText"
})
public class PersonType {

    @XmlElement(name = "ObjektId")
    protected ObjektIdType objektId;
    @XmlElement(name = "NATUERLICHE_PERSON", namespace = "urn:omds20")
    protected NATUERLICHEPERSONType natuerlicheperson;
    @XmlElement(name = "SONSTIGE_PERSON", namespace = "urn:omds20")
    protected SONSTIGEPERSONType sonstigeperson;
    @XmlElement(name = "Adresse")
    protected AdresseType adresse;
    @XmlElement(name = "EL-Kommunikation", namespace = "urn:omds20")
    protected List<ELKommunikationType> elKommunikation;
    @XmlElement(name = "EL-Legitimation", namespace = "urn:omds20")
    protected List<ELLegitimationType> elLegitimation;
    @XmlElement(name = "EL-Anzahl", namespace = "urn:omds20")
    protected List<ELAnzahlType> elAnzahl;
    @XmlElement(name = "EL-Einstufung", namespace = "urn:omds20")
    protected List<ELEinstufungType> elEinstufung;
    @XmlElement(name = "EL-Entscheidungsfrage", namespace = "urn:omds20")
    protected List<ELEntscheidungsfrageType> elEntscheidungsfrage;
    @XmlElement(name = "EL-Identifizierung", namespace = "urn:omds20")
    protected List<ELIdentifizierungType> elIdentifizierung;
    @XmlElement(name = "EL-Text", namespace = "urn:omds20")
    protected List<ELTextType> elText;
    @XmlAttribute(name = "PersArtCd", namespace = "urn:omds3CommonServiceTypes-1-1-0", required = true)
    protected PersArtCdType persArtCd;

    /**
     * Die Id der Person (entspricht der Personennr, hat aber auch die Möglichkeit eine ID von Service-Consumer Seite mitzugeben)
     * 
     * @return
     *     possible object is
     *     {@link ObjektIdType }
     *     
     */
    public ObjektIdType getObjektId() {
        return objektId;
    }

    /**
     * Legt den Wert der objektId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjektIdType }
     *     
     */
    public void setObjektId(ObjektIdType value) {
        this.objektId = value;
    }

    /**
     * Ruft den Wert der natuerlicheperson-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link NATUERLICHEPERSONType }
     *     
     */
    public NATUERLICHEPERSONType getNATUERLICHEPERSON() {
        return natuerlicheperson;
    }

    /**
     * Legt den Wert der natuerlicheperson-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link NATUERLICHEPERSONType }
     *     
     */
    public void setNATUERLICHEPERSON(NATUERLICHEPERSONType value) {
        this.natuerlicheperson = value;
    }

    /**
     * Ruft den Wert der sonstigeperson-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SONSTIGEPERSONType }
     *     
     */
    public SONSTIGEPERSONType getSONSTIGEPERSON() {
        return sonstigeperson;
    }

    /**
     * Legt den Wert der sonstigeperson-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SONSTIGEPERSONType }
     *     
     */
    public void setSONSTIGEPERSON(SONSTIGEPERSONType value) {
        this.sonstigeperson = value;
    }

    /**
     * Die Hauptadresse des Partners mit ObjektId
     * 
     * @return
     *     possible object is
     *     {@link AdresseType }
     *     
     */
    public AdresseType getAdresse() {
        return adresse;
    }

    /**
     * Legt den Wert der adresse-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AdresseType }
     *     
     */
    public void setAdresse(AdresseType value) {
        this.adresse = value;
    }

    /**
     * Gets the value of the elKommunikation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the elKommunikation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getELKommunikation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ELKommunikationType }
     * 
     * 
     */
    public List<ELKommunikationType> getELKommunikation() {
        if (elKommunikation == null) {
            elKommunikation = new ArrayList<ELKommunikationType>();
        }
        return this.elKommunikation;
    }

    /**
     * Gets the value of the elLegitimation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the elLegitimation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getELLegitimation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ELLegitimationType }
     * 
     * 
     */
    public List<ELLegitimationType> getELLegitimation() {
        if (elLegitimation == null) {
            elLegitimation = new ArrayList<ELLegitimationType>();
        }
        return this.elLegitimation;
    }

    /**
     * Gets the value of the elAnzahl property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the elAnzahl property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getELAnzahl().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ELAnzahlType }
     * 
     * 
     */
    public List<ELAnzahlType> getELAnzahl() {
        if (elAnzahl == null) {
            elAnzahl = new ArrayList<ELAnzahlType>();
        }
        return this.elAnzahl;
    }

    /**
     * Gets the value of the elEinstufung property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the elEinstufung property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getELEinstufung().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ELEinstufungType }
     * 
     * 
     */
    public List<ELEinstufungType> getELEinstufung() {
        if (elEinstufung == null) {
            elEinstufung = new ArrayList<ELEinstufungType>();
        }
        return this.elEinstufung;
    }

    /**
     * Gets the value of the elEntscheidungsfrage property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the elEntscheidungsfrage property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getELEntscheidungsfrage().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ELEntscheidungsfrageType }
     * 
     * 
     */
    public List<ELEntscheidungsfrageType> getELEntscheidungsfrage() {
        if (elEntscheidungsfrage == null) {
            elEntscheidungsfrage = new ArrayList<ELEntscheidungsfrageType>();
        }
        return this.elEntscheidungsfrage;
    }

    /**
     * Gets the value of the elIdentifizierung property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the elIdentifizierung property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getELIdentifizierung().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ELIdentifizierungType }
     * 
     * 
     */
    public List<ELIdentifizierungType> getELIdentifizierung() {
        if (elIdentifizierung == null) {
            elIdentifizierung = new ArrayList<ELIdentifizierungType>();
        }
        return this.elIdentifizierung;
    }

    /**
     * Gets the value of the elText property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the elText property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getELText().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ELTextType }
     * 
     * 
     */
    public List<ELTextType> getELText() {
        if (elText == null) {
            elText = new ArrayList<ELTextType>();
        }
        return this.elText;
    }

    /**
     * Ruft den Wert der persArtCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PersArtCdType }
     *     
     */
    public PersArtCdType getPersArtCd() {
        return persArtCd;
    }

    /**
     * Legt den Wert der persArtCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PersArtCdType }
     *     
     */
    public void setPersArtCd(PersArtCdType value) {
        this.persArtCd = value;
    }

}
