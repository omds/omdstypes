
package at.vvo.omds.types.omds3Types.r1_4_0.common;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für PolizzenversandType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="PolizzenversandType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="MAK2"/&gt;
 *     &lt;enumeration value="VN1"/&gt;
 *     &lt;enumeration value="MAKVN"/&gt;
 *     &lt;enumeration value="MAK1"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "PolizzenversandType")
@XmlEnum
public enum PolizzenversandType {


    /**
     *  2fach Makler: Ist nur bei Postversand möglich. Makler erhält Original und Kopie
     * 
     */
    @XmlEnumValue("MAK2")
    MAK_2("MAK2"),

    /**
     *  1fach Versicherungsnehmer: Kunde erhält Original
     * 
     */
    @XmlEnumValue("VN1")
    VN_1("VN1"),

    /**
     *  1fach Makler und 1fach Versicherungsnehmer: Kunde erhält Original und Makler die Kopie
     * 
     */
    MAKVN("MAKVN"),

    /**
     *  1fach Makler: Makler erhält Original
     * 
     */
    @XmlEnumValue("MAK1")
    MAK_1("MAK1");
    private final String value;

    PolizzenversandType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PolizzenversandType fromValue(String v) {
        for (PolizzenversandType c: PolizzenversandType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
