
package at.vvo.omds.types.omds3Types.r1_4_0.common;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds2Types.v2_11.WaehrungsCdType;


/**
 * Darstellung einer Praemie
 * 
 * <p>Java-Klasse für Praemie_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Praemie_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Zahlrhythmus" type="{urn:omds20}ZahlRhythmCd_Type"/&gt;
 *         &lt;element name="Zahlweg" type="{urn:omds20}ZahlWegCd_Type"/&gt;
 *         &lt;element name="PraemieNto" type="{urn:omds20}decimal"/&gt;
 *         &lt;element name="PraemieBto" type="{urn:omds20}decimal"/&gt;
 *         &lt;element name="Versicherungssteuer" type="{urn:omds3CommonServiceTypes-1-1-0}Versicherungssteuer_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="WaehrungsCd" type="{urn:omds20}WaehrungsCd_Type" minOccurs="0"/&gt;
 *         &lt;element name="Unterjaehrigkeitszuschlag" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/&gt;
 *         &lt;element name="Abschlag" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Praemie_Type", propOrder = {
    "zahlrhythmus",
    "zahlweg",
    "praemieNto",
    "praemieBto",
    "versicherungssteuer",
    "waehrungsCd",
    "unterjaehrigkeitszuschlag",
    "abschlag"
})
public class PraemieType {

    @XmlElement(name = "Zahlrhythmus", required = true)
    protected String zahlrhythmus;
    @XmlElement(name = "Zahlweg", required = true)
    protected String zahlweg;
    @XmlElement(name = "PraemieNto", required = true)
    protected BigDecimal praemieNto;
    @XmlElement(name = "PraemieBto", required = true)
    protected BigDecimal praemieBto;
    @XmlElement(name = "Versicherungssteuer")
    protected List<VersicherungssteuerType> versicherungssteuer;
    @XmlElement(name = "WaehrungsCd")
    @XmlSchemaType(name = "string")
    protected WaehrungsCdType waehrungsCd;
    @XmlElement(name = "Unterjaehrigkeitszuschlag")
    protected Double unterjaehrigkeitszuschlag;
    @XmlElement(name = "Abschlag")
    protected Double abschlag;

    /**
     * Ruft den Wert der zahlrhythmus-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZahlrhythmus() {
        return zahlrhythmus;
    }

    /**
     * Legt den Wert der zahlrhythmus-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZahlrhythmus(String value) {
        this.zahlrhythmus = value;
    }

    /**
     * Ruft den Wert der zahlweg-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZahlweg() {
        return zahlweg;
    }

    /**
     * Legt den Wert der zahlweg-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZahlweg(String value) {
        this.zahlweg = value;
    }

    /**
     * Ruft den Wert der praemieNto-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPraemieNto() {
        return praemieNto;
    }

    /**
     * Legt den Wert der praemieNto-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPraemieNto(BigDecimal value) {
        this.praemieNto = value;
    }

    /**
     * Ruft den Wert der praemieBto-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPraemieBto() {
        return praemieBto;
    }

    /**
     * Legt den Wert der praemieBto-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPraemieBto(BigDecimal value) {
        this.praemieBto = value;
    }

    /**
     * Gets the value of the versicherungssteuer property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the versicherungssteuer property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVersicherungssteuer().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link VersicherungssteuerType }
     * 
     * 
     */
    public List<VersicherungssteuerType> getVersicherungssteuer() {
        if (versicherungssteuer == null) {
            versicherungssteuer = new ArrayList<VersicherungssteuerType>();
        }
        return this.versicherungssteuer;
    }

    /**
     * Ruft den Wert der waehrungsCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link WaehrungsCdType }
     *     
     */
    public WaehrungsCdType getWaehrungsCd() {
        return waehrungsCd;
    }

    /**
     * Legt den Wert der waehrungsCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link WaehrungsCdType }
     *     
     */
    public void setWaehrungsCd(WaehrungsCdType value) {
        this.waehrungsCd = value;
    }

    /**
     * Ruft den Wert der unterjaehrigkeitszuschlag-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getUnterjaehrigkeitszuschlag() {
        return unterjaehrigkeitszuschlag;
    }

    /**
     * Legt den Wert der unterjaehrigkeitszuschlag-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setUnterjaehrigkeitszuschlag(Double value) {
        this.unterjaehrigkeitszuschlag = value;
    }

    /**
     * Ruft den Wert der abschlag-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getAbschlag() {
        return abschlag;
    }

    /**
     * Legt den Wert der abschlag-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setAbschlag(Double value) {
        this.abschlag = value;
    }

}
