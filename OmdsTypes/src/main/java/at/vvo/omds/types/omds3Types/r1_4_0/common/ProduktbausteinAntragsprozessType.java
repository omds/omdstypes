
package at.vvo.omds.types.omds3Types.r1_4_0.common;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Abstrakter Typ für alle Produktbausteine im Antragsprozess
 * 
 * <p>Java-Klasse für ProduktbausteinAntragsprozess_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ProduktbausteinAntragsprozess_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}Produktbaustein_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="VtgBeg" type="{urn:omds20}Datum-Zeit"/&gt;
 *         &lt;element name="VtgEnde" type="{urn:omds20}Datum-Zeit" minOccurs="0"/&gt;
 *         &lt;element name="Praemie" type="{urn:omds3CommonServiceTypes-1-1-0}Praemie_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="JahrespraemieNto" type="{urn:omds20}decimal"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProduktbausteinAntragsprozess_Type", propOrder = {
    "vtgBeg",
    "vtgEnde",
    "praemie",
    "jahrespraemieNto"
})
@XmlSeeAlso({
    VerkaufsproduktType.class,
    ProduktType.class,
    ElementarproduktType.class
})
public abstract class ProduktbausteinAntragsprozessType
    extends ProduktbausteinType
{

    @XmlElement(name = "VtgBeg", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar vtgBeg;
    @XmlElement(name = "VtgEnde")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar vtgEnde;
    @XmlElement(name = "Praemie")
    protected List<PraemieType> praemie;
    @XmlElement(name = "JahrespraemieNto", required = true)
    protected BigDecimal jahrespraemieNto;

    /**
     * Ruft den Wert der vtgBeg-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getVtgBeg() {
        return vtgBeg;
    }

    /**
     * Legt den Wert der vtgBeg-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setVtgBeg(XMLGregorianCalendar value) {
        this.vtgBeg = value;
    }

    /**
     * Ruft den Wert der vtgEnde-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getVtgEnde() {
        return vtgEnde;
    }

    /**
     * Legt den Wert der vtgEnde-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setVtgEnde(XMLGregorianCalendar value) {
        this.vtgEnde = value;
    }

    /**
     * Gets the value of the praemie property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the praemie property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPraemie().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PraemieType }
     * 
     * 
     */
    public List<PraemieType> getPraemie() {
        if (praemie == null) {
            praemie = new ArrayList<PraemieType>();
        }
        return this.praemie;
    }

    /**
     * Ruft den Wert der jahrespraemieNto-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getJahrespraemieNto() {
        return jahrespraemieNto;
    }

    /**
     * Legt den Wert der jahrespraemieNto-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setJahrespraemieNto(BigDecimal value) {
        this.jahrespraemieNto = value;
    }

}
