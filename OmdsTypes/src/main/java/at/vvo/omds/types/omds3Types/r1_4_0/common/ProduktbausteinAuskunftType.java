
package at.vvo.omds.types.omds3Types.r1_4_0.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Abstrakter Typ für alle Produktbausteine in Produktauskunftsservices
 * 
 * <p>Java-Klasse für ProduktbausteinAuskunft_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ProduktbausteinAuskunft_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}Produktbaustein_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Verkaufsbeginn" type="{urn:omds20}Datum-Zeit"/&gt;
 *         &lt;element name="Verkaufsende" type="{urn:omds20}Datum-Zeit" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProduktbausteinAuskunft_Type", propOrder = {
    "verkaufsbeginn",
    "verkaufsende"
})
public abstract class ProduktbausteinAuskunftType
    extends ProduktbausteinType
{

    @XmlElement(name = "Verkaufsbeginn", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar verkaufsbeginn;
    @XmlElement(name = "Verkaufsende")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar verkaufsende;

    /**
     * Ruft den Wert der verkaufsbeginn-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getVerkaufsbeginn() {
        return verkaufsbeginn;
    }

    /**
     * Legt den Wert der verkaufsbeginn-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setVerkaufsbeginn(XMLGregorianCalendar value) {
        this.verkaufsbeginn = value;
    }

    /**
     * Ruft den Wert der verkaufsende-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getVerkaufsende() {
        return verkaufsende;
    }

    /**
     * Legt den Wert der verkaufsende-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setVerkaufsende(XMLGregorianCalendar value) {
        this.verkaufsende = value;
    }

}
