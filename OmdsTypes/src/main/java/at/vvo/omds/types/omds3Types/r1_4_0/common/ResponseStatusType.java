
package at.vvo.omds.types.omds3Types.r1_4_0.common;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Statusinformationen im Response eines Serviceaufrufs
 * 
 * <p>Java-Klasse für ResponseStatus_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ResponseStatus_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="KorrelationsId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Ergebnis" type="{urn:omds3CommonServiceTypes-1-1-0}Status_Type"/&gt;
 *         &lt;element name="Meldungen" type="{urn:omds3CommonServiceTypes-1-1-0}ServiceFault" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResponseStatus_Type", propOrder = {
    "korrelationsId",
    "ergebnis",
    "meldungen"
})
public class ResponseStatusType {

    @XmlElement(name = "KorrelationsId", required = true)
    protected String korrelationsId;
    @XmlElement(name = "Ergebnis", required = true)
    @XmlSchemaType(name = "string")
    protected StatusType ergebnis;
    @XmlElement(name = "Meldungen")
    protected List<ServiceFault> meldungen;

    /**
     * Ruft den Wert der korrelationsId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKorrelationsId() {
        return korrelationsId;
    }

    /**
     * Legt den Wert der korrelationsId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKorrelationsId(String value) {
        this.korrelationsId = value;
    }

    /**
     * Ruft den Wert der ergebnis-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link StatusType }
     *     
     */
    public StatusType getErgebnis() {
        return ergebnis;
    }

    /**
     * Legt den Wert der ergebnis-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link StatusType }
     *     
     */
    public void setErgebnis(StatusType value) {
        this.ergebnis = value;
    }

    /**
     * Gets the value of the meldungen property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the meldungen property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMeldungen().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ServiceFault }
     * 
     * 
     */
    public List<ServiceFault> getMeldungen() {
        if (meldungen == null) {
            meldungen = new ArrayList<ServiceFault>();
        }
        return this.meldungen;
    }

}
