
package at.vvo.omds.types.omds3Types.r1_4_0.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Die Spezifikation eines Schadenobjektes
 * 
 * <p>Java-Klasse für SchadenObjektSpezifikation_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SchadenObjektSpezifikation_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}ObjektSpezifikation_Type"&gt;
 *       &lt;choice&gt;
 *         &lt;element ref="{urn:omds3CommonServiceTypes-1-1-0}Geschaeftsfallnummer"/&gt;
 *         &lt;element name="Schadennr" type="{urn:omds20}Schadennr"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SchadenObjektSpezifikation_Type", propOrder = {
    "geschaeftsfallnummer",
    "schadennr"
})
public class SchadenObjektSpezifikationType
    extends ObjektSpezifikationType
{

    @XmlElement(name = "Geschaeftsfallnummer")
    protected ObjektIdType geschaeftsfallnummer;
    @XmlElement(name = "Schadennr")
    protected String schadennr;

    /**
     * Eine Geschaeftsfallnummer
     * 
     * @return
     *     possible object is
     *     {@link ObjektIdType }
     *     
     */
    public ObjektIdType getGeschaeftsfallnummer() {
        return geschaeftsfallnummer;
    }

    /**
     * Legt den Wert der geschaeftsfallnummer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjektIdType }
     *     
     */
    public void setGeschaeftsfallnummer(ObjektIdType value) {
        this.geschaeftsfallnummer = value;
    }

    /**
     * Ruft den Wert der schadennr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSchadennr() {
        return schadennr;
    }

    /**
     * Legt den Wert der schadennr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSchadennr(String value) {
        this.schadennr = value;
    }

}
