
package at.vvo.omds.types.omds3Types.r1_4_0.common;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Dient zur Abbildung eines Selbstbehalts
 * 
 * <p>Java-Klasse für Selbstbehalt_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Selbstbehalt_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Selbstbehalt" type="{urn:omds20}decimal" minOccurs="0"/&gt;
 *         &lt;element name="SelbstbehaltProzentVs" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" minOccurs="0"/&gt;
 *         &lt;element name="SelbstbehaltMinBetrag" type="{urn:omds20}decimal" minOccurs="0"/&gt;
 *         &lt;element name="SelbstbehaltMaxBetrag" type="{urn:omds20}decimal" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Selbstbehalt_Type", propOrder = {
    "selbstbehalt",
    "selbstbehaltProzentVs",
    "selbstbehaltMinBetrag",
    "selbstbehaltMaxBetrag"
})
public class SelbstbehaltType {

    @XmlElement(name = "Selbstbehalt")
    protected BigDecimal selbstbehalt;
    @XmlElement(name = "SelbstbehaltProzentVs")
    @XmlSchemaType(name = "unsignedInt")
    protected Long selbstbehaltProzentVs;
    @XmlElement(name = "SelbstbehaltMinBetrag")
    protected BigDecimal selbstbehaltMinBetrag;
    @XmlElement(name = "SelbstbehaltMaxBetrag")
    protected BigDecimal selbstbehaltMaxBetrag;

    /**
     * Ruft den Wert der selbstbehalt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSelbstbehalt() {
        return selbstbehalt;
    }

    /**
     * Legt den Wert der selbstbehalt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSelbstbehalt(BigDecimal value) {
        this.selbstbehalt = value;
    }

    /**
     * Ruft den Wert der selbstbehaltProzentVs-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getSelbstbehaltProzentVs() {
        return selbstbehaltProzentVs;
    }

    /**
     * Legt den Wert der selbstbehaltProzentVs-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setSelbstbehaltProzentVs(Long value) {
        this.selbstbehaltProzentVs = value;
    }

    /**
     * Ruft den Wert der selbstbehaltMinBetrag-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSelbstbehaltMinBetrag() {
        return selbstbehaltMinBetrag;
    }

    /**
     * Legt den Wert der selbstbehaltMinBetrag-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSelbstbehaltMinBetrag(BigDecimal value) {
        this.selbstbehaltMinBetrag = value;
    }

    /**
     * Ruft den Wert der selbstbehaltMaxBetrag-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSelbstbehaltMaxBetrag() {
        return selbstbehaltMaxBetrag;
    }

    /**
     * Legt den Wert der selbstbehaltMaxBetrag-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSelbstbehaltMaxBetrag(BigDecimal value) {
        this.selbstbehaltMaxBetrag = value;
    }

}
