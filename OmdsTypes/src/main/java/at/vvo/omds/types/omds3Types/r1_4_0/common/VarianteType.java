
package at.vvo.omds.types.omds3Types.r1_4_0.common;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für Variante_Type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="Variante_Type"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Premiumschutz"/&gt;
 *     &lt;enumeration value="Classicschutz"/&gt;
 *     &lt;enumeration value="Basisschutz"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "Variante_Type")
@XmlEnum
public enum VarianteType {

    @XmlEnumValue("Premiumschutz")
    PREMIUMSCHUTZ("Premiumschutz"),
    @XmlEnumValue("Classicschutz")
    CLASSICSCHUTZ("Classicschutz"),
    @XmlEnumValue("Basisschutz")
    BASISSCHUTZ("Basisschutz");
    private final String value;

    VarianteType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static VarianteType fromValue(String v) {
        for (VarianteType c: VarianteType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
