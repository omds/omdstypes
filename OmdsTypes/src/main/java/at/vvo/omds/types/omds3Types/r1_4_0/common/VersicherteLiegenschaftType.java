
package at.vvo.omds.types.omds3Types.r1_4_0.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds2Types.v2_11.ADRESSEType;


/**
 * Versicherte Liegenschaft
 * 
 * <p>Java-Klasse für VersicherteLiegenschaft_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="VersicherteLiegenschaft_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}VersichertesInteresse_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:omds3CommonServiceTypes-1-1-0}ObjektId"/&gt;
 *         &lt;element name="Adresse" type="{urn:omds20}ADRESSE_Type"/&gt;
 *         &lt;element name="BebauteFlaecheInQm" type="{http://www.w3.org/2001/XMLSchema}unsignedInt"/&gt;
 *         &lt;element name="UeberdachteFlaecheInQm" type="{http://www.w3.org/2001/XMLSchema}unsignedInt"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VersicherteLiegenschaft_Type", propOrder = {
    "objektId",
    "adresse",
    "bebauteFlaecheInQm",
    "ueberdachteFlaecheInQm"
})
public class VersicherteLiegenschaftType
    extends VersichertesInteresseType
{

    @XmlElement(name = "ObjektId", required = true)
    protected ObjektIdType objektId;
    @XmlElement(name = "Adresse", required = true)
    protected ADRESSEType adresse;
    @XmlElement(name = "BebauteFlaecheInQm")
    @XmlSchemaType(name = "unsignedInt")
    protected long bebauteFlaecheInQm;
    @XmlElement(name = "UeberdachteFlaecheInQm")
    @XmlSchemaType(name = "unsignedInt")
    protected long ueberdachteFlaecheInQm;

    /**
     * Ruft den Wert der objektId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ObjektIdType }
     *     
     */
    public ObjektIdType getObjektId() {
        return objektId;
    }

    /**
     * Legt den Wert der objektId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjektIdType }
     *     
     */
    public void setObjektId(ObjektIdType value) {
        this.objektId = value;
    }

    /**
     * Ruft den Wert der adresse-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ADRESSEType }
     *     
     */
    public ADRESSEType getAdresse() {
        return adresse;
    }

    /**
     * Legt den Wert der adresse-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ADRESSEType }
     *     
     */
    public void setAdresse(ADRESSEType value) {
        this.adresse = value;
    }

    /**
     * Ruft den Wert der bebauteFlaecheInQm-Eigenschaft ab.
     * 
     */
    public long getBebauteFlaecheInQm() {
        return bebauteFlaecheInQm;
    }

    /**
     * Legt den Wert der bebauteFlaecheInQm-Eigenschaft fest.
     * 
     */
    public void setBebauteFlaecheInQm(long value) {
        this.bebauteFlaecheInQm = value;
    }

    /**
     * Ruft den Wert der ueberdachteFlaecheInQm-Eigenschaft ab.
     * 
     */
    public long getUeberdachteFlaecheInQm() {
        return ueberdachteFlaecheInQm;
    }

    /**
     * Legt den Wert der ueberdachteFlaecheInQm-Eigenschaft fest.
     * 
     */
    public void setUeberdachteFlaecheInQm(long value) {
        this.ueberdachteFlaecheInQm = value;
    }

}
