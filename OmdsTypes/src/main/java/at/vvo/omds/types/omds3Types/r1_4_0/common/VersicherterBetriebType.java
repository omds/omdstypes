
package at.vvo.omds.types.omds3Types.r1_4_0.common;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds2Types.v2_11.ADRESSEType;


/**
 * Versicherter Betrieb (Landwirtschaftlicher Betrieb, Verein)
 * 
 * <p>Java-Klasse für VersicherterBetrieb_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="VersicherterBetrieb_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}VersichertesInteresse_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:omds3CommonServiceTypes-1-1-0}ObjektId" minOccurs="0"/&gt;
 *         &lt;element name="Adresse" type="{urn:omds20}ADRESSE_Type"/&gt;
 *         &lt;element name="Betriebsart" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded"/&gt;
 *         &lt;element name="Mitarbeiteranzahl" type="{http://www.w3.org/2001/XMLSchema}unsignedInt"/&gt;
 *         &lt;element name="Umsatz" type="{http://www.w3.org/2001/XMLSchema}unsignedLong"/&gt;
 *         &lt;element name="FlaecheInHektar" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" minOccurs="0"/&gt;
 *         &lt;element name="Mitgliederanzahl" type="{http://www.w3.org/2001/XMLSchema}unsignedLong" minOccurs="0"/&gt;
 *         &lt;element name="ZusaetzlicheBetriebsdaten" type="{urn:omds3CommonServiceTypes-1-1-0}ZusaetzlicheBetriebsdaten_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VersicherterBetrieb_Type", propOrder = {
    "objektId",
    "adresse",
    "betriebsart",
    "mitarbeiteranzahl",
    "umsatz",
    "flaecheInHektar",
    "mitgliederanzahl",
    "zusaetzlicheBetriebsdaten"
})
public class VersicherterBetriebType
    extends VersichertesInteresseType
{

    @XmlElement(name = "ObjektId")
    protected ObjektIdType objektId;
    @XmlElement(name = "Adresse", required = true)
    protected ADRESSEType adresse;
    @XmlElement(name = "Betriebsart", required = true)
    protected List<String> betriebsart;
    @XmlElement(name = "Mitarbeiteranzahl")
    @XmlSchemaType(name = "unsignedInt")
    protected long mitarbeiteranzahl;
    @XmlElement(name = "Umsatz", required = true)
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger umsatz;
    @XmlElement(name = "FlaecheInHektar")
    @XmlSchemaType(name = "unsignedInt")
    protected Long flaecheInHektar;
    @XmlElement(name = "Mitgliederanzahl")
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger mitgliederanzahl;
    @XmlElement(name = "ZusaetzlicheBetriebsdaten")
    protected List<ZusaetzlicheBetriebsdatenType> zusaetzlicheBetriebsdaten;

    /**
     * Ruft den Wert der objektId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ObjektIdType }
     *     
     */
    public ObjektIdType getObjektId() {
        return objektId;
    }

    /**
     * Legt den Wert der objektId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjektIdType }
     *     
     */
    public void setObjektId(ObjektIdType value) {
        this.objektId = value;
    }

    /**
     * Ruft den Wert der adresse-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ADRESSEType }
     *     
     */
    public ADRESSEType getAdresse() {
        return adresse;
    }

    /**
     * Legt den Wert der adresse-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ADRESSEType }
     *     
     */
    public void setAdresse(ADRESSEType value) {
        this.adresse = value;
    }

    /**
     * Gets the value of the betriebsart property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the betriebsart property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBetriebsart().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getBetriebsart() {
        if (betriebsart == null) {
            betriebsart = new ArrayList<String>();
        }
        return this.betriebsart;
    }

    /**
     * Ruft den Wert der mitarbeiteranzahl-Eigenschaft ab.
     * 
     */
    public long getMitarbeiteranzahl() {
        return mitarbeiteranzahl;
    }

    /**
     * Legt den Wert der mitarbeiteranzahl-Eigenschaft fest.
     * 
     */
    public void setMitarbeiteranzahl(long value) {
        this.mitarbeiteranzahl = value;
    }

    /**
     * Ruft den Wert der umsatz-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getUmsatz() {
        return umsatz;
    }

    /**
     * Legt den Wert der umsatz-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setUmsatz(BigInteger value) {
        this.umsatz = value;
    }

    /**
     * Ruft den Wert der flaecheInHektar-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getFlaecheInHektar() {
        return flaecheInHektar;
    }

    /**
     * Legt den Wert der flaecheInHektar-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setFlaecheInHektar(Long value) {
        this.flaecheInHektar = value;
    }

    /**
     * Ruft den Wert der mitgliederanzahl-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMitgliederanzahl() {
        return mitgliederanzahl;
    }

    /**
     * Legt den Wert der mitgliederanzahl-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMitgliederanzahl(BigInteger value) {
        this.mitgliederanzahl = value;
    }

    /**
     * Gets the value of the zusaetzlicheBetriebsdaten property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the zusaetzlicheBetriebsdaten property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getZusaetzlicheBetriebsdaten().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ZusaetzlicheBetriebsdatenType }
     * 
     * 
     */
    public List<ZusaetzlicheBetriebsdatenType> getZusaetzlicheBetriebsdaten() {
        if (zusaetzlicheBetriebsdaten == null) {
            zusaetzlicheBetriebsdaten = new ArrayList<ZusaetzlicheBetriebsdatenType>();
        }
        return this.zusaetzlicheBetriebsdaten;
    }

}
