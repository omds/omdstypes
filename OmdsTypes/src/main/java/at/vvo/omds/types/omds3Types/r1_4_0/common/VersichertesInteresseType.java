
package at.vvo.omds.types.omds3Types.r1_4_0.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.sachPrivat.RisikoGebaeudeType;
import at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.sachPrivat.RisikoHaushaltType;
import at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.sachPrivat.VersichertesObjektSachPrivatType;


/**
 * Abstrakter Obertyp für versicherte Interessen
 * 
 * <p>Java-Klasse für VersichertesInteresse_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="VersichertesInteresse_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="Lfnr" use="required" type="{http://www.w3.org/2001/XMLSchema}unsignedShort" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VersichertesInteresse_Type")
@XmlSeeAlso({
    VersichertePersonType.class,
    VersicherteVeranstaltungType.class,
    FahrzeugType.class,
    VersicherterBetriebType.class,
    VersicherteLiegenschaftType.class,
    VersichertesObjektSachPrivatType.class,
    RisikoHaushaltType.class,
    RisikoGebaeudeType.class
})
public abstract class VersichertesInteresseType {

    @XmlAttribute(name = "Lfnr", namespace = "urn:omds3CommonServiceTypes-1-1-0", required = true)
    @XmlSchemaType(name = "unsignedShort")
    protected int lfnr;

    /**
     * Ruft den Wert der lfnr-Eigenschaft ab.
     * 
     */
    public int getLfnr() {
        return lfnr;
    }

    /**
     * Legt den Wert der lfnr-Eigenschaft fest.
     * 
     */
    public void setLfnr(int value) {
        this.lfnr = value;
    }

}
