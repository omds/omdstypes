
package at.vvo.omds.types.omds3Types.r1_4_0.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds2Types.v2_11.VtgRolleCdType;


/**
 * Definiert Vertragspersonen als Referenz auf eine Person plus Vertragsrolle
 * 
 * <p>Java-Klasse für Vertragsperson_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Vertragsperson_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="LfdNr" type="{http://www.w3.org/2001/XMLSchema}unsignedInt"/&gt;
 *         &lt;element name="Rolle" type="{urn:omds20}VtgRolleCd_Type"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Vertragsperson_Type", propOrder = {
    "lfdNr",
    "rolle"
})
public class VertragspersonType {

    @XmlElement(name = "LfdNr")
    @XmlSchemaType(name = "unsignedInt")
    protected long lfdNr;
    @XmlElement(name = "Rolle", required = true)
    @XmlSchemaType(name = "string")
    protected VtgRolleCdType rolle;

    /**
     * Ruft den Wert der lfdNr-Eigenschaft ab.
     * 
     */
    public long getLfdNr() {
        return lfdNr;
    }

    /**
     * Legt den Wert der lfdNr-Eigenschaft fest.
     * 
     */
    public void setLfdNr(long value) {
        this.lfdNr = value;
    }

    /**
     * Ruft den Wert der rolle-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VtgRolleCdType }
     *     
     */
    public VtgRolleCdType getRolle() {
        return rolle;
    }

    /**
     * Legt den Wert der rolle-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VtgRolleCdType }
     *     
     */
    public void setRolle(VtgRolleCdType value) {
        this.rolle = value;
    }

}
