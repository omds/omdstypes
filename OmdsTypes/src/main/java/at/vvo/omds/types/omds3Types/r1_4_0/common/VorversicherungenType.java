
package at.vvo.omds.types.omds3Types.r1_4_0.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Type Vorversicherungen
 * 
 * <p>Java-Klasse für Vorversicherungen_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Vorversicherungen_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="VorversicherungKfz" type="{urn:omds3CommonServiceTypes-1-1-0}VorversicherungenDetail_Type" minOccurs="0"/&gt;
 *         &lt;element name="VorversicherungRechtsschutz" type="{urn:omds3CommonServiceTypes-1-1-0}VorversicherungenDetail_Type" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Vorversicherungen_Type", propOrder = {
    "vorversicherungKfz",
    "vorversicherungRechtsschutz"
})
public class VorversicherungenType {

    @XmlElement(name = "VorversicherungKfz")
    protected VorversicherungenDetailType vorversicherungKfz;
    @XmlElement(name = "VorversicherungRechtsschutz")
    protected VorversicherungenDetailType vorversicherungRechtsschutz;

    /**
     * Ruft den Wert der vorversicherungKfz-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VorversicherungenDetailType }
     *     
     */
    public VorversicherungenDetailType getVorversicherungKfz() {
        return vorversicherungKfz;
    }

    /**
     * Legt den Wert der vorversicherungKfz-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VorversicherungenDetailType }
     *     
     */
    public void setVorversicherungKfz(VorversicherungenDetailType value) {
        this.vorversicherungKfz = value;
    }

    /**
     * Ruft den Wert der vorversicherungRechtsschutz-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VorversicherungenDetailType }
     *     
     */
    public VorversicherungenDetailType getVorversicherungRechtsschutz() {
        return vorversicherungRechtsschutz;
    }

    /**
     * Legt den Wert der vorversicherungRechtsschutz-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VorversicherungenDetailType }
     *     
     */
    public void setVorversicherungRechtsschutz(VorversicherungenDetailType value) {
        this.vorversicherungRechtsschutz = value;
    }

}
