
package at.vvo.omds.types.omds3Types.r1_4_0.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Typ Zahlweg
 * 
 * <p>Java-Klasse für Zahlweg_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Zahlweg_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice&gt;
 *         &lt;element name="Zahlungsanweisung" type="{http://www.w3.org/2001/XMLSchema}anyType"/&gt;
 *         &lt;element name="Lastschrift" type="{urn:omds3CommonServiceTypes-1-1-0}Bankverbindung_Type"/&gt;
 *         &lt;element name="Kundenkonto"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;attribute name="Kundenkontonummer" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Kreditkarte" type="{urn:omds3CommonServiceTypes-1-1-0}Kreditkarte_Type"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Zahlweg_Type", propOrder = {
    "zahlungsanweisung",
    "lastschrift",
    "kundenkonto",
    "kreditkarte"
})
public class ZahlwegType {

    @XmlElement(name = "Zahlungsanweisung")
    protected Object zahlungsanweisung;
    @XmlElement(name = "Lastschrift")
    protected BankverbindungType lastschrift;
    @XmlElement(name = "Kundenkonto")
    protected Kundenkonto kundenkonto;
    @XmlElement(name = "Kreditkarte")
    protected KreditkarteType kreditkarte;

    /**
     * Ruft den Wert der zahlungsanweisung-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getZahlungsanweisung() {
        return zahlungsanweisung;
    }

    /**
     * Legt den Wert der zahlungsanweisung-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setZahlungsanweisung(Object value) {
        this.zahlungsanweisung = value;
    }

    /**
     * Ruft den Wert der lastschrift-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BankverbindungType }
     *     
     */
    public BankverbindungType getLastschrift() {
        return lastschrift;
    }

    /**
     * Legt den Wert der lastschrift-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BankverbindungType }
     *     
     */
    public void setLastschrift(BankverbindungType value) {
        this.lastschrift = value;
    }

    /**
     * Ruft den Wert der kundenkonto-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Kundenkonto }
     *     
     */
    public Kundenkonto getKundenkonto() {
        return kundenkonto;
    }

    /**
     * Legt den Wert der kundenkonto-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Kundenkonto }
     *     
     */
    public void setKundenkonto(Kundenkonto value) {
        this.kundenkonto = value;
    }

    /**
     * Ruft den Wert der kreditkarte-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link KreditkarteType }
     *     
     */
    public KreditkarteType getKreditkarte() {
        return kreditkarte;
    }

    /**
     * Legt den Wert der kreditkarte-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link KreditkarteType }
     *     
     */
    public void setKreditkarte(KreditkarteType value) {
        this.kreditkarte = value;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;attribute name="Kundenkontonummer" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Kundenkonto {

        @XmlAttribute(name = "Kundenkontonummer", namespace = "urn:omds3CommonServiceTypes-1-1-0", required = true)
        protected String kundenkontonummer;

        /**
         * Ruft den Wert der kundenkontonummer-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getKundenkontonummer() {
            return kundenkontonummer;
        }

        /**
         * Legt den Wert der kundenkontonummer-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setKundenkontonummer(String value) {
            this.kundenkontonummer = value;
        }

    }

}
