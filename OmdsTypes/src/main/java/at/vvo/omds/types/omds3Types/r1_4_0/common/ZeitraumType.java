
package at.vvo.omds.types.omds3Types.r1_4_0.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java-Klasse für Zeitraum_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Zeitraum_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="ZRBeg" type="{urn:omds20}Datum" /&gt;
 *       &lt;attribute name="ZREnd" type="{urn:omds20}Datum" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Zeitraum_Type")
public class ZeitraumType {

    @XmlAttribute(name = "ZRBeg", namespace = "urn:omds3CommonServiceTypes-1-1-0")
    protected XMLGregorianCalendar zrBeg;
    @XmlAttribute(name = "ZREnd", namespace = "urn:omds3CommonServiceTypes-1-1-0")
    protected XMLGregorianCalendar zrEnd;

    /**
     * Ruft den Wert der zrBeg-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getZRBeg() {
        return zrBeg;
    }

    /**
     * Legt den Wert der zrBeg-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setZRBeg(XMLGregorianCalendar value) {
        this.zrBeg = value;
    }

    /**
     * Ruft den Wert der zrEnd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getZREnd() {
        return zrEnd;
    }

    /**
     * Legt den Wert der zrEnd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setZREnd(XMLGregorianCalendar value) {
        this.zrEnd = value;
    }

}
