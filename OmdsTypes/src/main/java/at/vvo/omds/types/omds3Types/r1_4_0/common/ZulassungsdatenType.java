
package at.vvo.omds.types.omds3Types.r1_4_0.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Typ für Zulassungsdaten
 * 
 * <p>Java-Klasse für Zulassungsdaten_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Zulassungsdaten_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="VBNummer" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Kennzeichen" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Fahrgestellnummer" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Zulassungsdaten_Type", propOrder = {
    "vbNummer",
    "kennzeichen",
    "fahrgestellnummer"
})
public class ZulassungsdatenType {

    @XmlElement(name = "VBNummer")
    protected String vbNummer;
    @XmlElement(name = "Kennzeichen")
    protected String kennzeichen;
    @XmlElement(name = "Fahrgestellnummer")
    protected String fahrgestellnummer;

    /**
     * Ruft den Wert der vbNummer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVBNummer() {
        return vbNummer;
    }

    /**
     * Legt den Wert der vbNummer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVBNummer(String value) {
        this.vbNummer = value;
    }

    /**
     * Ruft den Wert der kennzeichen-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKennzeichen() {
        return kennzeichen;
    }

    /**
     * Legt den Wert der kennzeichen-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKennzeichen(String value) {
        this.kennzeichen = value;
    }

    /**
     * Ruft den Wert der fahrgestellnummer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFahrgestellnummer() {
        return fahrgestellnummer;
    }

    /**
     * Legt den Wert der fahrgestellnummer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFahrgestellnummer(String value) {
        this.fahrgestellnummer = value;
    }

}
