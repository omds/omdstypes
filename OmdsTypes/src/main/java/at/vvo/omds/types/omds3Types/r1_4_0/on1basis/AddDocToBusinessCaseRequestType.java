
package at.vvo.omds.types.omds3Types.r1_4_0.on1basis;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.ObjektIdType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.UploadDokumentType;


/**
 * Typ des Requestobjekts, um Dokument zu Geschäftsfall hinzuzufügen
 * 
 * <p>Java-Klasse für AddDocToBusinessCaseRequest_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="AddDocToBusinessCaseRequest_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="VUNr" type="{urn:omds20}VUNr"/&gt;
 *         &lt;element name="Geschaeftsfallnummer" type="{urn:omds3CommonServiceTypes-1-1-0}ObjektId_Type"/&gt;
 *         &lt;element name="Dokument" type="{urn:omds3CommonServiceTypes-1-1-0}Upload_Dokument_Type"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AddDocToBusinessCaseRequest_Type", propOrder = {
    "vuNr",
    "geschaeftsfallnummer",
    "dokument"
})
public class AddDocToBusinessCaseRequestType {

    @XmlElement(name = "VUNr", required = true)
    protected String vuNr;
    @XmlElement(name = "Geschaeftsfallnummer", required = true)
    protected ObjektIdType geschaeftsfallnummer;
    @XmlElement(name = "Dokument", required = true)
    protected UploadDokumentType dokument;

    /**
     * Ruft den Wert der vuNr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVUNr() {
        return vuNr;
    }

    /**
     * Legt den Wert der vuNr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVUNr(String value) {
        this.vuNr = value;
    }

    /**
     * Ruft den Wert der geschaeftsfallnummer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ObjektIdType }
     *     
     */
    public ObjektIdType getGeschaeftsfallnummer() {
        return geschaeftsfallnummer;
    }

    /**
     * Legt den Wert der geschaeftsfallnummer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjektIdType }
     *     
     */
    public void setGeschaeftsfallnummer(ObjektIdType value) {
        this.geschaeftsfallnummer = value;
    }

    /**
     * Ruft den Wert der dokument-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link UploadDokumentType }
     *     
     */
    public UploadDokumentType getDokument() {
        return dokument;
    }

    /**
     * Legt den Wert der dokument-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link UploadDokumentType }
     *     
     */
    public void setDokument(UploadDokumentType value) {
        this.dokument = value;
    }

}
