
package at.vvo.omds.types.omds3Types.r1_4_0.on1basis;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.DokumentenReferenzType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.ServiceFault;


/**
 * Response von der VU, wenn ein zusätzliches Dokument übergeben wurde
 * 
 * <p>Java-Klasse für AddDocToBusinessCaseResponse_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="AddDocToBusinessCaseResponse_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice maxOccurs="unbounded"&gt;
 *         &lt;element name="DocRef" type="{urn:omds3CommonServiceTypes-1-1-0}DokumentenReferenz_Type"/&gt;
 *         &lt;element name="ServiceFault" type="{urn:omds3CommonServiceTypes-1-1-0}ServiceFault" maxOccurs="unbounded"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AddDocToBusinessCaseResponse_Type", propOrder = {
    "docRefOrServiceFault"
})
public class AddDocToBusinessCaseResponseType {

    @XmlElements({
        @XmlElement(name = "DocRef", type = DokumentenReferenzType.class),
        @XmlElement(name = "ServiceFault", type = ServiceFault.class)
    })
    protected List<Object> docRefOrServiceFault;

    /**
     * Gets the value of the docRefOrServiceFault property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the docRefOrServiceFault property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDocRefOrServiceFault().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DokumentenReferenzType }
     * {@link ServiceFault }
     * 
     * 
     */
    public List<Object> getDocRefOrServiceFault() {
        if (docRefOrServiceFault == null) {
            docRefOrServiceFault = new ArrayList<Object>();
        }
        return this.docRefOrServiceFault;
    }

}
