
package at.vvo.omds.types.omds3Types.r1_4_0.on1basis;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Typ fuer die Bekanntgabe von Statusänderungen
 * 
 * <p>Java-Klasse für DeclareStateChangesRequest_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="DeclareStateChangesRequest_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="StateChange" type="{urn:at.vvo.omds.types.omds3types.v1-3-0.on1basisfunktionen}AbstractStateChangeEvent_Type" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DeclareStateChangesRequest_Type", propOrder = {
    "stateChange"
})
public class DeclareStateChangesRequestType {

    @XmlElement(name = "StateChange", required = true)
    protected List<AbstractStateChangeEventType> stateChange;

    /**
     * Gets the value of the stateChange property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the stateChange property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStateChange().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AbstractStateChangeEventType }
     * 
     * 
     */
    public List<AbstractStateChangeEventType> getStateChange() {
        if (stateChange == null) {
            stateChange = new ArrayList<AbstractStateChangeEventType>();
        }
        return this.stateChange;
    }

}
