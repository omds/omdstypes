
package at.vvo.omds.types.omds3Types.r1_4_0.on1basis;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.AuthorizationFilter;
import at.vvo.omds.types.omds3Types.r1_4_0.common.ObjektSpezifikationType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.ZeitraumType;


/**
 * Typ mit Elementen fuer die Anfrage nach Dokumenten zu einem fachlichen Objekt
 * 
 * <p>Java-Klasse für GetDocumentsOfObjectRequest_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="GetDocumentsOfObjectRequest_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="VUNr" type="{urn:omds20}VUNr"/&gt;
 *         &lt;element name="AuthFilter" type="{urn:omds3CommonServiceTypes-1-1-0}AuthorizationFilter" minOccurs="0"/&gt;
 *         &lt;element name="ObjektSpezifikation" type="{urn:omds3CommonServiceTypes-1-1-0}ObjektSpezifikation_Type"/&gt;
 *         &lt;element name="Zeitraum" type="{urn:omds3CommonServiceTypes-1-1-0}Zeitraum_Type" minOccurs="0"/&gt;
 *         &lt;element name="DokumentType" type="{urn:omds3CommonServiceTypes-1-1-0}DocumentType" minOccurs="0"/&gt;
 *         &lt;element name="MaxResults" type="{http://www.w3.org/2001/XMLSchema}unsignedShort" minOccurs="0"/&gt;
 *         &lt;element name="Offset" type="{http://www.w3.org/2001/XMLSchema}unsignedShort" minOccurs="0"/&gt;
 *         &lt;element name="OrderBy" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;enumeration value="Erstellungsdatum aufsteigend"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetDocumentsOfObjectRequest_Type", propOrder = {
    "vuNr",
    "authFilter",
    "objektSpezifikation",
    "zeitraum",
    "dokumentType",
    "maxResults",
    "offset",
    "orderBy"
})
public class GetDocumentsOfObjectRequestType {

    @XmlElement(name = "VUNr", required = true)
    protected String vuNr;
    @XmlElement(name = "AuthFilter")
    protected AuthorizationFilter authFilter;
    @XmlElement(name = "ObjektSpezifikation", required = true)
    protected ObjektSpezifikationType objektSpezifikation;
    @XmlElement(name = "Zeitraum")
    protected ZeitraumType zeitraum;
    @XmlElement(name = "DokumentType")
    protected Integer dokumentType;
    @XmlElement(name = "MaxResults")
    @XmlSchemaType(name = "unsignedShort")
    protected Integer maxResults;
    @XmlElement(name = "Offset")
    @XmlSchemaType(name = "unsignedShort")
    protected Integer offset;
    @XmlElement(name = "OrderBy")
    protected String orderBy;

    /**
     * Ruft den Wert der vuNr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVUNr() {
        return vuNr;
    }

    /**
     * Legt den Wert der vuNr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVUNr(String value) {
        this.vuNr = value;
    }

    /**
     * Ruft den Wert der authFilter-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AuthorizationFilter }
     *     
     */
    public AuthorizationFilter getAuthFilter() {
        return authFilter;
    }

    /**
     * Legt den Wert der authFilter-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AuthorizationFilter }
     *     
     */
    public void setAuthFilter(AuthorizationFilter value) {
        this.authFilter = value;
    }

    /**
     * Ruft den Wert der objektSpezifikation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ObjektSpezifikationType }
     *     
     */
    public ObjektSpezifikationType getObjektSpezifikation() {
        return objektSpezifikation;
    }

    /**
     * Legt den Wert der objektSpezifikation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjektSpezifikationType }
     *     
     */
    public void setObjektSpezifikation(ObjektSpezifikationType value) {
        this.objektSpezifikation = value;
    }

    /**
     * Ruft den Wert der zeitraum-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ZeitraumType }
     *     
     */
    public ZeitraumType getZeitraum() {
        return zeitraum;
    }

    /**
     * Legt den Wert der zeitraum-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ZeitraumType }
     *     
     */
    public void setZeitraum(ZeitraumType value) {
        this.zeitraum = value;
    }

    /**
     * Ruft den Wert der dokumentType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDokumentType() {
        return dokumentType;
    }

    /**
     * Legt den Wert der dokumentType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDokumentType(Integer value) {
        this.dokumentType = value;
    }

    /**
     * Ruft den Wert der maxResults-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxResults() {
        return maxResults;
    }

    /**
     * Legt den Wert der maxResults-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxResults(Integer value) {
        this.maxResults = value;
    }

    /**
     * Ruft den Wert der offset-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOffset() {
        return offset;
    }

    /**
     * Legt den Wert der offset-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOffset(Integer value) {
        this.offset = value;
    }

    /**
     * Ruft den Wert der orderBy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderBy() {
        return orderBy;
    }

    /**
     * Legt den Wert der orderBy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderBy(String value) {
        this.orderBy = value;
    }

}
