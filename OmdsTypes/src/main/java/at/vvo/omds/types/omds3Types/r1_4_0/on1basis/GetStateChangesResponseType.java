
package at.vvo.omds.types.omds3Types.r1_4_0.on1basis;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.CommonSearchResponseType;


/**
 * Typ des Responseobjektes um Geschäftsfalle abzuholen
 * 
 * <p>Java-Klasse für GetStateChangesResponse_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="GetStateChangesResponse_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}CommonSearchResponse_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Event" type="{urn:at.vvo.omds.types.omds3types.v1-3-0.on1basisfunktionen}AbstractStateChangeEvent_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetStateChangesResponse_Type", propOrder = {
    "event"
})
public class GetStateChangesResponseType
    extends CommonSearchResponseType
{

    @XmlElement(name = "Event")
    protected List<AbstractStateChangeEventType> event;

    /**
     * Gets the value of the event property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the event property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEvent().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AbstractStateChangeEventType }
     * 
     * 
     */
    public List<AbstractStateChangeEventType> getEvent() {
        if (event == null) {
            event = new ArrayList<AbstractStateChangeEventType>();
        }
        return this.event;
    }

}
