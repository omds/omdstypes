
package at.vvo.omds.types.omds3Types.r1_4_0.on1basis;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Response-Type zum Bezug eines Securtity-Context-Tokens
 * 
 * <p>Java-Klasse für SecurityContextTokenResponse_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SecurityContextTokenResponse_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Token" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="DauerhaftGueltig" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="GueltigBis" type="{urn:omds20}Datum-Zeit" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SecurityContextTokenResponse_Type", propOrder = {
    "token",
    "dauerhaftGueltig",
    "gueltigBis"
})
public class SecurityContextTokenResponseType {

    @XmlElement(name = "Token", required = true)
    protected String token;
    @XmlElement(name = "DauerhaftGueltig")
    protected boolean dauerhaftGueltig;
    @XmlElement(name = "GueltigBis")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar gueltigBis;

    /**
     * Ruft den Wert der token-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getToken() {
        return token;
    }

    /**
     * Legt den Wert der token-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToken(String value) {
        this.token = value;
    }

    /**
     * Ruft den Wert der dauerhaftGueltig-Eigenschaft ab.
     * 
     */
    public boolean isDauerhaftGueltig() {
        return dauerhaftGueltig;
    }

    /**
     * Legt den Wert der dauerhaftGueltig-Eigenschaft fest.
     * 
     */
    public void setDauerhaftGueltig(boolean value) {
        this.dauerhaftGueltig = value;
    }

    /**
     * Ruft den Wert der gueltigBis-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getGueltigBis() {
        return gueltigBis;
    }

    /**
     * Legt den Wert der gueltigBis-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setGueltigBis(XMLGregorianCalendar value) {
        this.gueltigBis = value;
    }

}
