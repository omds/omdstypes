
package at.vvo.omds.types.omds3Types.r1_4_0.on1basis;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import at.vvo.omds.types.omds3Types.r1_4_0.common.GeschaeftsobjektArtType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.ObjektIdType;


/**
 * Standard-Event einer Statusänderung
 * 
 * <p>Java-Klasse für StateChangeEvent_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="StateChangeEvent_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on1basisfunktionen}AbstractStateChangeEvent_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Objektart" type="{urn:omds3CommonServiceTypes-1-1-0}GeschaeftsobjektArt_Type"/&gt;
 *         &lt;element name="ObjektId" type="{urn:omds3CommonServiceTypes-1-1-0}ObjektId_Type"/&gt;
 *         &lt;element name="Geschaeftsfallnummer" type="{urn:omds3CommonServiceTypes-1-1-0}ObjektId_Type" minOccurs="0"/&gt;
 *         &lt;element name="GeschaeftsfallArt" type="{urn:omds3CommonServiceTypes-1-1-0}GeschaeftsfallArt_Type" minOccurs="0"/&gt;
 *         &lt;element name="Aenderungsdatum" type="{urn:omds20}Datum-Zeit"/&gt;
 *         &lt;element name="StatusGueltigAbDatum" type="{urn:omds20}Datum-Zeit"/&gt;
 *         &lt;element name="BisherigerStatus" type="{urn:at.vvo.omds.types.omds3types.v1-3-0.on1basisfunktionen}AbstractStatusGeschaeftsfall_Type" minOccurs="0"/&gt;
 *         &lt;element name="NeuerStatus" type="{urn:at.vvo.omds.types.omds3types.v1-3-0.on1basisfunktionen}AbstractStatusGeschaeftsfall_Type"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StateChangeEvent_Type", propOrder = {
    "objektart",
    "objektId",
    "geschaeftsfallnummer",
    "geschaeftsfallArt",
    "aenderungsdatum",
    "statusGueltigAbDatum",
    "bisherigerStatus",
    "neuerStatus"
})
public class StateChangeEventType
    extends AbstractStateChangeEventType
{

    @XmlElement(name = "Objektart", required = true)
    @XmlSchemaType(name = "string")
    protected GeschaeftsobjektArtType objektart;
    @XmlElement(name = "ObjektId", required = true)
    protected ObjektIdType objektId;
    @XmlElement(name = "Geschaeftsfallnummer")
    protected ObjektIdType geschaeftsfallnummer;
    @XmlElement(name = "GeschaeftsfallArt")
    @XmlSchemaType(name = "anySimpleType")
    protected String geschaeftsfallArt;
    @XmlElement(name = "Aenderungsdatum", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar aenderungsdatum;
    @XmlElement(name = "StatusGueltigAbDatum", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar statusGueltigAbDatum;
    @XmlElement(name = "BisherigerStatus")
    protected AbstractStatusGeschaeftsfallType bisherigerStatus;
    @XmlElement(name = "NeuerStatus", required = true)
    protected AbstractStatusGeschaeftsfallType neuerStatus;

    /**
     * Ruft den Wert der objektart-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GeschaeftsobjektArtType }
     *     
     */
    public GeschaeftsobjektArtType getObjektart() {
        return objektart;
    }

    /**
     * Legt den Wert der objektart-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GeschaeftsobjektArtType }
     *     
     */
    public void setObjektart(GeschaeftsobjektArtType value) {
        this.objektart = value;
    }

    /**
     * Ruft den Wert der objektId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ObjektIdType }
     *     
     */
    public ObjektIdType getObjektId() {
        return objektId;
    }

    /**
     * Legt den Wert der objektId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjektIdType }
     *     
     */
    public void setObjektId(ObjektIdType value) {
        this.objektId = value;
    }

    /**
     * Ruft den Wert der geschaeftsfallnummer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ObjektIdType }
     *     
     */
    public ObjektIdType getGeschaeftsfallnummer() {
        return geschaeftsfallnummer;
    }

    /**
     * Legt den Wert der geschaeftsfallnummer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjektIdType }
     *     
     */
    public void setGeschaeftsfallnummer(ObjektIdType value) {
        this.geschaeftsfallnummer = value;
    }

    /**
     * Ruft den Wert der geschaeftsfallArt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGeschaeftsfallArt() {
        return geschaeftsfallArt;
    }

    /**
     * Legt den Wert der geschaeftsfallArt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGeschaeftsfallArt(String value) {
        this.geschaeftsfallArt = value;
    }

    /**
     * Ruft den Wert der aenderungsdatum-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getAenderungsdatum() {
        return aenderungsdatum;
    }

    /**
     * Legt den Wert der aenderungsdatum-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setAenderungsdatum(XMLGregorianCalendar value) {
        this.aenderungsdatum = value;
    }

    /**
     * Ruft den Wert der statusGueltigAbDatum-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStatusGueltigAbDatum() {
        return statusGueltigAbDatum;
    }

    /**
     * Legt den Wert der statusGueltigAbDatum-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStatusGueltigAbDatum(XMLGregorianCalendar value) {
        this.statusGueltigAbDatum = value;
    }

    /**
     * Ruft den Wert der bisherigerStatus-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AbstractStatusGeschaeftsfallType }
     *     
     */
    public AbstractStatusGeschaeftsfallType getBisherigerStatus() {
        return bisherigerStatus;
    }

    /**
     * Legt den Wert der bisherigerStatus-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AbstractStatusGeschaeftsfallType }
     *     
     */
    public void setBisherigerStatus(AbstractStatusGeschaeftsfallType value) {
        this.bisherigerStatus = value;
    }

    /**
     * Ruft den Wert der neuerStatus-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AbstractStatusGeschaeftsfallType }
     *     
     */
    public AbstractStatusGeschaeftsfallType getNeuerStatus() {
        return neuerStatus;
    }

    /**
     * Legt den Wert der neuerStatus-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AbstractStatusGeschaeftsfallType }
     *     
     */
    public void setNeuerStatus(AbstractStatusGeschaeftsfallType value) {
        this.neuerStatus = value;
    }

}
