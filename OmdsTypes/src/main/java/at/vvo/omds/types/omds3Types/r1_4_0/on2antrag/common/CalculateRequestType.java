
package at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.CommonProcessRequestType;
import at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.kfz.CalculateKfzRequestType;
import at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.rs.CalculateRechtsschutzRequestType;
import at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.sachPrivat.CalculateSachPrivatRequestType;
import at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.unfall.CalculateUnfallRequestType;


/**
 * Abstrakter Request für die Berechnung
 * 
 * <p>Java-Klasse für CalculateRequest_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CalculateRequest_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}CommonProcessRequest_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="RequestUpselling" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CalculateRequest_Type", propOrder = {
    "requestUpselling"
})
@XmlSeeAlso({
    CalculateKfzRequestType.class,
    CalculateRechtsschutzRequestType.class,
    CalculateSachPrivatRequestType.class,
    CalculateUnfallRequestType.class
})
public abstract class CalculateRequestType
    extends CommonProcessRequestType
{

    @XmlElement(name = "RequestUpselling", defaultValue = "false")
    protected boolean requestUpselling;

    /**
     * Ruft den Wert der requestUpselling-Eigenschaft ab.
     * 
     */
    public boolean isRequestUpselling() {
        return requestUpselling;
    }

    /**
     * Legt den Wert der requestUpselling-Eigenschaft fest.
     * 
     */
    public void setRequestUpselling(boolean value) {
        this.requestUpselling = value;
    }

}
