
package at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.CommonProcessResponseType;
import at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.kfz.CalculateKfzResponseType;
import at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.rs.CalculateRechtsschutzResponseType;
import at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.unfall.CalculateUnfallResponseType;


/**
 * Abstrakter Response, der das Ergebnis der Berechnung enthält bzw. Fehlermeldungen
 * 
 * <p>Java-Klasse für CalculateResponse_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CalculateResponse_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}CommonProcessResponse_Type"&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CalculateResponse_Type")
@XmlSeeAlso({
    CalculateKfzResponseType.class,
    CalculateRechtsschutzResponseType.class,
    CalculateUnfallResponseType.class
})
public abstract class CalculateResponseType
    extends CommonProcessResponseType
{


}
