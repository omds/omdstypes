
package at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.common;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.CommonProcessRequestType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.DateianhangType;
import at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.kfz.CreateApplicationKfzRequestType;
import at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.rs.CreateApplicationRechtsschutzRequestType;
import at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.sachPrivat.CreateApplicationSachPrivatRequestType;
import at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.unfall.CreateApplicationUnfallRequestType;


/**
 * Abstrakter Request für den Antrag
 * 
 * <p>Java-Klasse für CreateApplicationRequest_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CreateApplicationRequest_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}CommonProcessRequest_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Dateianhaenge" type="{urn:omds3CommonServiceTypes-1-1-0}Dateianhang_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreateApplicationRequest_Type", propOrder = {
    "dateianhaenge"
})
@XmlSeeAlso({
    CreateApplicationKfzRequestType.class,
    CreateApplicationRechtsschutzRequestType.class,
    CreateApplicationSachPrivatRequestType.class,
    CreateApplicationUnfallRequestType.class
})
public abstract class CreateApplicationRequestType
    extends CommonProcessRequestType
{

    @XmlElement(name = "Dateianhaenge")
    protected List<DateianhangType> dateianhaenge;

    /**
     * Gets the value of the dateianhaenge property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dateianhaenge property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDateianhaenge().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DateianhangType }
     * 
     * 
     */
    public List<DateianhangType> getDateianhaenge() {
        if (dateianhaenge == null) {
            dateianhaenge = new ArrayList<DateianhangType>();
        }
        return this.dateianhaenge;
    }

}
