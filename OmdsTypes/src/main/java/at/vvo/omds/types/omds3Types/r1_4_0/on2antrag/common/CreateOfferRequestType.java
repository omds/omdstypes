
package at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.CommonProcessRequestType;
import at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.kfz.CreateOfferKfzRequestType;
import at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.rs.CreateOfferRechtsschutzRequestType;
import at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.sachPrivat.CreateOfferSachPrivatRequestType;
import at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.unfall.CreateOfferUnfallRequestType;


/**
 * Abstrakter Request für das Offert
 * 
 * <p>Java-Klasse für CreateOfferRequest_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CreateOfferRequest_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}CommonProcessRequest_Type"&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreateOfferRequest_Type")
@XmlSeeAlso({
    CreateOfferKfzRequestType.class,
    CreateOfferRechtsschutzRequestType.class,
    CreateOfferSachPrivatRequestType.class,
    CreateOfferUnfallRequestType.class
})
public abstract class CreateOfferRequestType
    extends CommonProcessRequestType
{


}
