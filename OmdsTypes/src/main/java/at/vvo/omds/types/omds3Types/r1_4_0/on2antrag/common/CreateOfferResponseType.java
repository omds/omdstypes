
package at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.common;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.CommonProcessResponseType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.DokumentInfoType;
import at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.kfz.CreateOfferKfzResponseType;
import at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.rs.CreateOfferRechtsschutzResponseType;
import at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.unfall.CreateOfferUnfallResponseType;


/**
 * Abstrakter Response, der das Offert enthält bzw. Fehlermeldungen
 * 
 * <p>Java-Klasse für CreateOfferResponse_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CreateOfferResponse_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}CommonProcessResponse_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Dokumente" type="{urn:omds3CommonServiceTypes-1-1-0}DokumentInfo_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreateOfferResponse_Type", propOrder = {
    "dokumente"
})
@XmlSeeAlso({
    CreateOfferKfzResponseType.class,
    CreateOfferRechtsschutzResponseType.class,
    CreateOfferUnfallResponseType.class
})
public abstract class CreateOfferResponseType
    extends CommonProcessResponseType
{

    @XmlElement(name = "Dokumente")
    protected List<DokumentInfoType> dokumente;

    /**
     * Gets the value of the dokumente property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dokumente property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDokumente().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DokumentInfoType }
     * 
     * 
     */
    public List<DokumentInfoType> getDokumente() {
        if (dokumente == null) {
            dokumente = new ArrayList<DokumentInfoType>();
        }
        return this.dokumente;
    }

}
