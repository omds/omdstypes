
package at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.CommonResponseType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.DokumentInfoType;


/**
 * Typ um Offert- und Antragsdokumente zurückzugeben
 * 
 * <p>Java-Klasse für GetApplicationDocumentResponse_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="GetApplicationDocumentResponse_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}CommonResponse_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Dokument" type="{urn:omds3CommonServiceTypes-1-1-0}DokumentInfo_Type" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetApplicationDocumentResponse_Type", propOrder = {
    "dokument"
})
public class GetApplicationDocumentResponseType
    extends CommonResponseType
{

    @XmlElement(name = "Dokument")
    protected DokumentInfoType dokument;

    /**
     * Ruft den Wert der dokument-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DokumentInfoType }
     *     
     */
    public DokumentInfoType getDokument() {
        return dokument;
    }

    /**
     * Legt den Wert der dokument-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DokumentInfoType }
     *     
     */
    public void setDokument(DokumentInfoType value) {
        this.dokument = value;
    }

}
