
package at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.common;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.common package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetApplicationDocumentRequest_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.common", "GetApplicationDocumentRequest");
    private final static QName _GetApplicationDocumentResponse_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.common", "GetApplicationDocumentResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.common
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetApplicationDocumentRequestType }
     * 
     */
    public GetApplicationDocumentRequestType createGetApplicationDocumentRequestType() {
        return new GetApplicationDocumentRequestType();
    }

    /**
     * Create an instance of {@link GetApplicationDocumentResponseType }
     * 
     */
    public GetApplicationDocumentResponseType createGetApplicationDocumentResponseType() {
        return new GetApplicationDocumentResponseType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetApplicationDocumentRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.common", name = "GetApplicationDocumentRequest")
    public JAXBElement<GetApplicationDocumentRequestType> createGetApplicationDocumentRequest(GetApplicationDocumentRequestType value) {
        return new JAXBElement<GetApplicationDocumentRequestType>(_GetApplicationDocumentRequest_QNAME, GetApplicationDocumentRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetApplicationDocumentResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.common", name = "GetApplicationDocumentResponse")
    public JAXBElement<GetApplicationDocumentResponseType> createGetApplicationDocumentResponse(GetApplicationDocumentResponseType value) {
        return new JAXBElement<GetApplicationDocumentResponseType>(_GetApplicationDocumentResponse_QNAME, GetApplicationDocumentResponseType.class, null, value);
    }

}
