
package at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.common;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.AntragsartType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.DatenverwendungType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.KontierungType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.PolizzenversandType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.VertragspersonType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.VinkularglaeubigerType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.ZahlungsdatenType;
import at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.kfz.SpezAntragKfzType;
import at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.rs.SpezAntragRechtsschutzType;
import at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.sachPrivat.AntragSachPrivatType;
import at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.unfall.SpezAntragUnfallType;
import at.vvo.omds.types.omds3Types.r1_4_0.servicetypes.BeteiligtePersonVertragType;


/**
 * Abstrakter Basistyp für Antrag, der bei Request und bei Response gleich ist
 * 
 * <p>Java-Klasse für SpezAntrag_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SpezAntrag_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Antragsart" type="{urn:omds3CommonServiceTypes-1-1-0}Antragsart_Type" minOccurs="0"/&gt;
 *         &lt;element name="Personen" type="{urn:omds3ServiceTypes-1-1-0}BeteiligtePersonVertrag_Type" maxOccurs="unbounded"/&gt;
 *         &lt;element name="Versicherungsnehmer" type="{http://www.w3.org/2001/XMLSchema}unsignedShort"/&gt;
 *         &lt;element name="AbweichenderPraemienzahler" type="{http://www.w3.org/2001/XMLSchema}unsignedShort" minOccurs="0"/&gt;
 *         &lt;element name="WeitereVersicherungsnehmer" type="{http://www.w3.org/2001/XMLSchema}unsignedShort" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="WeitereVertragspersonen" type="{urn:omds3CommonServiceTypes-1-1-0}Vertragsperson_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Zahlungsdaten" type="{urn:omds3CommonServiceTypes-1-1-0}Zahlungsdaten_Type"/&gt;
 *         &lt;element name="Sepa" type="{urn:omds3CommonServiceTypes-1-1-0}SepaCd_Type" minOccurs="0"/&gt;
 *         &lt;element name="Vinkulierung" type="{urn:omds3CommonServiceTypes-1-1-0}Vinkularglaeubiger_Type" minOccurs="0"/&gt;
 *         &lt;element name="Polizzenversand" type="{urn:omds3CommonServiceTypes-1-1-0}PolizzenversandType" minOccurs="0"/&gt;
 *         &lt;element name="Datenschutzbestimmungen" type="{urn:omds3CommonServiceTypes-1-1-0}Datenverwendung_Type"/&gt;
 *         &lt;element name="Kontierung" type="{urn:omds3CommonServiceTypes-1-1-0}Kontierung_Type" maxOccurs="3" minOccurs="0"/&gt;
 *         &lt;element name="ZusaetzlicheAntragsdaten" type="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.common}ZusaetzlicheAntragsdaten_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SpezAntrag_Type", propOrder = {
    "antragsart",
    "personen",
    "versicherungsnehmer",
    "abweichenderPraemienzahler",
    "weitereVersicherungsnehmer",
    "weitereVertragspersonen",
    "zahlungsdaten",
    "sepa",
    "vinkulierung",
    "polizzenversand",
    "datenschutzbestimmungen",
    "kontierung",
    "zusaetzlicheAntragsdaten"
})
@XmlSeeAlso({
    SpezAntragKfzType.class,
    SpezAntragRechtsschutzType.class,
    AntragSachPrivatType.class,
    SpezAntragUnfallType.class
})
public abstract class SpezAntragType {

    @XmlElement(name = "Antragsart")
    @XmlSchemaType(name = "string")
    protected AntragsartType antragsart;
    @XmlElement(name = "Personen", required = true)
    protected List<BeteiligtePersonVertragType> personen;
    @XmlElement(name = "Versicherungsnehmer")
    @XmlSchemaType(name = "unsignedShort")
    protected int versicherungsnehmer;
    @XmlElement(name = "AbweichenderPraemienzahler")
    @XmlSchemaType(name = "unsignedShort")
    protected Integer abweichenderPraemienzahler;
    @XmlElement(name = "WeitereVersicherungsnehmer", type = Integer.class)
    @XmlSchemaType(name = "unsignedShort")
    protected List<Integer> weitereVersicherungsnehmer;
    @XmlElement(name = "WeitereVertragspersonen")
    protected List<VertragspersonType> weitereVertragspersonen;
    @XmlElement(name = "Zahlungsdaten", required = true)
    protected ZahlungsdatenType zahlungsdaten;
    @XmlElement(name = "Sepa")
    @XmlSchemaType(name = "unsignedByte")
    protected Short sepa;
    @XmlElement(name = "Vinkulierung")
    protected VinkularglaeubigerType vinkulierung;
    @XmlElement(name = "Polizzenversand")
    @XmlSchemaType(name = "string")
    protected PolizzenversandType polizzenversand;
    @XmlElement(name = "Datenschutzbestimmungen", required = true)
    protected DatenverwendungType datenschutzbestimmungen;
    @XmlElement(name = "Kontierung")
    protected List<KontierungType> kontierung;
    @XmlElement(name = "ZusaetzlicheAntragsdaten")
    protected List<ZusaetzlicheAntragsdatenType> zusaetzlicheAntragsdaten;

    /**
     * Ruft den Wert der antragsart-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AntragsartType }
     *     
     */
    public AntragsartType getAntragsart() {
        return antragsart;
    }

    /**
     * Legt den Wert der antragsart-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AntragsartType }
     *     
     */
    public void setAntragsart(AntragsartType value) {
        this.antragsart = value;
    }

    /**
     * Gets the value of the personen property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the personen property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPersonen().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BeteiligtePersonVertragType }
     * 
     * 
     */
    public List<BeteiligtePersonVertragType> getPersonen() {
        if (personen == null) {
            personen = new ArrayList<BeteiligtePersonVertragType>();
        }
        return this.personen;
    }

    /**
     * Ruft den Wert der versicherungsnehmer-Eigenschaft ab.
     * 
     */
    public int getVersicherungsnehmer() {
        return versicherungsnehmer;
    }

    /**
     * Legt den Wert der versicherungsnehmer-Eigenschaft fest.
     * 
     */
    public void setVersicherungsnehmer(int value) {
        this.versicherungsnehmer = value;
    }

    /**
     * Ruft den Wert der abweichenderPraemienzahler-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAbweichenderPraemienzahler() {
        return abweichenderPraemienzahler;
    }

    /**
     * Legt den Wert der abweichenderPraemienzahler-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAbweichenderPraemienzahler(Integer value) {
        this.abweichenderPraemienzahler = value;
    }

    /**
     * Gets the value of the weitereVersicherungsnehmer property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the weitereVersicherungsnehmer property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getWeitereVersicherungsnehmer().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Integer }
     * 
     * 
     */
    public List<Integer> getWeitereVersicherungsnehmer() {
        if (weitereVersicherungsnehmer == null) {
            weitereVersicherungsnehmer = new ArrayList<Integer>();
        }
        return this.weitereVersicherungsnehmer;
    }

    /**
     * Gets the value of the weitereVertragspersonen property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the weitereVertragspersonen property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getWeitereVertragspersonen().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link VertragspersonType }
     * 
     * 
     */
    public List<VertragspersonType> getWeitereVertragspersonen() {
        if (weitereVertragspersonen == null) {
            weitereVertragspersonen = new ArrayList<VertragspersonType>();
        }
        return this.weitereVertragspersonen;
    }

    /**
     * Ruft den Wert der zahlungsdaten-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ZahlungsdatenType }
     *     
     */
    public ZahlungsdatenType getZahlungsdaten() {
        return zahlungsdaten;
    }

    /**
     * Legt den Wert der zahlungsdaten-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ZahlungsdatenType }
     *     
     */
    public void setZahlungsdaten(ZahlungsdatenType value) {
        this.zahlungsdaten = value;
    }

    /**
     * Ruft den Wert der sepa-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getSepa() {
        return sepa;
    }

    /**
     * Legt den Wert der sepa-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setSepa(Short value) {
        this.sepa = value;
    }

    /**
     * Ruft den Wert der vinkulierung-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VinkularglaeubigerType }
     *     
     */
    public VinkularglaeubigerType getVinkulierung() {
        return vinkulierung;
    }

    /**
     * Legt den Wert der vinkulierung-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VinkularglaeubigerType }
     *     
     */
    public void setVinkulierung(VinkularglaeubigerType value) {
        this.vinkulierung = value;
    }

    /**
     * Ruft den Wert der polizzenversand-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PolizzenversandType }
     *     
     */
    public PolizzenversandType getPolizzenversand() {
        return polizzenversand;
    }

    /**
     * Legt den Wert der polizzenversand-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PolizzenversandType }
     *     
     */
    public void setPolizzenversand(PolizzenversandType value) {
        this.polizzenversand = value;
    }

    /**
     * Ruft den Wert der datenschutzbestimmungen-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DatenverwendungType }
     *     
     */
    public DatenverwendungType getDatenschutzbestimmungen() {
        return datenschutzbestimmungen;
    }

    /**
     * Legt den Wert der datenschutzbestimmungen-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DatenverwendungType }
     *     
     */
    public void setDatenschutzbestimmungen(DatenverwendungType value) {
        this.datenschutzbestimmungen = value;
    }

    /**
     * Gets the value of the kontierung property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the kontierung property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getKontierung().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link KontierungType }
     * 
     * 
     */
    public List<KontierungType> getKontierung() {
        if (kontierung == null) {
            kontierung = new ArrayList<KontierungType>();
        }
        return this.kontierung;
    }

    /**
     * Gets the value of the zusaetzlicheAntragsdaten property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the zusaetzlicheAntragsdaten property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getZusaetzlicheAntragsdaten().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ZusaetzlicheAntragsdatenType }
     * 
     * 
     */
    public List<ZusaetzlicheAntragsdatenType> getZusaetzlicheAntragsdaten() {
        if (zusaetzlicheAntragsdaten == null) {
            zusaetzlicheAntragsdaten = new ArrayList<ZusaetzlicheAntragsdatenType>();
        }
        return this.zusaetzlicheAntragsdaten;
    }

}
