
package at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.kfz.SpezBerechnungKfzType;
import at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.rs.SpezBerechnungRechtsschutzType;
import at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.sachPrivat.BerechnungSachPrivatType;
import at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.unfall.SpezBerechnungUnfallType;


/**
 * Abstrakter Basistyp Berechnung, der bei Request und bei Response gleich ist
 * 
 * <p>Java-Klasse für SpezBerechnung_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SpezBerechnung_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SpezBerechnung_Type")
@XmlSeeAlso({
    SpezBerechnungKfzType.class,
    SpezBerechnungRechtsschutzType.class,
    BerechnungSachPrivatType.class,
    SpezBerechnungUnfallType.class
})
public abstract class SpezBerechnungType {


}
