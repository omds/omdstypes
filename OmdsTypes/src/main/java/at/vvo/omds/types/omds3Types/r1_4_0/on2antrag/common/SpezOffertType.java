
package at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.common;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.ObjektIdType;
import at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.kfz.SpezOffertKfzType;
import at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.rs.SpezOffertRechtsschutzType;
import at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.sachPrivat.OffertSachPrivatType;
import at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.unfall.SpezOffertUnfallType;
import at.vvo.omds.types.omds3Types.r1_4_0.servicetypes.BeteiligtePersonVertragType;


/**
 * Abstrakter Basistyp Offert, der bei Request und Response gleich ist
 * 
 * <p>Java-Klasse für SpezOffert_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SpezOffert_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Offertnummer" type="{urn:omds3CommonServiceTypes-1-1-0}ObjektId_Type" minOccurs="0"/&gt;
 *         &lt;element name="Personen" type="{urn:omds3ServiceTypes-1-1-0}BeteiligtePersonVertrag_Type" maxOccurs="unbounded"/&gt;
 *         &lt;element name="Versicherungsnehmer" type="{http://www.w3.org/2001/XMLSchema}unsignedShort"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SpezOffert_Type", propOrder = {
    "offertnummer",
    "personen",
    "versicherungsnehmer"
})
@XmlSeeAlso({
    SpezOffertKfzType.class,
    SpezOffertRechtsschutzType.class,
    OffertSachPrivatType.class,
    SpezOffertUnfallType.class
})
public abstract class SpezOffertType {

    @XmlElement(name = "Offertnummer")
    protected ObjektIdType offertnummer;
    @XmlElement(name = "Personen", required = true)
    protected List<BeteiligtePersonVertragType> personen;
    @XmlElement(name = "Versicherungsnehmer")
    @XmlSchemaType(name = "unsignedShort")
    protected int versicherungsnehmer;

    /**
     * Ruft den Wert der offertnummer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ObjektIdType }
     *     
     */
    public ObjektIdType getOffertnummer() {
        return offertnummer;
    }

    /**
     * Legt den Wert der offertnummer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjektIdType }
     *     
     */
    public void setOffertnummer(ObjektIdType value) {
        this.offertnummer = value;
    }

    /**
     * Gets the value of the personen property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the personen property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPersonen().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BeteiligtePersonVertragType }
     * 
     * 
     */
    public List<BeteiligtePersonVertragType> getPersonen() {
        if (personen == null) {
            personen = new ArrayList<BeteiligtePersonVertragType>();
        }
        return this.personen;
    }

    /**
     * Ruft den Wert der versicherungsnehmer-Eigenschaft ab.
     * 
     */
    public int getVersicherungsnehmer() {
        return versicherungsnehmer;
    }

    /**
     * Legt den Wert der versicherungsnehmer-Eigenschaft fest.
     * 
     */
    public void setVersicherungsnehmer(int value) {
        this.versicherungsnehmer = value;
    }

}
