
package at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.CommonProcessRequestType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.ObjektIdType;
import at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.kfz.SubmitApplicationKfzRequestType;
import at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.rs.SubmitApplicationRechtsschutzRequestType;
import at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.sachPrivat.SubmitApplicationSachPrivatRequestType;
import at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.unfall.SubmitApplicationUnfallRequestType;


/**
 * Abstrakter Request für die Antragsüberleitung
 * 
 * <p>Java-Klasse für SubmitApplicationRequest_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SubmitApplicationRequest_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}CommonProcessRequest_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Antragsnummer" type="{urn:omds3CommonServiceTypes-1-1-0}ObjektId_Type" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubmitApplicationRequest_Type", propOrder = {
    "antragsnummer"
})
@XmlSeeAlso({
    SubmitApplicationKfzRequestType.class,
    SubmitApplicationRechtsschutzRequestType.class,
    SubmitApplicationSachPrivatRequestType.class,
    SubmitApplicationUnfallRequestType.class
})
public abstract class SubmitApplicationRequestType
    extends CommonProcessRequestType
{

    @XmlElement(name = "Antragsnummer")
    protected ObjektIdType antragsnummer;

    /**
     * Ruft den Wert der antragsnummer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ObjektIdType }
     *     
     */
    public ObjektIdType getAntragsnummer() {
        return antragsnummer;
    }

    /**
     * Legt den Wert der antragsnummer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjektIdType }
     *     
     */
    public void setAntragsnummer(ObjektIdType value) {
        this.antragsnummer = value;
    }

}
