
package at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.kfz;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.common.CreateOfferRequestType;


/**
 * Typ des Request für ein Kfz-Offert
 * 
 * <p>Java-Klasse für CreateOfferKfzRequest_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CreateOfferKfzRequest_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.common}CreateOfferRequest_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Offertanfrage" type="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz}SpezOffertKfz_Type"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreateOfferKfzRequest_Type", propOrder = {
    "offertanfrage"
})
public class CreateOfferKfzRequestType
    extends CreateOfferRequestType
{

    @XmlElement(name = "Offertanfrage", required = true)
    protected SpezOffertKfzType offertanfrage;

    /**
     * Ruft den Wert der offertanfrage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SpezOffertKfzType }
     *     
     */
    public SpezOffertKfzType getOffertanfrage() {
        return offertanfrage;
    }

    /**
     * Legt den Wert der offertanfrage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SpezOffertKfzType }
     *     
     */
    public void setOffertanfrage(SpezOffertKfzType value) {
        this.offertanfrage = value;
    }

}
