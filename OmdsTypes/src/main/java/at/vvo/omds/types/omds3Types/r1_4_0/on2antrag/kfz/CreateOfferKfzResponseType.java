
package at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.kfz;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.common.CreateOfferResponseType;


/**
 * Typ des Response für ein Kfz-Offert
 * 
 * <p>Java-Klasse für CreateOfferKfzResponse_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CreateOfferKfzResponse_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.common}CreateOfferResponse_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Offertantwort" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz}SpezOffertKfz_Type"&gt;
 *               &lt;/extension&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreateOfferKfzResponse_Type", propOrder = {
    "offertantwort"
})
public class CreateOfferKfzResponseType
    extends CreateOfferResponseType
{

    @XmlElement(name = "Offertantwort")
    protected Offertantwort offertantwort;

    /**
     * Ruft den Wert der offertantwort-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Offertantwort }
     *     
     */
    public Offertantwort getOffertantwort() {
        return offertantwort;
    }

    /**
     * Legt den Wert der offertantwort-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Offertantwort }
     *     
     */
    public void setOffertantwort(Offertantwort value) {
        this.offertantwort = value;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz}SpezOffertKfz_Type"&gt;
     *     &lt;/extension&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Offertantwort
        extends SpezOffertKfzType
    {


    }

}
