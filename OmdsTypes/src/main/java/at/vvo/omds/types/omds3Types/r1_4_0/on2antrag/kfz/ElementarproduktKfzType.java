
package at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.kfz;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.ElementarproduktType;


/**
 * Abstrakte Basisklasse für KFZ-Elementarprodukte
 * 
 * <p>Java-Klasse für ElementarproduktKfz_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ElementarproduktKfz_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}Elementarprodukt_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz}FahrzeugRefLfdNr" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ElementarproduktKfz_Type", propOrder = {
    "fahrzeugRefLfdNr"
})
@XmlSeeAlso({
    HaftpflichtKfzType.class,
    KaskoKfzType.class,
    InsassenUnfallKfzType.class,
    LenkerUnfallKfzType.class,
    AssistanceKfzType.class
})
public abstract class ElementarproduktKfzType
    extends ElementarproduktType
{

    @XmlElement(name = "FahrzeugRefLfdNr")
    protected String fahrzeugRefLfdNr;

    /**
     * Ruft den Wert der fahrzeugRefLfdNr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFahrzeugRefLfdNr() {
        return fahrzeugRefLfdNr;
    }

    /**
     * Legt den Wert der fahrzeugRefLfdNr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFahrzeugRefLfdNr(String value) {
        this.fahrzeugRefLfdNr = value;
    }

}
