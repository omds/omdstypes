
package at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.kfz;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds2Types.v2_11.ELEinstufungType;
import at.vvo.omds.types.omds2Types.v2_11.ELVersicherungssummeType;


/**
 * Typ für das Elementarprodukt KFZ-Haftpflicht
 * 
 * <p>Java-Klasse für HaftpflichtKfz_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="HaftpflichtKfz_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz}ElementarproduktKfz_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Art" type="{urn:omds20}VtgSparteCd_Type"/&gt;
 *         &lt;element ref="{urn:omds20}EL-Versicherungssumme"/&gt;
 *         &lt;element name="VarianteLeihwagen" type="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz}VarianteLeihwagen_Type"/&gt;
 *         &lt;element ref="{urn:omds20}EL-Einstufung"/&gt;
 *         &lt;element name="Zielpraemie" type="{urn:omds20}decimal" minOccurs="0"/&gt;
 *         &lt;element name="VDNummer" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HaftpflichtKfz_Type", propOrder = {
    "art",
    "elVersicherungssumme",
    "varianteLeihwagen",
    "elEinstufung",
    "zielpraemie",
    "vdNummer"
})
public class HaftpflichtKfzType
    extends ElementarproduktKfzType
{

    @XmlElement(name = "Art", required = true)
    protected String art;
    @XmlElement(name = "EL-Versicherungssumme", namespace = "urn:omds20", required = true)
    protected ELVersicherungssummeType elVersicherungssumme;
    @XmlElement(name = "VarianteLeihwagen", required = true)
    @XmlSchemaType(name = "string")
    protected VarianteLeihwagenType varianteLeihwagen;
    @XmlElement(name = "EL-Einstufung", namespace = "urn:omds20", required = true)
    protected ELEinstufungType elEinstufung;
    @XmlElement(name = "Zielpraemie")
    protected BigDecimal zielpraemie;
    @XmlElement(name = "VDNummer")
    protected String vdNummer;

    /**
     * Ruft den Wert der art-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArt() {
        return art;
    }

    /**
     * Legt den Wert der art-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArt(String value) {
        this.art = value;
    }

    /**
     * Versicherungssumme Haftpflicht
     * 
     * @return
     *     possible object is
     *     {@link ELVersicherungssummeType }
     *     
     */
    public ELVersicherungssummeType getELVersicherungssumme() {
        return elVersicherungssumme;
    }

    /**
     * Legt den Wert der elVersicherungssumme-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ELVersicherungssummeType }
     *     
     */
    public void setELVersicherungssumme(ELVersicherungssummeType value) {
        this.elVersicherungssumme = value;
    }

    /**
     * Ruft den Wert der varianteLeihwagen-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VarianteLeihwagenType }
     *     
     */
    public VarianteLeihwagenType getVarianteLeihwagen() {
        return varianteLeihwagen;
    }

    /**
     * Legt den Wert der varianteLeihwagen-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VarianteLeihwagenType }
     *     
     */
    public void setVarianteLeihwagen(VarianteLeihwagenType value) {
        this.varianteLeihwagen = value;
    }

    /**
     * Ruft den Wert der elEinstufung-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ELEinstufungType }
     *     
     */
    public ELEinstufungType getELEinstufung() {
        return elEinstufung;
    }

    /**
     * Legt den Wert der elEinstufung-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ELEinstufungType }
     *     
     */
    public void setELEinstufung(ELEinstufungType value) {
        this.elEinstufung = value;
    }

    /**
     * Ruft den Wert der zielpraemie-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getZielpraemie() {
        return zielpraemie;
    }

    /**
     * Legt den Wert der zielpraemie-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setZielpraemie(BigDecimal value) {
        this.zielpraemie = value;
    }

    /**
     * Ruft den Wert der vdNummer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVDNummer() {
        return vdNummer;
    }

    /**
     * Legt den Wert der vdNummer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVDNummer(String value) {
        this.vdNummer = value;
    }

}
