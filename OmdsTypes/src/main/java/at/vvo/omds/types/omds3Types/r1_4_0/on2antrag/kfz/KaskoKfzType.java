
package at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.kfz;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.BezugsrechtType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.VinkulierungType;


/**
 * Abstrakter Basistyp für die Kasko-Elementarprodukte
 * 
 * <p>Java-Klasse für KaskoKfz_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="KaskoKfz_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz}ElementarproduktKfz_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Art" type="{urn:omds20}VtgSparteCd_Type"/&gt;
 *         &lt;element name="SelbstbehaltBisBetrag" type="{urn:omds20}decimal" minOccurs="0"/&gt;
 *         &lt;element name="KMLeistung" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" minOccurs="0"/&gt;
 *         &lt;element name="VorsteuerAbzugBerechtigung" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="Vinkulierung" type="{urn:omds3CommonServiceTypes-1-1-0}Vinkulierung_Type" minOccurs="0"/&gt;
 *         &lt;element name="Bezugsrecht" type="{urn:omds3CommonServiceTypes-1-1-0}Bezugsrecht_Type" minOccurs="0"/&gt;
 *         &lt;element name="Zielpraemie" type="{urn:omds20}decimal" minOccurs="0"/&gt;
 *         &lt;element name="Eingeschraenkt" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "KaskoKfz_Type", propOrder = {
    "art",
    "selbstbehaltBisBetrag",
    "kmLeistung",
    "vorsteuerAbzugBerechtigung",
    "vinkulierung",
    "bezugsrecht",
    "zielpraemie",
    "eingeschraenkt"
})
@XmlSeeAlso({
    TeilkaskoKfzType.class,
    VollkaskoKfzType.class
})
public abstract class KaskoKfzType
    extends ElementarproduktKfzType
{

    @XmlElement(name = "Art", required = true)
    protected String art;
    @XmlElement(name = "SelbstbehaltBisBetrag")
    protected BigDecimal selbstbehaltBisBetrag;
    @XmlElement(name = "KMLeistung")
    @XmlSchemaType(name = "unsignedInt")
    protected Long kmLeistung;
    @XmlElement(name = "VorsteuerAbzugBerechtigung")
    protected boolean vorsteuerAbzugBerechtigung;
    @XmlElement(name = "Vinkulierung")
    protected VinkulierungType vinkulierung;
    @XmlElement(name = "Bezugsrecht")
    protected BezugsrechtType bezugsrecht;
    @XmlElement(name = "Zielpraemie")
    protected BigDecimal zielpraemie;
    @XmlElement(name = "Eingeschraenkt", defaultValue = "0")
    protected Boolean eingeschraenkt;

    /**
     * Ruft den Wert der art-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArt() {
        return art;
    }

    /**
     * Legt den Wert der art-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArt(String value) {
        this.art = value;
    }

    /**
     * Ruft den Wert der selbstbehaltBisBetrag-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSelbstbehaltBisBetrag() {
        return selbstbehaltBisBetrag;
    }

    /**
     * Legt den Wert der selbstbehaltBisBetrag-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSelbstbehaltBisBetrag(BigDecimal value) {
        this.selbstbehaltBisBetrag = value;
    }

    /**
     * Ruft den Wert der kmLeistung-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getKMLeistung() {
        return kmLeistung;
    }

    /**
     * Legt den Wert der kmLeistung-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setKMLeistung(Long value) {
        this.kmLeistung = value;
    }

    /**
     * Ruft den Wert der vorsteuerAbzugBerechtigung-Eigenschaft ab.
     * 
     */
    public boolean isVorsteuerAbzugBerechtigung() {
        return vorsteuerAbzugBerechtigung;
    }

    /**
     * Legt den Wert der vorsteuerAbzugBerechtigung-Eigenschaft fest.
     * 
     */
    public void setVorsteuerAbzugBerechtigung(boolean value) {
        this.vorsteuerAbzugBerechtigung = value;
    }

    /**
     * Ruft den Wert der vinkulierung-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VinkulierungType }
     *     
     */
    public VinkulierungType getVinkulierung() {
        return vinkulierung;
    }

    /**
     * Legt den Wert der vinkulierung-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VinkulierungType }
     *     
     */
    public void setVinkulierung(VinkulierungType value) {
        this.vinkulierung = value;
    }

    /**
     * Ruft den Wert der bezugsrecht-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BezugsrechtType }
     *     
     */
    public BezugsrechtType getBezugsrecht() {
        return bezugsrecht;
    }

    /**
     * Legt den Wert der bezugsrecht-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BezugsrechtType }
     *     
     */
    public void setBezugsrecht(BezugsrechtType value) {
        this.bezugsrecht = value;
    }

    /**
     * Ruft den Wert der zielpraemie-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getZielpraemie() {
        return zielpraemie;
    }

    /**
     * Legt den Wert der zielpraemie-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setZielpraemie(BigDecimal value) {
        this.zielpraemie = value;
    }

    /**
     * Ruft den Wert der eingeschraenkt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEingeschraenkt() {
        return eingeschraenkt;
    }

    /**
     * Legt den Wert der eingeschraenkt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEingeschraenkt(Boolean value) {
        this.eingeschraenkt = value;
    }

}
