
package at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.kfz;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.kfz package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CalculateKfzRequest_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz", "CalculateKfzRequest");
    private final static QName _CalculateKfzResponse_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz", "CalculateKfzResponse");
    private final static QName _CreateOfferKfzRequest_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz", "CreateOfferKfzRequest");
    private final static QName _CreateOfferKfzResponse_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz", "CreateOfferKfzResponse");
    private final static QName _CreateApplicationKfzRequest_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz", "CreateApplicationKfzRequest");
    private final static QName _CreateApplicationKfzResponse_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz", "CreateApplicationKfzResponse");
    private final static QName _SubmitApplicationKfzRequest_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz", "SubmitApplicationKfzRequest");
    private final static QName _SubmitApplicationKfzResponse_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz", "SubmitApplicationKfzResponse");
    private final static QName _FahrzeugRefLfdNr_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz", "FahrzeugRefLfdNr");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.kfz
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CreateOfferKfzResponseType }
     * 
     */
    public CreateOfferKfzResponseType createCreateOfferKfzResponseType() {
        return new CreateOfferKfzResponseType();
    }

    /**
     * Create an instance of {@link CalculateKfzRequestType }
     * 
     */
    public CalculateKfzRequestType createCalculateKfzRequestType() {
        return new CalculateKfzRequestType();
    }

    /**
     * Create an instance of {@link CalculateKfzResponseType }
     * 
     */
    public CalculateKfzResponseType createCalculateKfzResponseType() {
        return new CalculateKfzResponseType();
    }

    /**
     * Create an instance of {@link CreateOfferKfzRequestType }
     * 
     */
    public CreateOfferKfzRequestType createCreateOfferKfzRequestType() {
        return new CreateOfferKfzRequestType();
    }

    /**
     * Create an instance of {@link CreateApplicationKfzRequestType }
     * 
     */
    public CreateApplicationKfzRequestType createCreateApplicationKfzRequestType() {
        return new CreateApplicationKfzRequestType();
    }

    /**
     * Create an instance of {@link CreateApplicationKfzResponseType }
     * 
     */
    public CreateApplicationKfzResponseType createCreateApplicationKfzResponseType() {
        return new CreateApplicationKfzResponseType();
    }

    /**
     * Create an instance of {@link SubmitApplicationKfzRequestType }
     * 
     */
    public SubmitApplicationKfzRequestType createSubmitApplicationKfzRequestType() {
        return new SubmitApplicationKfzRequestType();
    }

    /**
     * Create an instance of {@link SubmitApplicationKfzResponseType }
     * 
     */
    public SubmitApplicationKfzResponseType createSubmitApplicationKfzResponseType() {
        return new SubmitApplicationKfzResponseType();
    }

    /**
     * Create an instance of {@link VerkaufsproduktKfzType }
     * 
     */
    public VerkaufsproduktKfzType createVerkaufsproduktKfzType() {
        return new VerkaufsproduktKfzType();
    }

    /**
     * Create an instance of {@link ProduktKfzType }
     * 
     */
    public ProduktKfzType createProduktKfzType() {
        return new ProduktKfzType();
    }

    /**
     * Create an instance of {@link HaftpflichtKfzType }
     * 
     */
    public HaftpflichtKfzType createHaftpflichtKfzType() {
        return new HaftpflichtKfzType();
    }

    /**
     * Create an instance of {@link TeilkaskoKfzType }
     * 
     */
    public TeilkaskoKfzType createTeilkaskoKfzType() {
        return new TeilkaskoKfzType();
    }

    /**
     * Create an instance of {@link VollkaskoKfzType }
     * 
     */
    public VollkaskoKfzType createVollkaskoKfzType() {
        return new VollkaskoKfzType();
    }

    /**
     * Create an instance of {@link InsassenUnfallKfzType }
     * 
     */
    public InsassenUnfallKfzType createInsassenUnfallKfzType() {
        return new InsassenUnfallKfzType();
    }

    /**
     * Create an instance of {@link LenkerUnfallKfzType }
     * 
     */
    public LenkerUnfallKfzType createLenkerUnfallKfzType() {
        return new LenkerUnfallKfzType();
    }

    /**
     * Create an instance of {@link AssistanceKfzType }
     * 
     */
    public AssistanceKfzType createAssistanceKfzType() {
        return new AssistanceKfzType();
    }

    /**
     * Create an instance of {@link ProduktKfzRechtsschutzType }
     * 
     */
    public ProduktKfzRechtsschutzType createProduktKfzRechtsschutzType() {
        return new ProduktKfzRechtsschutzType();
    }

    /**
     * Create an instance of {@link VerkehrsrechtsschutzKfzType }
     * 
     */
    public VerkehrsrechtsschutzKfzType createVerkehrsrechtsschutzKfzType() {
        return new VerkehrsrechtsschutzKfzType();
    }

    /**
     * Create an instance of {@link SpezBerechnungKfzType }
     * 
     */
    public SpezBerechnungKfzType createSpezBerechnungKfzType() {
        return new SpezBerechnungKfzType();
    }

    /**
     * Create an instance of {@link SpezOffertKfzType }
     * 
     */
    public SpezOffertKfzType createSpezOffertKfzType() {
        return new SpezOffertKfzType();
    }

    /**
     * Create an instance of {@link SpezAntragKfzType }
     * 
     */
    public SpezAntragKfzType createSpezAntragKfzType() {
        return new SpezAntragKfzType();
    }

    /**
     * Create an instance of {@link ZusaetzlicheKfzdatenType }
     * 
     */
    public ZusaetzlicheKfzdatenType createZusaetzlicheKfzdatenType() {
        return new ZusaetzlicheKfzdatenType();
    }

    /**
     * Create an instance of {@link WechselkennzeichenType }
     * 
     */
    public WechselkennzeichenType createWechselkennzeichenType() {
        return new WechselkennzeichenType();
    }

    /**
     * Create an instance of {@link FahrzeugzustandType }
     * 
     */
    public FahrzeugzustandType createFahrzeugzustandType() {
        return new FahrzeugzustandType();
    }

    /**
     * Create an instance of {@link CreateOfferKfzResponseType.Offertantwort }
     * 
     */
    public CreateOfferKfzResponseType.Offertantwort createCreateOfferKfzResponseTypeOffertantwort() {
        return new CreateOfferKfzResponseType.Offertantwort();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CalculateKfzRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz", name = "CalculateKfzRequest")
    public JAXBElement<CalculateKfzRequestType> createCalculateKfzRequest(CalculateKfzRequestType value) {
        return new JAXBElement<CalculateKfzRequestType>(_CalculateKfzRequest_QNAME, CalculateKfzRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CalculateKfzResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz", name = "CalculateKfzResponse")
    public JAXBElement<CalculateKfzResponseType> createCalculateKfzResponse(CalculateKfzResponseType value) {
        return new JAXBElement<CalculateKfzResponseType>(_CalculateKfzResponse_QNAME, CalculateKfzResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateOfferKfzRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz", name = "CreateOfferKfzRequest")
    public JAXBElement<CreateOfferKfzRequestType> createCreateOfferKfzRequest(CreateOfferKfzRequestType value) {
        return new JAXBElement<CreateOfferKfzRequestType>(_CreateOfferKfzRequest_QNAME, CreateOfferKfzRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateOfferKfzResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz", name = "CreateOfferKfzResponse")
    public JAXBElement<CreateOfferKfzResponseType> createCreateOfferKfzResponse(CreateOfferKfzResponseType value) {
        return new JAXBElement<CreateOfferKfzResponseType>(_CreateOfferKfzResponse_QNAME, CreateOfferKfzResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateApplicationKfzRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz", name = "CreateApplicationKfzRequest")
    public JAXBElement<CreateApplicationKfzRequestType> createCreateApplicationKfzRequest(CreateApplicationKfzRequestType value) {
        return new JAXBElement<CreateApplicationKfzRequestType>(_CreateApplicationKfzRequest_QNAME, CreateApplicationKfzRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateApplicationKfzResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz", name = "CreateApplicationKfzResponse")
    public JAXBElement<CreateApplicationKfzResponseType> createCreateApplicationKfzResponse(CreateApplicationKfzResponseType value) {
        return new JAXBElement<CreateApplicationKfzResponseType>(_CreateApplicationKfzResponse_QNAME, CreateApplicationKfzResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubmitApplicationKfzRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz", name = "SubmitApplicationKfzRequest")
    public JAXBElement<SubmitApplicationKfzRequestType> createSubmitApplicationKfzRequest(SubmitApplicationKfzRequestType value) {
        return new JAXBElement<SubmitApplicationKfzRequestType>(_SubmitApplicationKfzRequest_QNAME, SubmitApplicationKfzRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubmitApplicationKfzResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz", name = "SubmitApplicationKfzResponse")
    public JAXBElement<SubmitApplicationKfzResponseType> createSubmitApplicationKfzResponse(SubmitApplicationKfzResponseType value) {
        return new JAXBElement<SubmitApplicationKfzResponseType>(_SubmitApplicationKfzResponse_QNAME, SubmitApplicationKfzResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz", name = "FahrzeugRefLfdNr")
    public JAXBElement<String> createFahrzeugRefLfdNr(String value) {
        return new JAXBElement<String>(_FahrzeugRefLfdNr_QNAME, String.class, null, value);
    }

}
