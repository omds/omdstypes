
package at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.kfz;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für SchaedenAnScheibenKleinglas_Type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="SchaedenAnScheibenKleinglas_Type"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="keine Schäden an Scheiben / Kleinglas"/&gt;
 *     &lt;enumeration value="Schäden an Scheiben / Kleinglas"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "SchaedenAnScheibenKleinglas_Type")
@XmlEnum
public enum SchaedenAnScheibenKleinglasType {

    @XmlEnumValue("keine Sch\u00e4den an Scheiben / Kleinglas")
    KEINE_SCHÄDEN_AN_SCHEIBEN_KLEINGLAS("keine Sch\u00e4den an Scheiben / Kleinglas"),
    @XmlEnumValue("Sch\u00e4den an Scheiben / Kleinglas")
    SCHÄDEN_AN_SCHEIBEN_KLEINGLAS("Sch\u00e4den an Scheiben / Kleinglas");
    private final String value;

    SchaedenAnScheibenKleinglasType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SchaedenAnScheibenKleinglasType fromValue(String v) {
        for (SchaedenAnScheibenKleinglasType c: SchaedenAnScheibenKleinglasType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
