
package at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.kfz;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.BonusMalusSystemType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.ErsatzpolizzenType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.VorversicherungenType;
import at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.common.SpezAntragType;


/**
 * Typ der das Produkt beschreibt und in Antragsanfrage und Antragsantwort verwendet wird
 * 
 * <p>Java-Klasse für SpezAntragKfz_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SpezAntragKfz_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.common}SpezAntrag_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Verkaufsprodukt" type="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz}VerkaufsproduktKfz_Type"/&gt;
 *         &lt;element name="Ersatzpolizzennummer" type="{urn:omds3CommonServiceTypes-1-1-0}Ersatzpolizzen_Type" minOccurs="0"/&gt;
 *         &lt;element name="Vorversicherungen" type="{urn:omds3CommonServiceTypes-1-1-0}Vorversicherungen_Type" minOccurs="0"/&gt;
 *         &lt;element name="BonusMalus" type="{urn:omds3CommonServiceTypes-1-1-0}BonusMalusSystem_Type" minOccurs="0"/&gt;
 *         &lt;element name="ZusaetzlicheKfzDaten" type="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz}ZusaetzlicheKfzdaten_Type" minOccurs="0"/&gt;
 *         &lt;element name="ZusendungWeitereDokumente" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SpezAntragKfz_Type", propOrder = {
    "verkaufsprodukt",
    "ersatzpolizzennummer",
    "vorversicherungen",
    "bonusMalus",
    "zusaetzlicheKfzDaten",
    "zusendungWeitereDokumente"
})
public class SpezAntragKfzType
    extends SpezAntragType
{

    @XmlElement(name = "Verkaufsprodukt", required = true)
    protected VerkaufsproduktKfzType verkaufsprodukt;
    @XmlElement(name = "Ersatzpolizzennummer")
    protected ErsatzpolizzenType ersatzpolizzennummer;
    @XmlElement(name = "Vorversicherungen")
    protected VorversicherungenType vorversicherungen;
    @XmlElement(name = "BonusMalus")
    protected BonusMalusSystemType bonusMalus;
    @XmlElement(name = "ZusaetzlicheKfzDaten")
    protected ZusaetzlicheKfzdatenType zusaetzlicheKfzDaten;
    @XmlElement(name = "ZusendungWeitereDokumente")
    protected List<String> zusendungWeitereDokumente;

    /**
     * Ruft den Wert der verkaufsprodukt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VerkaufsproduktKfzType }
     *     
     */
    public VerkaufsproduktKfzType getVerkaufsprodukt() {
        return verkaufsprodukt;
    }

    /**
     * Legt den Wert der verkaufsprodukt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VerkaufsproduktKfzType }
     *     
     */
    public void setVerkaufsprodukt(VerkaufsproduktKfzType value) {
        this.verkaufsprodukt = value;
    }

    /**
     * Ruft den Wert der ersatzpolizzennummer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ErsatzpolizzenType }
     *     
     */
    public ErsatzpolizzenType getErsatzpolizzennummer() {
        return ersatzpolizzennummer;
    }

    /**
     * Legt den Wert der ersatzpolizzennummer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ErsatzpolizzenType }
     *     
     */
    public void setErsatzpolizzennummer(ErsatzpolizzenType value) {
        this.ersatzpolizzennummer = value;
    }

    /**
     * Ruft den Wert der vorversicherungen-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VorversicherungenType }
     *     
     */
    public VorversicherungenType getVorversicherungen() {
        return vorversicherungen;
    }

    /**
     * Legt den Wert der vorversicherungen-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VorversicherungenType }
     *     
     */
    public void setVorversicherungen(VorversicherungenType value) {
        this.vorversicherungen = value;
    }

    /**
     * Ruft den Wert der bonusMalus-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BonusMalusSystemType }
     *     
     */
    public BonusMalusSystemType getBonusMalus() {
        return bonusMalus;
    }

    /**
     * Legt den Wert der bonusMalus-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BonusMalusSystemType }
     *     
     */
    public void setBonusMalus(BonusMalusSystemType value) {
        this.bonusMalus = value;
    }

    /**
     * Ruft den Wert der zusaetzlicheKfzDaten-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ZusaetzlicheKfzdatenType }
     *     
     */
    public ZusaetzlicheKfzdatenType getZusaetzlicheKfzDaten() {
        return zusaetzlicheKfzDaten;
    }

    /**
     * Legt den Wert der zusaetzlicheKfzDaten-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ZusaetzlicheKfzdatenType }
     *     
     */
    public void setZusaetzlicheKfzDaten(ZusaetzlicheKfzdatenType value) {
        this.zusaetzlicheKfzDaten = value;
    }

    /**
     * Gets the value of the zusendungWeitereDokumente property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the zusendungWeitereDokumente property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getZusendungWeitereDokumente().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getZusendungWeitereDokumente() {
        if (zusendungWeitereDokumente == null) {
            zusendungWeitereDokumente = new ArrayList<String>();
        }
        return this.zusendungWeitereDokumente;
    }

}
