
package at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.kfz;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.common.SubmitApplicationRequestType;


/**
 * Typ des Requestobjekts für eine Antragseinreichung Kfz
 * 
 * <p>Java-Klasse für SubmitApplicationKfzRequest_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SubmitApplicationKfzRequest_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.common}SubmitApplicationRequest_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Antraganfrage" type="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz}SpezAntragKfz_Type" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubmitApplicationKfzRequest_Type", propOrder = {
    "antraganfrage"
})
public class SubmitApplicationKfzRequestType
    extends SubmitApplicationRequestType
{

    @XmlElement(name = "Antraganfrage")
    protected SpezAntragKfzType antraganfrage;

    /**
     * Ruft den Wert der antraganfrage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SpezAntragKfzType }
     *     
     */
    public SpezAntragKfzType getAntraganfrage() {
        return antraganfrage;
    }

    /**
     * Legt den Wert der antraganfrage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SpezAntragKfzType }
     *     
     */
    public void setAntraganfrage(SpezAntragKfzType value) {
        this.antraganfrage = value;
    }

}
