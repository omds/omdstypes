
package at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.kfz;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Typ für das Elementarprodukt KFZ-Teilkasko
 * 
 * <p>Java-Klasse für TeilkaskoKfz_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="TeilkaskoKfz_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz}KaskoKfz_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Vandalismusklausel" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TeilkaskoKfz_Type", propOrder = {
    "vandalismusklausel"
})
public class TeilkaskoKfzType
    extends KaskoKfzType
{

    @XmlElement(name = "Vandalismusklausel")
    protected boolean vandalismusklausel;

    /**
     * Ruft den Wert der vandalismusklausel-Eigenschaft ab.
     * 
     */
    public boolean isVandalismusklausel() {
        return vandalismusklausel;
    }

    /**
     * Legt den Wert der vandalismusklausel-Eigenschaft fest.
     * 
     */
    public void setVandalismusklausel(boolean value) {
        this.vandalismusklausel = value;
    }

}
