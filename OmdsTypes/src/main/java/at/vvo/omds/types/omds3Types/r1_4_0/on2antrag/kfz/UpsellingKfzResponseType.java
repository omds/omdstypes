
package at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.kfz;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Response Upselling Alternativen
 * 
 * <p>Java-Klasse für UpsellingKfzResponse_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="UpsellingKfzResponse_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="UpsellingVerkaufsprodukte" type="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz}VerkaufsproduktKfz_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UpsellingKfzResponse_Type", propOrder = {
    "upsellingVerkaufsprodukte"
})
public abstract class UpsellingKfzResponseType {

    @XmlElement(name = "UpsellingVerkaufsprodukte")
    protected List<VerkaufsproduktKfzType> upsellingVerkaufsprodukte;

    /**
     * Gets the value of the upsellingVerkaufsprodukte property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the upsellingVerkaufsprodukte property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUpsellingVerkaufsprodukte().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link VerkaufsproduktKfzType }
     * 
     * 
     */
    public List<VerkaufsproduktKfzType> getUpsellingVerkaufsprodukte() {
        if (upsellingVerkaufsprodukte == null) {
            upsellingVerkaufsprodukte = new ArrayList<VerkaufsproduktKfzType>();
        }
        return this.upsellingVerkaufsprodukte;
    }

}
