
package at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.kfz;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für VarianteLeihwagen_Type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="VarianteLeihwagen_Type"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="VA"/&gt;
 *     &lt;enumeration value="VB"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "VarianteLeihwagen_Type")
@XmlEnum
public enum VarianteLeihwagenType {


    /**
     * ohne Leihwagen
     * 
     */
    VA,

    /**
     * mit Leihwagen
     * 
     */
    VB;

    public String value() {
        return name();
    }

    public static VarianteLeihwagenType fromValue(String v) {
        return valueOf(v);
    }

}
