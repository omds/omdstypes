
package at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.kfz;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Typ für das Elementarprodukt KFZ-Vollkasko
 * 
 * <p>Java-Klasse für VollkaskoKfz_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="VollkaskoKfz_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz}KaskoKfz_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Neuwertklausel" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="Leasingklausel" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VollkaskoKfz_Type", propOrder = {
    "neuwertklausel",
    "leasingklausel"
})
public class VollkaskoKfzType
    extends KaskoKfzType
{

    @XmlElement(name = "Neuwertklausel")
    protected Boolean neuwertklausel;
    @XmlElement(name = "Leasingklausel")
    protected Boolean leasingklausel;

    /**
     * Ruft den Wert der neuwertklausel-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isNeuwertklausel() {
        return neuwertklausel;
    }

    /**
     * Legt den Wert der neuwertklausel-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNeuwertklausel(Boolean value) {
        this.neuwertklausel = value;
    }

    /**
     * Ruft den Wert der leasingklausel-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isLeasingklausel() {
        return leasingklausel;
    }

    /**
     * Legt den Wert der leasingklausel-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setLeasingklausel(Boolean value) {
        this.leasingklausel = value;
    }

}
