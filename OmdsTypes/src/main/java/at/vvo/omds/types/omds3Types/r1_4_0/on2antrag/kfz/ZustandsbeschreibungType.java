
package at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.kfz;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für Zustandsbeschreibung_Type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="Zustandsbeschreibung_Type"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Besichtigung durch Betreuer"/&gt;
 *     &lt;enumeration value="Kaufvertrag eines Markenhändlers mit Beschreibung (Kopie erforderlich)"/&gt;
 *     &lt;enumeration value="Besichtigung durch ÖAMTC"/&gt;
 *     &lt;enumeration value="Nachbesichtigung"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "Zustandsbeschreibung_Type")
@XmlEnum
public enum ZustandsbeschreibungType {

    @XmlEnumValue("Besichtigung durch Betreuer")
    BESICHTIGUNG_DURCH_BETREUER("Besichtigung durch Betreuer"),
    @XmlEnumValue("Kaufvertrag eines Markenh\u00e4ndlers mit Beschreibung (Kopie erforderlich)")
    KAUFVERTRAG_EINES_MARKENHÄNDLERS_MIT_BESCHREIBUNG_KOPIE_ERFORDERLICH("Kaufvertrag eines Markenh\u00e4ndlers mit Beschreibung (Kopie erforderlich)"),
    @XmlEnumValue("Besichtigung durch \u00d6AMTC")
    BESICHTIGUNG_DURCH_ÖAMTC("Besichtigung durch \u00d6AMTC"),
    @XmlEnumValue("Nachbesichtigung")
    NACHBESICHTIGUNG("Nachbesichtigung");
    private final String value;

    ZustandsbeschreibungType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ZustandsbeschreibungType fromValue(String v) {
        for (ZustandsbeschreibungType c: ZustandsbeschreibungType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
