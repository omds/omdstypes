
package at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.rs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.common.CalculateRequestType;


/**
 * Typ des Requestobjekts für eine Berechnung Rechtsschutz
 * 
 * <p>Java-Klasse für CalculateRechtsschutzRequest_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CalculateRechtsschutzRequest_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.common}CalculateRequest_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Berechnungsanfrage" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.rs}SpezBerechnungRechtsschutz_Type"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CalculateRechtsschutzRequest_Type", propOrder = {
    "berechnungsanfrage"
})
public class CalculateRechtsschutzRequestType
    extends CalculateRequestType
{

    @XmlElement(name = "Berechnungsanfrage", required = true)
    protected SpezBerechnungRechtsschutzType berechnungsanfrage;

    /**
     * Ruft den Wert der berechnungsanfrage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SpezBerechnungRechtsschutzType }
     *     
     */
    public SpezBerechnungRechtsschutzType getBerechnungsanfrage() {
        return berechnungsanfrage;
    }

    /**
     * Legt den Wert der berechnungsanfrage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SpezBerechnungRechtsschutzType }
     *     
     */
    public void setBerechnungsanfrage(SpezBerechnungRechtsschutzType value) {
        this.berechnungsanfrage = value;
    }

}
