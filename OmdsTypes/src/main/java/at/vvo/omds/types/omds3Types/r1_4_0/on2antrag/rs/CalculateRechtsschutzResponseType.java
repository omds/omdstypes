
package at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.rs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.common.CalculateResponseType;


/**
 * Typ des Responseobjekts für eine Rechtsschutz-Berechnung
 * 
 * <p>Java-Klasse für CalculateRechtsschutzResponse_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CalculateRechtsschutzResponse_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.common}CalculateResponse_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Berechnungsantwort" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.rs}SpezBerechnungRechtsschutz_Type"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CalculateRechtsschutzResponse_Type", propOrder = {
    "berechnungsantwort"
})
public class CalculateRechtsschutzResponseType
    extends CalculateResponseType
{

    @XmlElement(name = "Berechnungsantwort", required = true)
    protected SpezBerechnungRechtsschutzType berechnungsantwort;

    /**
     * Ruft den Wert der berechnungsantwort-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SpezBerechnungRechtsschutzType }
     *     
     */
    public SpezBerechnungRechtsschutzType getBerechnungsantwort() {
        return berechnungsantwort;
    }

    /**
     * Legt den Wert der berechnungsantwort-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SpezBerechnungRechtsschutzType }
     *     
     */
    public void setBerechnungsantwort(SpezBerechnungRechtsschutzType value) {
        this.berechnungsantwort = value;
    }

}
