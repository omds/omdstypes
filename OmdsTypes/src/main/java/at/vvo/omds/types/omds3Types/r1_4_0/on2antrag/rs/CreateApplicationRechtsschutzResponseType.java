
package at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.rs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.common.CreateApplicationResponseType;


/**
 * Type des Responseobjekts für die Erstellung eines Rechtsschutzantrags
 * 
 * <p>Java-Klasse für CreateApplicationRechtsschutzResponse_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CreateApplicationRechtsschutzResponse_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.common}CreateApplicationResponse_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Antragsantwort" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.rs}SpezAntragRechtsschutz_Type"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreateApplicationRechtsschutzResponse_Type", propOrder = {
    "antragsantwort"
})
public class CreateApplicationRechtsschutzResponseType
    extends CreateApplicationResponseType
{

    @XmlElement(name = "Antragsantwort", required = true)
    protected SpezAntragRechtsschutzType antragsantwort;

    /**
     * Ruft den Wert der antragsantwort-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SpezAntragRechtsschutzType }
     *     
     */
    public SpezAntragRechtsschutzType getAntragsantwort() {
        return antragsantwort;
    }

    /**
     * Legt den Wert der antragsantwort-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SpezAntragRechtsschutzType }
     *     
     */
    public void setAntragsantwort(SpezAntragRechtsschutzType value) {
        this.antragsantwort = value;
    }

}
