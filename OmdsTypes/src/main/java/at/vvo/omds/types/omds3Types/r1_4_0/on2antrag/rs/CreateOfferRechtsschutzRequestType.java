
package at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.rs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.common.CreateOfferRequestType;


/**
 * Typ des Requestobjekts für eine Erstellung eines Rechstsschutz-Offerts
 * 
 * <p>Java-Klasse für CreateOfferRechtsschutzRequest_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CreateOfferRechtsschutzRequest_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.common}CreateOfferRequest_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Offertanfrage" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.rs}SpezOffertRechtsschutz_Type"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreateOfferRechtsschutzRequest_Type", propOrder = {
    "offertanfrage"
})
public class CreateOfferRechtsschutzRequestType
    extends CreateOfferRequestType
{

    @XmlElement(name = "Offertanfrage", required = true)
    protected SpezOffertRechtsschutzType offertanfrage;

    /**
     * Ruft den Wert der offertanfrage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SpezOffertRechtsschutzType }
     *     
     */
    public SpezOffertRechtsschutzType getOffertanfrage() {
        return offertanfrage;
    }

    /**
     * Legt den Wert der offertanfrage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SpezOffertRechtsschutzType }
     *     
     */
    public void setOffertanfrage(SpezOffertRechtsschutzType value) {
        this.offertanfrage = value;
    }

}
