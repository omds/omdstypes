
package at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.rs;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.SelbstbehaltType;


/**
 * Typ für ein Elementarprodukt in der Sparte Rechtsschutz. Von diesem Typ werden etwaige Standard-Deckungen abgeleitet, siehe Vertragsrechtsschutz_Type. Von diesem Typ können einzelne VUs aber auch ihre eigenen Elementarprodukte ableiten, wenn sie möchten.
 * 
 * <p>Java-Klasse für ElementarproduktRechtsschutzStdImpl_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ElementarproduktRechtsschutzStdImpl_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.rs}ElementarproduktRechtsschutz_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Versicherungssumme" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;attribute name="Unlimitiert" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *                 &lt;attribute name="Betrag" type="{urn:omds20}decimal" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Selbstbehalt" type="{urn:omds3CommonServiceTypes-1-1-0}Selbstbehalt_Type" minOccurs="0"/&gt;
 *         &lt;element name="OertlicherGeltungsbereich" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.rs}OertlicherGeltungsbereichCd_Type" minOccurs="0"/&gt;
 *         &lt;element name="WartezeitInMonaten" type="{http://www.w3.org/2001/XMLSchema}unsignedByte" minOccurs="0"/&gt;
 *         &lt;element name="Wartezeitverzicht" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="Streitwertobergrenze" type="{urn:omds20}decimal" minOccurs="0"/&gt;
 *         &lt;element name="Streitwertuntergrenze" type="{urn:omds20}decimal" minOccurs="0"/&gt;
 *         &lt;element name="FreieAnwaltswahl" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ElementarproduktRechtsschutzStdImpl_Type", propOrder = {
    "versicherungssumme",
    "selbstbehalt",
    "oertlicherGeltungsbereich",
    "wartezeitInMonaten",
    "wartezeitverzicht",
    "streitwertobergrenze",
    "streitwertuntergrenze",
    "freieAnwaltswahl"
})
public class ElementarproduktRechtsschutzStdImplType
    extends ElementarproduktRechtsschutzType
{

    @XmlElement(name = "Versicherungssumme")
    protected Versicherungssumme versicherungssumme;
    @XmlElement(name = "Selbstbehalt")
    protected SelbstbehaltType selbstbehalt;
    @XmlElement(name = "OertlicherGeltungsbereich")
    @XmlSchemaType(name = "unsignedByte")
    protected Short oertlicherGeltungsbereich;
    @XmlElement(name = "WartezeitInMonaten")
    @XmlSchemaType(name = "unsignedByte")
    protected Short wartezeitInMonaten;
    @XmlElement(name = "Wartezeitverzicht")
    protected Boolean wartezeitverzicht;
    @XmlElement(name = "Streitwertobergrenze")
    protected BigDecimal streitwertobergrenze;
    @XmlElement(name = "Streitwertuntergrenze")
    protected BigDecimal streitwertuntergrenze;
    @XmlElement(name = "FreieAnwaltswahl")
    protected Boolean freieAnwaltswahl;

    /**
     * Ruft den Wert der versicherungssumme-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Versicherungssumme }
     *     
     */
    public Versicherungssumme getVersicherungssumme() {
        return versicherungssumme;
    }

    /**
     * Legt den Wert der versicherungssumme-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Versicherungssumme }
     *     
     */
    public void setVersicherungssumme(Versicherungssumme value) {
        this.versicherungssumme = value;
    }

    /**
     * Ruft den Wert der selbstbehalt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SelbstbehaltType }
     *     
     */
    public SelbstbehaltType getSelbstbehalt() {
        return selbstbehalt;
    }

    /**
     * Legt den Wert der selbstbehalt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SelbstbehaltType }
     *     
     */
    public void setSelbstbehalt(SelbstbehaltType value) {
        this.selbstbehalt = value;
    }

    /**
     * Ruft den Wert der oertlicherGeltungsbereich-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getOertlicherGeltungsbereich() {
        return oertlicherGeltungsbereich;
    }

    /**
     * Legt den Wert der oertlicherGeltungsbereich-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setOertlicherGeltungsbereich(Short value) {
        this.oertlicherGeltungsbereich = value;
    }

    /**
     * Ruft den Wert der wartezeitInMonaten-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getWartezeitInMonaten() {
        return wartezeitInMonaten;
    }

    /**
     * Legt den Wert der wartezeitInMonaten-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setWartezeitInMonaten(Short value) {
        this.wartezeitInMonaten = value;
    }

    /**
     * Ruft den Wert der wartezeitverzicht-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isWartezeitverzicht() {
        return wartezeitverzicht;
    }

    /**
     * Legt den Wert der wartezeitverzicht-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setWartezeitverzicht(Boolean value) {
        this.wartezeitverzicht = value;
    }

    /**
     * Ruft den Wert der streitwertobergrenze-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getStreitwertobergrenze() {
        return streitwertobergrenze;
    }

    /**
     * Legt den Wert der streitwertobergrenze-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setStreitwertobergrenze(BigDecimal value) {
        this.streitwertobergrenze = value;
    }

    /**
     * Ruft den Wert der streitwertuntergrenze-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getStreitwertuntergrenze() {
        return streitwertuntergrenze;
    }

    /**
     * Legt den Wert der streitwertuntergrenze-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setStreitwertuntergrenze(BigDecimal value) {
        this.streitwertuntergrenze = value;
    }

    /**
     * Ruft den Wert der freieAnwaltswahl-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isFreieAnwaltswahl() {
        return freieAnwaltswahl;
    }

    /**
     * Legt den Wert der freieAnwaltswahl-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFreieAnwaltswahl(Boolean value) {
        this.freieAnwaltswahl = value;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;attribute name="Unlimitiert" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
     *       &lt;attribute name="Betrag" type="{urn:omds20}decimal" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Versicherungssumme {

        @XmlAttribute(name = "Unlimitiert", namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.rs", required = true)
        protected boolean unlimitiert;
        @XmlAttribute(name = "Betrag", namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.rs")
        protected BigDecimal betrag;

        /**
         * Ruft den Wert der unlimitiert-Eigenschaft ab.
         * 
         */
        public boolean isUnlimitiert() {
            return unlimitiert;
        }

        /**
         * Legt den Wert der unlimitiert-Eigenschaft fest.
         * 
         */
        public void setUnlimitiert(boolean value) {
            this.unlimitiert = value;
        }

        /**
         * Ruft den Wert der betrag-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getBetrag() {
            return betrag;
        }

        /**
         * Legt den Wert der betrag-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setBetrag(BigDecimal value) {
            this.betrag = value;
        }

    }

}
