
package at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.rs;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.rs package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _VerkaufsproduktRechtsschutzStdImpl_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.rs", "VerkaufsproduktRechtsschutzStdImpl");
    private final static QName _ProduktRechtsschutzStdImpl_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.rs", "ProduktRechtsschutzStdImpl");
    private final static QName _ElementarproduktRechtsschutzStdImpl_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.rs", "ElementarproduktRechtsschutzStdImpl");
    private final static QName _CalculateRechtsschutzRequest_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.rs", "CalculateRechtsschutzRequest");
    private final static QName _CalculateRechtsschutzResponse_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.rs", "CalculateRechtsschutzResponse");
    private final static QName _CreateOfferRechtsschutzRequest_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.rs", "CreateOfferRechtsschutzRequest");
    private final static QName _CreateOfferRechtsschutzResponse_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.rs", "CreateOfferRechtsschutzResponse");
    private final static QName _CreateApplicationRechtsschutzRequest_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.rs", "CreateApplicationRechtsschutzRequest");
    private final static QName _CreateApplicationRechtsschutzResponse_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.rs", "CreateApplicationRechtsschutzResponse");
    private final static QName _SubmitApplicationRechtsschutzRequest_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.rs", "SubmitApplicationRechtsschutzRequest");
    private final static QName _SubmitApplicationRechtsschutzResponse_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.rs", "SubmitApplicationRechtsschutzResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.rs
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ElementarproduktRechtsschutzStdImplType }
     * 
     */
    public ElementarproduktRechtsschutzStdImplType createElementarproduktRechtsschutzStdImplType() {
        return new ElementarproduktRechtsschutzStdImplType();
    }

    /**
     * Create an instance of {@link VerkaufsproduktRechtsschutzStdImplType }
     * 
     */
    public VerkaufsproduktRechtsschutzStdImplType createVerkaufsproduktRechtsschutzStdImplType() {
        return new VerkaufsproduktRechtsschutzStdImplType();
    }

    /**
     * Create an instance of {@link ProduktRechtsschutzStdImplType }
     * 
     */
    public ProduktRechtsschutzStdImplType createProduktRechtsschutzStdImplType() {
        return new ProduktRechtsschutzStdImplType();
    }

    /**
     * Create an instance of {@link ElementarproduktRechtsschutzType }
     * 
     */
    public ElementarproduktRechtsschutzType createElementarproduktRechtsschutzType() {
        return new ElementarproduktRechtsschutzType();
    }

    /**
     * Create an instance of {@link CalculateRechtsschutzRequestType }
     * 
     */
    public CalculateRechtsschutzRequestType createCalculateRechtsschutzRequestType() {
        return new CalculateRechtsschutzRequestType();
    }

    /**
     * Create an instance of {@link CalculateRechtsschutzResponseType }
     * 
     */
    public CalculateRechtsschutzResponseType createCalculateRechtsschutzResponseType() {
        return new CalculateRechtsschutzResponseType();
    }

    /**
     * Create an instance of {@link CreateOfferRechtsschutzRequestType }
     * 
     */
    public CreateOfferRechtsschutzRequestType createCreateOfferRechtsschutzRequestType() {
        return new CreateOfferRechtsschutzRequestType();
    }

    /**
     * Create an instance of {@link CreateOfferRechtsschutzResponseType }
     * 
     */
    public CreateOfferRechtsschutzResponseType createCreateOfferRechtsschutzResponseType() {
        return new CreateOfferRechtsschutzResponseType();
    }

    /**
     * Create an instance of {@link CreateApplicationRechtsschutzRequestType }
     * 
     */
    public CreateApplicationRechtsschutzRequestType createCreateApplicationRechtsschutzRequestType() {
        return new CreateApplicationRechtsschutzRequestType();
    }

    /**
     * Create an instance of {@link CreateApplicationRechtsschutzResponseType }
     * 
     */
    public CreateApplicationRechtsschutzResponseType createCreateApplicationRechtsschutzResponseType() {
        return new CreateApplicationRechtsschutzResponseType();
    }

    /**
     * Create an instance of {@link SubmitApplicationRechtsschutzResponseType }
     * 
     */
    public SubmitApplicationRechtsschutzResponseType createSubmitApplicationRechtsschutzResponseType() {
        return new SubmitApplicationRechtsschutzResponseType();
    }

    /**
     * Create an instance of {@link ElementarproduktVertragsrechtsschutzType }
     * 
     */
    public ElementarproduktVertragsrechtsschutzType createElementarproduktVertragsrechtsschutzType() {
        return new ElementarproduktVertragsrechtsschutzType();
    }

    /**
     * Create an instance of {@link SpezBerechnungRechtsschutzType }
     * 
     */
    public SpezBerechnungRechtsschutzType createSpezBerechnungRechtsschutzType() {
        return new SpezBerechnungRechtsschutzType();
    }

    /**
     * Create an instance of {@link SpezOffertRechtsschutzType }
     * 
     */
    public SpezOffertRechtsschutzType createSpezOffertRechtsschutzType() {
        return new SpezOffertRechtsschutzType();
    }

    /**
     * Create an instance of {@link SpezAntragRechtsschutzType }
     * 
     */
    public SpezAntragRechtsschutzType createSpezAntragRechtsschutzType() {
        return new SpezAntragRechtsschutzType();
    }

    /**
     * Create an instance of {@link SubmitApplicationRechtsschutzRequestType }
     * 
     */
    public SubmitApplicationRechtsschutzRequestType createSubmitApplicationRechtsschutzRequestType() {
        return new SubmitApplicationRechtsschutzRequestType();
    }

    /**
     * Create an instance of {@link ElementarproduktRechtsschutzStdImplType.Versicherungssumme }
     * 
     */
    public ElementarproduktRechtsschutzStdImplType.Versicherungssumme createElementarproduktRechtsschutzStdImplTypeVersicherungssumme() {
        return new ElementarproduktRechtsschutzStdImplType.Versicherungssumme();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VerkaufsproduktRechtsschutzStdImplType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.rs", name = "VerkaufsproduktRechtsschutzStdImpl")
    public JAXBElement<VerkaufsproduktRechtsschutzStdImplType> createVerkaufsproduktRechtsschutzStdImpl(VerkaufsproduktRechtsschutzStdImplType value) {
        return new JAXBElement<VerkaufsproduktRechtsschutzStdImplType>(_VerkaufsproduktRechtsschutzStdImpl_QNAME, VerkaufsproduktRechtsschutzStdImplType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProduktRechtsschutzStdImplType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.rs", name = "ProduktRechtsschutzStdImpl")
    public JAXBElement<ProduktRechtsschutzStdImplType> createProduktRechtsschutzStdImpl(ProduktRechtsschutzStdImplType value) {
        return new JAXBElement<ProduktRechtsschutzStdImplType>(_ProduktRechtsschutzStdImpl_QNAME, ProduktRechtsschutzStdImplType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ElementarproduktRechtsschutzType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.rs", name = "ElementarproduktRechtsschutzStdImpl")
    public JAXBElement<ElementarproduktRechtsschutzType> createElementarproduktRechtsschutzStdImpl(ElementarproduktRechtsschutzType value) {
        return new JAXBElement<ElementarproduktRechtsschutzType>(_ElementarproduktRechtsschutzStdImpl_QNAME, ElementarproduktRechtsschutzType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CalculateRechtsschutzRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.rs", name = "CalculateRechtsschutzRequest")
    public JAXBElement<CalculateRechtsschutzRequestType> createCalculateRechtsschutzRequest(CalculateRechtsschutzRequestType value) {
        return new JAXBElement<CalculateRechtsschutzRequestType>(_CalculateRechtsschutzRequest_QNAME, CalculateRechtsschutzRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CalculateRechtsschutzResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.rs", name = "CalculateRechtsschutzResponse")
    public JAXBElement<CalculateRechtsschutzResponseType> createCalculateRechtsschutzResponse(CalculateRechtsschutzResponseType value) {
        return new JAXBElement<CalculateRechtsschutzResponseType>(_CalculateRechtsschutzResponse_QNAME, CalculateRechtsschutzResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateOfferRechtsschutzRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.rs", name = "CreateOfferRechtsschutzRequest")
    public JAXBElement<CreateOfferRechtsschutzRequestType> createCreateOfferRechtsschutzRequest(CreateOfferRechtsschutzRequestType value) {
        return new JAXBElement<CreateOfferRechtsschutzRequestType>(_CreateOfferRechtsschutzRequest_QNAME, CreateOfferRechtsschutzRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateOfferRechtsschutzResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.rs", name = "CreateOfferRechtsschutzResponse")
    public JAXBElement<CreateOfferRechtsschutzResponseType> createCreateOfferRechtsschutzResponse(CreateOfferRechtsschutzResponseType value) {
        return new JAXBElement<CreateOfferRechtsschutzResponseType>(_CreateOfferRechtsschutzResponse_QNAME, CreateOfferRechtsschutzResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateApplicationRechtsschutzRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.rs", name = "CreateApplicationRechtsschutzRequest")
    public JAXBElement<CreateApplicationRechtsschutzRequestType> createCreateApplicationRechtsschutzRequest(CreateApplicationRechtsschutzRequestType value) {
        return new JAXBElement<CreateApplicationRechtsschutzRequestType>(_CreateApplicationRechtsschutzRequest_QNAME, CreateApplicationRechtsschutzRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateApplicationRechtsschutzResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.rs", name = "CreateApplicationRechtsschutzResponse")
    public JAXBElement<CreateApplicationRechtsschutzResponseType> createCreateApplicationRechtsschutzResponse(CreateApplicationRechtsschutzResponseType value) {
        return new JAXBElement<CreateApplicationRechtsschutzResponseType>(_CreateApplicationRechtsschutzResponse_QNAME, CreateApplicationRechtsschutzResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubmitApplicationRechtsschutzResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.rs", name = "SubmitApplicationRechtsschutzRequest")
    public JAXBElement<SubmitApplicationRechtsschutzResponseType> createSubmitApplicationRechtsschutzRequest(SubmitApplicationRechtsschutzResponseType value) {
        return new JAXBElement<SubmitApplicationRechtsschutzResponseType>(_SubmitApplicationRechtsschutzRequest_QNAME, SubmitApplicationRechtsschutzResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubmitApplicationRechtsschutzResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.rs", name = "SubmitApplicationRechtsschutzResponse")
    public JAXBElement<SubmitApplicationRechtsschutzResponseType> createSubmitApplicationRechtsschutzResponse(SubmitApplicationRechtsschutzResponseType value) {
        return new JAXBElement<SubmitApplicationRechtsschutzResponseType>(_SubmitApplicationRechtsschutzResponse_QNAME, SubmitApplicationRechtsschutzResponseType.class, null, value);
    }

}
