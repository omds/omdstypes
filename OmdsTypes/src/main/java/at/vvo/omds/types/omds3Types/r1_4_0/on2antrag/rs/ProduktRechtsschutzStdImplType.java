
package at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.rs;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Typ der Standardimplementierung eines Produkts in der Sparte Rechtsschutz. Von diesem Typ können einzelne VUs ihre eigenen Produkte ableiten, wenn sie möchten.
 * 
 * <p>Java-Klasse für ProduktRechtsschutzStdImpl_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ProduktRechtsschutzStdImpl_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.rs}ProduktRechtsschutz_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Elementarprodukt" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.rs}ElementarproduktRechtsschutz_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProduktRechtsschutzStdImpl_Type", propOrder = {
    "elementarprodukt"
})
public class ProduktRechtsschutzStdImplType
    extends ProduktRechtsschutzType
{

    @XmlElement(name = "Elementarprodukt")
    protected List<ElementarproduktRechtsschutzType> elementarprodukt;

    /**
     * Gets the value of the elementarprodukt property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the elementarprodukt property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getElementarprodukt().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ElementarproduktRechtsschutzType }
     * 
     * 
     */
    public List<ElementarproduktRechtsschutzType> getElementarprodukt() {
        if (elementarprodukt == null) {
            elementarprodukt = new ArrayList<ElementarproduktRechtsschutzType>();
        }
        return this.elementarprodukt;
    }

}
