
package at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.rs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.ProduktType;


/**
 * Typ für ein Produkt in der Sparte Rechtsschutz. Von diesem Typ können einzelne VUs ihre eigenen Produkte ableiten, wenn sie möchten.
 * 
 * <p>Java-Klasse für ProduktRechtsschutz_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ProduktRechtsschutz_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}Produkt_Type"&gt;
 *       &lt;sequence&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProduktRechtsschutz_Type")
@XmlSeeAlso({
    ProduktRechtsschutzStdImplType.class
})
public abstract class ProduktRechtsschutzType
    extends ProduktType
{


}
