
package at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.rs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.common.SpezBerechnungType;


/**
 * Typ für den Schritt Berechnung
 * 
 * <p>Java-Klasse für SpezBerechnungRechtsschutz_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SpezBerechnungRechtsschutz_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.common}SpezBerechnung_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Verkaufsprodukt" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.rs}VerkaufsproduktRechtsschutz_Type"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SpezBerechnungRechtsschutz_Type", propOrder = {
    "verkaufsprodukt"
})
public class SpezBerechnungRechtsschutzType
    extends SpezBerechnungType
{

    @XmlElement(name = "Verkaufsprodukt", required = true)
    protected VerkaufsproduktRechtsschutzType verkaufsprodukt;

    /**
     * Ruft den Wert der verkaufsprodukt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VerkaufsproduktRechtsschutzType }
     *     
     */
    public VerkaufsproduktRechtsschutzType getVerkaufsprodukt() {
        return verkaufsprodukt;
    }

    /**
     * Legt den Wert der verkaufsprodukt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VerkaufsproduktRechtsschutzType }
     *     
     */
    public void setVerkaufsprodukt(VerkaufsproduktRechtsschutzType value) {
        this.verkaufsprodukt = value;
    }

}
