
package at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.rs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.common.SubmitApplicationRequestType;


/**
 * Type des Requests, um den Antrag einzureichen
 * 
 * <p>Java-Klasse für SubmitApplicationRechtsschutzRequest_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SubmitApplicationRechtsschutzRequest_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.common}SubmitApplicationRequest_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Antragsanfrage" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.rs}SpezAntragRechtsschutz_Type"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubmitApplicationRechtsschutzRequest_Type", propOrder = {
    "antragsanfrage"
})
public class SubmitApplicationRechtsschutzRequestType
    extends SubmitApplicationRequestType
{

    @XmlElement(name = "Antragsanfrage", required = true)
    protected SpezAntragRechtsschutzType antragsanfrage;

    /**
     * Ruft den Wert der antragsanfrage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SpezAntragRechtsschutzType }
     *     
     */
    public SpezAntragRechtsschutzType getAntragsanfrage() {
        return antragsanfrage;
    }

    /**
     * Legt den Wert der antragsanfrage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SpezAntragRechtsschutzType }
     *     
     */
    public void setAntragsanfrage(SpezAntragRechtsschutzType value) {
        this.antragsanfrage = value;
    }

}
