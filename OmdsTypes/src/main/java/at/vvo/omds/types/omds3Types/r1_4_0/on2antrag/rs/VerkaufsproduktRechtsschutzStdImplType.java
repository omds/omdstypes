
package at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.rs;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.ProduktType;


/**
 * Typ der Standardimplementierung für ein Verkaufsprodukt in der Sparte Rechtsschutz
 * 
 * <p>Java-Klasse für VerkaufsproduktRechtsschutzStdImpl_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="VerkaufsproduktRechtsschutzStdImpl_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.rs}VerkaufsproduktRechtsschutz_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Rechtsschutzversicherung" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.rs}ProduktRechtsschutz_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Zusatzversicherung" type="{urn:omds3CommonServiceTypes-1-1-0}Produkt_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VerkaufsproduktRechtsschutzStdImpl_Type", propOrder = {
    "rechtsschutzversicherung",
    "zusatzversicherung"
})
public class VerkaufsproduktRechtsschutzStdImplType
    extends VerkaufsproduktRechtsschutzType
{

    @XmlElement(name = "Rechtsschutzversicherung")
    protected List<ProduktRechtsschutzType> rechtsschutzversicherung;
    @XmlElement(name = "Zusatzversicherung")
    protected List<ProduktType> zusatzversicherung;

    /**
     * Gets the value of the rechtsschutzversicherung property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rechtsschutzversicherung property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRechtsschutzversicherung().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProduktRechtsschutzType }
     * 
     * 
     */
    public List<ProduktRechtsschutzType> getRechtsschutzversicherung() {
        if (rechtsschutzversicherung == null) {
            rechtsschutzversicherung = new ArrayList<ProduktRechtsschutzType>();
        }
        return this.rechtsschutzversicherung;
    }

    /**
     * Gets the value of the zusatzversicherung property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the zusatzversicherung property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getZusatzversicherung().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProduktType }
     * 
     * 
     */
    public List<ProduktType> getZusatzversicherung() {
        if (zusatzversicherung == null) {
            zusatzversicherung = new ArrayList<ProduktType>();
        }
        return this.zusatzversicherung;
    }

}
