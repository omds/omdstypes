
package at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.rs;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.VerkaufsproduktType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.VersichertesInteresseType;


/**
 * Typ für ein Verkaufsprodukt in der Sparte Rechtsschutz
 * 
 * <p>Java-Klasse für VerkaufsproduktRechtsschutz_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="VerkaufsproduktRechtsschutz_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}Verkaufsprodukt_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="VersicherteInteressen" type="{urn:omds3CommonServiceTypes-1-1-0}VersichertesInteresse_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VerkaufsproduktRechtsschutz_Type", propOrder = {
    "versicherteInteressen"
})
@XmlSeeAlso({
    VerkaufsproduktRechtsschutzStdImplType.class
})
public abstract class VerkaufsproduktRechtsschutzType
    extends VerkaufsproduktType
{

    @XmlElement(name = "VersicherteInteressen")
    protected List<VersichertesInteresseType> versicherteInteressen;

    /**
     * Gets the value of the versicherteInteressen property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the versicherteInteressen property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVersicherteInteressen().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link VersichertesInteresseType }
     * 
     * 
     */
    public List<VersichertesInteresseType> getVersicherteInteressen() {
        if (versicherteInteressen == null) {
            versicherteInteressen = new ArrayList<VersichertesInteresseType>();
        }
        return this.versicherteInteressen;
    }

}
