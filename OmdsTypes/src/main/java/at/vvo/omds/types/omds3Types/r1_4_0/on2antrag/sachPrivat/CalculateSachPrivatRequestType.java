
package at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.sachPrivat;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.common.CalculateRequestType;


/**
 * Typ des Requestobjekts für eine Berechnung Sach-Privat
 * 
 * <p>Java-Klasse für CalculateSachPrivatRequest_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CalculateSachPrivatRequest_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.common}CalculateRequest_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Berechnungsanfrage" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.sachprivat}BerechnungSachPrivat_Type"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CalculateSachPrivatRequest_Type", propOrder = {
    "berechnungsanfrage"
})
public class CalculateSachPrivatRequestType
    extends CalculateRequestType
{

    @XmlElement(name = "Berechnungsanfrage", required = true)
    protected BerechnungSachPrivatType berechnungsanfrage;

    /**
     * Ruft den Wert der berechnungsanfrage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BerechnungSachPrivatType }
     *     
     */
    public BerechnungSachPrivatType getBerechnungsanfrage() {
        return berechnungsanfrage;
    }

    /**
     * Legt den Wert der berechnungsanfrage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BerechnungSachPrivatType }
     *     
     */
    public void setBerechnungsanfrage(BerechnungSachPrivatType value) {
        this.berechnungsanfrage = value;
    }

}
