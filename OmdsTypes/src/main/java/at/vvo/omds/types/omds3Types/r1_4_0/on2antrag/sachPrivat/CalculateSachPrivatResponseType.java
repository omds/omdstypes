
package at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.sachPrivat;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.CommonResponseType;


/**
 * Typ des Responseobjekts für eine Berechnung Sach-Privat
 * 
 * <p>Java-Klasse für CalculateSachPrivatResponse_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CalculateSachPrivatResponse_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}CommonResponse_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Berechnungsantwort" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.sachprivat}BerechnungSachPrivat_Type"/&gt;
 *         &lt;element name="ResponseUpselling" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.sachprivat}UpsellingSachPrivatResponse_Type" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CalculateSachPrivatResponse_Type", propOrder = {
    "berechnungsantwort",
    "responseUpselling"
})
public class CalculateSachPrivatResponseType
    extends CommonResponseType
{

    @XmlElement(name = "Berechnungsantwort", required = true)
    protected BerechnungSachPrivatType berechnungsantwort;
    @XmlElement(name = "ResponseUpselling")
    protected UpsellingSachPrivatResponseType responseUpselling;

    /**
     * Ruft den Wert der berechnungsantwort-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BerechnungSachPrivatType }
     *     
     */
    public BerechnungSachPrivatType getBerechnungsantwort() {
        return berechnungsantwort;
    }

    /**
     * Legt den Wert der berechnungsantwort-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BerechnungSachPrivatType }
     *     
     */
    public void setBerechnungsantwort(BerechnungSachPrivatType value) {
        this.berechnungsantwort = value;
    }

    /**
     * Ruft den Wert der responseUpselling-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link UpsellingSachPrivatResponseType }
     *     
     */
    public UpsellingSachPrivatResponseType getResponseUpselling() {
        return responseUpselling;
    }

    /**
     * Legt den Wert der responseUpselling-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link UpsellingSachPrivatResponseType }
     *     
     */
    public void setResponseUpselling(UpsellingSachPrivatResponseType value) {
        this.responseUpselling = value;
    }

}
