
package at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.sachPrivat;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.common.CreateApplicationRequestType;


/**
 * Typ des Requestobjekts für einen Antrag Besitz
 * 
 * <p>Java-Klasse für CreateApplicationSachPrivatRequest_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CreateApplicationSachPrivatRequest_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.common}CreateApplicationRequest_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Antragsanfrage" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.sachprivat}AntragSachPrivat_Type"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreateApplicationSachPrivatRequest_Type", propOrder = {
    "antragsanfrage"
})
public class CreateApplicationSachPrivatRequestType
    extends CreateApplicationRequestType
{

    @XmlElement(name = "Antragsanfrage", required = true)
    protected AntragSachPrivatType antragsanfrage;

    /**
     * Ruft den Wert der antragsanfrage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AntragSachPrivatType }
     *     
     */
    public AntragSachPrivatType getAntragsanfrage() {
        return antragsanfrage;
    }

    /**
     * Legt den Wert der antragsanfrage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AntragSachPrivatType }
     *     
     */
    public void setAntragsanfrage(AntragSachPrivatType value) {
        this.antragsanfrage = value;
    }

}
