
package at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.sachPrivat;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.ElementarproduktType;


/**
 * <p>Java-Klasse für ElementarproduktSachPrivat_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ElementarproduktSachPrivat_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}Elementarprodukt_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="VersInteresseRefLfnr" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ElementarproduktSachPrivat_Type", propOrder = {
    "versInteresseRefLfnr"
})
@XmlSeeAlso({
    ElementarproduktGebaeudeType.class,
    ElementarproduktHaushaltType.class
})
public abstract class ElementarproduktSachPrivatType
    extends ElementarproduktType
{

    @XmlElement(name = "VersInteresseRefLfnr")
    protected List<String> versInteresseRefLfnr;

    /**
     * Gets the value of the versInteresseRefLfnr property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the versInteresseRefLfnr property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVersInteresseRefLfnr().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getVersInteresseRefLfnr() {
        if (versInteresseRefLfnr == null) {
            versInteresseRefLfnr = new ArrayList<String>();
        }
        return this.versInteresseRefLfnr;
    }

}
