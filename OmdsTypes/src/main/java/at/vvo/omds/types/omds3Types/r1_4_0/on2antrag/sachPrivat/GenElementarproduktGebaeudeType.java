
package at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.sachPrivat;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.KostenFixOderProzentType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.SelbstbehaltType;


/**
 * <p>Java-Klasse für GenElementarproduktGebaeude_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="GenElementarproduktGebaeude_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.sachprivat}ElementarproduktGebaeude_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Sparte" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.sachprivat}GebaeudeSpartenCd_Type"/&gt;
 *         &lt;element name="Pauschalbetrag" type="{http://www.w3.org/2001/XMLSchema}unsignedLong" minOccurs="0"/&gt;
 *         &lt;element name="Selbstbehalt" type="{urn:omds3CommonServiceTypes-1-1-0}Selbstbehalt_Type" minOccurs="0"/&gt;
 *         &lt;element name="Versicherungssumme" type="{http://www.w3.org/2001/XMLSchema}unsignedLong" minOccurs="0"/&gt;
 *         &lt;element name="Unterversicherungsverzicht" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="ProzentVersicherungssumme" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" minOccurs="0"/&gt;
 *         &lt;element name="Nebenkosten" type="{urn:omds3CommonServiceTypes-1-1-0}KostenFixOderProzent_Type" minOccurs="0"/&gt;
 *         &lt;element name="Vorsorge" type="{urn:omds3CommonServiceTypes-1-1-0}KostenFixOderProzent_Type" minOccurs="0"/&gt;
 *         &lt;element name="Hoechsthaftungssumme" type="{http://www.w3.org/2001/XMLSchema}unsignedLong" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GenElementarproduktGebaeude_Type", propOrder = {
    "sparte",
    "pauschalbetrag",
    "selbstbehalt",
    "versicherungssumme",
    "unterversicherungsverzicht",
    "prozentVersicherungssumme",
    "nebenkosten",
    "vorsorge",
    "hoechsthaftungssumme"
})
public class GenElementarproduktGebaeudeType
    extends ElementarproduktGebaeudeType
{

    @XmlElement(name = "Sparte", required = true)
    protected String sparte;
    @XmlElement(name = "Pauschalbetrag")
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger pauschalbetrag;
    @XmlElement(name = "Selbstbehalt")
    protected SelbstbehaltType selbstbehalt;
    @XmlElement(name = "Versicherungssumme")
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger versicherungssumme;
    @XmlElement(name = "Unterversicherungsverzicht")
    protected Boolean unterversicherungsverzicht;
    @XmlElement(name = "ProzentVersicherungssumme")
    @XmlSchemaType(name = "unsignedInt")
    protected Long prozentVersicherungssumme;
    @XmlElement(name = "Nebenkosten")
    protected KostenFixOderProzentType nebenkosten;
    @XmlElement(name = "Vorsorge")
    protected KostenFixOderProzentType vorsorge;
    @XmlElement(name = "Hoechsthaftungssumme")
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger hoechsthaftungssumme;

    /**
     * Ruft den Wert der sparte-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSparte() {
        return sparte;
    }

    /**
     * Legt den Wert der sparte-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSparte(String value) {
        this.sparte = value;
    }

    /**
     * Ruft den Wert der pauschalbetrag-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPauschalbetrag() {
        return pauschalbetrag;
    }

    /**
     * Legt den Wert der pauschalbetrag-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPauschalbetrag(BigInteger value) {
        this.pauschalbetrag = value;
    }

    /**
     * Ruft den Wert der selbstbehalt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SelbstbehaltType }
     *     
     */
    public SelbstbehaltType getSelbstbehalt() {
        return selbstbehalt;
    }

    /**
     * Legt den Wert der selbstbehalt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SelbstbehaltType }
     *     
     */
    public void setSelbstbehalt(SelbstbehaltType value) {
        this.selbstbehalt = value;
    }

    /**
     * Ruft den Wert der versicherungssumme-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getVersicherungssumme() {
        return versicherungssumme;
    }

    /**
     * Legt den Wert der versicherungssumme-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setVersicherungssumme(BigInteger value) {
        this.versicherungssumme = value;
    }

    /**
     * Ruft den Wert der unterversicherungsverzicht-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUnterversicherungsverzicht() {
        return unterversicherungsverzicht;
    }

    /**
     * Legt den Wert der unterversicherungsverzicht-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUnterversicherungsverzicht(Boolean value) {
        this.unterversicherungsverzicht = value;
    }

    /**
     * Ruft den Wert der prozentVersicherungssumme-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getProzentVersicherungssumme() {
        return prozentVersicherungssumme;
    }

    /**
     * Legt den Wert der prozentVersicherungssumme-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setProzentVersicherungssumme(Long value) {
        this.prozentVersicherungssumme = value;
    }

    /**
     * Ruft den Wert der nebenkosten-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link KostenFixOderProzentType }
     *     
     */
    public KostenFixOderProzentType getNebenkosten() {
        return nebenkosten;
    }

    /**
     * Legt den Wert der nebenkosten-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link KostenFixOderProzentType }
     *     
     */
    public void setNebenkosten(KostenFixOderProzentType value) {
        this.nebenkosten = value;
    }

    /**
     * Ruft den Wert der vorsorge-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link KostenFixOderProzentType }
     *     
     */
    public KostenFixOderProzentType getVorsorge() {
        return vorsorge;
    }

    /**
     * Legt den Wert der vorsorge-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link KostenFixOderProzentType }
     *     
     */
    public void setVorsorge(KostenFixOderProzentType value) {
        this.vorsorge = value;
    }

    /**
     * Ruft den Wert der hoechsthaftungssumme-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getHoechsthaftungssumme() {
        return hoechsthaftungssumme;
    }

    /**
     * Legt den Wert der hoechsthaftungssumme-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setHoechsthaftungssumme(BigInteger value) {
        this.hoechsthaftungssumme = value;
    }

}
