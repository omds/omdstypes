
package at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.sachPrivat;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für Nebengebaeude_Type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="Nebengebaeude_Type"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Abstellgebaeude"/&gt;
 *     &lt;enumeration value="Badehütte"/&gt;
 *     &lt;enumeration value="Bootshaus"/&gt;
 *     &lt;enumeration value="Carport"/&gt;
 *     &lt;enumeration value="ehemaliges Wirtschaftsgebaeude"/&gt;
 *     &lt;enumeration value="Garage"/&gt;
 *     &lt;enumeration value="Gartenhaus (nicht für Wohnzwecke)"/&gt;
 *     &lt;enumeration value="Holzhütte"/&gt;
 *     &lt;enumeration value="Keller"/&gt;
 *     &lt;enumeration value="Mobilheim (stationaer)"/&gt;
 *     &lt;enumeration value="Nebengebaeude"/&gt;
 *     &lt;enumeration value="Nebengebaeude mit Garage"/&gt;
 *     &lt;enumeration value="Presshaus"/&gt;
 *     &lt;enumeration value="Sauna"/&gt;
 *     &lt;enumeration value="Scheune / Schuppen / Stadel"/&gt;
 *     &lt;enumeration value="Werkstaette"/&gt;
 *     &lt;enumeration value="Werkzeug- und Geraeteschuppen"/&gt;
 *     &lt;enumeration value="Wintergarten, Veranda"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "Nebengebaeude_Type")
@XmlEnum
public enum NebengebaeudeType {

    @XmlEnumValue("Abstellgebaeude")
    ABSTELLGEBAEUDE("Abstellgebaeude"),
    @XmlEnumValue("Badeh\u00fctte")
    BADEHÜTTE("Badeh\u00fctte"),
    @XmlEnumValue("Bootshaus")
    BOOTSHAUS("Bootshaus"),
    @XmlEnumValue("Carport")
    CARPORT("Carport"),
    @XmlEnumValue("ehemaliges Wirtschaftsgebaeude")
    EHEMALIGES_WIRTSCHAFTSGEBAEUDE("ehemaliges Wirtschaftsgebaeude"),
    @XmlEnumValue("Garage")
    GARAGE("Garage"),
    @XmlEnumValue("Gartenhaus (nicht f\u00fcr Wohnzwecke)")
    GARTENHAUS_NICHT_FÜR_WOHNZWECKE("Gartenhaus (nicht f\u00fcr Wohnzwecke)"),
    @XmlEnumValue("Holzh\u00fctte")
    HOLZHÜTTE("Holzh\u00fctte"),
    @XmlEnumValue("Keller")
    KELLER("Keller"),
    @XmlEnumValue("Mobilheim (stationaer)")
    MOBILHEIM_STATIONAER("Mobilheim (stationaer)"),
    @XmlEnumValue("Nebengebaeude")
    NEBENGEBAEUDE("Nebengebaeude"),
    @XmlEnumValue("Nebengebaeude mit Garage")
    NEBENGEBAEUDE_MIT_GARAGE("Nebengebaeude mit Garage"),
    @XmlEnumValue("Presshaus")
    PRESSHAUS("Presshaus"),
    @XmlEnumValue("Sauna")
    SAUNA("Sauna"),
    @XmlEnumValue("Scheune / Schuppen / Stadel")
    SCHEUNE_SCHUPPEN_STADEL("Scheune / Schuppen / Stadel"),
    @XmlEnumValue("Werkstaette")
    WERKSTAETTE("Werkstaette"),
    @XmlEnumValue("Werkzeug- und Geraeteschuppen")
    WERKZEUG_UND_GERAETESCHUPPEN("Werkzeug- und Geraeteschuppen"),
    @XmlEnumValue("Wintergarten, Veranda")
    WINTERGARTEN_VERANDA("Wintergarten, Veranda");
    private final String value;

    NebengebaeudeType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static NebengebaeudeType fromValue(String v) {
        for (NebengebaeudeType c: NebengebaeudeType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
