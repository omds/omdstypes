
package at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.sachPrivat;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.sachPrivat package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ProduktGebaeudeversicherung_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.sachprivat", "ProduktGebaeudeversicherung");
    private final static QName _ProduktHaushaltsversicherung_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.sachprivat", "ProduktHaushaltsversicherung");
    private final static QName _CalculateSachPrivatRequest_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.sachprivat", "CalculateSachPrivatRequest");
    private final static QName _CalculateSachPrivatResponse_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.sachprivat", "CalculateSachPrivatResponse");
    private final static QName _CreateOfferSachPrivatRequest_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.sachprivat", "CreateOfferSachPrivatRequest");
    private final static QName _CreateOfferSachPrivatResponse_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.sachprivat", "CreateOfferSachPrivatResponse");
    private final static QName _CreateApplicationSachPrivatRequest_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.sachprivat", "CreateApplicationSachPrivatRequest");
    private final static QName _CreateApplicationSachPrivatResponse_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.sachprivat", "CreateApplicationSachPrivatResponse");
    private final static QName _SubmitApplicationSachPrivatRequest_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.sachprivat", "SubmitApplicationSachPrivatRequest");
    private final static QName _SubmitApplicationSachPrivatResponse_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.sachprivat", "SubmitApplicationSachPrivatResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.sachPrivat
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ProduktGebaeudeversicherungType }
     * 
     */
    public ProduktGebaeudeversicherungType createProduktGebaeudeversicherungType() {
        return new ProduktGebaeudeversicherungType();
    }

    /**
     * Create an instance of {@link ProduktHaushaltsversicherungType }
     * 
     */
    public ProduktHaushaltsversicherungType createProduktHaushaltsversicherungType() {
        return new ProduktHaushaltsversicherungType();
    }

    /**
     * Create an instance of {@link CalculateSachPrivatRequestType }
     * 
     */
    public CalculateSachPrivatRequestType createCalculateSachPrivatRequestType() {
        return new CalculateSachPrivatRequestType();
    }

    /**
     * Create an instance of {@link CalculateSachPrivatResponseType }
     * 
     */
    public CalculateSachPrivatResponseType createCalculateSachPrivatResponseType() {
        return new CalculateSachPrivatResponseType();
    }

    /**
     * Create an instance of {@link CreateOfferSachPrivatRequestType }
     * 
     */
    public CreateOfferSachPrivatRequestType createCreateOfferSachPrivatRequestType() {
        return new CreateOfferSachPrivatRequestType();
    }

    /**
     * Create an instance of {@link CreateOfferSachPrivatResponseType }
     * 
     */
    public CreateOfferSachPrivatResponseType createCreateOfferSachPrivatResponseType() {
        return new CreateOfferSachPrivatResponseType();
    }

    /**
     * Create an instance of {@link CreateApplicationSachPrivatRequestType }
     * 
     */
    public CreateApplicationSachPrivatRequestType createCreateApplicationSachPrivatRequestType() {
        return new CreateApplicationSachPrivatRequestType();
    }

    /**
     * Create an instance of {@link CreateApplicationSachPrivatResponseType }
     * 
     */
    public CreateApplicationSachPrivatResponseType createCreateApplicationSachPrivatResponseType() {
        return new CreateApplicationSachPrivatResponseType();
    }

    /**
     * Create an instance of {@link SubmitApplicationSachPrivatRequestType }
     * 
     */
    public SubmitApplicationSachPrivatRequestType createSubmitApplicationSachPrivatRequestType() {
        return new SubmitApplicationSachPrivatRequestType();
    }

    /**
     * Create an instance of {@link SubmitApplicationSachPrivatResponseType }
     * 
     */
    public SubmitApplicationSachPrivatResponseType createSubmitApplicationSachPrivatResponseType() {
        return new SubmitApplicationSachPrivatResponseType();
    }

    /**
     * Create an instance of {@link VersichertesObjektSachPrivatType }
     * 
     */
    public VersichertesObjektSachPrivatType createVersichertesObjektSachPrivatType() {
        return new VersichertesObjektSachPrivatType();
    }

    /**
     * Create an instance of {@link RisikoAdresseType }
     * 
     */
    public RisikoAdresseType createRisikoAdresseType() {
        return new RisikoAdresseType();
    }

    /**
     * Create an instance of {@link RisikoHaushaltType }
     * 
     */
    public RisikoHaushaltType createRisikoHaushaltType() {
        return new RisikoHaushaltType();
    }

    /**
     * Create an instance of {@link RisikoGebaeudeType }
     * 
     */
    public RisikoGebaeudeType createRisikoGebaeudeType() {
        return new RisikoGebaeudeType();
    }

    /**
     * Create an instance of {@link ZusaetzlicheGebaeudedatenWintergartenType }
     * 
     */
    public ZusaetzlicheGebaeudedatenWintergartenType createZusaetzlicheGebaeudedatenWintergartenType() {
        return new ZusaetzlicheGebaeudedatenWintergartenType();
    }

    /**
     * Create an instance of {@link ZusaetzlicheGebaeudedatenSolarthermieType }
     * 
     */
    public ZusaetzlicheGebaeudedatenSolarthermieType createZusaetzlicheGebaeudedatenSolarthermieType() {
        return new ZusaetzlicheGebaeudedatenSolarthermieType();
    }

    /**
     * Create an instance of {@link ZusaetzlicheGebaeudedatenSchwimmbadType }
     * 
     */
    public ZusaetzlicheGebaeudedatenSchwimmbadType createZusaetzlicheGebaeudedatenSchwimmbadType() {
        return new ZusaetzlicheGebaeudedatenSchwimmbadType();
    }

    /**
     * Create an instance of {@link ZusaetzlicheGebaeudedatenPhotovoltaikType }
     * 
     */
    public ZusaetzlicheGebaeudedatenPhotovoltaikType createZusaetzlicheGebaeudedatenPhotovoltaikType() {
        return new ZusaetzlicheGebaeudedatenPhotovoltaikType();
    }

    /**
     * Create an instance of {@link VerkaufsproduktSachPrivatType }
     * 
     */
    public VerkaufsproduktSachPrivatType createVerkaufsproduktSachPrivatType() {
        return new VerkaufsproduktSachPrivatType();
    }

    /**
     * Create an instance of {@link GenElementarproduktGebaeudeType }
     * 
     */
    public GenElementarproduktGebaeudeType createGenElementarproduktGebaeudeType() {
        return new GenElementarproduktGebaeudeType();
    }

    /**
     * Create an instance of {@link GenElementarproduktHaushaltType }
     * 
     */
    public GenElementarproduktHaushaltType createGenElementarproduktHaushaltType() {
        return new GenElementarproduktHaushaltType();
    }

    /**
     * Create an instance of {@link BerechnungSachPrivatType }
     * 
     */
    public BerechnungSachPrivatType createBerechnungSachPrivatType() {
        return new BerechnungSachPrivatType();
    }

    /**
     * Create an instance of {@link OffertSachPrivatType }
     * 
     */
    public OffertSachPrivatType createOffertSachPrivatType() {
        return new OffertSachPrivatType();
    }

    /**
     * Create an instance of {@link AntragSachPrivatType }
     * 
     */
    public AntragSachPrivatType createAntragSachPrivatType() {
        return new AntragSachPrivatType();
    }

    /**
     * Create an instance of {@link UpsellingSachPrivatResponseType }
     * 
     */
    public UpsellingSachPrivatResponseType createUpsellingSachPrivatResponseType() {
        return new UpsellingSachPrivatResponseType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProduktGebaeudeversicherungType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.sachprivat", name = "ProduktGebaeudeversicherung")
    public JAXBElement<ProduktGebaeudeversicherungType> createProduktGebaeudeversicherung(ProduktGebaeudeversicherungType value) {
        return new JAXBElement<ProduktGebaeudeversicherungType>(_ProduktGebaeudeversicherung_QNAME, ProduktGebaeudeversicherungType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProduktHaushaltsversicherungType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.sachprivat", name = "ProduktHaushaltsversicherung")
    public JAXBElement<ProduktHaushaltsversicherungType> createProduktHaushaltsversicherung(ProduktHaushaltsversicherungType value) {
        return new JAXBElement<ProduktHaushaltsversicherungType>(_ProduktHaushaltsversicherung_QNAME, ProduktHaushaltsversicherungType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CalculateSachPrivatRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.sachprivat", name = "CalculateSachPrivatRequest")
    public JAXBElement<CalculateSachPrivatRequestType> createCalculateSachPrivatRequest(CalculateSachPrivatRequestType value) {
        return new JAXBElement<CalculateSachPrivatRequestType>(_CalculateSachPrivatRequest_QNAME, CalculateSachPrivatRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CalculateSachPrivatResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.sachprivat", name = "CalculateSachPrivatResponse")
    public JAXBElement<CalculateSachPrivatResponseType> createCalculateSachPrivatResponse(CalculateSachPrivatResponseType value) {
        return new JAXBElement<CalculateSachPrivatResponseType>(_CalculateSachPrivatResponse_QNAME, CalculateSachPrivatResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateOfferSachPrivatRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.sachprivat", name = "CreateOfferSachPrivatRequest")
    public JAXBElement<CreateOfferSachPrivatRequestType> createCreateOfferSachPrivatRequest(CreateOfferSachPrivatRequestType value) {
        return new JAXBElement<CreateOfferSachPrivatRequestType>(_CreateOfferSachPrivatRequest_QNAME, CreateOfferSachPrivatRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateOfferSachPrivatResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.sachprivat", name = "CreateOfferSachPrivatResponse")
    public JAXBElement<CreateOfferSachPrivatResponseType> createCreateOfferSachPrivatResponse(CreateOfferSachPrivatResponseType value) {
        return new JAXBElement<CreateOfferSachPrivatResponseType>(_CreateOfferSachPrivatResponse_QNAME, CreateOfferSachPrivatResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateApplicationSachPrivatRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.sachprivat", name = "CreateApplicationSachPrivatRequest")
    public JAXBElement<CreateApplicationSachPrivatRequestType> createCreateApplicationSachPrivatRequest(CreateApplicationSachPrivatRequestType value) {
        return new JAXBElement<CreateApplicationSachPrivatRequestType>(_CreateApplicationSachPrivatRequest_QNAME, CreateApplicationSachPrivatRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateApplicationSachPrivatResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.sachprivat", name = "CreateApplicationSachPrivatResponse")
    public JAXBElement<CreateApplicationSachPrivatResponseType> createCreateApplicationSachPrivatResponse(CreateApplicationSachPrivatResponseType value) {
        return new JAXBElement<CreateApplicationSachPrivatResponseType>(_CreateApplicationSachPrivatResponse_QNAME, CreateApplicationSachPrivatResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubmitApplicationSachPrivatRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.sachprivat", name = "SubmitApplicationSachPrivatRequest")
    public JAXBElement<SubmitApplicationSachPrivatRequestType> createSubmitApplicationSachPrivatRequest(SubmitApplicationSachPrivatRequestType value) {
        return new JAXBElement<SubmitApplicationSachPrivatRequestType>(_SubmitApplicationSachPrivatRequest_QNAME, SubmitApplicationSachPrivatRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubmitApplicationSachPrivatResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.sachprivat", name = "SubmitApplicationSachPrivatResponse")
    public JAXBElement<SubmitApplicationSachPrivatResponseType> createSubmitApplicationSachPrivatResponse(SubmitApplicationSachPrivatResponseType value) {
        return new JAXBElement<SubmitApplicationSachPrivatResponseType>(_SubmitApplicationSachPrivatResponse_QNAME, SubmitApplicationSachPrivatResponseType.class, null, value);
    }

}
