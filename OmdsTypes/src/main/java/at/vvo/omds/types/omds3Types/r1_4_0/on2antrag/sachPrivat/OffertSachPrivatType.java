
package at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.sachPrivat;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.common.SpezOffertType;


/**
 * Typ der das Produkt beschreibt und in Offertanfrage und Offertantwort verwendet wird
 * 
 * <p>Java-Klasse für OffertSachPrivat_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="OffertSachPrivat_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.common}SpezOffert_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Verkaufsprodukt" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.sachprivat}VerkaufsproduktSachPrivat_Type"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OffertSachPrivat_Type", propOrder = {
    "verkaufsprodukt"
})
public class OffertSachPrivatType
    extends SpezOffertType
{

    @XmlElement(name = "Verkaufsprodukt", required = true)
    protected VerkaufsproduktSachPrivatType verkaufsprodukt;

    /**
     * Ruft den Wert der verkaufsprodukt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VerkaufsproduktSachPrivatType }
     *     
     */
    public VerkaufsproduktSachPrivatType getVerkaufsprodukt() {
        return verkaufsprodukt;
    }

    /**
     * Legt den Wert der verkaufsprodukt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VerkaufsproduktSachPrivatType }
     *     
     */
    public void setVerkaufsprodukt(VerkaufsproduktSachPrivatType value) {
        this.verkaufsprodukt = value;
    }

}
