
package at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.sachPrivat;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Typ für ein Besitz-Produkt Gebaeudeversicherung
 * 
 * <p>Java-Klasse für ProduktGebaeudeversicherung_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ProduktGebaeudeversicherung_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.sachprivat}ProduktSachPrivat_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Deckungen" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.sachprivat}ElementarproduktGebaeude_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProduktGebaeudeversicherung_Type", propOrder = {
    "deckungen"
})
public class ProduktGebaeudeversicherungType
    extends ProduktSachPrivatType
{

    @XmlElement(name = "Deckungen")
    protected List<ElementarproduktGebaeudeType> deckungen;

    /**
     * Gets the value of the deckungen property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the deckungen property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDeckungen().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ElementarproduktGebaeudeType }
     * 
     * 
     */
    public List<ElementarproduktGebaeudeType> getDeckungen() {
        if (deckungen == null) {
            deckungen = new ArrayList<ElementarproduktGebaeudeType>();
        }
        return this.deckungen;
    }

}
