
package at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.sachPrivat;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.ProduktType;


/**
 * <p>Java-Klasse für ProduktSachPrivat_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ProduktSachPrivat_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}Produkt_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="VersInteresseRefLfnr" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProduktSachPrivat_Type", propOrder = {
    "versInteresseRefLfnr"
})
@XmlSeeAlso({
    ProduktGebaeudeversicherungType.class,
    ProduktHaushaltsversicherungType.class
})
public abstract class ProduktSachPrivatType
    extends ProduktType
{

    @XmlElement(name = "VersInteresseRefLfnr", required = true)
    protected String versInteresseRefLfnr;

    /**
     * Ruft den Wert der versInteresseRefLfnr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersInteresseRefLfnr() {
        return versInteresseRefLfnr;
    }

    /**
     * Legt den Wert der versInteresseRefLfnr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersInteresseRefLfnr(String value) {
        this.versInteresseRefLfnr = value;
    }

}
