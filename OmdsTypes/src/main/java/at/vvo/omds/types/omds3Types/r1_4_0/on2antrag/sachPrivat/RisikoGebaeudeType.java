
package at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.sachPrivat;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds2Types.v2_11.ELFlaecheType;
import at.vvo.omds.types.omds2Types.v2_11.Entsch2Type;
import at.vvo.omds.types.omds3Types.r1_4_0.common.VersichertesInteresseType;


/**
 * Type zur Risikobeschreibung Gebäude
 * 
 * <p>Java-Klasse für RisikoGebaeude_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="RisikoGebaeude_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}VersichertesInteresse_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GebaeudeArtCd" type="{urn:omds20}GebaeudeArtCd_Type"/&gt;
 *         &lt;element name="GebaeudeBez" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="NutzungCd" type="{urn:omds20}NutzungCd_Type" minOccurs="0"/&gt;
 *         &lt;element name="BauartCd" type="{urn:omds20}BauartCd_Type" minOccurs="0"/&gt;
 *         &lt;element name="DachungCd" type="{urn:omds20}DachungCd_Type" minOccurs="0"/&gt;
 *         &lt;element name="GebaeudeHoeheCd" type="{urn:omds20}GebaeudeHoeheCd_Type" minOccurs="0"/&gt;
 *         &lt;element name="BaujahrGebaeude" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="EL-Flaeche" type="{urn:omds20}EL-Flaeche_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Vorschaeden" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.sachprivat}RisikoVorschaeden_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="AusstattungCd" type="{urn:omds20}AusstattungCd_Type" minOccurs="0"/&gt;
 *         &lt;element name="SichergKz" type="{urn:omds20}Entsch2_Type" minOccurs="0"/&gt;
 *         &lt;element name="PreisProQm" type="{urn:omds20}decimal"/&gt;
 *         &lt;element name="ZusaetzlicheGebaeudedaten" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.sachprivat}ZusaetzlicheGebaeudedaten_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RisikoGebaeude_Type", propOrder = {
    "gebaeudeArtCd",
    "gebaeudeBez",
    "nutzungCd",
    "bauartCd",
    "dachungCd",
    "gebaeudeHoeheCd",
    "baujahrGebaeude",
    "elFlaeche",
    "vorschaeden",
    "ausstattungCd",
    "sichergKz",
    "preisProQm",
    "zusaetzlicheGebaeudedaten"
})
public class RisikoGebaeudeType
    extends VersichertesInteresseType
{

    @XmlElement(name = "GebaeudeArtCd", required = true)
    protected String gebaeudeArtCd;
    @XmlElement(name = "GebaeudeBez", required = true)
    protected String gebaeudeBez;
    @XmlElement(name = "NutzungCd")
    protected String nutzungCd;
    @XmlElement(name = "BauartCd")
    protected String bauartCd;
    @XmlElement(name = "DachungCd")
    protected String dachungCd;
    @XmlElement(name = "GebaeudeHoeheCd")
    protected String gebaeudeHoeheCd;
    @XmlElement(name = "BaujahrGebaeude", required = true)
    protected BigInteger baujahrGebaeude;
    @XmlElement(name = "EL-Flaeche")
    protected List<ELFlaecheType> elFlaeche;
    @XmlElement(name = "Vorschaeden")
    @XmlSchemaType(name = "string")
    protected List<RisikoVorschaedenType> vorschaeden;
    @XmlElement(name = "AusstattungCd")
    protected String ausstattungCd;
    @XmlElement(name = "SichergKz")
    @XmlSchemaType(name = "string")
    protected Entsch2Type sichergKz;
    @XmlElement(name = "PreisProQm", required = true)
    protected BigDecimal preisProQm;
    @XmlElement(name = "ZusaetzlicheGebaeudedaten")
    protected List<ZusaetzlicheGebaeudedatenType> zusaetzlicheGebaeudedaten;

    /**
     * Ruft den Wert der gebaeudeArtCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGebaeudeArtCd() {
        return gebaeudeArtCd;
    }

    /**
     * Legt den Wert der gebaeudeArtCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGebaeudeArtCd(String value) {
        this.gebaeudeArtCd = value;
    }

    /**
     * Ruft den Wert der gebaeudeBez-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGebaeudeBez() {
        return gebaeudeBez;
    }

    /**
     * Legt den Wert der gebaeudeBez-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGebaeudeBez(String value) {
        this.gebaeudeBez = value;
    }

    /**
     * Ruft den Wert der nutzungCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNutzungCd() {
        return nutzungCd;
    }

    /**
     * Legt den Wert der nutzungCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNutzungCd(String value) {
        this.nutzungCd = value;
    }

    /**
     * Ruft den Wert der bauartCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBauartCd() {
        return bauartCd;
    }

    /**
     * Legt den Wert der bauartCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBauartCd(String value) {
        this.bauartCd = value;
    }

    /**
     * Ruft den Wert der dachungCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDachungCd() {
        return dachungCd;
    }

    /**
     * Legt den Wert der dachungCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDachungCd(String value) {
        this.dachungCd = value;
    }

    /**
     * Ruft den Wert der gebaeudeHoeheCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGebaeudeHoeheCd() {
        return gebaeudeHoeheCd;
    }

    /**
     * Legt den Wert der gebaeudeHoeheCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGebaeudeHoeheCd(String value) {
        this.gebaeudeHoeheCd = value;
    }

    /**
     * Ruft den Wert der baujahrGebaeude-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getBaujahrGebaeude() {
        return baujahrGebaeude;
    }

    /**
     * Legt den Wert der baujahrGebaeude-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setBaujahrGebaeude(BigInteger value) {
        this.baujahrGebaeude = value;
    }

    /**
     * Gets the value of the elFlaeche property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the elFlaeche property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getELFlaeche().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ELFlaecheType }
     * 
     * 
     */
    public List<ELFlaecheType> getELFlaeche() {
        if (elFlaeche == null) {
            elFlaeche = new ArrayList<ELFlaecheType>();
        }
        return this.elFlaeche;
    }

    /**
     * Gets the value of the vorschaeden property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the vorschaeden property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVorschaeden().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RisikoVorschaedenType }
     * 
     * 
     */
    public List<RisikoVorschaedenType> getVorschaeden() {
        if (vorschaeden == null) {
            vorschaeden = new ArrayList<RisikoVorschaedenType>();
        }
        return this.vorschaeden;
    }

    /**
     * Ruft den Wert der ausstattungCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAusstattungCd() {
        return ausstattungCd;
    }

    /**
     * Legt den Wert der ausstattungCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAusstattungCd(String value) {
        this.ausstattungCd = value;
    }

    /**
     * Ruft den Wert der sichergKz-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Entsch2Type }
     *     
     */
    public Entsch2Type getSichergKz() {
        return sichergKz;
    }

    /**
     * Legt den Wert der sichergKz-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Entsch2Type }
     *     
     */
    public void setSichergKz(Entsch2Type value) {
        this.sichergKz = value;
    }

    /**
     * Ruft den Wert der preisProQm-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPreisProQm() {
        return preisProQm;
    }

    /**
     * Legt den Wert der preisProQm-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPreisProQm(BigDecimal value) {
        this.preisProQm = value;
    }

    /**
     * Gets the value of the zusaetzlicheGebaeudedaten property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the zusaetzlicheGebaeudedaten property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getZusaetzlicheGebaeudedaten().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ZusaetzlicheGebaeudedatenType }
     * 
     * 
     */
    public List<ZusaetzlicheGebaeudedatenType> getZusaetzlicheGebaeudedaten() {
        if (zusaetzlicheGebaeudedaten == null) {
            zusaetzlicheGebaeudedaten = new ArrayList<ZusaetzlicheGebaeudedatenType>();
        }
        return this.zusaetzlicheGebaeudedaten;
    }

}
