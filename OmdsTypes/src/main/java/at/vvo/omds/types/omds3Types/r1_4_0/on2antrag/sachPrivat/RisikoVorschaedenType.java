
package at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.sachPrivat;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für RisikoVorschaeden_Type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="RisikoVorschaeden_Type"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="keine Vorschaeden"/&gt;
 *     &lt;enumeration value="ein Vorschaden"/&gt;
 *     &lt;enumeration value="mehr als ein Vorschaden"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "RisikoVorschaeden_Type")
@XmlEnum
public enum RisikoVorschaedenType {

    @XmlEnumValue("keine Vorschaeden")
    KEINE_VORSCHAEDEN("keine Vorschaeden"),
    @XmlEnumValue("ein Vorschaden")
    EIN_VORSCHADEN("ein Vorschaden"),
    @XmlEnumValue("mehr als ein Vorschaden")
    MEHR_ALS_EIN_VORSCHADEN("mehr als ein Vorschaden");
    private final String value;

    RisikoVorschaedenType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static RisikoVorschaedenType fromValue(String v) {
        for (RisikoVorschaedenType c: RisikoVorschaedenType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
