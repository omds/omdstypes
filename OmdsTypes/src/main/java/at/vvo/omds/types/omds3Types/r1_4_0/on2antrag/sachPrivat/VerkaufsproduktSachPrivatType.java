
package at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.sachPrivat;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.ProduktType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.VerkaufsproduktType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.VersichertesInteresseType;


/**
 * Typ für ein Besitz-Produktbündel, welches einem Vertrag entspricht
 * 
 * <p>Java-Klasse für VerkaufsproduktSachPrivat_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="VerkaufsproduktSachPrivat_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}Verkaufsprodukt_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Produkte" type="{urn:omds3CommonServiceTypes-1-1-0}Produkt_Type" maxOccurs="unbounded"/&gt;
 *         &lt;element name="Zusatzprodukte" type="{urn:omds3CommonServiceTypes-1-1-0}Produkt_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="VersicherteObjekte" type="{urn:omds3CommonServiceTypes-1-1-0}VersichertesInteresse_Type" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VerkaufsproduktSachPrivat_Type", propOrder = {
    "produkte",
    "zusatzprodukte",
    "versicherteObjekte"
})
public class VerkaufsproduktSachPrivatType
    extends VerkaufsproduktType
{

    @XmlElement(name = "Produkte", required = true)
    protected List<ProduktType> produkte;
    @XmlElement(name = "Zusatzprodukte")
    protected List<ProduktType> zusatzprodukte;
    @XmlElement(name = "VersicherteObjekte", required = true)
    protected List<VersichertesInteresseType> versicherteObjekte;

    /**
     * Gets the value of the produkte property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the produkte property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProdukte().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProduktType }
     * 
     * 
     */
    public List<ProduktType> getProdukte() {
        if (produkte == null) {
            produkte = new ArrayList<ProduktType>();
        }
        return this.produkte;
    }

    /**
     * Gets the value of the zusatzprodukte property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the zusatzprodukte property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getZusatzprodukte().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProduktType }
     * 
     * 
     */
    public List<ProduktType> getZusatzprodukte() {
        if (zusatzprodukte == null) {
            zusatzprodukte = new ArrayList<ProduktType>();
        }
        return this.zusatzprodukte;
    }

    /**
     * Gets the value of the versicherteObjekte property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the versicherteObjekte property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVersicherteObjekte().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link VersichertesInteresseType }
     * 
     * 
     */
    public List<VersichertesInteresseType> getVersicherteObjekte() {
        if (versicherteObjekte == null) {
            versicherteObjekte = new ArrayList<VersichertesInteresseType>();
        }
        return this.versicherteObjekte;
    }

}
