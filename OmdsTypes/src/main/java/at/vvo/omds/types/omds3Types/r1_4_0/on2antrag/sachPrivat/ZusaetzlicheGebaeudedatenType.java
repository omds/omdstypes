
package at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.sachPrivat;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Abstrakter Basistyp zur Definition zusätzlicher Gebäudedaten
 * 
 * <p>Java-Klasse für ZusaetzlicheGebaeudedaten_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ZusaetzlicheGebaeudedaten_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ZusaetzlicheGebaeudedaten_Type")
@XmlSeeAlso({
    ZusaetzlicheGebaeudedatenWintergartenType.class,
    ZusaetzlicheGebaeudedatenSolarthermieType.class,
    ZusaetzlicheGebaeudedatenSchwimmbadType.class,
    ZusaetzlicheGebaeudedatenPhotovoltaikType.class
})
public abstract class ZusaetzlicheGebaeudedatenType {


}
