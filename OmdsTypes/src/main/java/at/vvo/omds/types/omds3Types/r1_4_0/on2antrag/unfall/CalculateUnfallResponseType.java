
package at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.unfall;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.common.CalculateResponseType;


/**
 * Typ des Responseobjekts für eine Unfall-Berechnung
 * 
 * <p>Java-Klasse für CalculateUnfallResponse_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CalculateUnfallResponse_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.common}CalculateResponse_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Berechnungsantwort" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.unfall}SpezBerechnungUnfall_Type"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CalculateUnfallResponse_Type", propOrder = {
    "berechnungsantwort"
})
public class CalculateUnfallResponseType
    extends CalculateResponseType
{

    @XmlElement(name = "Berechnungsantwort", required = true)
    protected SpezBerechnungUnfallType berechnungsantwort;

    /**
     * Ruft den Wert der berechnungsantwort-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SpezBerechnungUnfallType }
     *     
     */
    public SpezBerechnungUnfallType getBerechnungsantwort() {
        return berechnungsantwort;
    }

    /**
     * Legt den Wert der berechnungsantwort-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SpezBerechnungUnfallType }
     *     
     */
    public void setBerechnungsantwort(SpezBerechnungUnfallType value) {
        this.berechnungsantwort = value;
    }

}
