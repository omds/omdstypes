
package at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.unfall;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.common.CreateOfferResponseType;


/**
 * Type des Responseobjekts für eine Erstellung eines Unfall-Offerts
 * 
 * <p>Java-Klasse für CreateOfferUnfallResponse_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CreateOfferUnfallResponse_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.common}CreateOfferResponse_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Offertantwort" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.unfall}SpezOffertUnfall_Type"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreateOfferUnfallResponse_Type", propOrder = {
    "offertantwort"
})
public class CreateOfferUnfallResponseType
    extends CreateOfferResponseType
{

    @XmlElement(name = "Offertantwort", required = true)
    protected SpezOffertUnfallType offertantwort;

    /**
     * Ruft den Wert der offertantwort-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SpezOffertUnfallType }
     *     
     */
    public SpezOffertUnfallType getOffertantwort() {
        return offertantwort;
    }

    /**
     * Legt den Wert der offertantwort-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SpezOffertUnfallType }
     *     
     */
    public void setOffertantwort(SpezOffertUnfallType value) {
        this.offertantwort = value;
    }

}
