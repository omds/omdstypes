
package at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.unfall;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.ElementarproduktType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.SelbstbehaltType;


/**
 * Typ für ein Elementarprodukt in der Sparte Unfall. Von diesem Typ werden etwaige Standard-Deckungen abgeleitet, siehe Vertragsrechtsschutz_Type. Von diesem Typ können einzelne VUs aber auch ihre eigenen Elementarprodukte ableiten, wenn sie möchten.
 * 
 * <p>Java-Klasse für ElementarproduktUnfall_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ElementarproduktUnfall_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}Elementarprodukt_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="VersPersonRefLfNr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Versicherungssumme" type="{http://www.w3.org/2001/XMLSchema}unsignedLong" minOccurs="0"/&gt;
 *         &lt;element name="Selbstbehalt" type="{urn:omds3CommonServiceTypes-1-1-0}Selbstbehalt_Type" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ElementarproduktUnfall_Type", propOrder = {
    "versPersonRefLfNr",
    "versicherungssumme",
    "selbstbehalt"
})
@XmlSeeAlso({
    GenElementarproduktUnfallType.class
})
public abstract class ElementarproduktUnfallType
    extends ElementarproduktType
{

    @XmlElement(name = "VersPersonRefLfNr")
    protected String versPersonRefLfNr;
    @XmlElement(name = "Versicherungssumme")
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger versicherungssumme;
    @XmlElement(name = "Selbstbehalt")
    protected SelbstbehaltType selbstbehalt;

    /**
     * Ruft den Wert der versPersonRefLfNr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersPersonRefLfNr() {
        return versPersonRefLfNr;
    }

    /**
     * Legt den Wert der versPersonRefLfNr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersPersonRefLfNr(String value) {
        this.versPersonRefLfNr = value;
    }

    /**
     * Ruft den Wert der versicherungssumme-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getVersicherungssumme() {
        return versicherungssumme;
    }

    /**
     * Legt den Wert der versicherungssumme-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setVersicherungssumme(BigInteger value) {
        this.versicherungssumme = value;
    }

    /**
     * Ruft den Wert der selbstbehalt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SelbstbehaltType }
     *     
     */
    public SelbstbehaltType getSelbstbehalt() {
        return selbstbehalt;
    }

    /**
     * Legt den Wert der selbstbehalt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SelbstbehaltType }
     *     
     */
    public void setSelbstbehalt(SelbstbehaltType value) {
        this.selbstbehalt = value;
    }

}
