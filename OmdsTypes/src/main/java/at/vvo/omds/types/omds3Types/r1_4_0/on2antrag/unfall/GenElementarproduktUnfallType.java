
package at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.unfall;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für GenElementarproduktUnfall_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="GenElementarproduktUnfall_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.unfall}ElementarproduktUnfall_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="LeistungsartCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GenElementarproduktUnfall_Type", propOrder = {
    "leistungsartCode"
})
public class GenElementarproduktUnfallType
    extends ElementarproduktUnfallType
{

    @XmlElement(name = "LeistungsartCode", required = true)
    protected String leistungsartCode;

    /**
     * Ruft den Wert der leistungsartCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLeistungsartCode() {
        return leistungsartCode;
    }

    /**
     * Legt den Wert der leistungsartCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLeistungsartCode(String value) {
        this.leistungsartCode = value;
    }

}
