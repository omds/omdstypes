
package at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.unfall;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.unfall package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _VerkaufsproduktUnfall_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.unfall", "VerkaufsproduktUnfall");
    private final static QName _ProduktUnfall_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.unfall", "ProduktUnfall");
    private final static QName _ElementarproduktUnfall_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.unfall", "ElementarproduktUnfall");
    private final static QName _GenElementarproduktUnfall_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.unfall", "GenElementarproduktUnfall");
    private final static QName _CalculateUnfallRequest_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.unfall", "CalculateUnfallRequest");
    private final static QName _CalculateUnfallResponse_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.unfall", "CalculateUnfallResponse");
    private final static QName _CreateOfferUnfallRequest_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.unfall", "CreateOfferUnfallRequest");
    private final static QName _CreateOfferUnfallResponse_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.unfall", "CreateOfferUnfallResponse");
    private final static QName _CreateApplicationUnfallRequest_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.unfall", "CreateApplicationUnfallRequest");
    private final static QName _CreateApplicationUnfallResponse_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.unfall", "CreateApplicationUnfallResponse");
    private final static QName _SubmitApplicationUnfallRequest_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.unfall", "SubmitApplicationUnfallRequest");
    private final static QName _SubmitApplicationUnfallResponse_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.unfall", "SubmitApplicationUnfallResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.unfall
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link VerkaufsproduktUnfallType }
     * 
     */
    public VerkaufsproduktUnfallType createVerkaufsproduktUnfallType() {
        return new VerkaufsproduktUnfallType();
    }

    /**
     * Create an instance of {@link GenElementarproduktUnfallType }
     * 
     */
    public GenElementarproduktUnfallType createGenElementarproduktUnfallType() {
        return new GenElementarproduktUnfallType();
    }

    /**
     * Create an instance of {@link CalculateUnfallRequestType }
     * 
     */
    public CalculateUnfallRequestType createCalculateUnfallRequestType() {
        return new CalculateUnfallRequestType();
    }

    /**
     * Create an instance of {@link CalculateUnfallResponseType }
     * 
     */
    public CalculateUnfallResponseType createCalculateUnfallResponseType() {
        return new CalculateUnfallResponseType();
    }

    /**
     * Create an instance of {@link CreateOfferUnfallRequestType }
     * 
     */
    public CreateOfferUnfallRequestType createCreateOfferUnfallRequestType() {
        return new CreateOfferUnfallRequestType();
    }

    /**
     * Create an instance of {@link CreateOfferUnfallResponseType }
     * 
     */
    public CreateOfferUnfallResponseType createCreateOfferUnfallResponseType() {
        return new CreateOfferUnfallResponseType();
    }

    /**
     * Create an instance of {@link CreateApplicationUnfallRequestType }
     * 
     */
    public CreateApplicationUnfallRequestType createCreateApplicationUnfallRequestType() {
        return new CreateApplicationUnfallRequestType();
    }

    /**
     * Create an instance of {@link CreateApplicationUnfallResponseType }
     * 
     */
    public CreateApplicationUnfallResponseType createCreateApplicationUnfallResponseType() {
        return new CreateApplicationUnfallResponseType();
    }

    /**
     * Create an instance of {@link SubmitApplicationUnfallResponseType }
     * 
     */
    public SubmitApplicationUnfallResponseType createSubmitApplicationUnfallResponseType() {
        return new SubmitApplicationUnfallResponseType();
    }

    /**
     * Create an instance of {@link SpezBerechnungUnfallType }
     * 
     */
    public SpezBerechnungUnfallType createSpezBerechnungUnfallType() {
        return new SpezBerechnungUnfallType();
    }

    /**
     * Create an instance of {@link SpezOffertUnfallType }
     * 
     */
    public SpezOffertUnfallType createSpezOffertUnfallType() {
        return new SpezOffertUnfallType();
    }

    /**
     * Create an instance of {@link SpezAntragUnfallType }
     * 
     */
    public SpezAntragUnfallType createSpezAntragUnfallType() {
        return new SpezAntragUnfallType();
    }

    /**
     * Create an instance of {@link SubmitApplicationUnfallRequestType }
     * 
     */
    public SubmitApplicationUnfallRequestType createSubmitApplicationUnfallRequestType() {
        return new SubmitApplicationUnfallRequestType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VerkaufsproduktUnfallType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.unfall", name = "VerkaufsproduktUnfall")
    public JAXBElement<VerkaufsproduktUnfallType> createVerkaufsproduktUnfall(VerkaufsproduktUnfallType value) {
        return new JAXBElement<VerkaufsproduktUnfallType>(_VerkaufsproduktUnfall_QNAME, VerkaufsproduktUnfallType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProduktUnfallType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.unfall", name = "ProduktUnfall")
    public JAXBElement<ProduktUnfallType> createProduktUnfall(ProduktUnfallType value) {
        return new JAXBElement<ProduktUnfallType>(_ProduktUnfall_QNAME, ProduktUnfallType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ElementarproduktUnfallType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.unfall", name = "ElementarproduktUnfall")
    public JAXBElement<ElementarproduktUnfallType> createElementarproduktUnfall(ElementarproduktUnfallType value) {
        return new JAXBElement<ElementarproduktUnfallType>(_ElementarproduktUnfall_QNAME, ElementarproduktUnfallType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GenElementarproduktUnfallType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.unfall", name = "GenElementarproduktUnfall")
    public JAXBElement<GenElementarproduktUnfallType> createGenElementarproduktUnfall(GenElementarproduktUnfallType value) {
        return new JAXBElement<GenElementarproduktUnfallType>(_GenElementarproduktUnfall_QNAME, GenElementarproduktUnfallType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CalculateUnfallRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.unfall", name = "CalculateUnfallRequest")
    public JAXBElement<CalculateUnfallRequestType> createCalculateUnfallRequest(CalculateUnfallRequestType value) {
        return new JAXBElement<CalculateUnfallRequestType>(_CalculateUnfallRequest_QNAME, CalculateUnfallRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CalculateUnfallResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.unfall", name = "CalculateUnfallResponse")
    public JAXBElement<CalculateUnfallResponseType> createCalculateUnfallResponse(CalculateUnfallResponseType value) {
        return new JAXBElement<CalculateUnfallResponseType>(_CalculateUnfallResponse_QNAME, CalculateUnfallResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateOfferUnfallRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.unfall", name = "CreateOfferUnfallRequest")
    public JAXBElement<CreateOfferUnfallRequestType> createCreateOfferUnfallRequest(CreateOfferUnfallRequestType value) {
        return new JAXBElement<CreateOfferUnfallRequestType>(_CreateOfferUnfallRequest_QNAME, CreateOfferUnfallRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateOfferUnfallResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.unfall", name = "CreateOfferUnfallResponse")
    public JAXBElement<CreateOfferUnfallResponseType> createCreateOfferUnfallResponse(CreateOfferUnfallResponseType value) {
        return new JAXBElement<CreateOfferUnfallResponseType>(_CreateOfferUnfallResponse_QNAME, CreateOfferUnfallResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateApplicationUnfallRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.unfall", name = "CreateApplicationUnfallRequest")
    public JAXBElement<CreateApplicationUnfallRequestType> createCreateApplicationUnfallRequest(CreateApplicationUnfallRequestType value) {
        return new JAXBElement<CreateApplicationUnfallRequestType>(_CreateApplicationUnfallRequest_QNAME, CreateApplicationUnfallRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateApplicationUnfallResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.unfall", name = "CreateApplicationUnfallResponse")
    public JAXBElement<CreateApplicationUnfallResponseType> createCreateApplicationUnfallResponse(CreateApplicationUnfallResponseType value) {
        return new JAXBElement<CreateApplicationUnfallResponseType>(_CreateApplicationUnfallResponse_QNAME, CreateApplicationUnfallResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubmitApplicationUnfallResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.unfall", name = "SubmitApplicationUnfallRequest")
    public JAXBElement<SubmitApplicationUnfallResponseType> createSubmitApplicationUnfallRequest(SubmitApplicationUnfallResponseType value) {
        return new JAXBElement<SubmitApplicationUnfallResponseType>(_SubmitApplicationUnfallRequest_QNAME, SubmitApplicationUnfallResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubmitApplicationUnfallResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.unfall", name = "SubmitApplicationUnfallResponse")
    public JAXBElement<SubmitApplicationUnfallResponseType> createSubmitApplicationUnfallResponse(SubmitApplicationUnfallResponseType value) {
        return new JAXBElement<SubmitApplicationUnfallResponseType>(_SubmitApplicationUnfallResponse_QNAME, SubmitApplicationUnfallResponseType.class, null, value);
    }

}
