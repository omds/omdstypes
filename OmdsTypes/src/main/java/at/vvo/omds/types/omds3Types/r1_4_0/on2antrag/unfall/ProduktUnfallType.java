
package at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.unfall;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.ProduktType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.SelbstbehaltType;


/**
 * Typ für ein Produkt in der Sparte Unfall. Von diesem Typ können einzelne VUs ihre eigenen Produkte ableiten, wenn sie möchten.
 * 
 * <p>Java-Klasse für ProduktUnfall_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ProduktUnfall_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}Produkt_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Selbstbehalt" type="{urn:omds3CommonServiceTypes-1-1-0}Selbstbehalt_Type" minOccurs="0"/&gt;
 *         &lt;element name="Leistungsarten" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.unfall}ElementarproduktUnfall_Type" maxOccurs="unbounded"/&gt;
 *         &lt;element name="VersPersonenRefLfnr" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProduktUnfall_Type", propOrder = {
    "selbstbehalt",
    "leistungsarten",
    "versPersonenRefLfnr"
})
public abstract class ProduktUnfallType
    extends ProduktType
{

    @XmlElement(name = "Selbstbehalt")
    protected SelbstbehaltType selbstbehalt;
    @XmlElement(name = "Leistungsarten", required = true)
    protected List<ElementarproduktUnfallType> leistungsarten;
    @XmlElement(name = "VersPersonenRefLfnr", required = true)
    protected List<String> versPersonenRefLfnr;

    /**
     * Ruft den Wert der selbstbehalt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SelbstbehaltType }
     *     
     */
    public SelbstbehaltType getSelbstbehalt() {
        return selbstbehalt;
    }

    /**
     * Legt den Wert der selbstbehalt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SelbstbehaltType }
     *     
     */
    public void setSelbstbehalt(SelbstbehaltType value) {
        this.selbstbehalt = value;
    }

    /**
     * Gets the value of the leistungsarten property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the leistungsarten property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLeistungsarten().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ElementarproduktUnfallType }
     * 
     * 
     */
    public List<ElementarproduktUnfallType> getLeistungsarten() {
        if (leistungsarten == null) {
            leistungsarten = new ArrayList<ElementarproduktUnfallType>();
        }
        return this.leistungsarten;
    }

    /**
     * Gets the value of the versPersonenRefLfnr property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the versPersonenRefLfnr property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVersPersonenRefLfnr().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getVersPersonenRefLfnr() {
        if (versPersonenRefLfnr == null) {
            versPersonenRefLfnr = new ArrayList<String>();
        }
        return this.versPersonenRefLfnr;
    }

}
