
package at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.unfall;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.common.SpezAntragType;


/**
 * Typ für den Schritt Antrags-Erzeugung
 * 
 * <p>Java-Klasse für SpezAntragUnfall_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SpezAntragUnfall_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.common}SpezAntrag_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Verkaufsprodukt" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.unfall}VerkaufsproduktUnfall_Type"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SpezAntragUnfall_Type", propOrder = {
    "verkaufsprodukt"
})
public class SpezAntragUnfallType
    extends SpezAntragType
{

    @XmlElement(name = "Verkaufsprodukt", required = true)
    protected VerkaufsproduktUnfallType verkaufsprodukt;

    /**
     * Ruft den Wert der verkaufsprodukt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VerkaufsproduktUnfallType }
     *     
     */
    public VerkaufsproduktUnfallType getVerkaufsprodukt() {
        return verkaufsprodukt;
    }

    /**
     * Legt den Wert der verkaufsprodukt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VerkaufsproduktUnfallType }
     *     
     */
    public void setVerkaufsprodukt(VerkaufsproduktUnfallType value) {
        this.verkaufsprodukt = value;
    }

}
