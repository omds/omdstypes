
package at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.unfall;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.common.SubmitApplicationResponseType;


/**
 * Type des Response, um den Antrag einzureichen
 * 
 * <p>Java-Klasse für SubmitApplicationUnfallResponse_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SubmitApplicationUnfallResponse_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.common}SubmitApplicationResponse_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Antragsantwort" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on2antrag.unfall}SpezAntragUnfall_Type"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubmitApplicationUnfallResponse_Type", propOrder = {
    "antragsantwort"
})
public class SubmitApplicationUnfallResponseType
    extends SubmitApplicationResponseType
{

    @XmlElement(name = "Antragsantwort", required = true)
    protected SpezAntragUnfallType antragsantwort;

    /**
     * Ruft den Wert der antragsantwort-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SpezAntragUnfallType }
     *     
     */
    public SpezAntragUnfallType getAntragsantwort() {
        return antragsantwort;
    }

    /**
     * Legt den Wert der antragsantwort-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SpezAntragUnfallType }
     *     
     */
    public void setAntragsantwort(SpezAntragUnfallType value) {
        this.antragsantwort = value;
    }

}
