
package at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.unfall;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.ProduktType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.VerkaufsproduktType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.VersichertesInteresseType;


/**
 * Typ für ein Verkaufsprodukt in der Sparte Unfall
 * 
 * <p>Java-Klasse für VerkaufsproduktUnfall_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="VerkaufsproduktUnfall_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}Verkaufsprodukt_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Produkte" type="{urn:omds3CommonServiceTypes-1-1-0}Produkt_Type" maxOccurs="unbounded"/&gt;
 *         &lt;element name="VersicherteInteressen" type="{urn:omds3CommonServiceTypes-1-1-0}VersichertesInteresse_Type" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VerkaufsproduktUnfall_Type", propOrder = {
    "produkte",
    "versicherteInteressen"
})
public class VerkaufsproduktUnfallType
    extends VerkaufsproduktType
{

    @XmlElement(name = "Produkte", required = true)
    protected List<ProduktType> produkte;
    @XmlElement(name = "VersicherteInteressen", required = true)
    protected List<VersichertesInteresseType> versicherteInteressen;

    /**
     * Gets the value of the produkte property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the produkte property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProdukte().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProduktType }
     * 
     * 
     */
    public List<ProduktType> getProdukte() {
        if (produkte == null) {
            produkte = new ArrayList<ProduktType>();
        }
        return this.produkte;
    }

    /**
     * Gets the value of the versicherteInteressen property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the versicherteInteressen property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVersicherteInteressen().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link VersichertesInteresseType }
     * 
     * 
     */
    public List<VersichertesInteresseType> getVersicherteInteressen() {
        if (versicherteInteressen == null) {
            versicherteInteressen = new ArrayList<VersichertesInteresseType>();
        }
        return this.versicherteInteressen;
    }

}
