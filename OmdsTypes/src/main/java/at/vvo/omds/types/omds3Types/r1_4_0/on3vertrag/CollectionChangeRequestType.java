
package at.vvo.omds.types.omds3Types.r1_4_0.on3vertrag;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import at.vvo.omds.types.omds3Types.r1_4_0.common.CommonRequestType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.DateianhangType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.ObjektIdType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.PersonType;


/**
 * Typ des Requestobjekts für eine Änderung von Inkassodaten
 * 
 * <p>Java-Klasse für CollectionChangeRequest_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CollectionChangeRequest_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}CommonRequest_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:omds3CommonServiceTypes-1-1-0}ObjektId"/&gt;
 *         &lt;element name="Zahlrhythmus" type="{urn:omds20}ZahlRhythmCd_Type"/&gt;
 *         &lt;element name="Inkassoadresse" type="{urn:omds3CommonServiceTypes-1-1-0}Person_Type"/&gt;
 *         &lt;element name="Zahlweg" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on3vertrag}Zahlweg_Type"/&gt;
 *         &lt;element name="Dateianhaenge" type="{urn:omds3CommonServiceTypes-1-1-0}Dateianhang_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="WirksamtkeitAb" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CollectionChangeRequest_Type", propOrder = {
    "objektId",
    "zahlrhythmus",
    "inkassoadresse",
    "zahlweg",
    "dateianhaenge",
    "wirksamtkeitAb"
})
public class CollectionChangeRequestType
    extends CommonRequestType
{

    @XmlElement(name = "ObjektId", namespace = "urn:omds3CommonServiceTypes-1-1-0", required = true)
    protected ObjektIdType objektId;
    @XmlElement(name = "Zahlrhythmus", required = true)
    protected String zahlrhythmus;
    @XmlElement(name = "Inkassoadresse", required = true)
    protected PersonType inkassoadresse;
    @XmlElement(name = "Zahlweg", required = true)
    protected ZahlwegType zahlweg;
    @XmlElement(name = "Dateianhaenge")
    protected List<DateianhangType> dateianhaenge;
    @XmlElement(name = "WirksamtkeitAb", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar wirksamtkeitAb;

    /**
     * Die Id des Vertrag als ObjektId (VertragsID des OMDS-Datensatzes)
     * 
     * @return
     *     possible object is
     *     {@link ObjektIdType }
     *     
     */
    public ObjektIdType getObjektId() {
        return objektId;
    }

    /**
     * Legt den Wert der objektId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjektIdType }
     *     
     */
    public void setObjektId(ObjektIdType value) {
        this.objektId = value;
    }

    /**
     * Ruft den Wert der zahlrhythmus-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZahlrhythmus() {
        return zahlrhythmus;
    }

    /**
     * Legt den Wert der zahlrhythmus-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZahlrhythmus(String value) {
        this.zahlrhythmus = value;
    }

    /**
     * Ruft den Wert der inkassoadresse-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PersonType }
     *     
     */
    public PersonType getInkassoadresse() {
        return inkassoadresse;
    }

    /**
     * Legt den Wert der inkassoadresse-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonType }
     *     
     */
    public void setInkassoadresse(PersonType value) {
        this.inkassoadresse = value;
    }

    /**
     * Ruft den Wert der zahlweg-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ZahlwegType }
     *     
     */
    public ZahlwegType getZahlweg() {
        return zahlweg;
    }

    /**
     * Legt den Wert der zahlweg-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ZahlwegType }
     *     
     */
    public void setZahlweg(ZahlwegType value) {
        this.zahlweg = value;
    }

    /**
     * Gets the value of the dateianhaenge property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dateianhaenge property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDateianhaenge().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DateianhangType }
     * 
     * 
     */
    public List<DateianhangType> getDateianhaenge() {
        if (dateianhaenge == null) {
            dateianhaenge = new ArrayList<DateianhangType>();
        }
        return this.dateianhaenge;
    }

    /**
     * Ruft den Wert der wirksamtkeitAb-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getWirksamtkeitAb() {
        return wirksamtkeitAb;
    }

    /**
     * Legt den Wert der wirksamtkeitAb-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setWirksamtkeitAb(XMLGregorianCalendar value) {
        this.wirksamtkeitAb = value;
    }

}
