
package at.vvo.omds.types.omds3Types.r1_4_0.on3vertrag;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds2Types.v2_11.VERTRAGType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.CommonResponseType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.ObjektIdType;


/**
 * Typ des Response für eine Änderung von Inkassodaten
 * 
 * <p>Java-Klasse für CollectionChangeResponse_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CollectionChangeResponse_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}CommonResponse_Type"&gt;
 *       &lt;sequence minOccurs="0"&gt;
 *         &lt;element ref="{urn:omds3CommonServiceTypes-1-1-0}ObjektId"/&gt;
 *         &lt;element name="Vertrag" type="{urn:omds20}VERTRAG_Type"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CollectionChangeResponse_Type", propOrder = {
    "objektId",
    "vertrag"
})
public class CollectionChangeResponseType
    extends CommonResponseType
{

    @XmlElement(name = "ObjektId", namespace = "urn:omds3CommonServiceTypes-1-1-0")
    protected ObjektIdType objektId;
    @XmlElement(name = "Vertrag")
    protected VERTRAGType vertrag;

    /**
     * ObjektId des Vertrags
     * 
     * @return
     *     possible object is
     *     {@link ObjektIdType }
     *     
     */
    public ObjektIdType getObjektId() {
        return objektId;
    }

    /**
     * Legt den Wert der objektId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjektIdType }
     *     
     */
    public void setObjektId(ObjektIdType value) {
        this.objektId = value;
    }

    /**
     * Ruft den Wert der vertrag-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VERTRAGType }
     *     
     */
    public VERTRAGType getVertrag() {
        return vertrag;
    }

    /**
     * Legt den Wert der vertrag-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VERTRAGType }
     *     
     */
    public void setVertrag(VERTRAGType value) {
        this.vertrag = value;
    }

}
