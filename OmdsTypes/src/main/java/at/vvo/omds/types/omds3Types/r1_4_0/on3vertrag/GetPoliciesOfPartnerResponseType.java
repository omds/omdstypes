
package at.vvo.omds.types.omds3Types.r1_4_0.on3vertrag;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds2Types.v2_11.VERTRAGType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.AdresseType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.CommonResponseType;


/**
 * Responsetyp zu den Polizzen, in denen ein Partner in der Rolle VN auftritt
 * 
 * <p>Java-Klasse für GetPoliciesOfPartnerResponse_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="GetPoliciesOfPartnerResponse_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}CommonResponse_Type"&gt;
 *       &lt;sequence maxOccurs="unbounded" minOccurs="0"&gt;
 *         &lt;element name="Vertrag" type="{urn:omds20}VERTRAG_Type"/&gt;
 *         &lt;element name="ObjektId" type="{http://www.w3.org/2001/XMLSchema}anyType"/&gt;
 *         &lt;element name="Zustelladresse" type="{urn:omds3CommonServiceTypes-1-1-0}Adresse_Type" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetPoliciesOfPartnerResponse_Type", propOrder = {
    "vertragAndObjektIdAndZustelladresse"
})
public class GetPoliciesOfPartnerResponseType
    extends CommonResponseType
{

    @XmlElements({
        @XmlElement(name = "Vertrag", type = VERTRAGType.class),
        @XmlElement(name = "ObjektId"),
        @XmlElement(name = "Zustelladresse", type = AdresseType.class)
    })
    protected List<Object> vertragAndObjektIdAndZustelladresse;

    /**
     * Gets the value of the vertragAndObjektIdAndZustelladresse property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the vertragAndObjektIdAndZustelladresse property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVertragAndObjektIdAndZustelladresse().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link VERTRAGType }
     * {@link Object }
     * {@link AdresseType }
     * 
     * 
     */
    public List<Object> getVertragAndObjektIdAndZustelladresse() {
        if (vertragAndObjektIdAndZustelladresse == null) {
            vertragAndObjektIdAndZustelladresse = new ArrayList<Object>();
        }
        return this.vertragAndObjektIdAndZustelladresse;
    }

}
