
package at.vvo.omds.types.omds3Types.r1_4_0.on3vertrag;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import at.vvo.omds.types.omds3Types.r1_4_0.common.AdresseType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.CommonRequestType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.ObjektIdType;


/**
 * Typ des Requestobjekts für das Setzen einer Zustelladresse
 * 
 * <p>Java-Klasse für SetMailingAddressRequest_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SetMailingAddressRequest_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}CommonRequest_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:omds3CommonServiceTypes-1-1-0}ObjektId"/&gt;
 *         &lt;element name="Zustelladresse" type="{urn:omds3CommonServiceTypes-1-1-0}Adresse_Type"/&gt;
 *         &lt;element name="WirksamtkeitAb" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SetMailingAddressRequest_Type", propOrder = {
    "objektId",
    "zustelladresse",
    "wirksamtkeitAb"
})
public class SetMailingAddressRequestType
    extends CommonRequestType
{

    @XmlElement(name = "ObjektId", namespace = "urn:omds3CommonServiceTypes-1-1-0", required = true)
    protected ObjektIdType objektId;
    @XmlElement(name = "Zustelladresse", required = true)
    protected AdresseType zustelladresse;
    @XmlElement(name = "WirksamtkeitAb", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar wirksamtkeitAb;

    /**
     * Die ObjektId des Vertrags
     * 
     * @return
     *     possible object is
     *     {@link ObjektIdType }
     *     
     */
    public ObjektIdType getObjektId() {
        return objektId;
    }

    /**
     * Legt den Wert der objektId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjektIdType }
     *     
     */
    public void setObjektId(ObjektIdType value) {
        this.objektId = value;
    }

    /**
     * Ruft den Wert der zustelladresse-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AdresseType }
     *     
     */
    public AdresseType getZustelladresse() {
        return zustelladresse;
    }

    /**
     * Legt den Wert der zustelladresse-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AdresseType }
     *     
     */
    public void setZustelladresse(AdresseType value) {
        this.zustelladresse = value;
    }

    /**
     * Ruft den Wert der wirksamtkeitAb-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getWirksamtkeitAb() {
        return wirksamtkeitAb;
    }

    /**
     * Legt den Wert der wirksamtkeitAb-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setWirksamtkeitAb(XMLGregorianCalendar value) {
        this.wirksamtkeitAb = value;
    }

}
