
package at.vvo.omds.types.omds3Types.r1_4_0.on3vertrag;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.BankverbindungType;


/**
 * <p>Java-Klasse für Zahlweg_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Zahlweg_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice&gt;
 *         &lt;element name="Zahlungsanweisung" type="{http://www.w3.org/2001/XMLSchema}anyType"/&gt;
 *         &lt;element name="BankverbindungAbbuchung" type="{urn:omds3CommonServiceTypes-1-1-0}Bankverbindung_Type"/&gt;
 *         &lt;element name="Kundenkonto"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;attribute name="Kundenkontonummer" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Kreditkarte"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;attribute name="Gesellschaft" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                 &lt;attribute name="Kartennummer" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                 &lt;attribute name="Inhaber" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                 &lt;attribute name="Pruefziffer" use="required"&gt;
 *                   &lt;simpleType&gt;
 *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt"&gt;
 *                       &lt;totalDigits value="3"/&gt;
 *                     &lt;/restriction&gt;
 *                   &lt;/simpleType&gt;
 *                 &lt;/attribute&gt;
 *                 &lt;attribute name="AblaufMonat" use="required"&gt;
 *                   &lt;simpleType&gt;
 *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte"&gt;
 *                       &lt;totalDigits value="2"/&gt;
 *                     &lt;/restriction&gt;
 *                   &lt;/simpleType&gt;
 *                 &lt;/attribute&gt;
 *                 &lt;attribute name="AblaufJahr" use="required"&gt;
 *                   &lt;simpleType&gt;
 *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte"&gt;
 *                       &lt;totalDigits value="2"/&gt;
 *                     &lt;/restriction&gt;
 *                   &lt;/simpleType&gt;
 *                 &lt;/attribute&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/choice&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Zahlweg_Type", propOrder = {
    "zahlungsanweisung",
    "bankverbindungAbbuchung",
    "kundenkonto",
    "kreditkarte"
})
public class ZahlwegType {

    @XmlElement(name = "Zahlungsanweisung")
    protected Object zahlungsanweisung;
    @XmlElement(name = "BankverbindungAbbuchung")
    protected BankverbindungType bankverbindungAbbuchung;
    @XmlElement(name = "Kundenkonto")
    protected Kundenkonto kundenkonto;
    @XmlElement(name = "Kreditkarte")
    protected Kreditkarte kreditkarte;

    /**
     * Ruft den Wert der zahlungsanweisung-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getZahlungsanweisung() {
        return zahlungsanweisung;
    }

    /**
     * Legt den Wert der zahlungsanweisung-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setZahlungsanweisung(Object value) {
        this.zahlungsanweisung = value;
    }

    /**
     * Ruft den Wert der bankverbindungAbbuchung-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BankverbindungType }
     *     
     */
    public BankverbindungType getBankverbindungAbbuchung() {
        return bankverbindungAbbuchung;
    }

    /**
     * Legt den Wert der bankverbindungAbbuchung-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BankverbindungType }
     *     
     */
    public void setBankverbindungAbbuchung(BankverbindungType value) {
        this.bankverbindungAbbuchung = value;
    }

    /**
     * Ruft den Wert der kundenkonto-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Kundenkonto }
     *     
     */
    public Kundenkonto getKundenkonto() {
        return kundenkonto;
    }

    /**
     * Legt den Wert der kundenkonto-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Kundenkonto }
     *     
     */
    public void setKundenkonto(Kundenkonto value) {
        this.kundenkonto = value;
    }

    /**
     * Ruft den Wert der kreditkarte-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Kreditkarte }
     *     
     */
    public Kreditkarte getKreditkarte() {
        return kreditkarte;
    }

    /**
     * Legt den Wert der kreditkarte-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Kreditkarte }
     *     
     */
    public void setKreditkarte(Kreditkarte value) {
        this.kreditkarte = value;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;attribute name="Gesellschaft" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *       &lt;attribute name="Kartennummer" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *       &lt;attribute name="Inhaber" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *       &lt;attribute name="Pruefziffer" use="required"&gt;
     *         &lt;simpleType&gt;
     *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt"&gt;
     *             &lt;totalDigits value="3"/&gt;
     *           &lt;/restriction&gt;
     *         &lt;/simpleType&gt;
     *       &lt;/attribute&gt;
     *       &lt;attribute name="AblaufMonat" use="required"&gt;
     *         &lt;simpleType&gt;
     *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte"&gt;
     *             &lt;totalDigits value="2"/&gt;
     *           &lt;/restriction&gt;
     *         &lt;/simpleType&gt;
     *       &lt;/attribute&gt;
     *       &lt;attribute name="AblaufJahr" use="required"&gt;
     *         &lt;simpleType&gt;
     *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte"&gt;
     *             &lt;totalDigits value="2"/&gt;
     *           &lt;/restriction&gt;
     *         &lt;/simpleType&gt;
     *       &lt;/attribute&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Kreditkarte {

        @XmlAttribute(name = "Gesellschaft", namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on3vertrag", required = true)
        protected String gesellschaft;
        @XmlAttribute(name = "Kartennummer", namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on3vertrag", required = true)
        protected String kartennummer;
        @XmlAttribute(name = "Inhaber", namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on3vertrag", required = true)
        protected String inhaber;
        @XmlAttribute(name = "Pruefziffer", namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on3vertrag", required = true)
        protected long pruefziffer;
        @XmlAttribute(name = "AblaufMonat", namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on3vertrag", required = true)
        protected short ablaufMonat;
        @XmlAttribute(name = "AblaufJahr", namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on3vertrag", required = true)
        protected short ablaufJahr;

        /**
         * Ruft den Wert der gesellschaft-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getGesellschaft() {
            return gesellschaft;
        }

        /**
         * Legt den Wert der gesellschaft-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setGesellschaft(String value) {
            this.gesellschaft = value;
        }

        /**
         * Ruft den Wert der kartennummer-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getKartennummer() {
            return kartennummer;
        }

        /**
         * Legt den Wert der kartennummer-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setKartennummer(String value) {
            this.kartennummer = value;
        }

        /**
         * Ruft den Wert der inhaber-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getInhaber() {
            return inhaber;
        }

        /**
         * Legt den Wert der inhaber-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setInhaber(String value) {
            this.inhaber = value;
        }

        /**
         * Ruft den Wert der pruefziffer-Eigenschaft ab.
         * 
         */
        public long getPruefziffer() {
            return pruefziffer;
        }

        /**
         * Legt den Wert der pruefziffer-Eigenschaft fest.
         * 
         */
        public void setPruefziffer(long value) {
            this.pruefziffer = value;
        }

        /**
         * Ruft den Wert der ablaufMonat-Eigenschaft ab.
         * 
         */
        public short getAblaufMonat() {
            return ablaufMonat;
        }

        /**
         * Legt den Wert der ablaufMonat-Eigenschaft fest.
         * 
         */
        public void setAblaufMonat(short value) {
            this.ablaufMonat = value;
        }

        /**
         * Ruft den Wert der ablaufJahr-Eigenschaft ab.
         * 
         */
        public short getAblaufJahr() {
            return ablaufJahr;
        }

        /**
         * Legt den Wert der ablaufJahr-Eigenschaft fest.
         * 
         */
        public void setAblaufJahr(short value) {
            this.ablaufJahr = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;attribute name="Kundenkontonummer" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Kundenkonto {

        @XmlAttribute(name = "Kundenkontonummer", namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on3vertrag", required = true)
        protected String kundenkontonummer;

        /**
         * Ruft den Wert der kundenkontonummer-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getKundenkontonummer() {
            return kundenkontonummer;
        }

        /**
         * Legt den Wert der kundenkontonummer-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setKundenkontonummer(String value) {
            this.kundenkontonummer = value;
        }

    }

}
