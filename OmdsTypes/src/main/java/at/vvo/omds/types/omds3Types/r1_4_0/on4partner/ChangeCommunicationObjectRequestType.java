
package at.vvo.omds.types.omds3Types.r1_4_0.on4partner;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import at.vvo.omds.types.omds2Types.v2_11.ELKommunikationType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.CommonRequestType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.DateianhangType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.ObjektIdType;


/**
 * Typ des Requestobjekts für eine Änderung einer bestehenden Kommunikationsverbindung
 * 
 * <p>Java-Klasse für ChangeCommunicationObjectRequest_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ChangeCommunicationObjectRequest_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}CommonRequest_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:omds3CommonServiceTypes-1-1-0}ObjektId"/&gt;
 *         &lt;sequence maxOccurs="unbounded"&gt;
 *           &lt;choice&gt;
 *             &lt;element ref="{urn:omds3CommonServiceTypes-1-1-0}ObjektId"/&gt;
 *             &lt;element name="BisherigeKommunikationsVerbindung" type="{urn:omds20}EL-Kommunikation_Type"/&gt;
 *           &lt;/choice&gt;
 *           &lt;element name="GeaenderteKommunikationsVerbindung" type="{urn:omds20}EL-Kommunikation_Type"/&gt;
 *         &lt;/sequence&gt;
 *         &lt;element name="WirksamtkeitAb" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *         &lt;element name="Dateianhaenge" type="{urn:omds3CommonServiceTypes-1-1-0}Dateianhang_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChangeCommunicationObjectRequest_Type", propOrder = {
    "objektId",
    "objektIdOrBisherigeKommunikationsVerbindungAndGeaenderteKommunikationsVerbindung",
    "wirksamtkeitAb",
    "dateianhaenge"
})
public class ChangeCommunicationObjectRequestType
    extends CommonRequestType
{

    @XmlElement(name = "ObjektId", namespace = "urn:omds3CommonServiceTypes-1-1-0", required = true)
    protected ObjektIdType objektId;
    @XmlElementRefs({
        @XmlElementRef(name = "ObjektId", namespace = "urn:omds3CommonServiceTypes-1-1-0", type = JAXBElement.class),
        @XmlElementRef(name = "BisherigeKommunikationsVerbindung", namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on4partner", type = JAXBElement.class),
        @XmlElementRef(name = "GeaenderteKommunikationsVerbindung", namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on4partner", type = JAXBElement.class)
    })
    protected List<JAXBElement<?>> objektIdOrBisherigeKommunikationsVerbindungAndGeaenderteKommunikationsVerbindung;
    @XmlElement(name = "WirksamtkeitAb", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar wirksamtkeitAb;
    @XmlElement(name = "Dateianhaenge")
    protected List<DateianhangType> dateianhaenge;

    /**
     * Die Personennr als ObjektId
     * 
     * @return
     *     possible object is
     *     {@link ObjektIdType }
     *     
     */
    public ObjektIdType getObjektId() {
        return objektId;
    }

    /**
     * Legt den Wert der objektId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjektIdType }
     *     
     */
    public void setObjektId(ObjektIdType value) {
        this.objektId = value;
    }

    /**
     * Gets the value of the objektIdOrBisherigeKommunikationsVerbindungAndGeaenderteKommunikationsVerbindung property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the objektIdOrBisherigeKommunikationsVerbindungAndGeaenderteKommunikationsVerbindung property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getObjektIdOrBisherigeKommunikationsVerbindungAndGeaenderteKommunikationsVerbindung().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link ELKommunikationType }{@code >}
     * {@link JAXBElement }{@code <}{@link ObjektIdType }{@code >}
     * {@link JAXBElement }{@code <}{@link ELKommunikationType }{@code >}
     * 
     * 
     */
    public List<JAXBElement<?>> getObjektIdOrBisherigeKommunikationsVerbindungAndGeaenderteKommunikationsVerbindung() {
        if (objektIdOrBisherigeKommunikationsVerbindungAndGeaenderteKommunikationsVerbindung == null) {
            objektIdOrBisherigeKommunikationsVerbindungAndGeaenderteKommunikationsVerbindung = new ArrayList<JAXBElement<?>>();
        }
        return this.objektIdOrBisherigeKommunikationsVerbindungAndGeaenderteKommunikationsVerbindung;
    }

    /**
     * Ruft den Wert der wirksamtkeitAb-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getWirksamtkeitAb() {
        return wirksamtkeitAb;
    }

    /**
     * Legt den Wert der wirksamtkeitAb-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setWirksamtkeitAb(XMLGregorianCalendar value) {
        this.wirksamtkeitAb = value;
    }

    /**
     * Gets the value of the dateianhaenge property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dateianhaenge property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDateianhaenge().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DateianhangType }
     * 
     * 
     */
    public List<DateianhangType> getDateianhaenge() {
        if (dateianhaenge == null) {
            dateianhaenge = new ArrayList<DateianhangType>();
        }
        return this.dateianhaenge;
    }

}
