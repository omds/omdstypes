
package at.vvo.omds.types.omds3Types.r1_4_0.on4partner;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.CommonResponseType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.PersonType;


/**
 * Typ des Response für eine Änderung, Löschung oder Neuanlage einer Kommunikationsverbindung
 * 
 * <p>Java-Klasse für ChangeCommunicationObjectResponse_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ChangeCommunicationObjectResponse_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}CommonResponse_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Person" type="{urn:omds3CommonServiceTypes-1-1-0}Person_Type" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChangeCommunicationObjectResponse_Type", propOrder = {
    "person"
})
@XmlSeeAlso({
    ChangeCommunicationObjectResponse.class
})
public class ChangeCommunicationObjectResponseType
    extends CommonResponseType
{

    @XmlElement(name = "Person")
    protected PersonType person;

    /**
     * Ruft den Wert der person-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PersonType }
     *     
     */
    public PersonType getPerson() {
        return person;
    }

    /**
     * Legt den Wert der person-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonType }
     *     
     */
    public void setPerson(PersonType value) {
        this.person = value;
    }

}
