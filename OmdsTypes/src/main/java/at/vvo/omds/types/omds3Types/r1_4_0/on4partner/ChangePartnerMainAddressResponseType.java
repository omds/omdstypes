
package at.vvo.omds.types.omds3Types.r1_4_0.on4partner;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds2Types.v2_11.VERTRAGType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.AdresseType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.CommonResponseType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.PersonType;


/**
 * Typ des Response für eine Änderung, Löschung oder Neuanlage einer Adresse
 * 
 * <p>Java-Klasse für ChangePartnerMainAddressResponse_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ChangePartnerMainAddressResponse_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}CommonResponse_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:omds3CommonServiceTypes-1-1-0}Adresse" minOccurs="0"/&gt;
 *         &lt;element name="VertraegeGeaendert" type="{urn:omds20}VERTRAG_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="VertraegeMitUnveraendertenRisikoadressen" type="{urn:omds20}VERTRAG_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="WeiterePersonenAnAdresse" type="{urn:omds3CommonServiceTypes-1-1-0}Person_Type" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChangePartnerMainAddressResponse_Type", propOrder = {
    "adresse",
    "vertraegeGeaendert",
    "vertraegeMitUnveraendertenRisikoadressen",
    "weiterePersonenAnAdresse"
})
public class ChangePartnerMainAddressResponseType
    extends CommonResponseType
{

    @XmlElement(name = "Adresse", namespace = "urn:omds3CommonServiceTypes-1-1-0")
    protected AdresseType adresse;
    @XmlElement(name = "VertraegeGeaendert")
    protected List<VERTRAGType> vertraegeGeaendert;
    @XmlElement(name = "VertraegeMitUnveraendertenRisikoadressen")
    protected List<VERTRAGType> vertraegeMitUnveraendertenRisikoadressen;
    @XmlElement(name = "WeiterePersonenAnAdresse")
    protected PersonType weiterePersonenAnAdresse;

    /**
     * Die neue Adresse
     * 
     * @return
     *     possible object is
     *     {@link AdresseType }
     *     
     */
    public AdresseType getAdresse() {
        return adresse;
    }

    /**
     * Legt den Wert der adresse-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AdresseType }
     *     
     */
    public void setAdresse(AdresseType value) {
        this.adresse = value;
    }

    /**
     * Gets the value of the vertraegeGeaendert property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the vertraegeGeaendert property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVertraegeGeaendert().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link VERTRAGType }
     * 
     * 
     */
    public List<VERTRAGType> getVertraegeGeaendert() {
        if (vertraegeGeaendert == null) {
            vertraegeGeaendert = new ArrayList<VERTRAGType>();
        }
        return this.vertraegeGeaendert;
    }

    /**
     * Gets the value of the vertraegeMitUnveraendertenRisikoadressen property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the vertraegeMitUnveraendertenRisikoadressen property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVertraegeMitUnveraendertenRisikoadressen().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link VERTRAGType }
     * 
     * 
     */
    public List<VERTRAGType> getVertraegeMitUnveraendertenRisikoadressen() {
        if (vertraegeMitUnveraendertenRisikoadressen == null) {
            vertraegeMitUnveraendertenRisikoadressen = new ArrayList<VERTRAGType>();
        }
        return this.vertraegeMitUnveraendertenRisikoadressen;
    }

    /**
     * Ruft den Wert der weiterePersonenAnAdresse-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PersonType }
     *     
     */
    public PersonType getWeiterePersonenAnAdresse() {
        return weiterePersonenAnAdresse;
    }

    /**
     * Legt den Wert der weiterePersonenAnAdresse-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonType }
     *     
     */
    public void setWeiterePersonenAnAdresse(PersonType value) {
        this.weiterePersonenAnAdresse = value;
    }

}
