
package at.vvo.omds.types.omds3Types.r1_4_0.on4partner;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import at.vvo.omds.types.omds2Types.v2_11.NATUERLICHEPERSONType;
import at.vvo.omds.types.omds2Types.v2_11.SONSTIGEPERSONType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.CommonRequestType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.DateianhangType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.ObjektIdType;


/**
 * Typ des Requestobjekts für eine Änderung der allgemeinen Personendaten
 * 
 * <p>Java-Klasse für ChangePersonDataRequest_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ChangePersonDataRequest_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}CommonRequest_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:omds3CommonServiceTypes-1-1-0}ObjektId"/&gt;
 *         &lt;element name="Hinweistext" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;choice&gt;
 *           &lt;element ref="{urn:omds20}NATUERLICHE_PERSON"/&gt;
 *           &lt;element ref="{urn:omds20}SONSTIGE_PERSON"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="WirksamtkeitAb" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *         &lt;element name="Dateianhaenge" type="{urn:omds3CommonServiceTypes-1-1-0}Dateianhang_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChangePersonDataRequest_Type", propOrder = {
    "objektId",
    "hinweistext",
    "natuerlicheperson",
    "sonstigeperson",
    "wirksamtkeitAb",
    "dateianhaenge"
})
public class ChangePersonDataRequestType
    extends CommonRequestType
{

    @XmlElement(name = "ObjektId", namespace = "urn:omds3CommonServiceTypes-1-1-0", required = true)
    protected ObjektIdType objektId;
    @XmlElement(name = "Hinweistext")
    protected String hinweistext;
    @XmlElement(name = "NATUERLICHE_PERSON", namespace = "urn:omds20")
    protected NATUERLICHEPERSONType natuerlicheperson;
    @XmlElement(name = "SONSTIGE_PERSON", namespace = "urn:omds20")
    protected SONSTIGEPERSONType sonstigeperson;
    @XmlElement(name = "WirksamtkeitAb", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar wirksamtkeitAb;
    @XmlElement(name = "Dateianhaenge")
    protected List<DateianhangType> dateianhaenge;

    /**
     * Personennr als ObjektId
     * 
     * @return
     *     possible object is
     *     {@link ObjektIdType }
     *     
     */
    public ObjektIdType getObjektId() {
        return objektId;
    }

    /**
     * Legt den Wert der objektId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjektIdType }
     *     
     */
    public void setObjektId(ObjektIdType value) {
        this.objektId = value;
    }

    /**
     * Ruft den Wert der hinweistext-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHinweistext() {
        return hinweistext;
    }

    /**
     * Legt den Wert der hinweistext-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHinweistext(String value) {
        this.hinweistext = value;
    }

    /**
     * Ruft den Wert der natuerlicheperson-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link NATUERLICHEPERSONType }
     *     
     */
    public NATUERLICHEPERSONType getNATUERLICHEPERSON() {
        return natuerlicheperson;
    }

    /**
     * Legt den Wert der natuerlicheperson-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link NATUERLICHEPERSONType }
     *     
     */
    public void setNATUERLICHEPERSON(NATUERLICHEPERSONType value) {
        this.natuerlicheperson = value;
    }

    /**
     * Ruft den Wert der sonstigeperson-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SONSTIGEPERSONType }
     *     
     */
    public SONSTIGEPERSONType getSONSTIGEPERSON() {
        return sonstigeperson;
    }

    /**
     * Legt den Wert der sonstigeperson-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SONSTIGEPERSONType }
     *     
     */
    public void setSONSTIGEPERSON(SONSTIGEPERSONType value) {
        this.sonstigeperson = value;
    }

    /**
     * Ruft den Wert der wirksamtkeitAb-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getWirksamtkeitAb() {
        return wirksamtkeitAb;
    }

    /**
     * Legt den Wert der wirksamtkeitAb-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setWirksamtkeitAb(XMLGregorianCalendar value) {
        this.wirksamtkeitAb = value;
    }

    /**
     * Gets the value of the dateianhaenge property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dateianhaenge property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDateianhaenge().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DateianhangType }
     * 
     * 
     */
    public List<DateianhangType> getDateianhaenge() {
        if (dateianhaenge == null) {
            dateianhaenge = new ArrayList<DateianhangType>();
        }
        return this.dateianhaenge;
    }

}
