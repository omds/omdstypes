
package at.vvo.omds.types.omds3Types.r1_4_0.on4partner;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.CommonResponseType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.ObjektIdType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.PersonType;


/**
 * Typ des Response für eine Änderung der allgemeinen Personendaten
 * 
 * <p>Java-Klasse für ChangePersonDataResponse_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ChangePersonDataResponse_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}CommonResponse_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Person" type="{urn:omds3CommonServiceTypes-1-1-0}Person_Type"/&gt;
 *         &lt;element name="BetroffeneObjekte" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Art" type="{http://www.w3.org/2001/XMLSchema}anyType"/&gt;
 *                   &lt;element ref="{urn:omds3CommonServiceTypes-1-1-0}ObjektId"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChangePersonDataResponse_Type", propOrder = {
    "person",
    "betroffeneObjekte"
})
public class ChangePersonDataResponseType
    extends CommonResponseType
{

    @XmlElement(name = "Person", required = true)
    protected PersonType person;
    @XmlElement(name = "BetroffeneObjekte")
    protected List<BetroffeneObjekte> betroffeneObjekte;

    /**
     * Ruft den Wert der person-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PersonType }
     *     
     */
    public PersonType getPerson() {
        return person;
    }

    /**
     * Legt den Wert der person-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonType }
     *     
     */
    public void setPerson(PersonType value) {
        this.person = value;
    }

    /**
     * Gets the value of the betroffeneObjekte property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the betroffeneObjekte property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBetroffeneObjekte().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BetroffeneObjekte }
     * 
     * 
     */
    public List<BetroffeneObjekte> getBetroffeneObjekte() {
        if (betroffeneObjekte == null) {
            betroffeneObjekte = new ArrayList<BetroffeneObjekte>();
        }
        return this.betroffeneObjekte;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Art" type="{http://www.w3.org/2001/XMLSchema}anyType"/&gt;
     *         &lt;element ref="{urn:omds3CommonServiceTypes-1-1-0}ObjektId"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "art",
        "objektId"
    })
    public static class BetroffeneObjekte {

        @XmlElement(name = "Art", required = true)
        protected Object art;
        @XmlElement(name = "ObjektId", namespace = "urn:omds3CommonServiceTypes-1-1-0", required = true)
        protected ObjektIdType objektId;

        /**
         * Ruft den Wert der art-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getArt() {
            return art;
        }

        /**
         * Legt den Wert der art-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setArt(Object value) {
            this.art = value;
        }

        /**
         * Ruft den Wert der objektId-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link ObjektIdType }
         *     
         */
        public ObjektIdType getObjektId() {
            return objektId;
        }

        /**
         * Legt den Wert der objektId-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link ObjektIdType }
         *     
         */
        public void setObjektId(ObjektIdType value) {
            this.objektId = value;
        }

    }

}
