
package at.vvo.omds.types.omds3Types.r1_4_0.on4partner;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import at.vvo.omds.types.omds2Types.v2_11.ELKommunikationType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.CommonRequestType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.ObjektIdType;


/**
 * Typ des Requestobjekts für die Löschung einer Kommunikationsverbindung
 * 
 * <p>Java-Klasse für DeleteCommunicationObjectRequest_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="DeleteCommunicationObjectRequest_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}CommonRequest_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:omds3CommonServiceTypes-1-1-0}ObjektId"/&gt;
 *         &lt;choice maxOccurs="unbounded"&gt;
 *           &lt;element ref="{urn:omds3CommonServiceTypes-1-1-0}ObjektId"/&gt;
 *           &lt;element name="KommunikationsVerbindung" type="{urn:omds20}EL-Kommunikation_Type"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="WirksamtkeitAb" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DeleteCommunicationObjectRequest_Type", propOrder = {
    "objektId",
    "objektIdOrKommunikationsVerbindung",
    "wirksamtkeitAb"
})
public class DeleteCommunicationObjectRequestType
    extends CommonRequestType
{

    @XmlElement(name = "ObjektId", namespace = "urn:omds3CommonServiceTypes-1-1-0", required = true)
    protected ObjektIdType objektId;
    @XmlElements({
        @XmlElement(name = "ObjektId", namespace = "urn:omds3CommonServiceTypes-1-1-0", type = ObjektIdType.class),
        @XmlElement(name = "KommunikationsVerbindung", type = ELKommunikationType.class)
    })
    protected List<Object> objektIdOrKommunikationsVerbindung;
    @XmlElement(name = "WirksamtkeitAb", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar wirksamtkeitAb;

    /**
     * Die Personennr als ObjektId
     * 
     * @return
     *     possible object is
     *     {@link ObjektIdType }
     *     
     */
    public ObjektIdType getObjektId() {
        return objektId;
    }

    /**
     * Legt den Wert der objektId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjektIdType }
     *     
     */
    public void setObjektId(ObjektIdType value) {
        this.objektId = value;
    }

    /**
     * Gets the value of the objektIdOrKommunikationsVerbindung property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the objektIdOrKommunikationsVerbindung property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getObjektIdOrKommunikationsVerbindung().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ObjektIdType }
     * {@link ELKommunikationType }
     * 
     * 
     */
    public List<Object> getObjektIdOrKommunikationsVerbindung() {
        if (objektIdOrKommunikationsVerbindung == null) {
            objektIdOrKommunikationsVerbindung = new ArrayList<Object>();
        }
        return this.objektIdOrKommunikationsVerbindung;
    }

    /**
     * Ruft den Wert der wirksamtkeitAb-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getWirksamtkeitAb() {
        return wirksamtkeitAb;
    }

    /**
     * Legt den Wert der wirksamtkeitAb-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setWirksamtkeitAb(XMLGregorianCalendar value) {
        this.wirksamtkeitAb = value;
    }

}
