
package at.vvo.omds.types.omds3Types.r1_4_0.on7schaden;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für BearbStandSchaden_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="BearbStandSchaden_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="BearbStandCd" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}BearbStandCd_Type"/&gt;
 *         &lt;element name="Schadennr" type="{urn:omds20}Schadennr"/&gt;
 *         &lt;element name="SachbearbVU" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}SachbearbVUType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BearbStandSchaden_Type", propOrder = {
    "bearbStandCd",
    "schadennr",
    "sachbearbVU"
})
public class BearbStandSchadenType {

    @XmlElement(name = "BearbStandCd", required = true)
    protected String bearbStandCd;
    @XmlElement(name = "Schadennr", required = true)
    protected String schadennr;
    @XmlElement(name = "SachbearbVU")
    protected SachbearbVUType sachbearbVU;

    /**
     * Ruft den Wert der bearbStandCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBearbStandCd() {
        return bearbStandCd;
    }

    /**
     * Legt den Wert der bearbStandCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBearbStandCd(String value) {
        this.bearbStandCd = value;
    }

    /**
     * Ruft den Wert der schadennr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSchadennr() {
        return schadennr;
    }

    /**
     * Legt den Wert der schadennr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSchadennr(String value) {
        this.schadennr = value;
    }

    /**
     * Ruft den Wert der sachbearbVU-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SachbearbVUType }
     *     
     */
    public SachbearbVUType getSachbearbVU() {
        return sachbearbVU;
    }

    /**
     * Legt den Wert der sachbearbVU-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SachbearbVUType }
     *     
     */
    public void setSachbearbVU(SachbearbVUType value) {
        this.sachbearbVU = value;
    }

}
