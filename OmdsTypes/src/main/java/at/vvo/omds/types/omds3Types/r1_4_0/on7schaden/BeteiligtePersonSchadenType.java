
package at.vvo.omds.types.omds3Types.r1_4_0.on7schaden;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds2Types.v2_11.PERSONType;


/**
 * Eine an einem Schaden beteiligte Person
 * 
 * <p>Java-Klasse für BeteiligtePersonSchaden_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="BeteiligtePersonSchaden_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Person" type="{urn:omds20}PERSON_Type"/&gt;
 *         &lt;element name="GeschInteresseLfnr" type="{http://www.w3.org/2001/XMLSchema}anyType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="BetLfnr" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="BetRolleCd" type="{urn:omds20}BetRolleCd_Type" /&gt;
 *       &lt;attribute name="BetTxt"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="100"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BeteiligtePersonSchaden_Type", propOrder = {
    "person",
    "geschInteresseLfnr"
})
public class BeteiligtePersonSchadenType {

    @XmlElement(name = "Person", required = true)
    protected PERSONType person;
    @XmlElement(name = "GeschInteresseLfnr")
    protected List<Object> geschInteresseLfnr;
    @XmlAttribute(name = "BetLfnr", namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", required = true)
    @XmlSchemaType(name = "anySimpleType")
    protected String betLfnr;
    @XmlAttribute(name = "BetRolleCd", namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden")
    protected String betRolleCd;
    @XmlAttribute(name = "BetTxt", namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden")
    protected String betTxt;

    /**
     * Ruft den Wert der person-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PERSONType }
     *     
     */
    public PERSONType getPerson() {
        return person;
    }

    /**
     * Legt den Wert der person-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PERSONType }
     *     
     */
    public void setPerson(PERSONType value) {
        this.person = value;
    }

    /**
     * Gets the value of the geschInteresseLfnr property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the geschInteresseLfnr property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGeschInteresseLfnr().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Object }
     * 
     * 
     */
    public List<Object> getGeschInteresseLfnr() {
        if (geschInteresseLfnr == null) {
            geschInteresseLfnr = new ArrayList<Object>();
        }
        return this.geschInteresseLfnr;
    }

    /**
     * Ruft den Wert der betLfnr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBetLfnr() {
        return betLfnr;
    }

    /**
     * Legt den Wert der betLfnr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBetLfnr(String value) {
        this.betLfnr = value;
    }

    /**
     * Ruft den Wert der betRolleCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBetRolleCd() {
        return betRolleCd;
    }

    /**
     * Legt den Wert der betRolleCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBetRolleCd(String value) {
        this.betRolleCd = value;
    }

    /**
     * Ruft den Wert der betTxt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBetTxt() {
        return betTxt;
    }

    /**
     * Legt den Wert der betTxt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBetTxt(String value) {
        this.betTxt = value;
    }

}
