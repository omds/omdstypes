
package at.vvo.omds.types.omds3Types.r1_4_0.on7schaden;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.ServiceFault;


/**
 * Typ für Response mit einer Liste von geänderten Schäden für einen bestimmten Zeitraum
 * 
 * <p>Java-Klasse für ChangedClaimsListResponse_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ChangedClaimsListResponse_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice&gt;
 *         &lt;element name="Result" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}ChangedClaimsListResponseResult_Type"/&gt;
 *         &lt;element name="ServiceFault" type="{urn:omds3CommonServiceTypes-1-1-0}ServiceFault" maxOccurs="unbounded"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChangedClaimsListResponse_Type", propOrder = {
    "result",
    "serviceFault"
})
public class ChangedClaimsListResponseType {

    @XmlElement(name = "Result")
    protected ChangedClaimsListResponseResultType result;
    @XmlElement(name = "ServiceFault")
    protected List<ServiceFault> serviceFault;

    /**
     * Ruft den Wert der result-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ChangedClaimsListResponseResultType }
     *     
     */
    public ChangedClaimsListResponseResultType getResult() {
        return result;
    }

    /**
     * Legt den Wert der result-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ChangedClaimsListResponseResultType }
     *     
     */
    public void setResult(ChangedClaimsListResponseResultType value) {
        this.result = value;
    }

    /**
     * Gets the value of the serviceFault property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the serviceFault property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getServiceFault().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ServiceFault }
     * 
     * 
     */
    public List<ServiceFault> getServiceFault() {
        if (serviceFault == null) {
            serviceFault = new ArrayList<ServiceFault>();
        }
        return this.serviceFault;
    }

}
