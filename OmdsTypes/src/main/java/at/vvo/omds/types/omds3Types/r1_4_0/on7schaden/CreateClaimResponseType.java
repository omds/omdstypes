
package at.vvo.omds.types.omds3Types.r1_4_0.on7schaden;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import at.vvo.omds.types.omds3Types.r1_4_0.common.CommonResponseType;


/**
 * Anworttyp beim Erzeugen einer Schadenmeldung
 * 
 * <p>Java-Klasse für CreateClaimResponse_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CreateClaimResponse_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}CommonResponse_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="MeldungsZusammenfassung" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}Meldungszusammenfassung_Type" minOccurs="0"/&gt;
 *         &lt;element name="Meldedat" type="{urn:omds20}Datum-Zeit"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreateClaimResponse_Type", propOrder = {
    "meldungsZusammenfassung",
    "meldedat"
})
public class CreateClaimResponseType
    extends CommonResponseType
{

    @XmlElement(name = "MeldungsZusammenfassung")
    protected MeldungszusammenfassungType meldungsZusammenfassung;
    @XmlElement(name = "Meldedat", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar meldedat;

    /**
     * Ruft den Wert der meldungsZusammenfassung-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MeldungszusammenfassungType }
     *     
     */
    public MeldungszusammenfassungType getMeldungsZusammenfassung() {
        return meldungsZusammenfassung;
    }

    /**
     * Legt den Wert der meldungsZusammenfassung-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MeldungszusammenfassungType }
     *     
     */
    public void setMeldungsZusammenfassung(MeldungszusammenfassungType value) {
        this.meldungsZusammenfassung = value;
    }

    /**
     * Ruft den Wert der meldedat-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getMeldedat() {
        return meldedat;
    }

    /**
     * Legt den Wert der meldedat-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setMeldedat(XMLGregorianCalendar value) {
        this.meldedat = value;
    }

}
