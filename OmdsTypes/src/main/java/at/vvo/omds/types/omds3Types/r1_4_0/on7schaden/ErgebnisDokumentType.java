
package at.vvo.omds.types.omds3Types.r1_4_0.on7schaden;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.DokumentenReferenzType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.ServiceFault;


/**
 * <p>Java-Klasse für ErgebnisDokument_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ErgebnisDokument_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Lfnr" type="{http://www.w3.org/2001/XMLSchema}unsignedShort"/&gt;
 *         &lt;choice&gt;
 *           &lt;element name="Dokument" type="{urn:omds3CommonServiceTypes-1-1-0}DokumentenReferenz_Type"/&gt;
 *           &lt;element name="FehlerDokumentenanlage" type="{urn:omds3CommonServiceTypes-1-1-0}ServiceFault"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ErgebnisDokument_Type", propOrder = {
    "lfnr",
    "dokument",
    "fehlerDokumentenanlage"
})
public class ErgebnisDokumentType {

    @XmlElement(name = "Lfnr")
    @XmlSchemaType(name = "unsignedShort")
    protected int lfnr;
    @XmlElement(name = "Dokument")
    protected DokumentenReferenzType dokument;
    @XmlElement(name = "FehlerDokumentenanlage")
    protected ServiceFault fehlerDokumentenanlage;

    /**
     * Ruft den Wert der lfnr-Eigenschaft ab.
     * 
     */
    public int getLfnr() {
        return lfnr;
    }

    /**
     * Legt den Wert der lfnr-Eigenschaft fest.
     * 
     */
    public void setLfnr(int value) {
        this.lfnr = value;
    }

    /**
     * Ruft den Wert der dokument-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DokumentenReferenzType }
     *     
     */
    public DokumentenReferenzType getDokument() {
        return dokument;
    }

    /**
     * Legt den Wert der dokument-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DokumentenReferenzType }
     *     
     */
    public void setDokument(DokumentenReferenzType value) {
        this.dokument = value;
    }

    /**
     * Ruft den Wert der fehlerDokumentenanlage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceFault }
     *     
     */
    public ServiceFault getFehlerDokumentenanlage() {
        return fehlerDokumentenanlage;
    }

    /**
     * Legt den Wert der fehlerDokumentenanlage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceFault }
     *     
     */
    public void setFehlerDokumentenanlage(ServiceFault value) {
        this.fehlerDokumentenanlage = value;
    }

}
