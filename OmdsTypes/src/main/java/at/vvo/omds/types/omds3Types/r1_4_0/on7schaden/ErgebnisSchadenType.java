
package at.vvo.omds.types.omds3Types.r1_4_0.on7schaden;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.ServiceFault;


/**
 * <p>Java-Klasse für ErgebnisSchaden_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ErgebnisSchaden_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Lfnr" type="{http://www.w3.org/2001/XMLSchema}unsignedShort"/&gt;
 *         &lt;choice&gt;
 *           &lt;element name="Schadenanlage" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}Schadenanlage_Type"/&gt;
 *           &lt;element name="FehlerSchadenanlage" type="{urn:omds3CommonServiceTypes-1-1-0}ServiceFault"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ErgebnisSchaden_Type", propOrder = {
    "lfnr",
    "schadenanlage",
    "fehlerSchadenanlage"
})
public class ErgebnisSchadenType {

    @XmlElement(name = "Lfnr")
    @XmlSchemaType(name = "unsignedShort")
    protected int lfnr;
    @XmlElement(name = "Schadenanlage")
    protected SchadenanlageType schadenanlage;
    @XmlElement(name = "FehlerSchadenanlage")
    protected ServiceFault fehlerSchadenanlage;

    /**
     * Ruft den Wert der lfnr-Eigenschaft ab.
     * 
     */
    public int getLfnr() {
        return lfnr;
    }

    /**
     * Legt den Wert der lfnr-Eigenschaft fest.
     * 
     */
    public void setLfnr(int value) {
        this.lfnr = value;
    }

    /**
     * Ruft den Wert der schadenanlage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SchadenanlageType }
     *     
     */
    public SchadenanlageType getSchadenanlage() {
        return schadenanlage;
    }

    /**
     * Legt den Wert der schadenanlage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SchadenanlageType }
     *     
     */
    public void setSchadenanlage(SchadenanlageType value) {
        this.schadenanlage = value;
    }

    /**
     * Ruft den Wert der fehlerSchadenanlage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceFault }
     *     
     */
    public ServiceFault getFehlerSchadenanlage() {
        return fehlerSchadenanlage;
    }

    /**
     * Legt den Wert der fehlerSchadenanlage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceFault }
     *     
     */
    public void setFehlerSchadenanlage(ServiceFault value) {
        this.fehlerSchadenanlage = value;
    }

}
