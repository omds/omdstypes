
package at.vvo.omds.types.omds3Types.r1_4_0.on7schaden;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * Geokoordinaten im Dezimalsystem
 * 
 * <p>Java-Klasse für Geokoordinaten_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Geokoordinaten_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="Breite" type="{http://www.w3.org/2001/XMLSchema}double" /&gt;
 *       &lt;attribute name="Laenge" type="{http://www.w3.org/2001/XMLSchema}double" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Geokoordinaten_Type")
public class GeokoordinatenType {

    @XmlAttribute(name = "Breite", namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden")
    protected Double breite;
    @XmlAttribute(name = "Laenge", namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden")
    protected Double laenge;

    /**
     * Ruft den Wert der breite-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getBreite() {
        return breite;
    }

    /**
     * Legt den Wert der breite-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setBreite(Double value) {
        this.breite = value;
    }

    /**
     * Ruft den Wert der laenge-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getLaenge() {
        return laenge;
    }

    /**
     * Legt den Wert der laenge-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setLaenge(Double value) {
        this.laenge = value;
    }

}
