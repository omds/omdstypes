
package at.vvo.omds.types.omds3Types.r1_4_0.on7schaden;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Erweiterung des geschädigten Interesses zu einem geschädigten Kfz
 * 
 * <p>Java-Klasse für GeschaedigtesObjektKfz_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="GeschaedigtesObjektKfz_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}GeschaedigtesInteresse_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="VUNrGesch" type="{urn:omds20}VUNr" minOccurs="0"/&gt;
 *         &lt;element name="VUNameGesch" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="PolNrGesch" type="{urn:omds20}Polizzennr"/&gt;
 *         &lt;element name="SchadennrGesch" type="{urn:omds20}Schadennr" minOccurs="0"/&gt;
 *         &lt;element name="LandesCd_GeschKfz" type="{urn:omds20}LandesCd_Type" minOccurs="0"/&gt;
 *         &lt;element name="Kennz_GeschKfz"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;minLength value="1"/&gt;
 *               &lt;maxLength value="12"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Marke" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="40"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Handelsbez" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;minLength value="1"/&gt;
 *               &lt;maxLength value="30"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Fahrgestnr" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="20"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="TypVarVer" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;minLength value="1"/&gt;
 *               &lt;maxLength value="20"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GeschaedigtesObjektKfz_Type", propOrder = {
    "vuNrGesch",
    "vuNameGesch",
    "polNrGesch",
    "schadennrGesch",
    "landesCdGeschKfz",
    "kennzGeschKfz",
    "marke",
    "handelsbez",
    "fahrgestnr",
    "typVarVer"
})
public class GeschaedigtesObjektKfzType
    extends GeschaedigtesInteresseType
{

    @XmlElement(name = "VUNrGesch")
    protected String vuNrGesch;
    @XmlElement(name = "VUNameGesch", required = true)
    protected String vuNameGesch;
    @XmlElement(name = "PolNrGesch", required = true)
    protected String polNrGesch;
    @XmlElement(name = "SchadennrGesch")
    protected String schadennrGesch;
    @XmlElement(name = "LandesCd_GeschKfz")
    protected String landesCdGeschKfz;
    @XmlElement(name = "Kennz_GeschKfz", required = true)
    protected String kennzGeschKfz;
    @XmlElement(name = "Marke")
    protected String marke;
    @XmlElement(name = "Handelsbez")
    protected String handelsbez;
    @XmlElement(name = "Fahrgestnr")
    protected String fahrgestnr;
    @XmlElement(name = "TypVarVer")
    protected String typVarVer;

    /**
     * Ruft den Wert der vuNrGesch-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVUNrGesch() {
        return vuNrGesch;
    }

    /**
     * Legt den Wert der vuNrGesch-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVUNrGesch(String value) {
        this.vuNrGesch = value;
    }

    /**
     * Ruft den Wert der vuNameGesch-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVUNameGesch() {
        return vuNameGesch;
    }

    /**
     * Legt den Wert der vuNameGesch-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVUNameGesch(String value) {
        this.vuNameGesch = value;
    }

    /**
     * Ruft den Wert der polNrGesch-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolNrGesch() {
        return polNrGesch;
    }

    /**
     * Legt den Wert der polNrGesch-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolNrGesch(String value) {
        this.polNrGesch = value;
    }

    /**
     * Ruft den Wert der schadennrGesch-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSchadennrGesch() {
        return schadennrGesch;
    }

    /**
     * Legt den Wert der schadennrGesch-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSchadennrGesch(String value) {
        this.schadennrGesch = value;
    }

    /**
     * Ruft den Wert der landesCdGeschKfz-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLandesCdGeschKfz() {
        return landesCdGeschKfz;
    }

    /**
     * Legt den Wert der landesCdGeschKfz-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLandesCdGeschKfz(String value) {
        this.landesCdGeschKfz = value;
    }

    /**
     * Ruft den Wert der kennzGeschKfz-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKennzGeschKfz() {
        return kennzGeschKfz;
    }

    /**
     * Legt den Wert der kennzGeschKfz-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKennzGeschKfz(String value) {
        this.kennzGeschKfz = value;
    }

    /**
     * Ruft den Wert der marke-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMarke() {
        return marke;
    }

    /**
     * Legt den Wert der marke-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMarke(String value) {
        this.marke = value;
    }

    /**
     * Ruft den Wert der handelsbez-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHandelsbez() {
        return handelsbez;
    }

    /**
     * Legt den Wert der handelsbez-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHandelsbez(String value) {
        this.handelsbez = value;
    }

    /**
     * Ruft den Wert der fahrgestnr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFahrgestnr() {
        return fahrgestnr;
    }

    /**
     * Legt den Wert der fahrgestnr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFahrgestnr(String value) {
        this.fahrgestnr = value;
    }

    /**
     * Ruft den Wert der typVarVer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTypVarVer() {
        return typVarVer;
    }

    /**
     * Legt den Wert der typVarVer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTypVarVer(String value) {
        this.typVarVer = value;
    }

}
