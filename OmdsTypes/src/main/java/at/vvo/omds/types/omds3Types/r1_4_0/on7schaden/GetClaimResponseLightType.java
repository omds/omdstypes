
package at.vvo.omds.types.omds3Types.r1_4_0.on7schaden;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.CommonResponseType;


/**
 * Leichtgewichtiges Response-Objekt für Schadenereignisse
 * 
 * <p>Java-Klasse für GetClaimResponseLight_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="GetClaimResponseLight_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}CommonResponse_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Schadenereignis" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}SchadenereignisLight_Type"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetClaimResponseLight_Type", propOrder = {
    "schadenereignis"
})
public class GetClaimResponseLightType
    extends CommonResponseType
{

    @XmlElement(name = "Schadenereignis", required = true)
    protected SchadenereignisLightType schadenereignis;

    /**
     * Ruft den Wert der schadenereignis-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SchadenereignisLightType }
     *     
     */
    public SchadenereignisLightType getSchadenereignis() {
        return schadenereignis;
    }

    /**
     * Legt den Wert der schadenereignis-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SchadenereignisLightType }
     *     
     */
    public void setSchadenereignis(SchadenereignisLightType value) {
        this.schadenereignis = value;
    }

}
