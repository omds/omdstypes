
package at.vvo.omds.types.omds3Types.r1_4_0.on7schaden;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import at.vvo.omds.types.omds3Types.r1_4_0.common.BankverbindungType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.CommonRequestType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.ObjektIdType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.UploadDokumentType;


/**
 * Typ für die Durchführung einer einfachen Schadenmeldung
 * 
 * <p>Java-Klasse für InitiateClaimRequest_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="InitiateClaimRequest_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}CommonRequest_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Geschaeftsfallnummer" type="{urn:omds3CommonServiceTypes-1-1-0}ObjektId_Type" minOccurs="0"/&gt;
 *         &lt;element name="Polizzennr" type="{urn:omds20}Polizzennr" minOccurs="0"/&gt;
 *         &lt;element name="VertragsID" type="{urn:omds20}VertragsID" minOccurs="0"/&gt;
 *         &lt;element name="Ereigniszpkt" type="{urn:omds20}Datum-Zeit"/&gt;
 *         &lt;element name="EreignisbeschrTxt" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="SchadOrt" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}Ort_Type"/&gt;
 *         &lt;element name="BeteiligtePersonen" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}BeteiligtePerson_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Rollen" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}ReferenzAufBeteiligtePersonSchaden_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Dokumente" type="{urn:omds3CommonServiceTypes-1-1-0}Upload_Dokument_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Schadenmelder" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}Schadenmelder_Type"/&gt;
 *         &lt;element name="Bankverbindung" type="{urn:omds3CommonServiceTypes-1-1-0}Bankverbindung_Type"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InitiateClaimRequest_Type", propOrder = {
    "geschaeftsfallnummer",
    "polizzennr",
    "vertragsID",
    "ereigniszpkt",
    "ereignisbeschrTxt",
    "schadOrt",
    "beteiligtePersonen",
    "rollen",
    "dokumente",
    "schadenmelder",
    "bankverbindung"
})
public class InitiateClaimRequestType
    extends CommonRequestType
{

    @XmlElement(name = "Geschaeftsfallnummer")
    protected ObjektIdType geschaeftsfallnummer;
    @XmlElement(name = "Polizzennr")
    protected String polizzennr;
    @XmlElement(name = "VertragsID")
    protected String vertragsID;
    @XmlElement(name = "Ereigniszpkt", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar ereigniszpkt;
    @XmlElement(name = "EreignisbeschrTxt", required = true)
    protected String ereignisbeschrTxt;
    @XmlElement(name = "SchadOrt", required = true)
    protected OrtType schadOrt;
    @XmlElement(name = "BeteiligtePersonen")
    protected List<BeteiligtePersonType> beteiligtePersonen;
    @XmlElement(name = "Rollen")
    protected List<ReferenzAufBeteiligtePersonSchadenType> rollen;
    @XmlElement(name = "Dokumente")
    protected List<UploadDokumentType> dokumente;
    @XmlElement(name = "Schadenmelder", required = true)
    protected SchadenmelderType schadenmelder;
    @XmlElement(name = "Bankverbindung", required = true)
    protected BankverbindungType bankverbindung;

    /**
     * Ruft den Wert der geschaeftsfallnummer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ObjektIdType }
     *     
     */
    public ObjektIdType getGeschaeftsfallnummer() {
        return geschaeftsfallnummer;
    }

    /**
     * Legt den Wert der geschaeftsfallnummer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjektIdType }
     *     
     */
    public void setGeschaeftsfallnummer(ObjektIdType value) {
        this.geschaeftsfallnummer = value;
    }

    /**
     * Ruft den Wert der polizzennr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolizzennr() {
        return polizzennr;
    }

    /**
     * Legt den Wert der polizzennr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolizzennr(String value) {
        this.polizzennr = value;
    }

    /**
     * Ruft den Wert der vertragsID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVertragsID() {
        return vertragsID;
    }

    /**
     * Legt den Wert der vertragsID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVertragsID(String value) {
        this.vertragsID = value;
    }

    /**
     * Ruft den Wert der ereigniszpkt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEreigniszpkt() {
        return ereigniszpkt;
    }

    /**
     * Legt den Wert der ereigniszpkt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEreigniszpkt(XMLGregorianCalendar value) {
        this.ereigniszpkt = value;
    }

    /**
     * Ruft den Wert der ereignisbeschrTxt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEreignisbeschrTxt() {
        return ereignisbeschrTxt;
    }

    /**
     * Legt den Wert der ereignisbeschrTxt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEreignisbeschrTxt(String value) {
        this.ereignisbeschrTxt = value;
    }

    /**
     * Ruft den Wert der schadOrt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OrtType }
     *     
     */
    public OrtType getSchadOrt() {
        return schadOrt;
    }

    /**
     * Legt den Wert der schadOrt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OrtType }
     *     
     */
    public void setSchadOrt(OrtType value) {
        this.schadOrt = value;
    }

    /**
     * Gets the value of the beteiligtePersonen property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the beteiligtePersonen property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBeteiligtePersonen().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BeteiligtePersonType }
     * 
     * 
     */
    public List<BeteiligtePersonType> getBeteiligtePersonen() {
        if (beteiligtePersonen == null) {
            beteiligtePersonen = new ArrayList<BeteiligtePersonType>();
        }
        return this.beteiligtePersonen;
    }

    /**
     * Gets the value of the rollen property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rollen property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRollen().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReferenzAufBeteiligtePersonSchadenType }
     * 
     * 
     */
    public List<ReferenzAufBeteiligtePersonSchadenType> getRollen() {
        if (rollen == null) {
            rollen = new ArrayList<ReferenzAufBeteiligtePersonSchadenType>();
        }
        return this.rollen;
    }

    /**
     * Gets the value of the dokumente property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dokumente property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDokumente().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link UploadDokumentType }
     * 
     * 
     */
    public List<UploadDokumentType> getDokumente() {
        if (dokumente == null) {
            dokumente = new ArrayList<UploadDokumentType>();
        }
        return this.dokumente;
    }

    /**
     * Ruft den Wert der schadenmelder-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SchadenmelderType }
     *     
     */
    public SchadenmelderType getSchadenmelder() {
        return schadenmelder;
    }

    /**
     * Legt den Wert der schadenmelder-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SchadenmelderType }
     *     
     */
    public void setSchadenmelder(SchadenmelderType value) {
        this.schadenmelder = value;
    }

    /**
     * Ruft den Wert der bankverbindung-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BankverbindungType }
     *     
     */
    public BankverbindungType getBankverbindung() {
        return bankverbindung;
    }

    /**
     * Legt den Wert der bankverbindung-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BankverbindungType }
     *     
     */
    public void setBankverbindung(BankverbindungType value) {
        this.bankverbindung = value;
    }

}
