
package at.vvo.omds.types.omds3Types.r1_4_0.on7schaden;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import at.vvo.omds.types.omds3Types.r1_4_0.common.CommonResponseType;


/**
 * Anworttyp beim Erzeugen einer einfachen Schadenmeldung
 * 
 * <p>Java-Klasse für InitiateClaimResponse_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="InitiateClaimResponse_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}CommonResponse_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="MeldungsZusammenfassung" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}MeldungszusammenfassungInitiateClaim_Type" minOccurs="0"/&gt;
 *         &lt;element name="Meldedat" type="{urn:omds20}Datum-Zeit"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InitiateClaimResponse_Type", propOrder = {
    "meldungsZusammenfassung",
    "meldedat"
})
public class InitiateClaimResponseType
    extends CommonResponseType
{

    @XmlElement(name = "MeldungsZusammenfassung")
    protected MeldungszusammenfassungInitiateClaimType meldungsZusammenfassung;
    @XmlElement(name = "Meldedat", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar meldedat;

    /**
     * Ruft den Wert der meldungsZusammenfassung-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MeldungszusammenfassungInitiateClaimType }
     *     
     */
    public MeldungszusammenfassungInitiateClaimType getMeldungsZusammenfassung() {
        return meldungsZusammenfassung;
    }

    /**
     * Legt den Wert der meldungsZusammenfassung-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MeldungszusammenfassungInitiateClaimType }
     *     
     */
    public void setMeldungsZusammenfassung(MeldungszusammenfassungInitiateClaimType value) {
        this.meldungsZusammenfassung = value;
    }

    /**
     * Ruft den Wert der meldedat-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getMeldedat() {
        return meldedat;
    }

    /**
     * Legt den Wert der meldedat-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setMeldedat(XMLGregorianCalendar value) {
        this.meldedat = value;
    }

}
