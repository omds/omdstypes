
package at.vvo.omds.types.omds3Types.r1_4_0.on7schaden;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds2Types.v2_11.VtgRolleCdType;
import at.vvo.omds.types.omds2Types.v2_11.WaehrungsCdType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.BankverbindungType;


/**
 * Die Meldung eines Schadens (Unterobjekt eines Schadenereignisses)
 * 
 * <p>Java-Klasse für MeldungSchaden_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="MeldungSchaden_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}Schadenzuordnung"/&gt;
 *         &lt;element name="Polizzennr" type="{urn:omds20}Polizzennr"/&gt;
 *         &lt;element name="VertragsID" type="{urn:omds20}VertragsID" minOccurs="0"/&gt;
 *         &lt;element name="SchadenTxt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="BeteiligtePersonen" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}ReferenzAufBeteiligtePersonSchaden_Type"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;choice&gt;
 *                     &lt;element name="Vertragsrolle" type="{urn:omds20}VtgRolleCd_Type"/&gt;
 *                     &lt;sequence&gt;
 *                       &lt;element name="Schadensrolle" type="{urn:omds20}BetRolleCd_Type"/&gt;
 *                       &lt;element name="GeschInteresseLfnr" type="{http://www.w3.org/2001/XMLSchema}unsignedShort" minOccurs="0"/&gt;
 *                     &lt;/sequence&gt;
 *                   &lt;/choice&gt;
 *                   &lt;element name="ZusaetzlicheRollendaten" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}ZusaetzlicheRollendaten_Type" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/extension&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="LeistungGeschaetzt" type="{urn:omds20}decimal" minOccurs="0"/&gt;
 *         &lt;element name="WaehrungsCd" type="{urn:omds20}WaehrungsCd_Type" minOccurs="0"/&gt;
 *         &lt;element name="Spartendetails" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}SpartendetailSchaden_Type" minOccurs="0"/&gt;
 *         &lt;element name="Bankverbindung" type="{urn:omds3CommonServiceTypes-1-1-0}Bankverbindung_Type" minOccurs="0"/&gt;
 *         &lt;element name="ZusaetzlicheSchadensdaten" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}ZusaetzlicheSchadensdaten_Type" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MeldungSchaden_Type", propOrder = {
    "schadenzuordnung",
    "polizzennr",
    "vertragsID",
    "schadenTxt",
    "beteiligtePersonen",
    "leistungGeschaetzt",
    "waehrungsCd",
    "spartendetails",
    "bankverbindung",
    "zusaetzlicheSchadensdaten"
})
@XmlSeeAlso({
    SchadenType.class
})
public class MeldungSchadenType {

    @XmlElement(name = "Schadenzuordnung", required = true)
    protected SchadenzuordnungType schadenzuordnung;
    @XmlElement(name = "Polizzennr", required = true)
    protected String polizzennr;
    @XmlElement(name = "VertragsID")
    protected String vertragsID;
    @XmlElement(name = "SchadenTxt")
    protected String schadenTxt;
    @XmlElement(name = "BeteiligtePersonen")
    protected List<BeteiligtePersonen> beteiligtePersonen;
    @XmlElement(name = "LeistungGeschaetzt")
    protected BigDecimal leistungGeschaetzt;
    @XmlElement(name = "WaehrungsCd")
    @XmlSchemaType(name = "string")
    protected WaehrungsCdType waehrungsCd;
    @XmlElement(name = "Spartendetails")
    protected SpartendetailSchadenType spartendetails;
    @XmlElement(name = "Bankverbindung")
    protected BankverbindungType bankverbindung;
    @XmlElement(name = "ZusaetzlicheSchadensdaten")
    protected ZusaetzlicheSchadensdatenType zusaetzlicheSchadensdaten;

    /**
     * Ruft den Wert der schadenzuordnung-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SchadenzuordnungType }
     *     
     */
    public SchadenzuordnungType getSchadenzuordnung() {
        return schadenzuordnung;
    }

    /**
     * Legt den Wert der schadenzuordnung-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SchadenzuordnungType }
     *     
     */
    public void setSchadenzuordnung(SchadenzuordnungType value) {
        this.schadenzuordnung = value;
    }

    /**
     * Ruft den Wert der polizzennr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolizzennr() {
        return polizzennr;
    }

    /**
     * Legt den Wert der polizzennr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolizzennr(String value) {
        this.polizzennr = value;
    }

    /**
     * Ruft den Wert der vertragsID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVertragsID() {
        return vertragsID;
    }

    /**
     * Legt den Wert der vertragsID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVertragsID(String value) {
        this.vertragsID = value;
    }

    /**
     * Ruft den Wert der schadenTxt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSchadenTxt() {
        return schadenTxt;
    }

    /**
     * Legt den Wert der schadenTxt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSchadenTxt(String value) {
        this.schadenTxt = value;
    }

    /**
     * Gets the value of the beteiligtePersonen property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the beteiligtePersonen property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBeteiligtePersonen().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BeteiligtePersonen }
     * 
     * 
     */
    public List<BeteiligtePersonen> getBeteiligtePersonen() {
        if (beteiligtePersonen == null) {
            beteiligtePersonen = new ArrayList<BeteiligtePersonen>();
        }
        return this.beteiligtePersonen;
    }

    /**
     * Ruft den Wert der leistungGeschaetzt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getLeistungGeschaetzt() {
        return leistungGeschaetzt;
    }

    /**
     * Legt den Wert der leistungGeschaetzt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setLeistungGeschaetzt(BigDecimal value) {
        this.leistungGeschaetzt = value;
    }

    /**
     * Ruft den Wert der waehrungsCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link WaehrungsCdType }
     *     
     */
    public WaehrungsCdType getWaehrungsCd() {
        return waehrungsCd;
    }

    /**
     * Legt den Wert der waehrungsCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link WaehrungsCdType }
     *     
     */
    public void setWaehrungsCd(WaehrungsCdType value) {
        this.waehrungsCd = value;
    }

    /**
     * Ruft den Wert der spartendetails-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SpartendetailSchadenType }
     *     
     */
    public SpartendetailSchadenType getSpartendetails() {
        return spartendetails;
    }

    /**
     * Legt den Wert der spartendetails-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SpartendetailSchadenType }
     *     
     */
    public void setSpartendetails(SpartendetailSchadenType value) {
        this.spartendetails = value;
    }

    /**
     * Ruft den Wert der bankverbindung-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BankverbindungType }
     *     
     */
    public BankverbindungType getBankverbindung() {
        return bankverbindung;
    }

    /**
     * Legt den Wert der bankverbindung-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BankverbindungType }
     *     
     */
    public void setBankverbindung(BankverbindungType value) {
        this.bankverbindung = value;
    }

    /**
     * Ruft den Wert der zusaetzlicheSchadensdaten-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ZusaetzlicheSchadensdatenType }
     *     
     */
    public ZusaetzlicheSchadensdatenType getZusaetzlicheSchadensdaten() {
        return zusaetzlicheSchadensdaten;
    }

    /**
     * Legt den Wert der zusaetzlicheSchadensdaten-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ZusaetzlicheSchadensdatenType }
     *     
     */
    public void setZusaetzlicheSchadensdaten(ZusaetzlicheSchadensdatenType value) {
        this.zusaetzlicheSchadensdaten = value;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}ReferenzAufBeteiligtePersonSchaden_Type"&gt;
     *       &lt;sequence&gt;
     *         &lt;choice&gt;
     *           &lt;element name="Vertragsrolle" type="{urn:omds20}VtgRolleCd_Type"/&gt;
     *           &lt;sequence&gt;
     *             &lt;element name="Schadensrolle" type="{urn:omds20}BetRolleCd_Type"/&gt;
     *             &lt;element name="GeschInteresseLfnr" type="{http://www.w3.org/2001/XMLSchema}unsignedShort" minOccurs="0"/&gt;
     *           &lt;/sequence&gt;
     *         &lt;/choice&gt;
     *         &lt;element name="ZusaetzlicheRollendaten" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}ZusaetzlicheRollendaten_Type" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/extension&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "vertragsrolle",
        "schadensrolle",
        "geschInteresseLfnr",
        "zusaetzlicheRollendaten"
    })
    public static class BeteiligtePersonen
        extends ReferenzAufBeteiligtePersonSchadenType
    {

        @XmlElement(name = "Vertragsrolle")
        @XmlSchemaType(name = "string")
        protected VtgRolleCdType vertragsrolle;
        @XmlElement(name = "Schadensrolle")
        protected String schadensrolle;
        @XmlElement(name = "GeschInteresseLfnr")
        @XmlSchemaType(name = "unsignedShort")
        protected Integer geschInteresseLfnr;
        @XmlElement(name = "ZusaetzlicheRollendaten")
        protected ZusaetzlicheRollendatenType zusaetzlicheRollendaten;

        /**
         * Ruft den Wert der vertragsrolle-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link VtgRolleCdType }
         *     
         */
        public VtgRolleCdType getVertragsrolle() {
            return vertragsrolle;
        }

        /**
         * Legt den Wert der vertragsrolle-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link VtgRolleCdType }
         *     
         */
        public void setVertragsrolle(VtgRolleCdType value) {
            this.vertragsrolle = value;
        }

        /**
         * Ruft den Wert der schadensrolle-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSchadensrolle() {
            return schadensrolle;
        }

        /**
         * Legt den Wert der schadensrolle-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSchadensrolle(String value) {
            this.schadensrolle = value;
        }

        /**
         * Ruft den Wert der geschInteresseLfnr-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getGeschInteresseLfnr() {
            return geschInteresseLfnr;
        }

        /**
         * Legt den Wert der geschInteresseLfnr-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setGeschInteresseLfnr(Integer value) {
            this.geschInteresseLfnr = value;
        }

        /**
         * Ruft den Wert der zusaetzlicheRollendaten-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link ZusaetzlicheRollendatenType }
         *     
         */
        public ZusaetzlicheRollendatenType getZusaetzlicheRollendaten() {
            return zusaetzlicheRollendaten;
        }

        /**
         * Legt den Wert der zusaetzlicheRollendaten-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link ZusaetzlicheRollendatenType }
         *     
         */
        public void setZusaetzlicheRollendaten(ZusaetzlicheRollendatenType value) {
            this.zusaetzlicheRollendaten = value;
        }

    }

}
