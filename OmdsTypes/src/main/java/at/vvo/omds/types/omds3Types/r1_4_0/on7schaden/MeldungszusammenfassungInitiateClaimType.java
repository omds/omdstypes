
package at.vvo.omds.types.omds3Types.r1_4_0.on7schaden;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.ObjektIdType;


/**
 * Objekt, welches GeschäftsfallId und Schadennummer, Dokumenten-Ids sowie den Bearbeitungsstand enthält
 * 
 * <p>Java-Klasse für MeldungszusammenfassungInitiateClaim_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="MeldungszusammenfassungInitiateClaim_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}IdGeschaeftsfallSchadenereignis"/&gt;
 *         &lt;element name="VUNr" type="{urn:omds20}VUNr"/&gt;
 *         &lt;element ref="{urn:omds3CommonServiceTypes-1-1-0}OrdnungsbegriffZuordFremd" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ErgebnisDokumente" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}ErgebnisDokumentAnlage_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Schaeden" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}BearbStandSchaden_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MeldungszusammenfassungInitiateClaim_Type", propOrder = {
    "idGeschaeftsfallSchadenereignis",
    "vuNr",
    "ordnungsbegriffZuordFremd",
    "ergebnisDokumente",
    "schaeden"
})
public class MeldungszusammenfassungInitiateClaimType {

    @XmlElement(name = "IdGeschaeftsfallSchadenereignis", required = true)
    protected ObjektIdType idGeschaeftsfallSchadenereignis;
    @XmlElement(name = "VUNr", required = true)
    protected String vuNr;
    @XmlElement(name = "OrdnungsbegriffZuordFremd", namespace = "urn:omds3CommonServiceTypes-1-1-0")
    protected List<String> ordnungsbegriffZuordFremd;
    @XmlElement(name = "ErgebnisDokumente")
    protected List<ErgebnisDokumentAnlageType> ergebnisDokumente;
    @XmlElement(name = "Schaeden")
    protected List<BearbStandSchadenType> schaeden;

    /**
     * Ruft den Wert der idGeschaeftsfallSchadenereignis-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ObjektIdType }
     *     
     */
    public ObjektIdType getIdGeschaeftsfallSchadenereignis() {
        return idGeschaeftsfallSchadenereignis;
    }

    /**
     * Legt den Wert der idGeschaeftsfallSchadenereignis-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjektIdType }
     *     
     */
    public void setIdGeschaeftsfallSchadenereignis(ObjektIdType value) {
        this.idGeschaeftsfallSchadenereignis = value;
    }

    /**
     * Ruft den Wert der vuNr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVUNr() {
        return vuNr;
    }

    /**
     * Legt den Wert der vuNr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVUNr(String value) {
        this.vuNr = value;
    }

    /**
     * Ordnungsbegriff des Schadenmelders auf Ebene des Schadenereignis Gets the value of the ordnungsbegriffZuordFremd property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ordnungsbegriffZuordFremd property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOrdnungsbegriffZuordFremd().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getOrdnungsbegriffZuordFremd() {
        if (ordnungsbegriffZuordFremd == null) {
            ordnungsbegriffZuordFremd = new ArrayList<String>();
        }
        return this.ordnungsbegriffZuordFremd;
    }

    /**
     * Gets the value of the ergebnisDokumente property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ergebnisDokumente property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getErgebnisDokumente().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ErgebnisDokumentAnlageType }
     * 
     * 
     */
    public List<ErgebnisDokumentAnlageType> getErgebnisDokumente() {
        if (ergebnisDokumente == null) {
            ergebnisDokumente = new ArrayList<ErgebnisDokumentAnlageType>();
        }
        return this.ergebnisDokumente;
    }

    /**
     * Gets the value of the schaeden property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the schaeden property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSchaeden().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BearbStandSchadenType }
     * 
     * 
     */
    public List<BearbStandSchadenType> getSchaeden() {
        if (schaeden == null) {
            schaeden = new ArrayList<BearbStandSchadenType>();
        }
        return this.schaeden;
    }

}
