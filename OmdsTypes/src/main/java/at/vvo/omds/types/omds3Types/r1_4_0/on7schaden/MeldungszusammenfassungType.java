
package at.vvo.omds.types.omds3Types.r1_4_0.on7schaden;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.ObjektIdType;


/**
 * Objekt, welches GeschäftsfallId und Schadennummer, Dokumenten-Ids sowie den Bearbeitungsstand enthält
 * 
 * <p>Java-Klasse für Meldungszusammenfassung_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Meldungszusammenfassung_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="VUNr" type="{urn:omds20}VUNr"/&gt;
 *         &lt;element ref="{urn:omds3CommonServiceTypes-1-1-0}Geschaeftsfallnummer"/&gt;
 *         &lt;element name="ErgebnisDokumente" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}ErgebnisDokument_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ErgebnisSchaeden" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}ErgebnisSchaden_Type" maxOccurs="unbounded"/&gt;
 *         &lt;element name="DeepLink" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Meldungszusammenfassung_Type", propOrder = {
    "vuNr",
    "geschaeftsfallnummer",
    "ergebnisDokumente",
    "ergebnisSchaeden",
    "deepLink"
})
public class MeldungszusammenfassungType {

    @XmlElement(name = "VUNr", required = true)
    protected String vuNr;
    @XmlElement(name = "Geschaeftsfallnummer", namespace = "urn:omds3CommonServiceTypes-1-1-0", required = true)
    protected ObjektIdType geschaeftsfallnummer;
    @XmlElement(name = "ErgebnisDokumente")
    protected List<ErgebnisDokumentType> ergebnisDokumente;
    @XmlElement(name = "ErgebnisSchaeden", required = true)
    protected List<ErgebnisSchadenType> ergebnisSchaeden;
    @XmlElement(name = "DeepLink")
    protected String deepLink;

    /**
     * Ruft den Wert der vuNr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVUNr() {
        return vuNr;
    }

    /**
     * Legt den Wert der vuNr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVUNr(String value) {
        this.vuNr = value;
    }

    /**
     * Geschäftsfallnummer der Anlage des Schadenereignis-Objektes
     * 
     * @return
     *     possible object is
     *     {@link ObjektIdType }
     *     
     */
    public ObjektIdType getGeschaeftsfallnummer() {
        return geschaeftsfallnummer;
    }

    /**
     * Legt den Wert der geschaeftsfallnummer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjektIdType }
     *     
     */
    public void setGeschaeftsfallnummer(ObjektIdType value) {
        this.geschaeftsfallnummer = value;
    }

    /**
     * Gets the value of the ergebnisDokumente property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ergebnisDokumente property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getErgebnisDokumente().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ErgebnisDokumentType }
     * 
     * 
     */
    public List<ErgebnisDokumentType> getErgebnisDokumente() {
        if (ergebnisDokumente == null) {
            ergebnisDokumente = new ArrayList<ErgebnisDokumentType>();
        }
        return this.ergebnisDokumente;
    }

    /**
     * Gets the value of the ergebnisSchaeden property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ergebnisSchaeden property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getErgebnisSchaeden().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ErgebnisSchadenType }
     * 
     * 
     */
    public List<ErgebnisSchadenType> getErgebnisSchaeden() {
        if (ergebnisSchaeden == null) {
            ergebnisSchaeden = new ArrayList<ErgebnisSchadenType>();
        }
        return this.ergebnisSchaeden;
    }

    /**
     * Ruft den Wert der deepLink-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeepLink() {
        return deepLink;
    }

    /**
     * Legt den Wert der deepLink-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeepLink(String value) {
        this.deepLink = value;
    }

}
