
package at.vvo.omds.types.omds3Types.r1_4_0.on7schaden;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für NatPerson_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="NatPerson_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="Familienname" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="Vorname" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="GeschlechtCd" use="required" type="{urn:omds20}GeschlechtCd_Type" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NatPerson_Type")
public class NatPersonType {

    @XmlAttribute(name = "Familienname", namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", required = true)
    @XmlSchemaType(name = "anySimpleType")
    protected String familienname;
    @XmlAttribute(name = "Vorname", namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden")
    @XmlSchemaType(name = "anySimpleType")
    protected String vorname;
    @XmlAttribute(name = "GeschlechtCd", namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", required = true)
    protected String geschlechtCd;

    /**
     * Ruft den Wert der familienname-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFamilienname() {
        return familienname;
    }

    /**
     * Legt den Wert der familienname-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFamilienname(String value) {
        this.familienname = value;
    }

    /**
     * Ruft den Wert der vorname-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVorname() {
        return vorname;
    }

    /**
     * Legt den Wert der vorname-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVorname(String value) {
        this.vorname = value;
    }

    /**
     * Ruft den Wert der geschlechtCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGeschlechtCd() {
        return geschlechtCd;
    }

    /**
     * Legt den Wert der geschlechtCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGeschlechtCd(String value) {
        this.geschlechtCd = value;
    }

}
