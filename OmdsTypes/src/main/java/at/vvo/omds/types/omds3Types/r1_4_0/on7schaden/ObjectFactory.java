
package at.vvo.omds.types.omds3Types.r1_4_0.on7schaden;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import at.vvo.omds.types.omds3Types.r1_4_0.common.ObjektIdType;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the at.vvo.omds.types.omds3Types.r1_4_0.on7schaden package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CreateClaimRequest_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", "CreateClaimRequest");
    private final static QName _CreateClaimResponse_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", "CreateClaimResponse");
    private final static QName _SubmitClaimRequest_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", "SubmitClaimRequest");
    private final static QName _SubmitClaimResponse_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", "SubmitClaimResponse");
    private final static QName _InitiateClaimRequest_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", "InitiateClaimRequest");
    private final static QName _InitiateClaimResponse_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", "InitiateClaimResponse");
    private final static QName _GetClaimRequest_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", "GetClaimRequest");
    private final static QName _GetClaimResponse_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", "GetClaimResponse");
    private final static QName _GetClaimLightRequest_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", "GetClaimLightRequest");
    private final static QName _GetClaimLightResponse_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", "GetClaimLightResponse");
    private final static QName _SearchClaimRequest_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", "SearchClaimRequest");
    private final static QName _SearchClaimResponse_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", "SearchClaimResponse");
    private final static QName _Schadenzuordnung_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", "Schadenzuordnung");
    private final static QName _ChangedClaimsListRequest_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", "ChangedClaimsListRequest");
    private final static QName _ChangedClaimsListResponse_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", "ChangedClaimsListResponse");
    private final static QName _LossEventListRequest_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", "LossEventListRequest");
    private final static QName _LossEventListResponse_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", "LossEventListResponse");
    private final static QName _IdGeschaeftsfallSchadenereignis_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", "IdGeschaeftsfallSchadenereignis");
    private final static QName _IdGeschaeftsfallSchadenanlage_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", "IdGeschaeftsfallSchadenanlage");
    private final static QName _DeclareNewClaimStatusRequest_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", "DeclareNewClaimStatusRequest");
    private final static QName _DeclareNewClaimStatusResponse_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", "DeclareNewClaimStatusResponse");
    private final static QName _LossEventRegisteredRequest_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", "LossEventRegisteredRequest");
    private final static QName _LossEventRegisteredResponse_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", "LossEventRegisteredResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: at.vvo.omds.types.omds3Types.r1_4_0.on7schaden
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SpartendetailSchadenKrankenType }
     * 
     */
    public SpartendetailSchadenKrankenType createSpartendetailSchadenKrankenType() {
        return new SpartendetailSchadenKrankenType();
    }

    /**
     * Create an instance of {@link MeldungSchadenType }
     * 
     */
    public MeldungSchadenType createMeldungSchadenType() {
        return new MeldungSchadenType();
    }

    /**
     * Create an instance of {@link CreateClaimRequestType }
     * 
     */
    public CreateClaimRequestType createCreateClaimRequestType() {
        return new CreateClaimRequestType();
    }

    /**
     * Create an instance of {@link CreateClaimResponseType }
     * 
     */
    public CreateClaimResponseType createCreateClaimResponseType() {
        return new CreateClaimResponseType();
    }

    /**
     * Create an instance of {@link SubmitClaimRequestType }
     * 
     */
    public SubmitClaimRequestType createSubmitClaimRequestType() {
        return new SubmitClaimRequestType();
    }

    /**
     * Create an instance of {@link SubmitClaimResponseType }
     * 
     */
    public SubmitClaimResponseType createSubmitClaimResponseType() {
        return new SubmitClaimResponseType();
    }

    /**
     * Create an instance of {@link InitiateClaimRequestType }
     * 
     */
    public InitiateClaimRequestType createInitiateClaimRequestType() {
        return new InitiateClaimRequestType();
    }

    /**
     * Create an instance of {@link InitiateClaimResponseType }
     * 
     */
    public InitiateClaimResponseType createInitiateClaimResponseType() {
        return new InitiateClaimResponseType();
    }

    /**
     * Create an instance of {@link SpezifikationSchadenType }
     * 
     */
    public SpezifikationSchadenType createSpezifikationSchadenType() {
        return new SpezifikationSchadenType();
    }

    /**
     * Create an instance of {@link GetClaimResponseType }
     * 
     */
    public GetClaimResponseType createGetClaimResponseType() {
        return new GetClaimResponseType();
    }

    /**
     * Create an instance of {@link GetClaimResponseLightType }
     * 
     */
    public GetClaimResponseLightType createGetClaimResponseLightType() {
        return new GetClaimResponseLightType();
    }

    /**
     * Create an instance of {@link SearchClaimRequestType }
     * 
     */
    public SearchClaimRequestType createSearchClaimRequestType() {
        return new SearchClaimRequestType();
    }

    /**
     * Create an instance of {@link SearchClaimResponseType }
     * 
     */
    public SearchClaimResponseType createSearchClaimResponseType() {
        return new SearchClaimResponseType();
    }

    /**
     * Create an instance of {@link SchadenzuordnungType }
     * 
     */
    public SchadenzuordnungType createSchadenzuordnungType() {
        return new SchadenzuordnungType();
    }

    /**
     * Create an instance of {@link ChangedClaimsListRequestType }
     * 
     */
    public ChangedClaimsListRequestType createChangedClaimsListRequestType() {
        return new ChangedClaimsListRequestType();
    }

    /**
     * Create an instance of {@link ChangedClaimsListResponseType }
     * 
     */
    public ChangedClaimsListResponseType createChangedClaimsListResponseType() {
        return new ChangedClaimsListResponseType();
    }

    /**
     * Create an instance of {@link LossEventListRequestType }
     * 
     */
    public LossEventListRequestType createLossEventListRequestType() {
        return new LossEventListRequestType();
    }

    /**
     * Create an instance of {@link LossEventListResponseType }
     * 
     */
    public LossEventListResponseType createLossEventListResponseType() {
        return new LossEventListResponseType();
    }

    /**
     * Create an instance of {@link DeclareNewClaimStatusRequestType }
     * 
     */
    public DeclareNewClaimStatusRequestType createDeclareNewClaimStatusRequestType() {
        return new DeclareNewClaimStatusRequestType();
    }

    /**
     * Create an instance of {@link DeclareNewClaimStatusResponseType }
     * 
     */
    public DeclareNewClaimStatusResponseType createDeclareNewClaimStatusResponseType() {
        return new DeclareNewClaimStatusResponseType();
    }

    /**
     * Create an instance of {@link LossEventType }
     * 
     */
    public LossEventType createLossEventType() {
        return new LossEventType();
    }

    /**
     * Create an instance of {@link LossEventRegisteredResponseType }
     * 
     */
    public LossEventRegisteredResponseType createLossEventRegisteredResponseType() {
        return new LossEventRegisteredResponseType();
    }

    /**
     * Create an instance of {@link MeldungSchadenereignisType }
     * 
     */
    public MeldungSchadenereignisType createMeldungSchadenereignisType() {
        return new MeldungSchadenereignisType();
    }

    /**
     * Create an instance of {@link SchadenereignisType }
     * 
     */
    public SchadenereignisType createSchadenereignisType() {
        return new SchadenereignisType();
    }

    /**
     * Create an instance of {@link SchadenType }
     * 
     */
    public SchadenType createSchadenType() {
        return new SchadenType();
    }

    /**
     * Create an instance of {@link BeteiligtePersonType }
     * 
     */
    public BeteiligtePersonType createBeteiligtePersonType() {
        return new BeteiligtePersonType();
    }

    /**
     * Create an instance of {@link SpartendetailSchadenKfzType }
     * 
     */
    public SpartendetailSchadenKfzType createSpartendetailSchadenKfzType() {
        return new SpartendetailSchadenKfzType();
    }

    /**
     * Create an instance of {@link SpartendetailSchadenUnfallType }
     * 
     */
    public SpartendetailSchadenUnfallType createSpartendetailSchadenUnfallType() {
        return new SpartendetailSchadenUnfallType();
    }

    /**
     * Create an instance of {@link OrtType }
     * 
     */
    public OrtType createOrtType() {
        return new OrtType();
    }

    /**
     * Create an instance of {@link GeokoordinatenType }
     * 
     */
    public GeokoordinatenType createGeokoordinatenType() {
        return new GeokoordinatenType();
    }

    /**
     * Create an instance of {@link GeschaedigtesObjektKfzType }
     * 
     */
    public GeschaedigtesObjektKfzType createGeschaedigtesObjektKfzType() {
        return new GeschaedigtesObjektKfzType();
    }

    /**
     * Create an instance of {@link GeschaedigtesObjektImmobilieType }
     * 
     */
    public GeschaedigtesObjektImmobilieType createGeschaedigtesObjektImmobilieType() {
        return new GeschaedigtesObjektImmobilieType();
    }

    /**
     * Create an instance of {@link SchadenmelderVermittlerType }
     * 
     */
    public SchadenmelderVermittlerType createSchadenmelderVermittlerType() {
        return new SchadenmelderVermittlerType();
    }

    /**
     * Create an instance of {@link MeldungszusammenfassungType }
     * 
     */
    public MeldungszusammenfassungType createMeldungszusammenfassungType() {
        return new MeldungszusammenfassungType();
    }

    /**
     * Create an instance of {@link ErgebnisDokumentType }
     * 
     */
    public ErgebnisDokumentType createErgebnisDokumentType() {
        return new ErgebnisDokumentType();
    }

    /**
     * Create an instance of {@link ErgebnisSchadenType }
     * 
     */
    public ErgebnisSchadenType createErgebnisSchadenType() {
        return new ErgebnisSchadenType();
    }

    /**
     * Create an instance of {@link SchadenanlageType }
     * 
     */
    public SchadenanlageType createSchadenanlageType() {
        return new SchadenanlageType();
    }

    /**
     * Create an instance of {@link MeldungszusammenfassungInitiateClaimType }
     * 
     */
    public MeldungszusammenfassungInitiateClaimType createMeldungszusammenfassungInitiateClaimType() {
        return new MeldungszusammenfassungInitiateClaimType();
    }

    /**
     * Create an instance of {@link BearbStandSchadenType }
     * 
     */
    public BearbStandSchadenType createBearbStandSchadenType() {
        return new BearbStandSchadenType();
    }

    /**
     * Create an instance of {@link ErgebnisDokumentAnlageType }
     * 
     */
    public ErgebnisDokumentAnlageType createErgebnisDokumentAnlageType() {
        return new ErgebnisDokumentAnlageType();
    }

    /**
     * Create an instance of {@link BeteiligtePersonSchadenType }
     * 
     */
    public BeteiligtePersonSchadenType createBeteiligtePersonSchadenType() {
        return new BeteiligtePersonSchadenType();
    }

    /**
     * Create an instance of {@link ReferenzAufBeteiligtePersonSchadenType }
     * 
     */
    public ReferenzAufBeteiligtePersonSchadenType createReferenzAufBeteiligtePersonSchadenType() {
        return new ReferenzAufBeteiligtePersonSchadenType();
    }

    /**
     * Create an instance of {@link SchadenbeteiligtePersonType }
     * 
     */
    public SchadenbeteiligtePersonType createSchadenbeteiligtePersonType() {
        return new SchadenbeteiligtePersonType();
    }

    /**
     * Create an instance of {@link SchadenereignisLightType }
     * 
     */
    public SchadenereignisLightType createSchadenereignisLightType() {
        return new SchadenereignisLightType();
    }

    /**
     * Create an instance of {@link SchadenLightType }
     * 
     */
    public SchadenLightType createSchadenLightType() {
        return new SchadenLightType();
    }

    /**
     * Create an instance of {@link SearchClaimResponseResultType }
     * 
     */
    public SearchClaimResponseResultType createSearchClaimResponseResultType() {
        return new SearchClaimResponseResultType();
    }

    /**
     * Create an instance of {@link SachbearbVUType }
     * 
     */
    public SachbearbVUType createSachbearbVUType() {
        return new SachbearbVUType();
    }

    /**
     * Create an instance of {@link NatPersonType }
     * 
     */
    public NatPersonType createNatPersonType() {
        return new NatPersonType();
    }

    /**
     * Create an instance of {@link ChangedClaimsListResponseResultType }
     * 
     */
    public ChangedClaimsListResponseResultType createChangedClaimsListResponseResultType() {
        return new ChangedClaimsListResponseResultType();
    }

    /**
     * Create an instance of {@link SchadenStatusType }
     * 
     */
    public SchadenStatusType createSchadenStatusType() {
        return new SchadenStatusType();
    }

    /**
     * Create an instance of {@link SchadenInfoType }
     * 
     */
    public SchadenInfoType createSchadenInfoType() {
        return new SchadenInfoType();
    }

    /**
     * Create an instance of {@link LossEventListResponseResultType }
     * 
     */
    public LossEventListResponseResultType createLossEventListResponseResultType() {
        return new LossEventListResponseResultType();
    }

    /**
     * Create an instance of {@link SpartendetailSchadenKrankenType.Behandlungen }
     * 
     */
    public SpartendetailSchadenKrankenType.Behandlungen createSpartendetailSchadenKrankenTypeBehandlungen() {
        return new SpartendetailSchadenKrankenType.Behandlungen();
    }

    /**
     * Create an instance of {@link MeldungSchadenType.BeteiligtePersonen }
     * 
     */
    public MeldungSchadenType.BeteiligtePersonen createMeldungSchadenTypeBeteiligtePersonen() {
        return new MeldungSchadenType.BeteiligtePersonen();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateClaimRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", name = "CreateClaimRequest")
    public JAXBElement<CreateClaimRequestType> createCreateClaimRequest(CreateClaimRequestType value) {
        return new JAXBElement<CreateClaimRequestType>(_CreateClaimRequest_QNAME, CreateClaimRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateClaimResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", name = "CreateClaimResponse")
    public JAXBElement<CreateClaimResponseType> createCreateClaimResponse(CreateClaimResponseType value) {
        return new JAXBElement<CreateClaimResponseType>(_CreateClaimResponse_QNAME, CreateClaimResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubmitClaimRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", name = "SubmitClaimRequest")
    public JAXBElement<SubmitClaimRequestType> createSubmitClaimRequest(SubmitClaimRequestType value) {
        return new JAXBElement<SubmitClaimRequestType>(_SubmitClaimRequest_QNAME, SubmitClaimRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubmitClaimResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", name = "SubmitClaimResponse")
    public JAXBElement<SubmitClaimResponseType> createSubmitClaimResponse(SubmitClaimResponseType value) {
        return new JAXBElement<SubmitClaimResponseType>(_SubmitClaimResponse_QNAME, SubmitClaimResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InitiateClaimRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", name = "InitiateClaimRequest")
    public JAXBElement<InitiateClaimRequestType> createInitiateClaimRequest(InitiateClaimRequestType value) {
        return new JAXBElement<InitiateClaimRequestType>(_InitiateClaimRequest_QNAME, InitiateClaimRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InitiateClaimResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", name = "InitiateClaimResponse")
    public JAXBElement<InitiateClaimResponseType> createInitiateClaimResponse(InitiateClaimResponseType value) {
        return new JAXBElement<InitiateClaimResponseType>(_InitiateClaimResponse_QNAME, InitiateClaimResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SpezifikationSchadenType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", name = "GetClaimRequest")
    public JAXBElement<SpezifikationSchadenType> createGetClaimRequest(SpezifikationSchadenType value) {
        return new JAXBElement<SpezifikationSchadenType>(_GetClaimRequest_QNAME, SpezifikationSchadenType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetClaimResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", name = "GetClaimResponse")
    public JAXBElement<GetClaimResponseType> createGetClaimResponse(GetClaimResponseType value) {
        return new JAXBElement<GetClaimResponseType>(_GetClaimResponse_QNAME, GetClaimResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SpezifikationSchadenType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", name = "GetClaimLightRequest")
    public JAXBElement<SpezifikationSchadenType> createGetClaimLightRequest(SpezifikationSchadenType value) {
        return new JAXBElement<SpezifikationSchadenType>(_GetClaimLightRequest_QNAME, SpezifikationSchadenType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetClaimResponseLightType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", name = "GetClaimLightResponse")
    public JAXBElement<GetClaimResponseLightType> createGetClaimLightResponse(GetClaimResponseLightType value) {
        return new JAXBElement<GetClaimResponseLightType>(_GetClaimLightResponse_QNAME, GetClaimResponseLightType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchClaimRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", name = "SearchClaimRequest")
    public JAXBElement<SearchClaimRequestType> createSearchClaimRequest(SearchClaimRequestType value) {
        return new JAXBElement<SearchClaimRequestType>(_SearchClaimRequest_QNAME, SearchClaimRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchClaimResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", name = "SearchClaimResponse")
    public JAXBElement<SearchClaimResponseType> createSearchClaimResponse(SearchClaimResponseType value) {
        return new JAXBElement<SearchClaimResponseType>(_SearchClaimResponse_QNAME, SearchClaimResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SchadenzuordnungType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", name = "Schadenzuordnung")
    public JAXBElement<SchadenzuordnungType> createSchadenzuordnung(SchadenzuordnungType value) {
        return new JAXBElement<SchadenzuordnungType>(_Schadenzuordnung_QNAME, SchadenzuordnungType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangedClaimsListRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", name = "ChangedClaimsListRequest")
    public JAXBElement<ChangedClaimsListRequestType> createChangedClaimsListRequest(ChangedClaimsListRequestType value) {
        return new JAXBElement<ChangedClaimsListRequestType>(_ChangedClaimsListRequest_QNAME, ChangedClaimsListRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangedClaimsListResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", name = "ChangedClaimsListResponse")
    public JAXBElement<ChangedClaimsListResponseType> createChangedClaimsListResponse(ChangedClaimsListResponseType value) {
        return new JAXBElement<ChangedClaimsListResponseType>(_ChangedClaimsListResponse_QNAME, ChangedClaimsListResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LossEventListRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", name = "LossEventListRequest")
    public JAXBElement<LossEventListRequestType> createLossEventListRequest(LossEventListRequestType value) {
        return new JAXBElement<LossEventListRequestType>(_LossEventListRequest_QNAME, LossEventListRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LossEventListResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", name = "LossEventListResponse")
    public JAXBElement<LossEventListResponseType> createLossEventListResponse(LossEventListResponseType value) {
        return new JAXBElement<LossEventListResponseType>(_LossEventListResponse_QNAME, LossEventListResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObjektIdType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", name = "IdGeschaeftsfallSchadenereignis")
    public JAXBElement<ObjektIdType> createIdGeschaeftsfallSchadenereignis(ObjektIdType value) {
        return new JAXBElement<ObjektIdType>(_IdGeschaeftsfallSchadenereignis_QNAME, ObjektIdType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObjektIdType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", name = "IdGeschaeftsfallSchadenanlage")
    public JAXBElement<ObjektIdType> createIdGeschaeftsfallSchadenanlage(ObjektIdType value) {
        return new JAXBElement<ObjektIdType>(_IdGeschaeftsfallSchadenanlage_QNAME, ObjektIdType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeclareNewClaimStatusRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", name = "DeclareNewClaimStatusRequest")
    public JAXBElement<DeclareNewClaimStatusRequestType> createDeclareNewClaimStatusRequest(DeclareNewClaimStatusRequestType value) {
        return new JAXBElement<DeclareNewClaimStatusRequestType>(_DeclareNewClaimStatusRequest_QNAME, DeclareNewClaimStatusRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeclareNewClaimStatusResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", name = "DeclareNewClaimStatusResponse")
    public JAXBElement<DeclareNewClaimStatusResponseType> createDeclareNewClaimStatusResponse(DeclareNewClaimStatusResponseType value) {
        return new JAXBElement<DeclareNewClaimStatusResponseType>(_DeclareNewClaimStatusResponse_QNAME, DeclareNewClaimStatusResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LossEventType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", name = "LossEventRegisteredRequest")
    public JAXBElement<LossEventType> createLossEventRegisteredRequest(LossEventType value) {
        return new JAXBElement<LossEventType>(_LossEventRegisteredRequest_QNAME, LossEventType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LossEventRegisteredResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden", name = "LossEventRegisteredResponse")
    public JAXBElement<LossEventRegisteredResponseType> createLossEventRegisteredResponse(LossEventRegisteredResponseType value) {
        return new JAXBElement<LossEventRegisteredResponseType>(_LossEventRegisteredResponse_QNAME, LossEventRegisteredResponseType.class, null, value);
    }

}
