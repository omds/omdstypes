
package at.vvo.omds.types.omds3Types.r1_4_0.on7schaden;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds2Types.v2_11.ADRESSEType;


/**
 * Typ für den Schadenort
 * 
 * <p>Java-Klasse für Ort_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Ort_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Adresse" type="{urn:omds20}ADRESSE_Type" minOccurs="0"/&gt;
 *         &lt;element name="Beschreibung" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Geokoordinaten" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}Geokoordinaten_Type" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Ort_Type", propOrder = {
    "adresse",
    "beschreibung",
    "geokoordinaten"
})
public class OrtType {

    @XmlElement(name = "Adresse")
    protected ADRESSEType adresse;
    @XmlElement(name = "Beschreibung")
    protected String beschreibung;
    @XmlElement(name = "Geokoordinaten")
    protected GeokoordinatenType geokoordinaten;

    /**
     * Ruft den Wert der adresse-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ADRESSEType }
     *     
     */
    public ADRESSEType getAdresse() {
        return adresse;
    }

    /**
     * Legt den Wert der adresse-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ADRESSEType }
     *     
     */
    public void setAdresse(ADRESSEType value) {
        this.adresse = value;
    }

    /**
     * Ruft den Wert der beschreibung-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBeschreibung() {
        return beschreibung;
    }

    /**
     * Legt den Wert der beschreibung-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBeschreibung(String value) {
        this.beschreibung = value;
    }

    /**
     * Ruft den Wert der geokoordinaten-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GeokoordinatenType }
     *     
     */
    public GeokoordinatenType getGeokoordinaten() {
        return geokoordinaten;
    }

    /**
     * Legt den Wert der geokoordinaten-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GeokoordinatenType }
     *     
     */
    public void setGeokoordinaten(GeokoordinatenType value) {
        this.geokoordinaten = value;
    }

}
