
package at.vvo.omds.types.omds3Types.r1_4_0.on7schaden;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds2Types.v2_11.ELKommunikationType;


/**
 * Typ für die Übermittlung von Kontaktdaten eines Sachbearbeiters
 * 
 * <p>Java-Klasse für SachbearbVUType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SachbearbVUType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Person" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}NatPerson_Type"/&gt;
 *         &lt;element ref="{urn:omds20}EL-Kommunikation" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SachbearbVUType", propOrder = {
    "person",
    "elKommunikation"
})
public class SachbearbVUType {

    @XmlElement(name = "Person", required = true)
    protected NatPersonType person;
    @XmlElement(name = "EL-Kommunikation", namespace = "urn:omds20", required = true)
    protected List<ELKommunikationType> elKommunikation;

    /**
     * Ruft den Wert der person-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link NatPersonType }
     *     
     */
    public NatPersonType getPerson() {
        return person;
    }

    /**
     * Legt den Wert der person-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link NatPersonType }
     *     
     */
    public void setPerson(NatPersonType value) {
        this.person = value;
    }

    /**
     * Gets the value of the elKommunikation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the elKommunikation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getELKommunikation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ELKommunikationType }
     * 
     * 
     */
    public List<ELKommunikationType> getELKommunikation() {
        if (elKommunikation == null) {
            elKommunikation = new ArrayList<ELKommunikationType>();
        }
        return this.elKommunikation;
    }

}
