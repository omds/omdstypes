
package at.vvo.omds.types.omds3Types.r1_4_0.on7schaden;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.ObjektIdType;


/**
 * <p>Java-Klasse für SchadenInfo_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SchadenInfo_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}IdGeschaeftsfallSchadenanlage"/&gt;
 *         &lt;element name="BearbStandCd" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}BearbStandCd_Type"/&gt;
 *         &lt;element name="Schadennr" type="{urn:omds20}Schadennr" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}Schadenzuordnung" minOccurs="0"/&gt;
 *         &lt;element name="SachbearbVU" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}SachbearbVUType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SchadenInfo_Type", propOrder = {
    "idGeschaeftsfallSchadenanlage",
    "bearbStandCd",
    "schadennr",
    "schadenzuordnung",
    "sachbearbVU"
})
public class SchadenInfoType {

    @XmlElement(name = "IdGeschaeftsfallSchadenanlage", required = true)
    protected ObjektIdType idGeschaeftsfallSchadenanlage;
    @XmlElement(name = "BearbStandCd", required = true)
    protected String bearbStandCd;
    @XmlElement(name = "Schadennr")
    protected String schadennr;
    @XmlElement(name = "Schadenzuordnung")
    protected SchadenzuordnungType schadenzuordnung;
    @XmlElement(name = "SachbearbVU")
    protected SachbearbVUType sachbearbVU;

    /**
     * Ruft den Wert der idGeschaeftsfallSchadenanlage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ObjektIdType }
     *     
     */
    public ObjektIdType getIdGeschaeftsfallSchadenanlage() {
        return idGeschaeftsfallSchadenanlage;
    }

    /**
     * Legt den Wert der idGeschaeftsfallSchadenanlage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjektIdType }
     *     
     */
    public void setIdGeschaeftsfallSchadenanlage(ObjektIdType value) {
        this.idGeschaeftsfallSchadenanlage = value;
    }

    /**
     * Ruft den Wert der bearbStandCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBearbStandCd() {
        return bearbStandCd;
    }

    /**
     * Legt den Wert der bearbStandCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBearbStandCd(String value) {
        this.bearbStandCd = value;
    }

    /**
     * Ruft den Wert der schadennr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSchadennr() {
        return schadennr;
    }

    /**
     * Legt den Wert der schadennr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSchadennr(String value) {
        this.schadennr = value;
    }

    /**
     * Anhand der Schadenzuordnung kann man erkennen, um welche Schadensparte es sich handelt.
     * 
     * @return
     *     possible object is
     *     {@link SchadenzuordnungType }
     *     
     */
    public SchadenzuordnungType getSchadenzuordnung() {
        return schadenzuordnung;
    }

    /**
     * Legt den Wert der schadenzuordnung-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SchadenzuordnungType }
     *     
     */
    public void setSchadenzuordnung(SchadenzuordnungType value) {
        this.schadenzuordnung = value;
    }

    /**
     * Ruft den Wert der sachbearbVU-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SachbearbVUType }
     *     
     */
    public SachbearbVUType getSachbearbVU() {
        return sachbearbVU;
    }

    /**
     * Legt den Wert der sachbearbVU-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SachbearbVUType }
     *     
     */
    public void setSachbearbVU(SachbearbVUType value) {
        this.sachbearbVU = value;
    }

}
