
package at.vvo.omds.types.omds3Types.r1_4_0.on7schaden;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.AbstraktesEreignisStatusAenderungType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.ObjektIdType;


/**
 * Objekt, welches GeschäftsfallId und Schadennummer sowie den Bearbeitungsstand enthält
 * 
 * <p>Java-Klasse für SchadenStatus_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SchadenStatus_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}AbstraktesEreignisStatusAenderung_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}IdGeschaeftsfallSchadenereignis"/&gt;
 *         &lt;element name="VUNr" type="{urn:omds20}VUNr"/&gt;
 *         &lt;element ref="{urn:omds3CommonServiceTypes-1-1-0}OrdnungsbegriffZuordFremd" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Schaeden" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}SchadenInfo_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SchadenStatus_Type", propOrder = {
    "idGeschaeftsfallSchadenereignis",
    "vuNr",
    "ordnungsbegriffZuordFremd",
    "schaeden"
})
public class SchadenStatusType
    extends AbstraktesEreignisStatusAenderungType
{

    @XmlElement(name = "IdGeschaeftsfallSchadenereignis", required = true)
    protected ObjektIdType idGeschaeftsfallSchadenereignis;
    @XmlElement(name = "VUNr", required = true)
    protected String vuNr;
    @XmlElement(name = "OrdnungsbegriffZuordFremd", namespace = "urn:omds3CommonServiceTypes-1-1-0")
    protected List<String> ordnungsbegriffZuordFremd;
    @XmlElement(name = "Schaeden")
    protected List<SchadenInfoType> schaeden;

    /**
     * Ruft den Wert der idGeschaeftsfallSchadenereignis-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ObjektIdType }
     *     
     */
    public ObjektIdType getIdGeschaeftsfallSchadenereignis() {
        return idGeschaeftsfallSchadenereignis;
    }

    /**
     * Legt den Wert der idGeschaeftsfallSchadenereignis-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjektIdType }
     *     
     */
    public void setIdGeschaeftsfallSchadenereignis(ObjektIdType value) {
        this.idGeschaeftsfallSchadenereignis = value;
    }

    /**
     * Ruft den Wert der vuNr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVUNr() {
        return vuNr;
    }

    /**
     * Legt den Wert der vuNr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVUNr(String value) {
        this.vuNr = value;
    }

    /**
     * Ordnungsbegriff des Schadenmelders auf Ebene des Schadenereignis Gets the value of the ordnungsbegriffZuordFremd property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ordnungsbegriffZuordFremd property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOrdnungsbegriffZuordFremd().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getOrdnungsbegriffZuordFremd() {
        if (ordnungsbegriffZuordFremd == null) {
            ordnungsbegriffZuordFremd = new ArrayList<String>();
        }
        return this.ordnungsbegriffZuordFremd;
    }

    /**
     * Gets the value of the schaeden property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the schaeden property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSchaeden().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SchadenInfoType }
     * 
     * 
     */
    public List<SchadenInfoType> getSchaeden() {
        if (schaeden == null) {
            schaeden = new ArrayList<SchadenInfoType>();
        }
        return this.schaeden;
    }

}
