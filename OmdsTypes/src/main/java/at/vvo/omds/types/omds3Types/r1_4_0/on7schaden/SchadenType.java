
package at.vvo.omds.types.omds3Types.r1_4_0.on7schaden;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.ObjektIdType;


/**
 * Die Darstellung eines Schadens (Unterobjekt eines Schadenereignisses)
 * 
 * <p>Java-Klasse für Schaden_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Schaden_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}MeldungSchaden_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="BearbStandCd" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}BearbStandCd_Type"/&gt;
 *         &lt;element name="GeschaeftsfallSchadenanlage" type="{urn:omds3CommonServiceTypes-1-1-0}ObjektId_Type"/&gt;
 *         &lt;element name="Schadennr" type="{urn:omds20}Schadennr" minOccurs="0"/&gt;
 *         &lt;element name="VormaligeSchadennummern" type="{urn:omds20}Schadennr" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="NachfolgendeSchadennr" type="{urn:omds20}Schadennr" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Schaden_Type", propOrder = {
    "bearbStandCd",
    "geschaeftsfallSchadenanlage",
    "schadennr",
    "vormaligeSchadennummern",
    "nachfolgendeSchadennr"
})
public class SchadenType
    extends MeldungSchadenType
{

    @XmlElement(name = "BearbStandCd", required = true)
    protected String bearbStandCd;
    @XmlElement(name = "GeschaeftsfallSchadenanlage", required = true)
    protected ObjektIdType geschaeftsfallSchadenanlage;
    @XmlElement(name = "Schadennr")
    protected String schadennr;
    @XmlElement(name = "VormaligeSchadennummern")
    protected List<String> vormaligeSchadennummern;
    @XmlElement(name = "NachfolgendeSchadennr")
    protected String nachfolgendeSchadennr;

    /**
     * Ruft den Wert der bearbStandCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBearbStandCd() {
        return bearbStandCd;
    }

    /**
     * Legt den Wert der bearbStandCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBearbStandCd(String value) {
        this.bearbStandCd = value;
    }

    /**
     * Ruft den Wert der geschaeftsfallSchadenanlage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ObjektIdType }
     *     
     */
    public ObjektIdType getGeschaeftsfallSchadenanlage() {
        return geschaeftsfallSchadenanlage;
    }

    /**
     * Legt den Wert der geschaeftsfallSchadenanlage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjektIdType }
     *     
     */
    public void setGeschaeftsfallSchadenanlage(ObjektIdType value) {
        this.geschaeftsfallSchadenanlage = value;
    }

    /**
     * Ruft den Wert der schadennr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSchadennr() {
        return schadennr;
    }

    /**
     * Legt den Wert der schadennr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSchadennr(String value) {
        this.schadennr = value;
    }

    /**
     * Gets the value of the vormaligeSchadennummern property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the vormaligeSchadennummern property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVormaligeSchadennummern().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getVormaligeSchadennummern() {
        if (vormaligeSchadennummern == null) {
            vormaligeSchadennummern = new ArrayList<String>();
        }
        return this.vormaligeSchadennummern;
    }

    /**
     * Ruft den Wert der nachfolgendeSchadennr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNachfolgendeSchadennr() {
        return nachfolgendeSchadennr;
    }

    /**
     * Legt den Wert der nachfolgendeSchadennr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNachfolgendeSchadennr(String value) {
        this.nachfolgendeSchadennr = value;
    }

}
