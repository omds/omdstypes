
package at.vvo.omds.types.omds3Types.r1_4_0.on7schaden;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.ObjektIdType;


/**
 * <p>Java-Klasse für Schadenanlage_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Schadenanlage_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:omds3CommonServiceTypes-1-1-0}Geschaeftsfallnummer"/&gt;
 *         &lt;element ref="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}Schadenzuordnung"/&gt;
 *         &lt;element name="Status" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}BearbStandCd_Type"/&gt;
 *         &lt;element name="Schadennr" type="{urn:omds20}Schadennr" minOccurs="0"/&gt;
 *         &lt;element name="SachbearbVU" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}SachbearbVUType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Schadenanlage_Type", propOrder = {
    "geschaeftsfallnummer",
    "schadenzuordnung",
    "status",
    "schadennr",
    "sachbearbVU"
})
public class SchadenanlageType {

    @XmlElement(name = "Geschaeftsfallnummer", namespace = "urn:omds3CommonServiceTypes-1-1-0", required = true)
    protected ObjektIdType geschaeftsfallnummer;
    @XmlElement(name = "Schadenzuordnung", required = true)
    protected SchadenzuordnungType schadenzuordnung;
    @XmlElement(name = "Status", required = true)
    protected String status;
    @XmlElement(name = "Schadennr")
    protected String schadennr;
    @XmlElement(name = "SachbearbVU")
    protected SachbearbVUType sachbearbVU;

    /**
     * GeschäftfallId der Anlage des spartenbezogenen Schaden-Objektes
     * 
     * @return
     *     possible object is
     *     {@link ObjektIdType }
     *     
     */
    public ObjektIdType getGeschaeftsfallnummer() {
        return geschaeftsfallnummer;
    }

    /**
     * Legt den Wert der geschaeftsfallnummer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjektIdType }
     *     
     */
    public void setGeschaeftsfallnummer(ObjektIdType value) {
        this.geschaeftsfallnummer = value;
    }

    /**
     * Anhand der Schadenzuordnung kann man erkennen, um welche Schadensparte es sich handelt.
     * 
     * @return
     *     possible object is
     *     {@link SchadenzuordnungType }
     *     
     */
    public SchadenzuordnungType getSchadenzuordnung() {
        return schadenzuordnung;
    }

    /**
     * Legt den Wert der schadenzuordnung-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SchadenzuordnungType }
     *     
     */
    public void setSchadenzuordnung(SchadenzuordnungType value) {
        this.schadenzuordnung = value;
    }

    /**
     * Ruft den Wert der status-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Legt den Wert der status-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Ruft den Wert der schadennr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSchadennr() {
        return schadennr;
    }

    /**
     * Legt den Wert der schadennr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSchadennr(String value) {
        this.schadennr = value;
    }

    /**
     * Ruft den Wert der sachbearbVU-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SachbearbVUType }
     *     
     */
    public SachbearbVUType getSachbearbVU() {
        return sachbearbVU;
    }

    /**
     * Legt den Wert der sachbearbVU-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SachbearbVUType }
     *     
     */
    public void setSachbearbVU(SachbearbVUType value) {
        this.sachbearbVU = value;
    }

}
