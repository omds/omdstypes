
package at.vvo.omds.types.omds3Types.r1_4_0.on7schaden;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import at.vvo.omds.types.omds3Types.r1_4_0.common.BankverbindungType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.UploadDokumentType;


/**
 * Eine Beschreibung eines Schadenereignisses ohne Ids. Von diesem Type erben die Meldung und die Darstellung eines Schadenereignisses.
 * 
 * <p>Java-Klasse für SchadenereignisAbstrakt_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SchadenereignisAbstrakt_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Ereigniszpkt" type="{urn:omds20}Datum-Zeit"/&gt;
 *         &lt;element name="EreignisbeschrTxt" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="SchadOrt" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}Ort_Type"/&gt;
 *         &lt;element name="BeteiligtePersonen" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}BeteiligtePerson_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="GeschaedigteInteressen" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}GeschaedigtesInteresse_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Dokumente" type="{urn:omds3CommonServiceTypes-1-1-0}Upload_Dokument_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Meldedat" type="{urn:omds20}Datum-Zeit" minOccurs="0"/&gt;
 *         &lt;element name="Schadenmelder" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}Schadenmelder_Type" minOccurs="0"/&gt;
 *         &lt;element name="Bankverbindung" type="{urn:omds3CommonServiceTypes-1-1-0}Bankverbindung_Type"/&gt;
 *         &lt;element name="ZusaetzlicheSchadensereignisdaten" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}ZusaetzlicheSchadensereignisdaten_Type" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SchadenereignisAbstrakt_Type", propOrder = {
    "ereigniszpkt",
    "ereignisbeschrTxt",
    "schadOrt",
    "beteiligtePersonen",
    "geschaedigteInteressen",
    "dokumente",
    "meldedat",
    "schadenmelder",
    "bankverbindung",
    "zusaetzlicheSchadensereignisdaten"
})
@XmlSeeAlso({
    MeldungSchadenereignisType.class,
    SchadenereignisType.class
})
public abstract class SchadenereignisAbstraktType {

    @XmlElement(name = "Ereigniszpkt", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar ereigniszpkt;
    @XmlElement(name = "EreignisbeschrTxt", required = true)
    protected String ereignisbeschrTxt;
    @XmlElement(name = "SchadOrt", required = true)
    protected OrtType schadOrt;
    @XmlElement(name = "BeteiligtePersonen")
    protected List<BeteiligtePersonType> beteiligtePersonen;
    @XmlElement(name = "GeschaedigteInteressen")
    protected List<GeschaedigtesInteresseType> geschaedigteInteressen;
    @XmlElement(name = "Dokumente")
    protected List<UploadDokumentType> dokumente;
    @XmlElement(name = "Meldedat")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar meldedat;
    @XmlElement(name = "Schadenmelder")
    protected SchadenmelderType schadenmelder;
    @XmlElement(name = "Bankverbindung", required = true)
    protected BankverbindungType bankverbindung;
    @XmlElement(name = "ZusaetzlicheSchadensereignisdaten")
    protected ZusaetzlicheSchadensereignisdatenType zusaetzlicheSchadensereignisdaten;

    /**
     * Ruft den Wert der ereigniszpkt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEreigniszpkt() {
        return ereigniszpkt;
    }

    /**
     * Legt den Wert der ereigniszpkt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEreigniszpkt(XMLGregorianCalendar value) {
        this.ereigniszpkt = value;
    }

    /**
     * Ruft den Wert der ereignisbeschrTxt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEreignisbeschrTxt() {
        return ereignisbeschrTxt;
    }

    /**
     * Legt den Wert der ereignisbeschrTxt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEreignisbeschrTxt(String value) {
        this.ereignisbeschrTxt = value;
    }

    /**
     * Ruft den Wert der schadOrt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OrtType }
     *     
     */
    public OrtType getSchadOrt() {
        return schadOrt;
    }

    /**
     * Legt den Wert der schadOrt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OrtType }
     *     
     */
    public void setSchadOrt(OrtType value) {
        this.schadOrt = value;
    }

    /**
     * Gets the value of the beteiligtePersonen property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the beteiligtePersonen property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBeteiligtePersonen().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BeteiligtePersonType }
     * 
     * 
     */
    public List<BeteiligtePersonType> getBeteiligtePersonen() {
        if (beteiligtePersonen == null) {
            beteiligtePersonen = new ArrayList<BeteiligtePersonType>();
        }
        return this.beteiligtePersonen;
    }

    /**
     * Gets the value of the geschaedigteInteressen property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the geschaedigteInteressen property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGeschaedigteInteressen().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GeschaedigtesInteresseType }
     * 
     * 
     */
    public List<GeschaedigtesInteresseType> getGeschaedigteInteressen() {
        if (geschaedigteInteressen == null) {
            geschaedigteInteressen = new ArrayList<GeschaedigtesInteresseType>();
        }
        return this.geschaedigteInteressen;
    }

    /**
     * Gets the value of the dokumente property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dokumente property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDokumente().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link UploadDokumentType }
     * 
     * 
     */
    public List<UploadDokumentType> getDokumente() {
        if (dokumente == null) {
            dokumente = new ArrayList<UploadDokumentType>();
        }
        return this.dokumente;
    }

    /**
     * Ruft den Wert der meldedat-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getMeldedat() {
        return meldedat;
    }

    /**
     * Legt den Wert der meldedat-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setMeldedat(XMLGregorianCalendar value) {
        this.meldedat = value;
    }

    /**
     * Ruft den Wert der schadenmelder-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SchadenmelderType }
     *     
     */
    public SchadenmelderType getSchadenmelder() {
        return schadenmelder;
    }

    /**
     * Legt den Wert der schadenmelder-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SchadenmelderType }
     *     
     */
    public void setSchadenmelder(SchadenmelderType value) {
        this.schadenmelder = value;
    }

    /**
     * Ruft den Wert der bankverbindung-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BankverbindungType }
     *     
     */
    public BankverbindungType getBankverbindung() {
        return bankverbindung;
    }

    /**
     * Legt den Wert der bankverbindung-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BankverbindungType }
     *     
     */
    public void setBankverbindung(BankverbindungType value) {
        this.bankverbindung = value;
    }

    /**
     * Ruft den Wert der zusaetzlicheSchadensereignisdaten-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ZusaetzlicheSchadensereignisdatenType }
     *     
     */
    public ZusaetzlicheSchadensereignisdatenType getZusaetzlicheSchadensereignisdaten() {
        return zusaetzlicheSchadensereignisdaten;
    }

    /**
     * Legt den Wert der zusaetzlicheSchadensereignisdaten-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ZusaetzlicheSchadensereignisdatenType }
     *     
     */
    public void setZusaetzlicheSchadensereignisdaten(ZusaetzlicheSchadensereignisdatenType value) {
        this.zusaetzlicheSchadensereignisdaten = value;
    }

}
