
package at.vvo.omds.types.omds3Types.r1_4_0.on7schaden;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import at.vvo.omds.types.omds3Types.r1_4_0.common.ObjektIdType;


/**
 * Typ zur leichtgewichtigen Abbildung von Schadenereignis-Objekten
 * 
 * <p>Java-Klasse für SchadenereignisLight_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SchadenereignisLight_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:omds3CommonServiceTypes-1-1-0}Geschaeftsfallnummer"/&gt;
 *         &lt;element name="VormaligeIdGeschaeftsfall" type="{urn:omds3CommonServiceTypes-1-1-0}ObjektId_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="NachfolgendeIdGeschaeftsfall" type="{urn:omds3CommonServiceTypes-1-1-0}ObjektId_Type" minOccurs="0"/&gt;
 *         &lt;element name="Schaeden" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}SchadenLight_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Meldedat" type="{urn:omds20}Datum-Zeit"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SchadenereignisLight_Type", propOrder = {
    "geschaeftsfallnummer",
    "vormaligeIdGeschaeftsfall",
    "nachfolgendeIdGeschaeftsfall",
    "schaeden",
    "meldedat"
})
public class SchadenereignisLightType {

    @XmlElement(name = "Geschaeftsfallnummer", namespace = "urn:omds3CommonServiceTypes-1-1-0", required = true)
    protected ObjektIdType geschaeftsfallnummer;
    @XmlElement(name = "VormaligeIdGeschaeftsfall")
    protected List<ObjektIdType> vormaligeIdGeschaeftsfall;
    @XmlElement(name = "NachfolgendeIdGeschaeftsfall")
    protected ObjektIdType nachfolgendeIdGeschaeftsfall;
    @XmlElement(name = "Schaeden")
    protected List<SchadenLightType> schaeden;
    @XmlElement(name = "Meldedat", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar meldedat;

    /**
     * Ruft den Wert der geschaeftsfallnummer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ObjektIdType }
     *     
     */
    public ObjektIdType getGeschaeftsfallnummer() {
        return geschaeftsfallnummer;
    }

    /**
     * Legt den Wert der geschaeftsfallnummer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjektIdType }
     *     
     */
    public void setGeschaeftsfallnummer(ObjektIdType value) {
        this.geschaeftsfallnummer = value;
    }

    /**
     * Gets the value of the vormaligeIdGeschaeftsfall property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the vormaligeIdGeschaeftsfall property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVormaligeIdGeschaeftsfall().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ObjektIdType }
     * 
     * 
     */
    public List<ObjektIdType> getVormaligeIdGeschaeftsfall() {
        if (vormaligeIdGeschaeftsfall == null) {
            vormaligeIdGeschaeftsfall = new ArrayList<ObjektIdType>();
        }
        return this.vormaligeIdGeschaeftsfall;
    }

    /**
     * Ruft den Wert der nachfolgendeIdGeschaeftsfall-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ObjektIdType }
     *     
     */
    public ObjektIdType getNachfolgendeIdGeschaeftsfall() {
        return nachfolgendeIdGeschaeftsfall;
    }

    /**
     * Legt den Wert der nachfolgendeIdGeschaeftsfall-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjektIdType }
     *     
     */
    public void setNachfolgendeIdGeschaeftsfall(ObjektIdType value) {
        this.nachfolgendeIdGeschaeftsfall = value;
    }

    /**
     * Gets the value of the schaeden property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the schaeden property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSchaeden().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SchadenLightType }
     * 
     * 
     */
    public List<SchadenLightType> getSchaeden() {
        if (schaeden == null) {
            schaeden = new ArrayList<SchadenLightType>();
        }
        return this.schaeden;
    }

    /**
     * Ruft den Wert der meldedat-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getMeldedat() {
        return meldedat;
    }

    /**
     * Legt den Wert der meldedat-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setMeldedat(XMLGregorianCalendar value) {
        this.meldedat = value;
    }

}
