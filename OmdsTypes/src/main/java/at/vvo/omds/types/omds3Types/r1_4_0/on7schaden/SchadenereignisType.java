
package at.vvo.omds.types.omds3Types.r1_4_0.on7schaden;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.ObjektIdType;


/**
 * Die Darstellung eines Schadenereignisses inkl. Ids
 * 
 * <p>Java-Klasse für Schadenereignis_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Schadenereignis_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}SchadenereignisAbstrakt_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:omds3CommonServiceTypes-1-1-0}Geschaeftsfallnummer"/&gt;
 *         &lt;element name="VorherigeSchadenmeldungen" type="{urn:omds3CommonServiceTypes-1-1-0}ObjektId_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="NachfolgendeSchadenmeldung" type="{urn:omds3CommonServiceTypes-1-1-0}ObjektId_Type" minOccurs="0"/&gt;
 *         &lt;element name="Schaeden" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}Schaden_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Schadenereignis_Type", propOrder = {
    "geschaeftsfallnummer",
    "vorherigeSchadenmeldungen",
    "nachfolgendeSchadenmeldung",
    "schaeden"
})
public class SchadenereignisType
    extends SchadenereignisAbstraktType
{

    @XmlElement(name = "Geschaeftsfallnummer", namespace = "urn:omds3CommonServiceTypes-1-1-0", required = true)
    protected ObjektIdType geschaeftsfallnummer;
    @XmlElement(name = "VorherigeSchadenmeldungen")
    protected List<ObjektIdType> vorherigeSchadenmeldungen;
    @XmlElement(name = "NachfolgendeSchadenmeldung")
    protected ObjektIdType nachfolgendeSchadenmeldung;
    @XmlElement(name = "Schaeden")
    protected List<SchadenType> schaeden;

    /**
     * Ruft den Wert der geschaeftsfallnummer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ObjektIdType }
     *     
     */
    public ObjektIdType getGeschaeftsfallnummer() {
        return geschaeftsfallnummer;
    }

    /**
     * Legt den Wert der geschaeftsfallnummer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjektIdType }
     *     
     */
    public void setGeschaeftsfallnummer(ObjektIdType value) {
        this.geschaeftsfallnummer = value;
    }

    /**
     * Gets the value of the vorherigeSchadenmeldungen property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the vorherigeSchadenmeldungen property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVorherigeSchadenmeldungen().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ObjektIdType }
     * 
     * 
     */
    public List<ObjektIdType> getVorherigeSchadenmeldungen() {
        if (vorherigeSchadenmeldungen == null) {
            vorherigeSchadenmeldungen = new ArrayList<ObjektIdType>();
        }
        return this.vorherigeSchadenmeldungen;
    }

    /**
     * Ruft den Wert der nachfolgendeSchadenmeldung-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ObjektIdType }
     *     
     */
    public ObjektIdType getNachfolgendeSchadenmeldung() {
        return nachfolgendeSchadenmeldung;
    }

    /**
     * Legt den Wert der nachfolgendeSchadenmeldung-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjektIdType }
     *     
     */
    public void setNachfolgendeSchadenmeldung(ObjektIdType value) {
        this.nachfolgendeSchadenmeldung = value;
    }

    /**
     * Gets the value of the schaeden property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the schaeden property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSchaeden().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SchadenType }
     * 
     * 
     */
    public List<SchadenType> getSchaeden() {
        if (schaeden == null) {
            schaeden = new ArrayList<SchadenType>();
        }
        return this.schaeden;
    }

}
