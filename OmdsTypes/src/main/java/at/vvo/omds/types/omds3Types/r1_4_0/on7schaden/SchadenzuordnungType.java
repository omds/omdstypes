
package at.vvo.omds.types.omds3Types.r1_4_0.on7schaden;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Typ für die Schadenzuordnung, welche eine vierstufige Systematik enthält, wobei die Schlüssel VU-spezifisch sind
 * 
 * <p>Java-Klasse für SchadenzuordnungType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SchadenzuordnungType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SchadenSparteCd" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="UrsacheCd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SpezifikationCd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ArtGeschaedigtesObjCd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SchadenzuordnungType", propOrder = {
    "schadenSparteCd",
    "ursacheCd",
    "spezifikationCd",
    "artGeschaedigtesObjCd"
})
public class SchadenzuordnungType {

    @XmlElement(name = "SchadenSparteCd", required = true)
    protected String schadenSparteCd;
    @XmlElement(name = "UrsacheCd")
    protected String ursacheCd;
    @XmlElement(name = "SpezifikationCd")
    protected String spezifikationCd;
    @XmlElement(name = "ArtGeschaedigtesObjCd")
    protected String artGeschaedigtesObjCd;

    /**
     * Ruft den Wert der schadenSparteCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSchadenSparteCd() {
        return schadenSparteCd;
    }

    /**
     * Legt den Wert der schadenSparteCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSchadenSparteCd(String value) {
        this.schadenSparteCd = value;
    }

    /**
     * Ruft den Wert der ursacheCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUrsacheCd() {
        return ursacheCd;
    }

    /**
     * Legt den Wert der ursacheCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUrsacheCd(String value) {
        this.ursacheCd = value;
    }

    /**
     * Ruft den Wert der spezifikationCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSpezifikationCd() {
        return spezifikationCd;
    }

    /**
     * Legt den Wert der spezifikationCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSpezifikationCd(String value) {
        this.spezifikationCd = value;
    }

    /**
     * Ruft den Wert der artGeschaedigtesObjCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArtGeschaedigtesObjCd() {
        return artGeschaedigtesObjCd;
    }

    /**
     * Legt den Wert der artGeschaedigtesObjCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArtGeschaedigtesObjCd(String value) {
        this.artGeschaedigtesObjCd = value;
    }

}
