
package at.vvo.omds.types.omds3Types.r1_4_0.on7schaden;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.AuthorizationFilter;
import at.vvo.omds.types.omds3Types.r1_4_0.common.ZeitraumType;


/**
 * Request-Type für die Suche nach einem Schaden
 * 
 * <p>Java-Klasse für SearchClaimRequest_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SearchClaimRequest_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="VUNr" type="{urn:omds20}VUNr"/&gt;
 *         &lt;element name="AuthFilter" type="{urn:omds3CommonServiceTypes-1-1-0}AuthorizationFilter" minOccurs="0"/&gt;
 *         &lt;element name="Suchbegriff" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Polizzennr" type="{urn:omds20}Polizzennr" minOccurs="0"/&gt;
 *         &lt;element name="Personennr" type="{urn:omds20}Personennr" minOccurs="0"/&gt;
 *         &lt;element name="Zeitraum" type="{urn:omds3CommonServiceTypes-1-1-0}Zeitraum_Type" minOccurs="0"/&gt;
 *         &lt;element name="MaxResults" type="{http://www.w3.org/2001/XMLSchema}unsignedInt"/&gt;
 *         &lt;element name="Offset" type="{http://www.w3.org/2001/XMLSchema}unsignedInt"/&gt;
 *         &lt;element name="OrderBy" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;enumeration value="Meldedatum aufsteigend"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchClaimRequest_Type", propOrder = {
    "vuNr",
    "authFilter",
    "suchbegriff",
    "polizzennr",
    "personennr",
    "zeitraum",
    "maxResults",
    "offset",
    "orderBy"
})
public class SearchClaimRequestType {

    @XmlElement(name = "VUNr", required = true)
    protected String vuNr;
    @XmlElement(name = "AuthFilter")
    protected AuthorizationFilter authFilter;
    @XmlElement(name = "Suchbegriff")
    protected String suchbegriff;
    @XmlElement(name = "Polizzennr")
    protected String polizzennr;
    @XmlElement(name = "Personennr")
    protected String personennr;
    @XmlElement(name = "Zeitraum")
    protected ZeitraumType zeitraum;
    @XmlElement(name = "MaxResults")
    @XmlSchemaType(name = "unsignedInt")
    protected long maxResults;
    @XmlElement(name = "Offset")
    @XmlSchemaType(name = "unsignedInt")
    protected long offset;
    @XmlElement(name = "OrderBy")
    protected String orderBy;

    /**
     * Ruft den Wert der vuNr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVUNr() {
        return vuNr;
    }

    /**
     * Legt den Wert der vuNr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVUNr(String value) {
        this.vuNr = value;
    }

    /**
     * Ruft den Wert der authFilter-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AuthorizationFilter }
     *     
     */
    public AuthorizationFilter getAuthFilter() {
        return authFilter;
    }

    /**
     * Legt den Wert der authFilter-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AuthorizationFilter }
     *     
     */
    public void setAuthFilter(AuthorizationFilter value) {
        this.authFilter = value;
    }

    /**
     * Ruft den Wert der suchbegriff-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSuchbegriff() {
        return suchbegriff;
    }

    /**
     * Legt den Wert der suchbegriff-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSuchbegriff(String value) {
        this.suchbegriff = value;
    }

    /**
     * Ruft den Wert der polizzennr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolizzennr() {
        return polizzennr;
    }

    /**
     * Legt den Wert der polizzennr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolizzennr(String value) {
        this.polizzennr = value;
    }

    /**
     * Ruft den Wert der personennr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPersonennr() {
        return personennr;
    }

    /**
     * Legt den Wert der personennr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPersonennr(String value) {
        this.personennr = value;
    }

    /**
     * Ruft den Wert der zeitraum-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ZeitraumType }
     *     
     */
    public ZeitraumType getZeitraum() {
        return zeitraum;
    }

    /**
     * Legt den Wert der zeitraum-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ZeitraumType }
     *     
     */
    public void setZeitraum(ZeitraumType value) {
        this.zeitraum = value;
    }

    /**
     * Ruft den Wert der maxResults-Eigenschaft ab.
     * 
     */
    public long getMaxResults() {
        return maxResults;
    }

    /**
     * Legt den Wert der maxResults-Eigenschaft fest.
     * 
     */
    public void setMaxResults(long value) {
        this.maxResults = value;
    }

    /**
     * Ruft den Wert der offset-Eigenschaft ab.
     * 
     */
    public long getOffset() {
        return offset;
    }

    /**
     * Legt den Wert der offset-Eigenschaft fest.
     * 
     */
    public void setOffset(long value) {
        this.offset = value;
    }

    /**
     * Ruft den Wert der orderBy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderBy() {
        return orderBy;
    }

    /**
     * Legt den Wert der orderBy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderBy(String value) {
        this.orderBy = value;
    }

}
