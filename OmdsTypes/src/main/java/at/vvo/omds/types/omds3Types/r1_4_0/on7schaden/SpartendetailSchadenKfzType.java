
package at.vvo.omds.types.omds3Types.r1_4_0.on7schaden;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Spartenerweiterung der Schadenmeldung für Kfz
 * 
 * <p>Java-Klasse für SpartendetailSchadenKfz_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SpartendetailSchadenKfz_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}SpartendetailSchaden_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="BehoerdlichAufgenommen" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="Kennzeichen" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="VerschuldenCd" type="{urn:omds20}VerschuldenCd_Type" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SpartendetailSchadenKfz_Type", propOrder = {
    "behoerdlichAufgenommen",
    "kennzeichen",
    "verschuldenCd"
})
public class SpartendetailSchadenKfzType
    extends SpartendetailSchadenType
{

    @XmlElement(name = "BehoerdlichAufgenommen")
    protected Boolean behoerdlichAufgenommen;
    @XmlElement(name = "Kennzeichen", required = true)
    protected String kennzeichen;
    @XmlElement(name = "VerschuldenCd")
    protected String verschuldenCd;

    /**
     * Ruft den Wert der behoerdlichAufgenommen-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isBehoerdlichAufgenommen() {
        return behoerdlichAufgenommen;
    }

    /**
     * Legt den Wert der behoerdlichAufgenommen-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setBehoerdlichAufgenommen(Boolean value) {
        this.behoerdlichAufgenommen = value;
    }

    /**
     * Ruft den Wert der kennzeichen-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKennzeichen() {
        return kennzeichen;
    }

    /**
     * Legt den Wert der kennzeichen-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKennzeichen(String value) {
        this.kennzeichen = value;
    }

    /**
     * Ruft den Wert der verschuldenCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVerschuldenCd() {
        return verschuldenCd;
    }

    /**
     * Legt den Wert der verschuldenCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVerschuldenCd(String value) {
        this.verschuldenCd = value;
    }

}
