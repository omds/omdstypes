
package at.vvo.omds.types.omds3Types.r1_4_0.on7schaden;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Spartenerweiterung der Schadenmeldung für Kranken
 * 
 * <p>Java-Klasse für SpartendetailSchadenKranken_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SpartendetailSchadenKranken_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}SpartendetailSchaden_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="BehandlerName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Behandlungen" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="BehandlungVon" type="{urn:omds20}Datum"/&gt;
 *                   &lt;element name="BehandlungBis" type="{urn:omds20}Datum" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SpartendetailSchadenKranken_Type", propOrder = {
    "behandlerName",
    "behandlungen"
})
public class SpartendetailSchadenKrankenType
    extends SpartendetailSchadenType
{

    @XmlElement(name = "BehandlerName", required = true)
    protected String behandlerName;
    @XmlElement(name = "Behandlungen")
    protected List<Behandlungen> behandlungen;

    /**
     * Ruft den Wert der behandlerName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBehandlerName() {
        return behandlerName;
    }

    /**
     * Legt den Wert der behandlerName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBehandlerName(String value) {
        this.behandlerName = value;
    }

    /**
     * Gets the value of the behandlungen property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the behandlungen property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBehandlungen().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Behandlungen }
     * 
     * 
     */
    public List<Behandlungen> getBehandlungen() {
        if (behandlungen == null) {
            behandlungen = new ArrayList<Behandlungen>();
        }
        return this.behandlungen;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="BehandlungVon" type="{urn:omds20}Datum"/&gt;
     *         &lt;element name="BehandlungBis" type="{urn:omds20}Datum" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "behandlungVon",
        "behandlungBis"
    })
    public static class Behandlungen {

        @XmlElement(name = "BehandlungVon", required = true)
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar behandlungVon;
        @XmlElement(name = "BehandlungBis")
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar behandlungBis;

        /**
         * Ruft den Wert der behandlungVon-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getBehandlungVon() {
            return behandlungVon;
        }

        /**
         * Legt den Wert der behandlungVon-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setBehandlungVon(XMLGregorianCalendar value) {
            this.behandlungVon = value;
        }

        /**
         * Ruft den Wert der behandlungBis-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getBehandlungBis() {
            return behandlungBis;
        }

        /**
         * Legt den Wert der behandlungBis-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setBehandlungBis(XMLGregorianCalendar value) {
            this.behandlungBis = value;
        }

    }

}
