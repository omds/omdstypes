
package at.vvo.omds.types.omds3Types.r1_4_0.on7schaden;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Spartenerweiterung der Schadenmeldung für Unfall
 * 
 * <p>Java-Klasse für SpartendetailSchadenUnfall_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SpartendetailSchadenUnfall_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}SpartendetailSchaden_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ArbeitsunfaehigVon" type="{urn:omds20}Datum" minOccurs="0"/&gt;
 *         &lt;element name="ArbeitsunfaehigBis" type="{urn:omds20}Datum" minOccurs="0"/&gt;
 *         &lt;element name="Diagnose" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="BehandlerName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SpartendetailSchadenUnfall_Type", propOrder = {
    "arbeitsunfaehigVon",
    "arbeitsunfaehigBis",
    "diagnose",
    "behandlerName"
})
public class SpartendetailSchadenUnfallType
    extends SpartendetailSchadenType
{

    @XmlElement(name = "ArbeitsunfaehigVon")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar arbeitsunfaehigVon;
    @XmlElement(name = "ArbeitsunfaehigBis")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar arbeitsunfaehigBis;
    @XmlElement(name = "Diagnose", required = true)
    protected String diagnose;
    @XmlElement(name = "BehandlerName")
    protected String behandlerName;

    /**
     * Ruft den Wert der arbeitsunfaehigVon-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getArbeitsunfaehigVon() {
        return arbeitsunfaehigVon;
    }

    /**
     * Legt den Wert der arbeitsunfaehigVon-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setArbeitsunfaehigVon(XMLGregorianCalendar value) {
        this.arbeitsunfaehigVon = value;
    }

    /**
     * Ruft den Wert der arbeitsunfaehigBis-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getArbeitsunfaehigBis() {
        return arbeitsunfaehigBis;
    }

    /**
     * Legt den Wert der arbeitsunfaehigBis-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setArbeitsunfaehigBis(XMLGregorianCalendar value) {
        this.arbeitsunfaehigBis = value;
    }

    /**
     * Ruft den Wert der diagnose-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDiagnose() {
        return diagnose;
    }

    /**
     * Legt den Wert der diagnose-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDiagnose(String value) {
        this.diagnose = value;
    }

    /**
     * Ruft den Wert der behandlerName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBehandlerName() {
        return behandlerName;
    }

    /**
     * Legt den Wert der behandlerName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBehandlerName(String value) {
        this.behandlerName = value;
    }

}
