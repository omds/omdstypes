
package at.vvo.omds.types.omds3Types.r1_4_0.on7schaden;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.AuthorizationFilter;
import at.vvo.omds.types.omds3Types.r1_4_0.common.CommonRequestType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.ObjektIdType;


/**
 * Dieser Typ enthält eine Schadennr oder eine GeschaeftsfallId
 * 
 * <p>Java-Klasse für SpezifikationSchaden_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SpezifikationSchaden_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}CommonRequest_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AuthFilter" type="{urn:omds3CommonServiceTypes-1-1-0}AuthorizationFilter" minOccurs="0"/&gt;
 *         &lt;choice&gt;
 *           &lt;element ref="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}IdGeschaeftsfallSchadenereignis"/&gt;
 *           &lt;element ref="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}IdGeschaeftsfallSchadenanlage"/&gt;
 *           &lt;element name="Schadennr" type="{urn:omds20}Schadennr"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SpezifikationSchaden_Type", propOrder = {
    "authFilter",
    "idGeschaeftsfallSchadenereignis",
    "idGeschaeftsfallSchadenanlage",
    "schadennr"
})
public class SpezifikationSchadenType
    extends CommonRequestType
{

    @XmlElement(name = "AuthFilter")
    protected AuthorizationFilter authFilter;
    @XmlElement(name = "IdGeschaeftsfallSchadenereignis")
    protected ObjektIdType idGeschaeftsfallSchadenereignis;
    @XmlElement(name = "IdGeschaeftsfallSchadenanlage")
    protected ObjektIdType idGeschaeftsfallSchadenanlage;
    @XmlElement(name = "Schadennr")
    protected String schadennr;

    /**
     * Ruft den Wert der authFilter-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AuthorizationFilter }
     *     
     */
    public AuthorizationFilter getAuthFilter() {
        return authFilter;
    }

    /**
     * Legt den Wert der authFilter-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AuthorizationFilter }
     *     
     */
    public void setAuthFilter(AuthorizationFilter value) {
        this.authFilter = value;
    }

    /**
     * Ruft den Wert der idGeschaeftsfallSchadenereignis-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ObjektIdType }
     *     
     */
    public ObjektIdType getIdGeschaeftsfallSchadenereignis() {
        return idGeschaeftsfallSchadenereignis;
    }

    /**
     * Legt den Wert der idGeschaeftsfallSchadenereignis-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjektIdType }
     *     
     */
    public void setIdGeschaeftsfallSchadenereignis(ObjektIdType value) {
        this.idGeschaeftsfallSchadenereignis = value;
    }

    /**
     * Ruft den Wert der idGeschaeftsfallSchadenanlage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ObjektIdType }
     *     
     */
    public ObjektIdType getIdGeschaeftsfallSchadenanlage() {
        return idGeschaeftsfallSchadenanlage;
    }

    /**
     * Legt den Wert der idGeschaeftsfallSchadenanlage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjektIdType }
     *     
     */
    public void setIdGeschaeftsfallSchadenanlage(ObjektIdType value) {
        this.idGeschaeftsfallSchadenanlage = value;
    }

    /**
     * Ruft den Wert der schadennr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSchadennr() {
        return schadennr;
    }

    /**
     * Legt den Wert der schadennr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSchadennr(String value) {
        this.schadennr = value;
    }

}
