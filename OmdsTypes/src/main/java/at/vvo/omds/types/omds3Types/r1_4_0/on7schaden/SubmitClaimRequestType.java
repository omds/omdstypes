
package at.vvo.omds.types.omds3Types.r1_4_0.on7schaden;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.CommonRequestType;
import at.vvo.omds.types.omds3Types.r1_4_0.common.ObjektIdType;


/**
 * Typ für die Durchführung einer Schadenmeldung
 * 
 * <p>Java-Klasse für SubmitClaimRequest_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SubmitClaimRequest_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}CommonRequest_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:omds3CommonServiceTypes-1-1-0}Geschaeftsfallnummer" minOccurs="0"/&gt;
 *         &lt;element name="Schadenereignis" type="{urn:at.vvo.omds.types.omds3types.v1-4-0.on7schaden}MeldungSchadenereignis_Type" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubmitClaimRequest_Type", propOrder = {
    "geschaeftsfallnummer",
    "schadenereignis"
})
public class SubmitClaimRequestType
    extends CommonRequestType
{

    @XmlElement(name = "Geschaeftsfallnummer", namespace = "urn:omds3CommonServiceTypes-1-1-0")
    protected ObjektIdType geschaeftsfallnummer;
    @XmlElement(name = "Schadenereignis")
    protected MeldungSchadenereignisType schadenereignis;

    /**
     * Ruft den Wert der geschaeftsfallnummer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ObjektIdType }
     *     
     */
    public ObjektIdType getGeschaeftsfallnummer() {
        return geschaeftsfallnummer;
    }

    /**
     * Legt den Wert der geschaeftsfallnummer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjektIdType }
     *     
     */
    public void setGeschaeftsfallnummer(ObjektIdType value) {
        this.geschaeftsfallnummer = value;
    }

    /**
     * Ruft den Wert der schadenereignis-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MeldungSchadenereignisType }
     *     
     */
    public MeldungSchadenereignisType getSchadenereignis() {
        return schadenereignis;
    }

    /**
     * Legt den Wert der schadenereignis-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MeldungSchadenereignisType }
     *     
     */
    public void setSchadenereignis(MeldungSchadenereignisType value) {
        this.schadenereignis = value;
    }

}
