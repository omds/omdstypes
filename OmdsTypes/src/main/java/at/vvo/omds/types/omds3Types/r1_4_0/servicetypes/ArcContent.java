
package at.vvo.omds.types.omds3Types.r1_4_0.servicetypes;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Informationen zu einem Dokument und das Dokument base64encoded
 * 
 * <p>Java-Klasse für ArcContent complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ArcContent"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="arcImageInfo" type="{urn:omds3ServiceTypes-1-1-0}ArcImageInfo"/&gt;
 *         &lt;element name="arcImage" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArcContent", propOrder = {
    "arcImageInfo",
    "arcImage"
})
public class ArcContent {

    @XmlElement(required = true)
    protected ArcImageInfo arcImageInfo;
    @XmlElement(required = true)
    protected byte[] arcImage;

    /**
     * Ruft den Wert der arcImageInfo-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ArcImageInfo }
     *     
     */
    public ArcImageInfo getArcImageInfo() {
        return arcImageInfo;
    }

    /**
     * Legt den Wert der arcImageInfo-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ArcImageInfo }
     *     
     */
    public void setArcImageInfo(ArcImageInfo value) {
        this.arcImageInfo = value;
    }

    /**
     * Ruft den Wert der arcImage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getArcImage() {
        return arcImage;
    }

    /**
     * Legt den Wert der arcImage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setArcImage(byte[] value) {
        this.arcImage = value;
    }

}
