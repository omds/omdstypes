
package at.vvo.omds.types.omds3Types.v1_0_1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * Ein spezieller AuthorizationFilterType, der eine Anfrage dahingehend einschränkt, dass nur Ergebnisse für eine bestimmte agentID (MaklerID) oder agentNumber (Vermittlernr) zurück gegeben werden
 * 
 * <p>Java-Klasse für AgentFilter complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="AgentFilter"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omdsServiceTypes}AuthorizationFilter"&gt;
 *       &lt;choice&gt;
 *         &lt;element ref="{urn:omdsServiceTypes}agentID"/&gt;
 *         &lt;element ref="{urn:omdsServiceTypes}agentNumber" maxOccurs="unbounded"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AgentFilter", propOrder = {
    "agentID",
    "agentNumber"
})
public class AgentFilter
    extends AuthorizationFilter
{

    protected String agentID;
    protected List<String> agentNumber;

    /**
     * Ruft den Wert der agentID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgentID() {
        return agentID;
    }

    /**
     * Legt den Wert der agentID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgentID(String value) {
        this.agentID = value;
    }

    /**
     * Gets the value of the agentNumber property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the agentNumber property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAgentNumber().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getAgentNumber() {
        if (agentNumber == null) {
            agentNumber = new ArrayList<String>();
        }
        return this.agentNumber;
    }

}
