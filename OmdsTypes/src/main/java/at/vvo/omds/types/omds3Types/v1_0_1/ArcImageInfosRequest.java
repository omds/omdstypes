
package at.vvo.omds.types.omds3Types.v1_0_1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java-Klasse für ArcImageInfosRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ArcImageInfosRequest"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:omdsServiceTypes}vuNr" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:omdsServiceTypes}authFilter" minOccurs="0"/&gt;
 *         &lt;element name="policyNumber" type="{urn:omds20}Polizzennr"/&gt;
 *         &lt;element name="policyType" type="{urn:omds20}VtgProdCd_Type" minOccurs="0"/&gt;
 *         &lt;element name="policyPartyRole" type="{urn:omdsServiceTypes}PolicyPartnerRole" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="documentType" type="{urn:omdsServiceTypes}PolicyDocumentType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="dateFrom" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="dateUntil" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArcImageInfosRequest", propOrder = {
    "vuNr",
    "authFilter",
    "policyNumber",
    "policyType",
    "policyPartyRole",
    "documentType",
    "dateFrom",
    "dateUntil"
})
public class ArcImageInfosRequest {

    protected String vuNr;
    protected AuthorizationFilter authFilter;
    @XmlElement(required = true)
    protected String policyNumber;
    protected String policyType;
    protected List<PolicyPartnerRole> policyPartyRole;
    @XmlElement(type = Integer.class)
    protected List<Integer> documentType;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dateFrom;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dateUntil;

    /**
     * Ruft den Wert der vuNr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVuNr() {
        return vuNr;
    }

    /**
     * Legt den Wert der vuNr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVuNr(String value) {
        this.vuNr = value;
    }

    /**
     * ermöglicht "Super-Usern" die Abfrage von spezifischen Elementen
     * 
     * @return
     *     possible object is
     *     {@link AuthorizationFilter }
     *     
     */
    public AuthorizationFilter getAuthFilter() {
        return authFilter;
    }

    /**
     * Legt den Wert der authFilter-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AuthorizationFilter }
     *     
     */
    public void setAuthFilter(AuthorizationFilter value) {
        this.authFilter = value;
    }

    /**
     * Ruft den Wert der policyNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolicyNumber() {
        return policyNumber;
    }

    /**
     * Legt den Wert der policyNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolicyNumber(String value) {
        this.policyNumber = value;
    }

    /**
     * Ruft den Wert der policyType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolicyType() {
        return policyType;
    }

    /**
     * Legt den Wert der policyType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolicyType(String value) {
        this.policyType = value;
    }

    /**
     * Gets the value of the policyPartyRole property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the policyPartyRole property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPolicyPartyRole().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PolicyPartnerRole }
     * 
     * 
     */
    public List<PolicyPartnerRole> getPolicyPartyRole() {
        if (policyPartyRole == null) {
            policyPartyRole = new ArrayList<PolicyPartnerRole>();
        }
        return this.policyPartyRole;
    }

    /**
     * Gets the value of the documentType property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the documentType property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDocumentType().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Integer }
     * 
     * 
     */
    public List<Integer> getDocumentType() {
        if (documentType == null) {
            documentType = new ArrayList<Integer>();
        }
        return this.documentType;
    }

    /**
     * Ruft den Wert der dateFrom-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateFrom() {
        return dateFrom;
    }

    /**
     * Legt den Wert der dateFrom-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateFrom(XMLGregorianCalendar value) {
        this.dateFrom = value;
    }

    /**
     * Ruft den Wert der dateUntil-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateUntil() {
        return dateUntil;
    }

    /**
     * Legt den Wert der dateUntil-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateUntil(XMLGregorianCalendar value) {
        this.dateUntil = value;
    }

}
