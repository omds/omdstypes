
package at.vvo.omds.types.omds3Types.v1_0_1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für LoginRequestType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="LoginRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:omdsServiceTypes}vuNr" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:omdsServiceTypes}authFilter" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LoginRequestType", propOrder = {
    "vuNr",
    "authFilter"
})
public class LoginRequestType {

    protected String vuNr;
    protected AuthorizationFilter authFilter;

    /**
     * Ruft den Wert der vuNr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVuNr() {
        return vuNr;
    }

    /**
     * Legt den Wert der vuNr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVuNr(String value) {
        this.vuNr = value;
    }

    /**
     * Ruft den Wert der authFilter-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AuthorizationFilter }
     *     
     */
    public AuthorizationFilter getAuthFilter() {
        return authFilter;
    }

    /**
     * Legt den Wert der authFilter-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AuthorizationFilter }
     *     
     */
    public void setAuthFilter(AuthorizationFilter value) {
        this.authFilter = value;
    }

}
