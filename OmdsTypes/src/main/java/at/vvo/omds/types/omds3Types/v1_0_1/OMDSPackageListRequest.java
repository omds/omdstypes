
package at.vvo.omds.types.omds3Types.v1_0_1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java-Klasse für OMDSPackageListRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="OMDSPackageListRequest"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:omdsServiceTypes}vuNr" minOccurs="0"/&gt;
 *         &lt;element name="agentFilter" type="{urn:omdsServiceTypes}AgentFilter" minOccurs="0"/&gt;
 *         &lt;element name="dateFrom" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="dateUntil" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OMDSPackageListRequest", propOrder = {
    "vuNr",
    "agentFilter",
    "dateFrom",
    "dateUntil"
})
public class OMDSPackageListRequest {

    protected String vuNr;
    protected AgentFilter agentFilter;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dateFrom;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dateUntil;

    /**
     * die VUNr aus OMDS als optionaler Filterparameter
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVuNr() {
        return vuNr;
    }

    /**
     * Legt den Wert der vuNr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVuNr(String value) {
        this.vuNr = value;
    }

    /**
     * Ruft den Wert der agentFilter-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AgentFilter }
     *     
     */
    public AgentFilter getAgentFilter() {
        return agentFilter;
    }

    /**
     * Legt den Wert der agentFilter-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AgentFilter }
     *     
     */
    public void setAgentFilter(AgentFilter value) {
        this.agentFilter = value;
    }

    /**
     * Ruft den Wert der dateFrom-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateFrom() {
        return dateFrom;
    }

    /**
     * Legt den Wert der dateFrom-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateFrom(XMLGregorianCalendar value) {
        this.dateFrom = value;
    }

    /**
     * Ruft den Wert der dateUntil-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateUntil() {
        return dateUntil;
    }

    /**
     * Legt den Wert der dateUntil-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateUntil(XMLGregorianCalendar value) {
        this.dateUntil = value;
    }

}
