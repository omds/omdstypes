
package at.vvo.omds.types.omds3Types.v1_0_1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the at.vvo.omds.types.omds30Types package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _LoginHeader_QNAME = new QName("urn:omdsServiceTypes", "loginHeader");
    private final static QName _ClientID_QNAME = new QName("urn:omdsServiceTypes", "ClientID");
    private final static QName _LoginRequest_QNAME = new QName("urn:omdsServiceTypes", "loginRequest");
    private final static QName _LoginResponse_QNAME = new QName("urn:omdsServiceTypes", "loginResponse");
    private final static QName _GetUserDataRequest_QNAME = new QName("urn:omdsServiceTypes", "getUserDataRequest");
    private final static QName _GetUserDataResponse_QNAME = new QName("urn:omdsServiceTypes", "getUserDataResponse");
    private final static QName _GetOMDSPackageListRequest_QNAME = new QName("urn:omdsServiceTypes", "getOMDSPackageListRequest");
    private final static QName _GetOMDSPackageListResponse_QNAME = new QName("urn:omdsServiceTypes", "getOMDSPackageListResponse");
    private final static QName _GetOMDSPackageRequest_QNAME = new QName("urn:omdsServiceTypes", "getOMDSPackageRequest");
    private final static QName _GetOMDSPackageResponse_QNAME = new QName("urn:omdsServiceTypes", "getOMDSPackageResponse");
    private final static QName _ServiceFault_QNAME = new QName("urn:omdsServiceTypes", "serviceFault");
    private final static QName _GetArcImageInfosRequest_QNAME = new QName("urn:omdsServiceTypes", "getArcImageInfosRequest");
    private final static QName _GetArcImageInfosResponse_QNAME = new QName("urn:omdsServiceTypes", "getArcImageInfosResponse");
    private final static QName _GetArcImageRequest_QNAME = new QName("urn:omdsServiceTypes", "getArcImageRequest");
    private final static QName _GetArcImageResponse_QNAME = new QName("urn:omdsServiceTypes", "getArcImageResponse");
    private final static QName _GetDeepLinkClaimRequest_QNAME = new QName("urn:omdsServiceTypes", "getDeepLinkClaimRequest");
    private final static QName _GetDeepLinkClaimResponse_QNAME = new QName("urn:omdsServiceTypes", "getDeepLinkClaimResponse");
    private final static QName _GetDeepLinkPartnerRequest_QNAME = new QName("urn:omdsServiceTypes", "getDeepLinkPartnerRequest");
    private final static QName _GetDeepLinkPartnerResponse_QNAME = new QName("urn:omdsServiceTypes", "getDeepLinkPartnerResponse");
    private final static QName _GetDeepLinkOfferRequest_QNAME = new QName("urn:omdsServiceTypes", "getDeepLinkOfferRequest");
    private final static QName _GetDeepLinkOfferResponse_QNAME = new QName("urn:omdsServiceTypes", "getDeepLinkOfferResponse");
    private final static QName _GetDeepLinkPolicyRequest_QNAME = new QName("urn:omdsServiceTypes", "getDeepLinkPolicyRequest");
    private final static QName _GetDeepLinkPolicyResponse_QNAME = new QName("urn:omdsServiceTypes", "getDeepLinkPolicyResponse");
    private final static QName _GetDeepLinkBusinessObjectResponse_QNAME = new QName("urn:omdsServiceTypes", "getDeepLinkBusinessObjectResponse");
    private final static QName _WithoutFrame_QNAME = new QName("urn:omdsServiceTypes", "withoutFrame");
    private final static QName _HttpActionLink_QNAME = new QName("urn:omdsServiceTypes", "httpActionLink");
    private final static QName _VuNr_QNAME = new QName("urn:omdsServiceTypes", "vuNr");
    private final static QName _AgentID_QNAME = new QName("urn:omdsServiceTypes", "agentID");
    private final static QName _AgentNumber_QNAME = new QName("urn:omdsServiceTypes", "agentNumber");
    private final static QName _AuthFilter_QNAME = new QName("urn:omdsServiceTypes", "authFilter");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: at.vvo.omds.types.omds30Types
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link UserDataResponse }
     * 
     */
    public UserDataResponse createUserDataResponse() {
        return new UserDataResponse();
    }

    /**
     * Create an instance of {@link RequestedOMDSPackage }
     * 
     */
    public RequestedOMDSPackage createRequestedOMDSPackage() {
        return new RequestedOMDSPackage();
    }

    /**
     * Create an instance of {@link Login }
     * 
     */
    public Login createLogin() {
        return new Login();
    }

    /**
     * Create an instance of {@link LoginRequestType }
     * 
     */
    public LoginRequestType createLoginRequestType() {
        return new LoginRequestType();
    }

    /**
     * Create an instance of {@link DeepLinkBusinessObjectResponse }
     * 
     */
    public DeepLinkBusinessObjectResponse createDeepLinkBusinessObjectResponse() {
        return new DeepLinkBusinessObjectResponse();
    }

    /**
     * Create an instance of {@link UserDataRequest }
     * 
     */
    public UserDataRequest createUserDataRequest() {
        return new UserDataRequest();
    }

    /**
     * Create an instance of {@link OMDSPackageListRequest }
     * 
     */
    public OMDSPackageListRequest createOMDSPackageListRequest() {
        return new OMDSPackageListRequest();
    }

    /**
     * Create an instance of {@link OMDSPackageListResponse }
     * 
     */
    public OMDSPackageListResponse createOMDSPackageListResponse() {
        return new OMDSPackageListResponse();
    }

    /**
     * Create an instance of {@link OMDSPackageRequest }
     * 
     */
    public OMDSPackageRequest createOMDSPackageRequest() {
        return new OMDSPackageRequest();
    }

    /**
     * Create an instance of {@link OMDSPackageResponse }
     * 
     */
    public OMDSPackageResponse createOMDSPackageResponse() {
        return new OMDSPackageResponse();
    }

    /**
     * Create an instance of {@link ServiceFault }
     * 
     */
    public ServiceFault createServiceFault() {
        return new ServiceFault();
    }

    /**
     * Create an instance of {@link ArcImageInfosRequest }
     * 
     */
    public ArcImageInfosRequest createArcImageInfosRequest() {
        return new ArcImageInfosRequest();
    }

    /**
     * Create an instance of {@link ArcImageInfosResponse }
     * 
     */
    public ArcImageInfosResponse createArcImageInfosResponse() {
        return new ArcImageInfosResponse();
    }

    /**
     * Create an instance of {@link ArcImageRequest }
     * 
     */
    public ArcImageRequest createArcImageRequest() {
        return new ArcImageRequest();
    }

    /**
     * Create an instance of {@link ArcImageResponse }
     * 
     */
    public ArcImageResponse createArcImageResponse() {
        return new ArcImageResponse();
    }

    /**
     * Create an instance of {@link DeepLinkClaimRequest }
     * 
     */
    public DeepLinkClaimRequest createDeepLinkClaimRequest() {
        return new DeepLinkClaimRequest();
    }

    /**
     * Create an instance of {@link DeepLinkPartnerRequest }
     * 
     */
    public DeepLinkPartnerRequest createDeepLinkPartnerRequest() {
        return new DeepLinkPartnerRequest();
    }

    /**
     * Create an instance of {@link DeepLinkOfferRequest }
     * 
     */
    public DeepLinkOfferRequest createDeepLinkOfferRequest() {
        return new DeepLinkOfferRequest();
    }

    /**
     * Create an instance of {@link DeepLinkPolicyRequest }
     * 
     */
    public DeepLinkPolicyRequest createDeepLinkPolicyRequest() {
        return new DeepLinkPolicyRequest();
    }

    /**
     * Create an instance of {@link HttpActionLinkType }
     * 
     */
    public HttpActionLinkType createHttpActionLinkType() {
        return new HttpActionLinkType();
    }

    /**
     * Create an instance of {@link LoginWSSEType }
     * 
     */
    public LoginWSSEType createLoginWSSEType() {
        return new LoginWSSEType();
    }

    /**
     * Create an instance of {@link OMDSPackageInfoType }
     * 
     */
    public OMDSPackageInfoType createOMDSPackageInfoType() {
        return new OMDSPackageInfoType();
    }

    /**
     * Create an instance of {@link PolicyPartnerRole }
     * 
     */
    public PolicyPartnerRole createPolicyPartnerRole() {
        return new PolicyPartnerRole();
    }

    /**
     * Create an instance of {@link ArcImageInfo }
     * 
     */
    public ArcImageInfo createArcImageInfo() {
        return new ArcImageInfo();
    }

    /**
     * Create an instance of {@link ArcContent }
     * 
     */
    public ArcContent createArcContent() {
        return new ArcContent();
    }

    /**
     * Create an instance of {@link AgentFilter }
     * 
     */
    public AgentFilter createAgentFilter() {
        return new AgentFilter();
    }

    /**
     * Create an instance of {@link ElementIdType }
     * 
     */
    public ElementIdType createElementIdType() {
        return new ElementIdType();
    }

    /**
     * Create an instance of {@link UserDataResponse.AvailableServices }
     * 
     */
    public UserDataResponse.AvailableServices createUserDataResponseAvailableServices() {
        return new UserDataResponse.AvailableServices();
    }

    /**
     * Create an instance of {@link RequestedOMDSPackage.OmdsPackage }
     * 
     */
    public RequestedOMDSPackage.OmdsPackage createRequestedOMDSPackageOmdsPackage() {
        return new RequestedOMDSPackage.OmdsPackage();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Login }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omdsServiceTypes", name = "loginHeader")
    public JAXBElement<Login> createLoginHeader(Login value) {
        return new JAXBElement<Login>(_LoginHeader_QNAME, Login.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omdsServiceTypes", name = "ClientID")
    public JAXBElement<String> createClientID(String value) {
        return new JAXBElement<String>(_ClientID_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoginRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omdsServiceTypes", name = "loginRequest")
    public JAXBElement<LoginRequestType> createLoginRequest(LoginRequestType value) {
        return new JAXBElement<LoginRequestType>(_LoginRequest_QNAME, LoginRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeepLinkBusinessObjectResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omdsServiceTypes", name = "loginResponse")
    public JAXBElement<DeepLinkBusinessObjectResponse> createLoginResponse(DeepLinkBusinessObjectResponse value) {
        return new JAXBElement<DeepLinkBusinessObjectResponse>(_LoginResponse_QNAME, DeepLinkBusinessObjectResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UserDataRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omdsServiceTypes", name = "getUserDataRequest")
    public JAXBElement<UserDataRequest> createGetUserDataRequest(UserDataRequest value) {
        return new JAXBElement<UserDataRequest>(_GetUserDataRequest_QNAME, UserDataRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UserDataResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omdsServiceTypes", name = "getUserDataResponse")
    public JAXBElement<UserDataResponse> createGetUserDataResponse(UserDataResponse value) {
        return new JAXBElement<UserDataResponse>(_GetUserDataResponse_QNAME, UserDataResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OMDSPackageListRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omdsServiceTypes", name = "getOMDSPackageListRequest")
    public JAXBElement<OMDSPackageListRequest> createGetOMDSPackageListRequest(OMDSPackageListRequest value) {
        return new JAXBElement<OMDSPackageListRequest>(_GetOMDSPackageListRequest_QNAME, OMDSPackageListRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OMDSPackageListResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omdsServiceTypes", name = "getOMDSPackageListResponse")
    public JAXBElement<OMDSPackageListResponse> createGetOMDSPackageListResponse(OMDSPackageListResponse value) {
        return new JAXBElement<OMDSPackageListResponse>(_GetOMDSPackageListResponse_QNAME, OMDSPackageListResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OMDSPackageRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omdsServiceTypes", name = "getOMDSPackageRequest")
    public JAXBElement<OMDSPackageRequest> createGetOMDSPackageRequest(OMDSPackageRequest value) {
        return new JAXBElement<OMDSPackageRequest>(_GetOMDSPackageRequest_QNAME, OMDSPackageRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OMDSPackageResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omdsServiceTypes", name = "getOMDSPackageResponse")
    public JAXBElement<OMDSPackageResponse> createGetOMDSPackageResponse(OMDSPackageResponse value) {
        return new JAXBElement<OMDSPackageResponse>(_GetOMDSPackageResponse_QNAME, OMDSPackageResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ServiceFault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omdsServiceTypes", name = "serviceFault")
    public JAXBElement<ServiceFault> createServiceFault(ServiceFault value) {
        return new JAXBElement<ServiceFault>(_ServiceFault_QNAME, ServiceFault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArcImageInfosRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omdsServiceTypes", name = "getArcImageInfosRequest")
    public JAXBElement<ArcImageInfosRequest> createGetArcImageInfosRequest(ArcImageInfosRequest value) {
        return new JAXBElement<ArcImageInfosRequest>(_GetArcImageInfosRequest_QNAME, ArcImageInfosRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArcImageInfosResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omdsServiceTypes", name = "getArcImageInfosResponse")
    public JAXBElement<ArcImageInfosResponse> createGetArcImageInfosResponse(ArcImageInfosResponse value) {
        return new JAXBElement<ArcImageInfosResponse>(_GetArcImageInfosResponse_QNAME, ArcImageInfosResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArcImageRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omdsServiceTypes", name = "getArcImageRequest")
    public JAXBElement<ArcImageRequest> createGetArcImageRequest(ArcImageRequest value) {
        return new JAXBElement<ArcImageRequest>(_GetArcImageRequest_QNAME, ArcImageRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArcImageResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omdsServiceTypes", name = "getArcImageResponse")
    public JAXBElement<ArcImageResponse> createGetArcImageResponse(ArcImageResponse value) {
        return new JAXBElement<ArcImageResponse>(_GetArcImageResponse_QNAME, ArcImageResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeepLinkClaimRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omdsServiceTypes", name = "getDeepLinkClaimRequest")
    public JAXBElement<DeepLinkClaimRequest> createGetDeepLinkClaimRequest(DeepLinkClaimRequest value) {
        return new JAXBElement<DeepLinkClaimRequest>(_GetDeepLinkClaimRequest_QNAME, DeepLinkClaimRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeepLinkBusinessObjectResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omdsServiceTypes", name = "getDeepLinkClaimResponse")
    public JAXBElement<DeepLinkBusinessObjectResponse> createGetDeepLinkClaimResponse(DeepLinkBusinessObjectResponse value) {
        return new JAXBElement<DeepLinkBusinessObjectResponse>(_GetDeepLinkClaimResponse_QNAME, DeepLinkBusinessObjectResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeepLinkPartnerRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omdsServiceTypes", name = "getDeepLinkPartnerRequest")
    public JAXBElement<DeepLinkPartnerRequest> createGetDeepLinkPartnerRequest(DeepLinkPartnerRequest value) {
        return new JAXBElement<DeepLinkPartnerRequest>(_GetDeepLinkPartnerRequest_QNAME, DeepLinkPartnerRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeepLinkBusinessObjectResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omdsServiceTypes", name = "getDeepLinkPartnerResponse")
    public JAXBElement<DeepLinkBusinessObjectResponse> createGetDeepLinkPartnerResponse(DeepLinkBusinessObjectResponse value) {
        return new JAXBElement<DeepLinkBusinessObjectResponse>(_GetDeepLinkPartnerResponse_QNAME, DeepLinkBusinessObjectResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeepLinkOfferRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omdsServiceTypes", name = "getDeepLinkOfferRequest")
    public JAXBElement<DeepLinkOfferRequest> createGetDeepLinkOfferRequest(DeepLinkOfferRequest value) {
        return new JAXBElement<DeepLinkOfferRequest>(_GetDeepLinkOfferRequest_QNAME, DeepLinkOfferRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeepLinkBusinessObjectResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omdsServiceTypes", name = "getDeepLinkOfferResponse")
    public JAXBElement<DeepLinkBusinessObjectResponse> createGetDeepLinkOfferResponse(DeepLinkBusinessObjectResponse value) {
        return new JAXBElement<DeepLinkBusinessObjectResponse>(_GetDeepLinkOfferResponse_QNAME, DeepLinkBusinessObjectResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeepLinkPolicyRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omdsServiceTypes", name = "getDeepLinkPolicyRequest")
    public JAXBElement<DeepLinkPolicyRequest> createGetDeepLinkPolicyRequest(DeepLinkPolicyRequest value) {
        return new JAXBElement<DeepLinkPolicyRequest>(_GetDeepLinkPolicyRequest_QNAME, DeepLinkPolicyRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeepLinkBusinessObjectResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omdsServiceTypes", name = "getDeepLinkPolicyResponse")
    public JAXBElement<DeepLinkBusinessObjectResponse> createGetDeepLinkPolicyResponse(DeepLinkBusinessObjectResponse value) {
        return new JAXBElement<DeepLinkBusinessObjectResponse>(_GetDeepLinkPolicyResponse_QNAME, DeepLinkBusinessObjectResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeepLinkBusinessObjectResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omdsServiceTypes", name = "getDeepLinkBusinessObjectResponse")
    public JAXBElement<DeepLinkBusinessObjectResponse> createGetDeepLinkBusinessObjectResponse(DeepLinkBusinessObjectResponse value) {
        return new JAXBElement<DeepLinkBusinessObjectResponse>(_GetDeepLinkBusinessObjectResponse_QNAME, DeepLinkBusinessObjectResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omdsServiceTypes", name = "withoutFrame")
    public JAXBElement<Boolean> createWithoutFrame(Boolean value) {
        return new JAXBElement<Boolean>(_WithoutFrame_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HttpActionLinkType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omdsServiceTypes", name = "httpActionLink")
    public JAXBElement<HttpActionLinkType> createHttpActionLink(HttpActionLinkType value) {
        return new JAXBElement<HttpActionLinkType>(_HttpActionLink_QNAME, HttpActionLinkType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omdsServiceTypes", name = "vuNr")
    public JAXBElement<String> createVuNr(String value) {
        return new JAXBElement<String>(_VuNr_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omdsServiceTypes", name = "agentID")
    public JAXBElement<String> createAgentID(String value) {
        return new JAXBElement<String>(_AgentID_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omdsServiceTypes", name = "agentNumber")
    public JAXBElement<String> createAgentNumber(String value) {
        return new JAXBElement<String>(_AgentNumber_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AuthorizationFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omdsServiceTypes", name = "authFilter")
    public JAXBElement<AuthorizationFilter> createAuthFilter(AuthorizationFilter value) {
        return new JAXBElement<AuthorizationFilter>(_AuthFilter_QNAME, AuthorizationFilter.class, null, value);
    }

}
