
package at.vvo.omds.types.omds3Types.v1_0_1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Typ fuer ein OMDS-Datensatz-Package
 * 
 * <p>Java-Klasse für RequestedOMDSPackage complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="RequestedOMDSPackage"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="requestedOmdsPackageId"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;minLength value="1"/&gt;
 *               &lt;maxLength value="1024"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="omdsPackage" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="omdsPackageInfo" type="{urn:omdsServiceTypes}OMDSPackageInfoType"/&gt;
 *                   &lt;element name="content" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="serviceFault" type="{urn:omdsServiceTypes}ServiceFault" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RequestedOMDSPackage", propOrder = {
    "requestedOmdsPackageId",
    "omdsPackage",
    "serviceFault"
})
public class RequestedOMDSPackage {

    @XmlElement(required = true)
    protected String requestedOmdsPackageId;
    protected RequestedOMDSPackage.OmdsPackage omdsPackage;
    protected List<ServiceFault> serviceFault;

    /**
     * Ruft den Wert der requestedOmdsPackageId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestedOmdsPackageId() {
        return requestedOmdsPackageId;
    }

    /**
     * Legt den Wert der requestedOmdsPackageId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestedOmdsPackageId(String value) {
        this.requestedOmdsPackageId = value;
    }

    /**
     * Ruft den Wert der omdsPackage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link RequestedOMDSPackage.OmdsPackage }
     *     
     */
    public RequestedOMDSPackage.OmdsPackage getOmdsPackage() {
        return omdsPackage;
    }

    /**
     * Legt den Wert der omdsPackage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestedOMDSPackage.OmdsPackage }
     *     
     */
    public void setOmdsPackage(RequestedOMDSPackage.OmdsPackage value) {
        this.omdsPackage = value;
    }

    /**
     * Gets the value of the serviceFault property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the serviceFault property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getServiceFault().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ServiceFault }
     * 
     * 
     */
    public List<ServiceFault> getServiceFault() {
        if (serviceFault == null) {
            serviceFault = new ArrayList<ServiceFault>();
        }
        return this.serviceFault;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="omdsPackageInfo" type="{urn:omdsServiceTypes}OMDSPackageInfoType"/&gt;
     *         &lt;element name="content" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "omdsPackageInfo",
        "content"
    })
    public static class OmdsPackage {

        @XmlElement(required = true)
        protected OMDSPackageInfoType omdsPackageInfo;
        @XmlElement(required = true)
        protected byte[] content;

        /**
         * Ruft den Wert der omdsPackageInfo-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link OMDSPackageInfoType }
         *     
         */
        public OMDSPackageInfoType getOmdsPackageInfo() {
            return omdsPackageInfo;
        }

        /**
         * Legt den Wert der omdsPackageInfo-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link OMDSPackageInfoType }
         *     
         */
        public void setOmdsPackageInfo(OMDSPackageInfoType value) {
            this.omdsPackageInfo = value;
        }

        /**
         * Ruft den Wert der content-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     byte[]
         */
        public byte[] getContent() {
            return content;
        }

        /**
         * Legt den Wert der content-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     byte[]
         */
        public void setContent(byte[] value) {
            this.content = value;
        }

    }

}
