
package at.vvo.omds.types.omds3Types.v1_1_1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Typ des Requestobjekts, um Dokument zu Schaden hinzuzufügen
 * 
 * <p>Java-Klasse für AddDocToClaimRequest_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="AddDocToClaimRequest_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="VUNr" type="{urn:omds20}VUNr" minOccurs="0"/&gt;
 *         &lt;choice&gt;
 *           &lt;element ref="{urn:omds3ServiceTypes-1-1-0}IdGeschaeftsfallSchadenereignis"/&gt;
 *           &lt;element name="Schadennr" type="{urn:omds20}Schadennr"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="Dokument" type="{urn:omds3ServiceTypes-1-1-0}Upload_Dokument_Type"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AddDocToClaimRequest_Type", propOrder = {
    "vuNr",
    "idGeschaeftsfallSchadenereignis",
    "schadennr",
    "dokument"
})
public class AddDocToClaimRequestType {

    @XmlElement(name = "VUNr")
    protected String vuNr;
    @XmlElement(name = "IdGeschaeftsfallSchadenereignis")
    protected String idGeschaeftsfallSchadenereignis;
    @XmlElement(name = "Schadennr")
    protected String schadennr;
    @XmlElement(name = "Dokument", required = true)
    protected UploadDokumentType dokument;

    /**
     * Ruft den Wert der vuNr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVUNr() {
        return vuNr;
    }

    /**
     * Legt den Wert der vuNr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVUNr(String value) {
        this.vuNr = value;
    }

    /**
     * Ruft den Wert der idGeschaeftsfallSchadenereignis-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdGeschaeftsfallSchadenereignis() {
        return idGeschaeftsfallSchadenereignis;
    }

    /**
     * Legt den Wert der idGeschaeftsfallSchadenereignis-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdGeschaeftsfallSchadenereignis(String value) {
        this.idGeschaeftsfallSchadenereignis = value;
    }

    /**
     * Ruft den Wert der schadennr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSchadennr() {
        return schadennr;
    }

    /**
     * Legt den Wert der schadennr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSchadennr(String value) {
        this.schadennr = value;
    }

    /**
     * Ruft den Wert der dokument-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link UploadDokumentType }
     *     
     */
    public UploadDokumentType getDokument() {
        return dokument;
    }

    /**
     * Legt den Wert der dokument-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link UploadDokumentType }
     *     
     */
    public void setDokument(UploadDokumentType value) {
        this.dokument = value;
    }

}
