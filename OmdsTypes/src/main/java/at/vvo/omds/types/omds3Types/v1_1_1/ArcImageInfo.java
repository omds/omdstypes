
package at.vvo.omds.types.omds3Types.v1_1_1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import at.vvo.omds.types.omds3Types.v1_1_1.common.ElementIdType;


/**
 * Information zu einem einzelnen Dokument
 * 
 * <p>Java-Klasse für ArcImageInfo complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ArcImageInfo"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="arcImageIdDetails" type="{urn:omds3CommonServiceTypes-1-1-0}ElementIdType"/&gt;
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="documentType" type="{urn:omds3CommonServiceTypes-1-1-0}DocumentType"/&gt;
 *         &lt;element name="arcContentType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="arcContentLength" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/&gt;
 *         &lt;element name="date" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *         &lt;element name="docReference" type="{urn:omds3ServiceTypes-1-1-0}ArcImageInfo" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArcImageInfo", propOrder = {
    "arcImageIdDetails",
    "name",
    "documentType",
    "arcContentType",
    "arcContentLength",
    "date",
    "docReference"
})
public class ArcImageInfo {

    @XmlElement(required = true)
    protected ElementIdType arcImageIdDetails;
    @XmlElement(required = true)
    protected String name;
    protected int documentType;
    protected String arcContentType;
    protected Long arcContentLength;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar date;
    protected List<ArcImageInfo> docReference;

    /**
     * Ruft den Wert der arcImageIdDetails-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ElementIdType }
     *     
     */
    public ElementIdType getArcImageIdDetails() {
        return arcImageIdDetails;
    }

    /**
     * Legt den Wert der arcImageIdDetails-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ElementIdType }
     *     
     */
    public void setArcImageIdDetails(ElementIdType value) {
        this.arcImageIdDetails = value;
    }

    /**
     * Ruft den Wert der name-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Legt den Wert der name-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Ruft den Wert der documentType-Eigenschaft ab.
     * 
     */
    public int getDocumentType() {
        return documentType;
    }

    /**
     * Legt den Wert der documentType-Eigenschaft fest.
     * 
     */
    public void setDocumentType(int value) {
        this.documentType = value;
    }

    /**
     * Ruft den Wert der arcContentType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArcContentType() {
        return arcContentType;
    }

    /**
     * Legt den Wert der arcContentType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArcContentType(String value) {
        this.arcContentType = value;
    }

    /**
     * Ruft den Wert der arcContentLength-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getArcContentLength() {
        return arcContentLength;
    }

    /**
     * Legt den Wert der arcContentLength-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setArcContentLength(Long value) {
        this.arcContentLength = value;
    }

    /**
     * Ruft den Wert der date-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDate() {
        return date;
    }

    /**
     * Legt den Wert der date-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDate(XMLGregorianCalendar value) {
        this.date = value;
    }

    /**
     * Gets the value of the docReference property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the docReference property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDocReference().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ArcImageInfo }
     * 
     * 
     */
    public List<ArcImageInfo> getDocReference() {
        if (docReference == null) {
            docReference = new ArrayList<ArcImageInfo>();
        }
        return this.docReference;
    }

}
