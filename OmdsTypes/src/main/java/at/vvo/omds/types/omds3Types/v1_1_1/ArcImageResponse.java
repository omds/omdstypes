
package at.vvo.omds.types.omds3Types.v1_1_1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.v1_1_1.common.ServiceFault;


/**
 * <p>Java-Klasse für ArcImageResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ArcImageResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ArcContent" type="{urn:omds3ServiceTypes-1-1-0}ArcContent" minOccurs="0"/&gt;
 *         &lt;element name="ServiceFault" type="{urn:omds3CommonServiceTypes-1-1-0}ServiceFault" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArcImageResponse", propOrder = {
    "arcContent",
    "serviceFault"
})
public class ArcImageResponse {

    @XmlElement(name = "ArcContent")
    protected ArcContent arcContent;
    @XmlElement(name = "ServiceFault")
    protected ServiceFault serviceFault;

    /**
     * Ruft den Wert der arcContent-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ArcContent }
     *     
     */
    public ArcContent getArcContent() {
        return arcContent;
    }

    /**
     * Legt den Wert der arcContent-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ArcContent }
     *     
     */
    public void setArcContent(ArcContent value) {
        this.arcContent = value;
    }

    /**
     * Ruft den Wert der serviceFault-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceFault }
     *     
     */
    public ServiceFault getServiceFault() {
        return serviceFault;
    }

    /**
     * Legt den Wert der serviceFault-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceFault }
     *     
     */
    public void setServiceFault(ServiceFault value) {
        this.serviceFault = value;
    }

}
