
package at.vvo.omds.types.omds3Types.v1_1_1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Typ für die Meldung von Personen, die an einem Schaden oder einem Vertrag beteiligt sind
 * 
 * <p>Java-Klasse für BeteiligtePerson_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="BeteiligtePerson_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:omds3ServiceTypes-1-1-0}InformationenPerson"/&gt;
 *         &lt;element name="GeschInteresseLfnr" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="BetLfnr" use="required" type="{http://www.w3.org/2001/XMLSchema}unsignedShort" /&gt;
 *       &lt;attribute name="BetRolleCd" type="{urn:omds20}BetRolleCd_Type" /&gt;
 *       &lt;attribute name="BetTxt"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="100"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BeteiligtePerson_Type", propOrder = {
    "informationenPerson",
    "geschInteresseLfnr"
})
public class BeteiligtePersonType {

    @XmlElement(name = "InformationenPerson", required = true)
    protected InformationenPersonType informationenPerson;
    @XmlElement(name = "GeschInteresseLfnr", type = Long.class)
    @XmlSchemaType(name = "unsignedInt")
    protected List<Long> geschInteresseLfnr;
    @XmlAttribute(name = "BetLfnr", namespace = "urn:omds3ServiceTypes-1-1-0", required = true)
    @XmlSchemaType(name = "unsignedShort")
    protected int betLfnr;
    @XmlAttribute(name = "BetRolleCd", namespace = "urn:omds3ServiceTypes-1-1-0")
    protected String betRolleCd;
    @XmlAttribute(name = "BetTxt", namespace = "urn:omds3ServiceTypes-1-1-0")
    protected String betTxt;

    /**
     * Objekt ähnlich zu omds:PERSON, aber Personennr ist nicht Pflichtfeld
     * 
     * @return
     *     possible object is
     *     {@link InformationenPersonType }
     *     
     */
    public InformationenPersonType getInformationenPerson() {
        return informationenPerson;
    }

    /**
     * Legt den Wert der informationenPerson-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link InformationenPersonType }
     *     
     */
    public void setInformationenPerson(InformationenPersonType value) {
        this.informationenPerson = value;
    }

    /**
     * Gets the value of the geschInteresseLfnr property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the geschInteresseLfnr property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGeschInteresseLfnr().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Long }
     * 
     * 
     */
    public List<Long> getGeschInteresseLfnr() {
        if (geschInteresseLfnr == null) {
            geschInteresseLfnr = new ArrayList<Long>();
        }
        return this.geschInteresseLfnr;
    }

    /**
     * Ruft den Wert der betLfnr-Eigenschaft ab.
     * 
     */
    public int getBetLfnr() {
        return betLfnr;
    }

    /**
     * Legt den Wert der betLfnr-Eigenschaft fest.
     * 
     */
    public void setBetLfnr(int value) {
        this.betLfnr = value;
    }

    /**
     * Ruft den Wert der betRolleCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBetRolleCd() {
        return betRolleCd;
    }

    /**
     * Legt den Wert der betRolleCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBetRolleCd(String value) {
        this.betRolleCd = value;
    }

    /**
     * Ruft den Wert der betTxt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBetTxt() {
        return betTxt;
    }

    /**
     * Legt den Wert der betTxt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBetTxt(String value) {
        this.betTxt = value;
    }

}
