
package at.vvo.omds.types.omds3Types.v1_1_1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.v1_1_1.common.ServiceFault;


/**
 * Typ für Response mit einer Liste von geänderten Schäden für einen bestimmten Zeitraum
 * 
 * <p>Java-Klasse für ChangedClaimsListResponse_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ChangedClaimsListResponse_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice&gt;
 *         &lt;element name="Result"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="ActualOffset" type="{http://www.w3.org/2001/XMLSchema}unsignedInt"/&gt;
 *                   &lt;element name="ActualMaxResults" type="{http://www.w3.org/2001/XMLSchema}unsignedInt"/&gt;
 *                   &lt;element name="TotalResults" type="{http://www.w3.org/2001/XMLSchema}unsignedInt"/&gt;
 *                   &lt;element name="GeaenderteObjekte" type="{urn:omds3ServiceTypes-1-1-0}SchadenStatus_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="ServiceFault" type="{urn:omds3CommonServiceTypes-1-1-0}ServiceFault"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChangedClaimsListResponse_Type", propOrder = {
    "result",
    "serviceFault"
})
public class ChangedClaimsListResponseType {

    @XmlElement(name = "Result")
    protected ChangedClaimsListResponseType.Result result;
    @XmlElement(name = "ServiceFault")
    protected ServiceFault serviceFault;

    /**
     * Ruft den Wert der result-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ChangedClaimsListResponseType.Result }
     *     
     */
    public ChangedClaimsListResponseType.Result getResult() {
        return result;
    }

    /**
     * Legt den Wert der result-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ChangedClaimsListResponseType.Result }
     *     
     */
    public void setResult(ChangedClaimsListResponseType.Result value) {
        this.result = value;
    }

    /**
     * Ruft den Wert der serviceFault-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceFault }
     *     
     */
    public ServiceFault getServiceFault() {
        return serviceFault;
    }

    /**
     * Legt den Wert der serviceFault-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceFault }
     *     
     */
    public void setServiceFault(ServiceFault value) {
        this.serviceFault = value;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="ActualOffset" type="{http://www.w3.org/2001/XMLSchema}unsignedInt"/&gt;
     *         &lt;element name="ActualMaxResults" type="{http://www.w3.org/2001/XMLSchema}unsignedInt"/&gt;
     *         &lt;element name="TotalResults" type="{http://www.w3.org/2001/XMLSchema}unsignedInt"/&gt;
     *         &lt;element name="GeaenderteObjekte" type="{urn:omds3ServiceTypes-1-1-0}SchadenStatus_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "actualOffset",
        "actualMaxResults",
        "totalResults",
        "geaenderteObjekte"
    })
    public static class Result {

        @XmlElement(name = "ActualOffset")
        @XmlSchemaType(name = "unsignedInt")
        protected long actualOffset;
        @XmlElement(name = "ActualMaxResults")
        @XmlSchemaType(name = "unsignedInt")
        protected long actualMaxResults;
        @XmlElement(name = "TotalResults")
        @XmlSchemaType(name = "unsignedInt")
        protected long totalResults;
        @XmlElement(name = "GeaenderteObjekte")
        protected List<SchadenStatusType> geaenderteObjekte;

        /**
         * Ruft den Wert der actualOffset-Eigenschaft ab.
         * 
         */
        public long getActualOffset() {
            return actualOffset;
        }

        /**
         * Legt den Wert der actualOffset-Eigenschaft fest.
         * 
         */
        public void setActualOffset(long value) {
            this.actualOffset = value;
        }

        /**
         * Ruft den Wert der actualMaxResults-Eigenschaft ab.
         * 
         */
        public long getActualMaxResults() {
            return actualMaxResults;
        }

        /**
         * Legt den Wert der actualMaxResults-Eigenschaft fest.
         * 
         */
        public void setActualMaxResults(long value) {
            this.actualMaxResults = value;
        }

        /**
         * Ruft den Wert der totalResults-Eigenschaft ab.
         * 
         */
        public long getTotalResults() {
            return totalResults;
        }

        /**
         * Legt den Wert der totalResults-Eigenschaft fest.
         * 
         */
        public void setTotalResults(long value) {
            this.totalResults = value;
        }

        /**
         * Gets the value of the geaenderteObjekte property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the geaenderteObjekte property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getGeaenderteObjekte().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link SchadenStatusType }
         * 
         * 
         */
        public List<SchadenStatusType> getGeaenderteObjekte() {
            if (geaenderteObjekte == null) {
                geaenderteObjekte = new ArrayList<SchadenStatusType>();
            }
            return this.geaenderteObjekte;
        }

    }

}
