
package at.vvo.omds.types.omds3Types.v1_1_1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Typ des Request, wenn die Zusammenlegung von Schadenfällen bekanntgegeben wird
 * 
 * <p>Java-Klasse für DeclareNewClaimStatusRequest_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="DeclareNewClaimStatusRequest_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="alt" type="{urn:omds3ServiceTypes-1-1-0}SchadenStatus_Type"/&gt;
 *         &lt;element name="neu" type="{urn:omds3ServiceTypes-1-1-0}SchadenStatus_Type"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DeclareNewClaimStatusRequest_Type", propOrder = {
    "alt",
    "neu"
})
public class DeclareNewClaimStatusRequestType {

    @XmlElement(required = true)
    protected SchadenStatusType alt;
    @XmlElement(required = true)
    protected SchadenStatusType neu;

    /**
     * Ruft den Wert der alt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SchadenStatusType }
     *     
     */
    public SchadenStatusType getAlt() {
        return alt;
    }

    /**
     * Legt den Wert der alt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SchadenStatusType }
     *     
     */
    public void setAlt(SchadenStatusType value) {
        this.alt = value;
    }

    /**
     * Ruft den Wert der neu-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SchadenStatusType }
     *     
     */
    public SchadenStatusType getNeu() {
        return neu;
    }

    /**
     * Legt den Wert der neu-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SchadenStatusType }
     *     
     */
    public void setNeu(SchadenStatusType value) {
        this.neu = value;
    }

}
