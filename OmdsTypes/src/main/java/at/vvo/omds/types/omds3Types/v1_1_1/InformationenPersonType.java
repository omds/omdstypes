
package at.vvo.omds.types.omds3Types.v1_1_1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds2Types.v2_9.ELAnzahlType;
import at.vvo.omds.types.omds2Types.v2_9.ELEinstufungType;
import at.vvo.omds.types.omds2Types.v2_9.ELEntscheidungsfrageType;
import at.vvo.omds.types.omds2Types.v2_9.ELIdentifizierungType;
import at.vvo.omds.types.omds2Types.v2_9.ELKommunikationType;
import at.vvo.omds.types.omds2Types.v2_9.ELLegitimationType;
import at.vvo.omds.types.omds2Types.v2_9.ELTextType;
import at.vvo.omds.types.omds2Types.v2_9.NATUERLICHEPERSONType;
import at.vvo.omds.types.omds2Types.v2_9.PersArtCdType;
import at.vvo.omds.types.omds2Types.v2_9.SONSTIGEPERSONType;


/**
 * Typ zur Übergabe personenbezogener Daten
 * 
 * <p>Java-Klasse für InformationenPerson_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="InformationenPerson_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice&gt;
 *           &lt;element ref="{urn:omds20}NATUERLICHE_PERSON"/&gt;
 *           &lt;element ref="{urn:omds20}SONSTIGE_PERSON"/&gt;
 *         &lt;/choice&gt;
 *         &lt;choice maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;element ref="{urn:omds20}EL-Anzahl"/&gt;
 *           &lt;element ref="{urn:omds20}EL-Einstufung"/&gt;
 *           &lt;element ref="{urn:omds20}EL-Entscheidungsfrage"/&gt;
 *           &lt;element ref="{urn:omds20}EL-Identifizierung"/&gt;
 *           &lt;element ref="{urn:omds20}EL-Kommunikation"/&gt;
 *           &lt;element ref="{urn:omds20}EL-Legitimation"/&gt;
 *           &lt;element ref="{urn:omds20}EL-Text"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *       &lt;attGroup ref="{urn:omds20}Adresse_Attribute"/&gt;
 *       &lt;attribute name="Personennr" type="{urn:omds20}Personennr" /&gt;
 *       &lt;attribute name="PersArtCd" use="required" type="{urn:omds20}PersArtCd_Type" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InformationenPerson_Type", propOrder = {
    "natuerlicheperson",
    "sonstigeperson",
    "elAnzahlOrELEinstufungOrELEntscheidungsfrage"
})
public class InformationenPersonType {

    @XmlElement(name = "NATUERLICHE_PERSON", namespace = "urn:omds20")
    protected NATUERLICHEPERSONType natuerlicheperson;
    @XmlElement(name = "SONSTIGE_PERSON", namespace = "urn:omds20")
    protected SONSTIGEPERSONType sonstigeperson;
    @XmlElements({
        @XmlElement(name = "EL-Anzahl", namespace = "urn:omds20", type = ELAnzahlType.class),
        @XmlElement(name = "EL-Einstufung", namespace = "urn:omds20", type = ELEinstufungType.class),
        @XmlElement(name = "EL-Entscheidungsfrage", namespace = "urn:omds20", type = ELEntscheidungsfrageType.class),
        @XmlElement(name = "EL-Identifizierung", namespace = "urn:omds20", type = ELIdentifizierungType.class),
        @XmlElement(name = "EL-Kommunikation", namespace = "urn:omds20", type = ELKommunikationType.class),
        @XmlElement(name = "EL-Legitimation", namespace = "urn:omds20", type = ELLegitimationType.class),
        @XmlElement(name = "EL-Text", namespace = "urn:omds20", type = ELTextType.class)
    })
    protected List<Object> elAnzahlOrELEinstufungOrELEntscheidungsfrage;
    @XmlAttribute(name = "Personennr", namespace = "urn:omds3ServiceTypes-1-1-0")
    protected String personennr;
    @XmlAttribute(name = "PersArtCd", namespace = "urn:omds3ServiceTypes-1-1-0", required = true)
    protected PersArtCdType persArtCd;
    @XmlAttribute(name = "Pac")
    @XmlSchemaType(name = "unsignedInt")
    protected Long pac;
    @XmlAttribute(name = "LandesCd")
    protected String landesCd;
    @XmlAttribute(name = "PLZ")
    protected String plz;
    @XmlAttribute(name = "Ort")
    protected String ort;
    @XmlAttribute(name = "Strasse")
    protected String strasse;
    @XmlAttribute(name = "Hausnr")
    protected String hausnr;
    @XmlAttribute(name = "Zusatz")
    protected String zusatz;

    /**
     * Ruft den Wert der natuerlicheperson-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link NATUERLICHEPERSONType }
     *     
     */
    public NATUERLICHEPERSONType getNATUERLICHEPERSON() {
        return natuerlicheperson;
    }

    /**
     * Legt den Wert der natuerlicheperson-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link NATUERLICHEPERSONType }
     *     
     */
    public void setNATUERLICHEPERSON(NATUERLICHEPERSONType value) {
        this.natuerlicheperson = value;
    }

    /**
     * Ruft den Wert der sonstigeperson-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SONSTIGEPERSONType }
     *     
     */
    public SONSTIGEPERSONType getSONSTIGEPERSON() {
        return sonstigeperson;
    }

    /**
     * Legt den Wert der sonstigeperson-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SONSTIGEPERSONType }
     *     
     */
    public void setSONSTIGEPERSON(SONSTIGEPERSONType value) {
        this.sonstigeperson = value;
    }

    /**
     * Gets the value of the elAnzahlOrELEinstufungOrELEntscheidungsfrage property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the elAnzahlOrELEinstufungOrELEntscheidungsfrage property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getELAnzahlOrELEinstufungOrELEntscheidungsfrage().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ELAnzahlType }
     * {@link ELEinstufungType }
     * {@link ELEntscheidungsfrageType }
     * {@link ELIdentifizierungType }
     * {@link ELKommunikationType }
     * {@link ELLegitimationType }
     * {@link ELTextType }
     * 
     * 
     */
    public List<Object> getELAnzahlOrELEinstufungOrELEntscheidungsfrage() {
        if (elAnzahlOrELEinstufungOrELEntscheidungsfrage == null) {
            elAnzahlOrELEinstufungOrELEntscheidungsfrage = new ArrayList<Object>();
        }
        return this.elAnzahlOrELEinstufungOrELEntscheidungsfrage;
    }

    /**
     * Ruft den Wert der personennr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPersonennr() {
        return personennr;
    }

    /**
     * Legt den Wert der personennr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPersonennr(String value) {
        this.personennr = value;
    }

    /**
     * Ruft den Wert der persArtCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PersArtCdType }
     *     
     */
    public PersArtCdType getPersArtCd() {
        return persArtCd;
    }

    /**
     * Legt den Wert der persArtCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PersArtCdType }
     *     
     */
    public void setPersArtCd(PersArtCdType value) {
        this.persArtCd = value;
    }

    /**
     * Ruft den Wert der pac-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getPac() {
        return pac;
    }

    /**
     * Legt den Wert der pac-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setPac(Long value) {
        this.pac = value;
    }

    /**
     * Ruft den Wert der landesCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLandesCd() {
        return landesCd;
    }

    /**
     * Legt den Wert der landesCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLandesCd(String value) {
        this.landesCd = value;
    }

    /**
     * Ruft den Wert der plz-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPLZ() {
        return plz;
    }

    /**
     * Legt den Wert der plz-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPLZ(String value) {
        this.plz = value;
    }

    /**
     * Ruft den Wert der ort-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrt() {
        return ort;
    }

    /**
     * Legt den Wert der ort-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrt(String value) {
        this.ort = value;
    }

    /**
     * Ruft den Wert der strasse-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStrasse() {
        return strasse;
    }

    /**
     * Legt den Wert der strasse-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStrasse(String value) {
        this.strasse = value;
    }

    /**
     * Ruft den Wert der hausnr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHausnr() {
        return hausnr;
    }

    /**
     * Legt den Wert der hausnr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHausnr(String value) {
        this.hausnr = value;
    }

    /**
     * Ruft den Wert der zusatz-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZusatz() {
        return zusatz;
    }

    /**
     * Legt den Wert der zusatz-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZusatz(String value) {
        this.zusatz = value;
    }

}
