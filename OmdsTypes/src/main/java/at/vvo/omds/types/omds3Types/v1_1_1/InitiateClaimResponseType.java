
package at.vvo.omds.types.omds3Types.v1_1_1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import at.vvo.omds.types.omds3Types.v1_1_1.common.ServiceFault;


/**
 * Anworttyp beim Erzeugen einer einfachen Schadenmeldung
 * 
 * <p>Java-Klasse für InitiateClaimResponse_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="InitiateClaimResponse_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="MeldungsZusammenfassung" type="{urn:omds3ServiceTypes-1-1-0}MeldungszusammenfassungInitiateClaim_Type" minOccurs="0"/&gt;
 *         &lt;element name="Meldedat" type="{urn:omds20}Datum-Zeit"/&gt;
 *         &lt;element name="ServiceFault" type="{urn:omds3CommonServiceTypes-1-1-0}ServiceFault" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InitiateClaimResponse_Type", propOrder = {
    "meldungsZusammenfassung",
    "meldedat",
    "serviceFault"
})
public class InitiateClaimResponseType {

    @XmlElement(name = "MeldungsZusammenfassung")
    protected MeldungszusammenfassungInitiateClaimType meldungsZusammenfassung;
    @XmlElement(name = "Meldedat", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar meldedat;
    @XmlElement(name = "ServiceFault")
    protected List<ServiceFault> serviceFault;

    /**
     * Ruft den Wert der meldungsZusammenfassung-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MeldungszusammenfassungInitiateClaimType }
     *     
     */
    public MeldungszusammenfassungInitiateClaimType getMeldungsZusammenfassung() {
        return meldungsZusammenfassung;
    }

    /**
     * Legt den Wert der meldungsZusammenfassung-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MeldungszusammenfassungInitiateClaimType }
     *     
     */
    public void setMeldungsZusammenfassung(MeldungszusammenfassungInitiateClaimType value) {
        this.meldungsZusammenfassung = value;
    }

    /**
     * Ruft den Wert der meldedat-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getMeldedat() {
        return meldedat;
    }

    /**
     * Legt den Wert der meldedat-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setMeldedat(XMLGregorianCalendar value) {
        this.meldedat = value;
    }

    /**
     * Gets the value of the serviceFault property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the serviceFault property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getServiceFault().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ServiceFault }
     * 
     * 
     */
    public List<ServiceFault> getServiceFault() {
        if (serviceFault == null) {
            serviceFault = new ArrayList<ServiceFault>();
        }
        return this.serviceFault;
    }

}
