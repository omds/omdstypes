
package at.vvo.omds.types.omds3Types.v1_1_1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.v1_1_1.common.ServiceFault;


/**
 * Objekt, welches GeschäftsfallId und Schadennummer, Dokumenten-Ids sowie den Bearbeitungsstand enthält
 * 
 * <p>Java-Klasse für MeldungszusammenfassungInitiateClaim_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="MeldungszusammenfassungInitiateClaim_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:omds3ServiceTypes-1-1-0}IdGeschaeftsfallSchadenereignis"/&gt;
 *         &lt;element name="VUNr" type="{urn:omds20}VUNr"/&gt;
 *         &lt;element ref="{urn:omds3CommonServiceTypes-1-1-0}OrdnungsbegriffZuordFremd" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ErgebnisDokumente" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="LfdNr" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *                   &lt;choice&gt;
 *                     &lt;element name="Dokument" type="{urn:omds3ServiceTypes-1-1-0}ArcImageInfo"/&gt;
 *                     &lt;element name="FehlerDokumentenanlage" type="{urn:omds3CommonServiceTypes-1-1-0}ServiceFault"/&gt;
 *                   &lt;/choice&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Schaeden" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="BearbStandCd" type="{urn:omds3ServiceTypes-1-1-0}BearbStandCd_Type"/&gt;
 *                   &lt;element name="Schadennr" type="{urn:omds20}Schadennr"/&gt;
 *                   &lt;element name="SachbearbVU" type="{urn:omds3ServiceTypes-1-1-0}SachbearbVUType" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MeldungszusammenfassungInitiateClaim_Type", propOrder = {
    "idGeschaeftsfallSchadenereignis",
    "vuNr",
    "ordnungsbegriffZuordFremd",
    "ergebnisDokumente",
    "schaeden"
})
public class MeldungszusammenfassungInitiateClaimType {

    @XmlElement(name = "IdGeschaeftsfallSchadenereignis", required = true)
    protected String idGeschaeftsfallSchadenereignis;
    @XmlElement(name = "VUNr", required = true)
    protected String vuNr;
    @XmlElement(name = "OrdnungsbegriffZuordFremd", namespace = "urn:omds3CommonServiceTypes-1-1-0")
    protected List<String> ordnungsbegriffZuordFremd;
    @XmlElement(name = "ErgebnisDokumente")
    protected List<MeldungszusammenfassungInitiateClaimType.ErgebnisDokumente> ergebnisDokumente;
    @XmlElement(name = "Schaeden")
    protected List<MeldungszusammenfassungInitiateClaimType.Schaeden> schaeden;

    /**
     * Ruft den Wert der idGeschaeftsfallSchadenereignis-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdGeschaeftsfallSchadenereignis() {
        return idGeschaeftsfallSchadenereignis;
    }

    /**
     * Legt den Wert der idGeschaeftsfallSchadenereignis-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdGeschaeftsfallSchadenereignis(String value) {
        this.idGeschaeftsfallSchadenereignis = value;
    }

    /**
     * Ruft den Wert der vuNr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVUNr() {
        return vuNr;
    }

    /**
     * Legt den Wert der vuNr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVUNr(String value) {
        this.vuNr = value;
    }

    /**
     * Ordnungsbegriff des Schadenmelders auf Ebene des Schadenereignis Gets the value of the ordnungsbegriffZuordFremd property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ordnungsbegriffZuordFremd property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOrdnungsbegriffZuordFremd().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getOrdnungsbegriffZuordFremd() {
        if (ordnungsbegriffZuordFremd == null) {
            ordnungsbegriffZuordFremd = new ArrayList<String>();
        }
        return this.ordnungsbegriffZuordFremd;
    }

    /**
     * Gets the value of the ergebnisDokumente property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ergebnisDokumente property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getErgebnisDokumente().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MeldungszusammenfassungInitiateClaimType.ErgebnisDokumente }
     * 
     * 
     */
    public List<MeldungszusammenfassungInitiateClaimType.ErgebnisDokumente> getErgebnisDokumente() {
        if (ergebnisDokumente == null) {
            ergebnisDokumente = new ArrayList<MeldungszusammenfassungInitiateClaimType.ErgebnisDokumente>();
        }
        return this.ergebnisDokumente;
    }

    /**
     * Gets the value of the schaeden property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the schaeden property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSchaeden().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MeldungszusammenfassungInitiateClaimType.Schaeden }
     * 
     * 
     */
    public List<MeldungszusammenfassungInitiateClaimType.Schaeden> getSchaeden() {
        if (schaeden == null) {
            schaeden = new ArrayList<MeldungszusammenfassungInitiateClaimType.Schaeden>();
        }
        return this.schaeden;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="LfdNr" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
     *         &lt;choice&gt;
     *           &lt;element name="Dokument" type="{urn:omds3ServiceTypes-1-1-0}ArcImageInfo"/&gt;
     *           &lt;element name="FehlerDokumentenanlage" type="{urn:omds3CommonServiceTypes-1-1-0}ServiceFault"/&gt;
     *         &lt;/choice&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "lfdNr",
        "dokument",
        "fehlerDokumentenanlage"
    })
    public static class ErgebnisDokumente {

        @XmlElement(name = "LfdNr")
        protected int lfdNr;
        @XmlElement(name = "Dokument")
        protected ArcImageInfo dokument;
        @XmlElement(name = "FehlerDokumentenanlage")
        protected ServiceFault fehlerDokumentenanlage;

        /**
         * Ruft den Wert der lfdNr-Eigenschaft ab.
         * 
         */
        public int getLfdNr() {
            return lfdNr;
        }

        /**
         * Legt den Wert der lfdNr-Eigenschaft fest.
         * 
         */
        public void setLfdNr(int value) {
            this.lfdNr = value;
        }

        /**
         * Ruft den Wert der dokument-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link ArcImageInfo }
         *     
         */
        public ArcImageInfo getDokument() {
            return dokument;
        }

        /**
         * Legt den Wert der dokument-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link ArcImageInfo }
         *     
         */
        public void setDokument(ArcImageInfo value) {
            this.dokument = value;
        }

        /**
         * Ruft den Wert der fehlerDokumentenanlage-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link ServiceFault }
         *     
         */
        public ServiceFault getFehlerDokumentenanlage() {
            return fehlerDokumentenanlage;
        }

        /**
         * Legt den Wert der fehlerDokumentenanlage-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link ServiceFault }
         *     
         */
        public void setFehlerDokumentenanlage(ServiceFault value) {
            this.fehlerDokumentenanlage = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="BearbStandCd" type="{urn:omds3ServiceTypes-1-1-0}BearbStandCd_Type"/&gt;
     *         &lt;element name="Schadennr" type="{urn:omds20}Schadennr"/&gt;
     *         &lt;element name="SachbearbVU" type="{urn:omds3ServiceTypes-1-1-0}SachbearbVUType" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "bearbStandCd",
        "schadennr",
        "sachbearbVU"
    })
    public static class Schaeden {

        @XmlElement(name = "BearbStandCd", required = true)
        protected String bearbStandCd;
        @XmlElement(name = "Schadennr", required = true)
        protected String schadennr;
        @XmlElement(name = "SachbearbVU")
        protected SachbearbVUType sachbearbVU;

        /**
         * Ruft den Wert der bearbStandCd-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBearbStandCd() {
            return bearbStandCd;
        }

        /**
         * Legt den Wert der bearbStandCd-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBearbStandCd(String value) {
            this.bearbStandCd = value;
        }

        /**
         * Ruft den Wert der schadennr-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSchadennr() {
            return schadennr;
        }

        /**
         * Legt den Wert der schadennr-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSchadennr(String value) {
            this.schadennr = value;
        }

        /**
         * Ruft den Wert der sachbearbVU-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link SachbearbVUType }
         *     
         */
        public SachbearbVUType getSachbearbVU() {
            return sachbearbVU;
        }

        /**
         * Legt den Wert der sachbearbVU-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link SachbearbVUType }
         *     
         */
        public void setSachbearbVU(SachbearbVUType value) {
            this.sachbearbVU = value;
        }

    }

}
