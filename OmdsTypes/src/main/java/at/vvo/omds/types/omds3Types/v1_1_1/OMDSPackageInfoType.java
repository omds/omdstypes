
package at.vvo.omds.types.omds3Types.v1_1_1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import at.vvo.omds.types.omds2Types.v2_9.PaketInhCdType;
import at.vvo.omds.types.omds2Types.v2_9.PaketUmfCdType;
import at.vvo.omds.types.omds3Types.v1_1_1.common.ElementIdType;


/**
 * Typ fuer die wesentlichen Informationen zu einem OMDS-Datensatz-Package
 * 
 * <p>Java-Klasse für OMDSPackageInfoType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="OMDSPackageInfoType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="VUNr" type="{urn:omds20}VUNr"/&gt;
 *         &lt;element name="MaklerID" type="{urn:omds3CommonServiceTypes-1-1-0}MaklerID_Type"/&gt;
 *         &lt;element name="omdsPackageIdDetails" type="{urn:omds3CommonServiceTypes-1-1-0}ElementIdType"/&gt;
 *         &lt;element name="timeStamp" type="{urn:omds20}Datum-Zeit"/&gt;
 *         &lt;element name="packageSize" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="packageExtent" type="{urn:omds20}PaketUmfCd_Type"/&gt;
 *         &lt;element name="packageContentCode" type="{urn:omds20}PaketInhCd_Type" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OMDSPackageInfoType", propOrder = {
    "vuNr",
    "maklerID",
    "omdsPackageIdDetails",
    "timeStamp",
    "packageSize",
    "packageExtent",
    "packageContentCode"
})
public class OMDSPackageInfoType {

    @XmlElement(name = "VUNr", required = true)
    protected String vuNr;
    @XmlElement(name = "MaklerID", required = true)
    protected String maklerID;
    @XmlElement(required = true)
    protected ElementIdType omdsPackageIdDetails;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timeStamp;
    protected int packageSize;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected PaketUmfCdType packageExtent;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected List<PaketInhCdType> packageContentCode;

    /**
     * Ruft den Wert der vuNr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVUNr() {
        return vuNr;
    }

    /**
     * Legt den Wert der vuNr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVUNr(String value) {
        this.vuNr = value;
    }

    /**
     * Ruft den Wert der maklerID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMaklerID() {
        return maklerID;
    }

    /**
     * Legt den Wert der maklerID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMaklerID(String value) {
        this.maklerID = value;
    }

    /**
     * Ruft den Wert der omdsPackageIdDetails-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ElementIdType }
     *     
     */
    public ElementIdType getOmdsPackageIdDetails() {
        return omdsPackageIdDetails;
    }

    /**
     * Legt den Wert der omdsPackageIdDetails-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ElementIdType }
     *     
     */
    public void setOmdsPackageIdDetails(ElementIdType value) {
        this.omdsPackageIdDetails = value;
    }

    /**
     * Ruft den Wert der timeStamp-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimeStamp() {
        return timeStamp;
    }

    /**
     * Legt den Wert der timeStamp-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimeStamp(XMLGregorianCalendar value) {
        this.timeStamp = value;
    }

    /**
     * Ruft den Wert der packageSize-Eigenschaft ab.
     * 
     */
    public int getPackageSize() {
        return packageSize;
    }

    /**
     * Legt den Wert der packageSize-Eigenschaft fest.
     * 
     */
    public void setPackageSize(int value) {
        this.packageSize = value;
    }

    /**
     * Ruft den Wert der packageExtent-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PaketUmfCdType }
     *     
     */
    public PaketUmfCdType getPackageExtent() {
        return packageExtent;
    }

    /**
     * Legt den Wert der packageExtent-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PaketUmfCdType }
     *     
     */
    public void setPackageExtent(PaketUmfCdType value) {
        this.packageExtent = value;
    }

    /**
     * Gets the value of the packageContentCode property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the packageContentCode property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPackageContentCode().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PaketInhCdType }
     * 
     * 
     */
    public List<PaketInhCdType> getPackageContentCode() {
        if (packageContentCode == null) {
            packageContentCode = new ArrayList<PaketInhCdType>();
        }
        return this.packageContentCode;
    }

}
