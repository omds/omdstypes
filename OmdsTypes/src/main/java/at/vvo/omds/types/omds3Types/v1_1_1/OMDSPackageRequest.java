
package at.vvo.omds.types.omds3Types.v1_1_1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.v1_1_1.common.AgentFilterType;


/**
 * <p>Java-Klasse für OMDSPackageRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="OMDSPackageRequest"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="VUNr" type="{urn:omds20}VUNr" minOccurs="0"/&gt;
 *         &lt;element name="agentFilter" type="{urn:omds3CommonServiceTypes-1-1-0}AgentFilter_Type" minOccurs="0"/&gt;
 *         &lt;element name="omdsPackageId" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OMDSPackageRequest", propOrder = {
    "vuNr",
    "agentFilter",
    "omdsPackageId"
})
public class OMDSPackageRequest {

    @XmlElement(name = "VUNr")
    protected String vuNr;
    protected AgentFilterType agentFilter;
    @XmlElement(required = true)
    protected List<String> omdsPackageId;

    /**
     * Ruft den Wert der vuNr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVUNr() {
        return vuNr;
    }

    /**
     * Legt den Wert der vuNr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVUNr(String value) {
        this.vuNr = value;
    }

    /**
     * Ruft den Wert der agentFilter-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AgentFilterType }
     *     
     */
    public AgentFilterType getAgentFilter() {
        return agentFilter;
    }

    /**
     * Legt den Wert der agentFilter-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AgentFilterType }
     *     
     */
    public void setAgentFilter(AgentFilterType value) {
        this.agentFilter = value;
    }

    /**
     * Gets the value of the omdsPackageId property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the omdsPackageId property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOmdsPackageId().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getOmdsPackageId() {
        if (omdsPackageId == null) {
            omdsPackageId = new ArrayList<String>();
        }
        return this.omdsPackageId;
    }

}
