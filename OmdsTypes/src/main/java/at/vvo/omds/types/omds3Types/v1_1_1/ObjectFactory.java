
package at.vvo.omds.types.omds3Types.v1_1_1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the at.vvo.omds.types.omds3Types.v1_1_1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _LoginRequest_QNAME = new QName("urn:omds3ServiceTypes-1-1-0", "loginRequest");
    private final static QName _LoginResponse_QNAME = new QName("urn:omds3ServiceTypes-1-1-0", "loginResponse");
    private final static QName _GetUserDataRequest_QNAME = new QName("urn:omds3ServiceTypes-1-1-0", "getUserDataRequest");
    private final static QName _GetUserDataResponse_QNAME = new QName("urn:omds3ServiceTypes-1-1-0", "getUserDataResponse");
    private final static QName _GetOMDSPackageListRequest_QNAME = new QName("urn:omds3ServiceTypes-1-1-0", "getOMDSPackageListRequest");
    private final static QName _GetOMDSPackageListResponse_QNAME = new QName("urn:omds3ServiceTypes-1-1-0", "getOMDSPackageListResponse");
    private final static QName _GetOMDSPackageRequest_QNAME = new QName("urn:omds3ServiceTypes-1-1-0", "getOMDSPackageRequest");
    private final static QName _GetOMDSPackageResponse_QNAME = new QName("urn:omds3ServiceTypes-1-1-0", "getOMDSPackageResponse");
    private final static QName _GetArcImageInfosRequest_QNAME = new QName("urn:omds3ServiceTypes-1-1-0", "getArcImageInfosRequest");
    private final static QName _GetArcImageInfosResponse_QNAME = new QName("urn:omds3ServiceTypes-1-1-0", "getArcImageInfosResponse");
    private final static QName _GetArcImageRequest_QNAME = new QName("urn:omds3ServiceTypes-1-1-0", "getArcImageRequest");
    private final static QName _GetArcImageResponse_QNAME = new QName("urn:omds3ServiceTypes-1-1-0", "getArcImageResponse");
    private final static QName _GetDeepLinkClaimRequest_QNAME = new QName("urn:omds3ServiceTypes-1-1-0", "getDeepLinkClaimRequest");
    private final static QName _GetDeepLinkClaimResponse_QNAME = new QName("urn:omds3ServiceTypes-1-1-0", "getDeepLinkClaimResponse");
    private final static QName _GetDeepLinkPartnerRequest_QNAME = new QName("urn:omds3ServiceTypes-1-1-0", "getDeepLinkPartnerRequest");
    private final static QName _GetDeepLinkPartnerResponse_QNAME = new QName("urn:omds3ServiceTypes-1-1-0", "getDeepLinkPartnerResponse");
    private final static QName _GetDeepLinkOfferRequest_QNAME = new QName("urn:omds3ServiceTypes-1-1-0", "getDeepLinkOfferRequest");
    private final static QName _GetDeepLinkOfferResponse_QNAME = new QName("urn:omds3ServiceTypes-1-1-0", "getDeepLinkOfferResponse");
    private final static QName _GetDeepLinkPolicyRequest_QNAME = new QName("urn:omds3ServiceTypes-1-1-0", "getDeepLinkPolicyRequest");
    private final static QName _GetDeepLinkPolicyResponse_QNAME = new QName("urn:omds3ServiceTypes-1-1-0", "getDeepLinkPolicyResponse");
    private final static QName _GetDeepLinkBusinessObjectResponse_QNAME = new QName("urn:omds3ServiceTypes-1-1-0", "getDeepLinkBusinessObjectResponse");
    private final static QName _WithoutFrame_QNAME = new QName("urn:omds3ServiceTypes-1-1-0", "withoutFrame");
    private final static QName _HttpActionLink_QNAME = new QName("urn:omds3ServiceTypes-1-1-0", "httpActionLink");
    private final static QName _CreateClaimRequest_QNAME = new QName("urn:omds3ServiceTypes-1-1-0", "CreateClaimRequest");
    private final static QName _InformationenPerson_QNAME = new QName("urn:omds3ServiceTypes-1-1-0", "InformationenPerson");
    private final static QName _CreateClaimResponse_QNAME = new QName("urn:omds3ServiceTypes-1-1-0", "CreateClaimResponse");
    private final static QName _InitiateClaimRequest_QNAME = new QName("urn:omds3ServiceTypes-1-1-0", "InitiateClaimRequest");
    private final static QName _InitiateClaimResponse_QNAME = new QName("urn:omds3ServiceTypes-1-1-0", "InitiateClaimResponse");
    private final static QName _AddDocToClaimRequest_QNAME = new QName("urn:omds3ServiceTypes-1-1-0", "AddDocToClaimRequest");
    private final static QName _AddDocToClaimResponse_QNAME = new QName("urn:omds3ServiceTypes-1-1-0", "AddDocToClaimResponse");
    private final static QName _GetClaimRequest_QNAME = new QName("urn:omds3ServiceTypes-1-1-0", "GetClaimRequest");
    private final static QName _GetClaimResponse_QNAME = new QName("urn:omds3ServiceTypes-1-1-0", "GetClaimResponse");
    private final static QName _GetClaimLightRequest_QNAME = new QName("urn:omds3ServiceTypes-1-1-0", "GetClaimLightRequest");
    private final static QName _GetClaimLightResponse_QNAME = new QName("urn:omds3ServiceTypes-1-1-0", "GetClaimLightResponse");
    private final static QName _GetNumberOfDocumentsRequest_QNAME = new QName("urn:omds3ServiceTypes-1-1-0", "GetNumberOfDocumentsRequest");
    private final static QName _GetNumberOfDocumentsResponse_QNAME = new QName("urn:omds3ServiceTypes-1-1-0", "GetNumberOfDocumentsResponse");
    private final static QName _GetDocumentInfosRequest_QNAME = new QName("urn:omds3ServiceTypes-1-1-0", "GetDocumentInfosRequest");
    private final static QName _GetDocumentInfosResponse_QNAME = new QName("urn:omds3ServiceTypes-1-1-0", "GetDocumentInfosResponse");
    private final static QName _SearchClaimRequest_QNAME = new QName("urn:omds3ServiceTypes-1-1-0", "SearchClaimRequest");
    private final static QName _SearchClaimResponse_QNAME = new QName("urn:omds3ServiceTypes-1-1-0", "SearchClaimResponse");
    private final static QName _Schadenzuordnung_QNAME = new QName("urn:omds3ServiceTypes-1-1-0", "Schadenzuordnung");
    private final static QName _ChangedClaimsListRequest_QNAME = new QName("urn:omds3ServiceTypes-1-1-0", "ChangedClaimsListRequest");
    private final static QName _ChangedClaimsListResponse_QNAME = new QName("urn:omds3ServiceTypes-1-1-0", "ChangedClaimsListResponse");
    private final static QName _LossEventListRequest_QNAME = new QName("urn:omds3ServiceTypes-1-1-0", "LossEventListRequest");
    private final static QName _LossEventListResponse_QNAME = new QName("urn:omds3ServiceTypes-1-1-0", "LossEventListResponse");
    private final static QName _IdGeschaeftsfallSchadenereignis_QNAME = new QName("urn:omds3ServiceTypes-1-1-0", "IdGeschaeftsfallSchadenereignis");
    private final static QName _IdGeschaeftsfallSchadenanlage_QNAME = new QName("urn:omds3ServiceTypes-1-1-0", "IdGeschaeftsfallSchadenanlage");
    private final static QName _DeclareEndpointRequest_QNAME = new QName("urn:omds3ServiceTypes-1-1-0", "DeclareEndpointRequest");
    private final static QName _ArtAuthentifizierung_QNAME = new QName("urn:omds3ServiceTypes-1-1-0", "ArtAuthentifizierung");
    private final static QName _DeclareEndpointResponse_QNAME = new QName("urn:omds3ServiceTypes-1-1-0", "DeclareEndpointResponse");
    private final static QName _SecurityContextTokenRequest_QNAME = new QName("urn:omds3ServiceTypes-1-1-0", "SecurityContextTokenRequest");
    private final static QName _SecurityContextTokenResponse_QNAME = new QName("urn:omds3ServiceTypes-1-1-0", "SecurityContextTokenResponse");
    private final static QName _DeclareNewClaimStatusRequest_QNAME = new QName("urn:omds3ServiceTypes-1-1-0", "DeclareNewClaimStatusRequest");
    private final static QName _DeclareNewClaimStatusResponse_QNAME = new QName("urn:omds3ServiceTypes-1-1-0", "DeclareNewClaimStatusResponse");
    private final static QName _LossEventRegisteredRequest_QNAME = new QName("urn:omds3ServiceTypes-1-1-0", "LossEventRegisteredRequest");
    private final static QName _LossEventRegisteredResponse_QNAME = new QName("urn:omds3ServiceTypes-1-1-0", "LossEventRegisteredResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: at.vvo.omds.types.omds3Types.v1_1_1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SchadenStatusType }
     * 
     */
    public SchadenStatusType createSchadenStatusType() {
        return new SchadenStatusType();
    }

    /**
     * Create an instance of {@link SachbearbVUType }
     * 
     */
    public SachbearbVUType createSachbearbVUType() {
        return new SachbearbVUType();
    }

    /**
     * Create an instance of {@link SchadenereignisType }
     * 
     */
    public SchadenereignisType createSchadenereignisType() {
        return new SchadenereignisType();
    }

    /**
     * Create an instance of {@link MeldungszusammenfassungInitiateClaimType }
     * 
     */
    public MeldungszusammenfassungInitiateClaimType createMeldungszusammenfassungInitiateClaimType() {
        return new MeldungszusammenfassungInitiateClaimType();
    }

    /**
     * Create an instance of {@link MeldungszusammenfassungType }
     * 
     */
    public MeldungszusammenfassungType createMeldungszusammenfassungType() {
        return new MeldungszusammenfassungType();
    }

    /**
     * Create an instance of {@link MeldungszusammenfassungType.ErgebnisSchaeden }
     * 
     */
    public MeldungszusammenfassungType.ErgebnisSchaeden createMeldungszusammenfassungTypeErgebnisSchaeden() {
        return new MeldungszusammenfassungType.ErgebnisSchaeden();
    }

    /**
     * Create an instance of {@link SchadenType }
     * 
     */
    public SchadenType createSchadenType() {
        return new SchadenType();
    }

    /**
     * Create an instance of {@link MeldungSchadenType }
     * 
     */
    public MeldungSchadenType createMeldungSchadenType() {
        return new MeldungSchadenType();
    }

    /**
     * Create an instance of {@link OrtType }
     * 
     */
    public OrtType createOrtType() {
        return new OrtType();
    }

    /**
     * Create an instance of {@link RequestedOMDSPackage }
     * 
     */
    public RequestedOMDSPackage createRequestedOMDSPackage() {
        return new RequestedOMDSPackage();
    }

    /**
     * Create an instance of {@link LossEventListResponseType }
     * 
     */
    public LossEventListResponseType createLossEventListResponseType() {
        return new LossEventListResponseType();
    }

    /**
     * Create an instance of {@link ChangedClaimsListResponseType }
     * 
     */
    public ChangedClaimsListResponseType createChangedClaimsListResponseType() {
        return new ChangedClaimsListResponseType();
    }

    /**
     * Create an instance of {@link SearchClaimResponseType }
     * 
     */
    public SearchClaimResponseType createSearchClaimResponseType() {
        return new SearchClaimResponseType();
    }

    /**
     * Create an instance of {@link GetDocumentInfosResponseType }
     * 
     */
    public GetDocumentInfosResponseType createGetDocumentInfosResponseType() {
        return new GetDocumentInfosResponseType();
    }

    /**
     * Create an instance of {@link UserDataResponse }
     * 
     */
    public UserDataResponse createUserDataResponse() {
        return new UserDataResponse();
    }

    /**
     * Create an instance of {@link LoginRequestType }
     * 
     */
    public LoginRequestType createLoginRequestType() {
        return new LoginRequestType();
    }

    /**
     * Create an instance of {@link DeepLinkBusinessObjectResponse }
     * 
     */
    public DeepLinkBusinessObjectResponse createDeepLinkBusinessObjectResponse() {
        return new DeepLinkBusinessObjectResponse();
    }

    /**
     * Create an instance of {@link UserDataRequest }
     * 
     */
    public UserDataRequest createUserDataRequest() {
        return new UserDataRequest();
    }

    /**
     * Create an instance of {@link OMDSPackageListRequest }
     * 
     */
    public OMDSPackageListRequest createOMDSPackageListRequest() {
        return new OMDSPackageListRequest();
    }

    /**
     * Create an instance of {@link OMDSPackageListResponse }
     * 
     */
    public OMDSPackageListResponse createOMDSPackageListResponse() {
        return new OMDSPackageListResponse();
    }

    /**
     * Create an instance of {@link OMDSPackageRequest }
     * 
     */
    public OMDSPackageRequest createOMDSPackageRequest() {
        return new OMDSPackageRequest();
    }

    /**
     * Create an instance of {@link OMDSPackageResponse }
     * 
     */
    public OMDSPackageResponse createOMDSPackageResponse() {
        return new OMDSPackageResponse();
    }

    /**
     * Create an instance of {@link ArcImageInfosRequest }
     * 
     */
    public ArcImageInfosRequest createArcImageInfosRequest() {
        return new ArcImageInfosRequest();
    }

    /**
     * Create an instance of {@link ArcImageInfosResponse }
     * 
     */
    public ArcImageInfosResponse createArcImageInfosResponse() {
        return new ArcImageInfosResponse();
    }

    /**
     * Create an instance of {@link ArcImageRequest }
     * 
     */
    public ArcImageRequest createArcImageRequest() {
        return new ArcImageRequest();
    }

    /**
     * Create an instance of {@link ArcImageResponse }
     * 
     */
    public ArcImageResponse createArcImageResponse() {
        return new ArcImageResponse();
    }

    /**
     * Create an instance of {@link DeepLinkClaimRequest }
     * 
     */
    public DeepLinkClaimRequest createDeepLinkClaimRequest() {
        return new DeepLinkClaimRequest();
    }

    /**
     * Create an instance of {@link DeepLinkPartnerRequest }
     * 
     */
    public DeepLinkPartnerRequest createDeepLinkPartnerRequest() {
        return new DeepLinkPartnerRequest();
    }

    /**
     * Create an instance of {@link DeepLinkOfferRequest }
     * 
     */
    public DeepLinkOfferRequest createDeepLinkOfferRequest() {
        return new DeepLinkOfferRequest();
    }

    /**
     * Create an instance of {@link DeepLinkPolicyRequest }
     * 
     */
    public DeepLinkPolicyRequest createDeepLinkPolicyRequest() {
        return new DeepLinkPolicyRequest();
    }

    /**
     * Create an instance of {@link HttpActionLinkType }
     * 
     */
    public HttpActionLinkType createHttpActionLinkType() {
        return new HttpActionLinkType();
    }

    /**
     * Create an instance of {@link CreateClaimRequestType }
     * 
     */
    public CreateClaimRequestType createCreateClaimRequestType() {
        return new CreateClaimRequestType();
    }

    /**
     * Create an instance of {@link InformationenPersonType }
     * 
     */
    public InformationenPersonType createInformationenPersonType() {
        return new InformationenPersonType();
    }

    /**
     * Create an instance of {@link CreateClaimResponseType }
     * 
     */
    public CreateClaimResponseType createCreateClaimResponseType() {
        return new CreateClaimResponseType();
    }

    /**
     * Create an instance of {@link InitiateClaimRequestType }
     * 
     */
    public InitiateClaimRequestType createInitiateClaimRequestType() {
        return new InitiateClaimRequestType();
    }

    /**
     * Create an instance of {@link InitiateClaimResponseType }
     * 
     */
    public InitiateClaimResponseType createInitiateClaimResponseType() {
        return new InitiateClaimResponseType();
    }

    /**
     * Create an instance of {@link AddDocToClaimRequestType }
     * 
     */
    public AddDocToClaimRequestType createAddDocToClaimRequestType() {
        return new AddDocToClaimRequestType();
    }

    /**
     * Create an instance of {@link AddDocToClaimResponseType }
     * 
     */
    public AddDocToClaimResponseType createAddDocToClaimResponseType() {
        return new AddDocToClaimResponseType();
    }

    /**
     * Create an instance of {@link SpezifikationSchadenType }
     * 
     */
    public SpezifikationSchadenType createSpezifikationSchadenType() {
        return new SpezifikationSchadenType();
    }

    /**
     * Create an instance of {@link GetClaimResponseType }
     * 
     */
    public GetClaimResponseType createGetClaimResponseType() {
        return new GetClaimResponseType();
    }

    /**
     * Create an instance of {@link GetClaimResponseLightType }
     * 
     */
    public GetClaimResponseLightType createGetClaimResponseLightType() {
        return new GetClaimResponseLightType();
    }

    /**
     * Create an instance of {@link GetNumberOfDocumentsRequestType }
     * 
     */
    public GetNumberOfDocumentsRequestType createGetNumberOfDocumentsRequestType() {
        return new GetNumberOfDocumentsRequestType();
    }

    /**
     * Create an instance of {@link GetNumberOfDocumentsResponseType }
     * 
     */
    public GetNumberOfDocumentsResponseType createGetNumberOfDocumentsResponseType() {
        return new GetNumberOfDocumentsResponseType();
    }

    /**
     * Create an instance of {@link GetDocumentInfosRequestType }
     * 
     */
    public GetDocumentInfosRequestType createGetDocumentInfosRequestType() {
        return new GetDocumentInfosRequestType();
    }

    /**
     * Create an instance of {@link SearchClaimRequestType }
     * 
     */
    public SearchClaimRequestType createSearchClaimRequestType() {
        return new SearchClaimRequestType();
    }

    /**
     * Create an instance of {@link SchadenzuordnungType }
     * 
     */
    public SchadenzuordnungType createSchadenzuordnungType() {
        return new SchadenzuordnungType();
    }

    /**
     * Create an instance of {@link ChangedClaimsListRequestType }
     * 
     */
    public ChangedClaimsListRequestType createChangedClaimsListRequestType() {
        return new ChangedClaimsListRequestType();
    }

    /**
     * Create an instance of {@link LossEventListRequestType }
     * 
     */
    public LossEventListRequestType createLossEventListRequestType() {
        return new LossEventListRequestType();
    }

    /**
     * Create an instance of {@link DeclareEndpointRequestType }
     * 
     */
    public DeclareEndpointRequestType createDeclareEndpointRequestType() {
        return new DeclareEndpointRequestType();
    }

    /**
     * Create an instance of {@link DeclareEndpointResponseType }
     * 
     */
    public DeclareEndpointResponseType createDeclareEndpointResponseType() {
        return new DeclareEndpointResponseType();
    }

    /**
     * Create an instance of {@link SecurityContextTokenRequestType }
     * 
     */
    public SecurityContextTokenRequestType createSecurityContextTokenRequestType() {
        return new SecurityContextTokenRequestType();
    }

    /**
     * Create an instance of {@link SecurityContextTokenResponseType }
     * 
     */
    public SecurityContextTokenResponseType createSecurityContextTokenResponseType() {
        return new SecurityContextTokenResponseType();
    }

    /**
     * Create an instance of {@link DeclareNewClaimStatusRequestType }
     * 
     */
    public DeclareNewClaimStatusRequestType createDeclareNewClaimStatusRequestType() {
        return new DeclareNewClaimStatusRequestType();
    }

    /**
     * Create an instance of {@link DeclareNewClaimStatusResponseType }
     * 
     */
    public DeclareNewClaimStatusResponseType createDeclareNewClaimStatusResponseType() {
        return new DeclareNewClaimStatusResponseType();
    }

    /**
     * Create an instance of {@link LossEventType }
     * 
     */
    public LossEventType createLossEventType() {
        return new LossEventType();
    }

    /**
     * Create an instance of {@link LossEventRegisteredResponseType }
     * 
     */
    public LossEventRegisteredResponseType createLossEventRegisteredResponseType() {
        return new LossEventRegisteredResponseType();
    }

    /**
     * Create an instance of {@link OMDSPackageInfoType }
     * 
     */
    public OMDSPackageInfoType createOMDSPackageInfoType() {
        return new OMDSPackageInfoType();
    }

    /**
     * Create an instance of {@link PolicyPartnerRole }
     * 
     */
    public PolicyPartnerRole createPolicyPartnerRole() {
        return new PolicyPartnerRole();
    }

    /**
     * Create an instance of {@link ArcImageInfo }
     * 
     */
    public ArcImageInfo createArcImageInfo() {
        return new ArcImageInfo();
    }

    /**
     * Create an instance of {@link ArcContent }
     * 
     */
    public ArcContent createArcContent() {
        return new ArcContent();
    }

    /**
     * Create an instance of {@link BeteiligtePersonType }
     * 
     */
    public BeteiligtePersonType createBeteiligtePersonType() {
        return new BeteiligtePersonType();
    }

    /**
     * Create an instance of {@link SpartendetailSchadenKfzType }
     * 
     */
    public SpartendetailSchadenKfzType createSpartendetailSchadenKfzType() {
        return new SpartendetailSchadenKfzType();
    }

    /**
     * Create an instance of {@link GeschaedigtesInteresseType }
     * 
     */
    public GeschaedigtesInteresseType createGeschaedigtesInteresseType() {
        return new GeschaedigtesInteresseType();
    }

    /**
     * Create an instance of {@link GeschaedigtesObjektKfzType }
     * 
     */
    public GeschaedigtesObjektKfzType createGeschaedigtesObjektKfzType() {
        return new GeschaedigtesObjektKfzType();
    }

    /**
     * Create an instance of {@link GeschaedigtesObjektImmobilieType }
     * 
     */
    public GeschaedigtesObjektImmobilieType createGeschaedigtesObjektImmobilieType() {
        return new GeschaedigtesObjektImmobilieType();
    }

    /**
     * Create an instance of {@link SchadenmelderType }
     * 
     */
    public SchadenmelderType createSchadenmelderType() {
        return new SchadenmelderType();
    }

    /**
     * Create an instance of {@link SchadenmelderVermittlerType }
     * 
     */
    public SchadenmelderVermittlerType createSchadenmelderVermittlerType() {
        return new SchadenmelderVermittlerType();
    }

    /**
     * Create an instance of {@link UploadDokumentType }
     * 
     */
    public UploadDokumentType createUploadDokumentType() {
        return new UploadDokumentType();
    }

    /**
     * Create an instance of {@link SchadenereignisLightType }
     * 
     */
    public SchadenereignisLightType createSchadenereignisLightType() {
        return new SchadenereignisLightType();
    }

    /**
     * Create an instance of {@link SchadenLightType }
     * 
     */
    public SchadenLightType createSchadenLightType() {
        return new SchadenLightType();
    }

    /**
     * Create an instance of {@link SchadenObjektSpezifikationType }
     * 
     */
    public SchadenObjektSpezifikationType createSchadenObjektSpezifikationType() {
        return new SchadenObjektSpezifikationType();
    }

    /**
     * Create an instance of {@link PolizzenObjektSpezifikationType }
     * 
     */
    public PolizzenObjektSpezifikationType createPolizzenObjektSpezifikationType() {
        return new PolizzenObjektSpezifikationType();
    }

    /**
     * Create an instance of {@link UsernamePasswordCredentialsType }
     * 
     */
    public UsernamePasswordCredentialsType createUsernamePasswordCredentialsType() {
        return new UsernamePasswordCredentialsType();
    }

    /**
     * Create an instance of {@link SchadenStatusType.Schaeden }
     * 
     */
    public SchadenStatusType.Schaeden createSchadenStatusTypeSchaeden() {
        return new SchadenStatusType.Schaeden();
    }

    /**
     * Create an instance of {@link SachbearbVUType.Person }
     * 
     */
    public SachbearbVUType.Person createSachbearbVUTypePerson() {
        return new SachbearbVUType.Person();
    }

    /**
     * Create an instance of {@link SchadenereignisType.BeteiligtePersonen }
     * 
     */
    public SchadenereignisType.BeteiligtePersonen createSchadenereignisTypeBeteiligtePersonen() {
        return new SchadenereignisType.BeteiligtePersonen();
    }

    /**
     * Create an instance of {@link MeldungszusammenfassungInitiateClaimType.ErgebnisDokumente }
     * 
     */
    public MeldungszusammenfassungInitiateClaimType.ErgebnisDokumente createMeldungszusammenfassungInitiateClaimTypeErgebnisDokumente() {
        return new MeldungszusammenfassungInitiateClaimType.ErgebnisDokumente();
    }

    /**
     * Create an instance of {@link MeldungszusammenfassungInitiateClaimType.Schaeden }
     * 
     */
    public MeldungszusammenfassungInitiateClaimType.Schaeden createMeldungszusammenfassungInitiateClaimTypeSchaeden() {
        return new MeldungszusammenfassungInitiateClaimType.Schaeden();
    }

    /**
     * Create an instance of {@link MeldungszusammenfassungType.ErgebnisDokumente }
     * 
     */
    public MeldungszusammenfassungType.ErgebnisDokumente createMeldungszusammenfassungTypeErgebnisDokumente() {
        return new MeldungszusammenfassungType.ErgebnisDokumente();
    }

    /**
     * Create an instance of {@link MeldungszusammenfassungType.ErgebnisSchaeden.Schadenanlage }
     * 
     */
    public MeldungszusammenfassungType.ErgebnisSchaeden.Schadenanlage createMeldungszusammenfassungTypeErgebnisSchaedenSchadenanlage() {
        return new MeldungszusammenfassungType.ErgebnisSchaeden.Schadenanlage();
    }

    /**
     * Create an instance of {@link SchadenType.Schadenbeteiligte }
     * 
     */
    public SchadenType.Schadenbeteiligte createSchadenTypeSchadenbeteiligte() {
        return new SchadenType.Schadenbeteiligte();
    }

    /**
     * Create an instance of {@link MeldungSchadenType.Schadenbeteiligte }
     * 
     */
    public MeldungSchadenType.Schadenbeteiligte createMeldungSchadenTypeSchadenbeteiligte() {
        return new MeldungSchadenType.Schadenbeteiligte();
    }

    /**
     * Create an instance of {@link OrtType.Geokoordinaten }
     * 
     */
    public OrtType.Geokoordinaten createOrtTypeGeokoordinaten() {
        return new OrtType.Geokoordinaten();
    }

    /**
     * Create an instance of {@link RequestedOMDSPackage.OmdsPackage }
     * 
     */
    public RequestedOMDSPackage.OmdsPackage createRequestedOMDSPackageOmdsPackage() {
        return new RequestedOMDSPackage.OmdsPackage();
    }

    /**
     * Create an instance of {@link LossEventListResponseType.Result }
     * 
     */
    public LossEventListResponseType.Result createLossEventListResponseTypeResult() {
        return new LossEventListResponseType.Result();
    }

    /**
     * Create an instance of {@link ChangedClaimsListResponseType.Result }
     * 
     */
    public ChangedClaimsListResponseType.Result createChangedClaimsListResponseTypeResult() {
        return new ChangedClaimsListResponseType.Result();
    }

    /**
     * Create an instance of {@link SearchClaimResponseType.Result }
     * 
     */
    public SearchClaimResponseType.Result createSearchClaimResponseTypeResult() {
        return new SearchClaimResponseType.Result();
    }

    /**
     * Create an instance of {@link GetDocumentInfosResponseType.Result }
     * 
     */
    public GetDocumentInfosResponseType.Result createGetDocumentInfosResponseTypeResult() {
        return new GetDocumentInfosResponseType.Result();
    }

    /**
     * Create an instance of {@link UserDataResponse.AvailableServices }
     * 
     */
    public UserDataResponse.AvailableServices createUserDataResponseAvailableServices() {
        return new UserDataResponse.AvailableServices();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoginRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds3ServiceTypes-1-1-0", name = "loginRequest")
    public JAXBElement<LoginRequestType> createLoginRequest(LoginRequestType value) {
        return new JAXBElement<LoginRequestType>(_LoginRequest_QNAME, LoginRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeepLinkBusinessObjectResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds3ServiceTypes-1-1-0", name = "loginResponse")
    public JAXBElement<DeepLinkBusinessObjectResponse> createLoginResponse(DeepLinkBusinessObjectResponse value) {
        return new JAXBElement<DeepLinkBusinessObjectResponse>(_LoginResponse_QNAME, DeepLinkBusinessObjectResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UserDataRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds3ServiceTypes-1-1-0", name = "getUserDataRequest")
    public JAXBElement<UserDataRequest> createGetUserDataRequest(UserDataRequest value) {
        return new JAXBElement<UserDataRequest>(_GetUserDataRequest_QNAME, UserDataRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UserDataResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds3ServiceTypes-1-1-0", name = "getUserDataResponse")
    public JAXBElement<UserDataResponse> createGetUserDataResponse(UserDataResponse value) {
        return new JAXBElement<UserDataResponse>(_GetUserDataResponse_QNAME, UserDataResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OMDSPackageListRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds3ServiceTypes-1-1-0", name = "getOMDSPackageListRequest")
    public JAXBElement<OMDSPackageListRequest> createGetOMDSPackageListRequest(OMDSPackageListRequest value) {
        return new JAXBElement<OMDSPackageListRequest>(_GetOMDSPackageListRequest_QNAME, OMDSPackageListRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OMDSPackageListResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds3ServiceTypes-1-1-0", name = "getOMDSPackageListResponse")
    public JAXBElement<OMDSPackageListResponse> createGetOMDSPackageListResponse(OMDSPackageListResponse value) {
        return new JAXBElement<OMDSPackageListResponse>(_GetOMDSPackageListResponse_QNAME, OMDSPackageListResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OMDSPackageRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds3ServiceTypes-1-1-0", name = "getOMDSPackageRequest")
    public JAXBElement<OMDSPackageRequest> createGetOMDSPackageRequest(OMDSPackageRequest value) {
        return new JAXBElement<OMDSPackageRequest>(_GetOMDSPackageRequest_QNAME, OMDSPackageRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OMDSPackageResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds3ServiceTypes-1-1-0", name = "getOMDSPackageResponse")
    public JAXBElement<OMDSPackageResponse> createGetOMDSPackageResponse(OMDSPackageResponse value) {
        return new JAXBElement<OMDSPackageResponse>(_GetOMDSPackageResponse_QNAME, OMDSPackageResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArcImageInfosRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds3ServiceTypes-1-1-0", name = "getArcImageInfosRequest")
    public JAXBElement<ArcImageInfosRequest> createGetArcImageInfosRequest(ArcImageInfosRequest value) {
        return new JAXBElement<ArcImageInfosRequest>(_GetArcImageInfosRequest_QNAME, ArcImageInfosRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArcImageInfosResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds3ServiceTypes-1-1-0", name = "getArcImageInfosResponse")
    public JAXBElement<ArcImageInfosResponse> createGetArcImageInfosResponse(ArcImageInfosResponse value) {
        return new JAXBElement<ArcImageInfosResponse>(_GetArcImageInfosResponse_QNAME, ArcImageInfosResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArcImageRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds3ServiceTypes-1-1-0", name = "getArcImageRequest")
    public JAXBElement<ArcImageRequest> createGetArcImageRequest(ArcImageRequest value) {
        return new JAXBElement<ArcImageRequest>(_GetArcImageRequest_QNAME, ArcImageRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArcImageResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds3ServiceTypes-1-1-0", name = "getArcImageResponse")
    public JAXBElement<ArcImageResponse> createGetArcImageResponse(ArcImageResponse value) {
        return new JAXBElement<ArcImageResponse>(_GetArcImageResponse_QNAME, ArcImageResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeepLinkClaimRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds3ServiceTypes-1-1-0", name = "getDeepLinkClaimRequest")
    public JAXBElement<DeepLinkClaimRequest> createGetDeepLinkClaimRequest(DeepLinkClaimRequest value) {
        return new JAXBElement<DeepLinkClaimRequest>(_GetDeepLinkClaimRequest_QNAME, DeepLinkClaimRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeepLinkBusinessObjectResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds3ServiceTypes-1-1-0", name = "getDeepLinkClaimResponse")
    public JAXBElement<DeepLinkBusinessObjectResponse> createGetDeepLinkClaimResponse(DeepLinkBusinessObjectResponse value) {
        return new JAXBElement<DeepLinkBusinessObjectResponse>(_GetDeepLinkClaimResponse_QNAME, DeepLinkBusinessObjectResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeepLinkPartnerRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds3ServiceTypes-1-1-0", name = "getDeepLinkPartnerRequest")
    public JAXBElement<DeepLinkPartnerRequest> createGetDeepLinkPartnerRequest(DeepLinkPartnerRequest value) {
        return new JAXBElement<DeepLinkPartnerRequest>(_GetDeepLinkPartnerRequest_QNAME, DeepLinkPartnerRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeepLinkBusinessObjectResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds3ServiceTypes-1-1-0", name = "getDeepLinkPartnerResponse")
    public JAXBElement<DeepLinkBusinessObjectResponse> createGetDeepLinkPartnerResponse(DeepLinkBusinessObjectResponse value) {
        return new JAXBElement<DeepLinkBusinessObjectResponse>(_GetDeepLinkPartnerResponse_QNAME, DeepLinkBusinessObjectResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeepLinkOfferRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds3ServiceTypes-1-1-0", name = "getDeepLinkOfferRequest")
    public JAXBElement<DeepLinkOfferRequest> createGetDeepLinkOfferRequest(DeepLinkOfferRequest value) {
        return new JAXBElement<DeepLinkOfferRequest>(_GetDeepLinkOfferRequest_QNAME, DeepLinkOfferRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeepLinkBusinessObjectResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds3ServiceTypes-1-1-0", name = "getDeepLinkOfferResponse")
    public JAXBElement<DeepLinkBusinessObjectResponse> createGetDeepLinkOfferResponse(DeepLinkBusinessObjectResponse value) {
        return new JAXBElement<DeepLinkBusinessObjectResponse>(_GetDeepLinkOfferResponse_QNAME, DeepLinkBusinessObjectResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeepLinkPolicyRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds3ServiceTypes-1-1-0", name = "getDeepLinkPolicyRequest")
    public JAXBElement<DeepLinkPolicyRequest> createGetDeepLinkPolicyRequest(DeepLinkPolicyRequest value) {
        return new JAXBElement<DeepLinkPolicyRequest>(_GetDeepLinkPolicyRequest_QNAME, DeepLinkPolicyRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeepLinkBusinessObjectResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds3ServiceTypes-1-1-0", name = "getDeepLinkPolicyResponse")
    public JAXBElement<DeepLinkBusinessObjectResponse> createGetDeepLinkPolicyResponse(DeepLinkBusinessObjectResponse value) {
        return new JAXBElement<DeepLinkBusinessObjectResponse>(_GetDeepLinkPolicyResponse_QNAME, DeepLinkBusinessObjectResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeepLinkBusinessObjectResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds3ServiceTypes-1-1-0", name = "getDeepLinkBusinessObjectResponse")
    public JAXBElement<DeepLinkBusinessObjectResponse> createGetDeepLinkBusinessObjectResponse(DeepLinkBusinessObjectResponse value) {
        return new JAXBElement<DeepLinkBusinessObjectResponse>(_GetDeepLinkBusinessObjectResponse_QNAME, DeepLinkBusinessObjectResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds3ServiceTypes-1-1-0", name = "withoutFrame")
    public JAXBElement<Boolean> createWithoutFrame(Boolean value) {
        return new JAXBElement<Boolean>(_WithoutFrame_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HttpActionLinkType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds3ServiceTypes-1-1-0", name = "httpActionLink")
    public JAXBElement<HttpActionLinkType> createHttpActionLink(HttpActionLinkType value) {
        return new JAXBElement<HttpActionLinkType>(_HttpActionLink_QNAME, HttpActionLinkType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateClaimRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds3ServiceTypes-1-1-0", name = "CreateClaimRequest")
    public JAXBElement<CreateClaimRequestType> createCreateClaimRequest(CreateClaimRequestType value) {
        return new JAXBElement<CreateClaimRequestType>(_CreateClaimRequest_QNAME, CreateClaimRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InformationenPersonType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds3ServiceTypes-1-1-0", name = "InformationenPerson")
    public JAXBElement<InformationenPersonType> createInformationenPerson(InformationenPersonType value) {
        return new JAXBElement<InformationenPersonType>(_InformationenPerson_QNAME, InformationenPersonType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateClaimResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds3ServiceTypes-1-1-0", name = "CreateClaimResponse")
    public JAXBElement<CreateClaimResponseType> createCreateClaimResponse(CreateClaimResponseType value) {
        return new JAXBElement<CreateClaimResponseType>(_CreateClaimResponse_QNAME, CreateClaimResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InitiateClaimRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds3ServiceTypes-1-1-0", name = "InitiateClaimRequest")
    public JAXBElement<InitiateClaimRequestType> createInitiateClaimRequest(InitiateClaimRequestType value) {
        return new JAXBElement<InitiateClaimRequestType>(_InitiateClaimRequest_QNAME, InitiateClaimRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InitiateClaimResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds3ServiceTypes-1-1-0", name = "InitiateClaimResponse")
    public JAXBElement<InitiateClaimResponseType> createInitiateClaimResponse(InitiateClaimResponseType value) {
        return new JAXBElement<InitiateClaimResponseType>(_InitiateClaimResponse_QNAME, InitiateClaimResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddDocToClaimRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds3ServiceTypes-1-1-0", name = "AddDocToClaimRequest")
    public JAXBElement<AddDocToClaimRequestType> createAddDocToClaimRequest(AddDocToClaimRequestType value) {
        return new JAXBElement<AddDocToClaimRequestType>(_AddDocToClaimRequest_QNAME, AddDocToClaimRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddDocToClaimResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds3ServiceTypes-1-1-0", name = "AddDocToClaimResponse")
    public JAXBElement<AddDocToClaimResponseType> createAddDocToClaimResponse(AddDocToClaimResponseType value) {
        return new JAXBElement<AddDocToClaimResponseType>(_AddDocToClaimResponse_QNAME, AddDocToClaimResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SpezifikationSchadenType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds3ServiceTypes-1-1-0", name = "GetClaimRequest")
    public JAXBElement<SpezifikationSchadenType> createGetClaimRequest(SpezifikationSchadenType value) {
        return new JAXBElement<SpezifikationSchadenType>(_GetClaimRequest_QNAME, SpezifikationSchadenType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetClaimResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds3ServiceTypes-1-1-0", name = "GetClaimResponse")
    public JAXBElement<GetClaimResponseType> createGetClaimResponse(GetClaimResponseType value) {
        return new JAXBElement<GetClaimResponseType>(_GetClaimResponse_QNAME, GetClaimResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SpezifikationSchadenType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds3ServiceTypes-1-1-0", name = "GetClaimLightRequest")
    public JAXBElement<SpezifikationSchadenType> createGetClaimLightRequest(SpezifikationSchadenType value) {
        return new JAXBElement<SpezifikationSchadenType>(_GetClaimLightRequest_QNAME, SpezifikationSchadenType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetClaimResponseLightType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds3ServiceTypes-1-1-0", name = "GetClaimLightResponse")
    public JAXBElement<GetClaimResponseLightType> createGetClaimLightResponse(GetClaimResponseLightType value) {
        return new JAXBElement<GetClaimResponseLightType>(_GetClaimLightResponse_QNAME, GetClaimResponseLightType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetNumberOfDocumentsRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds3ServiceTypes-1-1-0", name = "GetNumberOfDocumentsRequest")
    public JAXBElement<GetNumberOfDocumentsRequestType> createGetNumberOfDocumentsRequest(GetNumberOfDocumentsRequestType value) {
        return new JAXBElement<GetNumberOfDocumentsRequestType>(_GetNumberOfDocumentsRequest_QNAME, GetNumberOfDocumentsRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetNumberOfDocumentsResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds3ServiceTypes-1-1-0", name = "GetNumberOfDocumentsResponse")
    public JAXBElement<GetNumberOfDocumentsResponseType> createGetNumberOfDocumentsResponse(GetNumberOfDocumentsResponseType value) {
        return new JAXBElement<GetNumberOfDocumentsResponseType>(_GetNumberOfDocumentsResponse_QNAME, GetNumberOfDocumentsResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDocumentInfosRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds3ServiceTypes-1-1-0", name = "GetDocumentInfosRequest")
    public JAXBElement<GetDocumentInfosRequestType> createGetDocumentInfosRequest(GetDocumentInfosRequestType value) {
        return new JAXBElement<GetDocumentInfosRequestType>(_GetDocumentInfosRequest_QNAME, GetDocumentInfosRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDocumentInfosResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds3ServiceTypes-1-1-0", name = "GetDocumentInfosResponse")
    public JAXBElement<GetDocumentInfosResponseType> createGetDocumentInfosResponse(GetDocumentInfosResponseType value) {
        return new JAXBElement<GetDocumentInfosResponseType>(_GetDocumentInfosResponse_QNAME, GetDocumentInfosResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchClaimRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds3ServiceTypes-1-1-0", name = "SearchClaimRequest")
    public JAXBElement<SearchClaimRequestType> createSearchClaimRequest(SearchClaimRequestType value) {
        return new JAXBElement<SearchClaimRequestType>(_SearchClaimRequest_QNAME, SearchClaimRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchClaimResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds3ServiceTypes-1-1-0", name = "SearchClaimResponse")
    public JAXBElement<SearchClaimResponseType> createSearchClaimResponse(SearchClaimResponseType value) {
        return new JAXBElement<SearchClaimResponseType>(_SearchClaimResponse_QNAME, SearchClaimResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SchadenzuordnungType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds3ServiceTypes-1-1-0", name = "Schadenzuordnung")
    public JAXBElement<SchadenzuordnungType> createSchadenzuordnung(SchadenzuordnungType value) {
        return new JAXBElement<SchadenzuordnungType>(_Schadenzuordnung_QNAME, SchadenzuordnungType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangedClaimsListRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds3ServiceTypes-1-1-0", name = "ChangedClaimsListRequest")
    public JAXBElement<ChangedClaimsListRequestType> createChangedClaimsListRequest(ChangedClaimsListRequestType value) {
        return new JAXBElement<ChangedClaimsListRequestType>(_ChangedClaimsListRequest_QNAME, ChangedClaimsListRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangedClaimsListResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds3ServiceTypes-1-1-0", name = "ChangedClaimsListResponse")
    public JAXBElement<ChangedClaimsListResponseType> createChangedClaimsListResponse(ChangedClaimsListResponseType value) {
        return new JAXBElement<ChangedClaimsListResponseType>(_ChangedClaimsListResponse_QNAME, ChangedClaimsListResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LossEventListRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds3ServiceTypes-1-1-0", name = "LossEventListRequest")
    public JAXBElement<LossEventListRequestType> createLossEventListRequest(LossEventListRequestType value) {
        return new JAXBElement<LossEventListRequestType>(_LossEventListRequest_QNAME, LossEventListRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LossEventListResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds3ServiceTypes-1-1-0", name = "LossEventListResponse")
    public JAXBElement<LossEventListResponseType> createLossEventListResponse(LossEventListResponseType value) {
        return new JAXBElement<LossEventListResponseType>(_LossEventListResponse_QNAME, LossEventListResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds3ServiceTypes-1-1-0", name = "IdGeschaeftsfallSchadenereignis")
    public JAXBElement<String> createIdGeschaeftsfallSchadenereignis(String value) {
        return new JAXBElement<String>(_IdGeschaeftsfallSchadenereignis_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds3ServiceTypes-1-1-0", name = "IdGeschaeftsfallSchadenanlage")
    public JAXBElement<String> createIdGeschaeftsfallSchadenanlage(String value) {
        return new JAXBElement<String>(_IdGeschaeftsfallSchadenanlage_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeclareEndpointRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds3ServiceTypes-1-1-0", name = "DeclareEndpointRequest")
    public JAXBElement<DeclareEndpointRequestType> createDeclareEndpointRequest(DeclareEndpointRequestType value) {
        return new JAXBElement<DeclareEndpointRequestType>(_DeclareEndpointRequest_QNAME, DeclareEndpointRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds3ServiceTypes-1-1-0", name = "ArtAuthentifizierung")
    public JAXBElement<String> createArtAuthentifizierung(String value) {
        return new JAXBElement<String>(_ArtAuthentifizierung_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeclareEndpointResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds3ServiceTypes-1-1-0", name = "DeclareEndpointResponse")
    public JAXBElement<DeclareEndpointResponseType> createDeclareEndpointResponse(DeclareEndpointResponseType value) {
        return new JAXBElement<DeclareEndpointResponseType>(_DeclareEndpointResponse_QNAME, DeclareEndpointResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SecurityContextTokenRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds3ServiceTypes-1-1-0", name = "SecurityContextTokenRequest")
    public JAXBElement<SecurityContextTokenRequestType> createSecurityContextTokenRequest(SecurityContextTokenRequestType value) {
        return new JAXBElement<SecurityContextTokenRequestType>(_SecurityContextTokenRequest_QNAME, SecurityContextTokenRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SecurityContextTokenResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds3ServiceTypes-1-1-0", name = "SecurityContextTokenResponse")
    public JAXBElement<SecurityContextTokenResponseType> createSecurityContextTokenResponse(SecurityContextTokenResponseType value) {
        return new JAXBElement<SecurityContextTokenResponseType>(_SecurityContextTokenResponse_QNAME, SecurityContextTokenResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeclareNewClaimStatusRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds3ServiceTypes-1-1-0", name = "DeclareNewClaimStatusRequest")
    public JAXBElement<DeclareNewClaimStatusRequestType> createDeclareNewClaimStatusRequest(DeclareNewClaimStatusRequestType value) {
        return new JAXBElement<DeclareNewClaimStatusRequestType>(_DeclareNewClaimStatusRequest_QNAME, DeclareNewClaimStatusRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeclareNewClaimStatusResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds3ServiceTypes-1-1-0", name = "DeclareNewClaimStatusResponse")
    public JAXBElement<DeclareNewClaimStatusResponseType> createDeclareNewClaimStatusResponse(DeclareNewClaimStatusResponseType value) {
        return new JAXBElement<DeclareNewClaimStatusResponseType>(_DeclareNewClaimStatusResponse_QNAME, DeclareNewClaimStatusResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LossEventType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds3ServiceTypes-1-1-0", name = "LossEventRegisteredRequest")
    public JAXBElement<LossEventType> createLossEventRegisteredRequest(LossEventType value) {
        return new JAXBElement<LossEventType>(_LossEventRegisteredRequest_QNAME, LossEventType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LossEventRegisteredResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds3ServiceTypes-1-1-0", name = "LossEventRegisteredResponse")
    public JAXBElement<LossEventRegisteredResponseType> createLossEventRegisteredResponse(LossEventRegisteredResponseType value) {
        return new JAXBElement<LossEventRegisteredResponseType>(_LossEventRegisteredResponse_QNAME, LossEventRegisteredResponseType.class, null, value);
    }

}
