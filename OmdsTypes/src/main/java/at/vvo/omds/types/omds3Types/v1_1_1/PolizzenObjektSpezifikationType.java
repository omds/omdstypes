
package at.vvo.omds.types.omds3Types.v1_1_1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Die Spezifikation eines Polizzenobjektes
 * 
 * <p>Java-Klasse für PolizzenObjektSpezifikation_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="PolizzenObjektSpezifikation_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3ServiceTypes-1-1-0}ObjektSpezifikation_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Polizzennr" type="{urn:omds20}Polizzennr"/&gt;
 *         &lt;element name="VtgProdCd" type="{urn:omds20}VtgProdCd_Type" minOccurs="0"/&gt;
 *         &lt;element name="RollePartner" type="{urn:omds3ServiceTypes-1-1-0}PolicyPartnerRole" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PolizzenObjektSpezifikation_Type", propOrder = {
    "polizzennr",
    "vtgProdCd",
    "rollePartner"
})
public class PolizzenObjektSpezifikationType
    extends ObjektSpezifikationType
{

    @XmlElement(name = "Polizzennr", required = true)
    protected String polizzennr;
    @XmlElement(name = "VtgProdCd")
    protected String vtgProdCd;
    @XmlElement(name = "RollePartner")
    protected List<PolicyPartnerRole> rollePartner;

    /**
     * Ruft den Wert der polizzennr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolizzennr() {
        return polizzennr;
    }

    /**
     * Legt den Wert der polizzennr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolizzennr(String value) {
        this.polizzennr = value;
    }

    /**
     * Ruft den Wert der vtgProdCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVtgProdCd() {
        return vtgProdCd;
    }

    /**
     * Legt den Wert der vtgProdCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVtgProdCd(String value) {
        this.vtgProdCd = value;
    }

    /**
     * Gets the value of the rollePartner property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rollePartner property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRollePartner().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PolicyPartnerRole }
     * 
     * 
     */
    public List<PolicyPartnerRole> getRollePartner() {
        if (rollePartner == null) {
            rollePartner = new ArrayList<PolicyPartnerRole>();
        }
        return this.rollePartner;
    }

}
