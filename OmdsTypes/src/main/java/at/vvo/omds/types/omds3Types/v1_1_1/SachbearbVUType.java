
package at.vvo.omds.types.omds3Types.v1_1_1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds2Types.v2_9.ELKommunikationType;


/**
 * Typ für die Übermittlung von Kontaktdaten eines Sachbearbeiters
 * 
 * <p>Java-Klasse für SachbearbVUType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SachbearbVUType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Person"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;attribute name="Familienname" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *                 &lt;attribute name="Vorname" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *                 &lt;attribute name="GeschlechtCd" use="required" type="{urn:omds20}GeschlechtCd_Type" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element ref="{urn:omds20}EL-Kommunikation" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SachbearbVUType", propOrder = {
    "person",
    "elKommunikation"
})
public class SachbearbVUType {

    @XmlElement(name = "Person", required = true)
    protected SachbearbVUType.Person person;
    @XmlElement(name = "EL-Kommunikation", namespace = "urn:omds20", required = true)
    protected List<ELKommunikationType> elKommunikation;

    /**
     * Ruft den Wert der person-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SachbearbVUType.Person }
     *     
     */
    public SachbearbVUType.Person getPerson() {
        return person;
    }

    /**
     * Legt den Wert der person-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SachbearbVUType.Person }
     *     
     */
    public void setPerson(SachbearbVUType.Person value) {
        this.person = value;
    }

    /**
     * Gets the value of the elKommunikation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the elKommunikation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getELKommunikation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ELKommunikationType }
     * 
     * 
     */
    public List<ELKommunikationType> getELKommunikation() {
        if (elKommunikation == null) {
            elKommunikation = new ArrayList<ELKommunikationType>();
        }
        return this.elKommunikation;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;attribute name="Familienname" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
     *       &lt;attribute name="Vorname" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
     *       &lt;attribute name="GeschlechtCd" use="required" type="{urn:omds20}GeschlechtCd_Type" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Person {

        @XmlAttribute(name = "Familienname", namespace = "urn:omds3ServiceTypes-1-1-0", required = true)
        @XmlSchemaType(name = "anySimpleType")
        protected String familienname;
        @XmlAttribute(name = "Vorname", namespace = "urn:omds3ServiceTypes-1-1-0")
        @XmlSchemaType(name = "anySimpleType")
        protected String vorname;
        @XmlAttribute(name = "GeschlechtCd", namespace = "urn:omds3ServiceTypes-1-1-0", required = true)
        protected String geschlechtCd;

        /**
         * Ruft den Wert der familienname-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFamilienname() {
            return familienname;
        }

        /**
         * Legt den Wert der familienname-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFamilienname(String value) {
            this.familienname = value;
        }

        /**
         * Ruft den Wert der vorname-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVorname() {
            return vorname;
        }

        /**
         * Legt den Wert der vorname-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVorname(String value) {
            this.vorname = value;
        }

        /**
         * Ruft den Wert der geschlechtCd-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getGeschlechtCd() {
            return geschlechtCd;
        }

        /**
         * Legt den Wert der geschlechtCd-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setGeschlechtCd(String value) {
            this.geschlechtCd = value;
        }

    }

}
