
package at.vvo.omds.types.omds3Types.v1_1_1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Typ zur leichtgewichtigen Abbildung von Schadenobjekten 
 * 
 * <p>Java-Klasse für SchadenLight_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SchadenLight_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="BearbStandCd" type="{urn:omds3ServiceTypes-1-1-0}BearbStandCd_Type"/&gt;
 *         &lt;element ref="{urn:omds3ServiceTypes-1-1-0}IdGeschaeftsfallSchadenanlage"/&gt;
 *         &lt;element name="Schadennr" type="{urn:omds20}Schadennr" minOccurs="0"/&gt;
 *         &lt;element name="VormaligeSchadennr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NachfolgendeSchadennr" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="SachbearbVU" type="{urn:omds3ServiceTypes-1-1-0}SachbearbVUType" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:omds3ServiceTypes-1-1-0}Schadenzuordnung" minOccurs="0"/&gt;
 *         &lt;element name="Polizzennr" type="{urn:omds20}Polizzennr"/&gt;
 *         &lt;element name="VertragsID" type="{urn:omds20}VertragsID" minOccurs="0"/&gt;
 *         &lt;element name="SpartenCd" type="{urn:omds20}SpartenCd_Type" minOccurs="0"/&gt;
 *         &lt;element name="Spartenerweiterung" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="10"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="SchadUrsCd" type="{urn:omds20}SchadUrsCd_Type" minOccurs="0"/&gt;
 *         &lt;element name="SchadUrsTxt" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="40"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="ErledDat" type="{urn:omds20}Datum" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SchadenLight_Type", propOrder = {
    "bearbStandCd",
    "idGeschaeftsfallSchadenanlage",
    "schadennr",
    "vormaligeSchadennr",
    "nachfolgendeSchadennr",
    "sachbearbVU",
    "schadenzuordnung",
    "polizzennr",
    "vertragsID",
    "spartenCd",
    "spartenerweiterung",
    "schadUrsCd",
    "schadUrsTxt",
    "erledDat"
})
public class SchadenLightType {

    @XmlElement(name = "BearbStandCd", required = true)
    protected String bearbStandCd;
    @XmlElement(name = "IdGeschaeftsfallSchadenanlage", required = true)
    protected String idGeschaeftsfallSchadenanlage;
    @XmlElement(name = "Schadennr")
    protected String schadennr;
    @XmlElement(name = "VormaligeSchadennr")
    protected String vormaligeSchadennr;
    @XmlElement(name = "NachfolgendeSchadennr")
    protected List<String> nachfolgendeSchadennr;
    @XmlElement(name = "SachbearbVU")
    protected SachbearbVUType sachbearbVU;
    @XmlElement(name = "Schadenzuordnung")
    protected SchadenzuordnungType schadenzuordnung;
    @XmlElement(name = "Polizzennr", required = true)
    protected String polizzennr;
    @XmlElement(name = "VertragsID")
    protected String vertragsID;
    @XmlElement(name = "SpartenCd")
    protected String spartenCd;
    @XmlElement(name = "Spartenerweiterung")
    protected String spartenerweiterung;
    @XmlElement(name = "SchadUrsCd")
    protected String schadUrsCd;
    @XmlElement(name = "SchadUrsTxt")
    protected String schadUrsTxt;
    @XmlElement(name = "ErledDat")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar erledDat;

    /**
     * Ruft den Wert der bearbStandCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBearbStandCd() {
        return bearbStandCd;
    }

    /**
     * Legt den Wert der bearbStandCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBearbStandCd(String value) {
        this.bearbStandCd = value;
    }

    /**
     * Ruft den Wert der idGeschaeftsfallSchadenanlage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdGeschaeftsfallSchadenanlage() {
        return idGeschaeftsfallSchadenanlage;
    }

    /**
     * Legt den Wert der idGeschaeftsfallSchadenanlage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdGeschaeftsfallSchadenanlage(String value) {
        this.idGeschaeftsfallSchadenanlage = value;
    }

    /**
     * Ruft den Wert der schadennr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSchadennr() {
        return schadennr;
    }

    /**
     * Legt den Wert der schadennr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSchadennr(String value) {
        this.schadennr = value;
    }

    /**
     * Ruft den Wert der vormaligeSchadennr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVormaligeSchadennr() {
        return vormaligeSchadennr;
    }

    /**
     * Legt den Wert der vormaligeSchadennr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVormaligeSchadennr(String value) {
        this.vormaligeSchadennr = value;
    }

    /**
     * Gets the value of the nachfolgendeSchadennr property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the nachfolgendeSchadennr property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNachfolgendeSchadennr().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getNachfolgendeSchadennr() {
        if (nachfolgendeSchadennr == null) {
            nachfolgendeSchadennr = new ArrayList<String>();
        }
        return this.nachfolgendeSchadennr;
    }

    /**
     * Ruft den Wert der sachbearbVU-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SachbearbVUType }
     *     
     */
    public SachbearbVUType getSachbearbVU() {
        return sachbearbVU;
    }

    /**
     * Legt den Wert der sachbearbVU-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SachbearbVUType }
     *     
     */
    public void setSachbearbVU(SachbearbVUType value) {
        this.sachbearbVU = value;
    }

    /**
     * Ruft den Wert der schadenzuordnung-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SchadenzuordnungType }
     *     
     */
    public SchadenzuordnungType getSchadenzuordnung() {
        return schadenzuordnung;
    }

    /**
     * Legt den Wert der schadenzuordnung-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SchadenzuordnungType }
     *     
     */
    public void setSchadenzuordnung(SchadenzuordnungType value) {
        this.schadenzuordnung = value;
    }

    /**
     * Ruft den Wert der polizzennr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolizzennr() {
        return polizzennr;
    }

    /**
     * Legt den Wert der polizzennr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolizzennr(String value) {
        this.polizzennr = value;
    }

    /**
     * Ruft den Wert der vertragsID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVertragsID() {
        return vertragsID;
    }

    /**
     * Legt den Wert der vertragsID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVertragsID(String value) {
        this.vertragsID = value;
    }

    /**
     * Ruft den Wert der spartenCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSpartenCd() {
        return spartenCd;
    }

    /**
     * Legt den Wert der spartenCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSpartenCd(String value) {
        this.spartenCd = value;
    }

    /**
     * Ruft den Wert der spartenerweiterung-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSpartenerweiterung() {
        return spartenerweiterung;
    }

    /**
     * Legt den Wert der spartenerweiterung-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSpartenerweiterung(String value) {
        this.spartenerweiterung = value;
    }

    /**
     * Ruft den Wert der schadUrsCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSchadUrsCd() {
        return schadUrsCd;
    }

    /**
     * Legt den Wert der schadUrsCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSchadUrsCd(String value) {
        this.schadUrsCd = value;
    }

    /**
     * Ruft den Wert der schadUrsTxt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSchadUrsTxt() {
        return schadUrsTxt;
    }

    /**
     * Legt den Wert der schadUrsTxt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSchadUrsTxt(String value) {
        this.schadUrsTxt = value;
    }

    /**
     * Ruft den Wert der erledDat-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getErledDat() {
        return erledDat;
    }

    /**
     * Legt den Wert der erledDat-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setErledDat(XMLGregorianCalendar value) {
        this.erledDat = value;
    }

}
