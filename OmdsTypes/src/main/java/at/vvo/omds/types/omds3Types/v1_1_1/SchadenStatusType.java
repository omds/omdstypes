
package at.vvo.omds.types.omds3Types.v1_1_1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Objekt, welches GeschäftsfallId und Schadennummer sowie den Bearbeitungsstand enthält
 * 
 * <p>Java-Klasse für SchadenStatus_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SchadenStatus_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:omds3ServiceTypes-1-1-0}IdGeschaeftsfallSchadenereignis"/&gt;
 *         &lt;element name="VUNr" type="{urn:omds20}VUNr"/&gt;
 *         &lt;element ref="{urn:omds3CommonServiceTypes-1-1-0}OrdnungsbegriffZuordFremd" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Schaeden" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element ref="{urn:omds3ServiceTypes-1-1-0}IdGeschaeftsfallSchadenanlage"/&gt;
 *                   &lt;element name="BearbStandCd" type="{urn:omds3ServiceTypes-1-1-0}BearbStandCd_Type"/&gt;
 *                   &lt;element name="Schadennr" type="{urn:omds20}Schadennr" minOccurs="0"/&gt;
 *                   &lt;element ref="{urn:omds3ServiceTypes-1-1-0}Schadenzuordnung" minOccurs="0"/&gt;
 *                   &lt;element name="SachbearbVU" type="{urn:omds3ServiceTypes-1-1-0}SachbearbVUType" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SchadenStatus_Type", propOrder = {
    "idGeschaeftsfallSchadenereignis",
    "vuNr",
    "ordnungsbegriffZuordFremd",
    "schaeden"
})
public class SchadenStatusType {

    @XmlElement(name = "IdGeschaeftsfallSchadenereignis", required = true)
    protected String idGeschaeftsfallSchadenereignis;
    @XmlElement(name = "VUNr", required = true)
    protected String vuNr;
    @XmlElement(name = "OrdnungsbegriffZuordFremd", namespace = "urn:omds3CommonServiceTypes-1-1-0")
    protected List<String> ordnungsbegriffZuordFremd;
    @XmlElement(name = "Schaeden")
    protected List<SchadenStatusType.Schaeden> schaeden;

    /**
     * Ruft den Wert der idGeschaeftsfallSchadenereignis-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdGeschaeftsfallSchadenereignis() {
        return idGeschaeftsfallSchadenereignis;
    }

    /**
     * Legt den Wert der idGeschaeftsfallSchadenereignis-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdGeschaeftsfallSchadenereignis(String value) {
        this.idGeschaeftsfallSchadenereignis = value;
    }

    /**
     * Ruft den Wert der vuNr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVUNr() {
        return vuNr;
    }

    /**
     * Legt den Wert der vuNr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVUNr(String value) {
        this.vuNr = value;
    }

    /**
     * Ordnungsbegriff des Schadenmelders auf Ebene des Schadenereignis Gets the value of the ordnungsbegriffZuordFremd property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ordnungsbegriffZuordFremd property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOrdnungsbegriffZuordFremd().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getOrdnungsbegriffZuordFremd() {
        if (ordnungsbegriffZuordFremd == null) {
            ordnungsbegriffZuordFremd = new ArrayList<String>();
        }
        return this.ordnungsbegriffZuordFremd;
    }

    /**
     * Gets the value of the schaeden property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the schaeden property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSchaeden().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SchadenStatusType.Schaeden }
     * 
     * 
     */
    public List<SchadenStatusType.Schaeden> getSchaeden() {
        if (schaeden == null) {
            schaeden = new ArrayList<SchadenStatusType.Schaeden>();
        }
        return this.schaeden;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{urn:omds3ServiceTypes-1-1-0}IdGeschaeftsfallSchadenanlage"/&gt;
     *         &lt;element name="BearbStandCd" type="{urn:omds3ServiceTypes-1-1-0}BearbStandCd_Type"/&gt;
     *         &lt;element name="Schadennr" type="{urn:omds20}Schadennr" minOccurs="0"/&gt;
     *         &lt;element ref="{urn:omds3ServiceTypes-1-1-0}Schadenzuordnung" minOccurs="0"/&gt;
     *         &lt;element name="SachbearbVU" type="{urn:omds3ServiceTypes-1-1-0}SachbearbVUType" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "idGeschaeftsfallSchadenanlage",
        "bearbStandCd",
        "schadennr",
        "schadenzuordnung",
        "sachbearbVU"
    })
    public static class Schaeden {

        @XmlElement(name = "IdGeschaeftsfallSchadenanlage", required = true)
        protected String idGeschaeftsfallSchadenanlage;
        @XmlElement(name = "BearbStandCd", required = true)
        protected String bearbStandCd;
        @XmlElement(name = "Schadennr")
        protected String schadennr;
        @XmlElement(name = "Schadenzuordnung")
        protected SchadenzuordnungType schadenzuordnung;
        @XmlElement(name = "SachbearbVU")
        protected SachbearbVUType sachbearbVU;

        /**
         * Ruft den Wert der idGeschaeftsfallSchadenanlage-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIdGeschaeftsfallSchadenanlage() {
            return idGeschaeftsfallSchadenanlage;
        }

        /**
         * Legt den Wert der idGeschaeftsfallSchadenanlage-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIdGeschaeftsfallSchadenanlage(String value) {
            this.idGeschaeftsfallSchadenanlage = value;
        }

        /**
         * Ruft den Wert der bearbStandCd-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBearbStandCd() {
            return bearbStandCd;
        }

        /**
         * Legt den Wert der bearbStandCd-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBearbStandCd(String value) {
            this.bearbStandCd = value;
        }

        /**
         * Ruft den Wert der schadennr-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSchadennr() {
            return schadennr;
        }

        /**
         * Legt den Wert der schadennr-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSchadennr(String value) {
            this.schadennr = value;
        }

        /**
         * Anhand der Schadenzuordnung kann man erkennen, um welche Schadensparte es sich handelt.
         * 
         * @return
         *     possible object is
         *     {@link SchadenzuordnungType }
         *     
         */
        public SchadenzuordnungType getSchadenzuordnung() {
            return schadenzuordnung;
        }

        /**
         * Legt den Wert der schadenzuordnung-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link SchadenzuordnungType }
         *     
         */
        public void setSchadenzuordnung(SchadenzuordnungType value) {
            this.schadenzuordnung = value;
        }

        /**
         * Ruft den Wert der sachbearbVU-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link SachbearbVUType }
         *     
         */
        public SachbearbVUType getSachbearbVU() {
            return sachbearbVU;
        }

        /**
         * Legt den Wert der sachbearbVU-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link SachbearbVUType }
         *     
         */
        public void setSachbearbVU(SachbearbVUType value) {
            this.sachbearbVU = value;
        }

    }

}
