
package at.vvo.omds.types.omds3Types.v1_1_1;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import at.vvo.omds.types.omds2Types.v2_9.WaehrungsCdType;
import at.vvo.omds.types.omds2Types.v2_9.ZAHLUNGType;


/**
 * Typ um ein Schadenobjekt in der Schadendarstellung abzubilden
 * 
 * <p>Java-Klasse für Schaden_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Schaden_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="VUNr" type="{urn:omds20}VUNr"/&gt;
 *         &lt;element name="BearbStandCd" type="{urn:omds3ServiceTypes-1-1-0}BearbStandCd_Type"/&gt;
 *         &lt;element name="IdGeschaeftsfallSchadenanlage" type="{urn:omds3CommonServiceTypes-1-1-0}GeschaeftsfallId_Type"/&gt;
 *         &lt;element name="Schadennr" type="{urn:omds20}Schadennr" minOccurs="0"/&gt;
 *         &lt;element name="VormaligeSchadennr" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="NachfolgendeSchadennr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Polizzennr" type="{urn:omds20}Polizzennr"/&gt;
 *         &lt;element name="VertragsID" type="{urn:omds20}VertragsID" minOccurs="0"/&gt;
 *         &lt;element name="SachbearbVU" type="{urn:omds3ServiceTypes-1-1-0}SachbearbVUType" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:omds3CommonServiceTypes-1-1-0}OrdnungsbegriffZuordFremd" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:omds3ServiceTypes-1-1-0}Schadenzuordnung" minOccurs="0"/&gt;
 *         &lt;element name="SchadUrsCd" type="{urn:omds20}SchadUrsCd_Type" minOccurs="0"/&gt;
 *         &lt;element name="SchadUrsTxt" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="40"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="ErledDat" type="{urn:omds20}Datum" minOccurs="0"/&gt;
 *         &lt;element name="SchadenTxt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Schadenbeteiligte" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="GeschInteresseLfnr" type="{http://www.w3.org/2001/XMLSchema}unsignedShort" maxOccurs="unbounded" minOccurs="0"/&gt;
 *                   &lt;element ref="{urn:omds20}ZAHLUNG" maxOccurs="unbounded" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *                 &lt;attribute name="BetLfnr" use="required" type="{http://www.w3.org/2001/XMLSchema}unsignedShort" /&gt;
 *                 &lt;attribute name="BetRolleCd" use="required" type="{urn:omds20}BetRolleCd_Type" /&gt;
 *                 &lt;attribute name="BetTxt"&gt;
 *                   &lt;simpleType&gt;
 *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *                       &lt;maxLength value="100"/&gt;
 *                     &lt;/restriction&gt;
 *                   &lt;/simpleType&gt;
 *                 &lt;/attribute&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="LeistungGeschaetzt" type="{urn:omds20}decimal" minOccurs="0"/&gt;
 *         &lt;element name="Schadensreserve" type="{urn:omds20}decimal" minOccurs="0"/&gt;
 *         &lt;element name="SpartenCd" type="{urn:omds20}SpartenCd_Type" minOccurs="0"/&gt;
 *         &lt;element name="Spartenerweiterung" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="10"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="WaehrungsCd" type="{urn:omds20}WaehrungsCd_Type" minOccurs="0"/&gt;
 *         &lt;element name="Spartendetails" type="{urn:omds3ServiceTypes-1-1-0}SpartendetailSchaden_Type" minOccurs="0"/&gt;
 *         &lt;element name="Dokumente" type="{urn:omds3ServiceTypes-1-1-0}ArcImageInfo" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Schaden_Type", propOrder = {
    "vuNr",
    "bearbStandCd",
    "idGeschaeftsfallSchadenanlage",
    "schadennr",
    "vormaligeSchadennr",
    "nachfolgendeSchadennr",
    "polizzennr",
    "vertragsID",
    "sachbearbVU",
    "ordnungsbegriffZuordFremd",
    "schadenzuordnung",
    "schadUrsCd",
    "schadUrsTxt",
    "erledDat",
    "schadenTxt",
    "schadenbeteiligte",
    "leistungGeschaetzt",
    "schadensreserve",
    "spartenCd",
    "spartenerweiterung",
    "waehrungsCd",
    "spartendetails",
    "dokumente"
})
public class SchadenType {

    @XmlElement(name = "VUNr", required = true)
    protected String vuNr;
    @XmlElement(name = "BearbStandCd", required = true)
    protected String bearbStandCd;
    @XmlElement(name = "IdGeschaeftsfallSchadenanlage", required = true)
    protected String idGeschaeftsfallSchadenanlage;
    @XmlElement(name = "Schadennr")
    protected String schadennr;
    @XmlElement(name = "VormaligeSchadennr")
    protected List<String> vormaligeSchadennr;
    @XmlElement(name = "NachfolgendeSchadennr")
    protected String nachfolgendeSchadennr;
    @XmlElement(name = "Polizzennr", required = true)
    protected String polizzennr;
    @XmlElement(name = "VertragsID")
    protected String vertragsID;
    @XmlElement(name = "SachbearbVU")
    protected SachbearbVUType sachbearbVU;
    @XmlElement(name = "OrdnungsbegriffZuordFremd", namespace = "urn:omds3CommonServiceTypes-1-1-0")
    protected List<String> ordnungsbegriffZuordFremd;
    @XmlElement(name = "Schadenzuordnung")
    protected SchadenzuordnungType schadenzuordnung;
    @XmlElement(name = "SchadUrsCd")
    protected String schadUrsCd;
    @XmlElement(name = "SchadUrsTxt")
    protected String schadUrsTxt;
    @XmlElement(name = "ErledDat")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar erledDat;
    @XmlElement(name = "SchadenTxt")
    protected String schadenTxt;
    @XmlElement(name = "Schadenbeteiligte")
    protected List<SchadenType.Schadenbeteiligte> schadenbeteiligte;
    @XmlElement(name = "LeistungGeschaetzt")
    protected BigDecimal leistungGeschaetzt;
    @XmlElement(name = "Schadensreserve")
    protected BigDecimal schadensreserve;
    @XmlElement(name = "SpartenCd")
    protected String spartenCd;
    @XmlElement(name = "Spartenerweiterung")
    protected String spartenerweiterung;
    @XmlElement(name = "WaehrungsCd")
    @XmlSchemaType(name = "string")
    protected WaehrungsCdType waehrungsCd;
    @XmlElement(name = "Spartendetails")
    protected SpartendetailSchadenType spartendetails;
    @XmlElement(name = "Dokumente")
    protected List<ArcImageInfo> dokumente;

    /**
     * Ruft den Wert der vuNr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVUNr() {
        return vuNr;
    }

    /**
     * Legt den Wert der vuNr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVUNr(String value) {
        this.vuNr = value;
    }

    /**
     * Ruft den Wert der bearbStandCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBearbStandCd() {
        return bearbStandCd;
    }

    /**
     * Legt den Wert der bearbStandCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBearbStandCd(String value) {
        this.bearbStandCd = value;
    }

    /**
     * Ruft den Wert der idGeschaeftsfallSchadenanlage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdGeschaeftsfallSchadenanlage() {
        return idGeschaeftsfallSchadenanlage;
    }

    /**
     * Legt den Wert der idGeschaeftsfallSchadenanlage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdGeschaeftsfallSchadenanlage(String value) {
        this.idGeschaeftsfallSchadenanlage = value;
    }

    /**
     * Ruft den Wert der schadennr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSchadennr() {
        return schadennr;
    }

    /**
     * Legt den Wert der schadennr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSchadennr(String value) {
        this.schadennr = value;
    }

    /**
     * Gets the value of the vormaligeSchadennr property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the vormaligeSchadennr property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVormaligeSchadennr().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getVormaligeSchadennr() {
        if (vormaligeSchadennr == null) {
            vormaligeSchadennr = new ArrayList<String>();
        }
        return this.vormaligeSchadennr;
    }

    /**
     * Ruft den Wert der nachfolgendeSchadennr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNachfolgendeSchadennr() {
        return nachfolgendeSchadennr;
    }

    /**
     * Legt den Wert der nachfolgendeSchadennr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNachfolgendeSchadennr(String value) {
        this.nachfolgendeSchadennr = value;
    }

    /**
     * Ruft den Wert der polizzennr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolizzennr() {
        return polizzennr;
    }

    /**
     * Legt den Wert der polizzennr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolizzennr(String value) {
        this.polizzennr = value;
    }

    /**
     * Ruft den Wert der vertragsID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVertragsID() {
        return vertragsID;
    }

    /**
     * Legt den Wert der vertragsID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVertragsID(String value) {
        this.vertragsID = value;
    }

    /**
     * Ruft den Wert der sachbearbVU-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SachbearbVUType }
     *     
     */
    public SachbearbVUType getSachbearbVU() {
        return sachbearbVU;
    }

    /**
     * Legt den Wert der sachbearbVU-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SachbearbVUType }
     *     
     */
    public void setSachbearbVU(SachbearbVUType value) {
        this.sachbearbVU = value;
    }

    /**
     * Gets the value of the ordnungsbegriffZuordFremd property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ordnungsbegriffZuordFremd property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOrdnungsbegriffZuordFremd().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getOrdnungsbegriffZuordFremd() {
        if (ordnungsbegriffZuordFremd == null) {
            ordnungsbegriffZuordFremd = new ArrayList<String>();
        }
        return this.ordnungsbegriffZuordFremd;
    }

    /**
     * Ruft den Wert der schadenzuordnung-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SchadenzuordnungType }
     *     
     */
    public SchadenzuordnungType getSchadenzuordnung() {
        return schadenzuordnung;
    }

    /**
     * Legt den Wert der schadenzuordnung-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SchadenzuordnungType }
     *     
     */
    public void setSchadenzuordnung(SchadenzuordnungType value) {
        this.schadenzuordnung = value;
    }

    /**
     * Ruft den Wert der schadUrsCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSchadUrsCd() {
        return schadUrsCd;
    }

    /**
     * Legt den Wert der schadUrsCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSchadUrsCd(String value) {
        this.schadUrsCd = value;
    }

    /**
     * Ruft den Wert der schadUrsTxt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSchadUrsTxt() {
        return schadUrsTxt;
    }

    /**
     * Legt den Wert der schadUrsTxt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSchadUrsTxt(String value) {
        this.schadUrsTxt = value;
    }

    /**
     * Ruft den Wert der erledDat-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getErledDat() {
        return erledDat;
    }

    /**
     * Legt den Wert der erledDat-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setErledDat(XMLGregorianCalendar value) {
        this.erledDat = value;
    }

    /**
     * Ruft den Wert der schadenTxt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSchadenTxt() {
        return schadenTxt;
    }

    /**
     * Legt den Wert der schadenTxt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSchadenTxt(String value) {
        this.schadenTxt = value;
    }

    /**
     * Gets the value of the schadenbeteiligte property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the schadenbeteiligte property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSchadenbeteiligte().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SchadenType.Schadenbeteiligte }
     * 
     * 
     */
    public List<SchadenType.Schadenbeteiligte> getSchadenbeteiligte() {
        if (schadenbeteiligte == null) {
            schadenbeteiligte = new ArrayList<SchadenType.Schadenbeteiligte>();
        }
        return this.schadenbeteiligte;
    }

    /**
     * Ruft den Wert der leistungGeschaetzt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getLeistungGeschaetzt() {
        return leistungGeschaetzt;
    }

    /**
     * Legt den Wert der leistungGeschaetzt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setLeistungGeschaetzt(BigDecimal value) {
        this.leistungGeschaetzt = value;
    }

    /**
     * Ruft den Wert der schadensreserve-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSchadensreserve() {
        return schadensreserve;
    }

    /**
     * Legt den Wert der schadensreserve-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSchadensreserve(BigDecimal value) {
        this.schadensreserve = value;
    }

    /**
     * Ruft den Wert der spartenCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSpartenCd() {
        return spartenCd;
    }

    /**
     * Legt den Wert der spartenCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSpartenCd(String value) {
        this.spartenCd = value;
    }

    /**
     * Ruft den Wert der spartenerweiterung-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSpartenerweiterung() {
        return spartenerweiterung;
    }

    /**
     * Legt den Wert der spartenerweiterung-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSpartenerweiterung(String value) {
        this.spartenerweiterung = value;
    }

    /**
     * Ruft den Wert der waehrungsCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link WaehrungsCdType }
     *     
     */
    public WaehrungsCdType getWaehrungsCd() {
        return waehrungsCd;
    }

    /**
     * Legt den Wert der waehrungsCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link WaehrungsCdType }
     *     
     */
    public void setWaehrungsCd(WaehrungsCdType value) {
        this.waehrungsCd = value;
    }

    /**
     * Ruft den Wert der spartendetails-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SpartendetailSchadenType }
     *     
     */
    public SpartendetailSchadenType getSpartendetails() {
        return spartendetails;
    }

    /**
     * Legt den Wert der spartendetails-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SpartendetailSchadenType }
     *     
     */
    public void setSpartendetails(SpartendetailSchadenType value) {
        this.spartendetails = value;
    }

    /**
     * Gets the value of the dokumente property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dokumente property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDokumente().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ArcImageInfo }
     * 
     * 
     */
    public List<ArcImageInfo> getDokumente() {
        if (dokumente == null) {
            dokumente = new ArrayList<ArcImageInfo>();
        }
        return this.dokumente;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="GeschInteresseLfnr" type="{http://www.w3.org/2001/XMLSchema}unsignedShort" maxOccurs="unbounded" minOccurs="0"/&gt;
     *         &lt;element ref="{urn:omds20}ZAHLUNG" maxOccurs="unbounded" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *       &lt;attribute name="BetLfnr" use="required" type="{http://www.w3.org/2001/XMLSchema}unsignedShort" /&gt;
     *       &lt;attribute name="BetRolleCd" use="required" type="{urn:omds20}BetRolleCd_Type" /&gt;
     *       &lt;attribute name="BetTxt"&gt;
     *         &lt;simpleType&gt;
     *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
     *             &lt;maxLength value="100"/&gt;
     *           &lt;/restriction&gt;
     *         &lt;/simpleType&gt;
     *       &lt;/attribute&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "geschInteresseLfnr",
        "zahlung"
    })
    public static class Schadenbeteiligte {

        @XmlElement(name = "GeschInteresseLfnr", type = Integer.class)
        @XmlSchemaType(name = "unsignedShort")
        protected List<Integer> geschInteresseLfnr;
        @XmlElement(name = "ZAHLUNG", namespace = "urn:omds20")
        protected List<ZAHLUNGType> zahlung;
        @XmlAttribute(name = "BetLfnr", namespace = "urn:omds3ServiceTypes-1-1-0", required = true)
        @XmlSchemaType(name = "unsignedShort")
        protected int betLfnr;
        @XmlAttribute(name = "BetRolleCd", namespace = "urn:omds3ServiceTypes-1-1-0", required = true)
        protected String betRolleCd;
        @XmlAttribute(name = "BetTxt", namespace = "urn:omds3ServiceTypes-1-1-0")
        protected String betTxt;

        /**
         * Gets the value of the geschInteresseLfnr property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the geschInteresseLfnr property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getGeschInteresseLfnr().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Integer }
         * 
         * 
         */
        public List<Integer> getGeschInteresseLfnr() {
            if (geschInteresseLfnr == null) {
                geschInteresseLfnr = new ArrayList<Integer>();
            }
            return this.geschInteresseLfnr;
        }

        /**
         * Gets the value of the zahlung property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the zahlung property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getZAHLUNG().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ZAHLUNGType }
         * 
         * 
         */
        public List<ZAHLUNGType> getZAHLUNG() {
            if (zahlung == null) {
                zahlung = new ArrayList<ZAHLUNGType>();
            }
            return this.zahlung;
        }

        /**
         * Ruft den Wert der betLfnr-Eigenschaft ab.
         * 
         */
        public int getBetLfnr() {
            return betLfnr;
        }

        /**
         * Legt den Wert der betLfnr-Eigenschaft fest.
         * 
         */
        public void setBetLfnr(int value) {
            this.betLfnr = value;
        }

        /**
         * Ruft den Wert der betRolleCd-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBetRolleCd() {
            return betRolleCd;
        }

        /**
         * Legt den Wert der betRolleCd-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBetRolleCd(String value) {
            this.betRolleCd = value;
        }

        /**
         * Ruft den Wert der betTxt-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBetTxt() {
            return betTxt;
        }

        /**
         * Legt den Wert der betTxt-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBetTxt(String value) {
            this.betTxt = value;
        }

    }

}
