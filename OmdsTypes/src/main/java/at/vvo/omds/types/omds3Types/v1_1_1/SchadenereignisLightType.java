
package at.vvo.omds.types.omds3Types.v1_1_1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Typ zur leichtgewichtigen Abbildung von Schadenereignis-Objekten
 * 
 * <p>Java-Klasse für SchadenereignisLight_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SchadenereignisLight_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="VUNr" type="{urn:omds20}VUNr" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:omds3ServiceTypes-1-1-0}IdGeschaeftsfallSchadenereignis"/&gt;
 *         &lt;element name="VormaligeIdGeschaeftsfall" type="{urn:omds3CommonServiceTypes-1-1-0}GeschaeftsfallId_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="NachfolgendeIdGeschaeftsfall" type="{urn:omds3CommonServiceTypes-1-1-0}GeschaeftsfallId_Type" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:omds3CommonServiceTypes-1-1-0}OrdnungsbegriffZuordFremd" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Schaeden" type="{urn:omds3ServiceTypes-1-1-0}SchadenLight_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Meldedat" type="{urn:omds20}Datum-Zeit"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SchadenereignisLight_Type", propOrder = {
    "vuNr",
    "idGeschaeftsfallSchadenereignis",
    "vormaligeIdGeschaeftsfall",
    "nachfolgendeIdGeschaeftsfall",
    "ordnungsbegriffZuordFremd",
    "schaeden",
    "meldedat"
})
public class SchadenereignisLightType {

    @XmlElement(name = "VUNr")
    protected String vuNr;
    @XmlElement(name = "IdGeschaeftsfallSchadenereignis", required = true)
    protected String idGeschaeftsfallSchadenereignis;
    @XmlElement(name = "VormaligeIdGeschaeftsfall")
    protected List<String> vormaligeIdGeschaeftsfall;
    @XmlElement(name = "NachfolgendeIdGeschaeftsfall")
    protected String nachfolgendeIdGeschaeftsfall;
    @XmlElement(name = "OrdnungsbegriffZuordFremd", namespace = "urn:omds3CommonServiceTypes-1-1-0")
    protected List<String> ordnungsbegriffZuordFremd;
    @XmlElement(name = "Schaeden")
    protected List<SchadenLightType> schaeden;
    @XmlElement(name = "Meldedat", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar meldedat;

    /**
     * Ruft den Wert der vuNr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVUNr() {
        return vuNr;
    }

    /**
     * Legt den Wert der vuNr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVUNr(String value) {
        this.vuNr = value;
    }

    /**
     * Ruft den Wert der idGeschaeftsfallSchadenereignis-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdGeschaeftsfallSchadenereignis() {
        return idGeschaeftsfallSchadenereignis;
    }

    /**
     * Legt den Wert der idGeschaeftsfallSchadenereignis-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdGeschaeftsfallSchadenereignis(String value) {
        this.idGeschaeftsfallSchadenereignis = value;
    }

    /**
     * Gets the value of the vormaligeIdGeschaeftsfall property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the vormaligeIdGeschaeftsfall property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVormaligeIdGeschaeftsfall().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getVormaligeIdGeschaeftsfall() {
        if (vormaligeIdGeschaeftsfall == null) {
            vormaligeIdGeschaeftsfall = new ArrayList<String>();
        }
        return this.vormaligeIdGeschaeftsfall;
    }

    /**
     * Ruft den Wert der nachfolgendeIdGeschaeftsfall-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNachfolgendeIdGeschaeftsfall() {
        return nachfolgendeIdGeschaeftsfall;
    }

    /**
     * Legt den Wert der nachfolgendeIdGeschaeftsfall-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNachfolgendeIdGeschaeftsfall(String value) {
        this.nachfolgendeIdGeschaeftsfall = value;
    }

    /**
     * Gets the value of the ordnungsbegriffZuordFremd property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ordnungsbegriffZuordFremd property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOrdnungsbegriffZuordFremd().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getOrdnungsbegriffZuordFremd() {
        if (ordnungsbegriffZuordFremd == null) {
            ordnungsbegriffZuordFremd = new ArrayList<String>();
        }
        return this.ordnungsbegriffZuordFremd;
    }

    /**
     * Gets the value of the schaeden property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the schaeden property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSchaeden().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SchadenLightType }
     * 
     * 
     */
    public List<SchadenLightType> getSchaeden() {
        if (schaeden == null) {
            schaeden = new ArrayList<SchadenLightType>();
        }
        return this.schaeden;
    }

    /**
     * Ruft den Wert der meldedat-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getMeldedat() {
        return meldedat;
    }

    /**
     * Legt den Wert der meldedat-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setMeldedat(XMLGregorianCalendar value) {
        this.meldedat = value;
    }

}
