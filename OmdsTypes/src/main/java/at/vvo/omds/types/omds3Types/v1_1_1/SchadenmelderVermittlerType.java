
package at.vvo.omds.types.omds3Types.v1_1_1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Typ zur Angabe eines Vermittlers als Schadenmelder
 * 
 * <p>Java-Klasse für SchadenmelderVermittler_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SchadenmelderVermittler_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3ServiceTypes-1-1-0}ObjektSpezifikation_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Vermnr" type="{urn:omds20}Vermnr"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SchadenmelderVermittler_Type", propOrder = {
    "vermnr"
})
public class SchadenmelderVermittlerType
    extends ObjektSpezifikationType
{

    @XmlElement(name = "Vermnr", required = true)
    protected String vermnr;

    /**
     * Ruft den Wert der vermnr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVermnr() {
        return vermnr;
    }

    /**
     * Legt den Wert der vermnr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVermnr(String value) {
        this.vermnr = value;
    }

}
