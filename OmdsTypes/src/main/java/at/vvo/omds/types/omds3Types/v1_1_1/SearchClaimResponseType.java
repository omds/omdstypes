
package at.vvo.omds.types.omds3Types.v1_1_1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.v1_1_1.common.ServiceFault;


/**
 * Response-Type der Suche nach einem Schaden
 * 
 * <p>Java-Klasse für SearchClaimResponse_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SearchClaimResponse_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice&gt;
 *         &lt;element name="Result"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="ActualOffset" type="{http://www.w3.org/2001/XMLSchema}unsignedInt"/&gt;
 *                   &lt;element name="ActualMaxResults" type="{http://www.w3.org/2001/XMLSchema}unsignedInt"/&gt;
 *                   &lt;element name="TotalResults" type="{http://www.w3.org/2001/XMLSchema}unsignedInt"/&gt;
 *                   &lt;element name="Schadenereignisse" type="{urn:omds3ServiceTypes-1-1-0}Schadenereignis_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="ServiceFault" type="{urn:omds3CommonServiceTypes-1-1-0}ServiceFault"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchClaimResponse_Type", propOrder = {
    "result",
    "serviceFault"
})
public class SearchClaimResponseType {

    @XmlElement(name = "Result")
    protected SearchClaimResponseType.Result result;
    @XmlElement(name = "ServiceFault")
    protected ServiceFault serviceFault;

    /**
     * Ruft den Wert der result-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SearchClaimResponseType.Result }
     *     
     */
    public SearchClaimResponseType.Result getResult() {
        return result;
    }

    /**
     * Legt den Wert der result-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchClaimResponseType.Result }
     *     
     */
    public void setResult(SearchClaimResponseType.Result value) {
        this.result = value;
    }

    /**
     * Ruft den Wert der serviceFault-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceFault }
     *     
     */
    public ServiceFault getServiceFault() {
        return serviceFault;
    }

    /**
     * Legt den Wert der serviceFault-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceFault }
     *     
     */
    public void setServiceFault(ServiceFault value) {
        this.serviceFault = value;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="ActualOffset" type="{http://www.w3.org/2001/XMLSchema}unsignedInt"/&gt;
     *         &lt;element name="ActualMaxResults" type="{http://www.w3.org/2001/XMLSchema}unsignedInt"/&gt;
     *         &lt;element name="TotalResults" type="{http://www.w3.org/2001/XMLSchema}unsignedInt"/&gt;
     *         &lt;element name="Schadenereignisse" type="{urn:omds3ServiceTypes-1-1-0}Schadenereignis_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "actualOffset",
        "actualMaxResults",
        "totalResults",
        "schadenereignisse"
    })
    public static class Result {

        @XmlElement(name = "ActualOffset")
        @XmlSchemaType(name = "unsignedInt")
        protected long actualOffset;
        @XmlElement(name = "ActualMaxResults")
        @XmlSchemaType(name = "unsignedInt")
        protected long actualMaxResults;
        @XmlElement(name = "TotalResults")
        @XmlSchemaType(name = "unsignedInt")
        protected long totalResults;
        @XmlElement(name = "Schadenereignisse")
        protected List<SchadenereignisType> schadenereignisse;

        /**
         * Ruft den Wert der actualOffset-Eigenschaft ab.
         * 
         */
        public long getActualOffset() {
            return actualOffset;
        }

        /**
         * Legt den Wert der actualOffset-Eigenschaft fest.
         * 
         */
        public void setActualOffset(long value) {
            this.actualOffset = value;
        }

        /**
         * Ruft den Wert der actualMaxResults-Eigenschaft ab.
         * 
         */
        public long getActualMaxResults() {
            return actualMaxResults;
        }

        /**
         * Legt den Wert der actualMaxResults-Eigenschaft fest.
         * 
         */
        public void setActualMaxResults(long value) {
            this.actualMaxResults = value;
        }

        /**
         * Ruft den Wert der totalResults-Eigenschaft ab.
         * 
         */
        public long getTotalResults() {
            return totalResults;
        }

        /**
         * Legt den Wert der totalResults-Eigenschaft fest.
         * 
         */
        public void setTotalResults(long value) {
            this.totalResults = value;
        }

        /**
         * Gets the value of the schadenereignisse property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the schadenereignisse property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getSchadenereignisse().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link SchadenereignisType }
         * 
         * 
         */
        public List<SchadenereignisType> getSchadenereignisse() {
            if (schadenereignisse == null) {
                schadenereignisse = new ArrayList<SchadenereignisType>();
            }
            return this.schadenereignisse;
        }

    }

}
