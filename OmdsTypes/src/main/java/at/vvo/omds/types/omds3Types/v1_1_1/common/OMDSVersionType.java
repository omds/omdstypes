
package at.vvo.omds.types.omds3Types.v1_1_1.common;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für OMDSVersion_Type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="OMDSVersion_Type"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;maxLength value="20"/&gt;
 *     &lt;enumeration value="OMDS-Services 1.0.0"/&gt;
 *     &lt;enumeration value="OMDS-Services 1.1.0"/&gt;
 *     &lt;enumeration value="OMDS-Services 1.1.1"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "OMDSVersion_Type")
@XmlEnum
public enum OMDSVersionType {

    @XmlEnumValue("OMDS-Services 1.0.0")
    OMDS_SERVICES_1_0_0("OMDS-Services 1.0.0"),
    @XmlEnumValue("OMDS-Services 1.1.0")
    OMDS_SERVICES_1_1_0("OMDS-Services 1.1.0"),
    @XmlEnumValue("OMDS-Services 1.1.1")
    OMDS_SERVICES_1_1_1("OMDS-Services 1.1.1");
    private final String value;

    OMDSVersionType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static OMDSVersionType fromValue(String v) {
        for (OMDSVersionType c: OMDSVersionType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
