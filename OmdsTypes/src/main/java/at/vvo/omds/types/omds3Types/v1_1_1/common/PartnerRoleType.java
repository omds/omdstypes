
package at.vvo.omds.types.omds3Types.v1_1_1.common;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für PartnerRoleType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="PartnerRoleType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;length value="2"/&gt;
 *     &lt;enumeration value="VN"/&gt;
 *     &lt;enumeration value="VP"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "PartnerRoleType")
@XmlEnum
public enum PartnerRoleType {


    /**
     * Versicherungsnehmer
     * 
     */
    VN,

    /**
     * Versicherte Person
     * 
     */
    VP;

    public String value() {
        return name();
    }

    public static PartnerRoleType fromValue(String v) {
        return valueOf(v);
    }

}
