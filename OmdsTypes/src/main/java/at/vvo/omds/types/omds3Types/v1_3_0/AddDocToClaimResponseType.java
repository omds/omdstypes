
package at.vvo.omds.types.omds3Types.v1_3_0;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.v1_3_0.common.ServiceFault;


/**
 * Response von der VU, wenn ein zusätzliches Schadensdokument übergeben wurde
 * 
 * <p>Java-Klasse für AddDocToClaimResponse_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="AddDocToClaimResponse_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice maxOccurs="unbounded"&gt;
 *         &lt;element name="ArcImageId" type="{urn:omds3ServiceTypes-1-1-0}ArcImageInfo"/&gt;
 *         &lt;element name="ServiceFault" type="{urn:omds3CommonServiceTypes-1-1-0}ServiceFault" maxOccurs="unbounded"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AddDocToClaimResponse_Type", propOrder = {
    "arcImageIdOrServiceFault"
})
public class AddDocToClaimResponseType {

    @XmlElements({
        @XmlElement(name = "ArcImageId", type = ArcImageInfo.class),
        @XmlElement(name = "ServiceFault", type = ServiceFault.class)
    })
    protected List<Object> arcImageIdOrServiceFault;

    /**
     * Gets the value of the arcImageIdOrServiceFault property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the arcImageIdOrServiceFault property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getArcImageIdOrServiceFault().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ArcImageInfo }
     * {@link ServiceFault }
     * 
     * 
     */
    public List<Object> getArcImageIdOrServiceFault() {
        if (arcImageIdOrServiceFault == null) {
            arcImageIdOrServiceFault = new ArrayList<Object>();
        }
        return this.arcImageIdOrServiceFault;
    }

}
