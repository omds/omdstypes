
package at.vvo.omds.types.omds3Types.v1_3_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * Eine Objektspezifikation, die verwendt wird, wenn alle Dokumente für alle Objekttypen abgerufen werden sollen, also gerade kein Objekt spezifiziert werden soll. Dieser Umweg ist nötig, da in den Methoden eine Objektspezifikation als required vorgegeben ist. Es ist geplant die Anforderung "required" in späteren Versionen entfallen zu lassen, dann entfällt auch dieser Typ.
 * 
 * <p>Java-Klasse für AlleObjekteSpezifikation_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="AlleObjekteSpezifikation_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3ServiceTypes-1-1-0}ObjektSpezifikation_Type"&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AlleObjekteSpezifikation_Type")
public class AlleObjekteSpezifikationType
    extends ObjektSpezifikationType
{


}
