
package at.vvo.omds.types.omds3Types.v1_3_0;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.v1_3_0.common.ServiceFault;


/**
 * Rückgabetyp Informationen zu vertragsbezogenen Dokumenten
 * 
 * <p>Java-Klasse für ArcImageInfosResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ArcImageInfosResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="arcImageInfo" type="{urn:omds3ServiceTypes-1-1-0}ArcImageInfo" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ServiceFault" type="{urn:omds3CommonServiceTypes-1-1-0}ServiceFault" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArcImageInfosResponse", propOrder = {
    "arcImageInfo",
    "serviceFault"
})
public class ArcImageInfosResponse {

    protected List<ArcImageInfo> arcImageInfo;
    @XmlElement(name = "ServiceFault")
    protected ServiceFault serviceFault;

    /**
     * Gets the value of the arcImageInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the arcImageInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getArcImageInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ArcImageInfo }
     * 
     * 
     */
    public List<ArcImageInfo> getArcImageInfo() {
        if (arcImageInfo == null) {
            arcImageInfo = new ArrayList<ArcImageInfo>();
        }
        return this.arcImageInfo;
    }

    /**
     * Ruft den Wert der serviceFault-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceFault }
     *     
     */
    public ServiceFault getServiceFault() {
        return serviceFault;
    }

    /**
     * Legt den Wert der serviceFault-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceFault }
     *     
     */
    public void setServiceFault(ServiceFault value) {
        this.serviceFault = value;
    }

}
