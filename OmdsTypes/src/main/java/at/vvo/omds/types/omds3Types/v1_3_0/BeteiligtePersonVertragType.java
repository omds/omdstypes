
package at.vvo.omds.types.omds3Types.v1_3_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Typ für die Meldung von Personen, die an einem Vertrag beteiligt sind
 * 
 * <p>Java-Klasse für BeteiligtePersonVertrag_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="BeteiligtePersonVertrag_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Lfnr" type="{http://www.w3.org/2001/XMLSchema}unsignedShort"/&gt;
 *         &lt;element ref="{urn:omds3ServiceTypes-1-1-0}InformationenPerson"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BeteiligtePersonVertrag_Type", propOrder = {
    "lfnr",
    "informationenPerson"
})
public class BeteiligtePersonVertragType {

    @XmlElement(name = "Lfnr")
    @XmlSchemaType(name = "unsignedShort")
    protected int lfnr;
    @XmlElement(name = "InformationenPerson", required = true)
    protected InformationenPersonType informationenPerson;

    /**
     * Ruft den Wert der lfnr-Eigenschaft ab.
     * 
     */
    public int getLfnr() {
        return lfnr;
    }

    /**
     * Legt den Wert der lfnr-Eigenschaft fest.
     * 
     */
    public void setLfnr(int value) {
        this.lfnr = value;
    }

    /**
     * Objekt zur Übermittlung von Personendaten ähnlich zu omds:PERSON, aber Personennr ist nicht Pflichtfeld
     * 
     * @return
     *     possible object is
     *     {@link InformationenPersonType }
     *     
     */
    public InformationenPersonType getInformationenPerson() {
        return informationenPerson;
    }

    /**
     * Legt den Wert der informationenPerson-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link InformationenPersonType }
     *     
     */
    public void setInformationenPerson(InformationenPersonType value) {
        this.informationenPerson = value;
    }

}
