
package at.vvo.omds.types.omds3Types.v1_3_0;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Typ für die Durchführung einer Schadenmeldung
 * 
 * <p>Java-Klasse für CreateClaimRequest_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CreateClaimRequest_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="VUNr" type="{urn:omds20}VUNr" minOccurs="0"/&gt;
 *         &lt;element name="ReferenzGeschaeftsfallId" type="{urn:omds3CommonServiceTypes-1-1-0}GeschaeftsfallId_Type" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:omds3CommonServiceTypes-1-1-0}OrdnungsbegriffZuordFremd" minOccurs="0"/&gt;
 *         &lt;element name="Ereigniszpkt" type="{urn:omds20}Datum-Zeit"/&gt;
 *         &lt;element name="EreignisbeschrTxt" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="SchadOrt" type="{urn:omds3ServiceTypes-1-1-0}Ort_Type"/&gt;
 *         &lt;element name="BeteiligtePersonen" type="{urn:omds3ServiceTypes-1-1-0}BeteiligtePerson_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="GeschaedigteInteressen" type="{urn:omds3ServiceTypes-1-1-0}GeschaedigtesInteresse_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Dokumente" type="{urn:omds3ServiceTypes-1-1-0}Upload_Dokument_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Schaeden" type="{urn:omds3ServiceTypes-1-1-0}MeldungSchaden_Type" maxOccurs="unbounded"/&gt;
 *         &lt;element name="Schadenmelder" type="{urn:omds3ServiceTypes-1-1-0}Schadenmelder_Type" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreateClaimRequest_Type", propOrder = {
    "vuNr",
    "referenzGeschaeftsfallId",
    "ordnungsbegriffZuordFremd",
    "ereigniszpkt",
    "ereignisbeschrTxt",
    "schadOrt",
    "beteiligtePersonen",
    "geschaedigteInteressen",
    "dokumente",
    "schaeden",
    "schadenmelder"
})
public class CreateClaimRequestType {

    @XmlElement(name = "VUNr")
    protected String vuNr;
    @XmlElement(name = "ReferenzGeschaeftsfallId")
    protected String referenzGeschaeftsfallId;
    @XmlElement(name = "OrdnungsbegriffZuordFremd", namespace = "urn:omds3CommonServiceTypes-1-1-0")
    protected String ordnungsbegriffZuordFremd;
    @XmlElement(name = "Ereigniszpkt", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar ereigniszpkt;
    @XmlElement(name = "EreignisbeschrTxt", required = true)
    protected String ereignisbeschrTxt;
    @XmlElement(name = "SchadOrt", required = true)
    protected OrtType schadOrt;
    @XmlElement(name = "BeteiligtePersonen")
    protected List<BeteiligtePersonType> beteiligtePersonen;
    @XmlElement(name = "GeschaedigteInteressen")
    protected List<GeschaedigtesInteresseType> geschaedigteInteressen;
    @XmlElement(name = "Dokumente")
    protected List<UploadDokumentType> dokumente;
    @XmlElement(name = "Schaeden", required = true)
    protected List<MeldungSchadenType> schaeden;
    @XmlElement(name = "Schadenmelder")
    protected SchadenmelderType schadenmelder;

    /**
     * Ruft den Wert der vuNr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVUNr() {
        return vuNr;
    }

    /**
     * Legt den Wert der vuNr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVUNr(String value) {
        this.vuNr = value;
    }

    /**
     * Ruft den Wert der referenzGeschaeftsfallId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferenzGeschaeftsfallId() {
        return referenzGeschaeftsfallId;
    }

    /**
     * Legt den Wert der referenzGeschaeftsfallId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferenzGeschaeftsfallId(String value) {
        this.referenzGeschaeftsfallId = value;
    }

    /**
     * Ruft den Wert der ordnungsbegriffZuordFremd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrdnungsbegriffZuordFremd() {
        return ordnungsbegriffZuordFremd;
    }

    /**
     * Legt den Wert der ordnungsbegriffZuordFremd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrdnungsbegriffZuordFremd(String value) {
        this.ordnungsbegriffZuordFremd = value;
    }

    /**
     * Ruft den Wert der ereigniszpkt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEreigniszpkt() {
        return ereigniszpkt;
    }

    /**
     * Legt den Wert der ereigniszpkt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEreigniszpkt(XMLGregorianCalendar value) {
        this.ereigniszpkt = value;
    }

    /**
     * Ruft den Wert der ereignisbeschrTxt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEreignisbeschrTxt() {
        return ereignisbeschrTxt;
    }

    /**
     * Legt den Wert der ereignisbeschrTxt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEreignisbeschrTxt(String value) {
        this.ereignisbeschrTxt = value;
    }

    /**
     * Ruft den Wert der schadOrt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OrtType }
     *     
     */
    public OrtType getSchadOrt() {
        return schadOrt;
    }

    /**
     * Legt den Wert der schadOrt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OrtType }
     *     
     */
    public void setSchadOrt(OrtType value) {
        this.schadOrt = value;
    }

    /**
     * Gets the value of the beteiligtePersonen property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the beteiligtePersonen property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBeteiligtePersonen().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BeteiligtePersonType }
     * 
     * 
     */
    public List<BeteiligtePersonType> getBeteiligtePersonen() {
        if (beteiligtePersonen == null) {
            beteiligtePersonen = new ArrayList<BeteiligtePersonType>();
        }
        return this.beteiligtePersonen;
    }

    /**
     * Gets the value of the geschaedigteInteressen property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the geschaedigteInteressen property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGeschaedigteInteressen().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GeschaedigtesInteresseType }
     * 
     * 
     */
    public List<GeschaedigtesInteresseType> getGeschaedigteInteressen() {
        if (geschaedigteInteressen == null) {
            geschaedigteInteressen = new ArrayList<GeschaedigtesInteresseType>();
        }
        return this.geschaedigteInteressen;
    }

    /**
     * Gets the value of the dokumente property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dokumente property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDokumente().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link UploadDokumentType }
     * 
     * 
     */
    public List<UploadDokumentType> getDokumente() {
        if (dokumente == null) {
            dokumente = new ArrayList<UploadDokumentType>();
        }
        return this.dokumente;
    }

    /**
     * Gets the value of the schaeden property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the schaeden property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSchaeden().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MeldungSchadenType }
     * 
     * 
     */
    public List<MeldungSchadenType> getSchaeden() {
        if (schaeden == null) {
            schaeden = new ArrayList<MeldungSchadenType>();
        }
        return this.schaeden;
    }

    /**
     * Ruft den Wert der schadenmelder-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SchadenmelderType }
     *     
     */
    public SchadenmelderType getSchadenmelder() {
        return schadenmelder;
    }

    /**
     * Legt den Wert der schadenmelder-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SchadenmelderType }
     *     
     */
    public void setSchadenmelder(SchadenmelderType value) {
        this.schadenmelder = value;
    }

}
