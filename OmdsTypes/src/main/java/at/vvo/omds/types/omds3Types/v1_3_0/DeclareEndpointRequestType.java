
package at.vvo.omds.types.omds3Types.v1_3_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.v1_3_0.common.AuthorizationFilter;


/**
 * Typ für den DeclareEndpointRequest
 * 
 * <p>Java-Klasse für DeclareEndpointRequest_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="DeclareEndpointRequest_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="VUNr" type="{urn:omds20}VUNr" minOccurs="0"/&gt;
 *         &lt;element name="BasisUrlEndpoint"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="1000"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element ref="{urn:omds3ServiceTypes-1-1-0}ArtAuthentifizierung"/&gt;
 *         &lt;element name="Credentials" type="{urn:omds3ServiceTypes-1-1-0}Credentials_Type"/&gt;
 *         &lt;element name="AuthorizationFilter" type="{urn:omds3CommonServiceTypes-1-1-0}AuthorizationFilter" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DeclareEndpointRequest_Type", propOrder = {
    "vuNr",
    "basisUrlEndpoint",
    "artAuthentifizierung",
    "credentials",
    "authorizationFilter"
})
public class DeclareEndpointRequestType {

    @XmlElement(name = "VUNr")
    protected String vuNr;
    @XmlElement(name = "BasisUrlEndpoint", required = true)
    protected String basisUrlEndpoint;
    @XmlElement(name = "ArtAuthentifizierung", required = true)
    protected String artAuthentifizierung;
    @XmlElement(name = "Credentials", required = true)
    protected CredentialsType credentials;
    @XmlElement(name = "AuthorizationFilter")
    protected AuthorizationFilter authorizationFilter;

    /**
     * Ruft den Wert der vuNr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVUNr() {
        return vuNr;
    }

    /**
     * Legt den Wert der vuNr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVUNr(String value) {
        this.vuNr = value;
    }

    /**
     * Ruft den Wert der basisUrlEndpoint-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBasisUrlEndpoint() {
        return basisUrlEndpoint;
    }

    /**
     * Legt den Wert der basisUrlEndpoint-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBasisUrlEndpoint(String value) {
        this.basisUrlEndpoint = value;
    }

    /**
     * Ruft den Wert der artAuthentifizierung-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArtAuthentifizierung() {
        return artAuthentifizierung;
    }

    /**
     * Legt den Wert der artAuthentifizierung-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArtAuthentifizierung(String value) {
        this.artAuthentifizierung = value;
    }

    /**
     * Ruft den Wert der credentials-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CredentialsType }
     *     
     */
    public CredentialsType getCredentials() {
        return credentials;
    }

    /**
     * Legt den Wert der credentials-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CredentialsType }
     *     
     */
    public void setCredentials(CredentialsType value) {
        this.credentials = value;
    }

    /**
     * Ruft den Wert der authorizationFilter-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AuthorizationFilter }
     *     
     */
    public AuthorizationFilter getAuthorizationFilter() {
        return authorizationFilter;
    }

    /**
     * Legt den Wert der authorizationFilter-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AuthorizationFilter }
     *     
     */
    public void setAuthorizationFilter(AuthorizationFilter value) {
        this.authorizationFilter = value;
    }

}
