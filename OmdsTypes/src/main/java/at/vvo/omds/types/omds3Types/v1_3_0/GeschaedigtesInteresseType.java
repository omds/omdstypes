
package at.vvo.omds.types.omds3Types.v1_3_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Das geschädigte Interesse ist der Basistyp für den es Erweiterungen geben kann, z.B. das geschädigte Kfz
 * 
 * <p>Java-Klasse für GeschaedigtesInteresse_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="GeschaedigtesInteresse_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GeschInteresseLfnr" type="{http://www.w3.org/2001/XMLSchema}unsignedShort"/&gt;
 *         &lt;element name="SchadenBeschreibung"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="100"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GeschaedigtesInteresse_Type", propOrder = {
    "geschInteresseLfnr",
    "schadenBeschreibung"
})
@XmlSeeAlso({
    GeschaedigtesObjektKfzType.class,
    GeschaedigtesObjektImmobilieType.class
})
public class GeschaedigtesInteresseType {

    @XmlElement(name = "GeschInteresseLfnr")
    @XmlSchemaType(name = "unsignedShort")
    protected int geschInteresseLfnr;
    @XmlElement(name = "SchadenBeschreibung", required = true)
    protected String schadenBeschreibung;

    /**
     * Ruft den Wert der geschInteresseLfnr-Eigenschaft ab.
     * 
     */
    public int getGeschInteresseLfnr() {
        return geschInteresseLfnr;
    }

    /**
     * Legt den Wert der geschInteresseLfnr-Eigenschaft fest.
     * 
     */
    public void setGeschInteresseLfnr(int value) {
        this.geschInteresseLfnr = value;
    }

    /**
     * Ruft den Wert der schadenBeschreibung-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSchadenBeschreibung() {
        return schadenBeschreibung;
    }

    /**
     * Legt den Wert der schadenBeschreibung-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSchadenBeschreibung(String value) {
        this.schadenBeschreibung = value;
    }

}
