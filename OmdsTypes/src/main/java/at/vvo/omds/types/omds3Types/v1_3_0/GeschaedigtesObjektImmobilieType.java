
package at.vvo.omds.types.omds3Types.v1_3_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Erweiterung des geschädigten Interesses zu einer geschädigten Immobilie
 * 
 * <p>Java-Klasse für GeschaedigtesObjektImmobilie_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="GeschaedigtesObjektImmobilie_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3ServiceTypes-1-1-0}GeschaedigtesInteresse_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GebaeudeArtCd" type="{urn:omds20}GebaeudeArtCd_Type" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:omds3CommonServiceTypes-1-1-0}OrdnungsbegriffZuordFremd" minOccurs="0"/&gt;
 *         &lt;element name="GebauedeBez" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="60"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GeschaedigtesObjektImmobilie_Type", propOrder = {
    "gebaeudeArtCd",
    "ordnungsbegriffZuordFremd",
    "gebauedeBez"
})
public class GeschaedigtesObjektImmobilieType
    extends GeschaedigtesInteresseType
{

    @XmlElement(name = "GebaeudeArtCd")
    protected String gebaeudeArtCd;
    @XmlElement(name = "OrdnungsbegriffZuordFremd", namespace = "urn:omds3CommonServiceTypes-1-1-0")
    protected String ordnungsbegriffZuordFremd;
    @XmlElement(name = "GebauedeBez")
    protected String gebauedeBez;

    /**
     * Ruft den Wert der gebaeudeArtCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGebaeudeArtCd() {
        return gebaeudeArtCd;
    }

    /**
     * Legt den Wert der gebaeudeArtCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGebaeudeArtCd(String value) {
        this.gebaeudeArtCd = value;
    }

    /**
     * Ruft den Wert der ordnungsbegriffZuordFremd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrdnungsbegriffZuordFremd() {
        return ordnungsbegriffZuordFremd;
    }

    /**
     * Legt den Wert der ordnungsbegriffZuordFremd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrdnungsbegriffZuordFremd(String value) {
        this.ordnungsbegriffZuordFremd = value;
    }

    /**
     * Ruft den Wert der gebauedeBez-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGebauedeBez() {
        return gebauedeBez;
    }

    /**
     * Legt den Wert der gebauedeBez-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGebauedeBez(String value) {
        this.gebauedeBez = value;
    }

}
