
package at.vvo.omds.types.omds3Types.v1_3_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.v1_3_0.common.ServiceFault;


/**
 * Leichtgewichtiges Response-Objekt für Schadenereignisse
 * 
 * <p>Java-Klasse für GetClaimResponseLight_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="GetClaimResponseLight_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Schadenereignis" type="{urn:omds3ServiceTypes-1-1-0}SchadenereignisLight_Type" minOccurs="0"/&gt;
 *         &lt;element name="ServiceFault" type="{urn:omds3CommonServiceTypes-1-1-0}ServiceFault" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetClaimResponseLight_Type", propOrder = {
    "schadenereignis",
    "serviceFault"
})
public class GetClaimResponseLightType {

    @XmlElement(name = "Schadenereignis")
    protected SchadenereignisLightType schadenereignis;
    @XmlElement(name = "ServiceFault")
    protected ServiceFault serviceFault;

    /**
     * Ruft den Wert der schadenereignis-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SchadenereignisLightType }
     *     
     */
    public SchadenereignisLightType getSchadenereignis() {
        return schadenereignis;
    }

    /**
     * Legt den Wert der schadenereignis-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SchadenereignisLightType }
     *     
     */
    public void setSchadenereignis(SchadenereignisLightType value) {
        this.schadenereignis = value;
    }

    /**
     * Ruft den Wert der serviceFault-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceFault }
     *     
     */
    public ServiceFault getServiceFault() {
        return serviceFault;
    }

    /**
     * Legt den Wert der serviceFault-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceFault }
     *     
     */
    public void setServiceFault(ServiceFault value) {
        this.serviceFault = value;
    }

}
