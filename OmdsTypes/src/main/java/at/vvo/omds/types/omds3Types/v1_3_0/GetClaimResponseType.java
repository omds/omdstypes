
package at.vvo.omds.types.omds3Types.v1_3_0;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.v1_3_0.common.ServiceFault;


/**
 * Response-Objekt für Schadenereignisse
 * 
 * <p>Java-Klasse für GetClaimResponse_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="GetClaimResponse_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice&gt;
 *         &lt;element name="Schadenereignis" type="{urn:omds3ServiceTypes-1-1-0}Schadenereignis_Type"/&gt;
 *         &lt;element name="ServiceFault" type="{urn:omds3CommonServiceTypes-1-1-0}ServiceFault" maxOccurs="unbounded"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetClaimResponse_Type", propOrder = {
    "schadenereignis",
    "serviceFault"
})
public class GetClaimResponseType {

    @XmlElement(name = "Schadenereignis")
    protected SchadenereignisType schadenereignis;
    @XmlElement(name = "ServiceFault")
    protected List<ServiceFault> serviceFault;

    /**
     * Ruft den Wert der schadenereignis-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SchadenereignisType }
     *     
     */
    public SchadenereignisType getSchadenereignis() {
        return schadenereignis;
    }

    /**
     * Legt den Wert der schadenereignis-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SchadenereignisType }
     *     
     */
    public void setSchadenereignis(SchadenereignisType value) {
        this.schadenereignis = value;
    }

    /**
     * Gets the value of the serviceFault property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the serviceFault property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getServiceFault().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ServiceFault }
     * 
     * 
     */
    public List<ServiceFault> getServiceFault() {
        if (serviceFault == null) {
            serviceFault = new ArrayList<ServiceFault>();
        }
        return this.serviceFault;
    }

}
