
package at.vvo.omds.types.omds3Types.v1_3_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.v1_3_0.common.CommonSearchRequestType;
import at.vvo.omds.types.omds3Types.v1_3_0.common.GeschaeftsobjektArtType;


/**
 * Typ des Requestobjektes um Geschäftsfalle abzuholen
 * 
 * <p>Java-Klasse für GetStatusChangesRequest_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="GetStatusChangesRequest_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}CommonSearchRequest_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GeschaeftsobjektArt" type="{urn:omds3CommonServiceTypes-1-1-0}GeschaeftsobjektArt_Type" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetStatusChangesRequest_Type", propOrder = {
    "geschaeftsobjektArt"
})
public class GetStatusChangesRequestType
    extends CommonSearchRequestType
{

    @XmlElement(name = "GeschaeftsobjektArt")
    @XmlSchemaType(name = "string")
    protected GeschaeftsobjektArtType geschaeftsobjektArt;

    /**
     * Ruft den Wert der geschaeftsobjektArt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GeschaeftsobjektArtType }
     *     
     */
    public GeschaeftsobjektArtType getGeschaeftsobjektArt() {
        return geschaeftsobjektArt;
    }

    /**
     * Legt den Wert der geschaeftsobjektArt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GeschaeftsobjektArtType }
     *     
     */
    public void setGeschaeftsobjektArt(GeschaeftsobjektArtType value) {
        this.geschaeftsobjektArt = value;
    }

}
