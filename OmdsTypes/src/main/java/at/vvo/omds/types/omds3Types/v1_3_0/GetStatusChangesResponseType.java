
package at.vvo.omds.types.omds3Types.v1_3_0;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import at.vvo.omds.types.omds3Types.v1_3_0.common.CommonSearchResponseType;
import at.vvo.omds.types.omds3Types.v1_3_0.common.GeschaeftsobjektArtType;
import at.vvo.omds.types.omds3Types.v1_3_0.common.ObjektIdType;


/**
 * Typ des Responseobjektes um Geschäftsfalle abzuholen
 * 
 * <p>Java-Klasse für GetStatusChangesResponse_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="GetStatusChangesResponse_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}CommonSearchResponse_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Ergebnis" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Objektart" type="{urn:omds3CommonServiceTypes-1-1-0}GeschaeftsobjektArt_Type"/&gt;
 *                   &lt;element name="ObjektId" type="{urn:omds3CommonServiceTypes-1-1-0}ObjektId_Type"/&gt;
 *                   &lt;element name="LetzteAenderung" type="{urn:omds20}Datum-Zeit"/&gt;
 *                   &lt;element ref="{urn:omds3CommonServiceTypes-1-1-0}GeschaeftsfallId" minOccurs="0"/&gt;
 *                   &lt;element name="GeschaeftsfallArt" type="{urn:omds3CommonServiceTypes-1-1-0}GeschaeftsfallArt_Type" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetStatusChangesResponse_Type", propOrder = {
    "ergebnis"
})
public class GetStatusChangesResponseType
    extends CommonSearchResponseType
{

    @XmlElement(name = "Ergebnis")
    protected List<GetStatusChangesResponseType.Ergebnis> ergebnis;

    /**
     * Gets the value of the ergebnis property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ergebnis property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getErgebnis().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetStatusChangesResponseType.Ergebnis }
     * 
     * 
     */
    public List<GetStatusChangesResponseType.Ergebnis> getErgebnis() {
        if (ergebnis == null) {
            ergebnis = new ArrayList<GetStatusChangesResponseType.Ergebnis>();
        }
        return this.ergebnis;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Objektart" type="{urn:omds3CommonServiceTypes-1-1-0}GeschaeftsobjektArt_Type"/&gt;
     *         &lt;element name="ObjektId" type="{urn:omds3CommonServiceTypes-1-1-0}ObjektId_Type"/&gt;
     *         &lt;element name="LetzteAenderung" type="{urn:omds20}Datum-Zeit"/&gt;
     *         &lt;element ref="{urn:omds3CommonServiceTypes-1-1-0}GeschaeftsfallId" minOccurs="0"/&gt;
     *         &lt;element name="GeschaeftsfallArt" type="{urn:omds3CommonServiceTypes-1-1-0}GeschaeftsfallArt_Type" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "objektart",
        "objektId",
        "letzteAenderung",
        "geschaeftsfallId",
        "geschaeftsfallArt"
    })
    public static class Ergebnis {

        @XmlElement(name = "Objektart", required = true)
        @XmlSchemaType(name = "string")
        protected GeschaeftsobjektArtType objektart;
        @XmlElement(name = "ObjektId", required = true)
        protected ObjektIdType objektId;
        @XmlElement(name = "LetzteAenderung", required = true)
        @XmlSchemaType(name = "dateTime")
        protected XMLGregorianCalendar letzteAenderung;
        @XmlElement(name = "GeschaeftsfallId", namespace = "urn:omds3CommonServiceTypes-1-1-0")
        protected String geschaeftsfallId;
        @XmlElement(name = "GeschaeftsfallArt")
        @XmlSchemaType(name = "anySimpleType")
        protected String geschaeftsfallArt;

        /**
         * Ruft den Wert der objektart-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link GeschaeftsobjektArtType }
         *     
         */
        public GeschaeftsobjektArtType getObjektart() {
            return objektart;
        }

        /**
         * Legt den Wert der objektart-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link GeschaeftsobjektArtType }
         *     
         */
        public void setObjektart(GeschaeftsobjektArtType value) {
            this.objektart = value;
        }

        /**
         * Ruft den Wert der objektId-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link ObjektIdType }
         *     
         */
        public ObjektIdType getObjektId() {
            return objektId;
        }

        /**
         * Legt den Wert der objektId-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link ObjektIdType }
         *     
         */
        public void setObjektId(ObjektIdType value) {
            this.objektId = value;
        }

        /**
         * Ruft den Wert der letzteAenderung-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getLetzteAenderung() {
            return letzteAenderung;
        }

        /**
         * Legt den Wert der letzteAenderung-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setLetzteAenderung(XMLGregorianCalendar value) {
            this.letzteAenderung = value;
        }

        /**
         * Ruft den Wert der geschaeftsfallId-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getGeschaeftsfallId() {
            return geschaeftsfallId;
        }

        /**
         * Legt den Wert der geschaeftsfallId-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setGeschaeftsfallId(String value) {
            this.geschaeftsfallId = value;
        }

        /**
         * Ruft den Wert der geschaeftsfallArt-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getGeschaeftsfallArt() {
            return geschaeftsfallArt;
        }

        /**
         * Legt den Wert der geschaeftsfallArt-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setGeschaeftsfallArt(String value) {
            this.geschaeftsfallArt = value;
        }

    }

}
