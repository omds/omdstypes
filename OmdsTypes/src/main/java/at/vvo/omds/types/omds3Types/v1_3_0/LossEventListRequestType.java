
package at.vvo.omds.types.omds3Types.v1_3_0;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds2Types.v2_9.ELZeitraumType;
import at.vvo.omds.types.omds3Types.v1_3_0.common.AuthorizationFilter;


/**
 * Typ für Request um Liste mit Schadens-Events zu erhalten
 * 
 * <p>Java-Klasse für LossEventListRequest_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="LossEventListRequest_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="VUNr" type="{urn:omds20}VUNr" minOccurs="0"/&gt;
 *         &lt;element name="AuthFilter" type="{urn:omds3CommonServiceTypes-1-1-0}AuthorizationFilter" minOccurs="0"/&gt;
 *         &lt;element name="Polizzennr" type="{urn:omds20}Polizzennr" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Zeitraum" type="{urn:omds20}EL-Zeitraum_Type"/&gt;
 *         &lt;element name="MaxResults" type="{http://www.w3.org/2001/XMLSchema}unsignedInt"/&gt;
 *         &lt;element name="Offset" type="{http://www.w3.org/2001/XMLSchema}unsignedInt"/&gt;
 *         &lt;element name="OrderBy" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;enumeration value="Ereigniszeitpunkt"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LossEventListRequest_Type", propOrder = {
    "vuNr",
    "authFilter",
    "polizzennr",
    "zeitraum",
    "maxResults",
    "offset",
    "orderBy"
})
public class LossEventListRequestType {

    @XmlElement(name = "VUNr")
    protected String vuNr;
    @XmlElement(name = "AuthFilter")
    protected AuthorizationFilter authFilter;
    @XmlElement(name = "Polizzennr")
    protected List<String> polizzennr;
    @XmlElement(name = "Zeitraum", required = true)
    protected ELZeitraumType zeitraum;
    @XmlElement(name = "MaxResults")
    @XmlSchemaType(name = "unsignedInt")
    protected long maxResults;
    @XmlElement(name = "Offset")
    @XmlSchemaType(name = "unsignedInt")
    protected long offset;
    @XmlElement(name = "OrderBy")
    protected String orderBy;

    /**
     * Ruft den Wert der vuNr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVUNr() {
        return vuNr;
    }

    /**
     * Legt den Wert der vuNr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVUNr(String value) {
        this.vuNr = value;
    }

    /**
     * Ruft den Wert der authFilter-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AuthorizationFilter }
     *     
     */
    public AuthorizationFilter getAuthFilter() {
        return authFilter;
    }

    /**
     * Legt den Wert der authFilter-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AuthorizationFilter }
     *     
     */
    public void setAuthFilter(AuthorizationFilter value) {
        this.authFilter = value;
    }

    /**
     * Gets the value of the polizzennr property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the polizzennr property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPolizzennr().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getPolizzennr() {
        if (polizzennr == null) {
            polizzennr = new ArrayList<String>();
        }
        return this.polizzennr;
    }

    /**
     * Ruft den Wert der zeitraum-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ELZeitraumType }
     *     
     */
    public ELZeitraumType getZeitraum() {
        return zeitraum;
    }

    /**
     * Legt den Wert der zeitraum-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ELZeitraumType }
     *     
     */
    public void setZeitraum(ELZeitraumType value) {
        this.zeitraum = value;
    }

    /**
     * Ruft den Wert der maxResults-Eigenschaft ab.
     * 
     */
    public long getMaxResults() {
        return maxResults;
    }

    /**
     * Legt den Wert der maxResults-Eigenschaft fest.
     * 
     */
    public void setMaxResults(long value) {
        this.maxResults = value;
    }

    /**
     * Ruft den Wert der offset-Eigenschaft ab.
     * 
     */
    public long getOffset() {
        return offset;
    }

    /**
     * Legt den Wert der offset-Eigenschaft fest.
     * 
     */
    public void setOffset(long value) {
        this.offset = value;
    }

    /**
     * Ruft den Wert der orderBy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderBy() {
        return orderBy;
    }

    /**
     * Legt den Wert der orderBy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderBy(String value) {
        this.orderBy = value;
    }

}
