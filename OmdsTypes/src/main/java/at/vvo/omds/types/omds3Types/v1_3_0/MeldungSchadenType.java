
package at.vvo.omds.types.omds3Types.v1_3_0;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds2Types.v2_9.WaehrungsCdType;


/**
 * Typ um ein Schadenobjekt in der Schadenmeldung abzubilden
 * 
 * <p>Java-Klasse für MeldungSchaden_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="MeldungSchaden_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:omds3ServiceTypes-1-1-0}Schadenzuordnung"/&gt;
 *         &lt;element name="Polizzennr" type="{urn:omds20}Polizzennr"/&gt;
 *         &lt;element name="VertragsID" type="{urn:omds20}VertragsID" minOccurs="0"/&gt;
 *         &lt;element name="SchadenTxt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Schadenbeteiligte" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="GeschInteresseLfnr" type="{http://www.w3.org/2001/XMLSchema}unsignedShort" maxOccurs="unbounded" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *                 &lt;attribute name="BetLfnr" use="required" type="{http://www.w3.org/2001/XMLSchema}unsignedShort" /&gt;
 *                 &lt;attribute name="BetRolleCd" use="required" type="{urn:omds20}BetRolleCd_Type" /&gt;
 *                 &lt;attribute name="BetTxt"&gt;
 *                   &lt;simpleType&gt;
 *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *                       &lt;maxLength value="100"/&gt;
 *                     &lt;/restriction&gt;
 *                   &lt;/simpleType&gt;
 *                 &lt;/attribute&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="LeistungGeschaetzt" type="{urn:omds20}decimal" minOccurs="0"/&gt;
 *         &lt;element name="WaehrungsCd" type="{urn:omds20}WaehrungsCd_Type" minOccurs="0"/&gt;
 *         &lt;element name="Spartendetails" type="{urn:omds3ServiceTypes-1-1-0}SpartendetailSchaden_Type" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MeldungSchaden_Type", propOrder = {
    "schadenzuordnung",
    "polizzennr",
    "vertragsID",
    "schadenTxt",
    "schadenbeteiligte",
    "leistungGeschaetzt",
    "waehrungsCd",
    "spartendetails"
})
public class MeldungSchadenType {

    @XmlElement(name = "Schadenzuordnung", required = true)
    protected SchadenzuordnungType schadenzuordnung;
    @XmlElement(name = "Polizzennr", required = true)
    protected String polizzennr;
    @XmlElement(name = "VertragsID")
    protected String vertragsID;
    @XmlElement(name = "SchadenTxt")
    protected String schadenTxt;
    @XmlElement(name = "Schadenbeteiligte")
    protected List<MeldungSchadenType.Schadenbeteiligte> schadenbeteiligte;
    @XmlElement(name = "LeistungGeschaetzt")
    protected BigDecimal leistungGeschaetzt;
    @XmlElement(name = "WaehrungsCd")
    @XmlSchemaType(name = "string")
    protected WaehrungsCdType waehrungsCd;
    @XmlElement(name = "Spartendetails")
    protected SpartendetailSchadenType spartendetails;

    /**
     * Ruft den Wert der schadenzuordnung-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SchadenzuordnungType }
     *     
     */
    public SchadenzuordnungType getSchadenzuordnung() {
        return schadenzuordnung;
    }

    /**
     * Legt den Wert der schadenzuordnung-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SchadenzuordnungType }
     *     
     */
    public void setSchadenzuordnung(SchadenzuordnungType value) {
        this.schadenzuordnung = value;
    }

    /**
     * Ruft den Wert der polizzennr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolizzennr() {
        return polizzennr;
    }

    /**
     * Legt den Wert der polizzennr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolizzennr(String value) {
        this.polizzennr = value;
    }

    /**
     * Ruft den Wert der vertragsID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVertragsID() {
        return vertragsID;
    }

    /**
     * Legt den Wert der vertragsID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVertragsID(String value) {
        this.vertragsID = value;
    }

    /**
     * Ruft den Wert der schadenTxt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSchadenTxt() {
        return schadenTxt;
    }

    /**
     * Legt den Wert der schadenTxt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSchadenTxt(String value) {
        this.schadenTxt = value;
    }

    /**
     * Gets the value of the schadenbeteiligte property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the schadenbeteiligte property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSchadenbeteiligte().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MeldungSchadenType.Schadenbeteiligte }
     * 
     * 
     */
    public List<MeldungSchadenType.Schadenbeteiligte> getSchadenbeteiligte() {
        if (schadenbeteiligte == null) {
            schadenbeteiligte = new ArrayList<MeldungSchadenType.Schadenbeteiligte>();
        }
        return this.schadenbeteiligte;
    }

    /**
     * Ruft den Wert der leistungGeschaetzt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getLeistungGeschaetzt() {
        return leistungGeschaetzt;
    }

    /**
     * Legt den Wert der leistungGeschaetzt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setLeistungGeschaetzt(BigDecimal value) {
        this.leistungGeschaetzt = value;
    }

    /**
     * Ruft den Wert der waehrungsCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link WaehrungsCdType }
     *     
     */
    public WaehrungsCdType getWaehrungsCd() {
        return waehrungsCd;
    }

    /**
     * Legt den Wert der waehrungsCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link WaehrungsCdType }
     *     
     */
    public void setWaehrungsCd(WaehrungsCdType value) {
        this.waehrungsCd = value;
    }

    /**
     * Ruft den Wert der spartendetails-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SpartendetailSchadenType }
     *     
     */
    public SpartendetailSchadenType getSpartendetails() {
        return spartendetails;
    }

    /**
     * Legt den Wert der spartendetails-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SpartendetailSchadenType }
     *     
     */
    public void setSpartendetails(SpartendetailSchadenType value) {
        this.spartendetails = value;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="GeschInteresseLfnr" type="{http://www.w3.org/2001/XMLSchema}unsignedShort" maxOccurs="unbounded" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *       &lt;attribute name="BetLfnr" use="required" type="{http://www.w3.org/2001/XMLSchema}unsignedShort" /&gt;
     *       &lt;attribute name="BetRolleCd" use="required" type="{urn:omds20}BetRolleCd_Type" /&gt;
     *       &lt;attribute name="BetTxt"&gt;
     *         &lt;simpleType&gt;
     *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
     *             &lt;maxLength value="100"/&gt;
     *           &lt;/restriction&gt;
     *         &lt;/simpleType&gt;
     *       &lt;/attribute&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "geschInteresseLfnr"
    })
    public static class Schadenbeteiligte {

        @XmlElement(name = "GeschInteresseLfnr", type = Integer.class)
        @XmlSchemaType(name = "unsignedShort")
        protected List<Integer> geschInteresseLfnr;
        @XmlAttribute(name = "BetLfnr", namespace = "urn:omds3ServiceTypes-1-1-0", required = true)
        @XmlSchemaType(name = "unsignedShort")
        protected int betLfnr;
        @XmlAttribute(name = "BetRolleCd", namespace = "urn:omds3ServiceTypes-1-1-0", required = true)
        protected String betRolleCd;
        @XmlAttribute(name = "BetTxt", namespace = "urn:omds3ServiceTypes-1-1-0")
        protected String betTxt;

        /**
         * Gets the value of the geschInteresseLfnr property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the geschInteresseLfnr property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getGeschInteresseLfnr().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Integer }
         * 
         * 
         */
        public List<Integer> getGeschInteresseLfnr() {
            if (geschInteresseLfnr == null) {
                geschInteresseLfnr = new ArrayList<Integer>();
            }
            return this.geschInteresseLfnr;
        }

        /**
         * Ruft den Wert der betLfnr-Eigenschaft ab.
         * 
         */
        public int getBetLfnr() {
            return betLfnr;
        }

        /**
         * Legt den Wert der betLfnr-Eigenschaft fest.
         * 
         */
        public void setBetLfnr(int value) {
            this.betLfnr = value;
        }

        /**
         * Ruft den Wert der betRolleCd-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBetRolleCd() {
            return betRolleCd;
        }

        /**
         * Legt den Wert der betRolleCd-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBetRolleCd(String value) {
            this.betRolleCd = value;
        }

        /**
         * Ruft den Wert der betTxt-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBetTxt() {
            return betTxt;
        }

        /**
         * Legt den Wert der betTxt-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBetTxt(String value) {
            this.betTxt = value;
        }

    }

}
