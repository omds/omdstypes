
package at.vvo.omds.types.omds3Types.v1_3_0;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.v1_3_0.common.ServiceFault;


/**
 * Objekt, welches GeschäftsfallId und Schadennummer, Dokumenten-Ids sowie den Bearbeitungsstand enthält
 * 
 * <p>Java-Klasse für Meldungszusammenfassung_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Meldungszusammenfassung_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:omds3ServiceTypes-1-1-0}IdGeschaeftsfallSchadenereignis"/&gt;
 *         &lt;element name="VUNr" type="{urn:omds20}VUNr"/&gt;
 *         &lt;element ref="{urn:omds3CommonServiceTypes-1-1-0}OrdnungsbegriffZuordFremd" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ErgebnisDokumente" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="LfdNr" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *                   &lt;choice&gt;
 *                     &lt;element name="Dokument" type="{urn:omds3ServiceTypes-1-1-0}ArcImageInfo"/&gt;
 *                     &lt;element name="FehlerDokumentenanlage" type="{urn:omds3CommonServiceTypes-1-1-0}ServiceFault"/&gt;
 *                   &lt;/choice&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="ErgebnisSchaeden" maxOccurs="unbounded"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="LfdNr" type="{http://www.w3.org/2001/XMLSchema}unsignedInt"/&gt;
 *                   &lt;choice&gt;
 *                     &lt;element name="Schadenanlage"&gt;
 *                       &lt;complexType&gt;
 *                         &lt;complexContent&gt;
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                             &lt;sequence&gt;
 *                               &lt;element ref="{urn:omds3ServiceTypes-1-1-0}IdGeschaeftsfallSchadenanlage"/&gt;
 *                               &lt;element ref="{urn:omds3ServiceTypes-1-1-0}Schadenzuordnung"/&gt;
 *                               &lt;element name="BearbStandCd" type="{urn:omds3ServiceTypes-1-1-0}BearbStandCd_Type"/&gt;
 *                               &lt;element name="Schadennr" type="{urn:omds20}Schadennr" minOccurs="0"/&gt;
 *                               &lt;element name="SachbearbVU" type="{urn:omds3ServiceTypes-1-1-0}SachbearbVUType" minOccurs="0"/&gt;
 *                             &lt;/sequence&gt;
 *                           &lt;/restriction&gt;
 *                         &lt;/complexContent&gt;
 *                       &lt;/complexType&gt;
 *                     &lt;/element&gt;
 *                     &lt;element name="FehlerSchadenanlage" type="{urn:omds3CommonServiceTypes-1-1-0}ServiceFault"/&gt;
 *                   &lt;/choice&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Meldungszusammenfassung_Type", propOrder = {
    "idGeschaeftsfallSchadenereignis",
    "vuNr",
    "ordnungsbegriffZuordFremd",
    "ergebnisDokumente",
    "ergebnisSchaeden"
})
public class MeldungszusammenfassungType {

    @XmlElement(name = "IdGeschaeftsfallSchadenereignis", required = true)
    protected String idGeschaeftsfallSchadenereignis;
    @XmlElement(name = "VUNr", required = true)
    protected String vuNr;
    @XmlElement(name = "OrdnungsbegriffZuordFremd", namespace = "urn:omds3CommonServiceTypes-1-1-0")
    protected List<String> ordnungsbegriffZuordFremd;
    @XmlElement(name = "ErgebnisDokumente")
    protected List<MeldungszusammenfassungType.ErgebnisDokumente> ergebnisDokumente;
    @XmlElement(name = "ErgebnisSchaeden", required = true)
    protected List<MeldungszusammenfassungType.ErgebnisSchaeden> ergebnisSchaeden;

    /**
     * Ruft den Wert der idGeschaeftsfallSchadenereignis-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdGeschaeftsfallSchadenereignis() {
        return idGeschaeftsfallSchadenereignis;
    }

    /**
     * Legt den Wert der idGeschaeftsfallSchadenereignis-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdGeschaeftsfallSchadenereignis(String value) {
        this.idGeschaeftsfallSchadenereignis = value;
    }

    /**
     * Ruft den Wert der vuNr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVUNr() {
        return vuNr;
    }

    /**
     * Legt den Wert der vuNr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVUNr(String value) {
        this.vuNr = value;
    }

    /**
     * Ordnungsbegriff des Schadenmelders auf Ebene des Schadenereignis Gets the value of the ordnungsbegriffZuordFremd property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ordnungsbegriffZuordFremd property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOrdnungsbegriffZuordFremd().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getOrdnungsbegriffZuordFremd() {
        if (ordnungsbegriffZuordFremd == null) {
            ordnungsbegriffZuordFremd = new ArrayList<String>();
        }
        return this.ordnungsbegriffZuordFremd;
    }

    /**
     * Gets the value of the ergebnisDokumente property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ergebnisDokumente property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getErgebnisDokumente().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MeldungszusammenfassungType.ErgebnisDokumente }
     * 
     * 
     */
    public List<MeldungszusammenfassungType.ErgebnisDokumente> getErgebnisDokumente() {
        if (ergebnisDokumente == null) {
            ergebnisDokumente = new ArrayList<MeldungszusammenfassungType.ErgebnisDokumente>();
        }
        return this.ergebnisDokumente;
    }

    /**
     * Gets the value of the ergebnisSchaeden property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ergebnisSchaeden property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getErgebnisSchaeden().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MeldungszusammenfassungType.ErgebnisSchaeden }
     * 
     * 
     */
    public List<MeldungszusammenfassungType.ErgebnisSchaeden> getErgebnisSchaeden() {
        if (ergebnisSchaeden == null) {
            ergebnisSchaeden = new ArrayList<MeldungszusammenfassungType.ErgebnisSchaeden>();
        }
        return this.ergebnisSchaeden;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="LfdNr" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
     *         &lt;choice&gt;
     *           &lt;element name="Dokument" type="{urn:omds3ServiceTypes-1-1-0}ArcImageInfo"/&gt;
     *           &lt;element name="FehlerDokumentenanlage" type="{urn:omds3CommonServiceTypes-1-1-0}ServiceFault"/&gt;
     *         &lt;/choice&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "lfdNr",
        "dokument",
        "fehlerDokumentenanlage"
    })
    public static class ErgebnisDokumente {

        @XmlElement(name = "LfdNr")
        protected int lfdNr;
        @XmlElement(name = "Dokument")
        protected ArcImageInfo dokument;
        @XmlElement(name = "FehlerDokumentenanlage")
        protected ServiceFault fehlerDokumentenanlage;

        /**
         * Ruft den Wert der lfdNr-Eigenschaft ab.
         * 
         */
        public int getLfdNr() {
            return lfdNr;
        }

        /**
         * Legt den Wert der lfdNr-Eigenschaft fest.
         * 
         */
        public void setLfdNr(int value) {
            this.lfdNr = value;
        }

        /**
         * Ruft den Wert der dokument-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link ArcImageInfo }
         *     
         */
        public ArcImageInfo getDokument() {
            return dokument;
        }

        /**
         * Legt den Wert der dokument-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link ArcImageInfo }
         *     
         */
        public void setDokument(ArcImageInfo value) {
            this.dokument = value;
        }

        /**
         * Ruft den Wert der fehlerDokumentenanlage-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link ServiceFault }
         *     
         */
        public ServiceFault getFehlerDokumentenanlage() {
            return fehlerDokumentenanlage;
        }

        /**
         * Legt den Wert der fehlerDokumentenanlage-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link ServiceFault }
         *     
         */
        public void setFehlerDokumentenanlage(ServiceFault value) {
            this.fehlerDokumentenanlage = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="LfdNr" type="{http://www.w3.org/2001/XMLSchema}unsignedInt"/&gt;
     *         &lt;choice&gt;
     *           &lt;element name="Schadenanlage"&gt;
     *             &lt;complexType&gt;
     *               &lt;complexContent&gt;
     *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                   &lt;sequence&gt;
     *                     &lt;element ref="{urn:omds3ServiceTypes-1-1-0}IdGeschaeftsfallSchadenanlage"/&gt;
     *                     &lt;element ref="{urn:omds3ServiceTypes-1-1-0}Schadenzuordnung"/&gt;
     *                     &lt;element name="BearbStandCd" type="{urn:omds3ServiceTypes-1-1-0}BearbStandCd_Type"/&gt;
     *                     &lt;element name="Schadennr" type="{urn:omds20}Schadennr" minOccurs="0"/&gt;
     *                     &lt;element name="SachbearbVU" type="{urn:omds3ServiceTypes-1-1-0}SachbearbVUType" minOccurs="0"/&gt;
     *                   &lt;/sequence&gt;
     *                 &lt;/restriction&gt;
     *               &lt;/complexContent&gt;
     *             &lt;/complexType&gt;
     *           &lt;/element&gt;
     *           &lt;element name="FehlerSchadenanlage" type="{urn:omds3CommonServiceTypes-1-1-0}ServiceFault"/&gt;
     *         &lt;/choice&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "lfdNr",
        "schadenanlage",
        "fehlerSchadenanlage"
    })
    public static class ErgebnisSchaeden {

        @XmlElement(name = "LfdNr")
        @XmlSchemaType(name = "unsignedInt")
        protected long lfdNr;
        @XmlElement(name = "Schadenanlage")
        protected MeldungszusammenfassungType.ErgebnisSchaeden.Schadenanlage schadenanlage;
        @XmlElement(name = "FehlerSchadenanlage")
        protected ServiceFault fehlerSchadenanlage;

        /**
         * Ruft den Wert der lfdNr-Eigenschaft ab.
         * 
         */
        public long getLfdNr() {
            return lfdNr;
        }

        /**
         * Legt den Wert der lfdNr-Eigenschaft fest.
         * 
         */
        public void setLfdNr(long value) {
            this.lfdNr = value;
        }

        /**
         * Ruft den Wert der schadenanlage-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link MeldungszusammenfassungType.ErgebnisSchaeden.Schadenanlage }
         *     
         */
        public MeldungszusammenfassungType.ErgebnisSchaeden.Schadenanlage getSchadenanlage() {
            return schadenanlage;
        }

        /**
         * Legt den Wert der schadenanlage-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link MeldungszusammenfassungType.ErgebnisSchaeden.Schadenanlage }
         *     
         */
        public void setSchadenanlage(MeldungszusammenfassungType.ErgebnisSchaeden.Schadenanlage value) {
            this.schadenanlage = value;
        }

        /**
         * Ruft den Wert der fehlerSchadenanlage-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link ServiceFault }
         *     
         */
        public ServiceFault getFehlerSchadenanlage() {
            return fehlerSchadenanlage;
        }

        /**
         * Legt den Wert der fehlerSchadenanlage-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link ServiceFault }
         *     
         */
        public void setFehlerSchadenanlage(ServiceFault value) {
            this.fehlerSchadenanlage = value;
        }


        /**
         * <p>Java-Klasse für anonymous complex type.
         * 
         * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element ref="{urn:omds3ServiceTypes-1-1-0}IdGeschaeftsfallSchadenanlage"/&gt;
         *         &lt;element ref="{urn:omds3ServiceTypes-1-1-0}Schadenzuordnung"/&gt;
         *         &lt;element name="BearbStandCd" type="{urn:omds3ServiceTypes-1-1-0}BearbStandCd_Type"/&gt;
         *         &lt;element name="Schadennr" type="{urn:omds20}Schadennr" minOccurs="0"/&gt;
         *         &lt;element name="SachbearbVU" type="{urn:omds3ServiceTypes-1-1-0}SachbearbVUType" minOccurs="0"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "idGeschaeftsfallSchadenanlage",
            "schadenzuordnung",
            "bearbStandCd",
            "schadennr",
            "sachbearbVU"
        })
        public static class Schadenanlage {

            @XmlElement(name = "IdGeschaeftsfallSchadenanlage", required = true)
            protected String idGeschaeftsfallSchadenanlage;
            @XmlElement(name = "Schadenzuordnung", required = true)
            protected SchadenzuordnungType schadenzuordnung;
            @XmlElement(name = "BearbStandCd", required = true)
            protected String bearbStandCd;
            @XmlElement(name = "Schadennr")
            protected String schadennr;
            @XmlElement(name = "SachbearbVU")
            protected SachbearbVUType sachbearbVU;

            /**
             * Ruft den Wert der idGeschaeftsfallSchadenanlage-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIdGeschaeftsfallSchadenanlage() {
                return idGeschaeftsfallSchadenanlage;
            }

            /**
             * Legt den Wert der idGeschaeftsfallSchadenanlage-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIdGeschaeftsfallSchadenanlage(String value) {
                this.idGeschaeftsfallSchadenanlage = value;
            }

            /**
             * Anhand der Schadenzuordnung kann man erkennen, um welche Schadensparte es sich handelt.
             * 
             * @return
             *     possible object is
             *     {@link SchadenzuordnungType }
             *     
             */
            public SchadenzuordnungType getSchadenzuordnung() {
                return schadenzuordnung;
            }

            /**
             * Legt den Wert der schadenzuordnung-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link SchadenzuordnungType }
             *     
             */
            public void setSchadenzuordnung(SchadenzuordnungType value) {
                this.schadenzuordnung = value;
            }

            /**
             * Ruft den Wert der bearbStandCd-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBearbStandCd() {
                return bearbStandCd;
            }

            /**
             * Legt den Wert der bearbStandCd-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBearbStandCd(String value) {
                this.bearbStandCd = value;
            }

            /**
             * Ruft den Wert der schadennr-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSchadennr() {
                return schadennr;
            }

            /**
             * Legt den Wert der schadennr-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSchadennr(String value) {
                this.schadennr = value;
            }

            /**
             * Ruft den Wert der sachbearbVU-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link SachbearbVUType }
             *     
             */
            public SachbearbVUType getSachbearbVU() {
                return sachbearbVU;
            }

            /**
             * Legt den Wert der sachbearbVU-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link SachbearbVUType }
             *     
             */
            public void setSachbearbVU(SachbearbVUType value) {
                this.sachbearbVU = value;
            }

        }

    }

}
