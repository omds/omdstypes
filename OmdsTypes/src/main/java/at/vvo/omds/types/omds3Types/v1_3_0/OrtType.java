
package at.vvo.omds.types.omds3Types.v1_3_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds2Types.v2_9.ADRESSEType;


/**
 * Typ für den Schadenort
 * 
 * <p>Java-Klasse für Ort_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Ort_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Adresse" type="{urn:omds20}ADRESSE_Type" minOccurs="0"/&gt;
 *         &lt;element name="Beschreibung" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Geokoordinaten" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;attribute name="Breite" type="{http://www.w3.org/2001/XMLSchema}double" /&gt;
 *                 &lt;attribute name="Laenge" type="{http://www.w3.org/2001/XMLSchema}double" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Ort_Type", propOrder = {
    "adresse",
    "beschreibung",
    "geokoordinaten"
})
public class OrtType {

    @XmlElement(name = "Adresse")
    protected ADRESSEType adresse;
    @XmlElement(name = "Beschreibung")
    protected String beschreibung;
    @XmlElement(name = "Geokoordinaten")
    protected OrtType.Geokoordinaten geokoordinaten;

    /**
     * Ruft den Wert der adresse-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ADRESSEType }
     *     
     */
    public ADRESSEType getAdresse() {
        return adresse;
    }

    /**
     * Legt den Wert der adresse-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ADRESSEType }
     *     
     */
    public void setAdresse(ADRESSEType value) {
        this.adresse = value;
    }

    /**
     * Ruft den Wert der beschreibung-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBeschreibung() {
        return beschreibung;
    }

    /**
     * Legt den Wert der beschreibung-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBeschreibung(String value) {
        this.beschreibung = value;
    }

    /**
     * Ruft den Wert der geokoordinaten-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OrtType.Geokoordinaten }
     *     
     */
    public OrtType.Geokoordinaten getGeokoordinaten() {
        return geokoordinaten;
    }

    /**
     * Legt den Wert der geokoordinaten-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OrtType.Geokoordinaten }
     *     
     */
    public void setGeokoordinaten(OrtType.Geokoordinaten value) {
        this.geokoordinaten = value;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;attribute name="Breite" type="{http://www.w3.org/2001/XMLSchema}double" /&gt;
     *       &lt;attribute name="Laenge" type="{http://www.w3.org/2001/XMLSchema}double" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Geokoordinaten {

        @XmlAttribute(name = "Breite", namespace = "urn:omds3ServiceTypes-1-1-0")
        protected Double breite;
        @XmlAttribute(name = "Laenge", namespace = "urn:omds3ServiceTypes-1-1-0")
        protected Double laenge;

        /**
         * Ruft den Wert der breite-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Double }
         *     
         */
        public Double getBreite() {
            return breite;
        }

        /**
         * Legt den Wert der breite-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Double }
         *     
         */
        public void setBreite(Double value) {
            this.breite = value;
        }

        /**
         * Ruft den Wert der laenge-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Double }
         *     
         */
        public Double getLaenge() {
            return laenge;
        }

        /**
         * Legt den Wert der laenge-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Double }
         *     
         */
        public void setLaenge(Double value) {
            this.laenge = value;
        }

    }

}
