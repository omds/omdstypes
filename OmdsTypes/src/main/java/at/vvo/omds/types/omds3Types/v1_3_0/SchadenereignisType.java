
package at.vvo.omds.types.omds3Types.v1_3_0;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import at.vvo.omds.types.omds2Types.v2_9.PERSONType;


/**
 * Objekttyp für die Darstellungen von Schadenereignissen
 * 
 * <p>Java-Klasse für Schadenereignis_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Schadenereignis_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="VUNr" type="{urn:omds20}VUNr"/&gt;
 *         &lt;element ref="{urn:omds3ServiceTypes-1-1-0}IdGeschaeftsfallSchadenereignis"/&gt;
 *         &lt;element name="VormaligeIdGeschaeftsfall" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NachfolgendeIdGeschaeftsfall" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:omds3CommonServiceTypes-1-1-0}OrdnungsbegriffZuordFremd" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="EreignisZpkt" type="{urn:omds20}Datum-Zeit"/&gt;
 *         &lt;element name="EreignisbeschrTxt" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="SchadOrt" type="{urn:omds3ServiceTypes-1-1-0}Ort_Type"/&gt;
 *         &lt;element name="BeteiligtePersonen" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Person" type="{urn:omds20}PERSON_Type"/&gt;
 *                   &lt;element name="GeschInteresseLfnr" type="{http://www.w3.org/2001/XMLSchema}anyType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *                 &lt;attribute name="BetLfnr" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *                 &lt;attribute name="BetRolleCd" type="{urn:omds20}BetRolleCd_Type" /&gt;
 *                 &lt;attribute name="BetTxt"&gt;
 *                   &lt;simpleType&gt;
 *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *                       &lt;maxLength value="100"/&gt;
 *                     &lt;/restriction&gt;
 *                   &lt;/simpleType&gt;
 *                 &lt;/attribute&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="GeschaedigteInteressen" type="{urn:omds3ServiceTypes-1-1-0}GeschaedigtesInteresse_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Dokumente" type="{urn:omds3ServiceTypes-1-1-0}ArcImageInfo" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Schaeden" type="{urn:omds3ServiceTypes-1-1-0}Schaden_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Meldedat" type="{urn:omds20}Datum-Zeit"/&gt;
 *         &lt;element name="Schadenmelder" type="{urn:omds3ServiceTypes-1-1-0}Schadenmelder_Type"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Schadenereignis_Type", propOrder = {
    "vuNr",
    "idGeschaeftsfallSchadenereignis",
    "vormaligeIdGeschaeftsfall",
    "nachfolgendeIdGeschaeftsfall",
    "ordnungsbegriffZuordFremd",
    "ereignisZpkt",
    "ereignisbeschrTxt",
    "schadOrt",
    "beteiligtePersonen",
    "geschaedigteInteressen",
    "dokumente",
    "schaeden",
    "meldedat",
    "schadenmelder"
})
public class SchadenereignisType {

    @XmlElement(name = "VUNr", required = true)
    protected String vuNr;
    @XmlElement(name = "IdGeschaeftsfallSchadenereignis", required = true)
    protected String idGeschaeftsfallSchadenereignis;
    @XmlElement(name = "VormaligeIdGeschaeftsfall")
    protected String vormaligeIdGeschaeftsfall;
    @XmlElement(name = "NachfolgendeIdGeschaeftsfall")
    protected String nachfolgendeIdGeschaeftsfall;
    @XmlElement(name = "OrdnungsbegriffZuordFremd", namespace = "urn:omds3CommonServiceTypes-1-1-0")
    protected List<String> ordnungsbegriffZuordFremd;
    @XmlElement(name = "EreignisZpkt", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar ereignisZpkt;
    @XmlElement(name = "EreignisbeschrTxt", required = true)
    protected String ereignisbeschrTxt;
    @XmlElement(name = "SchadOrt", required = true)
    protected OrtType schadOrt;
    @XmlElement(name = "BeteiligtePersonen")
    protected List<SchadenereignisType.BeteiligtePersonen> beteiligtePersonen;
    @XmlElement(name = "GeschaedigteInteressen")
    protected List<GeschaedigtesInteresseType> geschaedigteInteressen;
    @XmlElement(name = "Dokumente")
    protected List<ArcImageInfo> dokumente;
    @XmlElement(name = "Schaeden")
    protected List<SchadenType> schaeden;
    @XmlElement(name = "Meldedat", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar meldedat;
    @XmlElement(name = "Schadenmelder", required = true)
    protected SchadenmelderType schadenmelder;

    /**
     * Ruft den Wert der vuNr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVUNr() {
        return vuNr;
    }

    /**
     * Legt den Wert der vuNr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVUNr(String value) {
        this.vuNr = value;
    }

    /**
     * Ruft den Wert der idGeschaeftsfallSchadenereignis-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdGeschaeftsfallSchadenereignis() {
        return idGeschaeftsfallSchadenereignis;
    }

    /**
     * Legt den Wert der idGeschaeftsfallSchadenereignis-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdGeschaeftsfallSchadenereignis(String value) {
        this.idGeschaeftsfallSchadenereignis = value;
    }

    /**
     * Ruft den Wert der vormaligeIdGeschaeftsfall-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVormaligeIdGeschaeftsfall() {
        return vormaligeIdGeschaeftsfall;
    }

    /**
     * Legt den Wert der vormaligeIdGeschaeftsfall-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVormaligeIdGeschaeftsfall(String value) {
        this.vormaligeIdGeschaeftsfall = value;
    }

    /**
     * Ruft den Wert der nachfolgendeIdGeschaeftsfall-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNachfolgendeIdGeschaeftsfall() {
        return nachfolgendeIdGeschaeftsfall;
    }

    /**
     * Legt den Wert der nachfolgendeIdGeschaeftsfall-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNachfolgendeIdGeschaeftsfall(String value) {
        this.nachfolgendeIdGeschaeftsfall = value;
    }

    /**
     * Gets the value of the ordnungsbegriffZuordFremd property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ordnungsbegriffZuordFremd property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOrdnungsbegriffZuordFremd().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getOrdnungsbegriffZuordFremd() {
        if (ordnungsbegriffZuordFremd == null) {
            ordnungsbegriffZuordFremd = new ArrayList<String>();
        }
        return this.ordnungsbegriffZuordFremd;
    }

    /**
     * Ruft den Wert der ereignisZpkt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEreignisZpkt() {
        return ereignisZpkt;
    }

    /**
     * Legt den Wert der ereignisZpkt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEreignisZpkt(XMLGregorianCalendar value) {
        this.ereignisZpkt = value;
    }

    /**
     * Ruft den Wert der ereignisbeschrTxt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEreignisbeschrTxt() {
        return ereignisbeschrTxt;
    }

    /**
     * Legt den Wert der ereignisbeschrTxt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEreignisbeschrTxt(String value) {
        this.ereignisbeschrTxt = value;
    }

    /**
     * Ruft den Wert der schadOrt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OrtType }
     *     
     */
    public OrtType getSchadOrt() {
        return schadOrt;
    }

    /**
     * Legt den Wert der schadOrt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OrtType }
     *     
     */
    public void setSchadOrt(OrtType value) {
        this.schadOrt = value;
    }

    /**
     * Gets the value of the beteiligtePersonen property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the beteiligtePersonen property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBeteiligtePersonen().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SchadenereignisType.BeteiligtePersonen }
     * 
     * 
     */
    public List<SchadenereignisType.BeteiligtePersonen> getBeteiligtePersonen() {
        if (beteiligtePersonen == null) {
            beteiligtePersonen = new ArrayList<SchadenereignisType.BeteiligtePersonen>();
        }
        return this.beteiligtePersonen;
    }

    /**
     * Gets the value of the geschaedigteInteressen property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the geschaedigteInteressen property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGeschaedigteInteressen().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GeschaedigtesInteresseType }
     * 
     * 
     */
    public List<GeschaedigtesInteresseType> getGeschaedigteInteressen() {
        if (geschaedigteInteressen == null) {
            geschaedigteInteressen = new ArrayList<GeschaedigtesInteresseType>();
        }
        return this.geschaedigteInteressen;
    }

    /**
     * Gets the value of the dokumente property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dokumente property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDokumente().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ArcImageInfo }
     * 
     * 
     */
    public List<ArcImageInfo> getDokumente() {
        if (dokumente == null) {
            dokumente = new ArrayList<ArcImageInfo>();
        }
        return this.dokumente;
    }

    /**
     * Gets the value of the schaeden property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the schaeden property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSchaeden().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SchadenType }
     * 
     * 
     */
    public List<SchadenType> getSchaeden() {
        if (schaeden == null) {
            schaeden = new ArrayList<SchadenType>();
        }
        return this.schaeden;
    }

    /**
     * Ruft den Wert der meldedat-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getMeldedat() {
        return meldedat;
    }

    /**
     * Legt den Wert der meldedat-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setMeldedat(XMLGregorianCalendar value) {
        this.meldedat = value;
    }

    /**
     * Ruft den Wert der schadenmelder-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SchadenmelderType }
     *     
     */
    public SchadenmelderType getSchadenmelder() {
        return schadenmelder;
    }

    /**
     * Legt den Wert der schadenmelder-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SchadenmelderType }
     *     
     */
    public void setSchadenmelder(SchadenmelderType value) {
        this.schadenmelder = value;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Person" type="{urn:omds20}PERSON_Type"/&gt;
     *         &lt;element name="GeschInteresseLfnr" type="{http://www.w3.org/2001/XMLSchema}anyType" maxOccurs="unbounded" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *       &lt;attribute name="BetLfnr" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
     *       &lt;attribute name="BetRolleCd" type="{urn:omds20}BetRolleCd_Type" /&gt;
     *       &lt;attribute name="BetTxt"&gt;
     *         &lt;simpleType&gt;
     *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
     *             &lt;maxLength value="100"/&gt;
     *           &lt;/restriction&gt;
     *         &lt;/simpleType&gt;
     *       &lt;/attribute&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "person",
        "geschInteresseLfnr"
    })
    public static class BeteiligtePersonen {

        @XmlElement(name = "Person", required = true)
        protected PERSONType person;
        @XmlElement(name = "GeschInteresseLfnr")
        protected List<Object> geschInteresseLfnr;
        @XmlAttribute(name = "BetLfnr", namespace = "urn:omds3ServiceTypes-1-1-0", required = true)
        @XmlSchemaType(name = "anySimpleType")
        protected String betLfnr;
        @XmlAttribute(name = "BetRolleCd", namespace = "urn:omds3ServiceTypes-1-1-0")
        protected String betRolleCd;
        @XmlAttribute(name = "BetTxt", namespace = "urn:omds3ServiceTypes-1-1-0")
        protected String betTxt;

        /**
         * Ruft den Wert der person-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link PERSONType }
         *     
         */
        public PERSONType getPerson() {
            return person;
        }

        /**
         * Legt den Wert der person-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link PERSONType }
         *     
         */
        public void setPerson(PERSONType value) {
            this.person = value;
        }

        /**
         * Gets the value of the geschInteresseLfnr property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the geschInteresseLfnr property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getGeschInteresseLfnr().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Object }
         * 
         * 
         */
        public List<Object> getGeschInteresseLfnr() {
            if (geschInteresseLfnr == null) {
                geschInteresseLfnr = new ArrayList<Object>();
            }
            return this.geschInteresseLfnr;
        }

        /**
         * Ruft den Wert der betLfnr-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBetLfnr() {
            return betLfnr;
        }

        /**
         * Legt den Wert der betLfnr-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBetLfnr(String value) {
            this.betLfnr = value;
        }

        /**
         * Ruft den Wert der betRolleCd-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBetRolleCd() {
            return betRolleCd;
        }

        /**
         * Legt den Wert der betRolleCd-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBetRolleCd(String value) {
            this.betRolleCd = value;
        }

        /**
         * Ruft den Wert der betTxt-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBetTxt() {
            return betTxt;
        }

        /**
         * Legt den Wert der betTxt-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBetTxt(String value) {
            this.betTxt = value;
        }

    }

}
