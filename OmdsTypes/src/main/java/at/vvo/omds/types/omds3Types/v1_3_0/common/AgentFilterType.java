
package at.vvo.omds.types.omds3Types.v1_3_0.common;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Ein spezieller AuthorizationFilterType, der eine Anfrage dahingehend einschränkt, dass nur Ergebnisse für eine bestimmte MaklerID oder Vermnr (Vermittlernummer) zurück gegeben werden
 * 
 * <p>Java-Klasse für AgentFilter_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="AgentFilter_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}AuthorizationFilter"&gt;
 *       &lt;choice&gt;
 *         &lt;element name="MaklerID" type="{urn:omds3CommonServiceTypes-1-1-0}MaklerID_Type"/&gt;
 *         &lt;element name="Vermnr" type="{urn:omds20}Vermnr" maxOccurs="unbounded"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AgentFilter_Type", propOrder = {
    "maklerID",
    "vermnr"
})
public class AgentFilterType
    extends AuthorizationFilter
{

    @XmlElement(name = "MaklerID")
    protected String maklerID;
    @XmlElement(name = "Vermnr")
    protected List<String> vermnr;

    /**
     * Ruft den Wert der maklerID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMaklerID() {
        return maklerID;
    }

    /**
     * Legt den Wert der maklerID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMaklerID(String value) {
        this.maklerID = value;
    }

    /**
     * Gets the value of the vermnr property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the vermnr property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVermnr().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getVermnr() {
        if (vermnr == null) {
            vermnr = new ArrayList<String>();
        }
        return this.vermnr;
    }

}
