
package at.vvo.omds.types.omds3Types.v1_3_0.common;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für Antragsart_Type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="Antragsart_Type"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="NA"/&gt;
 *     &lt;enumeration value="FW"/&gt;
 *     &lt;enumeration value="VW"/&gt;
 *     &lt;enumeration value="WK"/&gt;
 *     &lt;enumeration value="TU"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "Antragsart_Type")
@XmlEnum
public enum AntragsartType {


    /**
     * Neuantrag
     * 
     */
    NA,

    /**
     * Fahrzeugwechsel
     * 
     */
    FW,

    /**
     * Versicherungswechsel
     * 
     */
    VW,

    /**
     * Wechselkennzeichen
     * 
     */
    WK,

    /**
     * Tarifumstellung
     * 
     */
    TU;

    public String value() {
        return name();
    }

    public static AntragsartType fromValue(String v) {
        return valueOf(v);
    }

}
