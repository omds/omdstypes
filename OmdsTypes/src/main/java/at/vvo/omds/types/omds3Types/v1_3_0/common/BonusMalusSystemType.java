
package at.vvo.omds.types.omds3Types.v1_3_0.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für BonusMalusSystem_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="BonusMalusSystem_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="BonusMalusVorversicherung" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="Versicherungsgesellschaft" type="{urn:omds3CommonServiceTypes-1-1-0}Versicherungsgesellschaft_Type" minOccurs="0"/&gt;
 *         &lt;element name="Polizzennummer" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;minLength value="1"/&gt;
 *               &lt;maxLength value="15"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="OffeneSchaeden" type="{urn:omds3CommonServiceTypes-1-1-0}OffeneSchaeden_Type" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BonusMalusSystem_Type", propOrder = {
    "bonusMalusVorversicherung",
    "versicherungsgesellschaft",
    "polizzennummer",
    "offeneSchaeden"
})
public class BonusMalusSystemType {

    @XmlElement(name = "BonusMalusVorversicherung")
    protected boolean bonusMalusVorversicherung;
    @XmlElement(name = "Versicherungsgesellschaft")
    protected String versicherungsgesellschaft;
    @XmlElement(name = "Polizzennummer")
    protected String polizzennummer;
    @XmlElement(name = "OffeneSchaeden")
    protected OffeneSchaedenType offeneSchaeden;

    /**
     * Ruft den Wert der bonusMalusVorversicherung-Eigenschaft ab.
     * 
     */
    public boolean isBonusMalusVorversicherung() {
        return bonusMalusVorversicherung;
    }

    /**
     * Legt den Wert der bonusMalusVorversicherung-Eigenschaft fest.
     * 
     */
    public void setBonusMalusVorversicherung(boolean value) {
        this.bonusMalusVorversicherung = value;
    }

    /**
     * Ruft den Wert der versicherungsgesellschaft-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersicherungsgesellschaft() {
        return versicherungsgesellschaft;
    }

    /**
     * Legt den Wert der versicherungsgesellschaft-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersicherungsgesellschaft(String value) {
        this.versicherungsgesellschaft = value;
    }

    /**
     * Ruft den Wert der polizzennummer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolizzennummer() {
        return polizzennummer;
    }

    /**
     * Legt den Wert der polizzennummer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolizzennummer(String value) {
        this.polizzennummer = value;
    }

    /**
     * Ruft den Wert der offeneSchaeden-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OffeneSchaedenType }
     *     
     */
    public OffeneSchaedenType getOffeneSchaeden() {
        return offeneSchaeden;
    }

    /**
     * Legt den Wert der offeneSchaeden-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OffeneSchaedenType }
     *     
     */
    public void setOffeneSchaeden(OffeneSchaedenType value) {
        this.offeneSchaeden = value;
    }

}
