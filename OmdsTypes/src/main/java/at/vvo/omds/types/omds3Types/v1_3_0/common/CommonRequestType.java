
package at.vvo.omds.types.omds3Types.v1_3_0.common;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.v1_3_0.on2antrag.kfz.CalculateKfzRequestType;
import at.vvo.omds.types.omds3Types.v1_3_0.on2antrag.kfz.CreateApplicationKfzRequestType;
import at.vvo.omds.types.omds3Types.v1_3_0.on2antrag.kfz.CreateOfferKfzRequestType;
import at.vvo.omds.types.omds3Types.v1_3_0.on2antrag.kfz.SubmitApplicationKfzRequestType;
import at.vvo.omds.types.omds3Types.v1_3_0.on2antrag.sachPrivat.CalculateBesitzRequestType;
import at.vvo.omds.types.omds3Types.v1_3_0.on3vertrag.CollectionChangeRequestType;
import at.vvo.omds.types.omds3Types.v1_3_0.on3vertrag.GetPoliciesOfPartnerRequestType;
import at.vvo.omds.types.omds3Types.v1_3_0.on3vertrag.SetMailingAddressRequestType;
import at.vvo.omds.types.omds3Types.v1_3_0.on4partner.ChangeCommunicationObjectRequestType;
import at.vvo.omds.types.omds3Types.v1_3_0.on4partner.ChangePartnerMainAddressRequestType;
import at.vvo.omds.types.omds3Types.v1_3_0.on4partner.ChangePersonDataRequestType;
import at.vvo.omds.types.omds3Types.v1_3_0.on4partner.CheckAddressRequestType;
import at.vvo.omds.types.omds3Types.v1_3_0.on4partner.CreateNewCommunicationObjectRequestType;
import at.vvo.omds.types.omds3Types.v1_3_0.on4partner.DeleteCommunicationRequestType;
import at.vvo.omds.types.omds3Types.v1_3_0.on4partner.GetPartnerRequestType;


/**
 * Abstraktes RequestObjekt
 * 
 * <p>Java-Klasse für CommonRequest_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CommonRequest_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="VUNr" type="{urn:omds20}VUNr"/&gt;
 *         &lt;element name="ClientId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TechnischeParameter" type="{urn:omds3CommonServiceTypes-1-1-0}TechnicalKeyValue_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="KorrelationsId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommonRequest_Type", propOrder = {
    "vuNr",
    "clientId",
    "technischeParameter",
    "korrelationsId"
})
@XmlSeeAlso({
    CalculateBesitzRequestType.class,
    CalculateKfzRequestType.class,
    CreateOfferKfzRequestType.class,
    CreateApplicationKfzRequestType.class,
    SubmitApplicationKfzRequestType.class,
    GetPartnerRequestType.class,
    CheckAddressRequestType.class,
    ChangePartnerMainAddressRequestType.class,
    ChangePersonDataRequestType.class,
    ChangeCommunicationObjectRequestType.class,
    DeleteCommunicationRequestType.class,
    CreateNewCommunicationObjectRequestType.class,
    CommonSearchRequestType.class,
    GetPoliciesOfPartnerRequestType.class,
    SetMailingAddressRequestType.class,
    CollectionChangeRequestType.class
})
public abstract class CommonRequestType {

    @XmlElement(name = "VUNr", required = true)
    protected String vuNr;
    @XmlElement(name = "ClientId")
    protected String clientId;
    @XmlElement(name = "TechnischeParameter")
    protected List<TechnicalKeyValueType> technischeParameter;
    @XmlElement(name = "KorrelationsId", required = true)
    protected String korrelationsId;

    /**
     * Ruft den Wert der vuNr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVUNr() {
        return vuNr;
    }

    /**
     * Legt den Wert der vuNr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVUNr(String value) {
        this.vuNr = value;
    }

    /**
     * Ruft den Wert der clientId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClientId() {
        return clientId;
    }

    /**
     * Legt den Wert der clientId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClientId(String value) {
        this.clientId = value;
    }

    /**
     * Gets the value of the technischeParameter property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the technischeParameter property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTechnischeParameter().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TechnicalKeyValueType }
     * 
     * 
     */
    public List<TechnicalKeyValueType> getTechnischeParameter() {
        if (technischeParameter == null) {
            technischeParameter = new ArrayList<TechnicalKeyValueType>();
        }
        return this.technischeParameter;
    }

    /**
     * Ruft den Wert der korrelationsId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKorrelationsId() {
        return korrelationsId;
    }

    /**
     * Legt den Wert der korrelationsId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKorrelationsId(String value) {
        this.korrelationsId = value;
    }

}
