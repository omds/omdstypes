
package at.vvo.omds.types.omds3Types.v1_3_0.common;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.v1_3_0.DeleteResponseType;
import at.vvo.omds.types.omds3Types.v1_3_0.on2antrag.common.SubmitApplicationResponseType;
import at.vvo.omds.types.omds3Types.v1_3_0.on2antrag.kfz.CalculateKfzResponseType;
import at.vvo.omds.types.omds3Types.v1_3_0.on2antrag.kfz.CreateApplicationKfzResponseType;
import at.vvo.omds.types.omds3Types.v1_3_0.on2antrag.kfz.CreateOfferKfzResponseType;
import at.vvo.omds.types.omds3Types.v1_3_0.on2antrag.sachPrivat.CalculateBesitzResponseType;
import at.vvo.omds.types.omds3Types.v1_3_0.on3vertrag.CollectionChangeResponseType;
import at.vvo.omds.types.omds3Types.v1_3_0.on3vertrag.GetPoliciesOfPartnerResponseType;
import at.vvo.omds.types.omds3Types.v1_3_0.on3vertrag.SetMailingAddressResponseType;
import at.vvo.omds.types.omds3Types.v1_3_0.on4partner.ChangeCommunicationObjectResponseType;
import at.vvo.omds.types.omds3Types.v1_3_0.on4partner.ChangePartnerMainAddressResponseType;
import at.vvo.omds.types.omds3Types.v1_3_0.on4partner.ChangePersonDataResponseType;
import at.vvo.omds.types.omds3Types.v1_3_0.on4partner.CheckAddressResponseType;
import at.vvo.omds.types.omds3Types.v1_3_0.on4partner.CreateNewCommunicationObjectResponseType;
import at.vvo.omds.types.omds3Types.v1_3_0.on4partner.DeleteCommunicationObjectResponseType;
import at.vvo.omds.types.omds3Types.v1_3_0.on4partner.GetPartnerResponseType;


/**
 * Abstraktes ResponseObjekt
 * 
 * <p>Java-Klasse für CommonResponse_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CommonResponse_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Status"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="KorrelationsId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="Ergebnis" type="{urn:omds3CommonServiceTypes-1-1-0}Status_Type"/&gt;
 *                   &lt;element name="Meldungen" type="{urn:omds3CommonServiceTypes-1-1-0}ServiceFault" maxOccurs="unbounded" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommonResponse_Type", propOrder = {
    "status"
})
@XmlSeeAlso({
    CalculateBesitzResponseType.class,
    CalculateKfzResponseType.class,
    CreateOfferKfzResponseType.class,
    CreateApplicationKfzResponseType.class,
    GetPartnerResponseType.class,
    CheckAddressResponseType.class,
    ChangeCommunicationObjectResponseType.class,
    DeleteCommunicationObjectResponseType.class,
    CreateNewCommunicationObjectResponseType.class,
    SubmitApplicationResponseType.class,
    CommonSearchResponseType.class,
    GetPoliciesOfPartnerResponseType.class,
    SetMailingAddressResponseType.class,
    CollectionChangeResponseType.class,
    DeleteResponseType.class,
    ChangePersonDataResponseType.class,
    ChangePartnerMainAddressResponseType.class
})
public abstract class CommonResponseType {

    @XmlElement(name = "Status", required = true)
    protected CommonResponseType.Status status;

    /**
     * Ruft den Wert der status-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CommonResponseType.Status }
     *     
     */
    public CommonResponseType.Status getStatus() {
        return status;
    }

    /**
     * Legt den Wert der status-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CommonResponseType.Status }
     *     
     */
    public void setStatus(CommonResponseType.Status value) {
        this.status = value;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="KorrelationsId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="Ergebnis" type="{urn:omds3CommonServiceTypes-1-1-0}Status_Type"/&gt;
     *         &lt;element name="Meldungen" type="{urn:omds3CommonServiceTypes-1-1-0}ServiceFault" maxOccurs="unbounded" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "korrelationsId",
        "ergebnis",
        "meldungen"
    })
    public static class Status {

        @XmlElement(name = "KorrelationsId", required = true)
        protected String korrelationsId;
        @XmlElement(name = "Ergebnis", required = true)
        protected String ergebnis;
        @XmlElement(name = "Meldungen")
        protected List<ServiceFault> meldungen;

        /**
         * Ruft den Wert der korrelationsId-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getKorrelationsId() {
            return korrelationsId;
        }

        /**
         * Legt den Wert der korrelationsId-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setKorrelationsId(String value) {
            this.korrelationsId = value;
        }

        /**
         * Ruft den Wert der ergebnis-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getErgebnis() {
            return ergebnis;
        }

        /**
         * Legt den Wert der ergebnis-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setErgebnis(String value) {
            this.ergebnis = value;
        }

        /**
         * Gets the value of the meldungen property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the meldungen property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getMeldungen().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ServiceFault }
         * 
         * 
         */
        public List<ServiceFault> getMeldungen() {
            if (meldungen == null) {
                meldungen = new ArrayList<ServiceFault>();
            }
            return this.meldungen;
        }

    }

}
