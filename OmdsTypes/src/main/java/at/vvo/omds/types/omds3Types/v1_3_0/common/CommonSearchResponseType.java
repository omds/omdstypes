
package at.vvo.omds.types.omds3Types.v1_3_0.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.v1_3_0.GetStatusChangesResponseType;


/**
 * Abstrakter Typ für Ergebnisse von Suchen
 * 
 * <p>Java-Klasse für CommonSearchResponse_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CommonSearchResponse_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}CommonResponse_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ActualOffset" type="{http://www.w3.org/2001/XMLSchema}unsignedInt"/&gt;
 *         &lt;element name="ActualMaxResults" type="{http://www.w3.org/2001/XMLSchema}unsignedInt"/&gt;
 *         &lt;element name="TotalResults" type="{http://www.w3.org/2001/XMLSchema}unsignedInt"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommonSearchResponse_Type", propOrder = {
    "actualOffset",
    "actualMaxResults",
    "totalResults"
})
@XmlSeeAlso({
    GetStatusChangesResponseType.class
})
public abstract class CommonSearchResponseType
    extends CommonResponseType
{

    @XmlElement(name = "ActualOffset")
    @XmlSchemaType(name = "unsignedInt")
    protected long actualOffset;
    @XmlElement(name = "ActualMaxResults")
    @XmlSchemaType(name = "unsignedInt")
    protected long actualMaxResults;
    @XmlElement(name = "TotalResults")
    @XmlSchemaType(name = "unsignedInt")
    protected long totalResults;

    /**
     * Ruft den Wert der actualOffset-Eigenschaft ab.
     * 
     */
    public long getActualOffset() {
        return actualOffset;
    }

    /**
     * Legt den Wert der actualOffset-Eigenschaft fest.
     * 
     */
    public void setActualOffset(long value) {
        this.actualOffset = value;
    }

    /**
     * Ruft den Wert der actualMaxResults-Eigenschaft ab.
     * 
     */
    public long getActualMaxResults() {
        return actualMaxResults;
    }

    /**
     * Legt den Wert der actualMaxResults-Eigenschaft fest.
     * 
     */
    public void setActualMaxResults(long value) {
        this.actualMaxResults = value;
    }

    /**
     * Ruft den Wert der totalResults-Eigenschaft ab.
     * 
     */
    public long getTotalResults() {
        return totalResults;
    }

    /**
     * Legt den Wert der totalResults-Eigenschaft fest.
     * 
     */
    public void setTotalResults(long value) {
        this.totalResults = value;
    }

}
