
package at.vvo.omds.types.omds3Types.v1_3_0.common;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Dateianhang welcher an einen Geschäftsfall hinzugefügt werden kann
 *             
 * 
 * <p>Java-Klasse für Dateianhang_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Dateianhang_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Id" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="DateiMimeType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="DateiType" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}int"&gt;
 *               &lt;enumeration value="1"/&gt;
 *               &lt;enumeration value="2"/&gt;
 *               &lt;enumeration value="3"/&gt;
 *               &lt;enumeration value="4"/&gt;
 *               &lt;enumeration value="5"/&gt;
 *               &lt;enumeration value="6"/&gt;
 *               &lt;enumeration value="7"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="DateiName"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="100"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="DateiData" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/&gt;
 *         &lt;element name="DateiBeschreibung" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="200"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Dateianhang_Type", propOrder = {
    "id",
    "dateiMimeType",
    "dateiType",
    "dateiName",
    "dateiData",
    "dateiBeschreibung"
})
public class DateianhangType {

    @XmlElement(name = "Id", required = true)
    protected BigInteger id;
    @XmlElement(name = "DateiMimeType", required = true)
    protected String dateiMimeType;
    @XmlElement(name = "DateiType")
    protected Integer dateiType;
    @XmlElement(name = "DateiName", required = true)
    protected String dateiName;
    @XmlElement(name = "DateiData", required = true)
    protected byte[] dateiData;
    @XmlElement(name = "DateiBeschreibung")
    protected String dateiBeschreibung;

    /**
     * Ruft den Wert der id-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getId() {
        return id;
    }

    /**
     * Legt den Wert der id-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setId(BigInteger value) {
        this.id = value;
    }

    /**
     * Ruft den Wert der dateiMimeType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDateiMimeType() {
        return dateiMimeType;
    }

    /**
     * Legt den Wert der dateiMimeType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDateiMimeType(String value) {
        this.dateiMimeType = value;
    }

    /**
     * Ruft den Wert der dateiType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDateiType() {
        return dateiType;
    }

    /**
     * Legt den Wert der dateiType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDateiType(Integer value) {
        this.dateiType = value;
    }

    /**
     * Ruft den Wert der dateiName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDateiName() {
        return dateiName;
    }

    /**
     * Legt den Wert der dateiName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDateiName(String value) {
        this.dateiName = value;
    }

    /**
     * Ruft den Wert der dateiData-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getDateiData() {
        return dateiData;
    }

    /**
     * Legt den Wert der dateiData-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setDateiData(byte[] value) {
        this.dateiData = value;
    }

    /**
     * Ruft den Wert der dateiBeschreibung-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDateiBeschreibung() {
        return dateiBeschreibung;
    }

    /**
     * Legt den Wert der dateiBeschreibung-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDateiBeschreibung(String value) {
        this.dateiBeschreibung = value;
    }

}
