
package at.vvo.omds.types.omds3Types.v1_3_0.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für DeckungProzent_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="DeckungProzent_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="deckungActive" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="deckungProzent" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DeckungProzent_Type", propOrder = {
    "deckungActive",
    "deckungProzent"
})
public class DeckungProzentType {

    protected boolean deckungActive;
    protected int deckungProzent;

    /**
     * Ruft den Wert der deckungActive-Eigenschaft ab.
     * 
     */
    public boolean isDeckungActive() {
        return deckungActive;
    }

    /**
     * Legt den Wert der deckungActive-Eigenschaft fest.
     * 
     */
    public void setDeckungActive(boolean value) {
        this.deckungActive = value;
    }

    /**
     * Ruft den Wert der deckungProzent-Eigenschaft ab.
     * 
     */
    public int getDeckungProzent() {
        return deckungProzent;
    }

    /**
     * Legt den Wert der deckungProzent-Eigenschaft fest.
     * 
     */
    public void setDeckungProzent(int value) {
        this.deckungProzent = value;
    }

}
