
package at.vvo.omds.types.omds3Types.v1_3_0.common;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.v1_3_0.on2antrag.kfz.AssistanceKfzType;
import at.vvo.omds.types.omds3Types.v1_3_0.on2antrag.kfz.HaftpflichtKfzType;
import at.vvo.omds.types.omds3Types.v1_3_0.on2antrag.kfz.InsassenUnfallKfzType;
import at.vvo.omds.types.omds3Types.v1_3_0.on2antrag.kfz.KaskoKfzType;
import at.vvo.omds.types.omds3Types.v1_3_0.on2antrag.kfz.LenkerUnfallKfzType;
import at.vvo.omds.types.omds3Types.v1_3_0.on2antrag.kfz.VerkehrsrechtsschutzKfzType;


/**
 * Typ für ein Elementarprodukt, welches einem Risiko entspricht
 * 
 * <p>Java-Klasse für Elementarprodukt_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Elementarprodukt_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}Produktbaustein_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Bedingungen" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ZusaetzlicheElementarproduktdaten" type="{urn:omds3CommonServiceTypes-1-1-0}ZusaetzlicheElementarproduktdaten_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Elementarprodukt_Type", propOrder = {
    "bedingungen",
    "zusaetzlicheElementarproduktdaten"
})
@XmlSeeAlso({
    HaftpflichtKfzType.class,
    KaskoKfzType.class,
    InsassenUnfallKfzType.class,
    LenkerUnfallKfzType.class,
    AssistanceKfzType.class,
    VerkehrsrechtsschutzKfzType.class
})
public abstract class ElementarproduktType
    extends ProduktbausteinType
{

    @XmlElement(name = "Bedingungen")
    protected List<String> bedingungen;
    @XmlElement(name = "ZusaetzlicheElementarproduktdaten")
    protected List<ZusaetzlicheElementarproduktdatenType> zusaetzlicheElementarproduktdaten;

    /**
     * Gets the value of the bedingungen property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the bedingungen property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBedingungen().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getBedingungen() {
        if (bedingungen == null) {
            bedingungen = new ArrayList<String>();
        }
        return this.bedingungen;
    }

    /**
     * Gets the value of the zusaetzlicheElementarproduktdaten property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the zusaetzlicheElementarproduktdaten property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getZusaetzlicheElementarproduktdaten().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ZusaetzlicheElementarproduktdatenType }
     * 
     * 
     */
    public List<ZusaetzlicheElementarproduktdatenType> getZusaetzlicheElementarproduktdaten() {
        if (zusaetzlicheElementarproduktdaten == null) {
            zusaetzlicheElementarproduktdaten = new ArrayList<ZusaetzlicheElementarproduktdatenType>();
        }
        return this.zusaetzlicheElementarproduktdaten;
    }

}
