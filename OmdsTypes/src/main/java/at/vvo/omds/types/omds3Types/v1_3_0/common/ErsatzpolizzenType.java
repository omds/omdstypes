
package at.vvo.omds.types.omds3Types.v1_3_0.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Typ für Ersatzpolizzen
 * 
 * <p>Java-Klasse für Ersatzpolizzen_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Ersatzpolizzen_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Ersatzpolizzennummer1" type="{urn:omds20}Polizzennr"/&gt;
 *         &lt;element name="Ersatzpolizzennummer2" type="{urn:omds20}Polizzennr" minOccurs="0"/&gt;
 *         &lt;element name="Ersatzpolizzennummer3" type="{urn:omds20}Polizzennr" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Ersatzpolizzen_Type", propOrder = {
    "ersatzpolizzennummer1",
    "ersatzpolizzennummer2",
    "ersatzpolizzennummer3"
})
public class ErsatzpolizzenType {

    @XmlElement(name = "Ersatzpolizzennummer1", required = true)
    protected String ersatzpolizzennummer1;
    @XmlElement(name = "Ersatzpolizzennummer2")
    protected String ersatzpolizzennummer2;
    @XmlElement(name = "Ersatzpolizzennummer3")
    protected String ersatzpolizzennummer3;

    /**
     * Ruft den Wert der ersatzpolizzennummer1-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErsatzpolizzennummer1() {
        return ersatzpolizzennummer1;
    }

    /**
     * Legt den Wert der ersatzpolizzennummer1-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErsatzpolizzennummer1(String value) {
        this.ersatzpolizzennummer1 = value;
    }

    /**
     * Ruft den Wert der ersatzpolizzennummer2-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErsatzpolizzennummer2() {
        return ersatzpolizzennummer2;
    }

    /**
     * Legt den Wert der ersatzpolizzennummer2-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErsatzpolizzennummer2(String value) {
        this.ersatzpolizzennummer2 = value;
    }

    /**
     * Ruft den Wert der ersatzpolizzennummer3-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErsatzpolizzennummer3() {
        return ersatzpolizzennummer3;
    }

    /**
     * Legt den Wert der ersatzpolizzennummer3-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErsatzpolizzennummer3(String value) {
        this.ersatzpolizzennummer3 = value;
    }

}
