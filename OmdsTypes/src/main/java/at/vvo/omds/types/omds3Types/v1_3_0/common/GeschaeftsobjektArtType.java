
package at.vvo.omds.types.omds3Types.v1_3_0.common;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für GeschaeftsobjektArt_Type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="GeschaeftsobjektArt_Type"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="VTG"/&gt;
 *     &lt;enumeration value="SC"/&gt;
 *     &lt;enumeration value="AN"/&gt;
 *     &lt;enumeration value="IB"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "GeschaeftsobjektArt_Type")
@XmlEnum
public enum GeschaeftsobjektArtType {


    /**
     * Vertrag
     * 
     */
    VTG,

    /**
     * Schaden
     * 
     */
    SC,

    /**
     * Antrag
     * 
     */
    AN,

    /**
     * Interventionsbericht
     * 
     */
    IB;

    public String value() {
        return name();
    }

    public static GeschaeftsobjektArtType fromValue(String v) {
        return valueOf(v);
    }

}
