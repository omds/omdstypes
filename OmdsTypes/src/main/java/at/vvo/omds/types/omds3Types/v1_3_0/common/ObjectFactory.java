
package at.vvo.omds.types.omds3Types.v1_3_0.common;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the at.vvo.omds.types.omds3Types.v1_3_0.common package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ServiceFault_QNAME = new QName("urn:omds3CommonServiceTypes-1-1-0", "serviceFault");
    private final static QName _GeschaeftsfallId_QNAME = new QName("urn:omds3CommonServiceTypes-1-1-0", "GeschaeftsfallId");
    private final static QName _OrdnungsbegriffZuordFremd_QNAME = new QName("urn:omds3CommonServiceTypes-1-1-0", "OrdnungsbegriffZuordFremd");
    private final static QName _ObjektId_QNAME = new QName("urn:omds3CommonServiceTypes-1-1-0", "ObjektId");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: at.vvo.omds.types.omds3Types.v1_3_0.common
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link at.vvo.omds.types.omds3Types.v1_3_0.common.CommonResponseType.Status }
     * 
     */
    public at.vvo.omds.types.omds3Types.v1_3_0.common.CommonResponseType.Status createCommonResponseTypeStatus() {
        return new at.vvo.omds.types.omds3Types.v1_3_0.common.CommonResponseType.Status();
    }

    /**
     * Create an instance of {@link ServiceFault }
     * 
     */
    public ServiceFault createServiceFault() {
        return new ServiceFault();
    }

    /**
     * Create an instance of {@link ObjektIdType }
     * 
     */
    public ObjektIdType createObjektIdType() {
        return new ObjektIdType();
    }

    /**
     * Create an instance of {@link AgentFilterType }
     * 
     */
    public AgentFilterType createAgentFilterType() {
        return new AgentFilterType();
    }

    /**
     * Create an instance of {@link ElementIdType }
     * 
     */
    public ElementIdType createElementIdType() {
        return new ElementIdType();
    }

    /**
     * Create an instance of {@link DateianhangType }
     * 
     */
    public DateianhangType createDateianhangType() {
        return new DateianhangType();
    }

    /**
     * Create an instance of {@link KontierungType }
     * 
     */
    public KontierungType createKontierungType() {
        return new KontierungType();
    }

    /**
     * Create an instance of {@link PraemieType }
     * 
     */
    public PraemieType createPraemieType() {
        return new PraemieType();
    }

    /**
     * Create an instance of {@link VersicherungssteuerType }
     * 
     */
    public VersicherungssteuerType createVersicherungssteuerType() {
        return new VersicherungssteuerType();
    }

    /**
     * Create an instance of {@link BankverbindungType }
     * 
     */
    public BankverbindungType createBankverbindungType() {
        return new BankverbindungType();
    }

    /**
     * Create an instance of {@link ZahlungsdatenType }
     * 
     */
    public ZahlungsdatenType createZahlungsdatenType() {
        return new ZahlungsdatenType();
    }

    /**
     * Create an instance of {@link TechnicalKeyValueType }
     * 
     */
    public TechnicalKeyValueType createTechnicalKeyValueType() {
        return new TechnicalKeyValueType();
    }

    /**
     * Create an instance of {@link VertragspersonType }
     * 
     */
    public VertragspersonType createVertragspersonType() {
        return new VertragspersonType();
    }

    /**
     * Create an instance of {@link VinkulierungType }
     * 
     */
    public VinkulierungType createVinkulierungType() {
        return new VinkulierungType();
    }

    /**
     * Create an instance of {@link BezugsrechtType }
     * 
     */
    public BezugsrechtType createBezugsrechtType() {
        return new BezugsrechtType();
    }

    /**
     * Create an instance of {@link VinkularglaeubigerType }
     * 
     */
    public VinkularglaeubigerType createVinkularglaeubigerType() {
        return new VinkularglaeubigerType();
    }

    /**
     * Create an instance of {@link BonusMalusSystemType }
     * 
     */
    public BonusMalusSystemType createBonusMalusSystemType() {
        return new BonusMalusSystemType();
    }

    /**
     * Create an instance of {@link OffeneSchaedenType }
     * 
     */
    public OffeneSchaedenType createOffeneSchaedenType() {
        return new OffeneSchaedenType();
    }

    /**
     * Create an instance of {@link OffenerSchadenType }
     * 
     */
    public OffenerSchadenType createOffenerSchadenType() {
        return new OffenerSchadenType();
    }

    /**
     * Create an instance of {@link VorversicherungenType }
     * 
     */
    public VorversicherungenType createVorversicherungenType() {
        return new VorversicherungenType();
    }

    /**
     * Create an instance of {@link VorversicherungenDetailType }
     * 
     */
    public VorversicherungenDetailType createVorversicherungenDetailType() {
        return new VorversicherungenDetailType();
    }

    /**
     * Create an instance of {@link DatenverwendungType }
     * 
     */
    public DatenverwendungType createDatenverwendungType() {
        return new DatenverwendungType();
    }

    /**
     * Create an instance of {@link ErsatzpolizzenType }
     * 
     */
    public ErsatzpolizzenType createErsatzpolizzenType() {
        return new ErsatzpolizzenType();
    }

    /**
     * Create an instance of {@link DeckungVsType }
     * 
     */
    public DeckungVsType createDeckungVsType() {
        return new DeckungVsType();
    }

    /**
     * Create an instance of {@link DeckungProzentType }
     * 
     */
    public DeckungProzentType createDeckungProzentType() {
        return new DeckungProzentType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ServiceFault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds3CommonServiceTypes-1-1-0", name = "serviceFault")
    public JAXBElement<ServiceFault> createServiceFault(ServiceFault value) {
        return new JAXBElement<ServiceFault>(_ServiceFault_QNAME, ServiceFault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds3CommonServiceTypes-1-1-0", name = "GeschaeftsfallId")
    public JAXBElement<String> createGeschaeftsfallId(String value) {
        return new JAXBElement<String>(_GeschaeftsfallId_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds3CommonServiceTypes-1-1-0", name = "OrdnungsbegriffZuordFremd")
    public JAXBElement<String> createOrdnungsbegriffZuordFremd(String value) {
        return new JAXBElement<String>(_OrdnungsbegriffZuordFremd_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObjektIdType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:omds3CommonServiceTypes-1-1-0", name = "ObjektId")
    public JAXBElement<ObjektIdType> createObjektId(ObjektIdType value) {
        return new JAXBElement<ObjektIdType>(_ObjektId_QNAME, ObjektIdType.class, null, value);
    }

}
