
package at.vvo.omds.types.omds3Types.v1_3_0.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für OffeneSchaeden_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="OffeneSchaeden_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="OffenerSchaden1" type="{urn:omds3CommonServiceTypes-1-1-0}OffenerSchaden_Type"/&gt;
 *         &lt;element name="OffenerSchaden2" type="{urn:omds3CommonServiceTypes-1-1-0}OffenerSchaden_Type" minOccurs="0"/&gt;
 *         &lt;element name="OffenerSchaden3" type="{urn:omds3CommonServiceTypes-1-1-0}OffenerSchaden_Type" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OffeneSchaeden_Type", propOrder = {
    "offenerSchaden1",
    "offenerSchaden2",
    "offenerSchaden3"
})
public class OffeneSchaedenType {

    @XmlElement(name = "OffenerSchaden1", required = true)
    protected OffenerSchadenType offenerSchaden1;
    @XmlElement(name = "OffenerSchaden2")
    protected OffenerSchadenType offenerSchaden2;
    @XmlElement(name = "OffenerSchaden3")
    protected OffenerSchadenType offenerSchaden3;

    /**
     * Ruft den Wert der offenerSchaden1-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OffenerSchadenType }
     *     
     */
    public OffenerSchadenType getOffenerSchaden1() {
        return offenerSchaden1;
    }

    /**
     * Legt den Wert der offenerSchaden1-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OffenerSchadenType }
     *     
     */
    public void setOffenerSchaden1(OffenerSchadenType value) {
        this.offenerSchaden1 = value;
    }

    /**
     * Ruft den Wert der offenerSchaden2-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OffenerSchadenType }
     *     
     */
    public OffenerSchadenType getOffenerSchaden2() {
        return offenerSchaden2;
    }

    /**
     * Legt den Wert der offenerSchaden2-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OffenerSchadenType }
     *     
     */
    public void setOffenerSchaden2(OffenerSchadenType value) {
        this.offenerSchaden2 = value;
    }

    /**
     * Ruft den Wert der offenerSchaden3-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OffenerSchadenType }
     *     
     */
    public OffenerSchadenType getOffenerSchaden3() {
        return offenerSchaden3;
    }

    /**
     * Legt den Wert der offenerSchaden3-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OffenerSchadenType }
     *     
     */
    public void setOffenerSchaden3(OffenerSchadenType value) {
        this.offenerSchaden3 = value;
    }

}
