
package at.vvo.omds.types.omds3Types.v1_3_0.common;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.v1_3_0.on2antrag.kfz.ProduktKfzType;
import at.vvo.omds.types.omds3Types.v1_3_0.on2antrag.kfz.ZusatzproduktKfzType;
import at.vvo.omds.types.omds3Types.v1_3_0.on2antrag.sachPrivat.ProduktBesitzType;


/**
 * Typ für ein Produkt, welches einer Vertragssparte entspricht
 * 
 * <p>Java-Klasse für Produkt_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Produkt_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}Produktbaustein_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Produktgeneration" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Zahlweg" type="{urn:omds20}ZahlWegCd_Type" minOccurs="0"/&gt;
 *         &lt;element name="Zahlrhythmus" type="{urn:omds20}ZahlRhythmCd_Type"/&gt;
 *         &lt;element name="ZusaetzlicheProduktdaten" type="{urn:omds3CommonServiceTypes-1-1-0}ZusaetzlicheProduktdaten_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Produkt_Type", propOrder = {
    "produktgeneration",
    "zahlweg",
    "zahlrhythmus",
    "zusaetzlicheProduktdaten"
})
@XmlSeeAlso({
    ProduktBesitzType.class,
    ProduktKfzType.class,
    ZusatzproduktKfzType.class
})
public abstract class ProduktType
    extends ProduktbausteinType
{

    @XmlElement(name = "Produktgeneration", required = true)
    protected String produktgeneration;
    @XmlElement(name = "Zahlweg")
    protected String zahlweg;
    @XmlElement(name = "Zahlrhythmus", required = true)
    protected String zahlrhythmus;
    @XmlElement(name = "ZusaetzlicheProduktdaten")
    protected List<ZusaetzlicheProduktdatenType> zusaetzlicheProduktdaten;

    /**
     * Ruft den Wert der produktgeneration-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProduktgeneration() {
        return produktgeneration;
    }

    /**
     * Legt den Wert der produktgeneration-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProduktgeneration(String value) {
        this.produktgeneration = value;
    }

    /**
     * Ruft den Wert der zahlweg-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZahlweg() {
        return zahlweg;
    }

    /**
     * Legt den Wert der zahlweg-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZahlweg(String value) {
        this.zahlweg = value;
    }

    /**
     * Ruft den Wert der zahlrhythmus-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZahlrhythmus() {
        return zahlrhythmus;
    }

    /**
     * Legt den Wert der zahlrhythmus-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZahlrhythmus(String value) {
        this.zahlrhythmus = value;
    }

    /**
     * Gets the value of the zusaetzlicheProduktdaten property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the zusaetzlicheProduktdaten property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getZusaetzlicheProduktdaten().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ZusaetzlicheProduktdatenType }
     * 
     * 
     */
    public List<ZusaetzlicheProduktdatenType> getZusaetzlicheProduktdaten() {
        if (zusaetzlicheProduktdaten == null) {
            zusaetzlicheProduktdaten = new ArrayList<ZusaetzlicheProduktdatenType>();
        }
        return this.zusaetzlicheProduktdaten;
    }

}
