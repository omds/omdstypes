
package at.vvo.omds.types.omds3Types.v1_3_0.common;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Abstrakter Typ für alle Gemeinsamkeiten von Produktbausteinen
 * 
 * <p>Java-Klasse für Produktbaustein_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Produktbaustein_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Versicherungsbeginn" type="{urn:omds20}Datum-Zeit"/&gt;
 *         &lt;element name="Versicherungsablauf" type="{urn:omds20}Datum-Zeit" minOccurs="0"/&gt;
 *         &lt;element name="Id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Bezeichnung" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Praemie" type="{urn:omds3CommonServiceTypes-1-1-0}Praemie_Type" minOccurs="0"/&gt;
 *         &lt;element name="Meldungen" type="{urn:omds3CommonServiceTypes-1-1-0}ServiceFault" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Produktbaustein_Type", propOrder = {
    "versicherungsbeginn",
    "versicherungsablauf",
    "id",
    "bezeichnung",
    "praemie",
    "meldungen"
})
@XmlSeeAlso({
    VerkaufsproduktType.class,
    ProduktType.class,
    ElementarproduktType.class
})
public abstract class ProduktbausteinType {

    @XmlElement(name = "Versicherungsbeginn", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar versicherungsbeginn;
    @XmlElement(name = "Versicherungsablauf")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar versicherungsablauf;
    @XmlElement(name = "Id")
    protected String id;
    @XmlElement(name = "Bezeichnung")
    protected String bezeichnung;
    @XmlElement(name = "Praemie")
    protected PraemieType praemie;
    @XmlElement(name = "Meldungen")
    protected List<ServiceFault> meldungen;

    /**
     * Ruft den Wert der versicherungsbeginn-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getVersicherungsbeginn() {
        return versicherungsbeginn;
    }

    /**
     * Legt den Wert der versicherungsbeginn-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setVersicherungsbeginn(XMLGregorianCalendar value) {
        this.versicherungsbeginn = value;
    }

    /**
     * Ruft den Wert der versicherungsablauf-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getVersicherungsablauf() {
        return versicherungsablauf;
    }

    /**
     * Legt den Wert der versicherungsablauf-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setVersicherungsablauf(XMLGregorianCalendar value) {
        this.versicherungsablauf = value;
    }

    /**
     * Ruft den Wert der id-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Legt den Wert der id-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Ruft den Wert der bezeichnung-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBezeichnung() {
        return bezeichnung;
    }

    /**
     * Legt den Wert der bezeichnung-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBezeichnung(String value) {
        this.bezeichnung = value;
    }

    /**
     * Ruft den Wert der praemie-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PraemieType }
     *     
     */
    public PraemieType getPraemie() {
        return praemie;
    }

    /**
     * Legt den Wert der praemie-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PraemieType }
     *     
     */
    public void setPraemie(PraemieType value) {
        this.praemie = value;
    }

    /**
     * Gets the value of the meldungen property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the meldungen property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMeldungen().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ServiceFault }
     * 
     * 
     */
    public List<ServiceFault> getMeldungen() {
        if (meldungen == null) {
            meldungen = new ArrayList<ServiceFault>();
        }
        return this.meldungen;
    }

}
