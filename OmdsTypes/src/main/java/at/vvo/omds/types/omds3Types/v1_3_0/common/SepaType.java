
package at.vvo.omds.types.omds3Types.v1_3_0.common;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für Sepa_Type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="Sepa_Type"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Sepa nicht vorhanden"/&gt;
 *     &lt;enumeration value="Sepa überschreiben"/&gt;
 *     &lt;enumeration value="Sepa bereits vorhanden"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "Sepa_Type")
@XmlEnum
public enum SepaType {

    @XmlEnumValue("Sepa nicht vorhanden")
    SEPA_NICHT_VORHANDEN("Sepa nicht vorhanden"),
    @XmlEnumValue("Sepa \u00fcberschreiben")
    SEPA_ÜBERSCHREIBEN("Sepa \u00fcberschreiben"),
    @XmlEnumValue("Sepa bereits vorhanden")
    SEPA_BEREITS_VORHANDEN("Sepa bereits vorhanden");
    private final String value;

    SepaType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SepaType fromValue(String v) {
        for (SepaType c: SepaType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
