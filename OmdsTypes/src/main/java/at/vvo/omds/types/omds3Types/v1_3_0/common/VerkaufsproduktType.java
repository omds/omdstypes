
package at.vvo.omds.types.omds3Types.v1_3_0.common;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.v1_3_0.on2antrag.kfz.VerkaufsproduktKfzType;
import at.vvo.omds.types.omds3Types.v1_3_0.on2antrag.sachPrivat.VerkaufsproduktBesitzType;


/**
 * Typ für ein Produktbündel, welches einem Vertrag entspricht
 * 
 * <p>Java-Klasse für Verkaufsprodukt_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Verkaufsprodukt_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}Produktbaustein_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Verkaufsproduktgeneration" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Berechnungsvariante" type="{urn:omds3CommonServiceTypes-1-1-0}Berechnungsvariante_Type" minOccurs="0"/&gt;
 *         &lt;element name="ZusaetzlicheVerkaufproduktdaten" type="{urn:omds3CommonServiceTypes-1-1-0}ZusaetzlicheVerkaufproduktdaten_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Verkaufsprodukt_Type", propOrder = {
    "verkaufsproduktgeneration",
    "berechnungsvariante",
    "zusaetzlicheVerkaufproduktdaten"
})
@XmlSeeAlso({
    VerkaufsproduktBesitzType.class,
    VerkaufsproduktKfzType.class
})
public abstract class VerkaufsproduktType
    extends ProduktbausteinType
{

    @XmlElement(name = "Verkaufsproduktgeneration")
    protected String verkaufsproduktgeneration;
    @XmlElement(name = "Berechnungsvariante")
    @XmlSchemaType(name = "string")
    protected BerechnungsvarianteType berechnungsvariante;
    @XmlElement(name = "ZusaetzlicheVerkaufproduktdaten")
    protected List<ZusaetzlicheVerkaufproduktdatenType> zusaetzlicheVerkaufproduktdaten;

    /**
     * Ruft den Wert der verkaufsproduktgeneration-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVerkaufsproduktgeneration() {
        return verkaufsproduktgeneration;
    }

    /**
     * Legt den Wert der verkaufsproduktgeneration-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVerkaufsproduktgeneration(String value) {
        this.verkaufsproduktgeneration = value;
    }

    /**
     * Ruft den Wert der berechnungsvariante-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BerechnungsvarianteType }
     *     
     */
    public BerechnungsvarianteType getBerechnungsvariante() {
        return berechnungsvariante;
    }

    /**
     * Legt den Wert der berechnungsvariante-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BerechnungsvarianteType }
     *     
     */
    public void setBerechnungsvariante(BerechnungsvarianteType value) {
        this.berechnungsvariante = value;
    }

    /**
     * Gets the value of the zusaetzlicheVerkaufproduktdaten property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the zusaetzlicheVerkaufproduktdaten property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getZusaetzlicheVerkaufproduktdaten().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ZusaetzlicheVerkaufproduktdatenType }
     * 
     * 
     */
    public List<ZusaetzlicheVerkaufproduktdatenType> getZusaetzlicheVerkaufproduktdaten() {
        if (zusaetzlicheVerkaufproduktdaten == null) {
            zusaetzlicheVerkaufproduktdaten = new ArrayList<ZusaetzlicheVerkaufproduktdatenType>();
        }
        return this.zusaetzlicheVerkaufproduktdaten;
    }

}
