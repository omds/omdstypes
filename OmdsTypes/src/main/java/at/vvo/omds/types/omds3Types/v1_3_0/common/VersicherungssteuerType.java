
package at.vvo.omds.types.omds3Types.v1_3_0.common;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Stellt die Versicherungsteuer einer Prämie dar
 * 
 * <p>Java-Klasse für Versicherungssteuer_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Versicherungssteuer_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Versicherungssteuer" type="{urn:omds20}decimal"/&gt;
 *         &lt;element name="VersicherungssteuerArt" type="{urn:omds3CommonServiceTypes-1-1-0}VersicherungssteuerArt_Type"/&gt;
 *         &lt;element name="VersicherungssteuerSatz" type="{urn:omds20}decimal" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Versicherungssteuer_Type", propOrder = {
    "versicherungssteuer",
    "versicherungssteuerArt",
    "versicherungssteuerSatz"
})
public class VersicherungssteuerType {

    @XmlElement(name = "Versicherungssteuer", required = true)
    protected BigDecimal versicherungssteuer;
    @XmlElement(name = "VersicherungssteuerArt", required = true)
    protected String versicherungssteuerArt;
    @XmlElement(name = "VersicherungssteuerSatz")
    protected BigDecimal versicherungssteuerSatz;

    /**
     * Ruft den Wert der versicherungssteuer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getVersicherungssteuer() {
        return versicherungssteuer;
    }

    /**
     * Legt den Wert der versicherungssteuer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setVersicherungssteuer(BigDecimal value) {
        this.versicherungssteuer = value;
    }

    /**
     * Ruft den Wert der versicherungssteuerArt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersicherungssteuerArt() {
        return versicherungssteuerArt;
    }

    /**
     * Legt den Wert der versicherungssteuerArt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersicherungssteuerArt(String value) {
        this.versicherungssteuerArt = value;
    }

    /**
     * Ruft den Wert der versicherungssteuerSatz-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getVersicherungssteuerSatz() {
        return versicherungssteuerSatz;
    }

    /**
     * Legt den Wert der versicherungssteuerSatz-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setVersicherungssteuerSatz(BigDecimal value) {
        this.versicherungssteuerSatz = value;
    }

}
