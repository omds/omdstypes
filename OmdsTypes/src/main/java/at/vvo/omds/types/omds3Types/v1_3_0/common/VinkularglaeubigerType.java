
package at.vvo.omds.types.omds3Types.v1_3_0.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Typ zur Beschreibung einer Bank als Vinkulargläubiger
 * 
 * <p>Java-Klasse für Vinkularglaeubiger_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Vinkularglaeubiger_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Vertragsnummer"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;minLength value="1"/&gt;
 *               &lt;maxLength value="32"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="VinkularglaeubigerBank"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;minLength value="2"/&gt;
 *               &lt;maxLength value="100"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="VinkularglaeubigerPlz"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;minLength value="4"/&gt;
 *               &lt;maxLength value="5"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="VinkularglaeubigerStrasse" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;minLength value="3"/&gt;
 *               &lt;maxLength value="100"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Vinkularglaeubiger_Type", propOrder = {
    "vertragsnummer",
    "vinkularglaeubigerBank",
    "vinkularglaeubigerPlz",
    "vinkularglaeubigerStrasse"
})
public class VinkularglaeubigerType {

    @XmlElement(name = "Vertragsnummer", required = true)
    protected String vertragsnummer;
    @XmlElement(name = "VinkularglaeubigerBank", required = true)
    protected String vinkularglaeubigerBank;
    @XmlElement(name = "VinkularglaeubigerPlz", required = true)
    protected String vinkularglaeubigerPlz;
    @XmlElement(name = "VinkularglaeubigerStrasse")
    protected String vinkularglaeubigerStrasse;

    /**
     * Ruft den Wert der vertragsnummer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVertragsnummer() {
        return vertragsnummer;
    }

    /**
     * Legt den Wert der vertragsnummer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVertragsnummer(String value) {
        this.vertragsnummer = value;
    }

    /**
     * Ruft den Wert der vinkularglaeubigerBank-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVinkularglaeubigerBank() {
        return vinkularglaeubigerBank;
    }

    /**
     * Legt den Wert der vinkularglaeubigerBank-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVinkularglaeubigerBank(String value) {
        this.vinkularglaeubigerBank = value;
    }

    /**
     * Ruft den Wert der vinkularglaeubigerPlz-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVinkularglaeubigerPlz() {
        return vinkularglaeubigerPlz;
    }

    /**
     * Legt den Wert der vinkularglaeubigerPlz-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVinkularglaeubigerPlz(String value) {
        this.vinkularglaeubigerPlz = value;
    }

    /**
     * Ruft den Wert der vinkularglaeubigerStrasse-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVinkularglaeubigerStrasse() {
        return vinkularglaeubigerStrasse;
    }

    /**
     * Legt den Wert der vinkularglaeubigerStrasse-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVinkularglaeubigerStrasse(String value) {
        this.vinkularglaeubigerStrasse = value;
    }

}
