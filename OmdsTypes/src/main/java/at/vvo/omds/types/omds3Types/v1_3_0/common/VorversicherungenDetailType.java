
package at.vvo.omds.types.omds3Types.v1_3_0.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für VorversicherungenDetail_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="VorversicherungenDetail_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="WurdenVorversicherungenAufgeloest" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="Aufloesungsgrund" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;enumeration value="aus Schaden"/&gt;
 *               &lt;enumeration value="durch einvernehmliche Kündigung"/&gt;
 *               &lt;enumeration value="durch Ablauf"/&gt;
 *               &lt;enumeration value="durch Besitzwechsel"/&gt;
 *               &lt;enumeration value="aus sonstigen Gründen"/&gt;
 *               &lt;enumeration value="zum Ablauf gekündigte Vorversicherung"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Versicherungsgesellschaft" type="{urn:omds3CommonServiceTypes-1-1-0}Versicherungsgesellschaft_Type"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VorversicherungenDetail_Type", propOrder = {
    "wurdenVorversicherungenAufgeloest",
    "aufloesungsgrund",
    "versicherungsgesellschaft"
})
public class VorversicherungenDetailType {

    @XmlElement(name = "WurdenVorversicherungenAufgeloest")
    protected boolean wurdenVorversicherungenAufgeloest;
    @XmlElement(name = "Aufloesungsgrund")
    protected String aufloesungsgrund;
    @XmlElement(name = "Versicherungsgesellschaft", required = true)
    protected String versicherungsgesellschaft;

    /**
     * Ruft den Wert der wurdenVorversicherungenAufgeloest-Eigenschaft ab.
     * 
     */
    public boolean isWurdenVorversicherungenAufgeloest() {
        return wurdenVorversicherungenAufgeloest;
    }

    /**
     * Legt den Wert der wurdenVorversicherungenAufgeloest-Eigenschaft fest.
     * 
     */
    public void setWurdenVorversicherungenAufgeloest(boolean value) {
        this.wurdenVorversicherungenAufgeloest = value;
    }

    /**
     * Ruft den Wert der aufloesungsgrund-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAufloesungsgrund() {
        return aufloesungsgrund;
    }

    /**
     * Legt den Wert der aufloesungsgrund-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAufloesungsgrund(String value) {
        this.aufloesungsgrund = value;
    }

    /**
     * Ruft den Wert der versicherungsgesellschaft-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersicherungsgesellschaft() {
        return versicherungsgesellschaft;
    }

    /**
     * Legt den Wert der versicherungsgesellschaft-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersicherungsgesellschaft(String value) {
        this.versicherungsgesellschaft = value;
    }

}
