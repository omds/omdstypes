
package at.vvo.omds.types.omds3Types.v1_3_0.on2antrag.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.v1_3_0.on2antrag.kfz.SpezEinreichenAntragKfzType;


/**
 * Abstrakter Basistyp für alle Antragseinreichungen
 * 
 * <p>Java-Klasse für SpezEinreichungAntrag_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SpezEinreichungAntrag_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.common}SpezAnfrageAntrag_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="UnterschriftVorhanden" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SpezEinreichungAntrag_Type", propOrder = {
    "unterschriftVorhanden"
})
@XmlSeeAlso({
    SpezEinreichenAntragKfzType.class
})
public abstract class SpezEinreichungAntragType
    extends SpezAnfrageAntragType
{

    @XmlElement(name = "UnterschriftVorhanden")
    protected boolean unterschriftVorhanden;

    /**
     * Ruft den Wert der unterschriftVorhanden-Eigenschaft ab.
     * 
     */
    public boolean isUnterschriftVorhanden() {
        return unterschriftVorhanden;
    }

    /**
     * Legt den Wert der unterschriftVorhanden-Eigenschaft fest.
     * 
     */
    public void setUnterschriftVorhanden(boolean value) {
        this.unterschriftVorhanden = value;
    }

}
