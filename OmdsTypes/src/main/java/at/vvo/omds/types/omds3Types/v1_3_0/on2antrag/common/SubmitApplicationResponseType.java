
package at.vvo.omds.types.omds3Types.v1_3_0.on2antrag.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.v1_3_0.common.CommonResponseType;
import at.vvo.omds.types.omds3Types.v1_3_0.on2antrag.kfz.SubmitApplicationKfzResponseType;


/**
 * Typ für die Antragseinspielung
 * 
 * <p>Java-Klasse für SubmitApplicationResponse_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SubmitApplicationResponse_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}CommonResponse_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Antragstatus" type="{urn:omds3CommonServiceTypes-1-1-0}SubmitApplicationStatus_Type"/&gt;
 *         &lt;element name="AntragsId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubmitApplicationResponse_Type", propOrder = {
    "antragstatus",
    "antragsId"
})
@XmlSeeAlso({
    SubmitApplicationKfzResponseType.class
})
public abstract class SubmitApplicationResponseType
    extends CommonResponseType
{

    @XmlElement(name = "Antragstatus")
    protected int antragstatus;
    @XmlElement(name = "AntragsId", required = true)
    protected String antragsId;

    /**
     * Ruft den Wert der antragstatus-Eigenschaft ab.
     * 
     */
    public int getAntragstatus() {
        return antragstatus;
    }

    /**
     * Legt den Wert der antragstatus-Eigenschaft fest.
     * 
     */
    public void setAntragstatus(int value) {
        this.antragstatus = value;
    }

    /**
     * Ruft den Wert der antragsId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAntragsId() {
        return antragsId;
    }

    /**
     * Legt den Wert der antragsId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAntragsId(String value) {
        this.antragsId = value;
    }

}
