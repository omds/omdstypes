
package at.vvo.omds.types.omds3Types.v1_3_0.on2antrag.kfz;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.v1_3_0.common.CommonResponseType;


/**
 * Typ des Responseobjekts für eine Berechnung Kfz
 * 
 * <p>Java-Klasse für CalculateKfzResponse_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CalculateKfzResponse_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}CommonResponse_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Berechnungsantwort" type="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz}SpezBerechnungKfz_Type"/&gt;
 *         &lt;element name="ResponseUpselling" type="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz}UpsellingKfzResponse_Type" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CalculateKfzResponse_Type", propOrder = {
    "berechnungsantwort",
    "responseUpselling"
})
public class CalculateKfzResponseType
    extends CommonResponseType
{

    @XmlElement(name = "Berechnungsantwort", required = true)
    protected SpezBerechnungKfzType berechnungsantwort;
    @XmlElement(name = "ResponseUpselling")
    protected UpsellingKfzResponseType responseUpselling;

    /**
     * Ruft den Wert der berechnungsantwort-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SpezBerechnungKfzType }
     *     
     */
    public SpezBerechnungKfzType getBerechnungsantwort() {
        return berechnungsantwort;
    }

    /**
     * Legt den Wert der berechnungsantwort-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SpezBerechnungKfzType }
     *     
     */
    public void setBerechnungsantwort(SpezBerechnungKfzType value) {
        this.berechnungsantwort = value;
    }

    /**
     * Ruft den Wert der responseUpselling-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link UpsellingKfzResponseType }
     *     
     */
    public UpsellingKfzResponseType getResponseUpselling() {
        return responseUpselling;
    }

    /**
     * Legt den Wert der responseUpselling-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link UpsellingKfzResponseType }
     *     
     */
    public void setResponseUpselling(UpsellingKfzResponseType value) {
        this.responseUpselling = value;
    }

}
