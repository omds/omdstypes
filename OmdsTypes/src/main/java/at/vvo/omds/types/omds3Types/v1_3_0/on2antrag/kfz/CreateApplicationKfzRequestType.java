
package at.vvo.omds.types.omds3Types.v1_3_0.on2antrag.kfz;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.v1_3_0.common.CommonRequestType;


/**
 * Typ des Requestobjekts für die Erzeugung eines Antrags Kfz
 * 
 * <p>Java-Klasse für CreateApplicationKfzRequest_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CreateApplicationKfzRequest_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}CommonRequest_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Antraganfrage" type="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz}SpezAnfrageAntragKfz_Type"/&gt;
 *         &lt;element name="Zulassungsdaten" type="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz}Zulassungsdaten_Type" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreateApplicationKfzRequest_Type", propOrder = {
    "antraganfrage",
    "zulassungsdaten"
})
public class CreateApplicationKfzRequestType
    extends CommonRequestType
{

    @XmlElement(name = "Antraganfrage", required = true)
    protected SpezAnfrageAntragKfzType antraganfrage;
    @XmlElement(name = "Zulassungsdaten")
    protected ZulassungsdatenType zulassungsdaten;

    /**
     * Ruft den Wert der antraganfrage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SpezAnfrageAntragKfzType }
     *     
     */
    public SpezAnfrageAntragKfzType getAntraganfrage() {
        return antraganfrage;
    }

    /**
     * Legt den Wert der antraganfrage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SpezAnfrageAntragKfzType }
     *     
     */
    public void setAntraganfrage(SpezAnfrageAntragKfzType value) {
        this.antraganfrage = value;
    }

    /**
     * Ruft den Wert der zulassungsdaten-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ZulassungsdatenType }
     *     
     */
    public ZulassungsdatenType getZulassungsdaten() {
        return zulassungsdaten;
    }

    /**
     * Legt den Wert der zulassungsdaten-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ZulassungsdatenType }
     *     
     */
    public void setZulassungsdaten(ZulassungsdatenType value) {
        this.zulassungsdaten = value;
    }

}
