
package at.vvo.omds.types.omds3Types.v1_3_0.on2antrag.kfz;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.v1_3_0.ArcImageInfo;
import at.vvo.omds.types.omds3Types.v1_3_0.common.CommonResponseType;


/**
 * Typ des Responseobjekts für einen Antrag Kfz
 * 
 * <p>Java-Klasse für CreateApplicationKfzResponse_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CreateApplicationKfzResponse_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}CommonResponse_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Antragantwort"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz}SpezAnfrageAntragKfz_Type"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Dokumente" type="{urn:omds3ServiceTypes-1-1-0}ArcImageInfo" maxOccurs="unbounded" minOccurs="0"/&gt;
 *                   &lt;element name="AntragsId" type="{http://www.w3.org/2001/XMLSchema}anyType"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/extension&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreateApplicationKfzResponse_Type", propOrder = {
    "antragantwort"
})
public class CreateApplicationKfzResponseType
    extends CommonResponseType
{

    @XmlElement(name = "Antragantwort", required = true)
    protected CreateApplicationKfzResponseType.Antragantwort antragantwort;

    /**
     * Ruft den Wert der antragantwort-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CreateApplicationKfzResponseType.Antragantwort }
     *     
     */
    public CreateApplicationKfzResponseType.Antragantwort getAntragantwort() {
        return antragantwort;
    }

    /**
     * Legt den Wert der antragantwort-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CreateApplicationKfzResponseType.Antragantwort }
     *     
     */
    public void setAntragantwort(CreateApplicationKfzResponseType.Antragantwort value) {
        this.antragantwort = value;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz}SpezAnfrageAntragKfz_Type"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Dokumente" type="{urn:omds3ServiceTypes-1-1-0}ArcImageInfo" maxOccurs="unbounded" minOccurs="0"/&gt;
     *         &lt;element name="AntragsId" type="{http://www.w3.org/2001/XMLSchema}anyType"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/extension&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "dokumente",
        "antragsId"
    })
    public static class Antragantwort
        extends SpezAnfrageAntragKfzType
    {

        @XmlElement(name = "Dokumente")
        protected List<ArcImageInfo> dokumente;
        @XmlElement(name = "AntragsId", required = true)
        protected Object antragsId;

        /**
         * Gets the value of the dokumente property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the dokumente property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getDokumente().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ArcImageInfo }
         * 
         * 
         */
        public List<ArcImageInfo> getDokumente() {
            if (dokumente == null) {
                dokumente = new ArrayList<ArcImageInfo>();
            }
            return this.dokumente;
        }

        /**
         * Ruft den Wert der antragsId-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getAntragsId() {
            return antragsId;
        }

        /**
         * Legt den Wert der antragsId-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setAntragsId(Object value) {
            this.antragsId = value;
        }

    }

}
