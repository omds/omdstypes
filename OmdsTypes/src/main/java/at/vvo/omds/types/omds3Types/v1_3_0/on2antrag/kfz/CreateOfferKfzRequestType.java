
package at.vvo.omds.types.omds3Types.v1_3_0.on2antrag.kfz;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.v1_3_0.common.CommonRequestType;


/**
 * Typ des Request für eine Kfz-Offert
 * 
 * <p>Java-Klasse für CreateOfferKfzRequest_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CreateOfferKfzRequest_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}CommonRequest_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Offertanfrage" type="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz}SpezAnfrageOffertKfz_Type"/&gt;
 *         &lt;element name="Zulassungsdaten" type="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz}Zulassungsdaten_Type" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreateOfferKfzRequest_Type", propOrder = {
    "offertanfrage",
    "zulassungsdaten"
})
public class CreateOfferKfzRequestType
    extends CommonRequestType
{

    @XmlElement(name = "Offertanfrage", required = true)
    protected SpezAnfrageOffertKfzType offertanfrage;
    @XmlElement(name = "Zulassungsdaten")
    protected ZulassungsdatenType zulassungsdaten;

    /**
     * Ruft den Wert der offertanfrage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SpezAnfrageOffertKfzType }
     *     
     */
    public SpezAnfrageOffertKfzType getOffertanfrage() {
        return offertanfrage;
    }

    /**
     * Legt den Wert der offertanfrage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SpezAnfrageOffertKfzType }
     *     
     */
    public void setOffertanfrage(SpezAnfrageOffertKfzType value) {
        this.offertanfrage = value;
    }

    /**
     * Ruft den Wert der zulassungsdaten-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ZulassungsdatenType }
     *     
     */
    public ZulassungsdatenType getZulassungsdaten() {
        return zulassungsdaten;
    }

    /**
     * Legt den Wert der zulassungsdaten-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ZulassungsdatenType }
     *     
     */
    public void setZulassungsdaten(ZulassungsdatenType value) {
        this.zulassungsdaten = value;
    }

}
