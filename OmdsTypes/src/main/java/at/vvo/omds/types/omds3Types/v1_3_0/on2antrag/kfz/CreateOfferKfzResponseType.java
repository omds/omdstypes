
package at.vvo.omds.types.omds3Types.v1_3_0.on2antrag.kfz;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.v1_3_0.ArcImageInfo;
import at.vvo.omds.types.omds3Types.v1_3_0.common.CommonResponseType;


/**
 * Typ des Response für ein Kfz-Offert Kfz
 * 
 * <p>Java-Klasse für CreateOfferKfzResponse_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CreateOfferKfzResponse_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}CommonResponse_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Offertantwort"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz}SpezAnfrageOffertKfz_Type"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Dokumente" type="{urn:omds3ServiceTypes-1-1-0}ArcImageInfo" maxOccurs="unbounded" minOccurs="0"/&gt;
 *                   &lt;element name="OffertId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/extension&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreateOfferKfzResponse_Type", propOrder = {
    "offertantwort"
})
public class CreateOfferKfzResponseType
    extends CommonResponseType
{

    @XmlElement(name = "Offertantwort", required = true)
    protected CreateOfferKfzResponseType.Offertantwort offertantwort;

    /**
     * Ruft den Wert der offertantwort-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CreateOfferKfzResponseType.Offertantwort }
     *     
     */
    public CreateOfferKfzResponseType.Offertantwort getOffertantwort() {
        return offertantwort;
    }

    /**
     * Legt den Wert der offertantwort-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CreateOfferKfzResponseType.Offertantwort }
     *     
     */
    public void setOffertantwort(CreateOfferKfzResponseType.Offertantwort value) {
        this.offertantwort = value;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz}SpezAnfrageOffertKfz_Type"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Dokumente" type="{urn:omds3ServiceTypes-1-1-0}ArcImageInfo" maxOccurs="unbounded" minOccurs="0"/&gt;
     *         &lt;element name="OffertId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/extension&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "dokumente",
        "offertId"
    })
    public static class Offertantwort
        extends SpezAnfrageOffertKfzType
    {

        @XmlElement(name = "Dokumente")
        protected List<ArcImageInfo> dokumente;
        @XmlElement(name = "OffertId", required = true)
        protected String offertId;

        /**
         * Gets the value of the dokumente property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the dokumente property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getDokumente().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ArcImageInfo }
         * 
         * 
         */
        public List<ArcImageInfo> getDokumente() {
            if (dokumente == null) {
                dokumente = new ArrayList<ArcImageInfo>();
            }
            return this.dokumente;
        }

        /**
         * Ruft den Wert der offertId-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOffertId() {
            return offertId;
        }

        /**
         * Legt den Wert der offertId-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOffertId(String value) {
            this.offertId = value;
        }

    }

}
