
package at.vvo.omds.types.omds3Types.v1_3_0.on2antrag.kfz;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import at.vvo.omds.types.omds2Types.v2_9.Entsch2Type;
import at.vvo.omds.types.omds3Types.v1_3_0.common.ObjektIdType;


/**
 * Basisklasse für alle Fahrzeuge in der Kfz-Versicherung
 * 
 * <p>Java-Klasse für Fahrzeug_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Fahrzeug_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:omds3CommonServiceTypes-1-1-0}ObjektId"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="FzgArtCd" type="{urn:omds20}FzgArtCd_Type" /&gt;
 *       &lt;attribute name="Marke"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="40"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="Handelsbez"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="30"/&gt;
 *             &lt;minLength value="1"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="TypVarVer"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="20"/&gt;
 *             &lt;minLength value="1"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="Leasingkz" type="{urn:omds20}Entsch2_Type" /&gt;
 *       &lt;attribute name="Modelljahr" type="{http://www.w3.org/2001/XMLSchema}unsignedShort" /&gt;
 *       &lt;attribute name="Leistung" type="{http://www.w3.org/2001/XMLSchema}unsignedShort" /&gt;
 *       &lt;attribute name="Plaetze" type="{http://www.w3.org/2001/XMLSchema}unsignedShort" /&gt;
 *       &lt;attribute name="Nutzlast"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal"&gt;
 *             &lt;totalDigits value="6"/&gt;
 *             &lt;fractionDigits value="0"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="Hubraum" type="{http://www.w3.org/2001/XMLSchema}unsignedShort" /&gt;
 *       &lt;attribute name="AntriebsArtCd" type="{urn:omds20}AntriebsArtCd_Type" /&gt;
 *       &lt;attribute name="CO2_Ausstoss" type="{http://www.w3.org/2001/XMLSchema}unsignedShort" /&gt;
 *       &lt;attribute name="Fahrgestnr"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="20"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="Motornr"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="20"/&gt;
 *             &lt;minLength value="1"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="NatCode"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;minLength value="1"/&gt;
 *             &lt;maxLength value="26"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="VerwendzweckCd" type="{urn:omds20}VerwendzweckCd_Type" /&gt;
 *       &lt;attribute name="Erstzulassdat" type="{urn:omds20}Datum" /&gt;
 *       &lt;attribute name="LandesCd" type="{urn:omds20}LandesCd_Type" /&gt;
 *       &lt;attribute name="Pol_Kennz"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="12"/&gt;
 *             &lt;minLength value="1"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="Tueren" type="{http://www.w3.org/2001/XMLSchema}unsignedShort" /&gt;
 *       &lt;attribute name="Baujahr" type="{http://www.w3.org/2001/XMLSchema}unsignedShort" /&gt;
 *       &lt;attribute name="Gesamtgewicht" type="{http://www.w3.org/2001/XMLSchema}unsignedShort" /&gt;
 *       &lt;attribute name="Listenpreis" type="{urn:omds20}decimal" /&gt;
 *       &lt;attribute name="Sonderausstattung" type="{urn:omds20}decimal" /&gt;
 *       &lt;attribute name="Eigengewicht" type="{http://www.w3.org/2001/XMLSchema}unsignedShort" /&gt;
 *       &lt;attribute name="ZulassdatHalter" type="{urn:omds20}Datum" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Fahrzeug_Type", propOrder = {
    "objektId"
})
public class FahrzeugType {

    @XmlElement(name = "ObjektId", namespace = "urn:omds3CommonServiceTypes-1-1-0", required = true)
    protected ObjektIdType objektId;
    @XmlAttribute(name = "FzgArtCd", namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz")
    protected String fzgArtCd;
    @XmlAttribute(name = "Marke", namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz")
    protected String marke;
    @XmlAttribute(name = "Handelsbez", namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz")
    protected String handelsbez;
    @XmlAttribute(name = "TypVarVer", namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz")
    protected String typVarVer;
    @XmlAttribute(name = "Leasingkz", namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz")
    protected Entsch2Type leasingkz;
    @XmlAttribute(name = "Modelljahr", namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz")
    @XmlSchemaType(name = "unsignedShort")
    protected Integer modelljahr;
    @XmlAttribute(name = "Leistung", namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz")
    @XmlSchemaType(name = "unsignedShort")
    protected Integer leistung;
    @XmlAttribute(name = "Plaetze", namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz")
    @XmlSchemaType(name = "unsignedShort")
    protected Integer plaetze;
    @XmlAttribute(name = "Nutzlast", namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz")
    protected BigDecimal nutzlast;
    @XmlAttribute(name = "Hubraum", namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz")
    @XmlSchemaType(name = "unsignedShort")
    protected Integer hubraum;
    @XmlAttribute(name = "AntriebsArtCd", namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz")
    protected String antriebsArtCd;
    @XmlAttribute(name = "CO2_Ausstoss", namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz")
    @XmlSchemaType(name = "unsignedShort")
    protected Integer co2Ausstoss;
    @XmlAttribute(name = "Fahrgestnr", namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz")
    protected String fahrgestnr;
    @XmlAttribute(name = "Motornr", namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz")
    protected String motornr;
    @XmlAttribute(name = "NatCode", namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz")
    protected String natCode;
    @XmlAttribute(name = "VerwendzweckCd", namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz")
    protected String verwendzweckCd;
    @XmlAttribute(name = "Erstzulassdat", namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz")
    protected XMLGregorianCalendar erstzulassdat;
    @XmlAttribute(name = "LandesCd", namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz")
    protected String landesCd;
    @XmlAttribute(name = "Pol_Kennz", namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz")
    protected String polKennz;
    @XmlAttribute(name = "Tueren", namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz")
    @XmlSchemaType(name = "unsignedShort")
    protected Integer tueren;
    @XmlAttribute(name = "Baujahr", namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz")
    @XmlSchemaType(name = "unsignedShort")
    protected Integer baujahr;
    @XmlAttribute(name = "Gesamtgewicht", namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz")
    @XmlSchemaType(name = "unsignedShort")
    protected Integer gesamtgewicht;
    @XmlAttribute(name = "Listenpreis", namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz")
    protected BigDecimal listenpreis;
    @XmlAttribute(name = "Sonderausstattung", namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz")
    protected BigDecimal sonderausstattung;
    @XmlAttribute(name = "Eigengewicht", namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz")
    @XmlSchemaType(name = "unsignedShort")
    protected Integer eigengewicht;
    @XmlAttribute(name = "ZulassdatHalter", namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz")
    protected XMLGregorianCalendar zulassdatHalter;

    /**
     * Ruft den Wert der objektId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ObjektIdType }
     *     
     */
    public ObjektIdType getObjektId() {
        return objektId;
    }

    /**
     * Legt den Wert der objektId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjektIdType }
     *     
     */
    public void setObjektId(ObjektIdType value) {
        this.objektId = value;
    }

    /**
     * Ruft den Wert der fzgArtCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFzgArtCd() {
        return fzgArtCd;
    }

    /**
     * Legt den Wert der fzgArtCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFzgArtCd(String value) {
        this.fzgArtCd = value;
    }

    /**
     * Ruft den Wert der marke-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMarke() {
        return marke;
    }

    /**
     * Legt den Wert der marke-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMarke(String value) {
        this.marke = value;
    }

    /**
     * Ruft den Wert der handelsbez-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHandelsbez() {
        return handelsbez;
    }

    /**
     * Legt den Wert der handelsbez-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHandelsbez(String value) {
        this.handelsbez = value;
    }

    /**
     * Ruft den Wert der typVarVer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTypVarVer() {
        return typVarVer;
    }

    /**
     * Legt den Wert der typVarVer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTypVarVer(String value) {
        this.typVarVer = value;
    }

    /**
     * Ruft den Wert der leasingkz-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Entsch2Type }
     *     
     */
    public Entsch2Type getLeasingkz() {
        return leasingkz;
    }

    /**
     * Legt den Wert der leasingkz-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Entsch2Type }
     *     
     */
    public void setLeasingkz(Entsch2Type value) {
        this.leasingkz = value;
    }

    /**
     * Ruft den Wert der modelljahr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getModelljahr() {
        return modelljahr;
    }

    /**
     * Legt den Wert der modelljahr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setModelljahr(Integer value) {
        this.modelljahr = value;
    }

    /**
     * Ruft den Wert der leistung-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getLeistung() {
        return leistung;
    }

    /**
     * Legt den Wert der leistung-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setLeistung(Integer value) {
        this.leistung = value;
    }

    /**
     * Ruft den Wert der plaetze-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPlaetze() {
        return plaetze;
    }

    /**
     * Legt den Wert der plaetze-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPlaetze(Integer value) {
        this.plaetze = value;
    }

    /**
     * Ruft den Wert der nutzlast-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getNutzlast() {
        return nutzlast;
    }

    /**
     * Legt den Wert der nutzlast-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setNutzlast(BigDecimal value) {
        this.nutzlast = value;
    }

    /**
     * Ruft den Wert der hubraum-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getHubraum() {
        return hubraum;
    }

    /**
     * Legt den Wert der hubraum-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setHubraum(Integer value) {
        this.hubraum = value;
    }

    /**
     * Ruft den Wert der antriebsArtCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAntriebsArtCd() {
        return antriebsArtCd;
    }

    /**
     * Legt den Wert der antriebsArtCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAntriebsArtCd(String value) {
        this.antriebsArtCd = value;
    }

    /**
     * Ruft den Wert der co2Ausstoss-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCO2Ausstoss() {
        return co2Ausstoss;
    }

    /**
     * Legt den Wert der co2Ausstoss-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCO2Ausstoss(Integer value) {
        this.co2Ausstoss = value;
    }

    /**
     * Ruft den Wert der fahrgestnr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFahrgestnr() {
        return fahrgestnr;
    }

    /**
     * Legt den Wert der fahrgestnr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFahrgestnr(String value) {
        this.fahrgestnr = value;
    }

    /**
     * Ruft den Wert der motornr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMotornr() {
        return motornr;
    }

    /**
     * Legt den Wert der motornr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMotornr(String value) {
        this.motornr = value;
    }

    /**
     * Ruft den Wert der natCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNatCode() {
        return natCode;
    }

    /**
     * Legt den Wert der natCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNatCode(String value) {
        this.natCode = value;
    }

    /**
     * Ruft den Wert der verwendzweckCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVerwendzweckCd() {
        return verwendzweckCd;
    }

    /**
     * Legt den Wert der verwendzweckCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVerwendzweckCd(String value) {
        this.verwendzweckCd = value;
    }

    /**
     * Ruft den Wert der erstzulassdat-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getErstzulassdat() {
        return erstzulassdat;
    }

    /**
     * Legt den Wert der erstzulassdat-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setErstzulassdat(XMLGregorianCalendar value) {
        this.erstzulassdat = value;
    }

    /**
     * Ruft den Wert der landesCd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLandesCd() {
        return landesCd;
    }

    /**
     * Legt den Wert der landesCd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLandesCd(String value) {
        this.landesCd = value;
    }

    /**
     * Ruft den Wert der polKennz-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolKennz() {
        return polKennz;
    }

    /**
     * Legt den Wert der polKennz-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolKennz(String value) {
        this.polKennz = value;
    }

    /**
     * Ruft den Wert der tueren-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTueren() {
        return tueren;
    }

    /**
     * Legt den Wert der tueren-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTueren(Integer value) {
        this.tueren = value;
    }

    /**
     * Ruft den Wert der baujahr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getBaujahr() {
        return baujahr;
    }

    /**
     * Legt den Wert der baujahr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setBaujahr(Integer value) {
        this.baujahr = value;
    }

    /**
     * Ruft den Wert der gesamtgewicht-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getGesamtgewicht() {
        return gesamtgewicht;
    }

    /**
     * Legt den Wert der gesamtgewicht-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setGesamtgewicht(Integer value) {
        this.gesamtgewicht = value;
    }

    /**
     * Ruft den Wert der listenpreis-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getListenpreis() {
        return listenpreis;
    }

    /**
     * Legt den Wert der listenpreis-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setListenpreis(BigDecimal value) {
        this.listenpreis = value;
    }

    /**
     * Ruft den Wert der sonderausstattung-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSonderausstattung() {
        return sonderausstattung;
    }

    /**
     * Legt den Wert der sonderausstattung-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSonderausstattung(BigDecimal value) {
        this.sonderausstattung = value;
    }

    /**
     * Ruft den Wert der eigengewicht-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getEigengewicht() {
        return eigengewicht;
    }

    /**
     * Legt den Wert der eigengewicht-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setEigengewicht(Integer value) {
        this.eigengewicht = value;
    }

    /**
     * Ruft den Wert der zulassdatHalter-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getZulassdatHalter() {
        return zulassdatHalter;
    }

    /**
     * Legt den Wert der zulassdatHalter-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setZulassdatHalter(XMLGregorianCalendar value) {
        this.zulassdatHalter = value;
    }

}
