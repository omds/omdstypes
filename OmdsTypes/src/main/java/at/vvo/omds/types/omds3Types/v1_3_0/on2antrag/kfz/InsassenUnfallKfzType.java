
package at.vvo.omds.types.omds3Types.v1_3_0.on2antrag.kfz;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.v1_3_0.common.ElementarproduktType;


/**
 * Typ für das Elementarprodukt KFZ-Insassenunfall
 * 
 * <p>Java-Klasse für InsassenUnfallKfz_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="InsassenUnfallKfz_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}Elementarprodukt_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz}FahrzeugRefLfdNr" minOccurs="0"/&gt;
 *         &lt;element name="InsassenUnfallSystem" type="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz}InsassenUnfallSystem_Type"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InsassenUnfallKfz_Type", propOrder = {
    "fahrzeugRefLfdNr",
    "insassenUnfallSystem"
})
public class InsassenUnfallKfzType
    extends ElementarproduktType
{

    @XmlElement(name = "FahrzeugRefLfdNr")
    @XmlSchemaType(name = "unsignedShort")
    protected Integer fahrzeugRefLfdNr;
    @XmlElement(name = "InsassenUnfallSystem", required = true)
    @XmlSchemaType(name = "string")
    protected InsassenUnfallSystemType insassenUnfallSystem;

    /**
     * Ruft den Wert der fahrzeugRefLfdNr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getFahrzeugRefLfdNr() {
        return fahrzeugRefLfdNr;
    }

    /**
     * Legt den Wert der fahrzeugRefLfdNr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setFahrzeugRefLfdNr(Integer value) {
        this.fahrzeugRefLfdNr = value;
    }

    /**
     * Ruft den Wert der insassenUnfallSystem-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link InsassenUnfallSystemType }
     *     
     */
    public InsassenUnfallSystemType getInsassenUnfallSystem() {
        return insassenUnfallSystem;
    }

    /**
     * Legt den Wert der insassenUnfallSystem-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link InsassenUnfallSystemType }
     *     
     */
    public void setInsassenUnfallSystem(InsassenUnfallSystemType value) {
        this.insassenUnfallSystem = value;
    }

}
