
package at.vvo.omds.types.omds3Types.v1_3_0.on2antrag.kfz;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für InsassenUnfallSystem_Type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="InsassenUnfallSystem_Type"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Pauschalsystem"/&gt;
 *     &lt;enumeration value="Platzsystem1"/&gt;
 *     &lt;enumeration value="Platzsystem2"/&gt;
 *     &lt;enumeration value="Personensystem"/&gt;
 *     &lt;enumeration value="Lenkerunfallversicherung"/&gt;
 *     &lt;enumeration value="Aufsassenunfallversicherung"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "InsassenUnfallSystem_Type")
@XmlEnum
public enum InsassenUnfallSystemType {

    @XmlEnumValue("Pauschalsystem")
    PAUSCHALSYSTEM("Pauschalsystem"),
    @XmlEnumValue("Platzsystem1")
    PLATZSYSTEM_1("Platzsystem1"),
    @XmlEnumValue("Platzsystem2")
    PLATZSYSTEM_2("Platzsystem2"),
    @XmlEnumValue("Personensystem")
    PERSONENSYSTEM("Personensystem"),
    @XmlEnumValue("Lenkerunfallversicherung")
    LENKERUNFALLVERSICHERUNG("Lenkerunfallversicherung"),
    @XmlEnumValue("Aufsassenunfallversicherung")
    AUFSASSENUNFALLVERSICHERUNG("Aufsassenunfallversicherung");
    private final String value;

    InsassenUnfallSystemType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static InsassenUnfallSystemType fromValue(String v) {
        for (InsassenUnfallSystemType c: InsassenUnfallSystemType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
