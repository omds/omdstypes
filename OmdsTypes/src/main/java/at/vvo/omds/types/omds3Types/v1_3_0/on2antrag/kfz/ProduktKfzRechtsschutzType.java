
package at.vvo.omds.types.omds3Types.v1_3_0.on2antrag.kfz;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Typ für eine Kfz-Rechtsschutz-Versicherung, welches einer Vertragssparte entspricht
 * 
 * <p>Java-Klasse für ProduktKfzRechtsschutz_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ProduktKfzRechtsschutz_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz}ZusatzproduktKfz_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Verkehrsrechtsschutz" type="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz}VerkehrsrechtsschutzKfz_Type"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProduktKfzRechtsschutz_Type", propOrder = {
    "verkehrsrechtsschutz"
})
public class ProduktKfzRechtsschutzType
    extends ZusatzproduktKfzType
{

    @XmlElement(name = "Verkehrsrechtsschutz", required = true)
    protected VerkehrsrechtsschutzKfzType verkehrsrechtsschutz;

    /**
     * Ruft den Wert der verkehrsrechtsschutz-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VerkehrsrechtsschutzKfzType }
     *     
     */
    public VerkehrsrechtsschutzKfzType getVerkehrsrechtsschutz() {
        return verkehrsrechtsschutz;
    }

    /**
     * Legt den Wert der verkehrsrechtsschutz-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VerkehrsrechtsschutzKfzType }
     *     
     */
    public void setVerkehrsrechtsschutz(VerkehrsrechtsschutzKfzType value) {
        this.verkehrsrechtsschutz = value;
    }

}
