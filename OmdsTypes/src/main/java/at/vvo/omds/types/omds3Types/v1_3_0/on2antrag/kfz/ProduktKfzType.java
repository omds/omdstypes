
package at.vvo.omds.types.omds3Types.v1_3_0.on2antrag.kfz;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.v1_3_0.common.ObjektIdType;
import at.vvo.omds.types.omds3Types.v1_3_0.common.ProduktType;


/**
 * Typ für ein Kfz-Produkt, welches einer Vertragssparte entspricht
 * 
 * <p>Java-Klasse für ProduktKfz_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ProduktKfz_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}Produkt_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="VersichertesFahrzeug" type="{urn:omds3CommonServiceTypes-1-1-0}ObjektId_Type" maxOccurs="3"/&gt;
 *         &lt;element name="Haftpflicht" type="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz}HaftpflichtKfz_Type"/&gt;
 *         &lt;choice&gt;
 *           &lt;element name="Teilkasko" type="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz}TeilkaskoKfz_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *           &lt;element name="Vollkasko" type="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz}VollkaskoKfz_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="Insassenunfall" type="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz}InsassenUnfallKfz_Type" minOccurs="0"/&gt;
 *         &lt;element name="Assistance" type="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz}AssistanceKfz_Type" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProduktKfz_Type", propOrder = {
    "versichertesFahrzeug",
    "haftpflicht",
    "teilkasko",
    "vollkasko",
    "insassenunfall",
    "assistance"
})
public class ProduktKfzType
    extends ProduktType
{

    @XmlElement(name = "VersichertesFahrzeug", required = true)
    protected List<ObjektIdType> versichertesFahrzeug;
    @XmlElement(name = "Haftpflicht", required = true)
    protected HaftpflichtKfzType haftpflicht;
    @XmlElement(name = "Teilkasko")
    protected List<TeilkaskoKfzType> teilkasko;
    @XmlElement(name = "Vollkasko")
    protected List<VollkaskoKfzType> vollkasko;
    @XmlElement(name = "Insassenunfall")
    protected InsassenUnfallKfzType insassenunfall;
    @XmlElement(name = "Assistance")
    protected AssistanceKfzType assistance;

    /**
     * Gets the value of the versichertesFahrzeug property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the versichertesFahrzeug property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVersichertesFahrzeug().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ObjektIdType }
     * 
     * 
     */
    public List<ObjektIdType> getVersichertesFahrzeug() {
        if (versichertesFahrzeug == null) {
            versichertesFahrzeug = new ArrayList<ObjektIdType>();
        }
        return this.versichertesFahrzeug;
    }

    /**
     * Ruft den Wert der haftpflicht-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link HaftpflichtKfzType }
     *     
     */
    public HaftpflichtKfzType getHaftpflicht() {
        return haftpflicht;
    }

    /**
     * Legt den Wert der haftpflicht-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link HaftpflichtKfzType }
     *     
     */
    public void setHaftpflicht(HaftpflichtKfzType value) {
        this.haftpflicht = value;
    }

    /**
     * Gets the value of the teilkasko property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the teilkasko property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTeilkasko().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TeilkaskoKfzType }
     * 
     * 
     */
    public List<TeilkaskoKfzType> getTeilkasko() {
        if (teilkasko == null) {
            teilkasko = new ArrayList<TeilkaskoKfzType>();
        }
        return this.teilkasko;
    }

    /**
     * Gets the value of the vollkasko property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the vollkasko property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVollkasko().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link VollkaskoKfzType }
     * 
     * 
     */
    public List<VollkaskoKfzType> getVollkasko() {
        if (vollkasko == null) {
            vollkasko = new ArrayList<VollkaskoKfzType>();
        }
        return this.vollkasko;
    }

    /**
     * Ruft den Wert der insassenunfall-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link InsassenUnfallKfzType }
     *     
     */
    public InsassenUnfallKfzType getInsassenunfall() {
        return insassenunfall;
    }

    /**
     * Legt den Wert der insassenunfall-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link InsassenUnfallKfzType }
     *     
     */
    public void setInsassenunfall(InsassenUnfallKfzType value) {
        this.insassenunfall = value;
    }

    /**
     * Ruft den Wert der assistance-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AssistanceKfzType }
     *     
     */
    public AssistanceKfzType getAssistance() {
        return assistance;
    }

    /**
     * Legt den Wert der assistance-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AssistanceKfzType }
     *     
     */
    public void setAssistance(AssistanceKfzType value) {
        this.assistance = value;
    }

}
