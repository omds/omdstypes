
package at.vvo.omds.types.omds3Types.v1_3_0.on2antrag.kfz;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für SchaedenAmFahrzeug_Type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="SchaedenAmFahrzeug_Type"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="keine Schäden an der Karosserie"/&gt;
 *     &lt;enumeration value="Schäden an der Karosserie"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "SchaedenAmFahrzeug_Type")
@XmlEnum
public enum SchaedenAmFahrzeugType {

    @XmlEnumValue("keine Sch\u00e4den an der Karosserie")
    KEINE_SCHÄDEN_AN_DER_KAROSSERIE("keine Sch\u00e4den an der Karosserie"),
    @XmlEnumValue("Sch\u00e4den an der Karosserie")
    SCHÄDEN_AN_DER_KAROSSERIE("Sch\u00e4den an der Karosserie");
    private final String value;

    SchaedenAmFahrzeugType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SchaedenAmFahrzeugType fromValue(String v) {
        for (SchaedenAmFahrzeugType c: SchaedenAmFahrzeugType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
