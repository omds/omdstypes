
package at.vvo.omds.types.omds3Types.v1_3_0.on2antrag.kfz;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.v1_3_0.common.CommonRequestType;
import at.vvo.omds.types.omds3Types.v1_3_0.common.DateianhangType;


/**
 * Typ des Requestobjekts für eine Antragseinreichung Kfz
 * 
 * <p>Java-Klasse für SubmitApplicationKfzRequest_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SubmitApplicationKfzRequest_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}CommonRequest_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Antragseinreichung" type="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz}SpezEinreichenAntragKfz_Type"/&gt;
 *         &lt;element name="Zulassungsdaten" type="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz}Zulassungsdaten_Type" minOccurs="0"/&gt;
 *         &lt;element name="Dateianhaenge" type="{urn:omds3CommonServiceTypes-1-1-0}Dateianhang_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubmitApplicationKfzRequest_Type", propOrder = {
    "antragseinreichung",
    "zulassungsdaten",
    "dateianhaenge"
})
public class SubmitApplicationKfzRequestType
    extends CommonRequestType
{

    @XmlElement(name = "Antragseinreichung", required = true)
    protected SpezEinreichenAntragKfzType antragseinreichung;
    @XmlElement(name = "Zulassungsdaten")
    protected ZulassungsdatenType zulassungsdaten;
    @XmlElement(name = "Dateianhaenge")
    protected List<DateianhangType> dateianhaenge;

    /**
     * Ruft den Wert der antragseinreichung-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SpezEinreichenAntragKfzType }
     *     
     */
    public SpezEinreichenAntragKfzType getAntragseinreichung() {
        return antragseinreichung;
    }

    /**
     * Legt den Wert der antragseinreichung-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SpezEinreichenAntragKfzType }
     *     
     */
    public void setAntragseinreichung(SpezEinreichenAntragKfzType value) {
        this.antragseinreichung = value;
    }

    /**
     * Ruft den Wert der zulassungsdaten-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ZulassungsdatenType }
     *     
     */
    public ZulassungsdatenType getZulassungsdaten() {
        return zulassungsdaten;
    }

    /**
     * Legt den Wert der zulassungsdaten-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ZulassungsdatenType }
     *     
     */
    public void setZulassungsdaten(ZulassungsdatenType value) {
        this.zulassungsdaten = value;
    }

    /**
     * Gets the value of the dateianhaenge property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dateianhaenge property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDateianhaenge().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DateianhangType }
     * 
     * 
     */
    public List<DateianhangType> getDateianhaenge() {
        if (dateianhaenge == null) {
            dateianhaenge = new ArrayList<DateianhangType>();
        }
        return this.dateianhaenge;
    }

}
