
package at.vvo.omds.types.omds3Types.v1_3_0.on2antrag.kfz;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.v1_3_0.common.VerkaufsproduktType;


/**
 * Typ für ein Kfz-Produktbündel, welches einem Vertrag entspricht
 * 
 * <p>Java-Klasse für VerkaufsproduktKfz_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="VerkaufsproduktKfz_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}Verkaufsprodukt_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="KfzVersicherung" type="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz}ProduktKfz_Type" maxOccurs="unbounded"/&gt;
 *         &lt;element name="KfzZusatzVersicherung" type="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz}ZusatzproduktKfz_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="VersicherteFahrzeuge" type="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.kfz}Fahrzeug_Type" maxOccurs="3"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VerkaufsproduktKfz_Type", propOrder = {
    "kfzVersicherung",
    "kfzZusatzVersicherung",
    "versicherteFahrzeuge"
})
public class VerkaufsproduktKfzType
    extends VerkaufsproduktType
{

    @XmlElement(name = "KfzVersicherung", required = true)
    protected List<ProduktKfzType> kfzVersicherung;
    @XmlElement(name = "KfzZusatzVersicherung")
    protected List<ZusatzproduktKfzType> kfzZusatzVersicherung;
    @XmlElement(name = "VersicherteFahrzeuge", required = true)
    protected List<FahrzeugType> versicherteFahrzeuge;

    /**
     * Gets the value of the kfzVersicherung property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the kfzVersicherung property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getKfzVersicherung().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProduktKfzType }
     * 
     * 
     */
    public List<ProduktKfzType> getKfzVersicherung() {
        if (kfzVersicherung == null) {
            kfzVersicherung = new ArrayList<ProduktKfzType>();
        }
        return this.kfzVersicherung;
    }

    /**
     * Gets the value of the kfzZusatzVersicherung property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the kfzZusatzVersicherung property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getKfzZusatzVersicherung().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ZusatzproduktKfzType }
     * 
     * 
     */
    public List<ZusatzproduktKfzType> getKfzZusatzVersicherung() {
        if (kfzZusatzVersicherung == null) {
            kfzZusatzVersicherung = new ArrayList<ZusatzproduktKfzType>();
        }
        return this.kfzZusatzVersicherung;
    }

    /**
     * Gets the value of the versicherteFahrzeuge property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the versicherteFahrzeuge property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVersicherteFahrzeuge().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FahrzeugType }
     * 
     * 
     */
    public List<FahrzeugType> getVersicherteFahrzeuge() {
        if (versicherteFahrzeuge == null) {
            versicherteFahrzeuge = new ArrayList<FahrzeugType>();
        }
        return this.versicherteFahrzeuge;
    }

}
