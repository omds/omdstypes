
package at.vvo.omds.types.omds3Types.v1_3_0.on2antrag.kfz;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.v1_3_0.common.ObjektIdType;
import at.vvo.omds.types.omds3Types.v1_3_0.common.ProduktType;


/**
 * Abstrakte Basisklasse für KFZ-Zusatzprodukte, die mit einer
 *                 KFZ-Versicherung gebündelt werden können.
 * 
 * <p>Java-Klasse für ZusatzproduktKfz_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ZusatzproduktKfz_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}Produkt_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="VersichertesFahrzeug" type="{urn:omds3CommonServiceTypes-1-1-0}ObjektId_Type" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ZusatzproduktKfz_Type", propOrder = {
    "versichertesFahrzeug"
})
@XmlSeeAlso({
    ProduktKfzRechtsschutzType.class
})
public abstract class ZusatzproduktKfzType
    extends ProduktType
{

    @XmlElement(name = "VersichertesFahrzeug")
    protected ObjektIdType versichertesFahrzeug;

    /**
     * Ruft den Wert der versichertesFahrzeug-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ObjektIdType }
     *     
     */
    public ObjektIdType getVersichertesFahrzeug() {
        return versichertesFahrzeug;
    }

    /**
     * Legt den Wert der versichertesFahrzeug-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjektIdType }
     *     
     */
    public void setVersichertesFahrzeug(ObjektIdType value) {
        this.versichertesFahrzeug = value;
    }

}
