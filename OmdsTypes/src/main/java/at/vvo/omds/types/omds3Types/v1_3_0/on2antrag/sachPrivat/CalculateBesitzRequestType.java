
package at.vvo.omds.types.omds3Types.v1_3_0.on2antrag.sachPrivat;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.v1_3_0.common.CommonRequestType;


/**
 * Typ des Requestobjekts für eine Berechnung Besitz
 * 
 * <p>Java-Klasse für CalculateBesitzRequest_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CalculateBesitzRequest_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}CommonRequest_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Berechnungsanfrage" type="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.sachprivat}SpezBerechnungBesitz_Type"/&gt;
 *         &lt;element name="RequestUpselling" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CalculateBesitzRequest_Type", propOrder = {
    "berechnungsanfrage",
    "requestUpselling"
})
public class CalculateBesitzRequestType
    extends CommonRequestType
{

    @XmlElement(name = "Berechnungsanfrage", required = true)
    protected SpezBerechnungBesitzType berechnungsanfrage;
    @XmlElement(name = "RequestUpselling", defaultValue = "false")
    protected boolean requestUpselling;

    /**
     * Ruft den Wert der berechnungsanfrage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SpezBerechnungBesitzType }
     *     
     */
    public SpezBerechnungBesitzType getBerechnungsanfrage() {
        return berechnungsanfrage;
    }

    /**
     * Legt den Wert der berechnungsanfrage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SpezBerechnungBesitzType }
     *     
     */
    public void setBerechnungsanfrage(SpezBerechnungBesitzType value) {
        this.berechnungsanfrage = value;
    }

    /**
     * Ruft den Wert der requestUpselling-Eigenschaft ab.
     * 
     */
    public boolean isRequestUpselling() {
        return requestUpselling;
    }

    /**
     * Legt den Wert der requestUpselling-Eigenschaft fest.
     * 
     */
    public void setRequestUpselling(boolean value) {
        this.requestUpselling = value;
    }

}
