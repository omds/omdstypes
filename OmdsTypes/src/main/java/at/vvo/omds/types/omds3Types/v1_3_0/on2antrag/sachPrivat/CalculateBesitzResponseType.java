
package at.vvo.omds.types.omds3Types.v1_3_0.on2antrag.sachPrivat;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.v1_3_0.common.CommonResponseType;


/**
 * Typ des Responseobjekts für eine Berechnung Besitz
 * 
 * <p>Java-Klasse für CalculateBesitzResponse_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CalculateBesitzResponse_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}CommonResponse_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Berechnungsantwort" type="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.sachprivat}SpezBerechnungBesitz_Type"/&gt;
 *         &lt;element name="ResponseUpselling" type="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.sachprivat}UpsellingBesitzResponse_Type" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CalculateBesitzResponse_Type", propOrder = {
    "berechnungsantwort",
    "responseUpselling"
})
public class CalculateBesitzResponseType
    extends CommonResponseType
{

    @XmlElement(name = "Berechnungsantwort", required = true)
    protected SpezBerechnungBesitzType berechnungsantwort;
    @XmlElement(name = "ResponseUpselling")
    protected UpsellingBesitzResponseType responseUpselling;

    /**
     * Ruft den Wert der berechnungsantwort-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SpezBerechnungBesitzType }
     *     
     */
    public SpezBerechnungBesitzType getBerechnungsantwort() {
        return berechnungsantwort;
    }

    /**
     * Legt den Wert der berechnungsantwort-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SpezBerechnungBesitzType }
     *     
     */
    public void setBerechnungsantwort(SpezBerechnungBesitzType value) {
        this.berechnungsantwort = value;
    }

    /**
     * Ruft den Wert der responseUpselling-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link UpsellingBesitzResponseType }
     *     
     */
    public UpsellingBesitzResponseType getResponseUpselling() {
        return responseUpselling;
    }

    /**
     * Legt den Wert der responseUpselling-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link UpsellingBesitzResponseType }
     *     
     */
    public void setResponseUpselling(UpsellingBesitzResponseType value) {
        this.responseUpselling = value;
    }

}
