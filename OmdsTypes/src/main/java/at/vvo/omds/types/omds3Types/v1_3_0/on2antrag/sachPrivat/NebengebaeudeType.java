
package at.vvo.omds.types.omds3Types.v1_3_0.on2antrag.sachPrivat;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für Nebengebaeude_Type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="Nebengebaeude_Type"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Abstellgebäude"/&gt;
 *     &lt;enumeration value="Badehütte"/&gt;
 *     &lt;enumeration value="Bootshaus"/&gt;
 *     &lt;enumeration value="Carport"/&gt;
 *     &lt;enumeration value="ehemaliges Wirtschaftsgebäude"/&gt;
 *     &lt;enumeration value="Garage"/&gt;
 *     &lt;enumeration value="Gartenhaus (nicht für Wohnzwecke)"/&gt;
 *     &lt;enumeration value="Holzhütte"/&gt;
 *     &lt;enumeration value="Keller"/&gt;
 *     &lt;enumeration value="Mobilheim (stationär)"/&gt;
 *     &lt;enumeration value="Nebengebäude"/&gt;
 *     &lt;enumeration value="Nebengebäude mit Garage"/&gt;
 *     &lt;enumeration value="Presshaus"/&gt;
 *     &lt;enumeration value="Sauna"/&gt;
 *     &lt;enumeration value="Scheune / Schuppen / Stadel"/&gt;
 *     &lt;enumeration value="Werkstätte"/&gt;
 *     &lt;enumeration value="Werkzeug- und Geräteschuppen"/&gt;
 *     &lt;enumeration value="Wintergarten, Veranda"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "Nebengebaeude_Type")
@XmlEnum
public enum NebengebaeudeType {

    @XmlEnumValue("Abstellgeb\u00e4ude")
    ABSTELLGEBÄUDE("Abstellgeb\u00e4ude"),
    @XmlEnumValue("Badeh\u00fctte")
    BADEHÜTTE("Badeh\u00fctte"),
    @XmlEnumValue("Bootshaus")
    BOOTSHAUS("Bootshaus"),
    @XmlEnumValue("Carport")
    CARPORT("Carport"),
    @XmlEnumValue("ehemaliges Wirtschaftsgeb\u00e4ude")
    EHEMALIGES_WIRTSCHAFTSGEBÄUDE("ehemaliges Wirtschaftsgeb\u00e4ude"),
    @XmlEnumValue("Garage")
    GARAGE("Garage"),
    @XmlEnumValue("Gartenhaus (nicht f\u00fcr Wohnzwecke)")
    GARTENHAUS_NICHT_FÜR_WOHNZWECKE("Gartenhaus (nicht f\u00fcr Wohnzwecke)"),
    @XmlEnumValue("Holzh\u00fctte")
    HOLZHÜTTE("Holzh\u00fctte"),
    @XmlEnumValue("Keller")
    KELLER("Keller"),
    @XmlEnumValue("Mobilheim (station\u00e4r)")
    MOBILHEIM_STATIONÄR("Mobilheim (station\u00e4r)"),
    @XmlEnumValue("Nebengeb\u00e4ude")
    NEBENGEBÄUDE("Nebengeb\u00e4ude"),
    @XmlEnumValue("Nebengeb\u00e4ude mit Garage")
    NEBENGEBÄUDE_MIT_GARAGE("Nebengeb\u00e4ude mit Garage"),
    @XmlEnumValue("Presshaus")
    PRESSHAUS("Presshaus"),
    @XmlEnumValue("Sauna")
    SAUNA("Sauna"),
    @XmlEnumValue("Scheune / Schuppen / Stadel")
    SCHEUNE_SCHUPPEN_STADEL("Scheune / Schuppen / Stadel"),
    @XmlEnumValue("Werkst\u00e4tte")
    WERKSTÄTTE("Werkst\u00e4tte"),
    @XmlEnumValue("Werkzeug- und Ger\u00e4teschuppen")
    WERKZEUG_UND_GERÄTESCHUPPEN("Werkzeug- und Ger\u00e4teschuppen"),
    @XmlEnumValue("Wintergarten, Veranda")
    WINTERGARTEN_VERANDA("Wintergarten, Veranda");
    private final String value;

    NebengebaeudeType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static NebengebaeudeType fromValue(String v) {
        for (NebengebaeudeType c: NebengebaeudeType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
