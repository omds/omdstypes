
package at.vvo.omds.types.omds3Types.v1_3_0.on2antrag.sachPrivat;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds2Types.v2_9.ADRESSEType;
import at.vvo.omds.types.omds3Types.v1_3_0.common.ProduktType;


/**
 * Typ für ein Besitz-Produkt, welches einer Vertragssparte entspricht
 * 
 * <p>Java-Klasse für ProduktBesitz_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ProduktBesitz_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}Produkt_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="RisikoAdresse" type="{urn:omds20}ADRESSE_Type"/&gt;
 *         &lt;element name="RisikoEigenheim" type="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.sachprivat}RisikoEigenheim_Type"/&gt;
 *         &lt;element name="Tarifierung" type="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.sachprivat}TarifierungsdatenBesitz_Type"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProduktBesitz_Type", propOrder = {
    "risikoAdresse",
    "risikoEigenheim",
    "tarifierung"
})
public class ProduktBesitzType
    extends ProduktType
{

    @XmlElement(name = "RisikoAdresse", required = true)
    protected ADRESSEType risikoAdresse;
    @XmlElement(name = "RisikoEigenheim", required = true)
    protected RisikoEigenheimType risikoEigenheim;
    @XmlElement(name = "Tarifierung", required = true)
    protected TarifierungsdatenBesitzType tarifierung;

    /**
     * Ruft den Wert der risikoAdresse-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ADRESSEType }
     *     
     */
    public ADRESSEType getRisikoAdresse() {
        return risikoAdresse;
    }

    /**
     * Legt den Wert der risikoAdresse-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ADRESSEType }
     *     
     */
    public void setRisikoAdresse(ADRESSEType value) {
        this.risikoAdresse = value;
    }

    /**
     * Ruft den Wert der risikoEigenheim-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link RisikoEigenheimType }
     *     
     */
    public RisikoEigenheimType getRisikoEigenheim() {
        return risikoEigenheim;
    }

    /**
     * Legt den Wert der risikoEigenheim-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link RisikoEigenheimType }
     *     
     */
    public void setRisikoEigenheim(RisikoEigenheimType value) {
        this.risikoEigenheim = value;
    }

    /**
     * Ruft den Wert der tarifierung-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TarifierungsdatenBesitzType }
     *     
     */
    public TarifierungsdatenBesitzType getTarifierung() {
        return tarifierung;
    }

    /**
     * Legt den Wert der tarifierung-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TarifierungsdatenBesitzType }
     *     
     */
    public void setTarifierung(TarifierungsdatenBesitzType value) {
        this.tarifierung = value;
    }

}
