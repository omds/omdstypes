
package at.vvo.omds.types.omds3Types.v1_3_0.on2antrag.sachPrivat;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für RisikoEigenheim_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="RisikoEigenheim_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="BaujahrGebaeude" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="VerbauteFlaecheEG" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="VerbauteFlaecheOG" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="VerbauteFlaecheKeller" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="VerbauteFlaecheMansarde" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="Nebengebaeude" type="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.sachprivat}Nebengebaeude_Type" minOccurs="0"/&gt;
 *         &lt;element name="VerbauteFlaecheNebengebaeude" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="Vorschaeden" type="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.sachprivat}RisikoVorschaeden_Type"/&gt;
 *         &lt;element name="WenigerAls270TageBewohnt" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RisikoEigenheim_Type", propOrder = {
    "baujahrGebaeude",
    "verbauteFlaecheEG",
    "verbauteFlaecheOG",
    "verbauteFlaecheKeller",
    "verbauteFlaecheMansarde",
    "nebengebaeude",
    "verbauteFlaecheNebengebaeude",
    "vorschaeden",
    "wenigerAls270TageBewohnt"
})
public class RisikoEigenheimType {

    @XmlElement(name = "BaujahrGebaeude", required = true)
    protected BigInteger baujahrGebaeude;
    @XmlElement(name = "VerbauteFlaecheEG", required = true)
    protected BigInteger verbauteFlaecheEG;
    @XmlElement(name = "VerbauteFlaecheOG")
    protected BigInteger verbauteFlaecheOG;
    @XmlElement(name = "VerbauteFlaecheKeller")
    protected BigInteger verbauteFlaecheKeller;
    @XmlElement(name = "VerbauteFlaecheMansarde")
    protected BigInteger verbauteFlaecheMansarde;
    @XmlElement(name = "Nebengebaeude")
    @XmlSchemaType(name = "string")
    protected NebengebaeudeType nebengebaeude;
    @XmlElement(name = "VerbauteFlaecheNebengebaeude")
    protected BigInteger verbauteFlaecheNebengebaeude;
    @XmlElement(name = "Vorschaeden", required = true)
    @XmlSchemaType(name = "string")
    protected RisikoVorschaedenType vorschaeden;
    @XmlElement(name = "WenigerAls270TageBewohnt")
    protected Boolean wenigerAls270TageBewohnt;

    /**
     * Ruft den Wert der baujahrGebaeude-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getBaujahrGebaeude() {
        return baujahrGebaeude;
    }

    /**
     * Legt den Wert der baujahrGebaeude-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setBaujahrGebaeude(BigInteger value) {
        this.baujahrGebaeude = value;
    }

    /**
     * Ruft den Wert der verbauteFlaecheEG-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getVerbauteFlaecheEG() {
        return verbauteFlaecheEG;
    }

    /**
     * Legt den Wert der verbauteFlaecheEG-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setVerbauteFlaecheEG(BigInteger value) {
        this.verbauteFlaecheEG = value;
    }

    /**
     * Ruft den Wert der verbauteFlaecheOG-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getVerbauteFlaecheOG() {
        return verbauteFlaecheOG;
    }

    /**
     * Legt den Wert der verbauteFlaecheOG-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setVerbauteFlaecheOG(BigInteger value) {
        this.verbauteFlaecheOG = value;
    }

    /**
     * Ruft den Wert der verbauteFlaecheKeller-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getVerbauteFlaecheKeller() {
        return verbauteFlaecheKeller;
    }

    /**
     * Legt den Wert der verbauteFlaecheKeller-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setVerbauteFlaecheKeller(BigInteger value) {
        this.verbauteFlaecheKeller = value;
    }

    /**
     * Ruft den Wert der verbauteFlaecheMansarde-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getVerbauteFlaecheMansarde() {
        return verbauteFlaecheMansarde;
    }

    /**
     * Legt den Wert der verbauteFlaecheMansarde-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setVerbauteFlaecheMansarde(BigInteger value) {
        this.verbauteFlaecheMansarde = value;
    }

    /**
     * Ruft den Wert der nebengebaeude-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link NebengebaeudeType }
     *     
     */
    public NebengebaeudeType getNebengebaeude() {
        return nebengebaeude;
    }

    /**
     * Legt den Wert der nebengebaeude-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link NebengebaeudeType }
     *     
     */
    public void setNebengebaeude(NebengebaeudeType value) {
        this.nebengebaeude = value;
    }

    /**
     * Ruft den Wert der verbauteFlaecheNebengebaeude-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getVerbauteFlaecheNebengebaeude() {
        return verbauteFlaecheNebengebaeude;
    }

    /**
     * Legt den Wert der verbauteFlaecheNebengebaeude-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setVerbauteFlaecheNebengebaeude(BigInteger value) {
        this.verbauteFlaecheNebengebaeude = value;
    }

    /**
     * Ruft den Wert der vorschaeden-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link RisikoVorschaedenType }
     *     
     */
    public RisikoVorschaedenType getVorschaeden() {
        return vorschaeden;
    }

    /**
     * Legt den Wert der vorschaeden-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link RisikoVorschaedenType }
     *     
     */
    public void setVorschaeden(RisikoVorschaedenType value) {
        this.vorschaeden = value;
    }

    /**
     * Ruft den Wert der wenigerAls270TageBewohnt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isWenigerAls270TageBewohnt() {
        return wenigerAls270TageBewohnt;
    }

    /**
     * Legt den Wert der wenigerAls270TageBewohnt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setWenigerAls270TageBewohnt(Boolean value) {
        this.wenigerAls270TageBewohnt = value;
    }

}
