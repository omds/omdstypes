
package at.vvo.omds.types.omds3Types.v1_3_0.on2antrag.sachPrivat;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.v1_3_0.on2antrag.common.SpezBerechnungType;


/**
 * Typ der das Produkt beschreibt und in Berechnungsanfrage und Berechnungsantwort verwendet wird
 * 
 * <p>Java-Klasse für SpezBerechnungBesitz_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SpezBerechnungBesitz_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.common}SpezBerechnung_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Verkaufsprodukt" type="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.sachprivat}VerkaufsproduktBesitz_Type"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SpezBerechnungBesitz_Type", propOrder = {
    "verkaufsprodukt"
})
public class SpezBerechnungBesitzType
    extends SpezBerechnungType
{

    @XmlElement(name = "Verkaufsprodukt", required = true)
    protected VerkaufsproduktBesitzType verkaufsprodukt;

    /**
     * Ruft den Wert der verkaufsprodukt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VerkaufsproduktBesitzType }
     *     
     */
    public VerkaufsproduktBesitzType getVerkaufsprodukt() {
        return verkaufsprodukt;
    }

    /**
     * Legt den Wert der verkaufsprodukt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VerkaufsproduktBesitzType }
     *     
     */
    public void setVerkaufsprodukt(VerkaufsproduktBesitzType value) {
        this.verkaufsprodukt = value;
    }

}
