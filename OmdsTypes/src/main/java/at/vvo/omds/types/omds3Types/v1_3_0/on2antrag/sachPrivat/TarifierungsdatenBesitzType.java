
package at.vvo.omds.types.omds3Types.v1_3_0.on2antrag.sachPrivat;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import at.vvo.omds.types.omds3Types.v1_3_0.common.DeckungProzentType;
import at.vvo.omds.types.omds3Types.v1_3_0.common.DeckungVsType;
import at.vvo.omds.types.omds3Types.v1_3_0.common.VarianteType;


/**
 * <p>Java-Klasse für TarifierungsdatenBesitz_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="TarifierungsdatenBesitz_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="beginn" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="hauptfaelligkeit" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="versicherungsEnde" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="Zahlrhythmus" type="{urn:omds20}ZahlRhythmCd_Type"/&gt;
 *         &lt;element name="variante" type="{urn:omds3CommonServiceTypes-1-1-0}Variante_Type" minOccurs="0"/&gt;
 *         &lt;element name="sonderrabatt" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="sonderrabattRs" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="deckungFeuer" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="deckungLeitungswasser" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="deckungElementar" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="deckungHochwasser" type="{urn:omds3CommonServiceTypes-1-1-0}DeckungVs_Type" minOccurs="0"/&gt;
 *         &lt;element name="deckungNiederschlag" type="{urn:omds3CommonServiceTypes-1-1-0}DeckungVs_Type" minOccurs="0"/&gt;
 *         &lt;element name="deckungErdbeben" type="{urn:omds3CommonServiceTypes-1-1-0}DeckungVs_Type" minOccurs="0"/&gt;
 *         &lt;element name="deckungHaushalt" type="{urn:omds3CommonServiceTypes-1-1-0}DeckungVs_Type" minOccurs="0"/&gt;
 *         &lt;element name="deckungGlasbruch" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="deckungHaftpflicht" type="{urn:omds3CommonServiceTypes-1-1-0}DeckungVs_Type" minOccurs="0"/&gt;
 *         &lt;element name="deckungRechtsschutz" type="{urn:omds3CommonServiceTypes-1-1-0}DeckungVs_Type" minOccurs="0"/&gt;
 *         &lt;element name="deckungGrobeFahrlaessigkeit" type="{urn:omds3CommonServiceTypes-1-1-0}DeckungProzent_Type" minOccurs="0"/&gt;
 *         &lt;element name="deckungTipUndTat" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="deckungSonderverglasung" type="{urn:omds3CommonServiceTypes-1-1-0}DeckungVs_Type" minOccurs="0"/&gt;
 *         &lt;element name="deckungSchwimmbecken" type="{urn:omds3CommonServiceTypes-1-1-0}DeckungVs_Type" minOccurs="0"/&gt;
 *         &lt;element name="deckungHeizungsanlagen" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="deckungEZusatz" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="deckungFahrzeugeRs" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TarifierungsdatenBesitz_Type", propOrder = {
    "beginn",
    "hauptfaelligkeit",
    "versicherungsEnde",
    "zahlrhythmus",
    "variante",
    "sonderrabatt",
    "sonderrabattRs",
    "deckungFeuer",
    "deckungLeitungswasser",
    "deckungElementar",
    "deckungHochwasser",
    "deckungNiederschlag",
    "deckungErdbeben",
    "deckungHaushalt",
    "deckungGlasbruch",
    "deckungHaftpflicht",
    "deckungRechtsschutz",
    "deckungGrobeFahrlaessigkeit",
    "deckungTipUndTat",
    "deckungSonderverglasung",
    "deckungSchwimmbecken",
    "deckungHeizungsanlagen",
    "deckungEZusatz",
    "deckungFahrzeugeRs"
})
public class TarifierungsdatenBesitzType {

    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar beginn;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar hauptfaelligkeit;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar versicherungsEnde;
    @XmlElement(name = "Zahlrhythmus", required = true)
    protected String zahlrhythmus;
    @XmlSchemaType(name = "string")
    protected VarianteType variante;
    protected BigInteger sonderrabatt;
    protected BigInteger sonderrabattRs;
    protected Boolean deckungFeuer;
    protected Boolean deckungLeitungswasser;
    protected Boolean deckungElementar;
    protected DeckungVsType deckungHochwasser;
    protected DeckungVsType deckungNiederschlag;
    protected DeckungVsType deckungErdbeben;
    protected DeckungVsType deckungHaushalt;
    protected Boolean deckungGlasbruch;
    protected DeckungVsType deckungHaftpflicht;
    protected DeckungVsType deckungRechtsschutz;
    protected DeckungProzentType deckungGrobeFahrlaessigkeit;
    protected Boolean deckungTipUndTat;
    protected DeckungVsType deckungSonderverglasung;
    protected DeckungVsType deckungSchwimmbecken;
    protected Boolean deckungHeizungsanlagen;
    protected Boolean deckungEZusatz;
    protected Boolean deckungFahrzeugeRs;

    /**
     * Ruft den Wert der beginn-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getBeginn() {
        return beginn;
    }

    /**
     * Legt den Wert der beginn-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setBeginn(XMLGregorianCalendar value) {
        this.beginn = value;
    }

    /**
     * Ruft den Wert der hauptfaelligkeit-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getHauptfaelligkeit() {
        return hauptfaelligkeit;
    }

    /**
     * Legt den Wert der hauptfaelligkeit-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setHauptfaelligkeit(XMLGregorianCalendar value) {
        this.hauptfaelligkeit = value;
    }

    /**
     * Ruft den Wert der versicherungsEnde-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getVersicherungsEnde() {
        return versicherungsEnde;
    }

    /**
     * Legt den Wert der versicherungsEnde-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setVersicherungsEnde(XMLGregorianCalendar value) {
        this.versicherungsEnde = value;
    }

    /**
     * Ruft den Wert der zahlrhythmus-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZahlrhythmus() {
        return zahlrhythmus;
    }

    /**
     * Legt den Wert der zahlrhythmus-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZahlrhythmus(String value) {
        this.zahlrhythmus = value;
    }

    /**
     * Ruft den Wert der variante-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VarianteType }
     *     
     */
    public VarianteType getVariante() {
        return variante;
    }

    /**
     * Legt den Wert der variante-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VarianteType }
     *     
     */
    public void setVariante(VarianteType value) {
        this.variante = value;
    }

    /**
     * Ruft den Wert der sonderrabatt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSonderrabatt() {
        return sonderrabatt;
    }

    /**
     * Legt den Wert der sonderrabatt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSonderrabatt(BigInteger value) {
        this.sonderrabatt = value;
    }

    /**
     * Ruft den Wert der sonderrabattRs-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSonderrabattRs() {
        return sonderrabattRs;
    }

    /**
     * Legt den Wert der sonderrabattRs-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSonderrabattRs(BigInteger value) {
        this.sonderrabattRs = value;
    }

    /**
     * Ruft den Wert der deckungFeuer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDeckungFeuer() {
        return deckungFeuer;
    }

    /**
     * Legt den Wert der deckungFeuer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDeckungFeuer(Boolean value) {
        this.deckungFeuer = value;
    }

    /**
     * Ruft den Wert der deckungLeitungswasser-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDeckungLeitungswasser() {
        return deckungLeitungswasser;
    }

    /**
     * Legt den Wert der deckungLeitungswasser-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDeckungLeitungswasser(Boolean value) {
        this.deckungLeitungswasser = value;
    }

    /**
     * Ruft den Wert der deckungElementar-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDeckungElementar() {
        return deckungElementar;
    }

    /**
     * Legt den Wert der deckungElementar-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDeckungElementar(Boolean value) {
        this.deckungElementar = value;
    }

    /**
     * Ruft den Wert der deckungHochwasser-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DeckungVsType }
     *     
     */
    public DeckungVsType getDeckungHochwasser() {
        return deckungHochwasser;
    }

    /**
     * Legt den Wert der deckungHochwasser-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DeckungVsType }
     *     
     */
    public void setDeckungHochwasser(DeckungVsType value) {
        this.deckungHochwasser = value;
    }

    /**
     * Ruft den Wert der deckungNiederschlag-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DeckungVsType }
     *     
     */
    public DeckungVsType getDeckungNiederschlag() {
        return deckungNiederschlag;
    }

    /**
     * Legt den Wert der deckungNiederschlag-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DeckungVsType }
     *     
     */
    public void setDeckungNiederschlag(DeckungVsType value) {
        this.deckungNiederschlag = value;
    }

    /**
     * Ruft den Wert der deckungErdbeben-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DeckungVsType }
     *     
     */
    public DeckungVsType getDeckungErdbeben() {
        return deckungErdbeben;
    }

    /**
     * Legt den Wert der deckungErdbeben-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DeckungVsType }
     *     
     */
    public void setDeckungErdbeben(DeckungVsType value) {
        this.deckungErdbeben = value;
    }

    /**
     * Ruft den Wert der deckungHaushalt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DeckungVsType }
     *     
     */
    public DeckungVsType getDeckungHaushalt() {
        return deckungHaushalt;
    }

    /**
     * Legt den Wert der deckungHaushalt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DeckungVsType }
     *     
     */
    public void setDeckungHaushalt(DeckungVsType value) {
        this.deckungHaushalt = value;
    }

    /**
     * Ruft den Wert der deckungGlasbruch-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDeckungGlasbruch() {
        return deckungGlasbruch;
    }

    /**
     * Legt den Wert der deckungGlasbruch-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDeckungGlasbruch(Boolean value) {
        this.deckungGlasbruch = value;
    }

    /**
     * Ruft den Wert der deckungHaftpflicht-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DeckungVsType }
     *     
     */
    public DeckungVsType getDeckungHaftpflicht() {
        return deckungHaftpflicht;
    }

    /**
     * Legt den Wert der deckungHaftpflicht-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DeckungVsType }
     *     
     */
    public void setDeckungHaftpflicht(DeckungVsType value) {
        this.deckungHaftpflicht = value;
    }

    /**
     * Ruft den Wert der deckungRechtsschutz-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DeckungVsType }
     *     
     */
    public DeckungVsType getDeckungRechtsschutz() {
        return deckungRechtsschutz;
    }

    /**
     * Legt den Wert der deckungRechtsschutz-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DeckungVsType }
     *     
     */
    public void setDeckungRechtsschutz(DeckungVsType value) {
        this.deckungRechtsschutz = value;
    }

    /**
     * Ruft den Wert der deckungGrobeFahrlaessigkeit-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DeckungProzentType }
     *     
     */
    public DeckungProzentType getDeckungGrobeFahrlaessigkeit() {
        return deckungGrobeFahrlaessigkeit;
    }

    /**
     * Legt den Wert der deckungGrobeFahrlaessigkeit-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DeckungProzentType }
     *     
     */
    public void setDeckungGrobeFahrlaessigkeit(DeckungProzentType value) {
        this.deckungGrobeFahrlaessigkeit = value;
    }

    /**
     * Ruft den Wert der deckungTipUndTat-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDeckungTipUndTat() {
        return deckungTipUndTat;
    }

    /**
     * Legt den Wert der deckungTipUndTat-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDeckungTipUndTat(Boolean value) {
        this.deckungTipUndTat = value;
    }

    /**
     * Ruft den Wert der deckungSonderverglasung-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DeckungVsType }
     *     
     */
    public DeckungVsType getDeckungSonderverglasung() {
        return deckungSonderverglasung;
    }

    /**
     * Legt den Wert der deckungSonderverglasung-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DeckungVsType }
     *     
     */
    public void setDeckungSonderverglasung(DeckungVsType value) {
        this.deckungSonderverglasung = value;
    }

    /**
     * Ruft den Wert der deckungSchwimmbecken-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DeckungVsType }
     *     
     */
    public DeckungVsType getDeckungSchwimmbecken() {
        return deckungSchwimmbecken;
    }

    /**
     * Legt den Wert der deckungSchwimmbecken-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DeckungVsType }
     *     
     */
    public void setDeckungSchwimmbecken(DeckungVsType value) {
        this.deckungSchwimmbecken = value;
    }

    /**
     * Ruft den Wert der deckungHeizungsanlagen-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDeckungHeizungsanlagen() {
        return deckungHeizungsanlagen;
    }

    /**
     * Legt den Wert der deckungHeizungsanlagen-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDeckungHeizungsanlagen(Boolean value) {
        this.deckungHeizungsanlagen = value;
    }

    /**
     * Ruft den Wert der deckungEZusatz-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDeckungEZusatz() {
        return deckungEZusatz;
    }

    /**
     * Legt den Wert der deckungEZusatz-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDeckungEZusatz(Boolean value) {
        this.deckungEZusatz = value;
    }

    /**
     * Ruft den Wert der deckungFahrzeugeRs-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDeckungFahrzeugeRs() {
        return deckungFahrzeugeRs;
    }

    /**
     * Legt den Wert der deckungFahrzeugeRs-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDeckungFahrzeugeRs(Boolean value) {
        this.deckungFahrzeugeRs = value;
    }

}
