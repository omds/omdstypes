
package at.vvo.omds.types.omds3Types.v1_3_0.on2antrag.sachPrivat;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.v1_3_0.common.VerkaufsproduktType;


/**
 * Typ für ein Besitz-Produktbündel, welches einem Vertrag entspricht
 * 
 * <p>Java-Klasse für VerkaufsproduktBesitz_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="VerkaufsproduktBesitz_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}Verkaufsprodukt_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="BesitzVersicherung" type="{urn:at.vvo.omds.types.omds3types.v1-3-0.on2antrag.sachprivat}ProduktBesitz_Type"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VerkaufsproduktBesitz_Type", propOrder = {
    "besitzVersicherung"
})
public class VerkaufsproduktBesitzType
    extends VerkaufsproduktType
{

    @XmlElement(name = "BesitzVersicherung", required = true)
    protected ProduktBesitzType besitzVersicherung;

    /**
     * Ruft den Wert der besitzVersicherung-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ProduktBesitzType }
     *     
     */
    public ProduktBesitzType getBesitzVersicherung() {
        return besitzVersicherung;
    }

    /**
     * Legt den Wert der besitzVersicherung-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ProduktBesitzType }
     *     
     */
    public void setBesitzVersicherung(ProduktBesitzType value) {
        this.besitzVersicherung = value;
    }

}
