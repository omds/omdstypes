
package at.vvo.omds.types.omds3Types.v1_3_0.on3vertrag;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import at.vvo.omds.types.omds2Types.v2_9.ADRESSEType;
import at.vvo.omds.types.omds3Types.v1_3_0.common.BankverbindungType;
import at.vvo.omds.types.omds3Types.v1_3_0.common.CommonRequestType;
import at.vvo.omds.types.omds3Types.v1_3_0.common.DateianhangType;


/**
 * Typ des Requestobjekts für eine Änderung von Inkassodaten
 * 
 * <p>Java-Klasse für CollectionChangeRequest_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CollectionChangeRequest_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}CommonRequest_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="WirksamtkeitAb" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *         &lt;element name="Zahlrhythmus" type="{urn:omds20}ZahlRhythmCd_Type"/&gt;
 *         &lt;element name="Zahlweg"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;choice&gt;
 *                   &lt;element name="Zahlungsanweisung"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="Inkassoadresse" type="{urn:omds20}ADRESSE_Type"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="BankverbindungAbbuchung" type="{urn:omds3CommonServiceTypes-1-1-0}Bankverbindung_Type"/&gt;
 *                   &lt;element name="Kundenkonto"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;attribute name="Kundenkontonummer" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="Kreditkarte"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;attribute name="Gesellschaft" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                           &lt;attribute name="Kartennummer" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                           &lt;attribute name="Inhaber" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                           &lt;attribute name="Pruefziffer" use="required"&gt;
 *                             &lt;simpleType&gt;
 *                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt"&gt;
 *                                 &lt;totalDigits value="3"/&gt;
 *                               &lt;/restriction&gt;
 *                             &lt;/simpleType&gt;
 *                           &lt;/attribute&gt;
 *                           &lt;attribute name="AblaufMonat" use="required"&gt;
 *                             &lt;simpleType&gt;
 *                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte"&gt;
 *                                 &lt;totalDigits value="2"/&gt;
 *                               &lt;/restriction&gt;
 *                             &lt;/simpleType&gt;
 *                           &lt;/attribute&gt;
 *                           &lt;attribute name="AblaufJahr" use="required"&gt;
 *                             &lt;simpleType&gt;
 *                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte"&gt;
 *                                 &lt;totalDigits value="2"/&gt;
 *                               &lt;/restriction&gt;
 *                             &lt;/simpleType&gt;
 *                           &lt;/attribute&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/choice&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Dateianhaenge" type="{urn:omds3CommonServiceTypes-1-1-0}Dateianhang_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="Polizzennr" use="required" type="{urn:omds20}Polizzennr" /&gt;
 *       &lt;attribute name="VertragsID" type="{urn:omds20}VertragsID" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CollectionChangeRequest_Type", propOrder = {
    "wirksamtkeitAb",
    "zahlrhythmus",
    "zahlweg",
    "dateianhaenge"
})
public class CollectionChangeRequestType
    extends CommonRequestType
{

    @XmlElement(name = "WirksamtkeitAb", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar wirksamtkeitAb;
    @XmlElement(name = "Zahlrhythmus", required = true)
    protected String zahlrhythmus;
    @XmlElement(name = "Zahlweg", required = true)
    protected CollectionChangeRequestType.Zahlweg zahlweg;
    @XmlElement(name = "Dateianhaenge")
    protected List<DateianhangType> dateianhaenge;
    @XmlAttribute(name = "Polizzennr", namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on3vertrag", required = true)
    protected String polizzennr;
    @XmlAttribute(name = "VertragsID", namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on3vertrag")
    protected String vertragsID;

    /**
     * Ruft den Wert der wirksamtkeitAb-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getWirksamtkeitAb() {
        return wirksamtkeitAb;
    }

    /**
     * Legt den Wert der wirksamtkeitAb-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setWirksamtkeitAb(XMLGregorianCalendar value) {
        this.wirksamtkeitAb = value;
    }

    /**
     * Ruft den Wert der zahlrhythmus-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZahlrhythmus() {
        return zahlrhythmus;
    }

    /**
     * Legt den Wert der zahlrhythmus-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZahlrhythmus(String value) {
        this.zahlrhythmus = value;
    }

    /**
     * Ruft den Wert der zahlweg-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CollectionChangeRequestType.Zahlweg }
     *     
     */
    public CollectionChangeRequestType.Zahlweg getZahlweg() {
        return zahlweg;
    }

    /**
     * Legt den Wert der zahlweg-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CollectionChangeRequestType.Zahlweg }
     *     
     */
    public void setZahlweg(CollectionChangeRequestType.Zahlweg value) {
        this.zahlweg = value;
    }

    /**
     * Gets the value of the dateianhaenge property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dateianhaenge property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDateianhaenge().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DateianhangType }
     * 
     * 
     */
    public List<DateianhangType> getDateianhaenge() {
        if (dateianhaenge == null) {
            dateianhaenge = new ArrayList<DateianhangType>();
        }
        return this.dateianhaenge;
    }

    /**
     * Ruft den Wert der polizzennr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolizzennr() {
        return polizzennr;
    }

    /**
     * Legt den Wert der polizzennr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolizzennr(String value) {
        this.polizzennr = value;
    }

    /**
     * Ruft den Wert der vertragsID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVertragsID() {
        return vertragsID;
    }

    /**
     * Legt den Wert der vertragsID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVertragsID(String value) {
        this.vertragsID = value;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;choice&gt;
     *         &lt;element name="Zahlungsanweisung"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="Inkassoadresse" type="{urn:omds20}ADRESSE_Type"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="BankverbindungAbbuchung" type="{urn:omds3CommonServiceTypes-1-1-0}Bankverbindung_Type"/&gt;
     *         &lt;element name="Kundenkonto"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;attribute name="Kundenkontonummer" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="Kreditkarte"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;attribute name="Gesellschaft" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                 &lt;attribute name="Kartennummer" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                 &lt;attribute name="Inhaber" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                 &lt;attribute name="Pruefziffer" use="required"&gt;
     *                   &lt;simpleType&gt;
     *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt"&gt;
     *                       &lt;totalDigits value="3"/&gt;
     *                     &lt;/restriction&gt;
     *                   &lt;/simpleType&gt;
     *                 &lt;/attribute&gt;
     *                 &lt;attribute name="AblaufMonat" use="required"&gt;
     *                   &lt;simpleType&gt;
     *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte"&gt;
     *                       &lt;totalDigits value="2"/&gt;
     *                     &lt;/restriction&gt;
     *                   &lt;/simpleType&gt;
     *                 &lt;/attribute&gt;
     *                 &lt;attribute name="AblaufJahr" use="required"&gt;
     *                   &lt;simpleType&gt;
     *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte"&gt;
     *                       &lt;totalDigits value="2"/&gt;
     *                     &lt;/restriction&gt;
     *                   &lt;/simpleType&gt;
     *                 &lt;/attribute&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/choice&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "zahlungsanweisung",
        "bankverbindungAbbuchung",
        "kundenkonto",
        "kreditkarte"
    })
    public static class Zahlweg {

        @XmlElement(name = "Zahlungsanweisung")
        protected CollectionChangeRequestType.Zahlweg.Zahlungsanweisung zahlungsanweisung;
        @XmlElement(name = "BankverbindungAbbuchung")
        protected BankverbindungType bankverbindungAbbuchung;
        @XmlElement(name = "Kundenkonto")
        protected CollectionChangeRequestType.Zahlweg.Kundenkonto kundenkonto;
        @XmlElement(name = "Kreditkarte")
        protected CollectionChangeRequestType.Zahlweg.Kreditkarte kreditkarte;

        /**
         * Ruft den Wert der zahlungsanweisung-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link CollectionChangeRequestType.Zahlweg.Zahlungsanweisung }
         *     
         */
        public CollectionChangeRequestType.Zahlweg.Zahlungsanweisung getZahlungsanweisung() {
            return zahlungsanweisung;
        }

        /**
         * Legt den Wert der zahlungsanweisung-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link CollectionChangeRequestType.Zahlweg.Zahlungsanweisung }
         *     
         */
        public void setZahlungsanweisung(CollectionChangeRequestType.Zahlweg.Zahlungsanweisung value) {
            this.zahlungsanweisung = value;
        }

        /**
         * Ruft den Wert der bankverbindungAbbuchung-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link BankverbindungType }
         *     
         */
        public BankverbindungType getBankverbindungAbbuchung() {
            return bankverbindungAbbuchung;
        }

        /**
         * Legt den Wert der bankverbindungAbbuchung-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link BankverbindungType }
         *     
         */
        public void setBankverbindungAbbuchung(BankverbindungType value) {
            this.bankverbindungAbbuchung = value;
        }

        /**
         * Ruft den Wert der kundenkonto-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link CollectionChangeRequestType.Zahlweg.Kundenkonto }
         *     
         */
        public CollectionChangeRequestType.Zahlweg.Kundenkonto getKundenkonto() {
            return kundenkonto;
        }

        /**
         * Legt den Wert der kundenkonto-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link CollectionChangeRequestType.Zahlweg.Kundenkonto }
         *     
         */
        public void setKundenkonto(CollectionChangeRequestType.Zahlweg.Kundenkonto value) {
            this.kundenkonto = value;
        }

        /**
         * Ruft den Wert der kreditkarte-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link CollectionChangeRequestType.Zahlweg.Kreditkarte }
         *     
         */
        public CollectionChangeRequestType.Zahlweg.Kreditkarte getKreditkarte() {
            return kreditkarte;
        }

        /**
         * Legt den Wert der kreditkarte-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link CollectionChangeRequestType.Zahlweg.Kreditkarte }
         *     
         */
        public void setKreditkarte(CollectionChangeRequestType.Zahlweg.Kreditkarte value) {
            this.kreditkarte = value;
        }


        /**
         * <p>Java-Klasse für anonymous complex type.
         * 
         * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;attribute name="Gesellschaft" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *       &lt;attribute name="Kartennummer" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *       &lt;attribute name="Inhaber" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *       &lt;attribute name="Pruefziffer" use="required"&gt;
         *         &lt;simpleType&gt;
         *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedInt"&gt;
         *             &lt;totalDigits value="3"/&gt;
         *           &lt;/restriction&gt;
         *         &lt;/simpleType&gt;
         *       &lt;/attribute&gt;
         *       &lt;attribute name="AblaufMonat" use="required"&gt;
         *         &lt;simpleType&gt;
         *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte"&gt;
         *             &lt;totalDigits value="2"/&gt;
         *           &lt;/restriction&gt;
         *         &lt;/simpleType&gt;
         *       &lt;/attribute&gt;
         *       &lt;attribute name="AblaufJahr" use="required"&gt;
         *         &lt;simpleType&gt;
         *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte"&gt;
         *             &lt;totalDigits value="2"/&gt;
         *           &lt;/restriction&gt;
         *         &lt;/simpleType&gt;
         *       &lt;/attribute&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class Kreditkarte {

            @XmlAttribute(name = "Gesellschaft", namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on3vertrag", required = true)
            protected String gesellschaft;
            @XmlAttribute(name = "Kartennummer", namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on3vertrag", required = true)
            protected String kartennummer;
            @XmlAttribute(name = "Inhaber", namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on3vertrag", required = true)
            protected String inhaber;
            @XmlAttribute(name = "Pruefziffer", namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on3vertrag", required = true)
            protected long pruefziffer;
            @XmlAttribute(name = "AblaufMonat", namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on3vertrag", required = true)
            protected short ablaufMonat;
            @XmlAttribute(name = "AblaufJahr", namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on3vertrag", required = true)
            protected short ablaufJahr;

            /**
             * Ruft den Wert der gesellschaft-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getGesellschaft() {
                return gesellschaft;
            }

            /**
             * Legt den Wert der gesellschaft-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setGesellschaft(String value) {
                this.gesellschaft = value;
            }

            /**
             * Ruft den Wert der kartennummer-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getKartennummer() {
                return kartennummer;
            }

            /**
             * Legt den Wert der kartennummer-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setKartennummer(String value) {
                this.kartennummer = value;
            }

            /**
             * Ruft den Wert der inhaber-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getInhaber() {
                return inhaber;
            }

            /**
             * Legt den Wert der inhaber-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setInhaber(String value) {
                this.inhaber = value;
            }

            /**
             * Ruft den Wert der pruefziffer-Eigenschaft ab.
             * 
             */
            public long getPruefziffer() {
                return pruefziffer;
            }

            /**
             * Legt den Wert der pruefziffer-Eigenschaft fest.
             * 
             */
            public void setPruefziffer(long value) {
                this.pruefziffer = value;
            }

            /**
             * Ruft den Wert der ablaufMonat-Eigenschaft ab.
             * 
             */
            public short getAblaufMonat() {
                return ablaufMonat;
            }

            /**
             * Legt den Wert der ablaufMonat-Eigenschaft fest.
             * 
             */
            public void setAblaufMonat(short value) {
                this.ablaufMonat = value;
            }

            /**
             * Ruft den Wert der ablaufJahr-Eigenschaft ab.
             * 
             */
            public short getAblaufJahr() {
                return ablaufJahr;
            }

            /**
             * Legt den Wert der ablaufJahr-Eigenschaft fest.
             * 
             */
            public void setAblaufJahr(short value) {
                this.ablaufJahr = value;
            }

        }


        /**
         * <p>Java-Klasse für anonymous complex type.
         * 
         * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;attribute name="Kundenkontonummer" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class Kundenkonto {

            @XmlAttribute(name = "Kundenkontonummer", namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on3vertrag", required = true)
            protected String kundenkontonummer;

            /**
             * Ruft den Wert der kundenkontonummer-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getKundenkontonummer() {
                return kundenkontonummer;
            }

            /**
             * Legt den Wert der kundenkontonummer-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setKundenkontonummer(String value) {
                this.kundenkontonummer = value;
            }

        }


        /**
         * <p>Java-Klasse für anonymous complex type.
         * 
         * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="Inkassoadresse" type="{urn:omds20}ADRESSE_Type"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "inkassoadresse"
        })
        public static class Zahlungsanweisung {

            @XmlElement(name = "Inkassoadresse", required = true)
            protected ADRESSEType inkassoadresse;

            /**
             * Ruft den Wert der inkassoadresse-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link ADRESSEType }
             *     
             */
            public ADRESSEType getInkassoadresse() {
                return inkassoadresse;
            }

            /**
             * Legt den Wert der inkassoadresse-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link ADRESSEType }
             *     
             */
            public void setInkassoadresse(ADRESSEType value) {
                this.inkassoadresse = value;
            }

        }

    }

}
