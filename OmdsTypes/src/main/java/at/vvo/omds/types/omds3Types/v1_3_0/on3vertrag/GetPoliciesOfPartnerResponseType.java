
package at.vvo.omds.types.omds3Types.v1_3_0.on3vertrag;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds2Types.v2_9.ADRESSEType;
import at.vvo.omds.types.omds2Types.v2_9.VERTRAGType;
import at.vvo.omds.types.omds3Types.v1_3_0.common.CommonResponseType;


/**
 * Responsetyp zu den Polizzen, in denen ein Partner in der Rolle VN auftritt
 * 
 * <p>Java-Klasse für GetPoliciesOfPartnerResponse_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="GetPoliciesOfPartnerResponse_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}CommonResponse_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Vertraege" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Vertrag" type="{urn:omds20}VERTRAG_Type"/&gt;
 *                   &lt;element name="Zustelladresse" type="{urn:omds20}ADRESSE_Type" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetPoliciesOfPartnerResponse_Type", propOrder = {
    "vertraege"
})
public class GetPoliciesOfPartnerResponseType
    extends CommonResponseType
{

    @XmlElement(name = "Vertraege")
    protected List<GetPoliciesOfPartnerResponseType.Vertraege> vertraege;

    /**
     * Gets the value of the vertraege property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the vertraege property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVertraege().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetPoliciesOfPartnerResponseType.Vertraege }
     * 
     * 
     */
    public List<GetPoliciesOfPartnerResponseType.Vertraege> getVertraege() {
        if (vertraege == null) {
            vertraege = new ArrayList<GetPoliciesOfPartnerResponseType.Vertraege>();
        }
        return this.vertraege;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Vertrag" type="{urn:omds20}VERTRAG_Type"/&gt;
     *         &lt;element name="Zustelladresse" type="{urn:omds20}ADRESSE_Type" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "vertrag",
        "zustelladresse"
    })
    public static class Vertraege {

        @XmlElement(name = "Vertrag", required = true)
        protected VERTRAGType vertrag;
        @XmlElement(name = "Zustelladresse")
        protected ADRESSEType zustelladresse;

        /**
         * Ruft den Wert der vertrag-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link VERTRAGType }
         *     
         */
        public VERTRAGType getVertrag() {
            return vertrag;
        }

        /**
         * Legt den Wert der vertrag-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link VERTRAGType }
         *     
         */
        public void setVertrag(VERTRAGType value) {
            this.vertrag = value;
        }

        /**
         * Ruft den Wert der zustelladresse-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link ADRESSEType }
         *     
         */
        public ADRESSEType getZustelladresse() {
            return zustelladresse;
        }

        /**
         * Legt den Wert der zustelladresse-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link ADRESSEType }
         *     
         */
        public void setZustelladresse(ADRESSEType value) {
            this.zustelladresse = value;
        }

    }

}
