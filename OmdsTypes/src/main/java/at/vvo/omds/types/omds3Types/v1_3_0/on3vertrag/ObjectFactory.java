
package at.vvo.omds.types.omds3Types.v1_3_0.on3vertrag;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the at.vvo.omds.types.omds3Types.v1_3_0.on3vertrag package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetPoliciesOfPartnerRequest_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-3-0.on3vertrag", "GetPoliciesOfPartnerRequest");
    private final static QName _GetPoliciesOfPartnerResponse_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-3-0.on3vertrag", "GetPoliciesOfPartnerResponse");
    private final static QName _SetMailingAddressRequest_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-3-0.on3vertrag", "SetMailingAddressRequest");
    private final static QName _SetMailingAddressResponse_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-3-0.on3vertrag", "SetMailingAddressResponse");
    private final static QName _CollectionChangeRequest_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-3-0.on3vertrag", "CollectionChangeRequest");
    private final static QName _CollectionChangeResponse_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-3-0.on3vertrag", "CollectionChangeResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: at.vvo.omds.types.omds3Types.v1_3_0.on3vertrag
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CollectionChangeRequestType }
     * 
     */
    public CollectionChangeRequestType createCollectionChangeRequestType() {
        return new CollectionChangeRequestType();
    }

    /**
     * Create an instance of {@link CollectionChangeRequestType.Zahlweg }
     * 
     */
    public CollectionChangeRequestType.Zahlweg createCollectionChangeRequestTypeZahlweg() {
        return new CollectionChangeRequestType.Zahlweg();
    }

    /**
     * Create an instance of {@link SetMailingAddressRequestType }
     * 
     */
    public SetMailingAddressRequestType createSetMailingAddressRequestType() {
        return new SetMailingAddressRequestType();
    }

    /**
     * Create an instance of {@link GetPoliciesOfPartnerResponseType }
     * 
     */
    public GetPoliciesOfPartnerResponseType createGetPoliciesOfPartnerResponseType() {
        return new GetPoliciesOfPartnerResponseType();
    }

    /**
     * Create an instance of {@link GetPoliciesOfPartnerRequestType }
     * 
     */
    public GetPoliciesOfPartnerRequestType createGetPoliciesOfPartnerRequestType() {
        return new GetPoliciesOfPartnerRequestType();
    }

    /**
     * Create an instance of {@link SetMailingAddressResponseType }
     * 
     */
    public SetMailingAddressResponseType createSetMailingAddressResponseType() {
        return new SetMailingAddressResponseType();
    }

    /**
     * Create an instance of {@link CollectionChangeResponseType }
     * 
     */
    public CollectionChangeResponseType createCollectionChangeResponseType() {
        return new CollectionChangeResponseType();
    }

    /**
     * Create an instance of {@link CollectionChangeRequestType.Zahlweg.Zahlungsanweisung }
     * 
     */
    public CollectionChangeRequestType.Zahlweg.Zahlungsanweisung createCollectionChangeRequestTypeZahlwegZahlungsanweisung() {
        return new CollectionChangeRequestType.Zahlweg.Zahlungsanweisung();
    }

    /**
     * Create an instance of {@link CollectionChangeRequestType.Zahlweg.Kundenkonto }
     * 
     */
    public CollectionChangeRequestType.Zahlweg.Kundenkonto createCollectionChangeRequestTypeZahlwegKundenkonto() {
        return new CollectionChangeRequestType.Zahlweg.Kundenkonto();
    }

    /**
     * Create an instance of {@link CollectionChangeRequestType.Zahlweg.Kreditkarte }
     * 
     */
    public CollectionChangeRequestType.Zahlweg.Kreditkarte createCollectionChangeRequestTypeZahlwegKreditkarte() {
        return new CollectionChangeRequestType.Zahlweg.Kreditkarte();
    }

    /**
     * Create an instance of {@link SetMailingAddressRequestType.Vertrag }
     * 
     */
    public SetMailingAddressRequestType.Vertrag createSetMailingAddressRequestTypeVertrag() {
        return new SetMailingAddressRequestType.Vertrag();
    }

    /**
     * Create an instance of {@link GetPoliciesOfPartnerResponseType.Vertraege }
     * 
     */
    public GetPoliciesOfPartnerResponseType.Vertraege createGetPoliciesOfPartnerResponseTypeVertraege() {
        return new GetPoliciesOfPartnerResponseType.Vertraege();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPoliciesOfPartnerRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on3vertrag", name = "GetPoliciesOfPartnerRequest")
    public JAXBElement<GetPoliciesOfPartnerRequestType> createGetPoliciesOfPartnerRequest(GetPoliciesOfPartnerRequestType value) {
        return new JAXBElement<GetPoliciesOfPartnerRequestType>(_GetPoliciesOfPartnerRequest_QNAME, GetPoliciesOfPartnerRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPoliciesOfPartnerResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on3vertrag", name = "GetPoliciesOfPartnerResponse")
    public JAXBElement<GetPoliciesOfPartnerResponseType> createGetPoliciesOfPartnerResponse(GetPoliciesOfPartnerResponseType value) {
        return new JAXBElement<GetPoliciesOfPartnerResponseType>(_GetPoliciesOfPartnerResponse_QNAME, GetPoliciesOfPartnerResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetMailingAddressRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on3vertrag", name = "SetMailingAddressRequest")
    public JAXBElement<SetMailingAddressRequestType> createSetMailingAddressRequest(SetMailingAddressRequestType value) {
        return new JAXBElement<SetMailingAddressRequestType>(_SetMailingAddressRequest_QNAME, SetMailingAddressRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetMailingAddressResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on3vertrag", name = "SetMailingAddressResponse")
    public JAXBElement<SetMailingAddressResponseType> createSetMailingAddressResponse(SetMailingAddressResponseType value) {
        return new JAXBElement<SetMailingAddressResponseType>(_SetMailingAddressResponse_QNAME, SetMailingAddressResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CollectionChangeRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on3vertrag", name = "CollectionChangeRequest")
    public JAXBElement<CollectionChangeRequestType> createCollectionChangeRequest(CollectionChangeRequestType value) {
        return new JAXBElement<CollectionChangeRequestType>(_CollectionChangeRequest_QNAME, CollectionChangeRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CollectionChangeResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on3vertrag", name = "CollectionChangeResponse")
    public JAXBElement<CollectionChangeResponseType> createCollectionChangeResponse(CollectionChangeResponseType value) {
        return new JAXBElement<CollectionChangeResponseType>(_CollectionChangeResponse_QNAME, CollectionChangeResponseType.class, null, value);
    }

}
