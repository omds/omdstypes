
package at.vvo.omds.types.omds3Types.v1_3_0.on3vertrag;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds2Types.v2_9.ADRESSEType;
import at.vvo.omds.types.omds3Types.v1_3_0.common.CommonRequestType;


/**
 * Typ des Requestobjekts für das Setzen einer Zustelladresse
 * 
 * <p>Java-Klasse für SetMailingAddressRequest_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SetMailingAddressRequest_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}CommonRequest_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Vertrag"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;attribute name="Polizzennr" use="required" type="{urn:omds20}Polizzennr" /&gt;
 *                 &lt;attribute name="VertragsID" type="{urn:omds20}VertragsID" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Adresse" type="{urn:omds20}ADRESSE_Type"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SetMailingAddressRequest_Type", propOrder = {
    "vertrag",
    "adresse"
})
public class SetMailingAddressRequestType
    extends CommonRequestType
{

    @XmlElement(name = "Vertrag", required = true)
    protected SetMailingAddressRequestType.Vertrag vertrag;
    @XmlElement(name = "Adresse", required = true)
    protected ADRESSEType adresse;

    /**
     * Ruft den Wert der vertrag-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SetMailingAddressRequestType.Vertrag }
     *     
     */
    public SetMailingAddressRequestType.Vertrag getVertrag() {
        return vertrag;
    }

    /**
     * Legt den Wert der vertrag-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SetMailingAddressRequestType.Vertrag }
     *     
     */
    public void setVertrag(SetMailingAddressRequestType.Vertrag value) {
        this.vertrag = value;
    }

    /**
     * Ruft den Wert der adresse-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ADRESSEType }
     *     
     */
    public ADRESSEType getAdresse() {
        return adresse;
    }

    /**
     * Legt den Wert der adresse-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ADRESSEType }
     *     
     */
    public void setAdresse(ADRESSEType value) {
        this.adresse = value;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;attribute name="Polizzennr" use="required" type="{urn:omds20}Polizzennr" /&gt;
     *       &lt;attribute name="VertragsID" type="{urn:omds20}VertragsID" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Vertrag {

        @XmlAttribute(name = "Polizzennr", namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on3vertrag", required = true)
        protected String polizzennr;
        @XmlAttribute(name = "VertragsID", namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on3vertrag")
        protected String vertragsID;

        /**
         * Ruft den Wert der polizzennr-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPolizzennr() {
            return polizzennr;
        }

        /**
         * Legt den Wert der polizzennr-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPolizzennr(String value) {
            this.polizzennr = value;
        }

        /**
         * Ruft den Wert der vertragsID-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVertragsID() {
            return vertragsID;
        }

        /**
         * Legt den Wert der vertragsID-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVertragsID(String value) {
            this.vertragsID = value;
        }

    }

}
