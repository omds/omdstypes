
package at.vvo.omds.types.omds3Types.v1_3_0.on3vertrag;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds2Types.v2_9.ADRESSEType;
import at.vvo.omds.types.omds3Types.v1_3_0.common.CommonResponseType;


/**
 * Typ des Responseobjekts für das Setzen einer Zustelladresse
 * 
 * <p>Java-Klasse für SetMailingAddressResponse_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SetMailingAddressResponse_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}CommonResponse_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="BisherigeAdresse" type="{urn:omds20}ADRESSE_Type" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SetMailingAddressResponse_Type", propOrder = {
    "bisherigeAdresse"
})
public class SetMailingAddressResponseType
    extends CommonResponseType
{

    @XmlElement(name = "BisherigeAdresse")
    protected ADRESSEType bisherigeAdresse;

    /**
     * Ruft den Wert der bisherigeAdresse-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ADRESSEType }
     *     
     */
    public ADRESSEType getBisherigeAdresse() {
        return bisherigeAdresse;
    }

    /**
     * Legt den Wert der bisherigeAdresse-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ADRESSEType }
     *     
     */
    public void setBisherigeAdresse(ADRESSEType value) {
        this.bisherigeAdresse = value;
    }

}
