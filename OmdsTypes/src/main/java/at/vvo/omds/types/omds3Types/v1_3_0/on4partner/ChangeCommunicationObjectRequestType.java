
package at.vvo.omds.types.omds3Types.v1_3_0.on4partner;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import at.vvo.omds.types.omds2Types.v2_9.ELKommunikationType;
import at.vvo.omds.types.omds3Types.v1_3_0.common.CommonRequestType;
import at.vvo.omds.types.omds3Types.v1_3_0.common.DateianhangType;


/**
 * Typ des Requestobjekts für eine Änderung einer bestehenden Kommunikationsverbindung
 * 
 * <p>Java-Klasse für ChangeCommunicationObjectRequest_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ChangeCommunicationObjectRequest_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}CommonRequest_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Aenderunggrund" type="{http://www.w3.org/2001/XMLSchema}anyType"/&gt;
 *         &lt;element name="BisherigeKommunikationsVerbindung" type="{urn:omds20}EL-Kommunikation_Type"/&gt;
 *         &lt;element name="GeaenderteKommunikationsVerbindung" type="{urn:omds20}EL-Kommunikation_Type"/&gt;
 *         &lt;element name="WirksamtkeitAb" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *         &lt;element name="Dateianhaenge" type="{urn:omds3CommonServiceTypes-1-1-0}Dateianhang_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="Personennr" use="required"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{urn:omds20}Personennr"&gt;
 *             &lt;maxLength value="26"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChangeCommunicationObjectRequest_Type", propOrder = {
    "aenderunggrund",
    "bisherigeKommunikationsVerbindung",
    "geaenderteKommunikationsVerbindung",
    "wirksamtkeitAb",
    "dateianhaenge"
})
public class ChangeCommunicationObjectRequestType
    extends CommonRequestType
{

    @XmlElement(name = "Aenderunggrund", required = true)
    protected Object aenderunggrund;
    @XmlElement(name = "BisherigeKommunikationsVerbindung", required = true)
    protected ELKommunikationType bisherigeKommunikationsVerbindung;
    @XmlElement(name = "GeaenderteKommunikationsVerbindung", required = true)
    protected ELKommunikationType geaenderteKommunikationsVerbindung;
    @XmlElement(name = "WirksamtkeitAb", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar wirksamtkeitAb;
    @XmlElement(name = "Dateianhaenge")
    protected List<DateianhangType> dateianhaenge;
    @XmlAttribute(name = "Personennr", namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on4partner", required = true)
    protected String personennr;

    /**
     * Ruft den Wert der aenderunggrund-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getAenderunggrund() {
        return aenderunggrund;
    }

    /**
     * Legt den Wert der aenderunggrund-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setAenderunggrund(Object value) {
        this.aenderunggrund = value;
    }

    /**
     * Ruft den Wert der bisherigeKommunikationsVerbindung-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ELKommunikationType }
     *     
     */
    public ELKommunikationType getBisherigeKommunikationsVerbindung() {
        return bisherigeKommunikationsVerbindung;
    }

    /**
     * Legt den Wert der bisherigeKommunikationsVerbindung-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ELKommunikationType }
     *     
     */
    public void setBisherigeKommunikationsVerbindung(ELKommunikationType value) {
        this.bisherigeKommunikationsVerbindung = value;
    }

    /**
     * Ruft den Wert der geaenderteKommunikationsVerbindung-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ELKommunikationType }
     *     
     */
    public ELKommunikationType getGeaenderteKommunikationsVerbindung() {
        return geaenderteKommunikationsVerbindung;
    }

    /**
     * Legt den Wert der geaenderteKommunikationsVerbindung-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ELKommunikationType }
     *     
     */
    public void setGeaenderteKommunikationsVerbindung(ELKommunikationType value) {
        this.geaenderteKommunikationsVerbindung = value;
    }

    /**
     * Ruft den Wert der wirksamtkeitAb-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getWirksamtkeitAb() {
        return wirksamtkeitAb;
    }

    /**
     * Legt den Wert der wirksamtkeitAb-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setWirksamtkeitAb(XMLGregorianCalendar value) {
        this.wirksamtkeitAb = value;
    }

    /**
     * Gets the value of the dateianhaenge property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dateianhaenge property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDateianhaenge().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DateianhangType }
     * 
     * 
     */
    public List<DateianhangType> getDateianhaenge() {
        if (dateianhaenge == null) {
            dateianhaenge = new ArrayList<DateianhangType>();
        }
        return this.dateianhaenge;
    }

    /**
     * Ruft den Wert der personennr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPersonennr() {
        return personennr;
    }

    /**
     * Legt den Wert der personennr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPersonennr(String value) {
        this.personennr = value;
    }

}
