
package at.vvo.omds.types.omds3Types.v1_3_0.on4partner;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import at.vvo.omds.types.omds2Types.v2_9.ADRESSEType;
import at.vvo.omds.types.omds3Types.v1_3_0.common.CommonRequestType;
import at.vvo.omds.types.omds3Types.v1_3_0.common.DateianhangType;
import at.vvo.omds.types.omds3Types.v1_3_0.common.ObjektIdType;


/**
 * Typ des Requestobjekts für eine Änderung einer bestehenden Adresse
 * 
 * <p>Java-Klasse für ChangePartnerMainAddressRequest_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ChangePartnerMainAddressRequest_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}CommonRequest_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Aenderungsgrund" type="{http://www.w3.org/2001/XMLSchema}anyType"/&gt;
 *         &lt;element name="Hinweistext" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;choice&gt;
 *           &lt;element ref="{urn:omds3CommonServiceTypes-1-1-0}ObjektId"/&gt;
 *           &lt;element name="BisherigeAnschrift" type="{urn:omds20}ADRESSE_Type"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="GeaenderteAnschrift" type="{urn:omds20}ADRESSE_Type"/&gt;
 *         &lt;element name="WirksamtkeitAb" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *         &lt;element name="Dateianhaenge" type="{urn:omds3CommonServiceTypes-1-1-0}Dateianhang_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="UeberschreibeZustelladresseInVertraegen" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;attribute name="Polizzennr" use="required" type="{urn:omds20}Polizzennr" /&gt;
 *                 &lt;attribute name="VertragsID" type="{urn:omds20}VertragsID" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="Personennr" use="required"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{urn:omds20}Personennr"&gt;
 *             &lt;maxLength value="26"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChangePartnerMainAddressRequest_Type", propOrder = {
    "aenderungsgrund",
    "hinweistext",
    "objektId",
    "bisherigeAnschrift",
    "geaenderteAnschrift",
    "wirksamtkeitAb",
    "dateianhaenge",
    "ueberschreibeZustelladresseInVertraegen"
})
public class ChangePartnerMainAddressRequestType
    extends CommonRequestType
{

    @XmlElement(name = "Aenderungsgrund", required = true)
    protected Object aenderungsgrund;
    @XmlElement(name = "Hinweistext")
    protected String hinweistext;
    @XmlElement(name = "ObjektId", namespace = "urn:omds3CommonServiceTypes-1-1-0")
    protected ObjektIdType objektId;
    @XmlElement(name = "BisherigeAnschrift")
    protected ADRESSEType bisherigeAnschrift;
    @XmlElement(name = "GeaenderteAnschrift", required = true)
    protected ADRESSEType geaenderteAnschrift;
    @XmlElement(name = "WirksamtkeitAb", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar wirksamtkeitAb;
    @XmlElement(name = "Dateianhaenge")
    protected List<DateianhangType> dateianhaenge;
    @XmlElement(name = "UeberschreibeZustelladresseInVertraegen")
    protected List<ChangePartnerMainAddressRequestType.UeberschreibeZustelladresseInVertraegen> ueberschreibeZustelladresseInVertraegen;
    @XmlAttribute(name = "Personennr", namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on4partner", required = true)
    protected String personennr;

    /**
     * Ruft den Wert der aenderungsgrund-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getAenderungsgrund() {
        return aenderungsgrund;
    }

    /**
     * Legt den Wert der aenderungsgrund-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setAenderungsgrund(Object value) {
        this.aenderungsgrund = value;
    }

    /**
     * Ruft den Wert der hinweistext-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHinweistext() {
        return hinweistext;
    }

    /**
     * Legt den Wert der hinweistext-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHinweistext(String value) {
        this.hinweistext = value;
    }

    /**
     * Ruft den Wert der objektId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ObjektIdType }
     *     
     */
    public ObjektIdType getObjektId() {
        return objektId;
    }

    /**
     * Legt den Wert der objektId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjektIdType }
     *     
     */
    public void setObjektId(ObjektIdType value) {
        this.objektId = value;
    }

    /**
     * Ruft den Wert der bisherigeAnschrift-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ADRESSEType }
     *     
     */
    public ADRESSEType getBisherigeAnschrift() {
        return bisherigeAnschrift;
    }

    /**
     * Legt den Wert der bisherigeAnschrift-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ADRESSEType }
     *     
     */
    public void setBisherigeAnschrift(ADRESSEType value) {
        this.bisherigeAnschrift = value;
    }

    /**
     * Ruft den Wert der geaenderteAnschrift-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ADRESSEType }
     *     
     */
    public ADRESSEType getGeaenderteAnschrift() {
        return geaenderteAnschrift;
    }

    /**
     * Legt den Wert der geaenderteAnschrift-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ADRESSEType }
     *     
     */
    public void setGeaenderteAnschrift(ADRESSEType value) {
        this.geaenderteAnschrift = value;
    }

    /**
     * Ruft den Wert der wirksamtkeitAb-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getWirksamtkeitAb() {
        return wirksamtkeitAb;
    }

    /**
     * Legt den Wert der wirksamtkeitAb-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setWirksamtkeitAb(XMLGregorianCalendar value) {
        this.wirksamtkeitAb = value;
    }

    /**
     * Gets the value of the dateianhaenge property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dateianhaenge property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDateianhaenge().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DateianhangType }
     * 
     * 
     */
    public List<DateianhangType> getDateianhaenge() {
        if (dateianhaenge == null) {
            dateianhaenge = new ArrayList<DateianhangType>();
        }
        return this.dateianhaenge;
    }

    /**
     * Gets the value of the ueberschreibeZustelladresseInVertraegen property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ueberschreibeZustelladresseInVertraegen property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUeberschreibeZustelladresseInVertraegen().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ChangePartnerMainAddressRequestType.UeberschreibeZustelladresseInVertraegen }
     * 
     * 
     */
    public List<ChangePartnerMainAddressRequestType.UeberschreibeZustelladresseInVertraegen> getUeberschreibeZustelladresseInVertraegen() {
        if (ueberschreibeZustelladresseInVertraegen == null) {
            ueberschreibeZustelladresseInVertraegen = new ArrayList<ChangePartnerMainAddressRequestType.UeberschreibeZustelladresseInVertraegen>();
        }
        return this.ueberschreibeZustelladresseInVertraegen;
    }

    /**
     * Ruft den Wert der personennr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPersonennr() {
        return personennr;
    }

    /**
     * Legt den Wert der personennr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPersonennr(String value) {
        this.personennr = value;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;attribute name="Polizzennr" use="required" type="{urn:omds20}Polizzennr" /&gt;
     *       &lt;attribute name="VertragsID" type="{urn:omds20}VertragsID" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class UeberschreibeZustelladresseInVertraegen {

        @XmlAttribute(name = "Polizzennr", namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on4partner", required = true)
        protected String polizzennr;
        @XmlAttribute(name = "VertragsID", namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on4partner")
        protected String vertragsID;

        /**
         * Ruft den Wert der polizzennr-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPolizzennr() {
            return polizzennr;
        }

        /**
         * Legt den Wert der polizzennr-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPolizzennr(String value) {
            this.polizzennr = value;
        }

        /**
         * Ruft den Wert der vertragsID-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVertragsID() {
            return vertragsID;
        }

        /**
         * Legt den Wert der vertragsID-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVertragsID(String value) {
            this.vertragsID = value;
        }

    }

}
