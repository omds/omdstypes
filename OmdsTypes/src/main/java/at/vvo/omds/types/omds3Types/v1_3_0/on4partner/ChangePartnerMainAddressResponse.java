
package at.vvo.omds.types.omds3Types.v1_3_0.on4partner;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds2Types.v2_9.PERSONType;
import at.vvo.omds.types.omds2Types.v2_9.VERTRAGType;


/**
 * <p>Java-Klasse für anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on4partner}ChangePartnerMainAddressResponse_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="VertraegeGeaendert" type="{urn:omds20}VERTRAG_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="VertraegeMitUnveraendertenRisikoadressen" type="{urn:omds20}VERTRAG_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="WeiterePersonenAnAdresse" type="{urn:omds20}PERSON_Type"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "vertraegeGeaendert",
    "vertraegeMitUnveraendertenRisikoadressen",
    "weiterePersonenAnAdresse"
})
@XmlRootElement(name = "ChangePartnerMainAddressResponse")
public class ChangePartnerMainAddressResponse
    extends ChangePartnerMainAddressResponseType
{

    @XmlElement(name = "VertraegeGeaendert")
    protected List<VERTRAGType> vertraegeGeaendert;
    @XmlElement(name = "VertraegeMitUnveraendertenRisikoadressen")
    protected List<VERTRAGType> vertraegeMitUnveraendertenRisikoadressen;
    @XmlElement(name = "WeiterePersonenAnAdresse", required = true)
    protected PERSONType weiterePersonenAnAdresse;

    /**
     * Gets the value of the vertraegeGeaendert property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the vertraegeGeaendert property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVertraegeGeaendert().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link VERTRAGType }
     * 
     * 
     */
    public List<VERTRAGType> getVertraegeGeaendert() {
        if (vertraegeGeaendert == null) {
            vertraegeGeaendert = new ArrayList<VERTRAGType>();
        }
        return this.vertraegeGeaendert;
    }

    /**
     * Gets the value of the vertraegeMitUnveraendertenRisikoadressen property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the vertraegeMitUnveraendertenRisikoadressen property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVertraegeMitUnveraendertenRisikoadressen().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link VERTRAGType }
     * 
     * 
     */
    public List<VERTRAGType> getVertraegeMitUnveraendertenRisikoadressen() {
        if (vertraegeMitUnveraendertenRisikoadressen == null) {
            vertraegeMitUnveraendertenRisikoadressen = new ArrayList<VERTRAGType>();
        }
        return this.vertraegeMitUnveraendertenRisikoadressen;
    }

    /**
     * Ruft den Wert der weiterePersonenAnAdresse-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PERSONType }
     *     
     */
    public PERSONType getWeiterePersonenAnAdresse() {
        return weiterePersonenAnAdresse;
    }

    /**
     * Legt den Wert der weiterePersonenAnAdresse-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PERSONType }
     *     
     */
    public void setWeiterePersonenAnAdresse(PERSONType value) {
        this.weiterePersonenAnAdresse = value;
    }

}
