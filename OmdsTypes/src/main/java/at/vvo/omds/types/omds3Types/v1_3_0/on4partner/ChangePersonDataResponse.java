
package at.vvo.omds.types.omds3Types.v1_3_0.on4partner;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.v1_3_0.common.ObjektIdType;


/**
 * <p>Java-Klasse für anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:at.vvo.omds.types.omds3types.v1-3-0.on4partner}ChangePersonDataResponse_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="BetroffeneObjekte"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Art" type="{http://www.w3.org/2001/XMLSchema}anyType"/&gt;
 *                   &lt;element ref="{urn:omds3CommonServiceTypes-1-1-0}ObjektId"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "betroffeneObjekte"
})
@XmlRootElement(name = "ChangePersonDataResponse")
public class ChangePersonDataResponse
    extends ChangePersonDataResponseType
{

    @XmlElement(name = "BetroffeneObjekte", required = true)
    protected ChangePersonDataResponse.BetroffeneObjekte betroffeneObjekte;

    /**
     * Ruft den Wert der betroffeneObjekte-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ChangePersonDataResponse.BetroffeneObjekte }
     *     
     */
    public ChangePersonDataResponse.BetroffeneObjekte getBetroffeneObjekte() {
        return betroffeneObjekte;
    }

    /**
     * Legt den Wert der betroffeneObjekte-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ChangePersonDataResponse.BetroffeneObjekte }
     *     
     */
    public void setBetroffeneObjekte(ChangePersonDataResponse.BetroffeneObjekte value) {
        this.betroffeneObjekte = value;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Art" type="{http://www.w3.org/2001/XMLSchema}anyType"/&gt;
     *         &lt;element ref="{urn:omds3CommonServiceTypes-1-1-0}ObjektId"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "art",
        "objektId"
    })
    public static class BetroffeneObjekte {

        @XmlElement(name = "Art", required = true)
        protected Object art;
        @XmlElement(name = "ObjektId", namespace = "urn:omds3CommonServiceTypes-1-1-0", required = true)
        protected ObjektIdType objektId;

        /**
         * Ruft den Wert der art-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getArt() {
            return art;
        }

        /**
         * Legt den Wert der art-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setArt(Object value) {
            this.art = value;
        }

        /**
         * Ruft den Wert der objektId-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link ObjektIdType }
         *     
         */
        public ObjektIdType getObjektId() {
            return objektId;
        }

        /**
         * Legt den Wert der objektId-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link ObjektIdType }
         *     
         */
        public void setObjektId(ObjektIdType value) {
            this.objektId = value;
        }

    }

}
