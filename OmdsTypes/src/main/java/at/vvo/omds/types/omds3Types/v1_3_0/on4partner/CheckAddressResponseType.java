
package at.vvo.omds.types.omds3Types.v1_3_0.on4partner;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds2Types.v2_9.ADRESSEType;
import at.vvo.omds.types.omds3Types.v1_3_0.common.CommonResponseType;


/**
 * Responsetyp der Überprüfung einer Adresse
 * 
 * <p>Java-Klasse für CheckAddressResponse_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CheckAddressResponse_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}CommonResponse_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Adresse" type="{urn:omds20}ADRESSE_Type" minOccurs="0"/&gt;
 *         &lt;element name="AlternativeAdressen" type="{urn:omds20}ADRESSE_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CheckAddressResponse_Type", propOrder = {
    "adresse",
    "alternativeAdressen"
})
public class CheckAddressResponseType
    extends CommonResponseType
{

    @XmlElement(name = "Adresse")
    protected ADRESSEType adresse;
    @XmlElement(name = "AlternativeAdressen")
    protected List<ADRESSEType> alternativeAdressen;

    /**
     * Ruft den Wert der adresse-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ADRESSEType }
     *     
     */
    public ADRESSEType getAdresse() {
        return adresse;
    }

    /**
     * Legt den Wert der adresse-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ADRESSEType }
     *     
     */
    public void setAdresse(ADRESSEType value) {
        this.adresse = value;
    }

    /**
     * Gets the value of the alternativeAdressen property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the alternativeAdressen property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAlternativeAdressen().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ADRESSEType }
     * 
     * 
     */
    public List<ADRESSEType> getAlternativeAdressen() {
        if (alternativeAdressen == null) {
            alternativeAdressen = new ArrayList<ADRESSEType>();
        }
        return this.alternativeAdressen;
    }

}
