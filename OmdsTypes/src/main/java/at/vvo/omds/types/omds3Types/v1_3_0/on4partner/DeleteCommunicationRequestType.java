
package at.vvo.omds.types.omds3Types.v1_3_0.on4partner;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import at.vvo.omds.types.omds2Types.v2_9.ELKommunikationType;
import at.vvo.omds.types.omds3Types.v1_3_0.common.CommonRequestType;


/**
 * Typ des Requestobjekts für die Löschung einer Kommunikationsverbindung
 * 
 * <p>Java-Klasse für DeleteCommunicationRequest_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="DeleteCommunicationRequest_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}CommonRequest_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="KommunikationsVerbindung" type="{urn:omds20}EL-Kommunikation_Type"/&gt;
 *         &lt;element name="WirksamtkeitAb" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="Personennr" use="required"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{urn:omds20}Personennr"&gt;
 *             &lt;maxLength value="26"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DeleteCommunicationRequest_Type", propOrder = {
    "kommunikationsVerbindung",
    "wirksamtkeitAb"
})
public class DeleteCommunicationRequestType
    extends CommonRequestType
{

    @XmlElement(name = "KommunikationsVerbindung", required = true)
    protected ELKommunikationType kommunikationsVerbindung;
    @XmlElement(name = "WirksamtkeitAb", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar wirksamtkeitAb;
    @XmlAttribute(name = "Personennr", namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on4partner", required = true)
    protected String personennr;

    /**
     * Ruft den Wert der kommunikationsVerbindung-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ELKommunikationType }
     *     
     */
    public ELKommunikationType getKommunikationsVerbindung() {
        return kommunikationsVerbindung;
    }

    /**
     * Legt den Wert der kommunikationsVerbindung-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ELKommunikationType }
     *     
     */
    public void setKommunikationsVerbindung(ELKommunikationType value) {
        this.kommunikationsVerbindung = value;
    }

    /**
     * Ruft den Wert der wirksamtkeitAb-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getWirksamtkeitAb() {
        return wirksamtkeitAb;
    }

    /**
     * Legt den Wert der wirksamtkeitAb-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setWirksamtkeitAb(XMLGregorianCalendar value) {
        this.wirksamtkeitAb = value;
    }

    /**
     * Ruft den Wert der personennr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPersonennr() {
        return personennr;
    }

    /**
     * Legt den Wert der personennr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPersonennr(String value) {
        this.personennr = value;
    }

}
