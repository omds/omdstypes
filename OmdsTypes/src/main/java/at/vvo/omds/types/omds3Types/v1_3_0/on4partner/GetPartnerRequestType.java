
package at.vvo.omds.types.omds3Types.v1_3_0.on4partner;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds3Types.v1_3_0.common.AgentFilterType;
import at.vvo.omds.types.omds3Types.v1_3_0.common.CommonRequestType;


/**
 * Requesttyp um aktuelle Partnerdaten zu beziehen
 * 
 * <p>Java-Klasse für GetPartnerRequest_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="GetPartnerRequest_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}CommonRequest_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AuthFilter" type="{urn:omds3CommonServiceTypes-1-1-0}AgentFilter_Type" minOccurs="0"/&gt;
 *         &lt;element name="Personennr" type="{urn:omds20}Personennr"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetPartnerRequest_Type", propOrder = {
    "authFilter",
    "personennr"
})
public class GetPartnerRequestType
    extends CommonRequestType
{

    @XmlElement(name = "AuthFilter")
    protected AgentFilterType authFilter;
    @XmlElement(name = "Personennr", required = true)
    protected String personennr;

    /**
     * Ruft den Wert der authFilter-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AgentFilterType }
     *     
     */
    public AgentFilterType getAuthFilter() {
        return authFilter;
    }

    /**
     * Legt den Wert der authFilter-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AgentFilterType }
     *     
     */
    public void setAuthFilter(AgentFilterType value) {
        this.authFilter = value;
    }

    /**
     * Ruft den Wert der personennr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPersonennr() {
        return personennr;
    }

    /**
     * Legt den Wert der personennr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPersonennr(String value) {
        this.personennr = value;
    }

}
