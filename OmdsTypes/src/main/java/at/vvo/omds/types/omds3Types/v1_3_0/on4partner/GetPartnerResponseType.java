
package at.vvo.omds.types.omds3Types.v1_3_0.on4partner;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import at.vvo.omds.types.omds2Types.v2_9.PERSONType;
import at.vvo.omds.types.omds3Types.v1_3_0.common.CommonResponseType;
import at.vvo.omds.types.omds3Types.v1_3_0.common.ObjektIdType;


/**
 * Responsetyp um aktuelle Partnerdaten zu beziehen
 * 
 * <p>Java-Klasse für GetPartnerResponse_Type complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="GetPartnerResponse_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:omds3CommonServiceTypes-1-1-0}CommonResponse_Type"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:omds20}PERSON"/&gt;
 *         &lt;element ref="{urn:omds3CommonServiceTypes-1-1-0}ObjektId" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetPartnerResponse_Type", propOrder = {
    "person",
    "objektId"
})
public class GetPartnerResponseType
    extends CommonResponseType
{

    @XmlElement(name = "PERSON", namespace = "urn:omds20", required = true)
    protected PERSONType person;
    @XmlElement(name = "ObjektId", namespace = "urn:omds3CommonServiceTypes-1-1-0")
    protected ObjektIdType objektId;

    /**
     * Ruft den Wert der person-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PERSONType }
     *     
     */
    public PERSONType getPERSON() {
        return person;
    }

    /**
     * Legt den Wert der person-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PERSONType }
     *     
     */
    public void setPERSON(PERSONType value) {
        this.person = value;
    }

    /**
     * Ruft den Wert der objektId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ObjektIdType }
     *     
     */
    public ObjektIdType getObjektId() {
        return objektId;
    }

    /**
     * Legt den Wert der objektId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjektIdType }
     *     
     */
    public void setObjektId(ObjektIdType value) {
        this.objektId = value;
    }

}
