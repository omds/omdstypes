
package at.vvo.omds.types.omds3Types.v1_3_0.on4partner;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the at.vvo.omds.types.omds3Types.v1_3_0.on4partner package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetPartnerRequest_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-3-0.on4partner", "GetPartnerRequest");
    private final static QName _GetPartnerResponse_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-3-0.on4partner", "GetPartnerResponse");
    private final static QName _CheckAddressRequest_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-3-0.on4partner", "CheckAddressRequest");
    private final static QName _CheckAddressResponse_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-3-0.on4partner", "CheckAddressResponse");
    private final static QName _ChangePartnerMainAddressRequest_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-3-0.on4partner", "ChangePartnerMainAddressRequest");
    private final static QName _ChangePersonDataRequest_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-3-0.on4partner", "ChangePersonDataRequest");
    private final static QName _ChangeCommunicationObjectRequest_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-3-0.on4partner", "ChangeCommunicationObjectRequest");
    private final static QName _ChangeCommunicationObjectResponse_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-3-0.on4partner", "ChangeCommunicationObjectResponse");
    private final static QName _DeleteCommunicationObjectRequest_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-3-0.on4partner", "DeleteCommunicationObjectRequest");
    private final static QName _DeleteCommunicationObjectResponse_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-3-0.on4partner", "DeleteCommunicationObjectResponse");
    private final static QName _CreateNewCommunicationObjectRequest_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-3-0.on4partner", "CreateNewCommunicationObjectRequest");
    private final static QName _CreateNewCommunicationObjectResponse_QNAME = new QName("urn:at.vvo.omds.types.omds3types.v1-3-0.on4partner", "CreateNewCommunicationObjectResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: at.vvo.omds.types.omds3Types.v1_3_0.on4partner
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ChangePersonDataResponse }
     * 
     */
    public ChangePersonDataResponse createChangePersonDataResponse() {
        return new ChangePersonDataResponse();
    }

    /**
     * Create an instance of {@link ChangePartnerMainAddressRequestType }
     * 
     */
    public ChangePartnerMainAddressRequestType createChangePartnerMainAddressRequestType() {
        return new ChangePartnerMainAddressRequestType();
    }

    /**
     * Create an instance of {@link GetPartnerRequestType }
     * 
     */
    public GetPartnerRequestType createGetPartnerRequestType() {
        return new GetPartnerRequestType();
    }

    /**
     * Create an instance of {@link GetPartnerResponseType }
     * 
     */
    public GetPartnerResponseType createGetPartnerResponseType() {
        return new GetPartnerResponseType();
    }

    /**
     * Create an instance of {@link CheckAddressRequestType }
     * 
     */
    public CheckAddressRequestType createCheckAddressRequestType() {
        return new CheckAddressRequestType();
    }

    /**
     * Create an instance of {@link CheckAddressResponseType }
     * 
     */
    public CheckAddressResponseType createCheckAddressResponseType() {
        return new CheckAddressResponseType();
    }

    /**
     * Create an instance of {@link ChangePartnerMainAddressResponse }
     * 
     */
    public ChangePartnerMainAddressResponse createChangePartnerMainAddressResponse() {
        return new ChangePartnerMainAddressResponse();
    }

    /**
     * Create an instance of {@link ChangePartnerMainAddressResponseType }
     * 
     */
    public ChangePartnerMainAddressResponseType createChangePartnerMainAddressResponseType() {
        return new ChangePartnerMainAddressResponseType();
    }

    /**
     * Create an instance of {@link ChangePersonDataRequestType }
     * 
     */
    public ChangePersonDataRequestType createChangePersonDataRequestType() {
        return new ChangePersonDataRequestType();
    }

    /**
     * Create an instance of {@link ChangePersonDataResponseType }
     * 
     */
    public ChangePersonDataResponseType createChangePersonDataResponseType() {
        return new ChangePersonDataResponseType();
    }

    /**
     * Create an instance of {@link ChangePersonDataResponse.BetroffeneObjekte }
     * 
     */
    public ChangePersonDataResponse.BetroffeneObjekte createChangePersonDataResponseBetroffeneObjekte() {
        return new ChangePersonDataResponse.BetroffeneObjekte();
    }

    /**
     * Create an instance of {@link ChangeCommunicationObjectRequestType }
     * 
     */
    public ChangeCommunicationObjectRequestType createChangeCommunicationObjectRequestType() {
        return new ChangeCommunicationObjectRequestType();
    }

    /**
     * Create an instance of {@link ChangeCommunicationObjectResponseType }
     * 
     */
    public ChangeCommunicationObjectResponseType createChangeCommunicationObjectResponseType() {
        return new ChangeCommunicationObjectResponseType();
    }

    /**
     * Create an instance of {@link DeleteCommunicationRequestType }
     * 
     */
    public DeleteCommunicationRequestType createDeleteCommunicationRequestType() {
        return new DeleteCommunicationRequestType();
    }

    /**
     * Create an instance of {@link DeleteCommunicationObjectResponseType }
     * 
     */
    public DeleteCommunicationObjectResponseType createDeleteCommunicationObjectResponseType() {
        return new DeleteCommunicationObjectResponseType();
    }

    /**
     * Create an instance of {@link CreateNewCommunicationObjectRequestType }
     * 
     */
    public CreateNewCommunicationObjectRequestType createCreateNewCommunicationObjectRequestType() {
        return new CreateNewCommunicationObjectRequestType();
    }

    /**
     * Create an instance of {@link CreateNewCommunicationObjectResponseType }
     * 
     */
    public CreateNewCommunicationObjectResponseType createCreateNewCommunicationObjectResponseType() {
        return new CreateNewCommunicationObjectResponseType();
    }

    /**
     * Create an instance of {@link ChangePartnerMainAddressRequestType.UeberschreibeZustelladresseInVertraegen }
     * 
     */
    public ChangePartnerMainAddressRequestType.UeberschreibeZustelladresseInVertraegen createChangePartnerMainAddressRequestTypeUeberschreibeZustelladresseInVertraegen() {
        return new ChangePartnerMainAddressRequestType.UeberschreibeZustelladresseInVertraegen();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPartnerRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on4partner", name = "GetPartnerRequest")
    public JAXBElement<GetPartnerRequestType> createGetPartnerRequest(GetPartnerRequestType value) {
        return new JAXBElement<GetPartnerRequestType>(_GetPartnerRequest_QNAME, GetPartnerRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPartnerResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on4partner", name = "GetPartnerResponse")
    public JAXBElement<GetPartnerResponseType> createGetPartnerResponse(GetPartnerResponseType value) {
        return new JAXBElement<GetPartnerResponseType>(_GetPartnerResponse_QNAME, GetPartnerResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CheckAddressRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on4partner", name = "CheckAddressRequest")
    public JAXBElement<CheckAddressRequestType> createCheckAddressRequest(CheckAddressRequestType value) {
        return new JAXBElement<CheckAddressRequestType>(_CheckAddressRequest_QNAME, CheckAddressRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CheckAddressResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on4partner", name = "CheckAddressResponse")
    public JAXBElement<CheckAddressResponseType> createCheckAddressResponse(CheckAddressResponseType value) {
        return new JAXBElement<CheckAddressResponseType>(_CheckAddressResponse_QNAME, CheckAddressResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangePartnerMainAddressRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on4partner", name = "ChangePartnerMainAddressRequest")
    public JAXBElement<ChangePartnerMainAddressRequestType> createChangePartnerMainAddressRequest(ChangePartnerMainAddressRequestType value) {
        return new JAXBElement<ChangePartnerMainAddressRequestType>(_ChangePartnerMainAddressRequest_QNAME, ChangePartnerMainAddressRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangePersonDataRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on4partner", name = "ChangePersonDataRequest")
    public JAXBElement<ChangePersonDataRequestType> createChangePersonDataRequest(ChangePersonDataRequestType value) {
        return new JAXBElement<ChangePersonDataRequestType>(_ChangePersonDataRequest_QNAME, ChangePersonDataRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangeCommunicationObjectRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on4partner", name = "ChangeCommunicationObjectRequest")
    public JAXBElement<ChangeCommunicationObjectRequestType> createChangeCommunicationObjectRequest(ChangeCommunicationObjectRequestType value) {
        return new JAXBElement<ChangeCommunicationObjectRequestType>(_ChangeCommunicationObjectRequest_QNAME, ChangeCommunicationObjectRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangeCommunicationObjectResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on4partner", name = "ChangeCommunicationObjectResponse")
    public JAXBElement<ChangeCommunicationObjectResponseType> createChangeCommunicationObjectResponse(ChangeCommunicationObjectResponseType value) {
        return new JAXBElement<ChangeCommunicationObjectResponseType>(_ChangeCommunicationObjectResponse_QNAME, ChangeCommunicationObjectResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteCommunicationRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on4partner", name = "DeleteCommunicationObjectRequest")
    public JAXBElement<DeleteCommunicationRequestType> createDeleteCommunicationObjectRequest(DeleteCommunicationRequestType value) {
        return new JAXBElement<DeleteCommunicationRequestType>(_DeleteCommunicationObjectRequest_QNAME, DeleteCommunicationRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteCommunicationObjectResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on4partner", name = "DeleteCommunicationObjectResponse")
    public JAXBElement<DeleteCommunicationObjectResponseType> createDeleteCommunicationObjectResponse(DeleteCommunicationObjectResponseType value) {
        return new JAXBElement<DeleteCommunicationObjectResponseType>(_DeleteCommunicationObjectResponse_QNAME, DeleteCommunicationObjectResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateNewCommunicationObjectRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on4partner", name = "CreateNewCommunicationObjectRequest")
    public JAXBElement<CreateNewCommunicationObjectRequestType> createCreateNewCommunicationObjectRequest(CreateNewCommunicationObjectRequestType value) {
        return new JAXBElement<CreateNewCommunicationObjectRequestType>(_CreateNewCommunicationObjectRequest_QNAME, CreateNewCommunicationObjectRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateNewCommunicationObjectResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:at.vvo.omds.types.omds3types.v1-3-0.on4partner", name = "CreateNewCommunicationObjectResponse")
    public JAXBElement<CreateNewCommunicationObjectResponseType> createCreateNewCommunicationObjectResponse(CreateNewCommunicationObjectResponseType value) {
        return new JAXBElement<CreateNewCommunicationObjectResponseType>(_CreateNewCommunicationObjectResponse_QNAME, CreateNewCommunicationObjectResponseType.class, null, value);
    }

}
