Versionen
================================
OMDS 3 Version 1.0.0 - erste Veroeffentlichung der 'Quick-Wins'
OMDS 3 Version 1.1.0 - Veroeffentlichung Schaden, Services die von Maklern betrieben werden, Bugfixes zu 1.0.0
OMDS 3 Version 1.1.1 - Bugfixes zu 1.1.0: Bei einigen Elementen war der z.B. Typ nicht definiert



Zweck der Files in Version 1.1.1
================================

Fuer den Versicherer:
- omds3Services-x-.wsdl - Webservice Definition fuer den Versicherer
- omds3CommonServiceTypes-x-.xsd - Grundlegende gemeinsame Typen und Elemente
- omds3ServiceTypes-x-.xsd - Typen und Elemente der Webservices
- omds3ExampleVuServiceTypes.xsd - Beispiel f�r abgeleitete, selbstdefinierte Erweiterungen des Versicherers

Fuer den Makler:
- omds3ServicesBroker-x-.wsdl - Webservice Definition fuer den Makler
- omds3CommonServiceTypes-x-.xsd - Grundlegende gemeinsame Typen und Elemente
- omds3ServiceTypes-x-.xsd - Typen und Elemente der Webservices

wobei -x- die jeweilige Version bezeichnet.

